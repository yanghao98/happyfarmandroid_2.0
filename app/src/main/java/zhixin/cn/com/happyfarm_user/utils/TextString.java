package zhixin.cn.com.happyfarm_user.utils;

/**
 * TextString
 *
 * @author: Administrator.
 * @date: 2019/2/18
 */

public class TextString {

    public static final String DialogTitleText = "立刻登录";

    public static final String DialogContentText = "您尚未登录，是否立刻登录";

    public static final String NetworkRequestFailed = "网络请求失败，请检查一下网络";

    public static final String LandRequestFailed = "租地列表加载失败";

    public static final String PhoneLong = "手机号超出最大长度";

    public static final String InfoTitleText = "完善信息";

    public static final String InfoContentText = "您的信息暂未完善，是否完善您的信息";

    public static final String VIDEO_HELPFUL_HINTS = "请您理解：\n" +
            "\n" +
            "当您操控网络摄像头时，可能会因为您所使用的wifi或移动网络信号质量问题造成略微的延时，请耐心等待！\n" +
            "\n" +
            "此外，摄像头的转动有极限，当您尝试某个方向的转动摄像头不听使唤时，请您换个方向转动，放大缩小功能也一样哦！\n";

    public static final String SCREENSHOTS = "视频暂无播放，无法截图";

    public static final String welfarePageDialogOkButton = "知道了";

    public static final String welfarePageDialogContentText = "您在租用植信地块时，会自动抵扣优惠券金额";

    public static final String prizeExchangesOkButton = "兑换";

    public static final String prizeExchangesContentText = "信息不能修改，确保填写无误后选择兑换";

    public static final String selectBranchOkButton = "确定";

    public static final String selectBranchContentText = "请保证地址填写正确";

    public static final String goLuckDrawExplainOkButton = "查看";

    public static final String goLuckDrawExplainContentText = "以便您能更清楚的了解奖品，请前往奖品说明查看详细奖品描述";


}
