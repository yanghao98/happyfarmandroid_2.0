package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by Administrator on 2018/5/16.
 */

public class OrderAllPage {
    public String orderAllTime;//时间
    public String orderAllImg;//图片
    public String orderAllName;//名字
    public String orderAllNum;//份数
    public String foodMedicine;//是否生物药
    public String orderAllFertilizer;//是否有机肥
    public String orderAllBuy;//买卖家
    public String orderAllBuyName;//买卖家姓名
    public String orderAllDiamond;//交易钻石
    public String orderAllTrading;//交易是否成功
    public String orderAllBuyGoods;//已买到商品
    public String orderConsuAmout;//消费金额标题
    public String orderConsuAmoutP;//消费金额价格
    public String orderViewDetail;//查看物流或详情
    public String orderDiamondIntegral;//钻石或积分
}
