package zhixin.cn.com.happyfarm_user.easyMob;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.easeui.widget.EaseTitleBar;

import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.R;


public class ConverActivity extends ABaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conver);
        EaseTitleBar easeTitleBar = findViewById(R.id.title_bar);
        easeTitleBar.setBackgroundColor(0xff1DAC6D);
        easeTitleBar.setLeftImageResource(R.drawable.ease_mm_title_back);
        easeTitleBar.getLeftLayout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();//隐藏键盘
                finish();
            }
        });
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
    }

    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm =  (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    @Override
    public void back(View view) {

    }
}
