package zhixin.cn.com.happyfarm_user.Page;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.MyVegetablesAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.HistoryPicActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.MyGoods;
import zhixin.cn.com.happyfarm_user.model.searchMyGood;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by Administrator on 2018/5/14.
 */

public class MyVegetablesPage extends Fragment {

    private GridLayoutManager gridLayoutManager;
    private RecyclerView recyclerView;
    private MyVegetablesAdapter adapter;
    private ArrayList<MyGoods> list = new ArrayList<>();
    private RelativeLayout myGoodPrompt;
    private RefreshLayout refreshLayout;
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private Dialog mDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.my_vegetables_page_layout,null,false);
        pageNum = 1;
        pageSize = 15;
        myGoodPrompt = view.findViewById(R.id.my_goods_prompt);
        refreshLayout = view.findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
//        refreshLayout.setEnableLoadMore(false);
        //设置 Footer 为 球脉冲 样式
        refreshLayout.setRefreshFooter(new ClassicsFooter(getContext()).setSpinnerStyle(SpinnerStyle.Scale));
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()).setShowBezierWave(false));
        initRecyclerView(view);
        topRefreshLayout();
        bottomRefreshLayout();
        //自动加载
        recyclerViewButtom();
        StaggerLoadData(false,1);
        return view;
    }
    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.my_vegetables_recycler);

        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new MyVegetablesAdapter(getContext(), list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
//      调用按钮返回事件回调的方法
        adapter.buttonSetOnclick(new MyVegetablesAdapter.ButtonInterface() {
            @Override
            public void onclick(View view, int position,int myGoodId) {
                updateGoodsDialog(myGoodId);
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!list.isEmpty()){
            list.clear();
        }
        StaggerLoadData(false,1);
    }
    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!list.isEmpty())
                    list.clear();
                isLastPage = false;
                pageNum = 1;
                refreshlayout.setNoMoreData(false);//TODO 恢复没有更多数据的原始状态
                StaggerLoadData(false,1);
                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }
    private void StaggerLoadData(Boolean inversion, int pageNum) {
        HttpHelper.initHttpHelper().searchMyGoods(pageNum,pageSize).enqueue(new Callback<searchMyGood>() {
            @Override
            public void onResponse(Call<searchMyGood> call, Response<searchMyGood> response) {
                Log.i("MyVegetablesPage->", JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())){
                    LoadingDialog.closeDialog(mDialog);
                    isLastPage = response.body().getResult().isIsLastPage();
                    if (response.body().getResult().getList().isEmpty()){
                        myGoodPrompt.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }else {
                        myGoodPrompt.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        for (int i = 0;i<response.body().getResult().getList().size();i++){
                            MyGoods data = new MyGoods();
                            data.goodId = response.body().getResult().getList().get(i).getId();
                            data.myGoodCropName = response.body().getResult().getList().get(i).getCropName();
                            int fertilizer = response.body().getResult().getList().get(i).getIsOrganic();
                            if (fertilizer == 0){
                                data.myGoodFat = " ";
                            }else if (fertilizer == 1){
                                data.myGoodFat = "(有机肥)";
                            }else {
                                data.myGoodFat = "(无机肥)";
                            }
                            data.myGoodName = response.body().getResult().getList().get(i).getSellerNickname();
                            int l = response.body().getResult().getList().get(i).getNum();
                            data.myGoodNum = String.valueOf(response.body().getResult().getList().get(i).getNum());
                            data.myGoodDiamond = String.valueOf((response.body().getResult().getList().get(i).getPrice()));
                            data.myGoodIntegral = String.valueOf((response.body().getResult().getList().get(i).getIntegral()*l));
                            data.myGoodImg = response.body().getResult().getList().get(i).getCropImg();
                            list.add(data);
                        }
                        adapter.notifyDataSetChanged();

                    }
                }else {
                    LoadingDialog.closeDialog(mDialog);
                    myGoodPrompt.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<searchMyGood> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                myGoodPrompt.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    public void recyclerViewButtom(){
        //上拉滑动自动请求数据
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                    if(lastVisiblePosition >= gridLayoutManager.getItemCount() - 1){
                        if (isLastPage) {
//                            Toast.makeText(getContext(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            StaggerLoadData(false, pageNum);

                        }
                    }
                }
            }
        });
    }
    //上拉加载
    public void bottomRefreshLayout() {
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("info", "上拉刷新: "+isLastPage);

                        if (isLastPage) {
                            Toast.makeText(getContext(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();////TODO 将不会再次触发加载更多事件
                        } else {
//                            Toast.makeText(getApplication(), "数据加载完毕", Toast.LENGTH_SHORT).show();
                            pageNum++;
                            StaggerLoadData(false, pageNum);
//                            adapter.loadMore(initData());
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }

    /**
     * 蔬菜下架弹框
     */
    private void updateGoodsDialog(int goodId){
        String titleText = "立即下架";
        String contentText = "您确定要下架您的蔬菜吗？";
        final AlertUtilBest diyDialog = new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(titleText)
                .setContent(contentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        updateGoodsApi(goodId);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    /**
     * 蔬菜下架接口
     * @param goodId 蔬菜id
     */
    private void updateGoodsApi(int goodId) {
        HttpHelper.initHttpHelper().updateGoodsState(goodId,"-1").enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if (("success").equals(response.body().getFlag())){
                    Toast.makeText(getContext(),"商品下架成功",Toast.LENGTH_SHORT).show();
                    mDialog = LoadingDialog.createLoadingDialog(getContext(), "");
                    if (list != null){
                        list.clear();
                        StaggerLoadData(false,1);
                    }else {
                        StaggerLoadData(false,1);
                    }
                }else {
                    Toast.makeText(getContext(),response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }
}
