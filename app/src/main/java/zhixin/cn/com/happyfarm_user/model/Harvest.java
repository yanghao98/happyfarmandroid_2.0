package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by DELL on 2018/3/21.
 */

public class Harvest {

    String vgetableName;

    String cropName;

    String cropId;

    private int harvestDiamond;


    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getVgetableName() {
        return vgetableName;
    }

    public void setVgetableName(String vgetableName) {
        this.vgetableName = vgetableName;
    }

    public int getHarvestDiamond() {
        return harvestDiamond;
    }

    public void setHarvestDiamond(int harvestDiamond) {
        this.harvestDiamond = harvestDiamond;
    }
}
