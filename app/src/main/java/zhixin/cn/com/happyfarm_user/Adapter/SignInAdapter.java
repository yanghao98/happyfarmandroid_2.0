package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Crop;
import zhixin.cn.com.happyfarm_user.model.SignRecord;
import zhixin.cn.com.happyfarm_user.utils.BitmapRoundImageView;

public class SignInAdapter extends RecyclerView.Adapter<SignInAdapter.StaggerViewHolder> {

    private Context myContext;
    private List<SignRecord> myList;

    public  SignInAdapter(Context context, List<SignRecord> list) {
        this.myContext = context;
        this.myList = list;
    }

    @NonNull
    @Override
    public StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(myContext, R.layout.sign_in_item, null);
        return new SignInAdapter.StaggerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StaggerViewHolder staggerViewHolder, int i) {
        //从集合里拿对应item数据对象
//        Crop dataBean = mList.get(position);
        staggerViewHolder.setData(myList.get(i));
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if(myList!=null&&myList.size()>0){
            return myList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {

        private TextView day1;
        private BitmapRoundImageView sign_in_day_image1;
        private TextView day2;
        private BitmapRoundImageView sign_in_day_image2;
        private RelativeLayout sign_in_item_layout1;
        private RelativeLayout sign_in_item_layout2;


        public StaggerViewHolder(@NonNull View itemView) {
            super(itemView);
            day1 = itemView.findViewById(R.id.sign_in_day1);
            sign_in_day_image1 = itemView.findViewById(R.id.sign_in_day_image1);
            day2 = itemView.findViewById(R.id.sign_in_day2);
            sign_in_day_image2 = itemView.findViewById(R.id.sign_in_day_image2);
            sign_in_item_layout1 = itemView.findViewById(R.id.sign_in_item_layout1);
            sign_in_item_layout2 = itemView.findViewById(R.id.sign_in_item_layout2);
        }
        public void setData(SignRecord data) {
//            Log.i("", "setData: " + JSON.toJSONString(data));

            if (1== data.getType() || 2 == data.getType()) {
                sign_in_item_layout1.setVisibility(View.VISIBLE);
                sign_in_item_layout2.setVisibility(View.GONE);
            } else {
                sign_in_item_layout1.setVisibility(View.GONE);
                sign_in_item_layout2.setVisibility(View.VISIBLE);
            }
            switch (data.getType()) {
                case 1:
                    day1.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_yes).into(sign_in_day_image1);
                    break;
                case 2:
                    day1.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_no).into(sign_in_day_image1);
                    break;
                case 3:
                    day2.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_henhouse_yes).into(sign_in_day_image2);
                    break;
                case 4:
                    day2.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_henhouse_no).into(sign_in_day_image2);
                    break;
                case 5:
                    day2.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_incubation_yes).into(sign_in_day_image2);
                    break;
                case 6:
                    day2.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_incubation_no).into(sign_in_day_image2);
                    break;
                case 7:
                    day2.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_feed_yes).into(sign_in_day_image2);
                    break;
                case 8:
                    day2.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_feed_no).into(sign_in_day_image2);
                    break;
                case 9:
                    day2.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_chicken_yes).into(sign_in_day_image2);
                    break;
                case 10:
                    day2.setText(data.getDay()+"");
                    Glide.with(myContext).load(R.mipmap.sign_in_chicken_no).into(sign_in_day_image2);
                    break;
            }
        }
    }
}