package zhixin.cn.com.happyfarm_user;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import com.githang.statusbar.StatusBarCompat;
import zhixin.cn.com.happyfarm_user.Page.FarmPage;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;

public class LandListActivity extends ABaseActivity {
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.land_list_activity);
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        this.activity = LandListActivity.this;
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.personal_content,new FarmPage())
                .commit();
    }

    @Override
    public void back(View view) {

    }
}
