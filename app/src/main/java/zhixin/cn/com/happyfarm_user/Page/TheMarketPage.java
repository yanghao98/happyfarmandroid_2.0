package zhixin.cn.com.happyfarm_user.Page;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.ViewPagerAdapter;
//import zhixin.cn.com.happyfarm_user.HMSPushHelper;
import zhixin.cn.com.happyfarm_user.PersonalMessageActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by DELL on 2018/2/1.
 */

public class TheMarketPage extends Fragment {

    private ViewPager viewPager;
    private MagicIndicator indicator;
    private List<Fragment> frags;
    private List<String> titles;
    private RelativeLayout titleBarLayout;
    private Activity activity;
    private View myView;
    private Badge messageBadge;


    @Nullable
    @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.the_market_layout,null,false);
        myView = view;
        TextView title = view.findViewById(R.id.main_page_title_name);

        title.setText("菜场");
        ImageView imageView = view.findViewById(R.id.main_page_message_icon);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PersonalMessageActivity.class);
                startActivity(intent);
            }
        });
        initData();
        initPager(view);
        setStatusBarHeight();
        initIndicator();
        //登录环信
        loginEM();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(getActivity());
        //相对布局使用方法
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        titleBarLayout.setLayoutParams(params);
    }
    /**
     * 获取状态栏高度
     * @param context 上下文
     * @return int 状态栏高度值
     */
    protected static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    private void initPager(View view) {
        titleBarLayout = view.findViewById(R.id.title_bar_layout);
        viewPager = view.findViewById(R.id.market_viewpage);
        indicator = view.findViewById(R.id.top_indicator);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager(), frags);
        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(adapter);
        messageBadge = new QBadgeView(getContext())
                .bindTarget(myView.findViewById(R.id.main_page_icon_view_total))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
    }

    private void initIndicator() {
        CommonNavigator navigator = new CommonNavigator(getContext());
        navigator.setAdjustMode(true);//设置平分屏幕宽度
        navigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return frags.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int i) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(titles.get(i));
                titleView.setTextSize(16);
                TextPaint tp = titleView.getPaint();
                tp.setFakeBoldText(true);
                titleView.setNormalColor(Color.parseColor("#9a9a9a"));//未选中颜色
                titleView.setSelectedColor(Color.parseColor("#1DAC6D"));//选中颜色
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPager.setCurrentItem(i);
                    }
                });
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);//设置下滑线  自适应文字长度MODE_WRAP_CONTENT MODE_MATCH_EDGE
                linePagerIndicator.setColors(Color.parseColor("#1DAC6D"));
                return linePagerIndicator;
            }
        });
        indicator.setNavigator(navigator);
        /**设置中间竖线**/
//        LinearLayout titleContainer = navigator.getTitleContainer(); // must after setNavigator
//        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
//        titleContainer.setDividerPadding(UIUtil.dip2px(this, 20));
//        titleContainer.setDividerDrawable(getResources().getDrawable(R.drawable.simple_splitter));
        /**设置中间竖线**/
        //绑定viewpager滚动事件
        ViewPagerHelper.bind(indicator, viewPager);
    }

    private void initData(){
        frags = new ArrayList<>();
        frags.add(new AllGoodPage());
        frags.add(new MyVegetablesPage());
        frags.add(new OrderRecordPage());

        titles = new ArrayList<>();
        titles.add("所有商品");
        titles.add("我的商品");
        titles.add("订单记录");
    }

    public void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("failed").equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(activity, "User_data", strArr);
                    myView.findViewById(R.id.main_page_message_icon).setVisibility(View.GONE);
                } else {
                    myView.findViewById(R.id.main_page_message_icon).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
                myView.findViewById(R.id.main_page_message_icon).setVisibility(View.GONE);
            }
        });
    }


    /**
     * 设置消息显示数量
     * @param num 消息数量
     * position 设置显示tablayout的item 暂时未时候，考虑后期可能所有的item都需要
     */
    private void setMsgNum( int num){
        if (messageBadge != null){
//            Log.i(TAG, "消息设置成功setMsgNum: "+num);
            messageBadge.setBadgeNumber(num);
        }else {
//            Log.i(TAG, "消息设置失败setMsgNum: "+num);
        }
    }

    //这个页面因为进入到首页的时候回被预加载
    private void loginEM(){
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (response.isSuccessful() && response.body().getFlag().equals("success")){
                    GetSelfInfo.ResultBean resultBean = response.body().getResult();
                    loginEM(String.valueOf(resultBean.getId()),resultBean.getPassword());
                }
            }
            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }
    //login环信
    private void loginEM(String user, String pwd){
        EMClient.getInstance().login(user,pwd,new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                Log.d("EMLogmain","登录环信成功");
                //TODO 以下两个方法是为了保证进入主页面后本地会话和群组都 load 完毕。
                EMClient.getInstance().chatManager().loadAllConversations();
                EMClient.getInstance().groupManager().loadAllGroups();
                // 获取华为 HMS 推送 token
//                HMSPushHelper.getInstance().connectHMS(getActivity());
//                HMSPushHelper.getInstance().getHMSPushToken();
                setMsgNum(EMClient.getInstance().chatManager().getUnreadMessageCount());

                Log.i("打印消息数量2", "数量" + EMClient.getInstance().chatManager().getUnreadMessageCount());
            }

            @Override
            public void onProgress(int progress, String status) {}

            @Override
            public void onError(int code, String message) {
//                Log.d("EMLogmain", "登录聊天服务器失败！"+code+"信息："+message);
                Toast.makeText(getContext(),"登录聊天服务器失败！",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
            int total = EMClient.getInstance().chatManager().getUnreadMessageCount();
            setMsgNum(total);
    }
    public void getEMNumber(int number){
        setMsgNum(number);
    }
}
