package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;

/**
 * Created by DELL on 2018/3/18.
 */

public class ListDialogAdapter extends BaseAdapter{

    private List<String> mlist;
    private Context mContext;

    public ListDialogAdapter(Context context, List<String> list) {
        this.mContext = context;
        mlist = new ArrayList<String>();
        this.mlist = list;
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Person person = null;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_dialog_item,null);
            person = new Person();
            person.name = convertView.findViewById(R.id.tv_name);
            convertView.setTag(person);
        }else{
            person = (Person)convertView.getTag();
        }
        person.name.setText(mlist.get(position).toString());
        return convertView;
    }
    class Person{
        TextView name;
    }

}
