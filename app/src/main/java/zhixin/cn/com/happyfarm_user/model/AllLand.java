package zhixin.cn.com.happyfarm_user.model;


import java.math.BigDecimal;
import java.util.List;

public class AllLand {

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {


        private InfoBean info;
        private List<ListBean> list;

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class InfoBean {


            private int maxNo;
            private int maxX;
            private int minX;
            private int maxY;
            private int minY;

            public int getMaxNo() {
                return maxNo;
            }

            public void setMaxNo(int maxNo) {
                this.maxNo = maxNo;
            }

            public int getMaxX() {
                return maxX;
            }

            public void setMaxX(int maxX) {
                this.maxX = maxX;
            }

            public int getMinX() {
                return minX;
            }

            public void setMinX(int minX) {
                this.minX = minX;
            }

            public int getMaxY() {
                return maxY;
            }

            public void setMaxY(int maxY) {
                this.maxY = maxY;
            }

            public int getMinY() {
                return minY;
            }

            public void setMinY(int minY) {
                this.minY = minY;
            }
        }

        public static class ListBean {
            /**
             * id : 1
             * landNo : 1
             * userId : 0
             * details :
             * alias : A1-NE
             * landType : 1
             * x : 11
             * y : 0
             * rent : 0.01
             * leaseTime : 1548835393000
             * leaseTerm : 0
             * isHosted : 0
             * cameraIp : 192.168.1.10
             * loginHeader : null
             * startTime : 1548654671000
             * updateTime : 1564392299000
             * managerId : 1
             * state : 1
             * expirationTime : 1580371393000
             * isShed : 0
             * isUsed : 0
             * userName :
             * cropName :
             * headImg :
             * tel :
             * taskName : null
             * isCollections : null
             * isFriend : null
             */

            private int id;
            private int landNo;
            private int userId;
            private String details;
            private String alias;
            private int landType;
            private int x;
            private int y;
            private BigDecimal rent;
            private long leaseTime;
            private int leaseTerm;
            private int isHosted;
            private String cameraIp;
            private Object loginHeader;
            private long startTime;
            private long updateTime;
            private int managerId;
            private int state;
            private long expirationTime;
            private int isShed;
//            private int isUsed;
            private String userName;
            private String cropName;
            private String headImg;
            private String tel;
            private Object taskName;
            private Object isCollections;
            private Object isFriend;
            //开水编号
            private int waterNumber;
            //开灯编号
            private int lampNumber;
            //继电器ip
            private String relayIp;


            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getLandNo() {
                return landNo;
            }

            public void setLandNo(int landNo) {
                this.landNo = landNo;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getDetails() {
                return details;
            }

            public void setDetails(String details) {
                this.details = details;
            }

            public String getAlias() {
                return alias;
            }

            public void setAlias(String alias) {
                this.alias = alias;
            }

            public int getLandType() {
                return landType;
            }

            public void setLandType(int landType) {
                this.landType = landType;
            }

            public int getX() {
                return x;
            }

            public void setX(int x) {
                this.x = x;
            }

            public int getY() {
                return y;
            }

            public void setY(int y) {
                this.y = y;
            }

            public BigDecimal getRent() {
                return rent;
            }

            public void setRent(BigDecimal rent) {
                this.rent = rent;
            }

            public long getLeaseTime() {
                return leaseTime;
            }

            public void setLeaseTime(long leaseTime) {
                this.leaseTime = leaseTime;
            }

            public int getLeaseTerm() {
                return leaseTerm;
            }

            public void setLeaseTerm(int leaseTerm) {
                this.leaseTerm = leaseTerm;
            }

            public int getIsHosted() {
                return isHosted;
            }

            public void setIsHosted(int isHosted) {
                this.isHosted = isHosted;
            }

            public String getCameraIp() {
                return cameraIp;
            }

            public void setCameraIp(String cameraIp) {
                this.cameraIp = cameraIp;
            }

            public Object getLoginHeader() {
                return loginHeader;
            }

            public void setLoginHeader(Object loginHeader) {
                this.loginHeader = loginHeader;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public long getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(long updateTime) {
                this.updateTime = updateTime;
            }

            public int getManagerId() {
                return managerId;
            }

            public void setManagerId(int managerId) {
                this.managerId = managerId;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public long getExpirationTime() {
                return expirationTime;
            }

            public void setExpirationTime(long expirationTime) {
                this.expirationTime = expirationTime;
            }

            public int getIsShed() {
                return isShed;
            }

            public void setIsShed(int isShed) {
                this.isShed = isShed;
            }

//            public int getIsUsed() {
//                return isUsed;
//            }
//
//            public void setIsUsed(int isUsed) {
//                this.isUsed = isUsed;
//            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public String getCropName() {
                return cropName;
            }

            public void setCropName(String cropName) {
                this.cropName = cropName;
            }

            public String getHeadImg() {
                return headImg;
            }

            public void setHeadImg(String headImg) {
                this.headImg = headImg;
            }

            public String getTel() {
                return tel;
            }

            public void setTel(String tel) {
                this.tel = tel;
            }

            public Object getTaskName() {
                return taskName;
            }

            public void setTaskName(Object taskName) {
                this.taskName = taskName;
            }

            public Object getIsCollections() {
                return isCollections;
            }

            public void setIsCollections(Object isCollections) {
                this.isCollections = isCollections;
            }

            public Object getIsFriend() {
                return isFriend;
            }

            public void setIsFriend(Object isFriend) {
                this.isFriend = isFriend;
            }

            public int getWaterNumber() {
                return waterNumber;
            }

            public void setWaterNumber(int waterNumber) {
                this.waterNumber = waterNumber;
            }

            public int getLampNumber() {
                return lampNumber;
            }

            public void setLampNumber(int lampNumber) {
                this.lampNumber = lampNumber;
            }

            public String getRelayIp() {
                return relayIp;
            }

            public void setRelayIp(String relayIp) {
                this.relayIp = relayIp;
            }

        }
    }
}
