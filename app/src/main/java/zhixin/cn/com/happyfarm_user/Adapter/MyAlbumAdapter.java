package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.felipecsl.gifimageview.library.GifImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.MyAlbum;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

import static zhixin.cn.com.happyfarm_user.utils.Utils.downloadBitmap;

public class MyAlbumAdapter extends RecyclerView.Adapter<MyAlbumAdapter.StaggerViewHolder>{

    private CuringAdapter.MyItemClickListener mItemClickListener;
    private Context mContext;
    private List<MyAlbum> mList;
    private String TAG = "MyAlbumAdapter";

    public MyAlbumAdapter(Context context, List<MyAlbum> list){
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public MyAlbumAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.album_item, null);
        MyAlbumAdapter.StaggerViewHolder staggerViewHolder = new MyAlbumAdapter.StaggerViewHolder(view);
        return staggerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyAlbumAdapter.StaggerViewHolder holder, int position) {
        MyAlbum dataBean = mList.get(position);
        holder.setData(dataBean);
        if (mItemClickListener != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = holder.getLayoutPosition();
                    mItemClickListener.onItemClick(view,pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(mList!=null&&mList.size()>0){
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView times;
        private TextView photoAbstract;

        public StaggerViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.album_gifImageView);
            times = itemView.findViewById(R.id.album_time);
            photoAbstract = itemView.findViewById(R.id.photo_abstract);
        }

        public void setData(MyAlbum data) {

//            创建MediaMetadataRetriever对象
//            try {
////                Log.d("视频路径", "setData: " + data.videoUrl);
//                MediaMetadataRetriever mmr=new MediaMetadataRetriever();
//                //绑定资源
//                mmr.setDataSource(data.videoUrl, new HashMap());
//                //获取第一帧图像的bitmap对象
//                Bitmap bitmap=mmr.getFrameAtTime();
//                //加载到ImageView控件上
//                imageView.setImageBitmap(bitmap);
//                times.setText(data.Times);
//            } catch (RuntimeException e){
////                Log.e(TAG, "setData: "+"有以前的脏数据存在需要清除");
//            }
            times.setText("相册名：" + data.albumName);
            if (NumUtil.checkNull(data.albumFirstPicture)) {
                imageView.setImageResource(R.drawable.icon_test);
            } else {
                //Picasso使用了流式接口的调用方式
                //Picasso类是核心实现类。
                //实现图片加载功能至少需要三个参数：
                Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                        .load(data.albumFirstPicture)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                        .error(R.drawable.icon_user_img)
                        .into(imageView);//into(ImageView targetImageView)：图片最终要展示的地方。
            }
            if ("".equals(data.photoAbstract)) {
                photoAbstract.setText("这个人很懒没有做任何描述");
            } else {
                photoAbstract.setText("" + data.photoAbstract);
            }

        }
    }




        /**
         * 创建一个回调接口
         */
    public interface MyItemClickListener {
        void onItemClick(View view, int position);
    }

    /**
     * 在activity里面adapter就是调用的这个方法,将点击事件监听传递过来,并赋值给全局的监听
     *
     * @param myItemClickListener
     */
    public void setItemClickListener(CuringAdapter.MyItemClickListener myItemClickListener) {
        this.mItemClickListener = myItemClickListener;
    }

}
