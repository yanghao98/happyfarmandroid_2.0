package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by DELL on 2018/2/6.
 */

public class PublicKeyList {


    /**
     * result : MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCpAaxvRYvAfHl+z95B0+iwmaZkJD7+nRFCfJNL8NlwjdxLkaUf1qZyDIV2V3y2MSQtn643eaE25aUpdtd9P9Xh2PxrzuX6eUXxbbvlbjEs+nE02vGH50b9j7Ixozt6A6VzkTi6yN3zWRIbgssaO51pbZnat81LBQ69f4wrzkg8MwIDAQAB
     * flag : success
     */

    private String result;
    private String flag;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
