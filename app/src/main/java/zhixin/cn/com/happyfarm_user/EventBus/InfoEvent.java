package zhixin.cn.com.happyfarm_user.EventBus;

/**
 * Created by HuYueling on 2018/4/27.
 */

public class InfoEvent {
    public String INFOSUCCESSFUL;


    public InfoEvent(String INFOSUCCESSFUL) {
        this.INFOSUCCESSFUL = INFOSUCCESSFUL;
    }

    public String getINFOSUCCESSFUL() {
        return INFOSUCCESSFUL;
    }

    public void setINFOSUCCESSFUL(String INFOSUCCESSFUL) {
        this.INFOSUCCESSFUL = INFOSUCCESSFUL;
    }

}
