package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import cn.sharesdk.onekeyshare.OnekeyShare;
import zhixin.cn.com.happyfarm_user.base.App;

public class ShareActivity extends AppCompatActivity {


    private TextView imageText;
    private ImageView imageUrl;
    private String invitationCode;
    private TextView shareText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        Intent intent = getIntent();
        invitationCode = intent.getStringExtra("invitationCode");
        initView();
        shareImage();
    }


    private void initView () {
        imageText = findViewById(R.id.image_text);
        imageUrl = findViewById(R.id.image_url);
        shareText = findViewById(R.id.share_text);
        Picasso.get()
                .load("http://img.trustwusee.com/downloadApp.png")
                .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                .into(imageUrl);
//        System.out.println("用户名是：" + App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("nickName",""));

        imageText.setText(Html.fromHtml(getResources().getString(R.string.share_activit_content,App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("nickName",""),invitationCode)));
    }

    //分享功能
    public void shareImage () {
        shareText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showShare();
            }
        });
    }

    /**
     * 分享方法
     *
     */
    private void showShare() {
        Bitmap bitmap = null;
        String message = "您的好友"+App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("nickName","")+"邀请您加入植信家园，邀请码为"+invitationCode+"。点击查看";
        String url = "https://www.trustwusee.com/downloadApp";
        try {
            bitmap = captureView(findViewById(R.id.bbbbb));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
        // title标题，印象笔记、邮箱、信息、微信、人人网、QQ和QQ空间使用
        oks.setTitle("植信APP");
        // titleUrl是标题的网络链接，仅在Linked-in,QQ和QQ空间使用
        oks.setTitleUrl(url);
        // text是分享文本，所有平台都需要这个字段
        oks.setText(message);
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment(message);
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite("植信APP");
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl(url);
        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
        oks.setImageData(bitmap);
        // 启动分享GUI
        oks.show(this);
    }


    //诺,主要观察这两个方法

    /**
     * 压缩图片
     *
     * @param bgimage
     * @param newWidth
     * @param newHeight
     * @return
     */
    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 缩放图片动作
        //matrix.postScale(scaleWidth, scaleHeight);//TODO 因为宽高不确定的因素,所以不缩放
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
                (int) height, matrix, true);
        return bitmap;
    }

    /**
     * 截取指定View为图片
     *
     * @param view
     * @return
     * @throws Throwable
     */
    public static Bitmap captureView(View view) throws Throwable {
        Bitmap bm = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        view.draw(new Canvas(bm));
        return bm;
    }


    public void back(View view) {
        finish();
    }

}
