package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class SearchCropList {


    /**
     * result : {"pageNum":1,"pageSize":19,"size":19,"orderBy":null,"startRow":0,"endRow":18,"total":19,"pages":1,"list":[{"id":3,"cropName":"花椒","plantingDiamond":1,"harvestDiamond":1,"storeSale":1,"details":"美颜","startTime":1516066847000,"cycle":1,"remark":"","updateTime":1522381941000,"managerId":1,"priority":4,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":10,"harvestIntegral":10,"storeSaleIntegral":10},{"id":1,"cropName":"茄子","plantingDiamond":1,"harvestDiamond":1,"storeSale":1,"details":"好吃","startTime":1516070598000,"cycle":-1,"remark":"","updateTime":1522308585000,"managerId":1,"priority":4,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":10,"harvestIntegral":10,"storeSaleIntegral":10},{"id":15,"cropName":"小尖椒","plantingDiamond":5,"harvestDiamond":5,"storeSale":5,"details":"同样是辣的爽快","startTime":1516870984000,"cycle":5,"remark":"","updateTime":1522057377000,"managerId":1,"priority":4,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":50,"harvestIntegral":50,"storeSaleIntegral":50},{"id":2,"cropName":"胡萝卜","plantingDiamond":15,"harvestDiamond":1,"storeSale":1,"details":"大补","startTime":1516068362000,"cycle":1,"remark":"","updateTime":1522308590000,"managerId":4,"priority":6,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":150,"harvestIntegral":10,"storeSaleIntegral":10},{"id":10,"cropName":"小辣椒123","plantingDiamond":3,"harvestDiamond":3,"storeSale":10,"details":"美颜","startTime":1516070787000,"cycle":1,"remark":"","updateTime":1522308620000,"managerId":1,"priority":8,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":30,"harvestIntegral":30,"storeSaleIntegral":100},{"id":5,"cropName":"木瓜","plantingDiamond":2,"harvestDiamond":2,"storeSale":1,"details":"秒到","startTime":1516070594000,"cycle":-1,"remark":"","updateTime":1522308610000,"managerId":1,"priority":9,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":20,"harvestIntegral":20,"storeSaleIntegral":10},{"id":24,"cropName":"123","plantingDiamond":123,"harvestDiamond":123,"storeSale":123,"details":"123","startTime":1520991505000,"cycle":123,"remark":"123","updateTime":1522057352000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":1230,"harvestIntegral":1230,"storeSaleIntegral":1230},{"id":14,"cropName":"朝天椒","plantingDiamond":58,"harvestDiamond":5,"storeSale":5,"details":"够辣，够带劲","startTime":1516870181000,"cycle":20,"remark":"","updateTime":1522057312000,"managerId":14,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":580,"harvestIntegral":50,"storeSaleIntegral":50},{"id":7,"cropName":"大白菜","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"鲜嫩的大白菜","startTime":1516066931000,"cycle":20,"remark":"","updateTime":1522116670000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":18,"cropName":"红薯","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"胖胖的红薯，香甜可口","startTime":1516931604000,"cycle":30,"remark":"","updateTime":1522057392000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":17,"cropName":"豇豆","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"美颜","startTime":1516871473000,"cycle":20,"remark":"","updateTime":1522308622000,"managerId":12,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":9,"cropName":"辣椒","plantingDiamond":3,"harvestDiamond":3,"storeSale":10,"details":"美颜","startTime":1516067206000,"cycle":-1,"remark":"","updateTime":1522308617000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":30,"harvestIntegral":30,"storeSaleIntegral":100},{"id":4,"cropName":"龙葵","plantingDiamond":1,"harvestDiamond":1,"storeSale":1,"details":"迷倒万千少女","startTime":1516069438000,"cycle":-1,"remark":"","updateTime":1522308605000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":10,"harvestIntegral":10,"storeSaleIntegral":10},{"id":13,"cropName":"茄子","plantingDiamond":5,"harvestDiamond":5,"storeSale":5,"details":"长茄子，红烧茄子的最佳选择","startTime":1516869866000,"cycle":30,"remark":"","updateTime":1522057376000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":50,"harvestIntegral":50,"storeSaleIntegral":50},{"id":16,"cropName":"四季豆","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"长长的藤蔓上挂着长长的豆","startTime":1516871281000,"cycle":20,"remark":"","updateTime":1522057378000,"managerId":12,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":11,"cropName":"土豆","plantingDiamond":1,"harvestDiamond":1,"storeSale":2,"details":"美颜","startTime":1516070711000,"cycle":-1,"remark":"","updateTime":1522308620000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":10,"harvestIntegral":10,"storeSaleIntegral":20},{"id":12,"cropName":"仙女果","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"晶莹剔透的仙女果，甘甜可口","startTime":1516869163000,"cycle":30,"remark":"周期较长","updateTime":1522057375000,"managerId":12,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":19,"cropName":"小白菜","plantingDiamond":5,"harvestDiamond":5,"storeSale":5,"details":"美颜","startTime":1516936477000,"cycle":20,"remark":"","updateTime":1522308623000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":50,"harvestIntegral":50,"storeSaleIntegral":50},{"id":25,"cropName":"12311","plantingDiamond":123,"harvestDiamond":123,"storeSale":123,"details":"123","startTime":1520991512000,"cycle":123,"remark":"123","updateTime":1522057350000,"managerId":1,"priority":123,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":1230,"harvestIntegral":1230,"storeSaleIntegral":1230}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 19
         * size : 19
         * orderBy : null
         * startRow : 0
         * endRow : 18
         * total : 19
         * pages : 1
         * list : [{"id":3,"cropName":"花椒","plantingDiamond":1,"harvestDiamond":1,"storeSale":1,"details":"美颜","startTime":1516066847000,"cycle":1,"remark":"","updateTime":1522381941000,"managerId":1,"priority":4,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":10,"harvestIntegral":10,"storeSaleIntegral":10},{"id":1,"cropName":"茄子","plantingDiamond":1,"harvestDiamond":1,"storeSale":1,"details":"好吃","startTime":1516070598000,"cycle":-1,"remark":"","updateTime":1522308585000,"managerId":1,"priority":4,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":10,"harvestIntegral":10,"storeSaleIntegral":10},{"id":15,"cropName":"小尖椒","plantingDiamond":5,"harvestDiamond":5,"storeSale":5,"details":"同样是辣的爽快","startTime":1516870984000,"cycle":5,"remark":"","updateTime":1522057377000,"managerId":1,"priority":4,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":50,"harvestIntegral":50,"storeSaleIntegral":50},{"id":2,"cropName":"胡萝卜","plantingDiamond":15,"harvestDiamond":1,"storeSale":1,"details":"大补","startTime":1516068362000,"cycle":1,"remark":"","updateTime":1522308590000,"managerId":4,"priority":6,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":150,"harvestIntegral":10,"storeSaleIntegral":10},{"id":10,"cropName":"小辣椒123","plantingDiamond":3,"harvestDiamond":3,"storeSale":10,"details":"美颜","startTime":1516070787000,"cycle":1,"remark":"","updateTime":1522308620000,"managerId":1,"priority":8,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":30,"harvestIntegral":30,"storeSaleIntegral":100},{"id":5,"cropName":"木瓜","plantingDiamond":2,"harvestDiamond":2,"storeSale":1,"details":"秒到","startTime":1516070594000,"cycle":-1,"remark":"","updateTime":1522308610000,"managerId":1,"priority":9,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":20,"harvestIntegral":20,"storeSaleIntegral":10},{"id":24,"cropName":"123","plantingDiamond":123,"harvestDiamond":123,"storeSale":123,"details":"123","startTime":1520991505000,"cycle":123,"remark":"123","updateTime":1522057352000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":1230,"harvestIntegral":1230,"storeSaleIntegral":1230},{"id":14,"cropName":"朝天椒","plantingDiamond":58,"harvestDiamond":5,"storeSale":5,"details":"够辣，够带劲","startTime":1516870181000,"cycle":20,"remark":"","updateTime":1522057312000,"managerId":14,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":580,"harvestIntegral":50,"storeSaleIntegral":50},{"id":7,"cropName":"大白菜","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"鲜嫩的大白菜","startTime":1516066931000,"cycle":20,"remark":"","updateTime":1522116670000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":18,"cropName":"红薯","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"胖胖的红薯，香甜可口","startTime":1516931604000,"cycle":30,"remark":"","updateTime":1522057392000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":17,"cropName":"豇豆","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"美颜","startTime":1516871473000,"cycle":20,"remark":"","updateTime":1522308622000,"managerId":12,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":9,"cropName":"辣椒","plantingDiamond":3,"harvestDiamond":3,"storeSale":10,"details":"美颜","startTime":1516067206000,"cycle":-1,"remark":"","updateTime":1522308617000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":30,"harvestIntegral":30,"storeSaleIntegral":100},{"id":4,"cropName":"龙葵","plantingDiamond":1,"harvestDiamond":1,"storeSale":1,"details":"迷倒万千少女","startTime":1516069438000,"cycle":-1,"remark":"","updateTime":1522308605000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":10,"harvestIntegral":10,"storeSaleIntegral":10},{"id":13,"cropName":"茄子","plantingDiamond":5,"harvestDiamond":5,"storeSale":5,"details":"长茄子，红烧茄子的最佳选择","startTime":1516869866000,"cycle":30,"remark":"","updateTime":1522057376000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":50,"harvestIntegral":50,"storeSaleIntegral":50},{"id":16,"cropName":"四季豆","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"长长的藤蔓上挂着长长的豆","startTime":1516871281000,"cycle":20,"remark":"","updateTime":1522057378000,"managerId":12,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":11,"cropName":"土豆","plantingDiamond":1,"harvestDiamond":1,"storeSale":2,"details":"美颜","startTime":1516070711000,"cycle":-1,"remark":"","updateTime":1522308620000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":10,"harvestIntegral":10,"storeSaleIntegral":20},{"id":12,"cropName":"仙女果","plantingDiamond":10,"harvestDiamond":10,"storeSale":10,"details":"晶莹剔透的仙女果，甘甜可口","startTime":1516869163000,"cycle":30,"remark":"周期较长","updateTime":1522057375000,"managerId":12,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":100,"harvestIntegral":100,"storeSaleIntegral":100},{"id":19,"cropName":"小白菜","plantingDiamond":5,"harvestDiamond":5,"storeSale":5,"details":"美颜","startTime":1516936477000,"cycle":20,"remark":"","updateTime":1522308623000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":50,"harvestIntegral":50,"storeSaleIntegral":50},{"id":25,"cropName":"12311","plantingDiamond":123,"harvestDiamond":123,"storeSale":123,"details":"123","startTime":1520991512000,"cycle":123,"remark":"123","updateTime":1522057350000,"managerId":1,"priority":123,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1,"plantingIntegral":1230,"harvestIntegral":1230,"storeSaleIntegral":1230}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 3
             * cropName : 花椒
             * plantingDiamond : 1
             * harvestDiamond : 1
             * storeSale : 1
             * details : 美颜
             * startTime : 1516066847000
             * cycle : 1
             * remark :
             * updateTime : 1522381941000
             * managerId : 1
             * priority : 4
             * cropImg : https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg
             * state : 1
             * plantingIntegral : 10
             * harvestIntegral : 10
             * storeSaleIntegral : 10
             */

            private int id;
            private String cropName;
            private Double plantingDiamond;
            private Double harvestDiamond;
            private Double storeSale;
            private String details;
            private long startTime;
            private int cycle;
            private String remark;
            private long updateTime;
            private int managerId;
            private int priority;
            private String cropImg;
            private int state;
            private Double plantingIntegral;
            private Double harvestIntegral;
            private Double storeSaleIntegral;

            private String otherNames;
            private String nutritionalComponents;
            private long startSowing;
            private long endSeeding;
            private int pskstate;
            private int vystate;
            private int isExternal;


            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCropName() {
                return cropName;
            }

            public void setCropName(String cropName) {
                this.cropName = cropName;
            }

            public Double getPlantingDiamond() {
                return plantingDiamond;
            }

            public void setPlantingDiamond(Double plantingDiamond) {
                this.plantingDiamond = plantingDiamond;
            }

            public Double getHarvestDiamond() {
                return harvestDiamond;
            }

            public void setHarvestDiamond(Double harvestDiamond) {
                this.harvestDiamond = harvestDiamond;
            }

            public Double getStoreSale() {
                return storeSale;
            }

            public void setStoreSale(Double storeSale) {
                this.storeSale = storeSale;
            }

            public String getDetails() {
                return details;
            }

            public void setDetails(String details) {
                this.details = details;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public int getCycle() {
                return cycle;
            }

            public void setCycle(int cycle) {
                this.cycle = cycle;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public long getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(long updateTime) {
                this.updateTime = updateTime;
            }

            public int getManagerId() {
                return managerId;
            }

            public void setManagerId(int managerId) {
                this.managerId = managerId;
            }

            public int getPriority() {
                return priority;
            }

            public void setPriority(int priority) {
                this.priority = priority;
            }

            public String getCropImg() {
                return cropImg;
            }

            public void setCropImg(String cropImg) {
                this.cropImg = cropImg;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public Double getPlantingIntegral() {
                return plantingIntegral;
            }

            public void setPlantingIntegral(Double plantingIntegral) {
                this.plantingIntegral = plantingIntegral;
            }

            public Double getHarvestIntegral() {
                return harvestIntegral;
            }

            public void setHarvestIntegral(Double harvestIntegral) {
                this.harvestIntegral = harvestIntegral;
            }

            public Double getStoreSaleIntegral() {
                return storeSaleIntegral;
            }

            public void setStoreSaleIntegral(Double storeSaleIntegral) {
                this.storeSaleIntegral = storeSaleIntegral;
            }

            public String getOtherNames() {
                return otherNames;
            }

            public void setOtherNames(String otherNames) {
                this.otherNames = otherNames;
            }

            public String getNutritionalComponents() {
                return nutritionalComponents;
            }

            public void setNutritionalComponents(String nutritionalComponents) {
                this.nutritionalComponents = nutritionalComponents;
            }

            public long getStartSowing() {
                return startSowing;
            }

            public void setStartSowing(long startSowing) {
                this.startSowing = startSowing;
            }

            public long getEndSeeding() {
                return endSeeding;
            }

            public void setEndSeeding(long endSeeding) {
                this.endSeeding = endSeeding;
            }

            public int getPskstate() {
                return pskstate;
            }

            public void setPskstate(int pskstate) {
                this.pskstate = pskstate;
            }

            public int getVystate() {
                return vystate;
            }

            public void setVystate(int vystate) {
                this.vystate = vystate;
            }

            public int getIsExternal() {
                return isExternal;
            }

            public void setIsExternal(int isExternal) {
                this.isExternal = isExternal;
            }
        }
    }
}
