package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;

public class ListDialogsAdapter extends BaseAdapter {

    private List<String> namelist;
    private List<String> integral_diamondList;
    private Context mContext;

    public ListDialogsAdapter(Context context, List<String> list1,List<String> list2) {
        this.mContext = context;
        namelist = new ArrayList<String>();
        integral_diamondList = new ArrayList<String>();
        this.namelist = list1;
        this.integral_diamondList = list2;
    }

    @Override
    public int getCount() {
        return integral_diamondList.size();
    }

    @Override
    public Object getItem(int position) {
        return integral_diamondList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListDialogsAdapter.Person person = null;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_dialog_items,null);
            person = new ListDialogsAdapter.Person();
            person.name = convertView.findViewById(R.id.tv_name);
            person.integral_diamond = convertView.findViewById(R.id.integral_diamond);
            convertView.setTag(person);
        }else{
            person = (ListDialogsAdapter.Person)convertView.getTag();
        }
        person.name.setText(namelist.get(position).toString());
        person.integral_diamond.setText(integral_diamondList.get(position).toString());
        return convertView;
    }
    class Person{
        TextView name;
        TextView integral_diamond;
    }

}
