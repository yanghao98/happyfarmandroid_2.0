package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.MyRecipeAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.TutorialAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.Cooking;
import zhixin.cn.com.happyfarm_user.model.MyRecipe;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.TextString;

public class MyRecipeActivity extends AppCompatActivity {

    private RefreshLayout refreshLayout;
    private RelativeLayout noMyRecipe;
    private ListView listView;
    private MyRecipeAdapter adapter;
    private List<Cooking.ResultBean.CookingList> myRecipeData = new ArrayList<>();
    private Dialog dialog;
    private ImageView addCooking;
    private String tokenStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_recipe_activity);
        initView();

        searchCookingList();
    }

    private void initView() {
        refreshLayout = findViewById(R.id.refreshLayout);
        noMyRecipe = findViewById(R.id.no_my_recipe);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(MyRecipeActivity.this).setShowBezierWave(false));
        // 设置 Footer 为 球脉冲
        refreshLayout.setRefreshFooter(new ClassicsFooter(MyRecipeActivity.this).setSpinnerStyle(SpinnerStyle.Scale));
        if (!myRecipeData.isEmpty()){
            myRecipeData.clear();
        }
        addCooking = findViewById(R.id.add_cooking);
        addCooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getSharedPreferences("User_data",MODE_PRIVATE);
                tokenStr = pref.getString("token","");
                if ("".equals(tokenStr)) {
                    noLoginDialog();
                } else {
                    Intent intent = new Intent(MyRecipeActivity.this, AddMakeCookingActivity.class);
                    startActivity(intent);
                }
            }
        });
        listView = findViewById(R.id.my_recipe_list);
    }

    private void initRecyclerView() {
        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new MyRecipeAdapter(MyRecipeActivity.this, myRecipeData);
        //设置适配器
        listView.setAdapter(adapter);
        //ListView item的点击事件
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i("我点击的烹饪方法的信息", "onItemClick: " + JSON.toJSONString(myRecipeData.get(i)));

                Intent intent = new Intent(MyRecipeActivity.this, CookingRecommendedActivity.class);
                intent.putExtra("cookingId",myRecipeData.get(i).getId());
                startActivity(intent);
            }
        });
        LoadingDialog.closeDialog(dialog);
    }

    /**
     * 根据用户登录信息 查询 烹饪列表
     */
    private void searchCookingList(){
        dialog = LoadingDialog.createLoadingDialog(MyRecipeActivity.this, "正在加载数据");
        HttpHelper.initHttpHelper().searchCookingMethodsByUserId().enqueue(new Callback<Cooking>() {
            @Override
            public void onResponse(Call<Cooking> call, Response<Cooking> response) {
                System.out.println("searchCookingList:"+ JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    myRecipeData = response.body().getResult().getList();
                    if (myRecipeData.size() > 0) {
                        noMyRecipe.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                    } else {
                        noMyRecipe.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                    }
                    initRecyclerView();
                } else {
                    noMyRecipe.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                }

            }
            @Override
            public void onFailure(Call<Cooking> call, Throwable t) {
                noMyRecipe.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }


    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!myRecipeData.isEmpty()){
                    myRecipeData.clear();
                }
                refreshlayout.setNoMoreData(false);//TODO 恢复没有更多数据的原始状态
                searchCookingList();
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }
//
//    //上拉加载
//    public void bottomRefreshLayout() {
//        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
//                refreshLayout.getLayout().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
////                        if (isLastPage) {
////                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
////                            refreshLayout.finishLoadMore();
////                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
////                        } else {
//////                            Toast.makeText(getApplication(), "数据加载完毕", Toast.LENGTH_SHORT).show();
////                            pageNum++;
////                            StaggerLoadData(false, pageNum);
//////                            adapter.loadMore(initData());
////                            refreshLayout.finishLoadMore();
////                        }
//                    }
//                }, 0);
//            }
//        });
//    }
    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(MyRecipeActivity.this);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(MyRecipeActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    public void back(View view) {
        finish();
    }
}
