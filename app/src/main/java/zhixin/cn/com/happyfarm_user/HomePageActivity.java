package zhixin.cn.com.happyfarm_user;

import android.os.Bundle;
import android.view.View;

import zhixin.cn.com.happyfarm_user.base.ABaseActivity;

public class HomePageActivity extends ABaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
    }

    @Override
    public void back(View view) {

    }

}
