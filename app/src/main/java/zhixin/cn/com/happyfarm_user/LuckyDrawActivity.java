package zhixin.cn.com.happyfarm_user;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.GuideView;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

public class LuckyDrawActivity extends AppCompatActivity {

    private String TAG = "ActivityPage";
    private WebView webView;
    private WebSettings webSettings;
//    private String url ="http://192.168.50.140:8080/androidAPPLotteryPage";
    private String url ="https://www.trustwusee.com/androidAPPLotteryPage";
    private ProgressBar progressBar;//加载进度条
    private RefreshLayout refreshLayout;//下拉刷新
    private GuideView personalCenterPageGuideView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lucky_draw_activity);
//        findViewById(R.id.my_prompt1).setVisibility(View.VISIBLE);
//        findViewById(R.id.lottery_content).setVisibility(View.GONE);
        progressBar = findViewById(R.id.lottery_progressBar2);
        refreshLayout = findViewById(R.id.lotteryLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setDisableContentWhenLoading(false);//设置是否开启在加载时候禁止操作内容视图
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(LuckyDrawActivity.this).setShowBezierWave(false));
        webView = findViewById(R.id.myWeb);
        initView();
        topRefreshLayout();
    }

    private void initView() {
        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        TextView title = findViewById(R.id.title_name);
        title.setText("抽奖活动");
        /**
         * 引导用户去往奖品说明页面
         */
        if (true == Config.isNoGoLuckDrawExplain) {
            Config.isNoGoLuckDrawExplain = false;
            goLuckDrawExplain();
        }
    }
    private void goLuckDrawExplain() {
        final AlertUtilBest diyDialog = new AlertUtilBest(LuckyDrawActivity.this);
        diyDialog.setCancel("取消")
                .setOk(TextString.goLuckDrawExplainOkButton)
                .setContent(TextString.goLuckDrawExplainContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(LuckyDrawActivity.this, TermsActivity.class);
                        intent.putExtra("termsName", "奖品说明");
                        intent.putExtra("url", "https://www.trustwusee.com/luckDrawExplain");
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }


    private class JsInterface {
        private Context mContext;

        public JsInterface(Context context) {
            this.mContext = context;
        }

        //在js中调用window.AndroidWebView.showInfoFromJs(name)，便会触发此方法。
        @JavascriptInterface
        public void showInfoFromJs(String name) {
            System.out.println(name);
            if ("goSelectBranchActivity".equals(name)) {
                Intent intent = new Intent(LuckyDrawActivity.this, SelectBranchActivity.class);
                startActivity(intent);
            } else if ("goInvitationCodeActivity".equals(name)) {
                Intent intent = new Intent(LuckyDrawActivity.this, InvitationCodeActivity.class);
                intent.putExtra("invitation_code_activity", Config.invitationCode);
                startActivity(intent);
            }else {
                Intent intent = new Intent(LuckyDrawActivity.this, TermsActivity.class);
                intent.putExtra("termsName","奖品说明");
                intent.putExtra("url","https://www.trustwusee.com/luckDrawExplain");
                startActivity(intent);
            }
//            Toast.makeText(mContext, name, Toast.LENGTH_SHORT).show();
        }
    }

    //在java中调用js代码 (但是沒有試驗成功)
//    public void sendInfoToJs() {
//        //调用js中的函数：showInfoFromJava(msg)
//        webView.loadUrl("javascript:showInfoFromJava('" + "我从android来" + "')");
//        Toast.makeText(getContext(), "我发了一条消息给js", Toast.LENGTH_SHORT).show();
//    }

    private String getToken(){
        return App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("token","");
    }

    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        }
    }

    private class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            // TODO Auto-generated method stub
            if (newProgress == 100) {
                // 网页加载完成
                progressBar.setVisibility(View.GONE);//加载完网页进度条消失
            } else {
                // 加载中
                progressBar.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                progressBar.setProgress(newProgress);//设置进度值
            }
        }
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                webView.reload();

                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }

    /**
     * 验证token是否有效
     */
    public  void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
//                System.out.println("###"+ JSON.toJSONString(response.body()));
                if (("failed").equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(LuckyDrawActivity.this, "User_data", strArr);
                    findViewById(R.id.my_prompt1).setVisibility(View.VISIBLE);
                    findViewById(R.id.lottery_content).setVisibility(View.GONE);
                    noLoginDialog();
                } else if (("success").equals(response.body().getFlag())){
                    findViewById(R.id.my_prompt1).setVisibility(View.GONE);
                    findViewById(R.id.lottery_content).setVisibility(View.VISIBLE);
                    //token有效在加载web
                    webView.setWebChromeClient(new MyWebChromeClient());//判断页面加载过程
                    webView.addJavascriptInterface(new JsInterface(LuckyDrawActivity.this), "AndroidWebView"); //在js中调用本地java方法
                    webView.loadUrl(url);
                    /**
                     * 消息回调发送token
                     */
                    webView.setWebViewClient(new MyWebViewClient() {
                        @Override
                        public void onLoadResource(WebView view, String url) {
                            // TODO Auto-generated method stub
                            super.onLoadResource(view, url);
                            webView.loadUrl("javascript:showInfoFromJava('" + getToken() + " ')");
                        }
                    });
//                    searchSignRecord();
//                    selectUserInfo();
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(LuckyDrawActivity.this, TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }
    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog = new AlertUtilBest(LuckyDrawActivity.this);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(LuckyDrawActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }



    @Override
    public void onStart() {
        super.onStart();
        userInfoTokenTest();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void back(View view) {
        finish();
    }

}
