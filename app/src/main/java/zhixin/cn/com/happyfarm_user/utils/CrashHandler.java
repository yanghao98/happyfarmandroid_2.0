package zhixin.cn.com.happyfarm_user.utils;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;

import com.alibaba.fastjson.JSON;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Thread.UncaughtExceptionHandler;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.db.RetrofitUtil;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.other.DateUtil;

import static android.content.Context.MODE_PRIVATE;

/**
 * Create by ChenHao on 2018/6/29 9:30
 * use : 应用异常处理类
 * 使用方式： 在Application 中初始化  CrashHandler.getInstance().init(this);
 */
public class CrashHandler implements UncaughtExceptionHandler {
    private static final String TAG = "CrashHandler";
    public static final boolean DEBUG = true;
    /**
     * 文件名
     */
    public static final String FILE_NAME = "Android_";
    /**
     * 异常日志 存储位置为根目录下的 Crash文件夹
     */
    private static String PATH = null;
    /**
     * 文件名后缀
     */
    private static final String FILE_NAME_SUFFIX = ".txt";

    private static CrashHandler sInstance = new CrashHandler();
    private UncaughtExceptionHandler mDefaultCrashHandler;
    private Context mContext;
    private File logFile;

    private CrashHandler() {

    }

    public static CrashHandler getInstance() {
        return sInstance;
    }

    /**
     * 初始化
     *
     * @param context
     */
    public void init(Context context) {
        //得到系统的应用异常处理器
        mDefaultCrashHandler = Thread.getDefaultUncaughtExceptionHandler();
        //将当前应用异常处理器改为默认的
        Thread.setDefaultUncaughtExceptionHandler(this);
        mContext = context.getApplicationContext();
        PATH = Environment.getExternalStorageDirectory().getPath() + "/Android/data/" + mContext.getPackageName() + "/Crash/log/";
        getFilesAllName(PATH,2);
    }


    /**
     * 这个是最关键的函数，当系统中有未被捕获的异常，系统将会自动调用 uncaughtException 方法
     *
     * @param thread 为出现未捕获异常的线程
     * @param ex     为未捕获的异常 ，可以通过e 拿到异常信息
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

        //导入异常信息到SD卡中
        try {
            dumpExceptionToSDCard(ex);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //这里可以上传异常信息到服务器，便于开发人员分析日志从而解决Bug
        if (logFile != null)
            uploadExceptionToServer();
        SystemClock.sleep(1000);
        ex.printStackTrace();
        //如果系统提供了默认的异常处理器，则交给系统去结束程序，否则就由自己结束自己
        if (mDefaultCrashHandler != null) {
            mDefaultCrashHandler.uncaughtException(thread, ex);
        } else {
            Process.killProcess(Process.myPid());
            System.exit(1);
        }

    }

    /**
     * 将异常信息写入SD卡
     *
     * @param e
     */
    private void dumpExceptionToSDCard(Throwable e) throws IOException{
        //如果SD卡不存在或无法使用，则无法将异常信息写入SD卡
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            if (DEBUG) {
                Log.w(TAG, "sdcard unmounted,skip dump exception");
                return;
            }
        }
        File dir = new File(PATH);
        //如果目录下没有文件夹，就创建文件夹
        if (!dir.exists()) {
            dir.mkdirs();
        }
        //得到当前年月日时分秒
        long current = System.currentTimeMillis();
        String time = DateUtil.dateYMD_HMS(current);
        String logStart = "************* Crash Log Head ****************";
        //在定义的Crash文件夹下创建文件
        logFile = new File(PATH + FILE_NAME + time + FILE_NAME_SUFFIX);
        try{
            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(logFile)));
            pw.println("");
            //开始写入
            pw.println(logStart);
            //写入时间
            pw.print("Creation time      : ");
            pw.println(time);
            //写入手机信息
            dumpPhoneInfo(pw);
            pw.println();//换行
            e.printStackTrace(pw);
            pw.close();//关闭输入流
        } catch (Exception e1) {
            Log.e(TAG,"dump crash info failed");
        }

    }

    /**
     * 获取手机各项信息
     * @param pw
     */
    private void dumpPhoneInfo(PrintWriter pw) throws PackageManager.NameNotFoundException {
        //得到包管理器
        PackageManager pm = mContext.getPackageManager();
        //得到包对象
        PackageInfo pi = pm.getPackageInfo(mContext.getPackageName(),PackageManager.GET_ACTIVITIES);
        //写入APP版本号
        pw.print("App Version        : ");
        pw.print(pi.versionName);
        pw.print("_");
        pw.println(pi.versionCode);
        //写入 Android 版本号
        pw.print("Android Version    : ");
        pw.println(Build.VERSION.RELEASE);
        pw.print("Android SDK        : ");
        pw.println(Build.VERSION.SDK_INT);
        //手机制造商
        pw.print("Device Manufacturer: ");
        pw.println(Build.MANUFACTURER);
        //手机型号
        pw.print("Device Model       : ");
        pw.println(Build.MODEL);
        //CPU架构
        pw.print("CPU ABI            : ");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            pw.println(JSON.toJSONString(Build.SUPPORTED_ABIS));
        }else {
            pw.println(JSON.toJSONString(Build.CPU_ABI));
        }
        //结束写入
        pw.print("************* Crash Log Head ****************");
        pw.println("\n");
    }

    /**
     * 将错误信息上传至服务器
     */
    private void uploadExceptionToServer() {
        MultipartBody.Part body = RetrofitUtil.fileToMultipartBodyPart(logFile);
        HttpHelper.initHttpHelper().androidLogUpload(getTel(),body).enqueue(new retrofit2.Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                Log.i("信息上传","信息单文件返回数据"+response.body());
                if("success".equals(response.body().getFlag().toString())){
                    Log.i("信息上传",response.body().getResult());
                }else {
                    Log.i("信息上传",response.body().getResult());
                }
            }

            @Override
            public void onFailure(retrofit2.Call<CheckSMS> call, Throwable t) {
                Log.i("信息上传失败","信息单文件上传失败返回数据"+String.valueOf(t));
            }
        });
    }

    /**
     * 获取文件夹下的所有子文件名称 与 文件删除
     * @param path 文件路径
     * @param autoClearDay 文件保存天数
     */
    private void getFilesAllName(String path,final int autoClearDay) {
        new Thread(){
            @Override
            public void run() {
                super.run();
                //进行自己的操作
                File file = new File(path);
                File[] files = file.listFiles();
                if (files == null){
                    Log.e("error","空目录");
                    return;
                }
                //系统时间
                long currentTime = System.currentTimeMillis();
                Log.i("file_info", String.valueOf(currentTime));
                //文件创建时间
                long fileTime;
                for (File file1 : files) {
                    Log.e("file_info",
                            file1.getAbsolutePath() + "\n"
                                    + file1.lastModified() + "\n");
                    fileTime = file1.lastModified();
                    //相差时间
                    int day = (int) ((currentTime - fileTime)) / (1000 * 60 * 60 * 24);
                    if (day > autoClearDay) {
                        try {
                            // 找到文件所在的路径并删除该文件
                            file1.delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }.start();
    }
    //TODO 获得用户上次输入账号
    private String getTel(){
        String tel = App.CONTEXT.getSharedPreferences("data",MODE_PRIVATE).getString("tel","");
        Log.i(TAG, "getTel: "+tel);
        return tel;
    }
}

