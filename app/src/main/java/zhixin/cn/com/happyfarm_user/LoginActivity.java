package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import org.apaches.commons.codec.binary.Base64;

import java.security.PublicKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.LoginList;
import zhixin.cn.com.happyfarm_user.model.PublicKeyList;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.RSAUtil;
import zhixin.cn.com.happyfarm_user.other.TextChangedListener;
import zhixin.cn.com.happyfarm_user.other.Validator;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.GuideView;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by DELL on 2018/2/1.
 */

public class LoginActivity extends ABaseActivity {

    private String tel;
    private String pwd;
    private String pwds;
    private String publicKey1;

    private byte[] pwd2;
    private Dialog mDialog;
    private EditText username;
    private String landVideo;
    private String isAllGoodPage;
    private RelativeLayout titleBarLayout;
    private EditText password;
    private Button loginBtn;
    private Button register;
    private Button forgetPwd;
    private GuideView loginActivityGuideView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTransparent();
        setContentView(R.layout.login_layout);

        initView();
        loginActivity();//引导蒙层
        setStatusBarHeight();

        Intent intent = getIntent();
        String phone = intent.getStringExtra("tel");
        landVideo = intent.getStringExtra("isLandVideo");
        isAllGoodPage = intent.getStringExtra("isAllGoodPage");
        if (phone != null) {
            username.setText(phone);
        }
        //TODO　实现上次输入账号
        if (!("".equals(getTel()))) {
            username.setText(getTel());
        }

        //跳转忘记密码
        forgetPwd.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {

                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        //点击登录
        loginBtn.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {
                if (TextUtils.isEmpty(username.getText().toString())) {
                    showToastShort(LoginActivity.this, getResources().getString(R.string.username_null));
                } else if (!Validator.isChinaPhoneLegal(username.getText().toString())) {
                    showToastShort(LoginActivity.this, getResources().getString(R.string.phone_error));
                } else if (TextUtils.isEmpty(password.getText().toString())) {
                    showToastShort(LoginActivity.this, getResources().getString(R.string.password_null));
                }  else {
                    tel = username.getText().toString();
                    pwd = password.getText().toString();
                    pwds = password.getText().toString();
                    pwd2 = pwd.getBytes();
                    String loginIp = NetWorkUtils.getIPAddress(getApplicationContext());
                    if ("-1".equals(loginIp)) {
                        showToastShort(LoginActivity.this, getResources().getString(R.string.no_network));
                        //Toast.makeText(LoginActivity.this, "请查看网络是否链接",Toast.LENGTH_SHORT).show();
                    } else {
                        getPublicKey(loginIp);
                    }

                }

            }
        });

        //跳转注册
        register.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {
                interceptDialog();
            }
        });

        //点击空白处退出键盘
        findViewById(R.id.backView).setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                hideKeyboard();
            }
        });
        TextChangedListener textChangedListener = new TextChangedListener(11,username,LoginActivity.this,TextString.PhoneLong);
        username.addTextChangedListener(textChangedListener.getmTextWatcher());
        TextChangedListener textChangedListener2 = new TextChangedListener(16,password,LoginActivity.this,getResources().getString(R.string.change_pwd_length));
        password.addTextChangedListener(textChangedListener2.getmTextWatcher());
    }

    private void initView() {
        TextView title = findViewById(R.id.title_name);
        title.setText(getResources().getString(R.string.login_title));
        username = findViewById(R.id.user_name);
        Drawable drawable = getResources().getDrawable(R.mipmap.login_username);
        drawable .setBounds(0, 0, 80, 80);//第一个 0 是距左边距离，第二个 0 是距上边距离，40 分别是长宽
        username .setCompoundDrawables(drawable , null, null, null);//只放左边
        password = findViewById(R.id.pwd_name);
        Drawable drawable1 = getResources().getDrawable(R.mipmap.login_password);
        drawable1 .setBounds(0, 0, 80, 80);//第一个 0 是距左边距离，第二个 0 是距上边距离，40 分别是长宽
        password .setCompoundDrawables(drawable1 , null, null, null);//只放左边
        loginBtn = findViewById(R.id.login_btn);
        register = findViewById(R.id.register);
        forgetPwd = findViewById(R.id.forget_pwd);
        titleBarLayout = findViewById(R.id.title_bar_layout);
    }

    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(this);
        //相对布局使用方法
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        titleBarLayout.setLayoutParams(params);
    }

    /**
     * 租地拦截弹框
     * 由于业务限制，需要租地时告诉用户目前只对南京开放
     */
    private void interceptDialog(){
        final AlertUtilBest diyDialog = new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk(getResources().getString(R.string.intercept_ok))
                .setContent(getResources().getString(R.string.intercept_register_content))
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        //跳转注册
                        Intent intent = new Intent(LoginActivity.this, MobileRegistrationActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    private void getPublicKey(String loginIp) {
        mDialog = LoadingDialog.createLoadingDialog(LoginActivity.this, "正在登录");
        Call<PublicKeyList> publicKey = HttpHelper.initHttpHelper().publicKey(tel);
        publicKey.enqueue(new Callback<PublicKeyList>() {
            @Override
            public void onResponse(Call<PublicKeyList> call, Response<PublicKeyList> response) {
//                Log.e("11111", JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    publicKey1 = response.body().getResult();
                    try {
                        PublicKey publickey = RSAUtil.getPublicKey(publicKey1);
                        pwd2 = RSAUtil.encrypt(pwd2, publickey);
                        pwd = Base64.encodeBase64String(pwd2);
                        userLogin(tel, pwd, loginIp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    LoadingDialog.closeDialog(mDialog);
                    showToastShort(LoginActivity.this, response.body().getResult());
//                                    Snackbar.make(view, "登录失败", Snackbar.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<PublicKeyList> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                showToastShort(LoginActivity.this, getResources().getString(R.string.login_failed));
//                                Snackbar.make(view, "登录失败", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void userLogin(String tel, String pwd, String loginIp) {
        //登录接口实现
        Call<LoginList> login = HttpHelper.initHttpHelper().login(tel, pwd, loginIp);
        login.enqueue(new Callback<LoginList>() {
            @Override
            public void onResponse(Call<LoginList> call, Response<LoginList> response) {
//                Log.i("info", JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    LoadingDialog.closeDialog(mDialog);
//                    Log.d("login_up", "onResponse: " + response.body().getResult().getUser().getIsVoice());
                    //记录直播是否开启音频状态
                    Config.AUDIO_STATE = response.body().getResult().getUser().getIsVoice() == 1;
                    showToastShort(LoginActivity.this, getResources().getString(R.string.login_success));
                    Config.isLogin = true;
                    saveLogin(tel, pwds);
                    Log.e("token", response.body().getResult().getToken());
                    String headImg = response.body().getResult().getUser().getHeadImg();
                    String uuid = response.body().getResult().getUser().getUuid();
                    String nickName = response.body().getResult().getUser().getNickname();
                    saveToken(response.body().getResult().getToken(), String.valueOf(response.body().getResult().getUser().getId()), headImg, uuid,nickName);
                    if ("isLandVideo".equals(landVideo)) {
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        Log.d("LoginActivity->","userLogin"+"从租地界面进");
                    } else if ("isAllGoodPage".equals(isAllGoodPage)) {//判断是否是从菜场进入
                        Log.d("LoginActivity->","userLogin"+"判断从菜场进入");
                        Config.allGoodPageRefresh = true;
                    }
                    //设置推送token
                    Config.settingPushToken();
//                  MiPushClient.resumePush(LoginActivity.this,null);//恢复小米推送
                    finish();
                }

            }

            @Override
            public void onFailure(Call<LoginList> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                try {
                    if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                        showToastShort(LoginActivity.this, getResources().getString(R.string.username_or_pwd_error));
                    } else {
                        showToastShort(LoginActivity.this, TextString.NetworkRequestFailed);
                    }
                }catch (Exception e){
//                    Log.e("LoginActivity->", "onFailure: "+ e);
                }
                //Toast.makeText(LoginActivity.this, "账号或密码错误", Toast.LENGTH_SHORT).show();
//                                                Snackbar.make(view, "登录失败", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    /**
     * 使用SharedPreferences保存用户名和密码
     *
     * @param tel
     * @param pwd
     */
    public void saveLogin(String tel, String pwd) {
//        //获取SharedPreferences对象
//        SharedPreferences.Editor editor = getSharedPreferences("data", MODE_PRIVATE).edit();
//        //设置参数
//        editor.putString("tel", tel);
//        editor.putString("pwd", pwd);
//        //提交
//        editor.apply();
        String[] keyArr = {"tel", "pwd"};
        String[] valueArr = {tel, pwd};
        SharedPreferencesUtils.setParamStrings(this, "data", keyArr, valueArr);
    }

    /**
     * 使用SharedPreferences保存token userId headImg uuid
     *
     * @param token
     * @param userId
     * @param headImg
     * @param uuid
     */
    public void saveToken(String token, String userId, String headImg, String uuid,String nickName) {
//        SharedPreferences.Editor editor = getSharedPreferences("User_data", MODE_PRIVATE).edit();
//        editor.putString("token", token);
//        editor.putString("userId", userId);
//        editor.putString("userImg", headImg);
//        editor.putString("uuid", uuid);
//        editor.apply();
        String[] keyArr = {"token", "userId", "userImg", "uuid","nickName"};
        String[] valueArr = {token, userId, headImg, uuid,nickName};
        SharedPreferencesUtils.setParamStrings(this, "User_data", keyArr, valueArr);
    }

    public void back(View view) {
        hideKeyboard();
        finish();
    }

    //TODO 获得用户上次输入账号
    private String getTel() {
        return SharedPreferencesUtils.getParamString(this, "data", "tel", "");
    }

    /**
     * 引导蒙层
     */
    public void loginActivity() {
        //判断是否第一次登陆
        if (("isFirst").equals(getGuide()) ) {//不是第一次加载初始页面
            System.out.println("不是第一次登陆");
        } else {
            TextView tv = new TextView(this);
            tv.setText(R.string.tv7);
            tv.setTextColor(getResources().getColor(R.color.white));
            tv.setTextSize(20);
            tv.setGravity(Gravity.CENTER);

            // 使用图片
            final ImageView iv = new ImageView(this);
            iv.setImageResource(R.mipmap.wozhidaole);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            iv.setLayoutParams(params);

            loginActivityGuideView = GuideView.Builder
                    .newInstance(this)
                    .setTargetView(findViewById(R.id.register))
                    .setCustomGuideView(tv)
                    .setTextGuideView(iv)
                    .setDirction(GuideView.Direction.BOTTOM)
                    .setShape(GuideView.MyShape.ELLIPSE)   // 设置矩形显示区域，
                    .setRadius(10)          // 设置圆形或矩形透明区域半径，默认是targetView的显示矩形的半径，如果是矩形，这里是设置矩形圆角大小
                    .setBgColor(getResources().getColor(R.color.shadow))
                    .setOnclickListener(new GuideView.OnClickCallback() {
                        @Override
                        public void onClickedGuideView() {
                            loginActivityGuideView.hide();
                            saveGuide("isFirst");
                        }
                    })
                    .build();
            loginActivityGuideView.show();
        }
    }

    /**
     * 使用SharedPreferences保存导航页第几次
     */
    public void saveGuide(String isFirst){
        //获取SharedPreferences对象
        SharedPreferences.Editor editor = getSharedPreferences("LoginActivity",MODE_PRIVATE).edit();
        //设置参数
        editor.putString("isFirst", isFirst);
        //提交
        editor.apply();
    }
    /**
     * 是否是第一次安装
     * @return
     */
    private String getGuide(){
        return getSharedPreferences("LoginActivity",MODE_PRIVATE).getString("isFirst","");
    }
}
