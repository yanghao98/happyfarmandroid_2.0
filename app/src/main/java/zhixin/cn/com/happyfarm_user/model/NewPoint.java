package zhixin.cn.com.happyfarm_user.model;


public class NewPoint {

    private int xC;   //x坐标
    private int yC;   //y坐标
    private char type;
    private AllLand.ResultBean.ListBean land; //地的信息

    public NewPoint(char type, AllLand.ResultBean.ListBean p) {
        this.type = type;
        land = p;
    }

    public NewPoint() {
        super();
    }

    public int getxC() {
        return xC;
    }

    public void setxC(int xC) {
        this.xC = xC;
    }

    public int getyC() {
        return yC;
    }

    public void setyC(int yC) {
        this.yC = yC;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public AllLand.ResultBean.ListBean getLand() {
        return land;
    }
}
