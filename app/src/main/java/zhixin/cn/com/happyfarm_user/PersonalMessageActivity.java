package zhixin.cn.com.happyfarm_user;

import android.os.Bundle;
import android.view.View;


import zhixin.cn.com.happyfarm_user.Page.PersonalMessagePage;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;

/**
 * 由于原消息页面是一个fragment页面 所以需要一个activity来装载，所以就创建了此方法
 */
public class PersonalMessageActivity extends ABaseActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_message_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.personal_content,new PersonalMessagePage())
                .commit();
    }

        @Override
    public void back(View view) {

    }

}
