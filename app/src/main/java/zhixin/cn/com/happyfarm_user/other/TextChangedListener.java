package zhixin.cn.com.happyfarm_user.other;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import zhixin.cn.com.happyfarm_user.utils.CustomToast;

/**
 * TextChangedListener
 * 文本长度监听类
 * @author: Administrator.
 * @date: 2019/1/23
 */
public class TextChangedListener {

    private int le;
    private EditText editText;
    private Context context;
    private String message;
    private MTextWatcher mTextWatcher;

    public TextChangedListener(int le, EditText editText, Context context,String message){
        this.le = le;
        this.editText = editText;
        this.context = context;
        this.message = message;
        mTextWatcher = new MTextWatcher();
    }

    public synchronized TextWatcher getmTextWatcher(){
        return mTextWatcher;
    }
    class MTextWatcher implements TextWatcher {

        private CharSequence temp;
        private int editStart;
        private int editEnd;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // TODO Auto-generated method stub
            temp = s;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO Auto-generated method stub
//   mTextView.setText(s);//将输入的内容实时显示
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
            editStart = editText.getSelectionStart();
            editEnd = editText.getSelectionEnd();
            //mTextView.setText("您输入了" + temp.length() + "个字符");
            if (temp.length() > le) {
                CustomToast.showToast(context,message);
                s.delete(editStart - 1, editEnd);
                int tempSelection = editStart;
                editText.setText(s);
                editText.setSelection(tempSelection);
            }
        }
    }
}
