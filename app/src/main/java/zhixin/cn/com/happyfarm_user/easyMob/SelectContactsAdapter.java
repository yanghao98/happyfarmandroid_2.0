package zhixin.cn.com.happyfarm_user.easyMob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by Administrator on 2018/6/11.
 */

public class SelectContactsAdapter extends RecyclerView.Adapter<SelectContactsAdapter.StaggerViewHolder>{

    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<SelectContact> mList;
    private AdapterView.OnItemClickListener itemClickListener;
    private int position;
    private Set<String> listStr = new HashSet<>();

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public SelectContactsAdapter(Context context, List<SelectContact> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public SelectContactsAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.select_contacts_items, null);
        //创建一个staggerViewHolder对象
        SelectContactsAdapter.StaggerViewHolder staggerViewHolder = new SelectContactsAdapter.StaggerViewHolder(view);
//        多选框点击事件
        staggerViewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                position = staggerViewHolder.getAdapterPosition();
                SelectContact selectContact = mList.get(position);
//                staggerViewHolder.checkBox.toggle();// 在每次获取点击的item时改变checkbox的状态
//                SelectContactsAdapter.isSelected.put(position,staggerViewHolder.checkBox.isChecked()); // 同时修改map的值保存状态
                if (isChecked) {
                    staggerViewHolder.contactImg.setBorderColor(Color.parseColor("#2dbda5"));//Color.parseColor("#909090")
                    staggerViewHolder.checkBox.setChecked(true);
                    listStr.add(selectContact.contactId);
                } else {
                    staggerViewHolder.contactImg.setBorderColor(Color.parseColor("#c9c9c9"));
                    staggerViewHolder.checkBox.setChecked(false);
                    listStr.remove(selectContact.contactId);
                }
                Log.i("打印选中数据：", String.valueOf(listStr));
            }
        });
        //TODO 布局点击事件 主要和多选框点击配合
        staggerViewHolder.selectContactsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = staggerViewHolder.getAdapterPosition();
                SelectContact selectContact = mList.get(position);
                if (staggerViewHolder.checkBox.isChecked()){
                    staggerViewHolder.contactImg.setBorderColor(Color.parseColor("#2dbda5"));
                    staggerViewHolder.checkBox.setChecked(false);
                    listStr.remove(selectContact.contactId);
                }else {
                    staggerViewHolder.contactImg.setBorderColor(Color.parseColor("#9400D3"));
                    staggerViewHolder.checkBox.setChecked(true);
                    listStr.add(selectContact.contactId);
                }
            }
        });
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(SelectContactsAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        SelectContact dataBean = mList.get(position);
        int section = getSectionForPosition(position);
        //如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
        if (position == getPositionForSection(section)) {
            holder.header.setVisibility(View.VISIBLE);
            holder.selectContactsTopView.setVisibility(View.VISIBLE);//显示上方分割线
            holder.header.setText(mList.get(position).getLetters());
        } else {
            holder.selectContactsTopView.setVisibility(View.GONE);//隐藏上方分割线
            holder.header.setVisibility(View.GONE);
        }
        //给Holder里面的控件对象设置数据
        setData(holder,dataBean);
    }

    public void setData(SelectContactsAdapter.StaggerViewHolder holder,SelectContact data) {
        holder.contactName.setText(data.contactName);
        /*  TODO 搜索框搜索选中状态消失 加上一下判断  */
        if (listStr.contains(data.contactId)){
            holder.checkBox.setChecked(true);
        }else {
            holder.checkBox.setChecked(false);
        }
        /*  TODO *******************************  */
        if (NumUtil.checkNull(data.contactImg)){
            holder.contactImg.setImageResource(R.drawable.icon_user_img);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(data.contactImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                    .into(holder.contactImg);//into(ImageView targetImageView)：图片最终要展示的地方。
        }
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {

        private final CheckBox checkBox;
        private final CircleImageView contactImg;
        private final TextView contactName;
        private final TextView header;
        private final RelativeLayout selectContactsLayout;
        private final View selectContactsTopView;

        @SuppressLint("WrongViewCast")
        public StaggerViewHolder(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.select_contacts_checkbox);
            contactImg = itemView.findViewById(R.id.select_contacts_avatar);
            contactName = itemView.findViewById(R.id.select_contacts_name);
            header = itemView.findViewById(R.id.header);
            selectContactsLayout = itemView.findViewById(R.id.select_contacts_layout);
            selectContactsTopView = itemView.findViewById(R.id.select_contacts_top_view);
        }

    }
    public Set<String> contactIdList() {
        return listStr;
    }
        /**
         * 列表点击事件
         *
         * @param itemClickListener
         */
    public void setOnItemClickListener(AdapterView.OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
    /**
     * 提供给Activity刷新数据
     * @param list
     */
    public void updateList(List<SelectContact> list){
        this.mList = list;

        notifyDataSetChanged();
    }
    /**
     * 根据ListView的当前位置获取分类的首字母的char ascii值
     */
    public int getSectionForPosition(int position) {
        return mList.get(position).getLetters().charAt(0);
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mList.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }

}
