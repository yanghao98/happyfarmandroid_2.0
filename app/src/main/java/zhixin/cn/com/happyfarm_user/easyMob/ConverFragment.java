package zhixin.cn.com.happyfarm_user.easyMob;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.EaseUI;
import com.hyphenate.easeui.domain.EaseUser;
import com.hyphenate.easeui.ui.EaseConversationListFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.MODE_PRIVATE;


public class ConverFragment extends EaseConversationListFragment {
    private final String TAG = "ConverFragment->";
    private Dialog mDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //启动activity时不自动弹出软键盘 //TODO 会话页面软键盘
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        //通过注册消息监听来接收消息。
        EMClient.getInstance().chatManager().addMessageListener(msgListener);
        EaseUI.getInstance().setUserProfileProvider(new EaseUI.EaseUserProfileProvider() {
            @Override
            public EaseUser getUser(String username) {

                Log.i("info", "用户信息: "+username);
                //TODO 这里用来填入用户的昵称和头像
                EaseUser user = Config.AllEMFriendInfo.get(username);
                //TODO 这里是设置自己的头像
                if (user != null && username.equals(EMClient.getInstance().getCurrentUser())){
                    user.setAvatar(getHeadImg());
                    user.setNickname(username);
                }
                return user;
            }
        });
        setConversationListItemClickListener(conversation ->{
//            Log.i(TAG, String.format("%s,%s",conversation.conversationId(),conversation.));
            Log.e(TAG, String.valueOf(conversation.getType()));
            switch (conversation.getType()){
                case Chat:
                    Log.e(TAG,conversation.conversationId());
                    Intent intent = new Intent(getActivity(),ChatActivity.class);
                    if (conversation.conversationId().equals(Config.eChatId)){
                        intent.putExtra("customer","customer");
                    }
                    intent.putExtra(EaseConstant.EXTRA_USER_ID, conversation.conversationId());
                    startActivity(intent);
                    break;
                case GroupChat:
//                    Intent intent = new Intent(getActivity(), ChatActivity.class);
//                    // it is group chat
//                    intent.putExtra("chatType", Constant.CHATTYPE_GROUP);
//                    intent.putExtra("userId", conversation.conversationId());
//                    startActivityForResult(intent, 0);
                    searchGroupMembers(conversation);
                    break;
            }

        });
        //TODO 会话页长按功能
        setConversationListItemLongClickListener(new EaseConversationListItemLongClickListener() {
            @Override
            public void onListItemLongClicked(EMConversation conversation, View view) {
                String username = conversation.conversationId();
                Log.e(TAG,"ConverFragment->"+username);
                PopupMenu menu = new PopupMenu(getActivity(), view);
                menu.getMenuInflater().inflate(R.menu.em_delete_message, menu.getMenu());
                menu.setOnMenuItemClickListener(item -> {
                    if (item.getItemId() == R.id.delete_conversation) {
                        //删除会话
                        //删除和某个user会话，如果需要保留聊天记录，传false
                        EMClient.getInstance().chatManager().deleteConversation(username, false);
                        conversationList.clear();
                        conversationList.addAll(loadConversationList());
                        conversationListView.refresh();
                    }
                    if (item.getItemId() == R.id.delete_message){
                        //删除会话和消息
                        //删除和某个user会话，如果需要保留聊天记录，传false
                        EMClient.getInstance().chatManager().deleteConversation(username, true);
                        conversationList.clear();
                        conversationList.addAll(loadConversationList());
                        conversationListView.refresh();
                    }
                    return true;
                });
                menu.show();
            }
        });
        return view;
    }
    EMMessageListener msgListener = new EMMessageListener() {

        @Override
        public void onMessageReceived(List<EMMessage> messages) {
            //收到消息
            Log.i("messages收到消息", String.valueOf(messages));
            conversationListView.refresh();
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
            //收到透传消息
            Log.i("收到透传消息", String.valueOf(messages));
        }

        @Override
        public void onMessageRead(List<EMMessage> messages) {
            //收到已读回执
            Log.i("收到已读回执", String.valueOf(messages));
        }

        @Override
        public void onMessageDelivered(List<EMMessage> message) {
            //收到已送达回执
            Log.i("收到已送达回执", String.valueOf(message));
        }
        @Override
        public void onMessageRecalled(List<EMMessage> messages) {
            //消息被撤回
            Log.i("消息被撤回", String.valueOf(messages));
        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
            //消息状态变动
            Log.i("消息被撤回", String.valueOf(message));
        }
    };
    @Override
    public void onStart() {
        super.onStart();
        EMClient.getInstance().chatManager().loadAllConversations();
    }

    @Override
    protected void initView() {
        super.initView();
    }

    //TODO 查询群成员
    public void searchGroupMembers(EMConversation conversation){
        mDialog = LoadingDialog.createLoadingDialog(getContext(), null);
        HttpHelper.initHttpHelper().searchGroupMembers(String.valueOf(conversation.conversationId())).enqueue(new Callback<searchGroup>() {
            @Override
            public void onResponse(Call<searchGroup> call, Response<searchGroup> response) {
                if (("success").equals(response.body().getFlag())){
                    if (!("[]").equals(response.body().getResult().getListOwners().size())) {
                        for (int i = 0; i <= response.body().getResult().getListOwners().size() - 1; i++) {
                            EaseUser u = new EaseUser(response.body().getResult().getListOwners().get(i).getUuid().toString());
                            u.setAvatar(response.body().getResult().getListOwners().get(i).getHeadImg());
                            u.setNickname(response.body().getResult().getListOwners().get(i).getNickname());
//                            mEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListOwners().get(i).getUserId()), u);

                            if (!Config.AllEMFriendInfo.containsKey(""+String.valueOf(response.body().getResult().getListOwners().get(i).getUserId()))){
                                Config.AllEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListOwners().get(i).getUserId()), u);
                            }

                        }
                    }
                    if (!("[]").equals(response.body().getResult().getListAdmins().size())) {
                        for (int i = 0; i <= response.body().getResult().getListAdmins().size() - 1; i++) {
                            EaseUser u = new EaseUser(response.body().getResult().getListAdmins().get(i).getUuid().toString());
                            u.setAvatar(response.body().getResult().getListAdmins().get(i).getHeadImg());
                            u.setNickname(response.body().getResult().getListAdmins().get(i).getNickname());
//                            mEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListAdmins().get(i).getUserId()), u);

                            if (!Config.AllEMFriendInfo.containsKey(""+String.valueOf(response.body().getResult().getListAdmins().get(i).getUserId()))){
                                Config.AllEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListAdmins().get(i).getUserId()), u);
                            }
                        }
                    }
                    if (!("[]").equals(response.body().getResult().getListMembers().size())) {
                        for (int i = 0; i <= response.body().getResult().getListMembers().size() - 1; i++) {
                            EaseUser u = new EaseUser(response.body().getResult().getListMembers().get(i).getUuid().toString());
                            u.setAvatar(response.body().getResult().getListMembers().get(i).getHeadImg());
                            u.setNickname(response.body().getResult().getListMembers().get(i).getNickname());
//                            mEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListMembers().get(i).getUserId()), u);

                            if (!Config.AllEMFriendInfo.containsKey(""+String.valueOf(response.body().getResult().getListMembers().get(i).getUserId()))){
                                Config.AllEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListMembers().get(i).getUserId()), u);
                            }
                        }
                    }
                    //TODO 这里主要是根据用户uuid 存放自己的id 好填入自己的头像
                    EaseUser u = new EaseUser(getUuId());
//                    mEMFriendInfo.put(""+getUserId(), u);

                    if (!Config.AllEMFriendInfo.containsKey(""+getUserId())){
                        Config.AllEMFriendInfo.put(""+getUserId(), u);
                    }

                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    // it is group chat
                    intent.putExtra("chatType", Constant.CHATTYPE_GROUP);
                    intent.putExtra("userId", conversation.conversationId());
                    startActivityForResult(intent, 0);
                    LoadingDialog.closeDialog(mDialog);
                }else{
                    LoadingDialog.closeDialog(mDialog);
                }
                Log.i("GroupInfoActivity->", JSON.toJSONString(response.body()));
            }

            @Override
            public void onFailure(Call<searchGroup> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }

    //TODO 获得用户头像
    private String getHeadImg(){
        return App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userImg","");
    }
    //TODO 获得用户uuid
    private String getUuId(){
        return App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("uuid","");
    }
    //TODO 获得用户id
    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }
}
