package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by HuYueling on 2018/5/15.
 */

public class MyGoods {

    public int goodId;//蔬菜id
    public String myGoodImg;//图片
    public String myGoodCropName;//蔬菜名
    public String myGoodFat;//是否有机肥
    public String myGoodName;//卖家名
    public String myGoodNum;//份数
    public String myGoodDiamond;//钻石
    public String myGoodIntegral;//积分

}
