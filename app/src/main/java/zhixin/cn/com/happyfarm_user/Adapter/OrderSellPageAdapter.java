package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.OrderSell;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by Administrator on 2018/5/17.
 */

public class OrderSellPageAdapter extends RecyclerView.Adapter<OrderSellPageAdapter.StaggerViewHolder> {
    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<OrderSell> mList;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public OrderSellPageAdapter(Context context, List<OrderSell> list) {
        mContext = context;
        mList = list;
    }

    @Override
    //创建staggerViewHolder，并把ViewHolder返回出去
    public OrderSellPageAdapter.StaggerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.order_sell_items, null);
        //创建一个staggerViewHolder对象
        OrderSellPageAdapter.StaggerViewHolder staggerViewHolder = new OrderSellPageAdapter.StaggerViewHolder(view);
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(OrderSellPageAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        OrderSell dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        setData(holder, dataBean);
    }

    public void setData(OrderSellPageAdapter.StaggerViewHolder holder, OrderSell data) {
        holder.sellAllTime.setText(data.sellAllTime);//交易钻石
        holder.sellAllCropName.setText(data.sellAllCropName);//作物名字
        holder.sellAllFertilizer.setText(data.sellAllFertilizer);//是否有机肥
        holder.sellAllBuyName.setText(data.sellAllBuyName);//买卖家姓名
        holder.sellAllNum.setText(data.sellAllNum);//份数
        holder.sellAllOnePrice.setText(data.sellAllOnePrice);//单份价格
        holder.sellConsuAmoutP.setText(data.sellConsuAmoutP);//合计金额价格
        holder.order_number.setText(data.sellOrderNumber);//订单号
        if (NumUtil.checkNull(data.sellAllImg)){
            holder.sellAllImg.setImageResource(R.drawable.maintain);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(data.sellAllImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .error(R.drawable.maintain)//如果请求图像无法加载，则可以使用一个错误绘制。
                    .into(holder.sellAllImg);//into(ImageView targetImageView)：图片最终要展示的地方。
        }//图片
    }


    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final TextView sellAllTime;//交易时间
        private final ImageView sellAllImg;//作物图片
        private final TextView sellAllCropName;//作物名字
        private final TextView sellAllFertilizer;//是否有机肥
        private final TextView sellAllBuyName;//买卖家姓名
        private final TextView sellAllNum;//份数
        private final TextView sellAllOnePrice;//单份价格
        private final TextView sellConsuAmoutP;//合计金额价格
        private final TextView order_number;//订单号

        public StaggerViewHolder(View itemView) {
            super(itemView);
            sellAllBuyName = itemView.findViewById(R.id.order_sell_seller_name);
            sellAllFertilizer = itemView.findViewById(R.id.order_sell_fat);
            sellAllNum = itemView.findViewById(R.id.order_sell_good_num);
            sellAllCropName = itemView.findViewById(R.id.order_sell_crop_name);
            sellAllImg = itemView.findViewById(R.id.order_sell_img);
            sellAllTime = itemView.findViewById(R.id.order_sell_all_time);
            sellConsuAmoutP = itemView.findViewById(R.id.order_sell_total_amount);
            sellAllOnePrice = itemView.findViewById(R.id.order_sell_diamond);
            order_number = itemView.findViewById(R.id.order_number);
        }


    }
}
