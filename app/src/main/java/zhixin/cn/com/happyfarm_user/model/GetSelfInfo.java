package zhixin.cn.com.happyfarm_user.model;

import android.util.Log;

import zhixin.cn.com.happyfarm_user.utils.Config;

/**
 * Created by DELL on 2018/3/26.
 */

public class GetSelfInfo {

    /**
     * result : {"id":9,"uuid":"ade4d300-1ed6-11e9-9f16-75dbb3aa6213","nickname":"胡萝卜","password":"e10adc3949ba59abbe56e057f20f883e","headImg":"http://img.trustwusee.com/headImg_9_1548227233","tel":"15170029037","email":"1228205445@qq.com","realname":"huyueling12","gender":1,"birthday":1516636800000,"plotId":1,"plotName":"江苏省无锡市滨湖区马山镇先河苑","receiverName":null,"receiverTel":null,"certificates":1,"certificatesNo":"362330199507585565","diamond":100,"exp":0,"isLandlord":1,"registerTime":1548224299000,"registerIp":"192.168.0.133","loginIp":"192.168.0.104","state":1,"isSign":1,"superUser":0,"landId":285,"landNo":285,"xingeToken":null}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * id : 9
         * uuid : ade4d300-1ed6-11e9-9f16-75dbb3aa6213
         * nickname : 胡萝卜
         * password : e10adc3949ba59abbe56e057f20f883e
         * headImg : http://img.trustwusee.com/headImg_9_1548227233
         * tel : 15170029037
         * email : 1228205445@qq.com
         * realname : huyueling12
         * gender : 1
         * birthday : 1516636800000
         * plotId : 1
         * plotName : 江苏省无锡市滨湖区马山镇先河苑
         * receiverName : null
         * receiverTel : null
         * certificates : 1
         * certificatesNo : 362330199507585565
         * diamond : 100
         * exp : 0
         * isLandlord : 1
         * registerTime : 1548224299000
         * registerIp : 192.168.0.133
         * loginIp : 192.168.0.104
         * state : 1
         * isSign : 1
         * isVoice : 1
         * superUser : 0
         * landId : 285
         * landNo : 285
         * xingeToken : null
         */

        private int id;
        private String uuid;
        private String nickname;
        private String password;
        private String headImg;
        private String tel;
        private String email;
        private String realname;
        private int gender;
        private long birthday;
        private int plotId;
        private String plotName;
        private String invitationCode;
        private String receiverName;
        private String receiverTel;
        private int certificates;
        private String certificatesNo;
        private Double diamond;
        private Double integralEgg;
        private int exp;
        private int isLandlord;
        private long registerTime;
        private String registerIp;
        private String loginIp;
        private int state;
        private int isSign;
        private int superUser;
        private int landId;
        private int landNo;
        private int isVoice;
        private String xingeToken;
        private long leaseTime;
        private long endTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid == null ? "" : uuid;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname == null ? "" : nickname;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password == null ? "" : password;
        }

        public String getHeadImg() {
            return headImg;
        }

        public void setHeadImg(String headImg) {
            this.headImg = headImg == null ? "" : headImg;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel == null ? "" : tel;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email == null ? "" : email;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname == null ? "" : realname;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public long getBirthday() {
            return birthday;
        }

        public void setBirthday(long birthday) {
            this.birthday = birthday;
        }

        public int getPlotId() {
            return plotId;
        }

        public void setPlotId(int plotId) {
            this.plotId = plotId;
        }

        public String getPlotName() {
            return plotName;
        }

        public void setPlotName(String plotName) {
            this.plotName = plotName == null ? "" : plotName;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName == null ? "" : receiverName;
        }

        public String getReceiverTel() {
            return receiverTel;
        }

        public void setReceiverTel(String receiverTel) {
            this.receiverTel = receiverTel == null ? "" : receiverTel;
        }

        public int getCertificates() {
            return certificates;
        }

        public void setCertificates(int certificates) {
            this.certificates = certificates;
        }

        public String getCertificatesNo() {
            return certificatesNo;
        }

        public void setCertificatesNo(String certificatesNo) {
            this.certificatesNo = certificatesNo;
        }

        public Double getDiamond() {
            return diamond;
        }

        public void setDiamond(Double diamond) {
            this.diamond = diamond;
        }

        public int getExp() {
            return exp;
        }

        public void setExp(int exp) {
            this.exp = exp;
        }

        public int getIsLandlord() {
            return isLandlord;
        }

        public void setIsLandlord(int isLandlord) {
            this.isLandlord = isLandlord;
        }

        public long getRegisterTime() {
            return registerTime;
        }

        public void setRegisterTime(long registerTime) {
            this.registerTime = registerTime;
        }

        public String getRegisterIp() {
            return registerIp;
        }

        public void setRegisterIp(String registerIp) {
            this.registerIp = registerIp;
        }

        public String getLoginIp() {
            return loginIp;
        }

        public void setLoginIp(String loginIp) {
            this.loginIp = loginIp;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public int getIsSign() {
            return isSign;
        }

        public void setIsSign(int isSign) {
            this.isSign = isSign;
        }

        public int getSuperUser() {
            return superUser;
        }

        public void setSuperUser(int superUser) {
            this.superUser = superUser;
        }

        public int getLandId() {
            return landId;
        }

        public void setLandId(int landId) {
            this.landId = landId;
        }

        public int getLandNo() {
            return landNo;
        }

        public void setLandNo(int landNo) {
            this.landNo = landNo;
        }

        public String getXingeToken() {
            return xingeToken;
        }

        public void setXingeToken(String xingeToken) {
            this.xingeToken = xingeToken == null ? "" : xingeToken;
        }

        public int getIsVoice() {
            return isVoice;
        }

        public void setIsVoice(int isVoice) {
            this.isVoice = isVoice;
        }

        public String getInvitationCode() {
            return invitationCode;
        }

        public void setInvitationCode(String invitationCode) {
            this.invitationCode = invitationCode;
        }

        public long getLeaseTime() {
            return leaseTime;
        }

        public void setLeaseTime(long leaseTime) {
            this.leaseTime = leaseTime;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        public Double getIntegralEgg() {
            return integralEgg;
        }

        public void setIntegralEgg(Double integralEgg) {
            this.integralEgg = integralEgg;
        }

        @Override
        public String toString() {
            return "ResultBean{" +
                    "id=" + id +
                    ", uuid='" + uuid + '\'' +
                    ", nickname='" + nickname + '\'' +
                    ", password='" + password + '\'' +
                    ", headImg='" + headImg + '\'' +
                    ", tel='" + tel + '\'' +
                    ", email='" + email + '\'' +
                    ", realname='" + realname + '\'' +
                    ", gender=" + gender +
                    ", birthday=" + birthday +
                    ", plotId=" + plotId +
                    ", plotName='" + plotName + '\'' +
                    ", receiverName=" + receiverName +
                    ", receiverTel=" + receiverTel +
                    ", certificates=" + certificates +
                    ", certificatesNo='" + certificatesNo + '\'' +
                    ", diamond=" + diamond +
                    ", exp=" + exp +
                    ", isLandlord=" + isLandlord +
                    ", registerTime=" + registerTime +
                    ", registerIp='" + registerIp + '\'' +
                    ", loginIp='" + loginIp + '\'' +
                    ", state=" + state +
                    ", isSign=" + isSign +
                    ", superUser=" + superUser +
                    ", landId=" + landId +
                    ", landNo=" + landNo +
                    ", isVoice=" + isVoice +
                    ", xingeToken=" + xingeToken +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "GetSelfInfo{" +
                "result=" + result +
                ", flag='" + flag + '\'' +
                '}';
    }
}
