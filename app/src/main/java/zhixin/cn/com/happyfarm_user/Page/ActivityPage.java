package zhixin.cn.com.happyfarm_user.Page;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.InvitationCodeActivity;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.SelectBranchActivity;
import zhixin.cn.com.happyfarm_user.TermsActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.GuideView;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.MODE_PRIVATE;

public class ActivityPage extends Fragment {
    private String TAG = "ActivityPage";
    private View myView;
    private WebView webView;
    private WebSettings webSettings;
//    private String url ="http:/192.168.0.16:8080/androidAPPLotteryPage";
    private String url ="https://www.trustwusee.com/androidAPPLotteryPage";
    private ProgressBar progressBar;//加载进度条
    private RefreshLayout refreshLayout;//下拉刷新
    private GuideView personalCenterPageGuideView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_activity,null,false);
        myView = view;
        view.findViewById(R.id.my_prompt1).setVisibility(View.VISIBLE);
        view.findViewById(R.id.lottery_content).setVisibility(View.GONE);
        progressBar = view.findViewById(R.id.lottery_progressBar2);
        refreshLayout = view.findViewById(R.id.lotteryLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setDisableContentWhenLoading(false);//设置是否开启在加载时候禁止操作内容视图
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()).setShowBezierWave(false));
        webView = view.findViewById(R.id.myWeb);
        initView();
        topRefreshLayout();
        return view;
    }

    private void initView() {
        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
    }


    private class JsInterface {
        private Context mContext;

        public JsInterface(Context context) {
            this.mContext = context;
        }

        //在js中调用window.AndroidWebView.showInfoFromJs(name)，便会触发此方法。
        @JavascriptInterface
        public void showInfoFromJs(String name) {
            System.out.println(name);
            if ("goSelectBranchActivity".equals(name)) {
                Intent intent = new Intent(getContext(), SelectBranchActivity.class);
                startActivity(intent);
            } else if ("goInvitationCodeActivity".equals(name)) {
                Intent intent = new Intent(getContext(), InvitationCodeActivity.class);
                startActivity(intent);
            }else {
                Intent intent = new Intent(getContext(), TermsActivity.class);
                intent.putExtra("termsName","奖品说明");
                intent.putExtra("url","https://www.trustwusee.com/luckDrawExplain");
                startActivity(intent);
            }
//            Toast.makeText(mContext, name, Toast.LENGTH_SHORT).show();
        }
    }

    //在java中调用js代码 (但是沒有試驗成功)
//    public void sendInfoToJs() {
//        //调用js中的函数：showInfoFromJava(msg)
//        webView.loadUrl("javascript:showInfoFromJava('" + "我从android来" + "')");
//        Toast.makeText(getContext(), "我发了一条消息给js", Toast.LENGTH_SHORT).show();
//    }

    private String getToken(){
        return App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("token","");
    }

    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        }
    }

    private class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            // TODO Auto-generated method stub
            if (newProgress == 100) {
                // 网页加载完成
                progressBar.setVisibility(View.GONE);//加载完网页进度条消失
            } else {
                // 加载中
                progressBar.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                progressBar.setProgress(newProgress);//设置进度值
            }
        }
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                webView.reload();

                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }

    /**
     * 验证token是否有效
     */
    public  void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
//                System.out.println("###"+ JSON.toJSONString(response.body()));
                if (("failed").equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(getContext(), "User_data", strArr);
                    myView.findViewById(R.id.my_prompt1).setVisibility(View.VISIBLE);
                    myView.findViewById(R.id.lottery_content).setVisibility(View.GONE);
                    noLoginDialog();
                } else if (("success").equals(response.body().getFlag())){
                    myView.findViewById(R.id.my_prompt1).setVisibility(View.GONE);
                    myView.findViewById(R.id.lottery_content).setVisibility(View.VISIBLE);
                    //token有效在加载web
                    webView.setWebChromeClient(new MyWebChromeClient());//判断页面加载过程
                    webView.addJavascriptInterface(new JsInterface(getContext()), "AndroidWebView"); //在js中调用本地java方法
                    webView.loadUrl(url);
                    /**
                     * 消息回调发送token
                     */
                    webView.setWebViewClient(new MyWebViewClient() {
                        @Override
                        public void onLoadResource(WebView view, String url) {
                            // TODO Auto-generated method stub
                            super.onLoadResource(view, url);
                            webView.loadUrl("javascript:showInfoFromJava('" + getToken() + " ')");
                        }
                    });
//                    searchSignRecord();
//                    selectUserInfo();
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }
    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog = new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }



    @Override
    public void onStart() {
        super.onStart();
        userInfoTokenTest();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
