package zhixin.cn.com.happyfarm_user.model;

public class ShoppingCartNum {
    private int result;
    private String flag;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
