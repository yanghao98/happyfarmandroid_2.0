package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.List;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.MyGoods;
import zhixin.cn.com.happyfarm_user.other.NumUtil;


/**
 * Created by HuYueling on 2018/5/15.
 */

public class MyVegetablesAdapter extends RecyclerView.Adapter<MyVegetablesAdapter.StaggerViewHolder>{
    private ButtonInterface buttonInterface;
    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<MyGoods> mList;
    private int position;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public MyVegetablesAdapter(Context context, List<MyGoods> list) {
        mContext = context;
        mList = list;
    }

    @Nullable
    @Override
    //创建staggerViewHolder，并把ViewHolder返回出去
    public MyVegetablesAdapter.StaggerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.my_vegetables_page_items, null);
        //创建一个staggerViewHolder对象
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);

        //立即下架点击事件
        staggerViewHolder.myGoodShelves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = staggerViewHolder.getAdapterPosition();
                MyGoods myGoods = mList.get(position);
                int myGoodId = myGoods.goodId;
//                Log.i("myGoodId", "onClick: " + myGoodId);
                if(buttonInterface!=null) {
//                  接口实例化后的而对象，调用重写后的方法
                    buttonInterface.onclick(v,position,myGoodId);
                }
            }
        });
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(MyVegetablesAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        MyGoods dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        setData(holder, dataBean);
    }

    public void setData(MyVegetablesAdapter.StaggerViewHolder holder, MyGoods data) {

        holder.myGoodDiamond.setText(data.myGoodDiamond);//钻石
        holder.myGoodIntegral.setText(data.myGoodIntegral);//积分
        holder.myGoodName.setText(data.myGoodName);//姓名
        holder.myGoodFat.setText(data.myGoodFat);//有机肥
        holder.myGoodNum.setText(data.myGoodNum);//份数
        holder.myGoodCropName.setText(data.myGoodCropName);//作物名
        if (NumUtil.checkNull(data.myGoodImg)){
            holder.myGoodImg.setImageResource(R.drawable.maintain);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(data.myGoodImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .error(R.drawable.maintain)//如果请求图像无法加载，则可以使用一个错误绘制。
                    .into(holder.myGoodImg);//into(ImageView targetImageView)：图片最终要展示的地方。
        }//图片
//        holder.myGoodImg.setText(data.myGoodDiamond);//图片

    }


    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final TextView myGoodIntegral;//积分
        private final TextView myGoodDiamond;//钻石
        private final TextView myGoodName;//买卖家姓名
        //        private final TextView foodBuy;//买卖家
        private final TextView myGoodFat;//是否有机肥
        private final TextView myGoodNum;//份数
        private final TextView myGoodCropName;//蔬菜名
        private final ImageView myGoodImg;//图片
        private final Button myGoodShelves;//立即下架
        public StaggerViewHolder(View itemView) {
            super(itemView);
            myGoodIntegral = itemView.findViewById(R.id.my_vegetables_integral);
            myGoodDiamond = itemView.findViewById(R.id.my_vegetables_diamond);
            myGoodName = itemView.findViewById(R.id.my_good_seller_name);
            myGoodFat = itemView.findViewById(R.id.my_vegetables_fat);
            myGoodNum = itemView.findViewById(R.id.my_veg_good_num);
            myGoodCropName = itemView.findViewById(R.id.my_vegetables_name);
            myGoodImg = itemView.findViewById(R.id.my_vegetables_img);
            myGoodShelves = itemView.findViewById(R.id.my_vegetables_shelves);
        }
    }

    /**
     *按钮点击事件需要的方法
     */
    public void buttonSetOnclick(ButtonInterface buttonInterface){
        this.buttonInterface=buttonInterface;
    }

    /**
     * 按钮点击事件对应的接口
     */
    public interface ButtonInterface{
        public void onclick( View view,int position,int myGoodId);
    }

}
