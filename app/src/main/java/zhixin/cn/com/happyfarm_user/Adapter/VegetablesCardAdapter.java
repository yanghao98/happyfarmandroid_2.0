package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import zhixin.cn.com.happyfarm_user.MyImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.LandInfoCardItem;
import zhixin.cn.com.happyfarm_user.model.VegetablesCard;

public class VegetablesCardAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<LandInfoCardItem.landInfoCardBean.Crop> cardList;
    private float mBaseElevation;
    private ButtonInterface buttonInterface;

    public VegetablesCardAdapter() {
        cardList = new ArrayList<>();
        mViews = new ArrayList<>();
    }

    public void addCardItem(List<LandInfoCardItem.landInfoCardBean.Crop> item) {
        cardList = item;
        for (int i = 0;i < cardList.size();i++) {
            mViews.add(null);
        }
    }

    @Override
    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return cardList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.vegetables_card_item, container, false);
        container.addView(view);
        bind(cardList.get(position), view);
        CardView cardView = (CardView) view.findViewById(R.id.vegetables_card);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }
        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(LandInfoCardItem.landInfoCardBean.Crop item, View view) {
        MyImageView imageView = view.findViewById(R.id.vegetables_icon);
        TextView textView1 = view.findViewById(R.id.vegetables_name);
        TextView textView2 = view.findViewById(R.id.other_names);
        TextView textView4 = view.findViewById(R.id.planting_time);
        TextView textView3 = view.findViewById(R.id.cycle);
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM 月 dd 日");
        String sd1 = sdf1.format(new Date(Long.parseLong(String.valueOf(item.getStartSowing()))));
        SimpleDateFormat sdf2 = new SimpleDateFormat("MM 月 dd 日");
        String sd2 = sdf2.format(new Date(Long.parseLong(String.valueOf(item.getEndSeeding()))));
        textView4.setText("适宜种植时间："+sd1+" ~ "+sd2);
        imageView.setImageURL(item.getCropImg());
        textView1.setText(item.getCropName());
        if (null == item.getOtherNames()) {
            textView2.setText("别名："+"暂无别名");
        } else {
            textView2.setText("别名："+item.getOtherNames());
        }

        textView3.setText("生长周期："+item.getCycle()+"个月");
    }


    /**
     * 按钮点击事件需要的方法
     */
    public void buttonSetOnclick(ButtonInterface buttonInterface) {
        this.buttonInterface = buttonInterface;
    }

    /**
     * 按钮点击事件对应的接口
     */
    public interface ButtonInterface {
        void onclick(View view, int position);
    }
}
