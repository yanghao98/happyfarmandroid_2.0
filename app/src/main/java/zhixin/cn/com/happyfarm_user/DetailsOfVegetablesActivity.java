package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.youth.banner.Banner;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.CookingListAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.Cooking;
import zhixin.cn.com.happyfarm_user.model.CropGoodsDetails;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.ShoppingCartNum;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.GlideImageLoader;
import zhixin.cn.com.happyfarm_user.utils.ImageUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

public class DetailsOfVegetablesActivity extends AppCompatActivity {

    private GridView gridView;
    private TextView cropName;
    private TextView cropPrice;
    private TextView cropNutritionalComponents;
    private Badge messageBadgeShoppingCart;
    private Dialog mDialog;
    private Integer messageBadgeNum;
    private TextView addShoppingCart;
    private ImageView shoppingCart;
    private MyImageView image;
    private String TAG = "DetailsOfVegetablesActivity";
    private Dialog dialog;
    private LinearLayout detailsLayout;
    private RelativeLayout noDetailsLayout;
    private HorizontalScrollView cookingLayout;
    private TextView cookingNoLayout;
    private String tokenStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_of_vegetables_activity);
        int corpId = getIntent().getIntExtra("corpId",0);
        initView(corpId);
        searchShoppingCartNum();
        searchCropGoodsDetails(corpId);
    }

    private void initView(int corpId) {
        SharedPreferences pref = DetailsOfVegetablesActivity.this.getSharedPreferences("User_data",MODE_PRIVATE);
        tokenStr = pref.getString("token","");
        gridView = findViewById(R.id.cooking_list_GridView);
        image = findViewById(R.id.image_changtu);
        addShoppingCart = findViewById(R.id.add_shopping_cart);
        addShoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(tokenStr)) {
                    noLoginDialog();
                } else {
                    addShoppingCart(corpId);
                }
//                if (Utils.isFastClick()) {
//                    HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
//                        @Override
//                        public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
//                            if ("success".equals(response.body().getFlag())) {
//
//                            } else {
//                                noLoginDialog();
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<GetSelfInfo> call, Throwable t) {
//                            noLoginDialog();
//                        }
//                    });
//                }
            }
        });
        cropName = findViewById(R.id.details_corp_name);
        cropPrice = findViewById(R.id.details_corp_price);
        cropNutritionalComponents = findViewById(R.id.details_corp_nutritional_components);
        shoppingCart = findViewById(R.id.details_shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(tokenStr)) {
                    noLoginDialog();
                } else {
                    startActivity(new Intent(DetailsOfVegetablesActivity.this, ShoppingCartActivity.class));
                }
            }
        });
        messageBadgeShoppingCart = new QBadgeView(DetailsOfVegetablesActivity.this)
                .bindTarget(findViewById(R.id.details_shopping_message_view_total))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
        detailsLayout = findViewById(R.id.details_layout);
        noDetailsLayout = findViewById(R.id.no_details_layout);
        cookingLayout = findViewById(R.id.cooking_layout);
        cookingNoLayout = findViewById(R.id.cooking_no_layout);
    }


    private void searchCookingList(int id){
//        Log.i(TAG, "searchCookingList: " + id);
        HttpHelper.initHttpHelper().searchCookingMethods(id).enqueue(new Callback<Cooking>() {
            @Override
            public void onResponse(Call<Cooking> call, Response<Cooking> response) {
//                System.out.println("searchCookingList:"+ JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    if (response.body().getResult().getList().size() > 0) {
                        cookingLayout.setVisibility(View.VISIBLE);
                        cookingNoLayout.setVisibility(View.GONE);
                        setGridView(response.body().getResult().getList());
                    } else {
                        cookingLayout.setVisibility(View.GONE);
                        cookingNoLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
            @Override
            public void onFailure(Call<Cooking> call, Throwable t) {

            }
        });
    }

    /**设置GirdView参数，绑定数据*/
    private void setGridView(List<Cooking.ResultBean.CookingList> cookingList) {
        int size = cookingList.size();
        int length = 100;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;
        int gridViewWidth = (int) (size * (length + 4) * density);
        int itemWidth = (int) (length * density);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                gridViewWidth, LinearLayout.LayoutParams.MATCH_PARENT);
        gridView.setLayoutParams(params); // 设置GirdView布局参数,横向布局的关键
        gridView.setColumnWidth(itemWidth); // 设置列表项宽
        gridView.setHorizontalSpacing(5); // 设置列表项水平间距
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setNumColumns(size); // 设置列数量=列表集合数

        CookingListAdapter adapter = new CookingListAdapter(DetailsOfVegetablesActivity.this, cookingList);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(DetailsOfVegetablesActivity.this, "烹饪id:" + cookingList.get(i).getId(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(DetailsOfVegetablesActivity.this, CookingRecommendedActivity.class);
                intent.putExtra("cookingId",cookingList.get(i).getId());
                startActivity(intent);
            }
        });
    }


    /**
     * 根据id查询 菜品详情
     */
    private void searchCropGoodsDetails(int id) {
//        System.out.println("菜品id："+id);
        dialog = LoadingDialog.createLoadingDialog(DetailsOfVegetablesActivity.this, "正在加载数据");
        HttpHelper.initHttpHelper().searchCropGoodsDetails(id).enqueue(new Callback<CropGoodsDetails>() {
            @Override
            public void onResponse(Call<CropGoodsDetails> call, Response<CropGoodsDetails> response) {
//                System.out.println("菜品详情数据："+JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())){
                    if (0 == response.body().getResult().getCropId()) {
                        Toast.makeText(DetailsOfVegetablesActivity.this,"暂无该菜品信息",Toast.LENGTH_SHORT).show();
//                        startActivity(new Intent(DetailsOfVegetablesActivity.this,MainActivity.class));
                        detailsLayout.setVisibility(View.GONE);
                        noDetailsLayout.setVisibility(View.VISIBLE);
                    } else {
                        CropGoodsDetails.ResultBean obj = response.body().getResult();
                        int id = obj.getId();//记录id
                        int num = obj.getNum();//数量
                        int cropId = obj.getCropId();//菜品id
                        searchCookingList(cropId);
                        cropName.setText("菜品名："+obj.getCropName());//菜品名字
                        String dbasicIntroduction = obj.getBasicIntroduction();
                        Double discountPrice = obj.getDiscountPrice();
                        String latinName = obj.getLatinName();//英文名
                        Double bargainPrice = obj.getBargainPrice();//
                        cropPrice.setText("价格："+obj.getPrice().toString()+"元/份");//价格
                        String otherNames = obj.getOtherNames();//别名
                        String nickname = obj.getNickname();//卖家名

                        String edibleAttention = obj.getEdibleAttention();//使用价值EMPushConfig
                        String mainValue = obj.getMainValue();//优点
                        cropNutritionalComponents.setText(obj.getNutritionalComponents());//营养价值
                        List<CropGoodsDetails.ResultBean.CropGoodsDepictList> headImgList = obj.getCropGoodsDepict();
                        if (obj.getCropGoodsDetails().size()>0) {
                            ImageUtils.returnBitMap(obj.getCropGoodsDetails().get(0).getDetailsImg());

//                            BufferedInputStream in = new BufferedInputStream(
//                                    new FileInputStream(new File(path)));
//                            BitmapFactory.Options options = new BitmapFactory.Options();
//                            options.inJustDecodeBounds = false;
//                            options.inSampleSize = 2;//宽和高变为原来的1/2，即图片压缩为原来的1/4
//                            Bitmap bitmap = BitmapFactory.decodeStream(in, null, options);

                            image.setImageURL(obj.getCropGoodsDetails().get(0).getDetailsImg());
                        }
                        List images = new ArrayList();
                        for (int i = 0;i < headImgList.size(); i++) {
                            images.add(headImgList.get(i).getDepictImg());
                        }
                        Banner banner = findViewById(R.id.details_banner);
                        //设置图片加载器
                        banner.setImageLoader(new GlideImageLoader());
                        //设置图片集合
                        banner.setImages(images);
                        //banner设置方法全部调用完毕时最后调用
                        banner.start();
                    }
//                    List<CropGoodsDetails.ResultBean.CropGoodsDetailsList> detailsList = obj.getCropGoodsDetails();
//                    for (int j = 0; j < detailsList.size(); j++) {
//                        addImage(detailsList.get(j).getDetailsImg());
//                    }

                } else {
                    Toast.makeText(DetailsOfVegetablesActivity.this,"暂无菜品详情数据",Toast.LENGTH_SHORT).show();
                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(Call<CropGoodsDetails> call, Throwable t) {
                Toast.makeText(DetailsOfVegetablesActivity.this,"数据加载失败",Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(dialog);
                Log.i("异常", "onFailure: " + JSON.toJSONString(t));
            }
        });
    }

    /**
     * 获取购物车数量
     */
    public void searchShoppingCartNum() {
        HttpHelper.initHttpHelper().searchShoppingCartNum().enqueue(new Callback<ShoppingCartNum>() {
            @Override
            public void onResponse(Call<ShoppingCartNum> call, Response<ShoppingCartNum> response) {
                System.out.println("购物车数量"+JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    messageBadgeNum = response.body().getResult();
                    messageBadgeShoppingCart.setBadgeNumber(response.body().getResult());
                }
            }
            @Override
            public void onFailure(Call<ShoppingCartNum> call, Throwable t) {

            }
        });
    }


    /**
     * 添加购物车
     */
    private void addShoppingCart(Integer goodsId) {
        Log.i("", "addShoppingCart: "+goodsId+",");
        mDialog = LoadingDialog.createLoadingDialog(DetailsOfVegetablesActivity.this, "添加中...");
        HttpHelper.initHttpHelper().addShoppingCart(goodsId).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if ("success".equals(response.body().getFlag())) {
                    messageBadgeNum++;
                    messageBadgeShoppingCart.setBadgeNumber(messageBadgeNum);
                    Toast.makeText(DetailsOfVegetablesActivity.this,"加入购物车成功",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DetailsOfVegetablesActivity.this,response.body().getResult(),Toast.LENGTH_SHORT).show();
                }
                LoadingDialog.closeDialog(mDialog);
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(DetailsOfVegetablesActivity.this,"加入购物车失败",Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(mDialog);

            }
        });
    }

    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(DetailsOfVegetablesActivity.this);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }

                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(DetailsOfVegetablesActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    public void back(View v) {
        finish();
    }
}
