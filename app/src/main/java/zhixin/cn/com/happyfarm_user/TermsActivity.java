package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;

import zhixin.cn.com.happyfarm_user.base.ABaseActivity;

/**
 * TermsActivity
 *
 * @author: Administrator.
 * @date: 2019/1/22
 */

public class TermsActivity extends ABaseActivity {

    private WebView webView;
    private ProgressBar progressBar;
    private String TAG = "TermsActivity";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        TextView title = findViewById(R.id.title_name);

        initView();

        //获取当前进来的对象所传的值
        Intent intent = getIntent();
        String termsName = intent.getStringExtra("termsName");
        String url = intent.getStringExtra("url");
        if (url.equals("")) {
            Log.i(TAG, "onCreate: 没有url");
            switch (termsName) {
                case "agreement":
                    title.setText(getResources().getString(R.string.use_service_agreement));
                    //WebView加载web资源
                    webView.loadUrl("https://www.trustwusee.com/termsService");
                    break;
                case "privacy":
                    title.setText(getResources().getString(R.string.privacy_service_agreement));
                    //WebView加载web资源
                    webView.loadUrl("https://www.trustwusee.com/privacypolicy");
                    break;
                case "egg":
                    title.setText(getResources().getString(R.string.agg_service_agreement));
                    webView.loadUrl("https://www.trustwusee.com/eggRules");
                    break;
                case "member":
                    title.setText(getResources().getString(R.string.member_service_agreement));
                    webView.loadUrl("https://www.trustwusee.com/userPrivilege");
                    break;
                default:
                    showToastShort(this, "加载失败");
            }
        } else {
            Log.i(TAG, "onCreate: 有url");
            title.setText(termsName);
            //WebView加载web资源
            webView.loadUrl(url);
        }

    }

    private void initView() {
        webView = findViewById(R.id.terms_webView);
        progressBar = findViewById(R.id.terms_progressBar);
        //启用支持javascript
        WebSettings settings = webView.getSettings();
        //设置了 文字大小不管用 缩进还是按照原字体大小，所以这样设置 既保证了缩进有保证了手机屏上的字体大小
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);

        settings.setDefaultTextEncodingName("utf-8");//这句话去掉也没事。。只是设置了编码格式
        settings.setJavaScriptEnabled(true);  //这句话必须保留。。不解释
        settings.setDomStorageEnabled(true);//这句话必须保留。。否则无法播放优酷视频网页。。其他的可以
        //开启缓存
//        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        // 设置出现缩放工具
        settings.setBuiltInZoomControls(false);
        // 设置可以访问文件
        settings.setAllowFileAccess(true);
        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);
        //判断页面加载过程
        webView.setWebChromeClient(new MyWebChromeClient());
//覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("intent") || url.startsWith("youku")) {
                    return true;
                } else {
                    return super.shouldOverrideUrlLoading(view, url);
                }
            }
        });

        //TODO webView返回上一页
        webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
                        webView.goBack();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            // TODO Auto-generated method stub
            if (newProgress == 100) {
                // 网页加载完成
                progressBar.setVisibility(View.GONE);//加载完网页进度条消失
            } else {
                // 加载中
                progressBar.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                progressBar.setProgress(newProgress);//设置进度值
            }

        }


    }

    public void back(View view) {
        finish();
    }

}
