package zhixin.cn.com.happyfarm_user.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

/**
 * SharedPreferencesUtils
 * SharedPreferences的一个工具类，调用setParam就能保存String, Integer, Boolean, Float, Long类型的参数
 * 同样调用getParam就能获取到保存在手机里面的数据
 * @author: Administrator.
 * @date: 2019/6/27
 */
public class SharedPreferencesUtils {
    /**
     * 保存在手机里面的文件名
     */
    private static final String FILE_NAME = "share_date";

    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     * @param context 上下文
     * @param fileName 文件名
     * @param key 存储的键
     * @param value 存储的值
     */
    public static void setParamString(Context context , String fileName, String key, String value){
        SharedPreferences sp = context.getSharedPreferences(fileName, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     * @param context 上下文
     * @param fileName 文件名
     * @param key 存储的键
     * @param value 存储的值
     */
    public static void setParamStrings(Context context , String fileName, String[] key, String[] value){
        if (key.length != value.length) {
            return;
        }
        SharedPreferences sp = context.getSharedPreferences(fileName, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        for (int i = 0; i < key.length; i++) {
            Log.i("SharedPreferences->", "setParamStrings->key: " + key[i] + "; value->: " + value[i]);
            editor.putString(key[i], value[i]);
        }
        editor.apply();
    }

    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     * @param context 上下文
     * @param fileName 文件名
     * @param key 存储的键
     * @param value 存储的值
     */
    public static void setParamInt(Context context , String fileName, String key, int value){
        SharedPreferences sp = context.getSharedPreferences(fileName, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     * @param context 上下文
     * @param fileName 文件名
     * @param key 存储的键
     * @param value 存储的值
     */
    public static void setParamInts(Context context , String fileName, String[] key, int[] value){
        if (key.length != value.length) {
            return;
        }
        SharedPreferences sp = context.getSharedPreferences(fileName, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        for (int i = 0; i < key.length; i++) {
            editor.putInt(key[i], value[i]);
        }
        editor.apply();
    }

    /**
     * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     * @param context 上下文
     * @param fileName 文件名
     * @param key 键的名称
     * @param defaultValue 默认值
     * @return 返回内容数据
     */
    public static String getParamString(Context context, String fileName, String key, String defaultValue){
        SharedPreferences sp = context.getSharedPreferences(fileName, MODE_PRIVATE);
        return sp.getString(key, defaultValue);
    }

    /**
     * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     * @param context 上下文
     * @param fileName 文件名
     * @param key 键的名称
     * @param defaultValue 默认值
     * @return 返回内容数据
     */
    public static int getParamInt(Context context, String fileName, String key, int defaultValue){
        SharedPreferences sp = context.getSharedPreferences(fileName, MODE_PRIVATE);
        return sp.getInt(key, defaultValue);
    }

    /**
     * 清空已经保存的内容
     * @param context 上下文
     * @param filename 文件名
     * @param key 键的名称
     */
    public static void cleanShared(Context context, String filename, String[] key) {
        //步骤1：创建一个SharedPreferences对象
        SharedPreferences pref = context.getSharedPreferences(filename, MODE_PRIVATE);
        //步骤2： 实例化SharedPreferences.Editor对象
        SharedPreferences.Editor editor = pref.edit();
        for (String k : key) {
            //步骤3：将获取过来的值放入文件
            editor.remove(k);
        }
        //步骤4：提交
        editor.apply();
    }

    /*
     使用也很简单，保存数据
     SharedPreferencesUtils.setParam(this, "String", "xiaanming");
     SharedPreferencesUtils.setParam(this, "int", 10);
     SharedPreferencesUtils.setParam(this, "boolean", true);
     SharedPreferencesUtils.setParam(this, "long", 100L);
     SharedPreferencesUtils.setParam(this, "float", 1.1f);

     获取数据
     SharedPreferencesUtils.getParam(TimerActivity.this, "String", "");                                                                                        SharedPreferencesUtils.getParam(TimerActivity.this, "int", 0);
     SharedPreferencesUtils.getParam(TimerActivity.this, "boolean", false);
     SharedPreferencesUtils.getParam(TimerActivity.this, "long", 0L);
     SharedPreferencesUtils.getParam(TimerActivity.this, "float", 0.0f);
     */
}