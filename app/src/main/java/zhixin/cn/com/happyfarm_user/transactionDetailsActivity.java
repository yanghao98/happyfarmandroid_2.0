package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.TransactionDetailsAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.TransactionDetails;
import zhixin.cn.com.happyfarm_user.utils.Utils;

public class TransactionDetailsActivity extends AppCompatActivity {

    private RefreshLayout mRefreshLayout;
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private String TAG = "TransactionDetailsActivity";
    private List<Map<String,Object>> mapList = new ArrayList<>();
    private TransactionDetailsAdapter adapter;
    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_details_activity);
        pageNum = 1;
        pageSize = 15;
        initView();
        searchUserConsumptionRecord(pageNum);
    }
    public void initView() {

        TextView title = findViewById(R.id.title_name);
        title.setText("消费明细");
        mRefreshLayout = (RefreshLayout) findViewById(R.id.transaction_details_refreshLayout);
        recyclerView = findViewById(R.id.transaction_details_recycler_view);
        adapter = new TransactionDetailsAdapter(TransactionDetailsActivity.this,mapList);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(TransactionDetailsActivity.this, 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
            //上拉滑动自动请求数据
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                    if(lastVisiblePosition >= gridLayoutManager.getItemCount() - 1){
                        if (isLastPage == true) {
//                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            searchUserConsumptionRecord(pageNum);

                        }
                    }
                }
            }
        });
        topRefreshLayout();
        bottomRefreshLayout();

    }

    /**
     * 查询消费记录
     */
    public void searchUserConsumptionRecord(int pageNum) {
        dialog = LoadingDialog.createLoadingDialog(TransactionDetailsActivity.this, "正在加载数据");
        HttpHelper.initHttpHelper().searchUserConsumptionRecord(pageNum,pageSize).enqueue(new Callback<TransactionDetails>() {
            @Override
            public void onResponse(Call<TransactionDetails> call, Response<TransactionDetails> response) {
                Log.i(TAG, "onResponse: " + JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    isLastPage = response.body().getResult().isLastPage();
                    for (TransactionDetails.ResultBean.ListBean listBean : response.body().getResult().getList()) {
                        Map<String,Object> map = new HashMap<>();
                        String content = "";
                        String time = "";
                        if (listBean.getPayState() == 1) {
                            //增加收益
                            if (1 == listBean.getScene()) {
                                //充值
                                content = "充值" +listBean.getAmount()+ "元成功";
                            }
                            if (5 == listBean.getScene()) {
                                //签到
                                content = "签到成功+"+listBean.getAmount()+"个鸡蛋";
                            }
                        } else if (2 == listBean.getPayState()) {
                            //减少收益
                            if (2 == listBean.getScene()) {
                                //操作
                                content = "操作了用户："+ listBean.getMaintainName() +"扣除," +listBean.getAmount()+"元";
                            }
                            if (3 == listBean.getScene()) {
                                //买地
                                content = "购买了"+listBean.getLandNo()+"号租地,"+"扣除" +listBean.getAmount()+"元";
                            }
                            if (4 == listBean.getScene()) {
                                //购买商品
                                content = "购买了"+listBean.getNickname()+"的"+listBean.getGoodsName()+",扣除" +listBean.getAmount()+"元";
                            }
                        }
                        time = Utils.getDateToStringMMDDHHmm(listBean.getStartTime());
                        map.put("time",time);
                        map.put("content",content);
                        map.put("type",listBean.getPayState());
                        mapList.add(map);
                    }
                    adapter.notifyDataSetChanged();
                }else {
//                    myDynamicPrompt.setVisibility(View.VISIBLE);
//                    recyclerView.setVisibility(View.GONE);
                    Toast.makeText(TransactionDetailsActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(Call<TransactionDetails> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Toast.makeText(TransactionDetailsActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(dialog);
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        mRefreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (mapList != null)
                    mapList.clear();
                isLastPage = false;
                pageNum = 1;
                refreshlayout.setNoMoreData(false);//TODO 恢复没有更多数据的原始状态
                searchUserConsumptionRecord(pageNum);
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }

    //上拉加载
    public void bottomRefreshLayout() {
        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        Log.i("info", "上拉刷新: "+isLastPage);
                        if (isLastPage == true) {
                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
                        } else {
                            pageNum++;
                            searchUserConsumptionRecord(pageNum);
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }

    public void back(View view) {
        finish();
    }
}