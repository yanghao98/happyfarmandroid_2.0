package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.ListDialogAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.SearchPlotManagements;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by Administrator on 2018/5/9.
 */

public class ModifyAddressActivity extends ABaseActivity {

    private SwitchButton defaultAddressSwitch;
    private Dialog mDialog;
    private int addressId = 0;
    private Button saveBtn;
    private String UserRealname;
    private String UserTel;
    private Button spinnerLower;
    private ArrayAdapter<String> arr_adapter;
    private List<Integer> addressIdList;
    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;
    private int poss;
    private TextView choiceText;
    private EditText addressPhone;
    private EditText addressName;
    private int isDefault;
    private int plotId;
    private String titleName;
    private int sqlAddressId;
    private LinkedList<String> addressNameList;
    private boolean inquireAddress = true;//查询地址接口是否需要重复请求

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_address_layout);
        Intent intent =getIntent();
        titleName = intent.getStringExtra("newAddress");
        UserRealname = intent.getStringExtra("UserRealname");
        UserTel = intent.getStringExtra("tel");
        TextView title = findViewById(R.id.title_name);
        if (titleName.equals("新增收货地址")){
            title.setText("新增收货地址");
        }else {
            isDefault = Integer.parseInt(intent.getStringExtra("isDefault"));
            System.out.println("修改收货地址:"+isDefault);
            addressId = Integer.parseInt(intent.getStringExtra("plotId"));
            System.out.println("修改收货地址:"+addressId);
//            choiceText.setText(intent.getStringExtra("address"));
            sqlAddressId = Integer.parseInt(intent.getStringExtra("addressId"));
            System.out.println("修改收货地址:"+sqlAddressId);
            title.setText("修改收货地址");
        }
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        defaultAddressSwitch = findViewById(R.id.default_address_switch);
        saveBtn = findViewById(R.id.save_btn);
        spinnerLower =findViewById(R.id.spinner_lower);
        choiceText = findViewById(R.id.choice_address_text);
        addressPhone = findViewById(R.id.modify_address_phone);
        addressName = findViewById(R.id.modify_address_name);

        if ("修改收货地址".equals(titleName)){
            if (isDefault == 1){
                defaultAddressSwitch.setChecked(true);
            }
            choiceText.setText(intent.getStringExtra("address"));
        }

        addressPhone.setText(UserTel);
        addressName.setText(UserRealname);
        defaultAddressSwitch();
        saveBtn();
        spinnerLower();
        //点击空白处退出键盘
        findViewById(R.id.addressBackView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

    }
    //TODO 默认地址点击事件
    public void defaultAddressSwitch() {
        defaultAddressSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    isDefault = 1;
                } else {
                    isDefault = 0;
                }

            }
        });
    }
    //TODO 保持地址点击事件
    public void saveBtn() {
        saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!NetWorkUtils.isNetworkAvalible(ModifyAddressActivity.this)){
                    Toast.makeText(ModifyAddressActivity.this,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
                }else {
                    if (addressId == 0) {
                        Toast.makeText(ModifyAddressActivity.this, "请选择您的收货地址", Toast.LENGTH_SHORT).show();
                    }else {
                        if ("新增收货地址".equals(titleName)){
                            HttpHelper.initHttpHelper().addReceivingAddress(addressId, String.valueOf(isDefault)).enqueue(new Callback<CheckSMS>() {
                                @Override
                                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                                    if (("success").equals(response.body().getFlag())) {
                                        Config.isManageAddressRefresh = true;//地址添加完成改为true 返回上一页则会刷新数据
                                        Toast.makeText(ModifyAddressActivity.this, "地址添加成功", Toast.LENGTH_SHORT).show();
                                        finish();
                                    } else {
                                        Toast.makeText(ModifyAddressActivity.this, response.body().getResult(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<CheckSMS> call, Throwable t) {
                                    showToastShort(ModifyAddressActivity.this, TextString.NetworkRequestFailed);
                                }
                            });
                        }else {
                            HttpHelper.initHttpHelper().updateReceivingAddress(sqlAddressId,addressId, String.valueOf(isDefault)).enqueue(new Callback<CheckSMS>() {
                                @Override
                                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                                    if (("success").equals(response.body().getFlag())) {
                                        Config.isManageAddressRefresh = true;//地址修改完成改为true 返回上一页则会刷新数据
                                        Toast.makeText(ModifyAddressActivity.this, "地址修改成功", Toast.LENGTH_SHORT).show();
                                        finish();
                                    } else {
                                        Toast.makeText(ModifyAddressActivity.this, response.body().getResult(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<CheckSMS> call, Throwable t) {
                                    showToastShort(ModifyAddressActivity.this, TextString.NetworkRequestFailed);
                                }
                            });
                        }
                    }
                }
            }
        });
    }
    //TODO 查询地址点击事件
    public void spinnerLower() {
        spinnerLower.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                chooseAddressInterface();
            }
        });
        choiceText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseAddressInterface();
            }
        });
    }
    //TODO 查询地址接口
    private void chooseAddressInterface(){
        if (inquireAddress){
            mDialog = LoadingDialog.createLoadingDialog(ModifyAddressActivity.this, "正在加载地址...");
            if (!NetWorkUtils.isNetworkAvalible(ModifyAddressActivity.this)){
                LoadingDialog.closeDialog(mDialog);
                Toast.makeText(ModifyAddressActivity.this,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
            }else {
                HttpHelper.initHttpHelper().searchPlotManagements().enqueue(new Callback<SearchPlotManagements>() {
                    @Override
                    public void onResponse(Call<SearchPlotManagements> call, Response<SearchPlotManagements> response) {
                        if (("success").equals(response.body().getFlag())) {
                            inquireAddress = false;//网络请求成功，查询后则不再查询 直到下次再来到此页面
                            LoadingDialog.closeDialog(mDialog);
//                            Log.e("response", JSON.toJSONString(response.body()));
                            addressIdList = new ArrayList<>();
                            addressNameList = new LinkedList<String>();//LinkedList链表结构插入快
                            for (int i = 0; i <= response.body().getResult().getList().size() - 1; i++) {
                                addressIdList.add(response.body().getResult().getList().get(i).getId());
                                addressNameList.add(response.body().getResult().getList().get(i).getAddressName() + response.body().getResult().getList().get(i).getPlotName());
                            }
                            arr_adapter = new ArrayAdapter<>(ModifyAddressActivity.this, android.R.layout.simple_spinner_item, addressNameList);
                            ShowDialogs(addressNameList, "地址");
                        } else {
                            LoadingDialog.closeDialog(mDialog);
                            Toast.makeText(ModifyAddressActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<SearchPlotManagements> call, Throwable t) {
                        LoadingDialog.closeDialog(mDialog);
                        showToastShort(ModifyAddressActivity.this, TextString.NetworkRequestFailed);
                    }
                });
            }
        }else {
            ShowDialogs(addressNameList, "地址");
        }
    }
    //Todo 地址按钮弹框
    public void ShowDialogs(List<String> list, String titles) {
        Context context = ModifyAddressActivity.this;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_dialog, null);
        ListView myListView = layout.findViewById(R.id.formcustomspinner_list);
        ListDialogAdapter adapter = new ListDialogAdapter(context, list);
        myListView.setAdapter(adapter);
        TextView title = layout.findViewById(R.id.label);
        title.setText(titles);
        layout.findViewById(R.id.list_dialog_bt).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                poss = i;
                addressId = addressIdList.get(i);
                choiceText.setText(list.get(i));
                alertDialog.cancel();

            }
        });
        builder = new AlertDialog.Builder(context);
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.show();
    }
    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm =  (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }
    public void back(View view) {
        finish();
    }

    /**
     * 重写返回键
     * 监听Back键按下事件
     * super.onBackPressed()会自动调用finish()方法,关闭当前Activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
