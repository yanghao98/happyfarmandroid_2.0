package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class SearchUserLandcrop {


    /**
     * result : [{"id":1,"cropName":"茄子","plantingDiamond":1,"harvestDiamond":1,"storeSale":1,"details":"好吃","startTime":1516070598000,"cycle":-1,"remark":"","updateTime":1522308585000,"managerId":1,"priority":4,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1},{"id":2,"cropName":"胡萝卜","plantingDiamond":15,"harvestDiamond":1,"storeSale":1,"details":"大补","startTime":1516068362000,"cycle":1,"remark":"","updateTime":1522308590000,"managerId":4,"priority":6,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1},{"id":11,"cropName":"土豆","plantingDiamond":1,"harvestDiamond":1,"storeSale":2,"details":"美颜","startTime":1516070711000,"cycle":-1,"remark":"","updateTime":1522308620000,"managerId":1,"priority":10,"cropImg":"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg","state":1}]
     * flag : success
     */

    private String flag;
    private List<ResultBean> result;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * id : 1
         * cropName : 茄子
         * plantingDiamond : 1
         * harvestDiamond : 1
         * storeSale : 1
         * details : 好吃
         * startTime : 1516070598000
         * cycle : -1
         * remark :
         * updateTime : 1522308585000
         * managerId : 1
         * priority : 4
         * cropImg : https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1522067249283&di=969559d9563e753c934afd5e8e1135c5&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fw%253D580%2Fsign%3D2e2cce9c8026cffc692abfba89004a7d%2F0d3735d12f2eb9380239c2f7d7628535e4dd6fab.jpg
         * state : 1
         */

        private int id;
        private String cropName;
        private int plantingDiamond;
        private int harvestDiamond;
        private int storeSale;
        private String details;
        private long startTime;
        private int cycle;
        private String remark;
        private long updateTime;
        private int managerId;
        private int priority;
        private String cropImg;
        private int state;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCropName() {
            return cropName;
        }

        public void setCropName(String cropName) {
            this.cropName = cropName;
        }

        public int getPlantingDiamond() {
            return plantingDiamond;
        }

        public void setPlantingDiamond(int plantingDiamond) {
            this.plantingDiamond = plantingDiamond;
        }

        public int getHarvestDiamond() {
            return harvestDiamond;
        }

        public void setHarvestDiamond(int harvestDiamond) {
            this.harvestDiamond = harvestDiamond;
        }

        public int getStoreSale() {
            return storeSale;
        }

        public void setStoreSale(int storeSale) {
            this.storeSale = storeSale;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public int getCycle() {
            return cycle;
        }

        public void setCycle(int cycle) {
            this.cycle = cycle;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getManagerId() {
            return managerId;
        }

        public void setManagerId(int managerId) {
            this.managerId = managerId;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public String getCropImg() {
            return cropImg;
        }

        public void setCropImg(String cropImg) {
            this.cropImg = cropImg;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }
}
