package zhixin.cn.com.happyfarm_user.easyMob;

/**
 * Created by Administrator on 2018/6/22.
 */

public class GroupInfo {
    public String groupImg;
    public String groupName;

    public String getGroupImg() {
        return groupImg;
    }

    public void setGroupImg(String groupImg) {
        this.groupImg = groupImg;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
