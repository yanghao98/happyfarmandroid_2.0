package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.dou361.ijkplayer.widget.PlayStateParams;
import com.dou361.ijkplayer.widget.PlayerView;

public class OtherLandVideoPlayActivity extends AppCompatActivity {

    private String albumName;
    private String videoUrl;
    private String photoAbstract;
    private String TAG = "OtherLandVideoPlayActivity ->";
    private PlayerView playerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.other_land_video_paly);
        initData();
    }
    private void initData() {
        Intent intent = getIntent();
        albumName = intent.getStringExtra("albumName");
        videoUrl = intent.getStringExtra("videoUrl");
        photoAbstract = intent.getStringExtra("abstract");
        TextView textView = findViewById(R.id.photoAbstract);

        if (albumName != null) {
            TextView title = findViewById(R.id.title_name);
            title.setText(albumName);
        }
//        Log.d(TAG, "initData: "+ videoUrl);
//        Log.d(TAG, "initData: "+albumName);
        if (videoUrl != null) {
            playerView = new PlayerView(this)
                    .setTitle(albumName)//视频名称
                    .setScaleType(PlayStateParams.fillparent)
                    .hideMenu(true)
                    .forbidTouch(false)
                    .setPlaySource(videoUrl);

            playerView.startPlay();
        }
        if (photoAbstract != null) {

            textView.setText(photoAbstract);
        } else {
            textView.setText("此用户很懒，没有对该视频作出描述！");
        }
    }

    //返回
    public void back(View view) {
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            playerView.stopPlay();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
