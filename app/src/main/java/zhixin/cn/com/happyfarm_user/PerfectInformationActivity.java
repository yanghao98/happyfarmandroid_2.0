package zhixin.cn.com.happyfarm_user;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.githang.statusbar.StatusBarCompat;
import com.qiniu.android.common.AutoZone;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.ListDialogAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.QiNiu;
import zhixin.cn.com.happyfarm_user.model.SearchPlotManagements;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.other.TextChangedListener;
import zhixin.cn.com.happyfarm_user.other.Validator;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by DELL on 2018/3/13.
 */

public class PerfectInformationActivity extends ABaseActivity {
    private Dialog mDialog;
    private TextView tv_data;
    private List<Integer> addressIdList;
    private int addressId;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private int gender;
    private EditText userNickname;
    private EditText userRealName;
    private EditText userEmail;
    //收货人姓名，暂时以去除
    private EditText userReceiverName;
    private EditText userCertificatesNo;
    private EditText userReceiverTel;
    private String birthdayTime;
    private String landNo;
    private String landId;
    private ImageView headImage;
    private TextView headNickName;
    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;
    private Button addressBtn;
    private TextView choiceText;
    private int poss;
    private Button modifyPictureBtn;

    private String msgEvent;
    private String personalEvent;
    private LinearLayout ersonalpCenter;

    public static final int TAKE_PHOTO = 1;//拍照
    public static final int CHOOSE_PHOTO = 2;//选择相册
    public static final int PICTURE_CUT = 3;//剪切图片
    private Uri imageUri;//相机拍照图片保存地址
    private Uri outputUri;//裁剪完照片保存地址
    private String imagePath;//打开相册选择照片的路径
    private boolean isClickCamera;//是否是拍照裁剪
    // 创建File对象，用于存储拍照后的图片
    private File outputImage;
    private Uri uri;
    private File dataFile;
    private String headImgName;
    private String headImg = null;
    private UploadManager uploadManager;
    private int landType;
    private RelativeLayout addressPrompt;//地址提示
//    @BindView(R.id.iv)
//    ImageView iv;//裁剪完照片展示控件

    private String tel;
    private String realname;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //启动activity时不自动弹出软键盘 //TODO 完善信息页面软键盘
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfect_information_layout);

        //自动识别上传区域
        Configuration config = new Configuration.Builder().zone(AutoZone.autoZone).build();
        uploadManager = new UploadManager(config);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("完善个人信息");
        modifyPictureBtn = findViewById(R.id.modify_picture_btn);
        tv_data = findViewById(R.id.perfect_information);
        radioGroup = findViewById(R.id.radioGroup);
        RadioButton radioMan = findViewById(R.id.radioMan);
        RadioButton radioWonan = findViewById(R.id.radioWonan);
        addressBtn = findViewById(R.id.spinner_lower);
        choiceText = findViewById(R.id.choice_text);
        TextView perfect_address = findViewById(R.id.perfect_address);
        userNickname = findViewById(R.id.user_nickname);
        userRealName = findViewById(R.id.user_real_name);
        userCertificatesNo = findViewById(R.id.user_certificates_no);
        userReceiverTel = findViewById(R.id.user_receiver_tel);
        headImage = findViewById(R.id.head_image);
        headNickName = findViewById(R.id.head_nickname);
        ersonalpCenter = findViewById(R.id.ersonalp_center);
        userEmail = findViewById(R.id.user_email);
        addressPrompt = findViewById(R.id.address_prompt);

        Intent intent = getIntent();
        landNo = intent.getStringExtra("landNo");
        landId = intent.getStringExtra("landId");
        msgEvent = intent.getStringExtra("infosuccessful");
        landType = Integer.parseInt(intent.getStringExtra("landType"));
        if ("infosuccessful".equals(msgEvent)) {//代表个人中心跳转
            ersonalpCenter.setVisibility(View.GONE);
            addressPrompt.setVisibility(View.GONE);
        }
        personalEvent = intent.getStringExtra("PersonalCenter");
        String headName = intent.getStringExtra("nickName");
        String headImageUser = intent.getStringExtra("userImg");

        if (NumUtil.checkNull(intent.getStringExtra("UserRealname"))) {
            userRealName.setText("");
        } else {
            userRealName.setText(intent.getStringExtra("UserRealname"));
        }
        if (NumUtil.checkNull(intent.getStringExtra("UserEmail"))) {
            userEmail.setText("");
        } else {
            userEmail.setText(intent.getStringExtra("UserEmail"));
        }
        if (NumUtil.checkNull(intent.getStringExtra("UserCertificatesNo"))) {
            userCertificatesNo.setText("");
        } else {
            userCertificatesNo.setText(intent.getStringExtra("UserCertificatesNo"));
        }
        if (NumUtil.checkNull(intent.getStringExtra("tel"))) {
            userReceiverTel.setText("");
        } else {
            userReceiverTel.setText(intent.getStringExtra("tel"));
        }
        String addressid = intent.getStringExtra("UserPlotId");
        System.out.println("完善信息页：" + addressid);
        //********************************************************//
        if (addressid != null && 0 != Integer.parseInt(addressid)) {
            String i = intent.getStringExtra("UserPlotId");
            addressId = Integer.parseInt(i);
            choiceText.setText(intent.getStringExtra("UserPlotName"));
        } else {
            addressId = 0;
        }
        //********************************************************//
        String genderid = intent.getStringExtra("UserGender");
        System.out.println("完善信息页：" + genderid);
        if (genderid != null && 0 != Integer.parseInt(genderid)) {
            gender = Integer.parseInt(intent.getStringExtra("UserGender"));
            System.out.println("完善信息页：" + gender);
            if (gender == 2) {
                radioWonan.setChecked(true);
            } else {
                radioMan.setChecked(true);
            }
        }
        //********************************************************//
        String Birthdayid = intent.getStringExtra("UserBirthday");
        System.out.println("完善信息页：" + Birthdayid);
        if (NumUtil.checkNull(intent.getStringExtra("UserBirthday"))) {
            tv_data.setText("");
        } else {
            birthdayTime = DateUtil.getStrTime(intent.getStringExtra("UserBirthday"));
            tv_data.setText(DateUtil.getStrTimeYear(intent.getStringExtra("UserBirthday")));
//            Log.i("用户生日信息打印：", DateUtil.getStrTimeYear(intent.getStringExtra("UserBirthday")));
        }

        headNickName.setText(headName);
        userNickname.setText(headName);
        if (NumUtil.checkNull(headImageUser)) {
            headImage.setImageResource(R.drawable.icon_user_img);
        } else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(headImageUser)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .error(R.drawable.icon_user_img)
                    .into(headImage);//into(ImageView targetImageView)：图片最终要展示的地方。
        }

        findViewById(R.id.select_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectTime();
            }
        });
        //获取单选框选中的内容为
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButton = findViewById(radioGroup.getCheckedRadioButtonId());
                String text = radioButton.getText().toString();
                if (text.equals("男")) {
                    gender = 1;
                } else if (text.equals("女")) {
                    gender = 2;
                } else {
                    gender = -10;
                }
            }
        });
//        mDialog = LoadingDialog.createLoadingDialog(PerfectInformationActivity.this,"加载中...");
//                    appCompatSpinner.setAdapter(arr_adapter);
        chooseAddress(); //选择地址
        modifyPictureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionSheetDialog();
            }
        });

        findViewById(R.id.perfect_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(userNickname.getText().toString());
//                Log.i("qiniudata", "Upload Success" + dataFile);
                userInfoSubmit();
            }
        });
        //点击空白处退出键盘
        findViewById(R.id.userInfoBackView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        if ("".equals(Config.qiuNiuToken)) {
            getQiniuToken();
        }
        editTextListener();
    }

    /**
     * 文本框监听
     */
    private void editTextListener() {
        //限制昵称长度
        TextChangedListener textChangedListener = new TextChangedListener(10, userNickname, PerfectInformationActivity.this, "昵称最大长度为10");
        userNickname.addTextChangedListener(textChangedListener.getmTextWatcher());
        //限制真实姓名长度
        TextChangedListener textChangedListener2 = new TextChangedListener(15, userRealName, PerfectInformationActivity.this, "姓名最大长度为15");
        userRealName.addTextChangedListener(textChangedListener2.getmTextWatcher());
        //限制身份证长度
        TextChangedListener textChangedListener3 = new TextChangedListener(18, userCertificatesNo, PerfectInformationActivity.this, "证件号最大长度为18");
        userCertificatesNo.addTextChangedListener(textChangedListener3.getmTextWatcher());
    }

    /**
     * 获取七牛信息
     */
    private void getQiniuToken() {
        HttpHelper.initHttpHelper().getQiniuToken().enqueue(new Callback<QiNiu>() {
            @Override
            public void onResponse(Call<QiNiu> call, Response<QiNiu> response) {
//                Log.e("data", JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    Config.qiuNiuToken = response.body().getResult().getUploadToken();
                    Config.qiuNiuDomain = response.body().getResult().getDomain();
                    Config.qiuNiuImgUrl = response.body().getResult().getImgUrl();
//                    Log.i("qiuNiuToken", "Upload Success" + Config.qiuNiuToken);
                } else {
//                    Log.i("qiuNiuToken", "Upload failure" + Config.qiuNiuToken);
                }
            }

            @Override
            public void onFailure(Call<QiNiu> call, Throwable t) {
//                Log.e("qiNiuToken", "onFailure: " + t);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    //TODO 选择地址点击事件
    private void chooseAddress() {
        addressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseAddressInterface();
            }
        });
        choiceText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseAddressInterface();
            }
        });
    }

    //TODO 选择地址接口
    private void chooseAddressInterface() {
        mDialog = LoadingDialog.createLoadingDialog(PerfectInformationActivity.this, "正在加载地址...");
        HttpHelper.initHttpHelper().searchPlotManagements().enqueue(new Callback<SearchPlotManagements>() {
            @Override
            public void onResponse(Call<SearchPlotManagements> call, Response<SearchPlotManagements> response) {
                if (("success").equals(response.body().getFlag())) {
                    LoadingDialog.closeDialog(mDialog);
//                    Log.e("response", JSON.toJSONString(response.body()));
                    addressIdList = new ArrayList<>();
                    LinkedList<String> addressNameList = new LinkedList<String>();//LinkedList链表结构插入快
                    for (int i = 0; i < response.body().getResult().getList().size(); i++) {
                        addressIdList.add(response.body().getResult().getList().get(i).getId());
                        addressNameList.add(response.body().getResult().getList().get(i).getAddressName() + response.body().getResult().getList().get(i).getPlotName());
                    }
                    ShowDialogs(addressNameList, "地址");
                } else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(PerfectInformationActivity.this, response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SearchPlotManagements> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                showToastShort(PerfectInformationActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }

    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    //Todo 完善用户信息
    public void userInfoSubmit() {
        if ("infosuccessful".equals(msgEvent)) {//代表个人中心跳转
            if (userNickname.getText().toString().equals("")) {
                Toast.makeText(PerfectInformationActivity.this, "请输入您的昵称(必填)", Toast.LENGTH_SHORT).show();
            } else if (gender == 0) {
                Toast.makeText(PerfectInformationActivity.this, "请选择您的性别(必填)", Toast.LENGTH_SHORT).show();
            } else if (NumUtil.checkNull(userRealName.getText().toString())) {
                Toast.makeText(PerfectInformationActivity.this, "请输入您的真实姓名(必填)", Toast.LENGTH_SHORT).show();
            } else if (!Validator.isIDCard(userCertificatesNo.getText().toString())) {
                Toast.makeText(PerfectInformationActivity.this, "请输入正确的证件号(必填)", Toast.LENGTH_SHORT).show();
            } else if (NumUtil.checkNull(birthdayTime)) {
                Toast.makeText(PerfectInformationActivity.this, "请选择您的出生日期(必填)", Toast.LENGTH_SHORT).show();
            } else if (!Validator.isEmail(userEmail.getText().toString())) {
                Toast.makeText(PerfectInformationActivity.this, "请输入正确的邮箱号码(必填)", Toast.LENGTH_SHORT).show();
            } else {
                mDialog = LoadingDialog.createLoadingDialog(PerfectInformationActivity.this, "修改中...");
                PerfectInfoNet("1");
            }
        } else {
            if (userNickname.getText().toString().equals("")) {
                Toast.makeText(PerfectInformationActivity.this, "请输入您的昵称(必填)", Toast.LENGTH_SHORT).show();
            } else if (gender == 0) {
                Toast.makeText(PerfectInformationActivity.this, "请选择您的性别(必填)", Toast.LENGTH_SHORT).show();
            } else if (NumUtil.checkNull(userRealName.getText().toString())) {
                Toast.makeText(PerfectInformationActivity.this, "请输入您的真实姓名(必填)", Toast.LENGTH_SHORT).show();
            } else if (!Validator.isIDCard(userCertificatesNo.getText().toString())) {
                Toast.makeText(PerfectInformationActivity.this, "请输入正确的证件号(必填)", Toast.LENGTH_SHORT).show();
            } else if (NumUtil.checkNull(userReceiverTel.getText().toString())) {
                Toast.makeText(PerfectInformationActivity.this, "请输入您的收货联系号码(必填)", Toast.LENGTH_SHORT).show();
            } else if (!Validator.isChinaPhoneLegal(userReceiverTel.getText().toString())) {
                Toast.makeText(PerfectInformationActivity.this, "请输入正确的手机号(必填)", Toast.LENGTH_SHORT).show();
            } else if (addressId == 0) {
                Toast.makeText(PerfectInformationActivity.this, "请选择您的收货地址(必填)", Toast.LENGTH_SHORT).show();
            } else if (NumUtil.checkNull(birthdayTime)) {
                Toast.makeText(PerfectInformationActivity.this, "请选择您的出生日期(必填)", Toast.LENGTH_SHORT).show();
            } else if (!Validator.isEmail(userEmail.getText().toString())) {
                Toast.makeText(PerfectInformationActivity.this, "请输入正确的邮箱号码(必填)", Toast.LENGTH_SHORT).show();
            } else {
                tel = userReceiverTel.getText().toString();
                realname = userRealName.getText().toString();
                mDialog = LoadingDialog.createLoadingDialog(PerfectInformationActivity.this, "修改中...");
                PerfectInfoNet("2");
            }
        }
    }

    //完善信息网络请求
    public void PerfectInfoNet(String judge) {
        //TODO judge判断是哪一个跳过来的请求
        if (judge.equals("1")) {
            Call<CheckSMS> updateUserInfo = HttpHelper.initHttpHelper().updateUserInfo(userNickname.getText().toString(), gender,
                    birthdayTime, userRealName.getText().toString(), userCertificatesNo.getText().toString(), userEmail.getText().toString());
            updateUserInfo.enqueue(new Callback<CheckSMS>() {
                @Override
                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                    if ("success".equals(response.body().getFlag())) {
                        LoadingDialog.closeDialog(mDialog);
//                        Toast.makeText(PerfectInformationActivity.this, "完善信息成功", Toast.LENGTH_SHORT).show();
                        showToastShort(PerfectInformationActivity.this, getResources().getString(R.string.update_user_info_success));
                        finish();
                    } else {
                        LoadingDialog.closeDialog(mDialog);
                        Toast.makeText(PerfectInformationActivity.this, response.body().getResult(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CheckSMS> call, Throwable t) {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(PerfectInformationActivity.this, "失败", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Call<CheckSMS> updateUserInfo = HttpHelper.initHttpHelper().updateUserInfo(userNickname.getText().toString(), gender,
                    birthdayTime, userRealName.getText().toString(), userCertificatesNo.getText().toString(), userEmail.getText().toString());
            updateUserInfo.enqueue(new Callback<CheckSMS>() {
                @Override
                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                    if ("success".equals(response.body().getFlag())) {
                        Call<CheckSMS> addReceivingAddress = HttpHelper.initHttpHelper().addReceivingAddress(addressId, "0");
                        addReceivingAddress.enqueue(new Callback<CheckSMS>() {
                            @Override
                            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                                if ("success".equals(response.body().getFlag())) {
                                    LoadingDialog.closeDialog(mDialog);
                                    if ("PersonalCenter".equals(personalEvent)) {
                                        //Toast.makeText(PerfectInformationActivity.this, "完善信息成功", Toast.LENGTH_SHORT).show();
                                        showToastShort(PerfectInformationActivity.this, getResources().getString(R.string.update_user_info_success));
                                        finish();
                                    } else {
                                        //Toast.makeText(PerfectInformationActivity.this, "完善信息成功", Toast.LENGTH_SHORT).show();
                                        showToastShort(PerfectInformationActivity.this, getResources().getString(R.string.update_user_info_success));
                                        interceptDialog();
                                    }
                                } else {
                                    LoadingDialog.closeDialog(mDialog);
                                    //Toast.makeText(PerfectInformationActivity.this, response.body().getResult(), Toast.LENGTH_SHORT).show();
                                    showToastShort(PerfectInformationActivity.this, response.body().getResult());
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckSMS> call, Throwable t) {
                                LoadingDialog.closeDialog(mDialog);
                            }
                        });

                    } else {
                        LoadingDialog.closeDialog(mDialog);
                        //Toast.makeText(PerfectInformationActivity.this, response.body().getResult(), Toast.LENGTH_SHORT).show();
                        showToastShort(PerfectInformationActivity.this, response.body().getResult());
                    }
                }

                @Override
                public void onFailure(Call<CheckSMS> call, Throwable t) {
                    LoadingDialog.closeDialog(mDialog);
                    showToastShort(PerfectInformationActivity.this, TextString.NetworkRequestFailed);
                }
            });
        }
    }

    /**
     * 租地拦截弹框
     * 由于业务限制，需要租地时告诉用户目前只对南京开放
     */
    private void interceptDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(PerfectInformationActivity.this);
        diyDialog.setCancel("取消")
                .setOk(getResources().getString(R.string.intercept_ok))
                .setContent(getResources().getString(R.string.intercept_content))
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                        finish();
                    }

                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        //跳转确认收货地址页面
                        Intent intent = new Intent(PerfectInformationActivity.this, ManageAddress2Activity.class);
                        intent.putExtra("tel", tel);
                        intent.putExtra("UserRealname", realname);
                        intent.putExtra("landNo", landNo);
                        intent.putExtra("landId", landId);
                        intent.putExtra("landType", landType + "");
                        startActivity(intent);
                        finish();
//                        //跳转支付页面
//                        Intent intent = new Intent(PerfectInformationActivity.this, PaymentInformationActivity.class);
//                        intent.putExtra("landNo", landNo);
//                        intent.putExtra("landId",landId);
//                        intent.putExtra("landType",landType+"");
//                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    //TODO 上传头像弹框
    private void ActionSheetDialog() {
        final String[] stringItems = {"拍照", "从相册中选择"};
        final ActionSheetDialog dialog = new ActionSheetDialog(PerfectInformationActivity.this, stringItems, null);
        dialog.title("上传头像")//
                .titleTextSize_SP(14.5f)//
                .show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
//                T.showShort(mContext, stringItems[position]);
                if (position == 0) {
//                            xiangjiClick(view);
                    //获取相机权限，如果不开启会报错  REQUEST_GET_PERMISSION与回调对应
                    if (ContextCompat.checkSelfPermission(PerfectInformationActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                        ActivityCompat.requestPermissions(PerfectInformationActivity.this,
                                new String[]{Manifest.permission.CAMERA}, 1);
                    } else {
                        openCamera();
                    }
                }
                if (position == 1) {
                    //动态权限
                    if (ContextCompat.checkSelfPermission(PerfectInformationActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(PerfectInformationActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                    } else {
                        openAlbum();//打开相册
                    }
                }
                dialog.dismiss();
            }
        });
    }

    //图片结果处理方法
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TAKE_PHOTO://拍照
                isClickCamera = true;
                if (resultCode == RESULT_OK) {
                    cropPhoto(imageUri);//裁剪图片
                }
                break;
            case CHOOSE_PHOTO://打开相册

                if (data == null) {//这里判断主要测试与小米手机，点击相册允许后，再点取消会崩溃
                    return;
                }// 判断手机系统版本号
                if (Build.VERSION.SDK_INT >= 19) {
                    // 4.4及以上系统使用这个方法处理图片
                    handleImageOnKitKat(data);
                } else {
                    // 4.4以下系统使用这个方法处理图片
                    handleImageBeforeKitKat(data);
                }
                break;
            case PICTURE_CUT://裁剪完成
                if (data != null) {
                    Bitmap bitmap = null;
                    try {
                        if (isClickCamera) {
                            bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(outputUri));
                        } else {
                            bitmap = BitmapFactory.decodeFile(imagePath);
                        }
                        //TODO 这里暂时有问题
                        if (bitmap == null) {
                            //用于删除上面创建的拍照产生的图片
                            outputImage.delete();
                        } else {
                            //设置剪切完的图片显示位置
                            headImage.setImageBitmap(bitmap);
                        }
                        //用于七牛 先拿到图片的位置
                        dataFile = new File(outputUri.getPath());
                        if (dataFile != null) {
                            long timeStampSec = System.currentTimeMillis() / 1000;
                            String timestamp = String.format("%010d", timeStampSec);
                            headImgName = "headImg_" + getUserId() + "_" + timestamp;//<指定七牛服务上的文件名，或 null>;
                            uploadManager.put(dataFile, headImgName, Config.qiuNiuToken,
                                    new UpCompletionHandler() {
                                        @Override
                                        public void complete(String key, ResponseInfo info, org.json.JSONObject response) {
                                            //res包含hash、key等信息，具体字段取决于上传策略的设置
                                            if (info.isOK()) {
                                                String headImg = Config.qiuNiuImgUrl + headImgName;
                                                Picasso.get()
                                                        .load(headImg)
                                                        .error(R.drawable.icon_user_img)
                                                        .into(headImage);
                                                userHeadImage(headImg);
                                                if (dataFile.exists()) {
                                                    dataFile.delete();
                                                }
                                                Log.i("qiniu", "Upload Success");
                                            } else {
                                                Config.qiuNiuToken = "";
                                                Config.qiuNiuDomain = "";
                                                Config.qiuNiuDomain = "";
                                                Log.i("qiniu", "Upload Fail");
                                                //如果失败，这里可以把info信息上报自己的服务器，便于后面分析上传错误原因
                                            }
                                            Log.i("qiniu", key + ",\r\n " + info + ",\r\n " + response);
                                        }
                                    }, null);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    private void openCamera() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        String filename = timeStampFormat.format(new Date());
        // 创建File对象，用于存储拍照后的图片
        outputImage = new File(getExternalCacheDir(), filename + "output_image.png");
        try {
            if (outputImage.exists()) {
                outputImage.delete();
            } else {
                outputImage.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT < 24) {
            imageUri = Uri.fromFile(outputImage);
        } else {
            //Android 7.0系统开始 使用本地真实的Uri路径不安全,使用FileProvider封装共享Uri
            //参数二:fileprovider绝对路径 com.dyb.testcamerademo：项目包名
            imageUri = FileProvider.getUriForFile(PerfectInformationActivity.this, "zhixin.cn.com.happyfarm_user", outputImage);
        }
        // 启动相机程序
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, TAKE_PHOTO);

    }

    //    private void selectFromAlbum() {
//        if (ContextCompat.checkSelfPermission(PerfectInformationActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(PerfectInformationActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//        } else {
//            openAlbum();
//        }
//    }
    private void openAlbum() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent, CHOOSE_PHOTO); // 打开相册
    }

    /**
     * 裁剪图片
     */
    private void cropPhoto(Uri uri) {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        String filename = timeStampFormat.format(new Date());
        ;
        // 创建File对象，用于存储裁剪后的图片，避免更改原图
        //TODO id 加 head
        File file = new File(getExternalCacheDir(), getUserId() + "_head.png");
        try {
            if (file.exists()) {
                file.delete();
            } else {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        outputUri = Uri.fromFile(file);
        Intent intent = new Intent("com.android.camera.action.CROP");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.setDataAndType(uri, "image/*");
        //裁剪图片的宽高比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("crop", "true");//可裁剪
        // 裁剪后输出图片的尺寸大小
        //intent.putExtra("outputX", 400);
        //intent.putExtra("outputY", 200);
        intent.putExtra("scale", true);//支持缩放
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.toString());//输出图片格式
        intent.putExtra("noFaceDetection", true);//取消人脸识别
        startActivityForResult(intent, PICTURE_CUT);
    }

    // 4.4及以上系统使用这个方法处理图片 相册图片返回的不再是真实的Uri,而是分装过的Uri
    @TargetApi(19)
    private void handleImageOnKitKat(Intent data) {
        imagePath = null;
        uri = data.getData();
//        Log.d("TAG", "handleImageOnKitKat: uri is " + uri);
        if (DocumentsContract.isDocumentUri(this, uri)) {
            // 如果是document类型的Uri，则通过document id处理
            String docId = DocumentsContract.getDocumentId(uri);
            if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
                String id = docId.split(":")[1]; // 解析出数字格式的id
                String selection = MediaStore.Images.Media._ID + "=" + id;
                imagePath = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection);
            } else if ("com.android.providers.downloads.documents".equals(uri.getAuthority())) {
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
                imagePath = getImagePath(contentUri, null);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // 如果是content类型的Uri，则使用普通方式处理
            imagePath = getImagePath(uri, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            // 如果是file类型的Uri，直接获取图片路径即可
            imagePath = uri.getPath();
        }
        cropPhoto(uri);
    }

    private String getImagePath(Uri uri, String selection) {
        String path = null;
        // 通过Uri和selection来获取真实的图片路径
        Cursor cursor = getContentResolver().query(uri, null, selection, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }

    private void handleImageBeforeKitKat(Intent data) {
        Uri uri = data.getData();
        imagePath = getImagePath(uri, null);
        cropPhoto(uri);
    }

    //获取用户权限  打开相机
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            //此处1 ，2  与上方打开的1，2对应
            case 1:
                //判断是否有权限
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera();//打开相机
                } else {
                    Toast.makeText(this, "你需要授权才可以使用哦", Toast.LENGTH_LONG).show();
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openAlbum();
                } else {
                    Toast.makeText(this, "你需要授权才可以使用哦", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    //时间选择器
    public void SelectTime() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectedDate = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        Calendar startDateTime = Calendar.getInstance();
        startDateTime.set(1900, 0, 31);
        //时间选择器
        TimePickerView pvTime = new TimePickerBuilder(this, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                tv_data.setText(sdf.format(date));
                String a = DateUtil.getTime(tv_data.getText().toString());
                birthdayTime = DateUtil.getStrTime(a);
            }
        })
                .setTitleText("选择时间")//标题文字
                .setTitleSize(15)//标题文字大小
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setCancelColor(Color.GRAY)//取消按钮文字颜色
                .isDialog(true)
                .setDate(selectedDate)
                .setRangDate(startDateTime, selectedDate)
                .build();
        pvTime.show();
        Dialog mDialog = pvTime.getDialog();
        if (mDialog != null) {

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvTime.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
//                    dialogWindow.setWindowAnimations(com.bigkoo.pickerview.R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
            }
        }
    }

    //Todo 上传头像接口
    public void userHeadImage(String headImage) {
        mDialog = LoadingDialog.createLoadingDialog(PerfectInformationActivity.this, "上传头像中...");
        Call<CheckSMS> updateUserInfo = HttpHelper.initHttpHelper().updateUserImg(headImage);
        updateUserInfo.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if (response.body().getFlag().equals("success")) {
                    LoadingDialog.closeDialog(mDialog);
                    saveHeadImage(headImage);
//                    view.postInvalidate();
//                    new GameThread().run();
                    Toast.makeText(PerfectInformationActivity.this, "头像上传成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PerfectInformationActivity.this, response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(PerfectInformationActivity.this, "失败", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void saveHeadImage(String ImageUrl) {
        SharedPreferences.Editor editor = getSharedPreferences("User_data", MODE_PRIVATE).edit();
        editor.putString("userImg", ImageUrl);
        editor.apply();
    }

    public void ShowDialogs(List<String> list, String titles) {
        Context context = PerfectInformationActivity.this;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_dialog, null);
        ListView myListView = layout.findViewById(R.id.formcustomspinner_list);
        ListDialogAdapter adapter = new ListDialogAdapter(context, list);
        myListView.setAdapter(adapter);
        TextView title = layout.findViewById(R.id.label);
        title.setText(titles);
        layout.findViewById(R.id.list_dialog_bt).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                poss = i;
                addressId = addressIdList.get(i);
                choiceText.setText(list.get(i));
                alertDialog.cancel();

            }
        });
        builder = new AlertDialog.Builder(context);
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void back(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        // 隐藏软键盘
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private int getUserId() {
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", "").equals("")) {
            userId = 0;
            return userId;
        } else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", ""));
        }
        return userId;
    }
}
