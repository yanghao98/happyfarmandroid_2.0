package zhixin.cn.com.happyfarm_user.easyMob;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.EaseConstant;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.searchFriendDetail;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by Administrator on 2018/6/14.
 */

public class FriendsDetailsActivity extends ABaseActivity {

    private TextView name;
    private TextView phone;
    private TextView lease;
    private CircleImageView img;
    private LinearLayout deleteRecords;
    private Button removeBuddy;
    private String userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.em_activity_friends_details_layout);

        TextView title = findViewById(R.id.title_name);
        title.setText("聊天详情");
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        Intent intent = getIntent();
        userId = intent.getStringExtra(EaseConstant.EXTRA_USER_ID);
        name = findViewById(R.id.friends_details_name);
        phone = findViewById(R.id.friends_details_account);
        lease = findViewById(R.id.friends_details_lease);
        img = findViewById(R.id.friends_details_image);
        deleteRecords = findViewById(R.id.delete_layout);
        removeBuddy = findViewById(R.id.delete_btn);
        DeleteRecords();
        DeleteFriend();
        friendsDetails();
    }

    //用户信息接口
    public void friendsDetails() {
        //TODO 这里主要判断环信管理员账号发送来的信息
        if ("admin".equals(userId)) {
            name.setText("admin(管理员)");
            phone.setText("");
            lease.setText("");
            img.setImageResource(R.drawable.icon_user_img);
        } else {
            if (TextUtils.isDigitsOnly(userId)) {
                HttpHelper.initHttpHelper().searchFriendDetail(Integer.parseInt(userId)).enqueue(new Callback<searchFriendDetail>() {
                    @Override
                    public void onResponse(Call<searchFriendDetail> call, Response<searchFriendDetail> response) {
                        try {
                            Log.e("FriendsResponse", JSON.toJSONString(response.body()));
                            if (("success").equals(response.body().getFlag())) {
                                if (null != response.body().getResult().toString()) {
                                    name.setText(response.body().getResult().get(0).getNickname().toString());
                                    phone.setText(response.body().getResult().get(0).getTel().toString());
                                    //TODO 此处地号打印格式为307.0
                                    String str = String.valueOf(response.body().getResult().get(0).getLandNo());
//                                if (str != null) {
//                                    //截取#之前的字符串
//                                    str = str.substring(0, str.indexOf("."));
//                                    Log.i("好友详情地号", "onResponse: " + str);
//                                }
                                    lease.setText(str);
                                    if (NumUtil.checkNull(response.body().getResult().get(0).getHeadImg())) {
                                        img.setImageResource(R.drawable.icon_user_img);
                                    } else {
                                        //Picasso使用了流式接口的调用方式
                                        //Picasso类是核心实现类。
                                        //实现图片加载功能至少需要三个参数：
                                        Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                                                .load(response.body().getResult().get(0).getHeadImg())//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                                                .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                                                .into(img);//into(ImageView targetImageView)：图片最终要展示的地方。
                                    }
                                }
                            } else {
                                Toast.makeText(FriendsDetailsActivity.this, response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (NullPointerException e) {
                            name.setText("");
                            phone.setText("");
                            lease.setText("");
                            img.setImageResource(R.drawable.icon_user_img);
                        }
                    }

                    @Override
                    public void onFailure(Call<searchFriendDetail> call, Throwable t) {
                        Toast.makeText(FriendsDetailsActivity.this, "网络请求超时,请检查一下网络", Toast.LENGTH_SHORT).show();
                    }
                });
            }else {
                deleteRecords.setVisibility(View.GONE);
                removeBuddy.setVisibility(View.GONE);
            }

        }
    }

    //删除聊天记录
    public void DeleteRecords() {
        deleteRecords.setOnClickListener(v -> {
            showDialog("确认清除", "您确定清除你与该用户的聊天信息吗？清除以后是没有办法找回的！");
        });
    }

    //删除好友
    public void DeleteFriend() {
        removeBuddy.setOnClickListener(v -> {
            showDialog("确认删除", "您确定删除该好友吗？确定删除会清除你与该用户的聊天信息");
        });
    }

    //删除好友接口
    public void deleteFriend() {
        HttpHelper.initHttpHelper().deleteFriend(Integer.parseInt(userId)).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if (("success").equals(response.body().getFlag())) {
                    //删除和某个user会话，如果需要保留聊天记录，传false
                    EMClient.getInstance().chatManager().deleteConversation(userId, true);
                    Toast.makeText(FriendsDetailsActivity.this, response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                    ChatActivity.destoryActivity(String.valueOf(ChatActivity.class)); //TODO 销毁上个聊天页面
                    finish();
                } else {
                    Toast.makeText(FriendsDetailsActivity.this, response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(FriendsDetailsActivity.this, "网络请求超时,请检查一下网络", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void back(View view) {
        finish();
    }

    //删除好友 或者 清除聊天记录
    private void showDialog(String LblTitle, String ContentTitle) {
        final AlertUtilBest diyDialog =new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk(LblTitle)
                .setContent(ContentTitle)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        if ("确认删除".equals(LblTitle)) {
                            deleteFriend();
                        }
                        if ("确认清除".equals(LblTitle)) {
                            //删除和某个user会话，如果需要保留聊天记录，传false
                            EMClient.getInstance().chatManager().deleteConversation(userId, true);
                        }
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
}
