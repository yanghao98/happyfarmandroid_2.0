package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by Administrator on 2018/5/9.
 */

public class ManageAddress {
    public String addressId;
    public String isDefault;
    public String plotId;
    public String defaultAddress;
    public String manageAddressName;
    public String manageAddressPhone;
    public String manageAddressDetail;
    public String manageAddressNum;
}
