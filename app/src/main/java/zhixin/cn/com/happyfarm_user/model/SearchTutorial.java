package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class SearchTutorial {


    /**
     * result : {"pageNum":1,"pageSize":4,"size":4,"orderBy":null,"startRow":0,"endRow":3,"total":4,"pages":1,"list":[{"id":16,"tutorialName":"123","author":"123","startTime":1521793910000,"keyword":"123","priority":2,"state":1,"updateTime":null,"managerId":1},{"id":8,"tutorialName":"青菜养护教程","author":"周子龙","startTime":1516260280000,"keyword":null,"priority":10,"state":1,"updateTime":null,"managerId":6},{"id":7,"tutorialName":"秋波养护教程","author":"周子龙","startTime":1516264710000,"keyword":null,"priority":10,"state":1,"updateTime":null,"managerId":1},{"id":2,"tutorialName":"秋葵养护教程","author":"zzl","startTime":1516262369000,"keyword":"hello","priority":10,"state":1,"updateTime":null,"managerId":6}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 4
         * size : 4
         * orderBy : null
         * startRow : 0
         * endRow : 3
         * total : 4
         * pages : 1
         * list : [{"id":16,"tutorialName":"123","author":"123","startTime":1521793910000,"keyword":"123","priority":2,"state":1,"updateTime":null,"managerId":1},{"id":8,"tutorialName":"青菜养护教程","author":"周子龙","startTime":1516260280000,"keyword":null,"priority":10,"state":1,"updateTime":null,"managerId":6},{"id":7,"tutorialName":"秋波养护教程","author":"周子龙","startTime":1516264710000,"keyword":null,"priority":10,"state":1,"updateTime":null,"managerId":1},{"id":2,"tutorialName":"秋葵养护教程","author":"zzl","startTime":1516262369000,"keyword":"hello","priority":10,"state":1,"updateTime":null,"managerId":6}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 16
             * tutorialName : 123
             * author : 123
             * startTime : 1521793910000
             * keyword : 123
             * priority : 2
             * state : 1
             * updateTime : null
             * managerId : 1
             */

            private int id;
            private String tutorialName;
            private String author;
            private long startTime;
            private String keyword;
            private int priority;
            private int state;
            private Object updateTime;
            private int managerId;
            private String cropImg;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTutorialName() {
                return tutorialName;
            }

            public void setTutorialName(String tutorialName) {
                this.tutorialName = tutorialName;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public String getKeyword() {
                return keyword;
            }

            public void setKeyword(String keyword) {
                this.keyword = keyword;
            }

            public int getPriority() {
                return priority;
            }

            public void setPriority(int priority) {
                this.priority = priority;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public Object getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(Object updateTime) {
                this.updateTime = updateTime;
            }

            public int getManagerId() {
                return managerId;
            }

            public void setManagerId(int managerId) {
                this.managerId = managerId;
            }

            public String getCropImg() {
                return cropImg;
            }

            public void setCropImg(String cropImg) {
                this.cropImg = cropImg;
            }
        }
    }
}
