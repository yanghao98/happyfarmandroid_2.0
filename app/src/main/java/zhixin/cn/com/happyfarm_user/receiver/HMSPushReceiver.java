/*
 *  * EaseMob CONFIDENTIAL
 * __________________
 * Copyright (C) 2017 EaseMob Technologies. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of EaseMob Technologies.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from EaseMob Technologies.
 */
package zhixin.cn.com.happyfarm_user.receiver;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

//import com.huawei.hms.support.api.push.PushReceiver;
import com.hyphenate.chat.EMClient;
import com.hyphenate.util.EMLog;

import zhixin.cn.com.happyfarm_user.utils.Config;

public class HMSPushReceiver {

}

//public class HMSPushReceiver extends PushReceiver{
//    private static final String TAG = HMSPushReceiver.class.getSimpleName();
//
//    @Override
//    public void onToken(Context context, String token, Bundle extras){
//        //没有失败回调，假定token失败时token为null
//        if(token != null && !token.equals("")){
//            EMLog.d("HWHMSPush", "register huawei hms push token success token:" + token);
//            Config.setPUSHTOKEN("huaWeiPush_" + token);
//            Log.i("PushService", "HuaWeiPush_Token success : " + Config.getPUSHTOKEN());
//            EMClient.getInstance().sendHMSPushTokenToServer("100316239", token);
//        }else{
//            Log.i("PushService", "HuaWeiPush_Token fail : " + Config.getPUSHTOKEN());
//            EMLog.e("HWHMSPush", "register huawei hms push token fail!");
//        }
//    }
//}
