package zhixin.cn.com.happyfarm_user.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.LuckDrawRecordActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.SignIn;
import zhixin.cn.com.happyfarm_user.model.SignMonthRecord;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

/**
 * Created by Administrator on 2017/8/16.
 */

public class AdapterDate extends BaseAdapter {

    private Context context;
    private Dialog dialog;
    private List<Integer> days = new ArrayList<>();
    //日历数据
    private List<Boolean> status = new ArrayList<>();
    //签到状态，实际应用中初始化签到状态可通过该字段传递
    private OnSignedSuccess onSignedSuccess;
    //签到成功的回调方法，相应的可自行添加签到失败时的回调方法

    //改：添加一个已经签到过日期的的数组

    public AdapterDate(Context context) {
        this.context = context;
        int maxDay = Utils.getCurrentMonthLastDay();//获取当月天数
        for (int i = 0; i < Utils.getFirstDayOfMonth() - 1; i++) {
            //DateUtil.getFirstDayOfMonth()获取当月第一天是星期几，星期日是第一天，依次类推
            days.add(0);
            //0代表需要隐藏的item
            status.add(false);
            //false代表为签到状态
        }
        for (int i = 0; i < maxDay; i++) {
            days.add(i+1);
            status.add(false);
        }
    }

    public AdapterDate(Context context, List<SignMonthRecord.ResultBean.ResultBeanList> lists) {
        this.context = context;
        int maxDay = Utils.getCurrentMonthLastDay();//获取当月天数
        for (int i = 0; i < Utils.getFirstDayOfMonth() - 1; i++) {
            //DateUtil.getFirstDayOfMonth()获取当月第一天是星期几，星期日是第一天，依次类推
            days.add(0);
            //0代表需要隐藏的item
            status.add(false);
            //false代表为签到状态
        }

        for (int i = 0; i < maxDay; i++) {
            int i1 = 0;
            for (int j = 0 ; j < lists.size(); j++) {
                if (i+1 == lists.get(j).getFewDay()) {
                    days.add(i + 1);
                    //初始化日历数据
                    status.add(true);
                    //初始化日历签到状态
                    i1++;
                }
            }
            if(i1 != 1){
                days.add(i+1);
                //初始化日历数据
                status.add(false);
                //初始化日历签到状态

            }

        }
    }

    @Override
    public int getCount() {
        return days.size();
    }

    @Override
    public Object getItem(int i) {
        return days.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if(view==null){
            view = LayoutInflater.from(context).inflate(R.layout.item_gv,null);
            viewHolder = new ViewHolder();
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.tv = view.findViewById(R.id.tvWeek);
        viewHolder.rlItem = view.findViewById(R.id.rlItem);
        viewHolder.ivStatus = view.findViewById(R.id.ivStatus);
//        System.out.println("今"+Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == days.get(i)) {
            viewHolder.tv.setText("今");
        } else {
            viewHolder.tv.setText(days.get(i)+"");
        }
        if(days.get(i)==0) {
            viewHolder.rlItem.setVisibility(View.GONE);
        }
        if(status.get(i)){
            viewHolder.tv.setTextColor(Color.parseColor("#1DAC6D"));
            viewHolder.ivStatus.setVisibility(View.VISIBLE);
        }else{
            viewHolder.tv.setTextColor(Color.parseColor("#666666"));
            viewHolder.ivStatus.setVisibility(View.GONE);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == days.get(i)) {
                    if(status.get(i)){
                        Toast.makeText(context,"已经签到过了哟!", Toast.LENGTH_SHORT).show();
                    }else{
//                        signInClick(i);
                    }
                } else {
//                    Toast.makeText(context,"不能签到咯!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    class ViewHolder{
        RelativeLayout rlItem;
        TextView tv;
        ImageView ivStatus;
    }

    public void setOnSignedSuccess(OnSignedSuccess onSignedSuccess){
        this.onSignedSuccess = onSignedSuccess;
    }

    /**
     * 签到点击事件
     */
//    private void signInClick(int i) {
//        dialog = LoadingDialog.createLoadingDialog(context, "正在签到");
//        HttpHelper.initHttpHelper().updateIsSign().enqueue(new Callback<SignIn>() {
//            @Override
//            public void onResponse(Call<SignIn> call, Response<SignIn> response) {
//                if (null != response.body()){
//                    if (("success").equals(response.body().getFlag())){
//                        status.set(i,true);
//                        notifyDataSetChanged();
//                        if(onSignedSuccess!=null) {
//                            onSignedSuccess.OnSignedSuccess();
//                        }
//                        if (0 < response.body().getResult().getGiftList().size()) {
////                            Log.i("这里应该弹一个弹窗", "onResponse: ");
//                            signReward("签到时间达到7天，获得青菜一份.赶快去领取吧");
//                        } else {
//                            Toast.makeText(context,"签到成功!植信蛋+"+response.body().getResult().getEggs()+"个", Toast.LENGTH_SHORT).show();
//                        }
//                    }else {
//                        int result = response.body().getErrCode();
//                        if ( -103 == result) {
//                            Toast.makeText(context, "今日已签到！",Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, "签到失败",Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                } else {
//
//                }
//                LoadingDialog.closeDialog(dialog);
//            }
//
//            @Override
//            public void onFailure(Call<SignIn> call, Throwable t) {
//                Toast.makeText(context, TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
//                LoadingDialog.closeDialog(dialog);
//            }
//        });
//    }
    /**
     * 未登录弹框点击事件
     */

    private void signReward(String content){
        final AlertUtilBest diyDialog = new AlertUtilBest(context);
        diyDialog.setCancel("取消")
                .setOk("知道了")
                .setContent(content)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
//                        context.startActivity(new Intent(context, LuckDrawRecordActivity.class));
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

}
