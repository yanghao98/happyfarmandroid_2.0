package zhixin.cn.com.happyfarm_user.Page;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.EMCallBack;
import com.hyphenate.EMContactListener;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMCmdMessageBody;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.EaseUI;
import com.hyphenate.easeui.domain.EaseUser;
import com.hyphenate.util.EMLog;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import cc.solart.wave.WaveSideBarView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.PersonalMessageAdapter;
import zhixin.cn.com.happyfarm_user.MainActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
//import zhixin.cn.com.happyfarm_user.HMSPushHelper;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.easyMob.ChatActivity;
import zhixin.cn.com.happyfarm_user.easyMob.ConverActivity;
import zhixin.cn.com.happyfarm_user.easyMob.GroupsActivity;
import zhixin.cn.com.happyfarm_user.easyMob.NewFriendsMsgActivity;
import zhixin.cn.com.happyfarm_user.model.CusChat;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.PersonalMessage;
import zhixin.cn.com.happyfarm_user.model.SeachFriend;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.model.searchFriendDetail;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.OSUtils;
import zhixin.cn.com.happyfarm_user.other.PinyinComparatorPersonal;
import zhixin.cn.com.happyfarm_user.other.PinyinUtils;

import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.CustomToast;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by DELL on 2018/3/1.
 */

public class PersonalMessagePage extends Fragment {
    public static final String TAG = "PersonalMessagePage->";
    private InputMethodManager inputMethodManager;
    private PersonalMessageAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private RecyclerView recyclerView;
    private ImageButton conver;
    private List<PersonalMessage> list = new ArrayList<>();
    private RelativeLayout personalPrompt;
    protected ImageButton clearSearch;
    protected EditText query;
    private WaveSideBarView sideBar;
    private List<String> friendId = new ArrayList<>();
    private RefreshLayout refreshLayout;
    /**
     * 根据拼音来排列RecyclerView里面的数据类
     */
    private PinyinComparatorPersonal pinyinComparator;
    private Map<String, EaseUser> mEMFriendInfo = new HashMap<>();
    public List<Badge> userBadges = new ArrayList<>();
    private List<Badge> badge = new ArrayList<>();
    private Badge groupBadge;
    private Button messageLoginBtn;
    private RelativeLayout notLoginLayout;
    private RelativeLayout searchText;
    private Dialog mDialog;
    private RecyclerViewHeader header;
    private Activity activity;
    private int positionNum;
    private RelativeLayout messageNewFriend;//新的朋友
    private RelativeLayout messageGroup;//群聊
    private RelativeLayout messageCustomer;//客服
    private RelativeLayout personalMessageTitleBar;
    private ImageView backButton;

    private Vibrator vibrator;
    private View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: " + "初始化完成");
        View view = inflater.inflate(R.layout.personal_message_layout, null,false);
        myView = view;
        inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        TextView title = view.findViewById(R.id.title_name);
        title.setText("通讯录");
        //返回按钮需要特殊对待（添加晚了点击返回按钮会奔溃）
        backButton = view.findViewById(R.id.back_button);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(getActivity(), getResources().getColor(R.color.theme_color));
        //search
        initView(view);//初始化页面布局
//        setStatusBarHeight();
        EaseUI.getInstance().setUserProfileProvider(new EaseUI.EaseUserProfileProvider() {
            @Override
            public EaseUser getUser(String username) {
                Log.i("info", "用户信息: " + username);
                //TODO 这里用来填入用户的昵称和头像
                EaseUser user = Config.AllEMFriendInfo.get(username);
                //TODO 这里是设置自己的头像
                if (user != null && username.equals(EMClient.getInstance().getCurrentUser())) {
                    user.setAvatar(getHeadImg());
                    user.setNickname(username);
                }
                return user;
            }
        });
        setRefreshLayout();//设置刷新器
        initView();
        converBtn();
        topRefreshLayout();
        messageLogin();
        EMClient.getInstance().chatManager().addMessageListener(msgListener);
        //TODO 通过注册消息监听好友请求。
        EMClient.getInstance().contactManager().setContactListener(mContactListener);
        return view;
    }

    public void initView() {
        //消息角标
        userBadges.add(new QBadgeView(getContext())
                .bindTarget(myView.findViewById(R.id.conver_view_total))
                .setBadgeNumber(0)
                .setBadgeTextSize(5, true));
        System.out.println("角标组件个数0：" + userBadges.size());
        //添加好友角标
        badge.add(new QBadgeView(getContext())
                .bindTarget(myView.findViewById(R.id.new_friend_icon))
                .setBadgeNumber(0)
                .setBadgeTextSize(5, true));
        groupBadge = new QBadgeView(getContext()).bindTarget(myView.findViewById(R.id.group_icon)).setBadgeNumber(0).setBadgeTextSize(5, true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }
    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(activity);
        //相对布局使用方法
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        personalMessageTitleBar.setLayoutParams(params);
    }

    /**
     * 获取状态栏高度
     * @param context 上下文
     * @return int 状态栏高度值
     */
    protected static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    //TODO 为好友设置环信头像
    public void getEMuserInfo(){
        HttpHelper.initHttpHelper().seachFriend(0).enqueue(new Callback<SeachFriend>() {
            @Override
            public void onResponse(Call<SeachFriend> call, Response<SeachFriend> response) {
                if ("success".equals(response.body().getFlag())) {
                    for (SeachFriend.ResultBean.ListBean user : response.body().getResult().getList()) {
                        if (user.getUuid() != null) {
                            Log.i("info", user.getFriendHeadImg() + "  " + user.getFriendNickname());
                            EaseUser u = new EaseUser(user.getUuid().toString());
                            u.setAvatar(user.getFriendHeadImg());
                            u.setNickname(user.getFriendNickname());
                            Log.i("info", "  uuid = " + user.getToId() + "  " + user.getFriendId());
//                            mEMFriendInfo.put("" + user.getToId(), u);
                            if (!Config.AllEMFriendInfo.containsKey("" + user.getToId())) {
                                Config.AllEMFriendInfo.put("" + user.getToId(), u);
                            }
                        }
                    }
                    //TODO 这里主要是根据用户uuid 存放自己的id 好填入自己的头像
                    EaseUser u = new EaseUser(getUuId());
//                    mEMFriendInfo.put("" + getUserId(), u);
                    if (!Config.AllEMFriendInfo.containsKey("" + getUserId())) {
                        Config.AllEMFriendInfo.put("" + getUserId(), u);
                    }
                }
            }

            @Override
            public void onFailure(Call<SeachFriend> call, Throwable t) {

            }
        });
    }
    //TODO 身份信息
    public void userInfoTokenCus(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("failed").equals(response.body().getFlag())) {
                    noLoginDialog();//进入登录页
                } else {
                    customerAllot();
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }
    //TODO 客服分配接口
    public void customerAllot(){
        System.out.println("角标组件个数2：" + userBadges.size());
        Call<CusChat> cusAllot = HttpHelper.initHttpHelper().addCusChatList();
        cusAllot.enqueue(new Callback<CusChat>() {
            @Override
            public void onResponse(Call<CusChat> call, Response<CusChat> response) {
                if (("failed").equals(response.body().getFlag())) {
                    Toast.makeText(getContext(),response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                } else {
                    for (CusChat.ResultBean user : response.body().getResult()) {
                        if (null != user.getCusUuid()) {
                            EaseUser u = new EaseUser(user.getCusUuid().toString());
                            u.setAvatar("http://www.xtai.com/static/img/side_s01.gif");
                            u.setNickname("客服" + "（" + user.getCusServiceName() + "）");
                            System.out.println("客服" + "（" + user.getCusServiceName() + "）");
                            if (!Config.AllEMFriendInfo.containsKey("" + user.getEChatId())) {
                                Config.AllEMFriendInfo.put("" + user.getEChatId(), u);
                            }
                            Config.eChatId = String.valueOf(user.getEChatId());
                        }
                    }
                    //TODO 这里主要是根据用户uuid 存放自己的id 好填入自己的头像
                    EaseUser u = new EaseUser(getUuId());
                    if (!Config.AllEMFriendInfo.containsKey("" + getUserId())) {
                        Config.AllEMFriendInfo.put("" + getUserId(), u);
                    }
                    //TODO 进入聊天
                    Intent intent = new Intent(activity, ChatActivity.class);
                    intent.putExtra("customer","customer");
                    intent.putExtra(EaseConstant.EXTRA_USER_ID, String.valueOf(response.body().getResult().get(0).getEChatId()));
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<CusChat> call, Throwable t) {
                try {
                    if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                        Toast.makeText(getContext(), "客服暂时不在线", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Log.e(TAG, "onFailure: "+ e);
                }
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!list.isEmpty()){
            list.clear();
        }
        initRecyclerView(getView());
        AllFriend();
        getEMuserInfo();
        updateTotal();//更新消息角标
    }

    /**
     * 设置刷新器
     */
    private void setRefreshLayout(){
        refreshLayout.setEnableHeaderTranslationContent(false);
        //设置上拉加载
        refreshLayout.setEnableLoadMore(false);
        //设置下拉刷新
        refreshLayout.setEnableRefresh(false);
        //设置是否开启在加载时候禁止操作内容视图
        refreshLayout.setDisableContentWhenLoading(true);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()).setShowBezierWave(false));
        //设置 Footer 为 球脉冲 样式
//        refreshLayout.setRefreshFooter(new ClassicsFooter(getContext()).setSpinnerStyle(SpinnerStyle.Scale));
    }

    /**
     * 初始化页面数据
     * @param view
     */
    private void initView(View view){
        personalMessageTitleBar = view.findViewById(R.id.personal_message_title_bar);
        query = view.findViewById(com.hyphenate.easeui.R.id.query);
        clearSearch = view.findViewById(com.hyphenate.easeui.R.id.search_clear);
        personalPrompt = view.findViewById(R.id.personal_prompt);
        conver = view.findViewById(R.id.personal_btn_conver);
        messageLoginBtn = view.findViewById(R.id.message_login_btn);
        notLoginLayout = view.findViewById(R.id.not_login_layout);
        searchText = view.findViewById(R.id.message_search_bar);
        messageNewFriend = view.findViewById(R.id.message_new_friend2);
        messageGroup = view.findViewById(R.id.message_group2);
        messageCustomer = view.findViewById(R.id.message_customer2);
        refreshLayout = view.findViewById(R.id.refreshLayout);

    }

    /**
     * 每次来到页面更新角标
     */
    private void updateTotal(){
        //设置消息角标
        try {
            setConverBadge(EMClient.getInstance().chatManager().getUnreadMessageCount());
//            Log.i("打印消息数量", "数量" + EMClient.getInstance().chatManager().getUnreadMessageCount());
        } catch (Exception e) {

        }

        //设置好友添加角标
        try {
            int total = Integer.parseInt(activity.getSharedPreferences("User_data",MODE_PRIVATE).getString("badge",""));
            for (Badge badge : badge) {
                badge.setBadgeNumber(total);
            }
//            Log.i("打印消息数量", "数量" + total);
        } catch (Exception e) {

        }

        //设置组群角标
//        try {
//            int total = Integer.parseInt(activity.getSharedPreferences("User_data",MODE_PRIVATE).getString("groupBadge",""));
//            groupBadge.setBadgeNumber(total);
//            Log.i("打印消息数量", "数量" + total);
//        } catch (Exception e) {
//
//        }
    }

    //TODO 登录
    private void messageLogin() {
        messageLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }
    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog =new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    //TODO 查询自己所有好友
    public void AllFriend() {
        HttpHelper.initHttpHelper().seachFriend(0).enqueue(new Callback<SeachFriend>() {
            @Override
            public void onResponse(Call<SeachFriend> call, Response<SeachFriend> response) {
//                Log.e("好友：", JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())) {
                    LoadingDialog.closeDialog(mDialog);
                    //String.valueOf(response.body().getResult().getList()).equals("[]")
                    if (response.body().getResult().getList().isEmpty()) {
                        personalPrompt.setVisibility(View.INVISIBLE);
//                        recyclerView.setVisibility(View.INVISIBLE);
                        searchText.setVisibility(View.VISIBLE);
                        conver.setVisibility(View.VISIBLE);
                        sideBar.setVisibility(View.INVISIBLE);
                        messageNewFriend.setVisibility(View.VISIBLE);
                        messageGroup.setVisibility(View.VISIBLE);
                        messageCustomer.setVisibility(View.VISIBLE);
                    } else {
                        personalPrompt.setVisibility(View.INVISIBLE);
                        searchText.setVisibility(View.VISIBLE);
                        conver.setVisibility(View.VISIBLE);
                        sideBar.setVisibility(View.VISIBLE);
                        messageNewFriend.setVisibility(View.VISIBLE);
                        messageGroup.setVisibility(View.VISIBLE);
                        messageCustomer.setVisibility(View.VISIBLE);

                        recyclerView.setVisibility(View.VISIBLE);
                        String sortString = null;
                        for (SeachFriend.ResultBean.ListBean resultBean: response.body().getResult().getList()) {
                            PersonalMessage dataBean = new PersonalMessage();
                            dataBean.setPersonalImg(resultBean.getFriendHeadImg());
                            dataBean.setPersonalName(resultBean.getFriendNickname());
                            friendId.add(String.valueOf(resultBean.getToId()));
                            dataBean.setPersonalId(String.valueOf(resultBean.getToId()));
//                            dataBean.contactId = String.valueOf(resultBean.getToId());
                            //汉字转换成拼音
                            String pinyin = PinyinUtils.getPingYin(resultBean.getFriendNickname());
                            //TODO 判断是否为空，否则无法截取拼音
                            if ("".equals(pinyin) || pinyin == null){
                                sortString = "#";
                            }else {
                                sortString = pinyin.substring(0, 1).toUpperCase();
                            }
                            // 正则表达式，判断首字母是否是英文字母
                            if (sortString.matches("[A-Z]")) {
                                dataBean.setLetters(sortString.toUpperCase());
                            } else {
                                dataBean.setLetters("#");
                            }
                            list.add(dataBean);
                            // 根据a-z进行排序源数据
                            Collections.sort(list, pinyinComparator);
                        }
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(getContext(), response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                    personalPrompt.setVisibility(View.INVISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<SeachFriend> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                personalPrompt.setVisibility(View.INVISIBLE);
                recyclerView.setVisibility(View.INVISIBLE);
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                mDialog = LoadingDialog.createLoadingDialog(getContext(), "");
                if (!list.isEmpty())
                    list.clear();
                AllFriend();
                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }

    //TODO 跳转会话页
    private void converBtn() {
        conver.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                //TODO 判断当前是否有账号登录
                if (!EMClient.getInstance().isLoggedInBefore() || "isOneLogin".equals(Config.isOneLogin)){
                    Config.isOneLogin = "noOneLogin";
//                LoginActivity.loginEM();
                    loginEM(5);
                }else {
                    converCustomerAllot();
                    startActivity(new Intent(activity, ConverActivity.class));
                }
            }
        });
    }
    //TODO 直接点击会话页 分配客服
    public void converCustomerAllot(){
        Call<CusChat> cusAllot = HttpHelper.initHttpHelper().addCusChatList();
        cusAllot.enqueue(new Callback<CusChat>() {
            @Override
            public void onResponse(Call<CusChat> call, Response<CusChat> response) {
                if (("failed").equals(response.body().getFlag())) {
                    Toast.makeText(getContext(),response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                } else {
                    for (CusChat.ResultBean user : response.body().getResult()) {
                        if (null != user.getCusUuid()) {
                            EaseUser u = new EaseUser(user.getCusUuid().toString());
                            u.setAvatar("http://www.xtai.com/static/img/side_s01.gif");
                            u.setNickname("客服" + "（" + user.getCusServiceName() + "）");
                            if (!Config.AllEMFriendInfo.containsKey("" + user.getEChatId())) {
                                Config.AllEMFriendInfo.put("" + user.getEChatId(), u);
                            }
                            Config.eChatId = String.valueOf(user.getEChatId());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CusChat> call, Throwable t) {
                Log.e(TAG, "onFailure: "+ t);
                try {
                    if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                        //Toast.makeText(getContext(), "客服分配失败", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Log.e(TAG, "onFailure: "+ e);
                }
            }
        });
    }
    @SuppressLint("ClickableViewAccessibility")//可能会和点击事件发生冲突
    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.personal_recyclerView);
        header = view.findViewById(R.id.RecyclerViewHeader);//头部视图
        pinyinComparator = new PinyinComparatorPersonal();
        sideBar = view.findViewById(R.id.side_view);
        sideBar.setLetters(Arrays.asList(getResources().getStringArray(R.array.waveSideBarLetter)));

        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new PersonalMessageAdapter(getContext(), list);
        //输入框监听
        query.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
                filterData(s.toString());
                if (s.length() > 0) {
                    clearSearch.setVisibility(View.VISIBLE);
                } else {
                    clearSearch.setVisibility(View.INVISIBLE);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        //输入框清除监听
        clearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query.getText().clear();
                hideSoftKeyboard();
            }
        });
        //TODO 进入聊天页面
        adapter.setItemClickListener(new PersonalMessageAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //TODO 判断当前是否有账号登录
                if (!EMClient.getInstance().isLoggedInBefore() || "isOneLogin".equals(Config.isOneLogin)){
                    Config.isOneLogin = "noOneLogin";
//                    LoginActivity.loginEM();
                    positionNum = position;
                    loginEM(4);
                } else {
                    //TODO 预防新加好友出现名字显示uuid
                    friendsDetails(position);
                }
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard();
                return false;
            }
        });
        //设置右侧SideBar触摸监听
        sideBar.setOnTouchLetterChangeListener(new WaveSideBarView.OnTouchLetterChangeListener() {
            @Override
            public void onLetterChange(String letter) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(letter.charAt(0));
                if (position != -1) {
                    gridLayoutManager.scrollToPositionWithOffset(position, 0);
                }
                refreshLayout.setEnableRefresh(false);
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    //屏幕中最后一个可见子项的position
                    int lastVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                    if (lastVisibleItemPosition > 0)
                        refreshLayout.setEnableRefresh(false);
                    else
                        refreshLayout.setEnableRefresh(true);
//                    Log.i("info", "newState= " + lastVisibleItemPosition);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
        //TODO 新的朋友
        messageNewFriend.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                //TODO 新的朋友点击
                for (Badge badge : badge) {
                    badge.setBadgeNumber(0);
                }
                saveBadge("0");
                //TODO 判断当前是否有账号登录
                if (!EMClient.getInstance().isLoggedInBefore() || "isOneLogin".equals(Config.isOneLogin)){
                    Config.isOneLogin = "noOneLogin";
//                    LoginActivity.loginEM();
                    loginEM(1);
                }else{
                    Intent intent = new Intent(activity, NewFriendsMsgActivity.class);
                    startActivity(intent);
                }
            }
        });
        //TODO 群组
        messageGroup.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
//                setGroupNumber(0);
                saveGroupBadge("0");
                //TODO 判断当前是否有账号登录
                if (!EMClient.getInstance().isLoggedInBefore() || "isOneLogin".equals(Config.isOneLogin)){
                    Config.isOneLogin = "noOneLogin";
//                    LoginActivity.loginEM();
                    loginEM(2);
                }else
                    startActivity(new Intent(activity, GroupsActivity.class));
            }
        });
        //TODO 客服
        messageCustomer.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                //TODO 判断当前是否有账号登录
                if (!EMClient.getInstance().isLoggedInBefore() || "isOneLogin".equals(Config.isOneLogin)){
                    Config.isOneLogin = "noOneLogin";
                    loginEM(3);
                }else {
                    userInfoTokenCus();
                }
            }
        });
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
        header.attachTo(recyclerView);

        //返回主页面
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("返回主页面");
                Intent  intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * 根据输入框中的值来过滤数据并更新RecyclerView
     *
     * @param filterStr
     */
    private void filterData(String filterStr) {
        List<PersonalMessage> filterDateList = new ArrayList<>();

        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = list;
        } else {
            filterDateList.clear();
            for (PersonalMessage personalMessage : list) {
                String name = personalMessage.getPersonalName();
                if (name.indexOf(filterStr.toString()) != -1 ||
                        PinyinUtils.getFirstSpell(name).startsWith(filterStr)
                        //不区分大小写
                        || PinyinUtils.getFirstSpell(name).toLowerCase().startsWith(filterStr)
                        || PinyinUtils.getFirstSpell(name).toUpperCase().startsWith(filterStr)
                        ) {
                    filterDateList.add(personalMessage);
                }
            }
        }
        // 根据a-z进行排序
        Collections.sort(filterDateList, pinyinComparator);
        adapter.updateList(filterDateList);
    }

    //隐藏键盘
    protected void hideSoftKeyboard() {
        if (activity.getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
            if (activity.getCurrentFocus() != null)
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    //TODO 获得用户头像
    private String getHeadImg(){

        return activity.getSharedPreferences("User_data", MODE_PRIVATE).getString("userImg", "");
    }

    //TODO 获得用户uuid
    private String getUuId() {
        return activity.getSharedPreferences("User_data", MODE_PRIVATE).getString("uuid", "");
    }

    //TODO 获得用户id
    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", "").equals("")) {
            userId = 0;
            return userId;
        } else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", ""));
        }
        return userId;
    }

    /**
     * 设置有好友添加时显示小红点
     * @param total 数量
     */
    public void setNewFriendNumber(int total) {
        for (Badge badge : badge) {
            badge.setBadgeNumber(total);
        }
    }

    /**
     * 设置群组消息红点
     * @param total 数量
     */
    public void setGroupNumber(int total) {
        groupBadge.setBadgeNumber(total);
    }

    //TODO 查询好友信息添加头像昵称
    public void friendsDetails(int userId) {
        HttpHelper.initHttpHelper().searchFriendDetail(userId).enqueue(new Callback<searchFriendDetail>() {
            @Override
            public void onResponse(Call<searchFriendDetail> call, Response<searchFriendDetail> response) {
//                Log.e("responsess",JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())) {
                    if (null != response.body().getResult()) {
                        EaseUser u = new EaseUser(response.body().getResult().get(0).getUuid());
                        u.setAvatar(response.body().getResult().get(0).getHeadImg());
//                        u.setAvatar("http://img1.imgtn.bdimg.com/it/u=580080393,299327986&fm=27&gp=0.jpg");
                        Log.e("UserImage", response.body().getResult().get(0).getHeadImg());
                        u.setNickname(response.body().getResult().get(0).getNickname());
                        Config.AllEMFriendInfo.put("" + response.body().getResult().get(0).getId(), u);
//                        if (!Config.AllEMFriendInfo.containsKey("" + response.body().getResult().get(0).getId())) {
//
//                            Log.i("环信设置头像", "onResponse: "+Config.AllEMFriendInfo);
//                        }
                    }
                    //TODO 这里主要是根据用户uuid 存放自己的id 好填入自己的头像
                    EaseUser u = new EaseUser(getUuId());
                    if (!Config.AllEMFriendInfo.containsKey("" + getUserId())) {
                        Config.AllEMFriendInfo.put("" + getUserId(), u);
                        Log.i("环信设置头像", "onResponse: "+Config.AllEMFriendInfo);
                    }
                    startActivity(new Intent(activity, ChatActivity.class).putExtra(EaseConstant.EXTRA_USER_ID, String.valueOf(userId)));
                } else if (("failed").equals(response.body().getFlag()))  {
                    Toast.makeText(getContext(), response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<searchFriendDetail> call, Throwable t) {
                t.printStackTrace();
                Log.i(TAG, "onFailure: "+ t.getMessage());
                Toast.makeText(activity, TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("切到其他页面", ">>>>>>>>>>>>>>>>>>>切到后台 activity process");
    }
    
    public void userInfoTokenTest() {
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("failed").equals(response.body().getFlag())) {
                    setViewShowOrGone(false);
                    if (Config.AllEMFriendInfo.size() != 0)
                        Config.AllEMFriendInfo.clear();//TODO 清空环信设置头像的数组内容，预防登录其他账号无法加载头像与昵称问题
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(activity, "User_data", strArr);
                    setConverBadge(0);
                } else {
                    setViewShowOrGone(true);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                setViewShowOrGone(false);
                setConverBadge(0);
            }
        });
    }

    /**
     * 设置会话按钮的图标数量
     * @param total 数量个数 角标数
     */
    public void setConverBadge(int total) {
        System.out.println(userBadges.size());
        //设置角标
        for (Badge badge : userBadges) {
            badge.setBadgeNumber(total);
        }
    }

    /**
     * 设置登录状态与未登录状态布局文件展示或隐藏
     * @param isLogin true 登录 false 未登录
     */
    private void setViewShowOrGone(boolean isLogin) {
        if (isLogin) {
            notLoginLayout.setVisibility(View.GONE);
            searchText.setVisibility(View.VISIBLE);
            conver.setVisibility(View.VISIBLE);
            sideBar.setVisibility(View.VISIBLE);
            messageNewFriend.setVisibility(View.VISIBLE);
            messageGroup.setVisibility(View.VISIBLE);
            messageCustomer.setVisibility(View.VISIBLE);
            return;
        }
        conver.setVisibility(View.INVISIBLE);
        sideBar.setVisibility(View.INVISIBLE);
        messageNewFriend.setVisibility(View.INVISIBLE);
        messageGroup.setVisibility(View.INVISIBLE);
        messageCustomer.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
        notLoginLayout.setVisibility(View.VISIBLE);
        searchText.setVisibility(View.GONE);
    }

    public void loginEM(int num){
//        loginEM("15170029037","e10adc3949ba59abbe56e057f20f883e");
        mDialog = LoadingDialog.createLoadingDialog(getContext(),"");
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (response.isSuccessful() && "success".equals(response.body().getFlag())){
                    GetSelfInfo.ResultBean resultBean = response.body().getResult();
                    loginEM(String.valueOf(resultBean.getId()),resultBean.getPassword(),num);
                } else {
                    LoadingDialog.closeDialog(mDialog);
                    CustomToast.showToast(getContext(),"请检查账号是否登录");
                }
            }

            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                CustomToast.showToast(getContext(),TextString.NetworkRequestFailed);
            }
        });
    }
    //login环信
    public void loginEM(String user, String pwd, int num){
        EMClient.getInstance().login(user,pwd,new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                Log.d("EMLogmain","登录环信成功");
                //TODO 以下两个方法是为了保证进入主页面后本地会话和群组都 load 完毕。
                EMClient.getInstance().chatManager().loadAllConversations();
                EMClient.getInstance().groupManager().loadAllGroups();
                // 获取华为 HMS 推送 token
//                HMSPushHelper.getInstance().connectHMS(activity);
//                HMSPushHelper.getInstance().getHMSPushToken();
                LoadingDialog.closeDialog(mDialog);
                switch (num){
                    case 1:
                        //新的朋友
                        Intent intent = new Intent(activity, NewFriendsMsgActivity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        //群组
                        startActivity(new Intent(activity, GroupsActivity.class));
                        break;
                    case 3:
                        //客服
                        userInfoTokenCus();
                        break;
                    case 4:
                        //好友
                        //TODO 预防新加好友出现名字显示uuid
                        friendsDetails(positionNum);
                        break;
                    case 5:
                        //会话
                        converCustomerAllot();
                        startActivity(new Intent(activity, ConverActivity.class));
                        break;
                }
            }

            @Override
            public void onProgress(int progress, String status) {
                Log.e(TAG, "onProgress: "+progress);
                Log.e(TAG, "status: "+status);
            }

            @Override
            public void onError(int code, String message) {
                Log.d("EMLogmain", "登录聊天服务器失败！"+code+"信息："+message);
                LoadingDialog.closeDialog(mDialog);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //此时已在主线程中，可以更新UI了
                        Toast.makeText(getContext(),"登录聊天服务器失败！",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
    //TODO 保存添加好友total
    public void saveBadge(String badge){
//        SharedPreferences.Editor editor = activity.getSharedPreferences("User_data",MODE_PRIVATE).edit();
//        editor.putString("badge",badge);
//        editor.apply();
        SharedPreferencesUtils.setParamString(activity, "User_data", "badge", badge);
    }

    /**
     * 保存群聊item角标
     * @param badge 角标数
     */
    public void saveGroupBadge(String badge) {
//        SharedPreferences.Editor editor = activity.getSharedPreferences("User_data", MODE_PRIVATE).edit();
//        editor.putString("groupBadge", badge);
//        editor.apply();
        SharedPreferencesUtils.setParamString(activity, "User_data", "groupBadge", badge);
    }

    public void saveEChatId(String EChatId){
//        SharedPreferences.Editor editor = activity.getSharedPreferences("User_data",MODE_PRIVATE).edit();
//        editor.putString("EChatId",EChatId);
//        editor.apply();
        SharedPreferencesUtils.setParamString(activity, "User_data", "EchatId", EChatId);
    }


    EMMessageListener msgListener = new EMMessageListener() {
        @Override
        public void onMessageReceived(List<EMMessage> messages) {
            //收到消息
//            Log.i("messages收到消息1", JSON.toJSONString(messages));
            String chatType = String.valueOf(messages.get(0).getChatType());
//            Log.i("messages收到消息类型1", chatType);
            if ("GroupChat".equals(chatType)) {
                //群聊提示图标
//                setGroupNumber(-1);
            }
            int total = EMClient.getInstance().chatManager().getUnreadMessageCount();
            setConverBadge(total);
            //TODO 收到消息重新获取未读数量  并判断是否再main页面
//            if (away == 3){
//            personalMessagePage.setBadgeNumber(total);
//            vipPage.getEMNumber(total);
//            }
            /*
             * 想设置震动大小可以通过改变pattern来设定，如果开启时间太短，震动效果可能感觉不到
             * */
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
            //收到透传消息
//            Log.i("收到透传消息", String.valueOf(messages));
            for (EMMessage message : messages) {
                EMLog.d("收到透传消息", "receive command message");
//                get message body
                EMCmdMessageBody cmdMsgBody = (EMCmdMessageBody) message.getBody();
                final String action = cmdMsgBody.action();//获取自定义action
                //获取扩展属性 此处省略
                //maybe you need get extension of your message
                //message.getStringAttribute("");
                EMLog.d("收到透传消息", String.format("Command：action:%s,message:%s", action, message.toString()));
            }
        }

        @Override
        public void onMessageRead(List<EMMessage> messages) {
            //收到已读回执
//            Log.i("收到已读回执", String.valueOf(messages));
        }

        @Override
        public void onMessageDelivered(List<EMMessage> message) {
            //收到已送达回执
//            Log.i("收到已送达回执", String.valueOf(message));
        }

        @Override
        public void onMessageRecalled(List<EMMessage> messages) {
            //消息被撤回
//            Log.i("消息被撤回", String.valueOf(messages));
        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
            //消息状态变动
//            Log.i("消息被撤回", String.valueOf(message));
        }
    };
    private EMContactListener mContactListener = new EMContactListener() {
        @Override
        public void onContactInvited(String username, String reason) {
//            Log.i("想要添加您为好友:", username);
            //收到好友邀请
            setNewFriendNumber(-1);
            saveBadge("-1");
        }

        @Override
        public void onFriendRequestAccepted(String s) {

        }

        @Override
        public void onFriendRequestDeclined(String s) {
            //好友请求被拒绝
        }

        @Override
        public void onContactDeleted(String username) {
//            Log.i("想要删除您为好友:", "onContactDeleted: " + username);
            //被删除时回调此方法
            //删除和某个user会话，如果需要保留聊天记录，传false
            EMClient.getInstance().chatManager().deleteConversation(username, true);
        }

        @Override
        public void onContactAdded(String username) {
//            Log.i("想要增加您为好友:", "onContactAdded: " + username);
            //增加了联系人时回调此方法
        }
    };

}
