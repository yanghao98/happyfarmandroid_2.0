package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import retrofit2.Call;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.RechargeAdapter;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.RechargeList;
import zhixin.cn.com.happyfarm_user.model.SearchDiamondBag;

/**
 * Created by DELL on 2018/3/18.
 */

public class RechargeActivity extends ABaseActivity {

    private List<RechargeList> mDatas;
    private RechargeAdapter rechargeAdapter;
    private ListView listView;
    private String landNo;
    private int positions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recharge_lalyout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this,getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("余额充值");

        SharedPreferences pref = getSharedPreferences("User_data",MODE_PRIVATE);
        String token = pref.getString("token","");

        Intent intent = getIntent();
        landNo = intent.getStringExtra("landNo");

        listView = findViewById(R.id.recharge_list);
//        Log.e("token",token);
       Call<SearchDiamondBag> searchDiamondBag = HttpHelper.initHttpHelper().searchDiamondBag();
       searchDiamondBag.enqueue(new Callback<SearchDiamondBag>() {
           @Override
           public void onResponse(Call<SearchDiamondBag> call, Response<SearchDiamondBag> response) {
               mDatas = new ArrayList<RechargeList>();
               if (response.body().getResult().getList()!=null && !response.body().getResult().getList().isEmpty()){
                   for (int i = 0;i<response.body().getResult().getList().size();i++){
                       RechargeList rechargeList = new RechargeList(JSON.toJSONString(response.body().getResult().getList().get(i).getMoney()),
                               JSON.toJSONString(response.body().getResult().getList().get(i).getDiamondNum()));
                       mDatas.add(rechargeList);
                       initData();
                   }
               }
           }

           @Override
           public void onFailure(Call<SearchDiamondBag> call, Throwable t) {

               Toast.makeText(RechargeActivity.this,"套餐获取失败",Toast.LENGTH_SHORT).show();

           }
       });

        findViewById(R.id.recharge_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (!rechargeAdapter.getStates(positions)){
                        Toast.makeText(RechargeActivity.this,"请选择套餐",Toast.LENGTH_SHORT).show();
                    }else {
                        finish();
                        Intent intent = new Intent(RechargeActivity.this,PaymentMethodActivity.class);
                        intent.putExtra("state","1");
                        intent.putExtra("Money",mDatas.get(positions).getRechargeMoney());
                        intent.putExtra("Name",mDatas.get(positions).getRechargeDiamonds());
                        intent.putExtra("landNo",landNo);
                        startActivity(intent);
                    }

                }catch (NullPointerException e){
                    Toast.makeText(RechargeActivity.this,"当前无套餐",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void initData(){
        rechargeAdapter = new RechargeAdapter(this,mDatas);
        listView.setAdapter(rechargeAdapter);
        listView.setVisibility(View.VISIBLE);
        setListViewHeightBasedOnChildren(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                transfer(view,i);
            }
        });

    }

    private void transfer(View view,int position) {
        RadioButton mRB = view.findViewById(R.id.recharge_rbt);
        rechargeAdapter.clearStates(position);
        positions = position;
//        Log.e("CZ","position:"+position+",states:"+rechargeAdapter.getStates(position));
        mRB.setChecked(true);
        mRB.setChecked(rechargeAdapter.getStates(position));
//        mRB.setText(mDatas.get(position).getMoney());
//        Log.e("bool",Boolean.getBoolean(rechargeAdapter.states.toString())+"");
        rechargeAdapter.notifyDataSetChanged();

    }

    public void setListViewHeightBasedOnChildren(ListView listView) {

        Adapter listAdapter = listView.getAdapter();

        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        int viewCount = listAdapter.getCount();
        for (int i = 0; i < viewCount; i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();

        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount()-1)) + 10;//加10是为了适配自定义背景

        listView.setLayoutParams(params);
    }

//    public void disableRadioGroup(RadioGroup testRadioGroup) {
//        for (int i = 0; i < testRadioGroup.getChildCount(); i++) {
//            testRadioGroup.getChildAt(i).setEnabled(false);
//        }
//    }

    public void back(View view){
        finish();
    }

}
