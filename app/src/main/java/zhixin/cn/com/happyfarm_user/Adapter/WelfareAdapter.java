package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.LuckDrawRecord;

import static zhixin.cn.com.happyfarm_user.other.DateUtil.getStrTimeYear;

public class WelfareAdapter extends RecyclerView.Adapter<WelfareAdapter.StaggerViewHolder> {

    private Context myContext;
    private List<LuckDrawRecord.LuckDrawBean.LuckDrawList> luckDrawLists;
    private WelfareAdapter.ButtonInterface buttonInterface;

    public WelfareAdapter(Context context, List<LuckDrawRecord.LuckDrawBean.LuckDrawList>  listView) {
        myContext = context;
        luckDrawLists = listView;
    }

    @NonNull
    @Override
    public WelfareAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(myContext, R.layout.welfare_item,null);
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //立即使用点击事件
        staggerViewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.e("WelfareAdapter", "onClick: "+staggerViewHolder.getAdapterPosition()  );
                LuckDrawRecord.LuckDrawBean.LuckDrawList luckDrawListData = luckDrawLists.get(staggerViewHolder.getAdapterPosition());
                int luckDrawRecordId = luckDrawListData.getId();
                int luckyDrawType = luckDrawListData.getLuckyDrawType();
                if(buttonInterface!=null) {
//                  接口实例化后的而对象，调用重写后的方法
                    buttonInterface.onclick(v,staggerViewHolder.getAdapterPosition(),luckDrawRecordId,luckyDrawType);
                }
            }
        });
        return staggerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull StaggerViewHolder staggerViewHolder, int i) {
        LuckDrawRecord.LuckDrawBean.LuckDrawList dataBean = luckDrawLists.get(i);
        staggerViewHolder.setData(dataBean);
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if(luckDrawLists!=null&&luckDrawLists.size()>0){
            return luckDrawLists.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView1;
        private TextView textView2;
        private Button button;

        public StaggerViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.welfare_image);
            textView1 = itemView.findViewById(R.id.luck_draw_content);
            textView2 = itemView.findViewById(R.id.dut_time_content);
            button= itemView.findViewById(R.id.luck_draw_button);

        }

        public void setData(LuckDrawRecord.LuckDrawBean.LuckDrawList  luckDrawList) {
//            Log.i("待使用奖品信息", "setData: " + JSON.toJSONString(luckDrawList));
            Glide.with(myContext)
                    .load(luckDrawList.getImageUrl())
                    .into(imageView);
            if (1 == luckDrawList.getLuckyDrawType()) {
                textView1.setText("租地享受" + luckDrawList.getLuckyDrawContent()+"优惠");
            } else {
                textView1.setText(luckDrawList.getLuckyDrawContent()+ "兑换卡一张");
            }
            if (4 == luckDrawList.getLuckyDrawType()) {
                button.setVisibility(View.GONE);
            } else {
                button.setVisibility(View.VISIBLE);
            }
            textView2.setText("到期时间：" + getStrTimeYear(luckDrawList.getDueTime()));
        }
    }

    /**
     *按钮点击事件需要的方法
     */
    public void buttonSetOnclick(WelfareAdapter.ButtonInterface buttonInterface){
        this.buttonInterface=buttonInterface;
    }

    /**
     * 按钮点击事件对应的接口
     */
    public interface ButtonInterface{
        public void onclick(View view, int position, int luckDrawRecordId, int luckyDrawType);
    }
}
