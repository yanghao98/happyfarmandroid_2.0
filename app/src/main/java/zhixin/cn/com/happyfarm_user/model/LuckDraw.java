package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class LuckDraw {
    private List<LuckDrawBean> result;
    private String flag;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<LuckDrawBean> getResult() {
        return result;
    }

    public void setResult(List<LuckDrawBean> result) {
        this.result = result;
    }

    public class LuckDrawBean {
        private int id;
        private int luckyDrawType;
        private String luckyDrawContent;
        private int managerId;
        private float discount;
        private float probability;
        private long startTime;
        private long updateTime;
        private int state;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLuckyDrawType() {
            return luckyDrawType;
        }

        public void setLuckyDrawType(int luckyDrawType) {
            this.luckyDrawType = luckyDrawType;
        }

        public String getLuckyDrawContent() {
            return luckyDrawContent;
        }

        public void setLuckyDrawContent(String luckyDrawContent) {
            this.luckyDrawContent = luckyDrawContent;
        }

        public int getManagerId() {
            return managerId;
        }

        public void setManagerId(int managerId) {
            this.managerId = managerId;
        }

        public float getDiscount() {
            return discount;
        }

        public void setDiscount(float discount) {
            this.discount = discount;
        }

        public float getProbability() {
            return probability;
        }

        public void setProbability(float probability) {
            this.probability = probability;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }
}
