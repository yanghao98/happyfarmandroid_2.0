package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.qiniu.android.utils.Json;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.searchOrderlogList;
import zhixin.cn.com.happyfarm_user.utils.RoundImageView;
import zhixin.cn.com.happyfarm_user.utils.Utils;

public class WaitingDeliveryAdapter extends RecyclerView.Adapter<WaitingDeliveryAdapter.StaggerViewHolder> {

    private Context myContext;
    private List<searchOrderlogList.ResultBean.ListBean> myList;
    private WaitingDeliveryAdapter.MyItemClickListener myItemClickListener;

    public WaitingDeliveryAdapter(Context context, List<searchOrderlogList.ResultBean.ListBean> list) {
        this.myContext = context;
        this.myList = list;
//        Log.i("进来了", "WaitingDeliveryAdapter: " + JSON.toJSONString(list));
    }


    @NonNull
    @Override
    public WaitingDeliveryAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(myContext,R.layout.waiting_delivery_item,null);

        return new WaitingDeliveryAdapter.StaggerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WaitingDeliveryAdapter.StaggerViewHolder staggerViewHolder, int i) {

        searchOrderlogList.ResultBean.ListBean listBean = myList.get(i);
        staggerViewHolder.setData(listBean);

        if (myItemClickListener != null)
        staggerViewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = staggerViewHolder.getLayoutPosition();
                myItemClickListener.onItemClick(view,position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return myList.size();
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private TextView orderTime;
        private RoundImageView imageView;
        private TextView textView1;
        private TextView textView2;
        private TextView textView3;
        private TextView textView4;
        private TextView allPrice;
        private TextView order_number;
        private TextView button;
        private TextView button2;

        public StaggerViewHolder(@NonNull View itemView) {
            super(itemView);
            orderTime = itemView.findViewById(R.id.order_time);
            imageView = itemView.findViewById(R.id.waiting_delivery_image);
            textView1 = itemView.findViewById(R.id.waiting_delivery_name);
            textView2 = itemView.findViewById(R.id.waiting_delivery_number);
            textView3 = itemView.findViewById(R.id.waiting_delivery_price);
            textView4 = itemView.findViewById(R.id.waiting_delivery_username);
            allPrice = itemView.findViewById(R.id.waiting_delivery_all_price);
            button = itemView.findViewById(R.id.waiting_delivery_button);
            button2 = itemView.findViewById(R.id.waiting_picking_button);
            order_number = itemView.findViewById(R.id.order_number);
        }

        public void setData(searchOrderlogList.ResultBean.ListBean data) {
            Log.i("查到的待发货订单", "setData: " + JSON.toJSONString(data));
            orderTime.setText("订单日期：" + Utils.getDateToString(data.getStartTime()));
            imageView.setImageURL(data.getOrderImg());
            textView1.setText("商品名：" + data.getCropName());
            textView2.setText("发货数量："+data.getClosing()+"份");
            textView3.setText(data.getPrice()+"元/份");
            textView4.setText("卖家："+data.getSellerNickname());
            allPrice.setText(data.getFinalPrice() + "元");
            order_number.setText("订单编号："+data.getOrderNum());
            if (0 == data.getState()) {
                button.setVisibility(View.GONE);
                button2.setVisibility(View.VISIBLE);
            } else if (1 == data.getState()) {
                button.setVisibility(View.VISIBLE);
                button2.setVisibility(View.GONE);
            }
        }
    }

    public interface MyItemClickListener {
        void onItemClick(View view,int position);
    }

    public void  setItemClickListener (WaitingDeliveryAdapter.MyItemClickListener myItemClickListener) {
        this.myItemClickListener = myItemClickListener;
    }


}
