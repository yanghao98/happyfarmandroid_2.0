package zhixin.cn.com.happyfarm_user;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.AlertUtilOneButton;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by DELL on 2018/3/18.
 */

public class ConfirmPlantActivity extends ABaseActivity {

    private int number = 1;
    private String num;
    private String cropName;
    private String plantingDiamond;
    private String plantingIntegral;
    private String cycle;
    private String detail;
    private String landId;
    private ImageView cropImg;
    TextView plant_number;
    //private int payType = 2;
    private int payType = 0;
    private ArrayList<Integer> sellNum = new ArrayList<>();
    private String id;
    private Dialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_plant_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText(getResources().getString(R.string.plant_title));

        ImageButton reduce = findViewById(R.id.reduce);
        ImageButton plus = findViewById(R.id.plus);
        plant_number = findViewById(R.id.plant_number);
        Button confirm_next = findViewById(R.id.confirm_next);
        TextView crop_name = findViewById(R.id.crop_name);
        TextView planting_diamond = findViewById(R.id.planting_diamond);
//        TextView planting_integral = findViewById(R.id.planting_integral);
        TextView cycles = findViewById(R.id.cycle);
        TextView details = findViewById(R.id.details);
        cropImg = findViewById(R.id.plant_image);

        Intent intent = getIntent();
        cropName = intent.getStringExtra("cropName");
        String corpImage = intent.getStringExtra("cropImg");
        if (NumUtil.checkNull(corpImage)){
            cropImg.setImageResource(R.drawable.maintain);
        }else {
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(corpImage)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .error(R.drawable.maintain)//如果请求图像无法加载，则可以使用一个错误绘制。
                    .into(cropImg);//into(ImageView targetImageView)：图片最终要展示的地方。
//        cropImg = intent.getExtras("cropImg");
        }
        plantingDiamond = intent.getStringExtra("plantingDiamond");
        plantingIntegral = intent.getStringExtra("plantingIntegral");
        cycle = intent.getStringExtra("cycle");
        detail = intent.getStringExtra("details");
        landId = intent.getStringExtra("landId");
        id = intent.getStringExtra("id");
//        Log.e("UserID",id);
//        Log.e("crop",cropName);
        crop_name.setText(cropName);
        planting_diamond.setText(plantingDiamond);
//        planting_integral.setText(plantingIntegral);
        cycles.setText(cycle);
        details.setText("营养价值" + detail);

        num = (String) plant_number.getText();
        number = Integer.parseInt(num);

        reduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (number > 1){
                    number = number - 1;
                    num = String.valueOf(number);
                    plant_number.setText(num);
                }

            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (number < 20){
                    number = number + 1;
                    num = String.valueOf(number);
                    plant_number.setText(num);
                }else {
                    showToastShort(ConfirmPlantActivity.this, getResources().getString(R.string.plant_max));
                }
            }
        });

        SharedPreferences pref = getSharedPreferences("User_data",MODE_PRIVATE);
        String token = pref.getString("token","");

        confirm_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int Id,cropNum,landid;
                Id = Integer.valueOf(id);
                landid = Integer.valueOf(landId);
                cropNum = Integer.valueOf(plant_number.getText().toString());
                if (payType == 2) {
                    showToastShort(ConfirmPlantActivity.this, getResources().getString(R.string.plant_pay));
                }else {
                    perfectInfoDialog();
                }
            }
        });

    }
    //确认金额弹框
    @SuppressLint("SetTextI18n")
    private void perfectInfoDialog() {
        final AlertUtilBest diyDialog =new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk("确认种植")
                //.setContent("您本次种植将消耗"+(Double.valueOf(plantingDiamond) * number)+" 元")
                .setContent("您本次种植将消耗"+(Double.valueOf(plantingDiamond) * number)+" 元")
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        confirmPlantNet();
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    public void confirmPlantNet(){
        mDialog = LoadingDialog.createLoadingDialog(ConfirmPlantActivity.this, "提交中...");
        int Id,cropNum,landid;
        Id = Integer.valueOf(id);
        landid = Integer.valueOf(landId);
        cropNum = Integer.valueOf(plant_number.getText().toString());
        String sellNums = String.valueOf(sellNum);
        Call<CheckSMS> addUserTask = HttpHelper.initHttpHelper().addUserTask(payType,1,0,null,landid,Id,cropNum);

        addUserTask.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                Log.e("response",JSON.toJSONString(response.body()));
                if ("null".equals(JSON.toJSONString(response.body()))){
                    NormalDialogOneBtn("操作异常，请稍后再试");
                }else {
                    if (("success").equals(response.body().getFlag())){
                        showToastShort(ConfirmPlantActivity.this, getResources().getString(R.string.plant_tasks_success));
                        finish();
                    }else {
                        showToastShort(ConfirmPlantActivity.this, response.body().getResult());
                    }
                }
                LoadingDialog.closeDialog(mDialog);
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                showToastShort(ConfirmPlantActivity.this, TextString.NetworkRequestFailed);
                LoadingDialog.closeDialog(mDialog);
            }
        });
    }
    //操作成功弹窗
    private void NormalDialogOneBtn(String content) {
        final AlertUtilOneButton diyDialog = new AlertUtilOneButton(this);
        diyDialog.setOk("我知道了")
                .setContent(content)
                .setDialogClickListener(new AlertUtilOneButton.DialogClickListener() {
                    @Override
                    public void ok() {
                        diyDialog.cancle();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    public void back(View view){
        finish();
    }

}
