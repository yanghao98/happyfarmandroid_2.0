package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.dou361.ijkplayer.widget.PlayStateParams;
import com.dou361.ijkplayer.widget.PlayerView;
import com.felipecsl.gifimageview.library.GifImageView;
import com.githang.statusbar.StatusBarCompat;
import com.kyleduo.switchbutton.SwitchButton;
import com.pili.pldroid.player.widget.PLVideoTextureView;

import java.io.IOException;
import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.onekeyshare.ShareContentCustomizeCallback;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.Default;
import zhixin.cn.com.happyfarm_user.other.GifDataDownloader;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.widget.MediaController;

public class AlbumDetailsActivity extends ABaseActivity {

    private String albumName;
    private String photoAbstracts;
    private String isPublic;
    private String videoUrl;
    private SwitchButton defaultAddressSwitch;
    private int id;
    private int isDefault;
    private String TAG = "AlbumDetailsActivity->";
    private EditText photoAbstract;
    private EditText album_name;
//    private GifImageView gifImageView;
    private Button albumSave;
    private RelativeLayout titleBarLayout;

    private PlayerView playerView;
    private Button shareButton;
    private Bitmap bitmap;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTransparent();
        setContentView(R.layout.album_details_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        initView();
        setStatusBarHeight();
        initData();
        defaultAddressSwitch();
        saveAlbum();
        shareVideo();
    }

    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(this);
        //相对布局使用方法
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
    }

    private void initView() {
        //设置导航条文字
        TextView title = findViewById(R.id.title_name);
        title.setText(getResources().getString(R.string.album_details_title));
        album_name = findViewById(R.id.album_name);
        photoAbstract = findViewById(R.id.photoAbstract);
        photoAbstract.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        photoAbstract.setGravity(Gravity.TOP);
        photoAbstract.setSingleLine(false);
        photoAbstract.setHorizontallyScrolling(false);
//        gifImageView = findViewById(R.id.album_gifImageView);
        defaultAddressSwitch = findViewById(R.id.album_switch);
        albumSave = findViewById(R.id.album_seave);
        titleBarLayout = findViewById(R.id.title);
        shareButton = findViewById(R.id.share_button);

    }

    private void initData() {
        Intent intent = getIntent();
        albumName = intent.getStringExtra("albumName");
        photoAbstracts = intent.getStringExtra("photoAbstract");
        isPublic = intent.getStringExtra("isPublic");
        videoUrl = intent.getStringExtra("videoUrl");
        id = Integer.valueOf(intent.getStringExtra("id"));
        if (albumName != null) {
            album_name.setText(albumName);
        }
        if (photoAbstracts != null) {
//            Log.e("photoAbstracts", photoAbstracts);
            photoAbstract.setText(photoAbstracts);
        }
//        Log.d(TAG, "initData: "+ videoUrl);
//        Log.d(TAG, "initData: "+album_name);
        if (videoUrl != null) {
            playerView = new PlayerView(this)
                    .setTitle(albumName)//视频名称
                    .setScaleType(PlayStateParams.fillparent)
                    .hideMenu(true)
                    .forbidTouch(false)
                    .setPlaySource(videoUrl);

            playerView.startPlay();
        }
//        Log.e(TAG, isPublic);
        if ("1".equals(isPublic)) {
            defaultAddressSwitch.setChecked(true);
            isDefault = 1;
        } else {
            defaultAddressSwitch.setChecked(false);
            isDefault = 0;
        }
    }

    /**
     * 保存
     */
    private void saveAlbum() {
        albumSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                albumName = album_name.getText().toString();
                photoAbstracts = photoAbstract.getText().toString();
                if (isDefault == 1) {
                    isPublic = "1";
                } else if (isDefault == 0) {
                    isPublic = "0";
                }
                HttpHelper.initHttpHelper().updatePhotoAlbum(id, albumName, photoAbstracts, isPublic).enqueue(new Callback<Default>() {
                    @Override
                    public void onResponse(Call<Default> call, Response<Default> response) {
//                        Log.i(TAG, "id: " + id + "    albumName:" + albumName + "  photoAbstracts:" + photoAbstracts + "   isPublic:" + isPublic);
                        if (("success").equals(response.body().getFlag())) {
                            Config.myAlbumRefresh = true;
                            showToastShort(AlbumDetailsActivity.this, getResources().getString(R.string.save_success));
                            finish();
                        } else {
                            showToastShort(AlbumDetailsActivity.this, getResources().getString(R.string.save_failed));
                            finish();
                        }
                    }
                    @Override
                    public void onFailure(Call<Default> call, Throwable t) {
                        showToastShort(AlbumDetailsActivity.this, TextString.NetworkRequestFailed);
                    }
                });
            }
        });
    }

    public void defaultAddressSwitch() {
        defaultAddressSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isDefault = 1;
                } else {
                    isDefault = 0;
                }
            }
        });
    }

    public void back(View view) {
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            playerView.stopPlay();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }



    //分享功能
    public void shareVideo () {
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showShare(videoUrl);
                Log.d(TAG, "onClick: " );
            }
        });
    }
    /**
     * onekeyshare分享调用九宫格方法
     */
    private void showShare(String videoUrl) {
        try {
//                Log.d("视频路径", "setData: " + data.videoUrl);
            MediaMetadataRetriever mmr=new MediaMetadataRetriever();
            //绑定资源
            mmr.setDataSource(videoUrl, new HashMap());
            //获取第一帧图像的bitmap对象
            bitmap=mmr.getFrameAtTime();
        } catch (RuntimeException e){
//                Log.e(TAG, "setData: "+"有以前的脏数据存在需要清除");

        }
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
        // title标题，印象笔记、邮箱、信息、微信、人人网、QQ和QQ空间使用
        oks.setTitle(albumName);
        // titleUrl是标题的网络链接，仅在Linked-in,QQ和QQ空间使用
        oks.setTitleUrl(videoUrl);
        // text是分享文本，所有平台都需要这个字段
        oks.setText("我在植信家园制作的相册，快来围观");
        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
        oks.setImageData(bitmap);
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl(videoUrl);
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment("我在植信家园制作的相册，快来围观");
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite("植信网");
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl(videoUrl);

// 启动分享GUI
        oks.show(this);
    }

//    private void showShare() {
//        OnekeyShare oks = new OnekeyShare();
//        //关闭sso授权
//        oks.disableSSOWhenAuthorize();
//        // title标题，印象笔记、邮箱、信息、微信、人人网、QQ和QQ空间使用
//        oks.setTitle("标题");
//        // titleUrl是标题的网络链接，仅在Linked-in,QQ和QQ空间使用
//        oks.setTitleUrl("http://sharesdk.cn");
//        // text是分享文本，所有平台都需要这个字段
//        oks.setText("我是分享文本");
//        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
//        oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
//        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
//        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
//        // url仅在微信（包括好友和朋友圈）中使用
//        oks.setUrl("http://sharesdk.cn");
//        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
//        oks.setComment("我是测试评论文本");
//        // site是分享此内容的网站名称，仅在QQ空间使用
//        oks.setSite("ShareSDK");
//        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
//        oks.setSiteUrl("http://sharesdk.cn");
//
//// 启动分享GUI
//        oks.show(this);
//    }
}