package zhixin.cn.com.happyfarm_user.model;

import com.google.gson.Gson;

import java.util.List;

public class SearchPlotManagements {

    /**
     * result : {"pageNum":1,"pageSize":27,"size":27,"orderBy":null,"startRow":0,"endRow":26,"total":27,"pages":1,"list":[{"id":3,"plotName":"335","plotSize":5000,"plotHousePrice":21000,"plotHouseholds":300,"chestNum":3,"chestUsedNum":1,"startTime":1517222615000,"addressName":"河南省 新乡市 辉县市 冀屯镇","updateTime":1523261528000,"managerId":6,"state":1,"provinceId":12,"cityId":12495,"areaId":12534,"territoryId":12537},{"id":4,"plotName":"滨湖","plotSize":1000000,"plotHousePrice":20000,"plotHouseholds":500,"chestNum":2,"chestUsedNum":0,"startTime":1516886147000,"addressName":"江西省 赣州市 信丰县 小河镇","updateTime":1522807960000,"managerId":4,"state":1,"provinceId":21,"cityId":25193,"areaId":25418,"territoryId":25422},{"id":7,"plotName":"欢乐颂","plotSize":1000,"plotHousePrice":26000,"plotHouseholds":80,"chestNum":34,"chestUsedNum":0,"startTime":1517213869000,"addressName":"江西省 吉安市 遂川县 滁洲乡","updateTime":1523496080000,"managerId":4,"state":1,"provinceId":21,"cityId":25521,"areaId":25648,"territoryId":25659},{"id":9,"plotName":"欢乐hgd","plotSize":1000,"plotHousePrice":15000,"plotHouseholds":80,"chestNum":2,"chestUsedNum":0,"startTime":1517223029000,"addressName":"江西省 南昌市 进贤县 二塘乡","updateTime":1522134292000,"managerId":1,"state":1,"provinceId":21,"cityId":26054,"areaId":26082,"territoryId":26084},{"id":11,"plotName":"312","plotSize":9600,"plotHousePrice":18000,"plotHouseholds":200,"chestNum":0,"chestUsedNum":0,"startTime":1517991396000,"addressName":"江西省 南昌市 进贤县 泉岭乡","updateTime":1522134344000,"managerId":6,"state":1,"provinceId":21,"cityId":26054,"areaId":26082,"territoryId":26085},{"id":12,"plotName":"3399","plotSize":8000,"plotHousePrice":8000,"plotHouseholds":230,"chestNum":0,"chestUsedNum":0,"startTime":1517991691000,"addressName":"安徽省 阜阳市 临泉县 城关镇","updateTime":1522134362000,"managerId":6,"state":1,"provinceId":14,"cityId":16490,"areaId":16539,"territoryId":16548},{"id":15,"plotName":"世界第一","plotSize":20000,"plotHousePrice":9800,"plotHouseholds":400,"chestNum":0,"chestUsedNum":0,"startTime":1517992329000,"addressName":"河南省 新乡市 辉县市 西平罗乡","updateTime":1522134378000,"managerId":6,"state":1,"provinceId":12,"cityId":12495,"areaId":12534,"territoryId":12547},{"id":16,"plotName":"王府","plotSize":2000,"plotHousePrice":5000,"plotHouseholds":60,"chestNum":1,"chestUsedNum":1,"startTime":1517993063000,"addressName":"河南省 三门峡市 卢氏县 范里镇","updateTime":1523261513000,"managerId":6,"state":1,"provinceId":12,"cityId":12310,"areaId":12338,"territoryId":12354},{"id":18,"plotName":"小圣贤庄","plotSize":100000,"plotHousePrice":12000,"plotHouseholds":666,"chestNum":33,"chestUsedNum":1,"startTime":1517996917000,"addressName":"河南省 三门峡市 卢氏县 瓦窖沟乡","updateTime":1523496085000,"managerId":6,"state":1,"provinceId":12,"cityId":12310,"areaId":12338,"territoryId":12345},{"id":19,"plotName":"上坝","plotSize":60000,"plotHousePrice":5000,"plotHouseholds":3000,"chestNum":200,"chestUsedNum":0,"startTime":1518092854000,"addressName":"甘肃省 陇南市 徽县 江洛镇","updateTime":1522134427000,"managerId":1,"state":1,"provinceId":11,"cityId":10162,"areaId":10205,"territoryId":10211},{"id":20,"plotName":"ASa","plotSize":0,"plotHousePrice":0,"plotHouseholds":0,"chestNum":5,"chestUsedNum":0,"startTime":1518166058000,"addressName":"甘肃省 陇南市 徽县 江洛镇","updateTime":1523170465000,"managerId":4,"state":1,"provinceId":11,"cityId":10162,"areaId":10205,"territoryId":10211},{"id":22,"plotName":"雪溪苑","plotSize":8000,"plotHousePrice":1200,"plotHouseholds":450,"chestNum":50,"chestUsedNum":0,"startTime":1518167791000,"addressName":"江苏省 无锡市 滨湖区","updateTime":1522136918000,"managerId":1,"state":1,"provinceId":22,"cityId":27772,"areaId":27773,"territoryId":null},{"id":29,"plotName":"1212","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":123,"chestUsedNum":11,"startTime":1523244742000,"addressName":"重庆市 重庆城区 巴南区","updateTime":1523268412000,"managerId":1,"state":1,"provinceId":2,"cityId":393,"areaId":394,"territoryId":null},{"id":30,"plotName":"123","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":123,"chestUsedNum":11,"startTime":1523423900000,"addressName":"广东省 中山市 南头镇","updateTime":1523423900000,"managerId":1,"state":1,"provinceId":4,"cityId":1608,"areaId":1609,"territoryId":0},{"id":31,"plotName":"11","plotSize":11,"plotHousePrice":11,"plotHouseholds":11,"chestNum":123,"chestUsedNum":1,"startTime":1523423923000,"addressName":"福建省 福州市 仓山区 临江街道","updateTime":1523423935000,"managerId":1,"state":1,"provinceId":5,"cityId":3491,"areaId":3492,"territoryId":3494},{"id":33,"plotName":"1231","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":11,"chestUsedNum":1,"startTime":1523424754000,"addressName":"广东省 潮州市 潮安区 古巷镇","updateTime":1523424754000,"managerId":1,"state":1,"provinceId":4,"cityId":1636,"areaId":1637,"territoryId":1638},{"id":34,"plotName":"瑞城","plotSize":2500,"plotHousePrice":1300,"plotHouseholds":2000,"chestNum":50,"chestUsedNum":20,"startTime":1523428860000,"addressName":"江苏省 无锡市 新吴区 坊前镇","updateTime":1523428860000,"managerId":1,"state":1,"provinceId":22,"cityId":27772,"areaId":27848,"territoryId":27852},{"id":36,"plotName":"啊啊123","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":123,"chestUsedNum":123,"startTime":1523496697000,"addressName":"江苏省 无锡市 新吴区 坊前镇","updateTime":1523496697000,"managerId":2,"state":1,"provinceId":22,"cityId":27772,"areaId":27848,"territoryId":27852},{"id":37,"plotName":"哈哈哈哈哈哈哈哈哈","plotSize":1,"plotHousePrice":1,"plotHouseholds":5,"chestNum":5,"chestUsedNum":5,"startTime":1523499025000,"addressName":"北京市 北京城区 昌平区 南口镇","updateTime":1523499025000,"managerId":1,"state":1,"provinceId":1,"cityId":35,"areaId":36,"territoryId":37},{"id":38,"plotName":"哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈","plotSize":5,"plotHousePrice":5,"plotHouseholds":5,"chestNum":5,"chestUsedNum":5,"startTime":1523499081000,"addressName":"重庆市 重庆城区 巴南区 跳石镇","updateTime":1523499081000,"managerId":1,"state":1,"provinceId":2,"cityId":393,"areaId":394,"territoryId":395},{"id":39,"plotName":"啊啊啊啊啊啊啊啊啊","plotSize":1,"plotHousePrice":1,"plotHouseholds":1,"chestNum":1,"chestUsedNum":1,"startTime":1523499117000,"addressName":"北京市 北京城区 朝阳区 常营回族乡","updateTime":1523499117000,"managerId":1,"state":1,"provinceId":1,"cityId":35,"areaId":57,"territoryId":58},{"id":41,"plotName":"888899999999","plotSize":999999,"plotHousePrice":877777,"plotHouseholds":999999,"chestNum":6,"chestUsedNum":5,"startTime":1523499335000,"addressName":"重庆市 重庆郊县 丰都县 江池镇","updateTime":1523499335000,"managerId":4,"state":1,"provinceId":2,"cityId":1095,"areaId":1149,"territoryId":1156},{"id":45,"plotName":"lallalalllala","plotSize":67886658000,"plotHousePrice":45463536,"plotHouseholds":23244,"chestNum":1214,"chestUsedNum":111,"startTime":1523499531000,"addressName":"贵州省 黔西南布依族苗族自治州 兴仁县 波阳镇","updateTime":1523499531000,"managerId":4,"state":1,"provinceId":9,"cityId":7058,"areaId":7129,"territoryId":7132},{"id":46,"plotName":"1234567890","plotSize":1234567940,"plotHousePrice":123456,"plotHouseholds":223356,"chestNum":123,"chestUsedNum":1,"startTime":1523499559000,"addressName":"重庆市 重庆郊县 梁平区 礼让镇","updateTime":1523499559000,"managerId":4,"state":1,"provinceId":2,"cityId":1095,"areaId":1223,"territoryId":1234},{"id":47,"plotName":"123456ui","plotSize":12345,"plotHousePrice":34566,"plotHouseholds":24465,"chestNum":3,"chestUsedNum":2,"startTime":1523499644000,"addressName":"山西省 长治市 屯留县 西贾乡","updateTime":1523499644000,"managerId":4,"state":1,"provinceId":7,"cityId":6318,"areaId":6414,"territoryId":6422},{"id":48,"plotName":"kkkkkkkkkk","plotSize":123456,"plotHousePrice":1234567898,"plotHouseholds":5,"chestNum":4,"chestUsedNum":4,"startTime":1523499682000,"addressName":"安徽省 滁州市 明光市 城关街道","updateTime":1523499682000,"managerId":4,"state":1,"provinceId":14,"cityId":16264,"areaId":16350,"territoryId":16358},{"id":50,"plotName":"121131","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":123,"chestUsedNum":11,"startTime":1523504524000,"addressName":"澳门特别行政区 花王堂区","updateTime":1523504524000,"managerId":1,"state":1,"provinceId":3,"cityId":1600,"areaId":0,"territoryId":0}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public static SearchPlotManagements objectFromData(String str) {

        return new Gson().fromJson(str, SearchPlotManagements.class);
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 27
         * size : 27
         * orderBy : null
         * startRow : 0
         * endRow : 26
         * total : 27
         * pages : 1
         * list : [{"id":3,"plotName":"335","plotSize":5000,"plotHousePrice":21000,"plotHouseholds":300,"chestNum":3,"chestUsedNum":1,"startTime":1517222615000,"addressName":"河南省 新乡市 辉县市 冀屯镇","updateTime":1523261528000,"managerId":6,"state":1,"provinceId":12,"cityId":12495,"areaId":12534,"territoryId":12537},{"id":4,"plotName":"滨湖","plotSize":1000000,"plotHousePrice":20000,"plotHouseholds":500,"chestNum":2,"chestUsedNum":0,"startTime":1516886147000,"addressName":"江西省 赣州市 信丰县 小河镇","updateTime":1522807960000,"managerId":4,"state":1,"provinceId":21,"cityId":25193,"areaId":25418,"territoryId":25422},{"id":7,"plotName":"欢乐颂","plotSize":1000,"plotHousePrice":26000,"plotHouseholds":80,"chestNum":34,"chestUsedNum":0,"startTime":1517213869000,"addressName":"江西省 吉安市 遂川县 滁洲乡","updateTime":1523496080000,"managerId":4,"state":1,"provinceId":21,"cityId":25521,"areaId":25648,"territoryId":25659},{"id":9,"plotName":"欢乐hgd","plotSize":1000,"plotHousePrice":15000,"plotHouseholds":80,"chestNum":2,"chestUsedNum":0,"startTime":1517223029000,"addressName":"江西省 南昌市 进贤县 二塘乡","updateTime":1522134292000,"managerId":1,"state":1,"provinceId":21,"cityId":26054,"areaId":26082,"territoryId":26084},{"id":11,"plotName":"312","plotSize":9600,"plotHousePrice":18000,"plotHouseholds":200,"chestNum":0,"chestUsedNum":0,"startTime":1517991396000,"addressName":"江西省 南昌市 进贤县 泉岭乡","updateTime":1522134344000,"managerId":6,"state":1,"provinceId":21,"cityId":26054,"areaId":26082,"territoryId":26085},{"id":12,"plotName":"3399","plotSize":8000,"plotHousePrice":8000,"plotHouseholds":230,"chestNum":0,"chestUsedNum":0,"startTime":1517991691000,"addressName":"安徽省 阜阳市 临泉县 城关镇","updateTime":1522134362000,"managerId":6,"state":1,"provinceId":14,"cityId":16490,"areaId":16539,"territoryId":16548},{"id":15,"plotName":"世界第一","plotSize":20000,"plotHousePrice":9800,"plotHouseholds":400,"chestNum":0,"chestUsedNum":0,"startTime":1517992329000,"addressName":"河南省 新乡市 辉县市 西平罗乡","updateTime":1522134378000,"managerId":6,"state":1,"provinceId":12,"cityId":12495,"areaId":12534,"territoryId":12547},{"id":16,"plotName":"王府","plotSize":2000,"plotHousePrice":5000,"plotHouseholds":60,"chestNum":1,"chestUsedNum":1,"startTime":1517993063000,"addressName":"河南省 三门峡市 卢氏县 范里镇","updateTime":1523261513000,"managerId":6,"state":1,"provinceId":12,"cityId":12310,"areaId":12338,"territoryId":12354},{"id":18,"plotName":"小圣贤庄","plotSize":100000,"plotHousePrice":12000,"plotHouseholds":666,"chestNum":33,"chestUsedNum":1,"startTime":1517996917000,"addressName":"河南省 三门峡市 卢氏县 瓦窖沟乡","updateTime":1523496085000,"managerId":6,"state":1,"provinceId":12,"cityId":12310,"areaId":12338,"territoryId":12345},{"id":19,"plotName":"上坝","plotSize":60000,"plotHousePrice":5000,"plotHouseholds":3000,"chestNum":200,"chestUsedNum":0,"startTime":1518092854000,"addressName":"甘肃省 陇南市 徽县 江洛镇","updateTime":1522134427000,"managerId":1,"state":1,"provinceId":11,"cityId":10162,"areaId":10205,"territoryId":10211},{"id":20,"plotName":"ASa","plotSize":0,"plotHousePrice":0,"plotHouseholds":0,"chestNum":5,"chestUsedNum":0,"startTime":1518166058000,"addressName":"甘肃省 陇南市 徽县 江洛镇","updateTime":1523170465000,"managerId":4,"state":1,"provinceId":11,"cityId":10162,"areaId":10205,"territoryId":10211},{"id":22,"plotName":"雪溪苑","plotSize":8000,"plotHousePrice":1200,"plotHouseholds":450,"chestNum":50,"chestUsedNum":0,"startTime":1518167791000,"addressName":"江苏省 无锡市 滨湖区","updateTime":1522136918000,"managerId":1,"state":1,"provinceId":22,"cityId":27772,"areaId":27773,"territoryId":null},{"id":29,"plotName":"1212","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":123,"chestUsedNum":11,"startTime":1523244742000,"addressName":"重庆市 重庆城区 巴南区","updateTime":1523268412000,"managerId":1,"state":1,"provinceId":2,"cityId":393,"areaId":394,"territoryId":null},{"id":30,"plotName":"123","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":123,"chestUsedNum":11,"startTime":1523423900000,"addressName":"广东省 中山市 南头镇","updateTime":1523423900000,"managerId":1,"state":1,"provinceId":4,"cityId":1608,"areaId":1609,"territoryId":0},{"id":31,"plotName":"11","plotSize":11,"plotHousePrice":11,"plotHouseholds":11,"chestNum":123,"chestUsedNum":1,"startTime":1523423923000,"addressName":"福建省 福州市 仓山区 临江街道","updateTime":1523423935000,"managerId":1,"state":1,"provinceId":5,"cityId":3491,"areaId":3492,"territoryId":3494},{"id":33,"plotName":"1231","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":11,"chestUsedNum":1,"startTime":1523424754000,"addressName":"广东省 潮州市 潮安区 古巷镇","updateTime":1523424754000,"managerId":1,"state":1,"provinceId":4,"cityId":1636,"areaId":1637,"territoryId":1638},{"id":34,"plotName":"瑞城","plotSize":2500,"plotHousePrice":1300,"plotHouseholds":2000,"chestNum":50,"chestUsedNum":20,"startTime":1523428860000,"addressName":"江苏省 无锡市 新吴区 坊前镇","updateTime":1523428860000,"managerId":1,"state":1,"provinceId":22,"cityId":27772,"areaId":27848,"territoryId":27852},{"id":36,"plotName":"啊啊123","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":123,"chestUsedNum":123,"startTime":1523496697000,"addressName":"江苏省 无锡市 新吴区 坊前镇","updateTime":1523496697000,"managerId":2,"state":1,"provinceId":22,"cityId":27772,"areaId":27848,"territoryId":27852},{"id":37,"plotName":"哈哈哈哈哈哈哈哈哈","plotSize":1,"plotHousePrice":1,"plotHouseholds":5,"chestNum":5,"chestUsedNum":5,"startTime":1523499025000,"addressName":"北京市 北京城区 昌平区 南口镇","updateTime":1523499025000,"managerId":1,"state":1,"provinceId":1,"cityId":35,"areaId":36,"territoryId":37},{"id":38,"plotName":"哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈","plotSize":5,"plotHousePrice":5,"plotHouseholds":5,"chestNum":5,"chestUsedNum":5,"startTime":1523499081000,"addressName":"重庆市 重庆城区 巴南区 跳石镇","updateTime":1523499081000,"managerId":1,"state":1,"provinceId":2,"cityId":393,"areaId":394,"territoryId":395},{"id":39,"plotName":"啊啊啊啊啊啊啊啊啊","plotSize":1,"plotHousePrice":1,"plotHouseholds":1,"chestNum":1,"chestUsedNum":1,"startTime":1523499117000,"addressName":"北京市 北京城区 朝阳区 常营回族乡","updateTime":1523499117000,"managerId":1,"state":1,"provinceId":1,"cityId":35,"areaId":57,"territoryId":58},{"id":41,"plotName":"888899999999","plotSize":999999,"plotHousePrice":877777,"plotHouseholds":999999,"chestNum":6,"chestUsedNum":5,"startTime":1523499335000,"addressName":"重庆市 重庆郊县 丰都县 江池镇","updateTime":1523499335000,"managerId":4,"state":1,"provinceId":2,"cityId":1095,"areaId":1149,"territoryId":1156},{"id":45,"plotName":"lallalalllala","plotSize":67886658000,"plotHousePrice":45463536,"plotHouseholds":23244,"chestNum":1214,"chestUsedNum":111,"startTime":1523499531000,"addressName":"贵州省 黔西南布依族苗族自治州 兴仁县 波阳镇","updateTime":1523499531000,"managerId":4,"state":1,"provinceId":9,"cityId":7058,"areaId":7129,"territoryId":7132},{"id":46,"plotName":"1234567890","plotSize":1234567940,"plotHousePrice":123456,"plotHouseholds":223356,"chestNum":123,"chestUsedNum":1,"startTime":1523499559000,"addressName":"重庆市 重庆郊县 梁平区 礼让镇","updateTime":1523499559000,"managerId":4,"state":1,"provinceId":2,"cityId":1095,"areaId":1223,"territoryId":1234},{"id":47,"plotName":"123456ui","plotSize":12345,"plotHousePrice":34566,"plotHouseholds":24465,"chestNum":3,"chestUsedNum":2,"startTime":1523499644000,"addressName":"山西省 长治市 屯留县 西贾乡","updateTime":1523499644000,"managerId":4,"state":1,"provinceId":7,"cityId":6318,"areaId":6414,"territoryId":6422},{"id":48,"plotName":"kkkkkkkkkk","plotSize":123456,"plotHousePrice":1234567898,"plotHouseholds":5,"chestNum":4,"chestUsedNum":4,"startTime":1523499682000,"addressName":"安徽省 滁州市 明光市 城关街道","updateTime":1523499682000,"managerId":4,"state":1,"provinceId":14,"cityId":16264,"areaId":16350,"territoryId":16358},{"id":50,"plotName":"121131","plotSize":123,"plotHousePrice":123,"plotHouseholds":123,"chestNum":123,"chestUsedNum":11,"startTime":1523504524000,"addressName":"澳门特别行政区 花王堂区","updateTime":1523504524000,"managerId":1,"state":1,"provinceId":3,"cityId":1600,"areaId":0,"territoryId":0}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public static ResultBean objectFromData(String str) {

            return new Gson().fromJson(str, ResultBean.class);
        }

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 3
             * plotName : 335
             * plotSize : 5000
             * plotHousePrice : 21000
             * plotHouseholds : 300
             * chestNum : 3
             * chestUsedNum : 1
             * startTime : 1517222615000
             * addressName : 河南省 新乡市 辉县市 冀屯镇
             * updateTime : 1523261528000
             * managerId : 6
             * state : 1
             * provinceId : 12
             * cityId : 12495
             * areaId : 12534
             * territoryId : 12537
             */

            private int id;
            private String plotName;
            private float plotSize;
            private int plotHousePrice;
            private int plotHouseholds;
            private int chestNum;
            private int chestUsedNum;
            private long startTime;
            private String addressName;
            private long updateTime;
            private int managerId;
            private int state;
            private int provinceId;
            private int cityId;
            private int areaId;
            private int territoryId;

            public static ListBean objectFromData(String str) {

                return new Gson().fromJson(str, ListBean.class);
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPlotName() {
                return plotName;
            }

            public void setPlotName(String plotName) {
                this.plotName = plotName;
            }

            public float getPlotSize() {
                return plotSize;
            }

            public void setPlotSize(float plotSize) {
                this.plotSize = plotSize;
            }

            public int getPlotHousePrice() {
                return plotHousePrice;
            }

            public void setPlotHousePrice(int plotHousePrice) {
                this.plotHousePrice = plotHousePrice;
            }

            public int getPlotHouseholds() {
                return plotHouseholds;
            }

            public void setPlotHouseholds(int plotHouseholds) {
                this.plotHouseholds = plotHouseholds;
            }

            public int getChestNum() {
                return chestNum;
            }

            public void setChestNum(int chestNum) {
                this.chestNum = chestNum;
            }

            public int getChestUsedNum() {
                return chestUsedNum;
            }

            public void setChestUsedNum(int chestUsedNum) {
                this.chestUsedNum = chestUsedNum;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public String getAddressName() {
                return addressName;
            }

            public void setAddressName(String addressName) {
                this.addressName = addressName;
            }

            public long getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(long updateTime) {
                this.updateTime = updateTime;
            }

            public int getManagerId() {
                return managerId;
            }

            public void setManagerId(int managerId) {
                this.managerId = managerId;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public int getProvinceId() {
                return provinceId;
            }

            public void setProvinceId(int provinceId) {
                this.provinceId = provinceId;
            }

            public int getCityId() {
                return cityId;
            }

            public void setCityId(int cityId) {
                this.cityId = cityId;
            }

            public int getAreaId() {
                return areaId;
            }

            public void setAreaId(int areaId) {
                this.areaId = areaId;
            }

            public int getTerritoryId() {
                return territoryId;
            }

            public void setTerritoryId(int territoryId) {
                this.territoryId = territoryId;
            }
        }
    }
}
