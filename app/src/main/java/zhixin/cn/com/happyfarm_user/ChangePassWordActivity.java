package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import org.apaches.commons.codec.binary.Base64;

import java.security.PublicKey;

import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.PublicKeyList;
import zhixin.cn.com.happyfarm_user.other.AlertUtilOneButton;
import zhixin.cn.com.happyfarm_user.other.RSAUtil;
import zhixin.cn.com.happyfarm_user.other.TextChangedListener;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by HuYueling on 2018/4/27.
 */

public class ChangePassWordActivity extends ABaseActivity {
    private Button changePwdBtn;
    private EditText originalPwd;
    private EditText changePwdOne;
    private EditText changePwdTwo;
    private Button confirmPwdBtn;
    private Dialog mDialog;
    private String userTel;
    private String psw_encryption;
    private String publicKey1;
    private String password;
    private byte[] password2;
    private String oldPassword;
    private byte[] oldPassword2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        TextView title = findViewById(R.id.title_name);
        title.setText(getResources().getString(R.string.change_pwd_title));

        originalPwd = findViewById(R.id.change_password1);
        changePwdOne = findViewById(R.id.change_password2);
        changePwdTwo = findViewById(R.id.change_password3);
        changePwdBtn = findViewById(R.id.change_pwd_btn);
        confirmPwdBtn = findViewById(R.id.change_password_determine);

        Intent intent = getIntent();
        userTel = intent.getStringExtra("tel");


        confirmPwdBtn();

        changePwdBtn();

        //点击空白处退出键盘
        findViewById(R.id.changePwdBackView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        //限制密码长度
        TextChangedListener textChangedListener = new TextChangedListener(16,originalPwd,ChangePassWordActivity.this,getResources().getString(R.string.change_pwd_length));
        originalPwd.addTextChangedListener(textChangedListener.getmTextWatcher());
        //限制密码长度
        TextChangedListener textChangedListener2 = new TextChangedListener(16,changePwdOne,ChangePassWordActivity.this,getResources().getString(R.string.change_pwd_length));
        changePwdOne.addTextChangedListener(textChangedListener2.getmTextWatcher());
        //限制密码长度
        TextChangedListener textChangedListener3 = new TextChangedListener(16,changePwdTwo,ChangePassWordActivity.this,getResources().getString(R.string.change_pwd_length));
        changePwdTwo.addTextChangedListener(textChangedListener3.getmTextWatcher());
    }
    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm =  (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }
    public void changePwdBtn() {
        changePwdBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(ChangePassWordActivity.this, ForgotPasswordActivity.class));
            }
        });
    }

    public void confirmPwdBtn() {
        confirmPwdBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if ("".equals(originalPwd.getText().toString())) {
                    showToastShort(ChangePassWordActivity.this, getResources().getString(R.string.change_pwd_old));
                } else if ("".equals(changePwdOne.getText().toString())) {
                    showToastShort(ChangePassWordActivity.this, getResources().getString(R.string.change_pwd_new));
                } else if ("".equals(changePwdTwo.getText().toString())) {
                    showToastShort(ChangePassWordActivity.this, getResources().getString(R.string.change_pwd_new2));
                } else if (!(changePwdOne.getText().toString()).equals(changePwdTwo.getText().toString())) {
                    showToastShort(ChangePassWordActivity.this, getResources().getString(R.string.change_pwd_disaccord));
                } else {
                    oldPassword = originalPwd.getText().toString();
                    oldPassword2 = oldPassword.getBytes();
                    password = changePwdTwo.getText().toString();
                    password2 = password.getBytes();
                    mDialog = LoadingDialog.createLoadingDialog(ChangePassWordActivity.this, "修改中...");
                    retrofit2.Call<PublicKeyList> publicKey = HttpHelper.initHttpHelper().publicKey(userTel);
                    publicKey.enqueue(new Callback<PublicKeyList>() {
                        @Override
                        public void onResponse(retrofit2.Call<PublicKeyList> call, Response<PublicKeyList> response) {
//                            Log.i("info", response.body().getResult());
                            publicKey1 = response.body().getResult();
                            PublicKey publicKey = null;

                            try {
                                publicKey = RSAUtil.getPublicKey(publicKey1);
                                oldPassword2 = RSAUtil.encrypt(oldPassword2, publicKey);
                                oldPassword = Base64.encodeBase64String(oldPassword2);
                                password2 = RSAUtil.encrypt(password2, publicKey);
                                password = Base64.encodeBase64String(password2);
//                                                mDialog = LoadingDialog.createLoadingDialog(RegisterActivity.this,null);
                                HttpHelper.initHttpHelper().updatePassword(password, oldPassword).enqueue(new Callback<CheckSMS>() {
                                    @Override
                                    public void onResponse(retrofit2.Call<CheckSMS> call, Response<CheckSMS> response) {
//                                        Log.e("responses", JSON.toJSONString(response.body()));
                                        if (("success").equals(response.body().getFlag())) {
                                            LoadingDialog.closeDialog(mDialog);
//                                            showToastShort(ChangePassWordActivity.this, getResources().getString(R.string.change_pwd_success));
//                                            finish();
                                            normalDialogOneBtn(getResources().getString(R.string.change_pwd_success));
                                        } else {
                                            LoadingDialog.closeDialog(mDialog);
                                            showToastShort(ChangePassWordActivity.this, response.body().getResult());
                                        }
                                    }

                                    @Override
                                    public void onFailure(retrofit2.Call<CheckSMS> call, Throwable t) {
                                        LoadingDialog.closeDialog(mDialog);
                                        showToastShort(ChangePassWordActivity.this, TextString.NetworkRequestFailed);
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(retrofit2.Call<PublicKeyList> call, Throwable t) {
                            LoadingDialog.closeDialog(mDialog);
                        }
                    });

                }
            }
        });
    }

    private void normalDialogOneBtn(String textContent) {
        Config.personalCenterDialogShow = true;
        final AlertUtilOneButton diyDialog = new AlertUtilOneButton(this);
        diyDialog.setOk("确定")
                .setContent(textContent)
                .setDialogClickListener(new AlertUtilOneButton.DialogClickListener() {
                    @Override
                    public void ok() {
                        diyDialog.cancle();
                        finish();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    public void back(View view) {
        hideKeyboard();
        finish();
    }
}
