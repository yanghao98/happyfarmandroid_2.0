package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * Created by Administrator on 2018/5/11.
 */

public class searchOrderlogList {

    /**
     * result : {"pageNum":1,"pageSize":1,"size":1,"orderBy":null,"startRow":0,"endRow":0,"total":1,"pages":1,"list":[{"id":3,"orderNum":"1528835459295147","userId":12,"finalPrice":288,"integral":2880,"orderImg":"http://p7blgq6id.bkt.clouddn.com/05.png","startTime":1525945956000,"transactionTime":1526386994000,"updateTime":null,"sellerNickname":"你是一个超级神奇的人物","buyId":"1","buyNickname":"use1","num":1,"cropName":"萝卜","managerName":null,"plot_name":"仙河苑","staffName":null,"isOrganic":1,"state":2}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 1
         * size : 1
         * orderBy : null
         * startRow : 0
         * endRow : 0
         * total : 1
         * pages : 1
         * list : [{"id":3,"orderNum":"1528835459295147","userId":12,"finalPrice":288,"integral":2880,"orderImg":"http://p7blgq6id.bkt.clouddn.com/05.png","startTime":1525945956000,"transactionTime":1526386994000,"updateTime":null,"sellerNickname":"你是一个超级神奇的人物","buyId":"1","buyNickname":"use1","num":1,"cropName":"萝卜","managerName":null,"plot_name":"仙河苑","staffName":null,"isOrganic":1,"state":2}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 3
             * orderNum : 1528835459295147
             * "receivingCode": "8d228e6b1e3611b525e61d25d52d5df1090f8471580f8fdc8a6251bec89bdffb"，
             * closing: 0,
             * userId : 12
             * finalPrice : 288
             * integral : 2880
             * orderImg : http://p7blgq6id.bkt.clouddn.com/05.png
             * startTime : 1525945956000
             * transactionTime : 1526386994000
             * updateTime : null
             * sellerNickname : 你是一个超级神奇的人物
             * buyId : 1
             * buyNickname : use1
             * num : 1
             * cropName : 萝卜
             * managerName : null
             * plot_name : 仙河苑
             * staffName : null
             * isOrganic : 1
             * state : 2
             */

            private int id;
            private String orderNum;
            private String receivingCode;
            private int closing;
            private int userId;
            private Double finalPrice;
            private Double integral;
            private String orderImg;
            private long startTime;
            private long transactionTime;
            private Object updateTime;
            private String sellerNickname;
            private String buyId;
            private String buyNickname;
            private int num;
            private String cropName;
            private Object managerName;
            private String plot_name;
            private Object staffName;
            private int isOrganic;
            private int state;
            private double price;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getOrderNum() {
                return orderNum;
            }

            public void setOrderNum(String orderNum) {
                this.orderNum = orderNum;
            }

            public String getReceivingCode() {
                return receivingCode;
            }

            public void setReceivingCode(String receivingCode) {
                this.receivingCode = receivingCode;
            }

            public int getClosing() {
                return closing;
            }

            public void setClosing(int closing) {
                this.closing = closing;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public Double getFinalPrice() {
                return finalPrice;
            }

            public void setFinalPrice(Double finalPrice) {
                this.finalPrice = finalPrice;
            }

            public Double getIntegral() {
                return integral;
            }

            public void setIntegral(Double integral) {
                this.integral = integral;
            }

            public String getOrderImg() {
                return orderImg;
            }

            public void setOrderImg(String orderImg) {
                this.orderImg = orderImg;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public long getTransactionTime() {
                return transactionTime;
            }

            public void setTransactionTime(long transactionTime) {
                this.transactionTime = transactionTime;
            }

            public Object getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(Object updateTime) {
                this.updateTime = updateTime;
            }

            public String getSellerNickname() {
                return sellerNickname;
            }

            public void setSellerNickname(String sellerNickname) {
                this.sellerNickname = sellerNickname;
            }

            public String getBuyId() {
                return buyId;
            }

            public void setBuyId(String buyId) {
                this.buyId = buyId;
            }

            public String getBuyNickname() {
                return buyNickname;
            }

            public void setBuyNickname(String buyNickname) {
                this.buyNickname = buyNickname;
            }

            public int getNum() {
                return num;
            }

            public void setNum(int num) {
                this.num = num;
            }

            public String getCropName() {
                return cropName;
            }

            public void setCropName(String cropName) {
                this.cropName = cropName;
            }

            public Object getManagerName() {
                return managerName;
            }

            public void setManagerName(Object managerName) {
                this.managerName = managerName;
            }

            public String getPlot_name() {
                return plot_name;
            }

            public void setPlot_name(String plot_name) {
                this.plot_name = plot_name;
            }

            public Object getStaffName() {
                return staffName;
            }

            public void setStaffName(Object staffName) {
                this.staffName = staffName;
            }

            public int getIsOrganic() {
                return isOrganic;
            }

            public void setIsOrganic(int isOrganic) {
                this.isOrganic = isOrganic;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }
        }
    }
}
