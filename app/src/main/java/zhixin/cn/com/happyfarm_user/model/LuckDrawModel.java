package zhixin.cn.com.happyfarm_user.model;

public class LuckDrawModel {
//    {
//        "result": {
//        "id": 6,
//                "luckyDrawType": 1,
//                "luckyDrawContent": "3折",
//                "numberPrizes": 0,
//                "managerId": 1,
//                "discount": 0.3,
//                "probability": 0.1,
//                "startTime": 1575879756000,
//                "updateTime": 1576128883000,
//                "state": 1
//    },
//        "flag": "success"
//    }
    private LuckDrawNameBean result;
    private String flag;

    public LuckDrawNameBean getResult() {
        return result;
    }

    public void setResult(LuckDrawNameBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class LuckDrawNameBean {
        private int id;
        private int luckyDrawType;
        private String luckyDrawContent;
        private int numberPrizes;
        private int managerId;
        private float discount;
        private float probability;
        private long startTime;
        private long updateTime;
        private int state;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLuckyDrawType() {
            return luckyDrawType;
        }

        public void setLuckyDrawType(int luckyDrawType) {
            this.luckyDrawType = luckyDrawType;
        }

        public String getLuckyDrawContent() {
            return luckyDrawContent;
        }

        public void setLuckyDrawContent(String luckyDrawContent) {
            this.luckyDrawContent = luckyDrawContent;
        }

        public int getNumberPrizes() {
            return numberPrizes;
        }

        public void setNumberPrizes(int numberPrizes) {
            this.numberPrizes = numberPrizes;
        }

        public int getManagerId() {
            return managerId;
        }

        public void setManagerId(int managerId) {
            this.managerId = managerId;
        }

        public float getDiscount() {
            return discount;
        }

        public void setDiscount(float discount) {
            this.discount = discount;
        }

        public float getProbability() {
            return probability;
        }

        public void setProbability(float probability) {
            this.probability = probability;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }

}
