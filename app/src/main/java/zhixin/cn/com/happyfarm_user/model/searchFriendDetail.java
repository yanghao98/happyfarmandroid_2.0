package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * Created by Administrator on 2018/6/14.
 */

public class searchFriendDetail {

    /**
     * result : [{"id":33,"uuid":"b1cc0960-4af6-11e9-bc05-65804922aff0","nickname":"18061650808","password":"e10adc3949ba59abbe56e057f20f883e","headImg":"","tel":"18061650808","email":"","realname":"","gender":0,"birthday":null,"plotId":null,"plotName":null,"receiverName":null,"receiverTel":null,"certificates":1,"certificatesNo":"","diamond":0,"exp":0,"isLandlord":0,"isVoice":0,"registerTime":1553075901000,"registerIp":"0:0:0:0:0:0:0:1","loginIp":"192.186.1.108","state":1,"todayEgg":null,"todayFailEgg":null,"isSign":0,"superUser":0,"landId":null,"landNo":null,"landType":null,"xingeToken":null}]
     * flag : success
     */

    private String flag;
    private List<ResultBean> result;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * id : 33
         * uuid : b1cc0960-4af6-11e9-bc05-65804922aff0
         * nickname : 18061650808
         * password : e10adc3949ba59abbe56e057f20f883e
         * headImg :
         * tel : 18061650808
         * email :
         * realname :
         * gender : 0
         * birthday : null
         * plotId : null
         * plotName : null
         * receiverName : null
         * receiverTel : null
         * certificates : 1
         * certificatesNo :
         * diamond : 0
         * exp : 0
         * isLandlord : 0
         * isVoice : 0
         * registerTime : 1553075901000
         * registerIp : 0:0:0:0:0:0:0:1
         * loginIp : 192.186.1.108
         * state : 1
         * todayEgg : null
         * todayFailEgg : null
         * isSign : 0
         * superUser : 0
         * landId : null
         * landNo : null
         * landType : null
         * xingeToken : null
         */

        private Integer id;
        private String uuid;
        private String nickname;
        private String password;
        private String headImg;
        private String tel;
        private String email;
        private String realname;
        private Integer gender;
        private long birthday;
        private Object plotId;
        private Object plotName;
        private Object receiverName;
        private Object receiverTel;
        private int certificates;
        private String certificatesNo;
        private double diamond;
        private Integer exp;
        private Integer isLandlord;
        private Integer isVoice;
        private long registerTime;
        private String registerIp;
        private String loginIp;
        private Integer state;
        private Object todayEgg;
        private Object todayFailEgg;
        private Integer isSign;
        private Integer superUser;
        private Integer landId;
        private Integer landNo;
        private Integer landType;
        private String xingeToken;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = null == id ? 0 : id;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getHeadImg() {
            return headImg;
        }

        public void setHeadImg(String headImg) {
            this.headImg = headImg;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public Integer getGender() {
            return gender;
        }

        public void setGender(Integer gender) {
            this.gender = gender;
        }

        public long getBirthday() {
            return birthday;
        }

        public void setBirthday(long birthday) {
            this.birthday = birthday;
        }

        public Object getPlotId() {
            return plotId;
        }

        public void setPlotId(Object plotId) {
            this.plotId = plotId;
        }

        public Object getPlotName() {
            return plotName;
        }

        public void setPlotName(Object plotName) {
            this.plotName = plotName;
        }

        public Object getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(Object receiverName) {
            this.receiverName = receiverName;
        }

        public Object getReceiverTel() {
            return receiverTel;
        }

        public void setReceiverTel(Object receiverTel) {
            this.receiverTel = receiverTel;
        }

        public int getCertificates() {
            return certificates;
        }

        public void setCertificates(int certificates) {
            this.certificates = certificates;
        }

        public String getCertificatesNo() {
            return certificatesNo;
        }

        public void setCertificatesNo(String certificatesNo) {
            this.certificatesNo = certificatesNo;
        }

        public double getDiamond() {
            return diamond;
        }

        public void setDiamond(double diamond) {
            this.diamond = diamond;
        }

        public Integer getExp() {
            return exp;
        }

        public void setExp(Integer exp) {
            this.exp = exp;
        }

        public Integer getIsLandlord() {
            return isLandlord;
        }

        public void setIsLandlord(Integer isLandlord) {
            this.isLandlord = null == isLandlord ? 0 : isLandlord;
        }

        public Integer getIsVoice() {
            return isVoice;
        }

        public void setIsVoice(Integer isVoice) {
            this.isVoice = null == isVoice ? 0 : isVoice;
        }

        public long getRegisterTime() {
            return registerTime;
        }

        public void setRegisterTime(long registerTime) {
            this.registerTime = registerTime;
        }

        public String getRegisterIp() {
            return registerIp;
        }

        public void setRegisterIp(String registerIp) {
            this.registerIp = registerIp;
        }

        public String getLoginIp() {
            return loginIp;
        }

        public void setLoginIp(String loginIp) {
            this.loginIp = loginIp;
        }

        public Integer getState() {
            return state;
        }

        public void setState(Integer state) {
            this.state = state;
        }

        public Object getTodayEgg() {
            return todayEgg;
        }

        public void setTodayEgg(Object todayEgg) {
            this.todayEgg = todayEgg;
        }

        public Object getTodayFailEgg() {
            return todayFailEgg;
        }

        public void setTodayFailEgg(Object todayFailEgg) {
            this.todayFailEgg = todayFailEgg;
        }

        public Integer getIsSign() {
            return isSign;
        }

        public void setIsSign(Integer isSign) {
            this.isSign = null == isSign ? 0 : isSign;
        }

        public Integer getSuperUser() {
            return superUser;
        }

        public void setSuperUser(Integer superUser) {
            this.superUser = null == superUser ? 0 : superUser;
        }

        public Integer getLandId() {
            return landId;
        }

        public void setLandId(Integer landId) {
            this.landId = null == landId ? 0 : landId;
        }

        public Integer getLandNo() {
            return landNo;
        }

        public void setLandNo(Integer landNo) {
            this.landNo = null == landNo ? 0 : landNo;
        }

        public Integer getLandType() {
            return landType;
        }

        public void setLandType(Integer landType) {
            this.landType = null == landType ? 0 : landType;
        }

        public String getXingeToken() {
            return xingeToken;
        }

        public void setXingeToken(String xingeToken) {
            this.xingeToken = xingeToken;
        }
    }
}
