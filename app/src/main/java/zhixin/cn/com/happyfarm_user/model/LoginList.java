package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by DELL on 2018/2/7.
 */

public class LoginList {

    /**
     * result : {"user":{"id":9,"uuid":"ade4d300-1ed6-11e9-9f16-75dbb3aa6213","nickname":"胡萝卜","password":"e10adc3949ba59abbe56e057f20f883e","headImg":"http://img.trustwusee.com/headImg_9_1548227233","tel":"15170029037","email":"1228205445@qq.com","realname":"huyueling12","gender":1,"birthday":1.5166368E12,"certificates":1,"certificatesNo":"362330199507585565","diamond":887.4,"exp":0,"isLandlord":1,"isVoice":1,"registerTime":1.548224299E12,"registerIp":"192.168.0.133","loginTime":1.560737598E12,"loginIp":"192.168.0.104","state":1,"todayEgg":2.4,"isSign":0,"superUser":0,"landId":285,"landNo":285,"xingeToken":"iOS_02f89d807e434e2df575833fb47dcfdc74189eda61d8a272f1b295d3fc009210"},"token":"c8d5ef5d-1102-41f1-8aa4-8376a6e6b970"}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * user : {"id":9,"uuid":"ade4d300-1ed6-11e9-9f16-75dbb3aa6213","nickname":"胡萝卜","password":"e10adc3949ba59abbe56e057f20f883e","headImg":"http://img.trustwusee.com/headImg_9_1548227233","tel":"15170029037","email":"1228205445@qq.com","realname":"huyueling12","gender":1,"birthday":1.5166368E12,"certificates":1,"certificatesNo":"362330199507585565","diamond":887.4,"exp":0,"isLandlord":1,"isVoice":1,"registerTime":1.548224299E12,"registerIp":"192.168.0.133","loginTime":1.560737598E12,"loginIp":"192.168.0.104","state":1,"todayEgg":2.4,"isSign":0,"superUser":0,"landId":285,"landNo":285,"xingeToken":"iOS_02f89d807e434e2df575833fb47dcfdc74189eda61d8a272f1b295d3fc009210"}
         * token : c8d5ef5d-1102-41f1-8aa4-8376a6e6b970
         */

        private UserBean user;
        private String token;

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public static class UserBean {
            /**
             * id : 9
             * uuid : ade4d300-1ed6-11e9-9f16-75dbb3aa6213
             * nickname : 胡萝卜
             * password : e10adc3949ba59abbe56e057f20f883e
             * headImg : http://img.trustwusee.com/headImg_9_1548227233
             * tel : 15170029037
             * email : 1228205445@qq.com
             * realname : huyueling12
             * gender : 1
             * birthday : 1.5166368E12
             * certificates : 1
             * certificatesNo : 362330199507585565
             * diamond : 887.4
             * exp : 0
             * isLandlord : 1
             * isVoice : 1
             * registerTime : 1.548224299E12
             * registerIp : 192.168.0.133
             * loginTime : 1.560737598E12
             * loginIp : 192.168.0.104
             * state : 1
             * todayEgg : 2.4
             * isSign : 0
             * superUser : 0
             * landId : 285
             * landNo : 285
             * xingeToken : iOS_02f89d807e434e2df575833fb47dcfdc74189eda61d8a272f1b295d3fc009210
             */

            private int id;
            private String uuid;
            private String nickname;
            private String password;
            private String headImg;
            private String tel;
            private String email;
            private String realname;
            private int gender;
            private double birthday;
            private int certificates;
            private String certificatesNo;
            private double diamond;
            private int exp;
            private int isLandlord;
            private int isVoice;
            private double registerTime;
            private String registerIp;
            private double loginTime;
            private String loginIp;
            private int state;
            private double todayEgg;
            private int isSign;
            private int superUser;
            private int landId;
            private int landNo;
            private String xingeToken;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getHeadImg() {
                return headImg;
            }

            public void setHeadImg(String headImg) {
                this.headImg = headImg;
            }

            public String getTel() {
                return tel;
            }

            public void setTel(String tel) {
                this.tel = tel;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getRealname() {
                return realname;
            }

            public void setRealname(String realname) {
                this.realname = realname;
            }

            public int getGender() {
                return gender;
            }

            public void setGender(int gender) {
                this.gender = gender;
            }

            public double getBirthday() {
                return birthday;
            }

            public void setBirthday(double birthday) {
                this.birthday = birthday;
            }

            public int getCertificates() {
                return certificates;
            }

            public void setCertificates(int certificates) {
                this.certificates = certificates;
            }

            public String getCertificatesNo() {
                return certificatesNo;
            }

            public void setCertificatesNo(String certificatesNo) {
                this.certificatesNo = certificatesNo;
            }

            public double getDiamond() {
                return diamond;
            }

            public void setDiamond(double diamond) {
                this.diamond = diamond;
            }

            public int getExp() {
                return exp;
            }

            public void setExp(int exp) {
                this.exp = exp;
            }

            public int getIsLandlord() {
                return isLandlord;
            }

            public void setIsLandlord(int isLandlord) {
                this.isLandlord = isLandlord;
            }

            public int getIsVoice() {
                return isVoice;
            }

            public void setIsVoice(int isVoice) {
                this.isVoice = isVoice;
            }

            public double getRegisterTime() {
                return registerTime;
            }

            public void setRegisterTime(double registerTime) {
                this.registerTime = registerTime;
            }

            public String getRegisterIp() {
                return registerIp;
            }

            public void setRegisterIp(String registerIp) {
                this.registerIp = registerIp;
            }

            public double getLoginTime() {
                return loginTime;
            }

            public void setLoginTime(double loginTime) {
                this.loginTime = loginTime;
            }

            public String getLoginIp() {
                return loginIp;
            }

            public void setLoginIp(String loginIp) {
                this.loginIp = loginIp;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public double getTodayEgg() {
                return todayEgg;
            }

            public void setTodayEgg(double todayEgg) {
                this.todayEgg = todayEgg;
            }

            public int getIsSign() {
                return isSign;
            }

            public void setIsSign(int isSign) {
                this.isSign = isSign;
            }

            public int getSuperUser() {
                return superUser;
            }

            public void setSuperUser(int superUser) {
                this.superUser = superUser;
            }

            public int getLandId() {
                return landId;
            }

            public void setLandId(int landId) {
                this.landId = landId;
            }

            public int getLandNo() {
                return landNo;
            }

            public void setLandNo(int landNo) {
                this.landNo = landNo;
            }

            public String getXingeToken() {
                return xingeToken;
            }

            public void setXingeToken(String xingeToken) {
                this.xingeToken = xingeToken;
            }
        }
    }
}
