package zhixin.cn.com.happyfarm_user.Page;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.EventBus.VideoEvent;
import zhixin.cn.com.happyfarm_user.HistoryPicActivity;
import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.PaymentInformationActivity;
import zhixin.cn.com.happyfarm_user.PerfectInformationActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.utils.TextString;


/**
 * Created by HuYueling on 2018/3/21.
 */

public class HistoryPage extends Fragment {
    private int search_num = 0;

    private String startDate; //查询的开始时间
    private String endDate; //查询的结束时间
    private Date startDateTime;//开始时间
    private Date endDateTime;//结束时间
    private String state;
    private String landNo;
    private String landId;
    private String UserNickName;
    private String UserImg;
    private String UserTel;
    private String UserCertificatesNo;
    private String UserEmail;
    private String UserRealname;
    private int UserPlotId;
    private String UserPlotName;
    private int UserGender;
    private long UserBirthday;
    private TextView yearView;
    private TextView monthView;
    private TextView dayView;
    private TextView endYearView;
    private TextView endMonthView;
    private TextView endDayView;
    private RelativeLayout yearViewLayout;
    private RelativeLayout monthViewLayout;
    private RelativeLayout dayViewLayout;
    private RelativeLayout endYearViewLayout;
    private RelativeLayout endMonthViewLayout;
    private RelativeLayout endDayViewLayout;
    private int landType;
    private Spinner spinner;//下拉选择器
    private Button searchBtn;//查询按钮
    private boolean okData = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.history_layout, null, false);

        initView(view);
        timeSelect();//时间选择
        showFrame();
        searchImage();
        return view;

    }

    /**
     * 布局初始化
     * @param view 视图layout
     */
    private void initView(View view) {
        spinner = view.findViewById(R.id.spinner);
        searchBtn = view.findViewById(R.id.btn_search);
        yearView = view.findViewById(R.id.year_text_view);
        monthView = view.findViewById(R.id.month_text_view);
        dayView = view.findViewById(R.id.day_text_view);
        endYearView = view.findViewById(R.id.end_year_text_view);
        endMonthView = view.findViewById(R.id.end_month_text_view);
        endDayView = view.findViewById(R.id.end_day_text_view);
        yearViewLayout = view.findViewById(R.id.year_text_view_layout);
        monthViewLayout = view.findViewById(R.id.month_text_view_layout);
        dayViewLayout = view.findViewById(R.id.day_text_view_layout);
        endYearViewLayout = view.findViewById(R.id.end_year_text_view_layout);
        endMonthViewLayout = view.findViewById(R.id.end_month_text_view_layout);
        endDayViewLayout = view.findViewById(R.id.end_day_text_view_layout);
    }

    /**
     * 时间选择方法
     */
    private void timeSelect() {
        yearViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DatePickDialog dialog = new DatePickDialog(getContext());
//                //设置上下年分限制
//                dialog.setYearLimt(5);
//                //设置标题
//                dialog.setTitle("选择时间");
//                //设置类型
//                dialog.setType(DateType.TYPE_YMD);
//                //设置消息体的显示格式，日期格式
//                dialog.setMessageFormat("yyyy-MM-dd");
//                //设置选择回调
//                dialog.setOnChangeLisener(null);
//                //设置点击确定按钮回调
//                dialog.setOnSureLisener(new OnSureLisener() {
//                    @Override
//                    public void onSure(Date date) {
//                        startTime.setText(DateUtil.formatCn(date));
//                        startDate = DateUtil.formatYYYYMMdd(date);
//                    }
//                });
//                dialog.show();
                startTime();

            }
        });
        monthViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTime();
            }
        });
        dayViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTime();
            }
        });
        endYearViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime();
            }
        });
        endMonthViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime();
            }
        });
        endDayViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime();
            }
        });
    }

    /**
     * 下拉选择器，历史图片需要展示张数
     */
    private void showFrame() {
        spinner.setDropDownWidth(200); //下拉宽度
        spinner.setDropDownHorizontalOffset(100);//下拉的横向偏移
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String[] num = getResources().getStringArray(R.array.pic_num);
                search_num = pos + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
            }
        });
    }

    /**
     * 时间选择器弹框
     */
    public void startTime() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectedDate = Calendar.getInstance();

        Calendar startDateTimes = Calendar.getInstance();
        startDateTimes.set(2017, 0, 31);
        //时间选择器
        TimePickerView pvTime = new TimePickerBuilder(getContext(), new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                yearView.setText(DateUtil.formatY(date));
                monthView.setText(DateUtil.formatM(date));
                dayView.setText(DateUtil.formatD(date));
                startDate = DateUtil.formatYYYYMMdd(date);
                startDateTime = date;
            }
        })
                .setTitleText("选择时间")//标题文字
                .setTitleSize(15)//标题文字大小
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setCancelColor(Color.GRAY)//取消按钮文字颜色
                .isDialog(true)
                .setDate(selectedDate)
                .setRangDate(startDateTimes, selectedDate)
                .build();
        pvTime.show();
        Dialog mDialog = pvTime.getDialog();
        if (mDialog != null) {

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvTime.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
//                    dialogWindow.setWindowAnimations(com.bigkoo.pickerview.R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
            }
        }
    }

    public void endTime() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectedDate = Calendar.getInstance();

        Calendar endDateTimes = Calendar.getInstance();
        endDateTimes.set(2017, 0, 31);
        //时间选择器
        TimePickerView pvTime = new TimePickerBuilder(getContext(), new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                endYearView.setText(DateUtil.formatY(date));
                endMonthView.setText(DateUtil.formatM(date));
                endDayView.setText(DateUtil.formatD(date));
                endDate = DateUtil.formatYYYYMMdd(date);
                endDateTime = date;
            }
        })
                .setTitleText("选择时间")//标题文字
                .setTitleSize(15)//标题文字大小
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setCancelColor(Color.GRAY)//取消按钮文字颜色
                .isDialog(true)
                .setDate(selectedDate)
                .setRangDate(endDateTimes, selectedDate)
                .build();
        pvTime.show();
        Dialog mDialog = pvTime.getDialog();
        if (mDialog != null) {

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvTime.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
//                    dialogWindow.setWindowAnimations(com.bigkoo.pickerview.R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //主要用于刚进来获取用户信息，好做传值
        getUserInfo();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            state = ((LandVideoActivity) activity).getState();
            landId = ((LandVideoActivity) activity).getLandId();
            landNo = ((LandVideoActivity) activity).getLandNos();
            landType = ((LandVideoActivity) activity).getLandType();
            Log.i(state +":"+ landId +":"+ landNo +":"+ landType, "onAttach: ");
            okData = true;
        } catch (Exception e) {
            okData = false;
        }

    }

    /**
     * 查询历史图片点击事件
     */
    private void searchImage() {

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (okData) {
                    userInfoToken();
                    Log.i("我的租地页面进来的", "onClick: ");
                } else {
                    Log.i("首页进来的", "onClick: ");
                    if (startDate == null) {
                        Toast.makeText(getContext(), "请选择开始日期", Toast.LENGTH_SHORT).show();
                    } else if (endDate == null) {
                        Toast.makeText(getContext(), "请选择结束日期", Toast.LENGTH_SHORT).show();
                    } else {
                        //compareTo()方法的返回值，startDateTime小于endDateTime返回-1，startDateTime大于startDateTime返回1，相等返回0
                        int compareTo = startDateTime.compareTo(endDateTime);
                        Log.i("HistoryPage", "onResponse: " + compareTo);
                        if (1 == compareTo) {
                            Toast.makeText(getContext(), "开始时间大于结束时间", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        EventBus.getDefault().post(new VideoEvent("INFOSUCCESSFUL"));
                        Intent intent = new Intent(getContext(), HistoryPicActivity.class);
                        intent.putExtra("startDate", startDate);
                        intent.putExtra("endDate", endDate);
                        intent.putExtra("num", search_num);
                        startActivity(intent);
                    }

                }
            }
        });
    }

    //完善用户信息layout弹窗
    private void perfectInfoDialog() {
        Dialog bottomDialog = new Dialog(getContext(), R.style.dialog);
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.layout_perfect_information_dialog, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        //设置左右间距
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(getContext(), 100f);
        params.bottomMargin = DensityUtil.dp2px(getContext(), 0f);
        contentView.findViewById(R.id.lease_immediately_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomDialog.cancel();
//                finish();
            }
        });
        contentView.findViewById(R.id.lease_immediately_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //获取用户信息是否完善
                retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
                getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
                    @Override
                    public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                        if (("success").equals(response.body().getFlag())) {
                            String titleText = "完善信息";
                            String contentText = "您的信息暂未完善，是否完善您的信息";
                            if (NumUtil.checkNull(response.body().getResult().getRealname())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getCertificatesNo())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getPlotId())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (response.body().getResult().getGender() == 0) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getBirthday())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getTel())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getEmail())) {
                                showUserInfoDialog(titleText, contentText);
                            } else {
                                if (okData) {
                                    Intent intent = new Intent(getContext(), PaymentInformationActivity.class);
                                    intent.putExtra("landNo", landNo);
                                    intent.putExtra("landId", landId);
                                    intent.putExtra("landType", landType + "");
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(getContext(),"不能购买使看地块",Toast.LENGTH_SHORT).show();
                                }
                            }

                        } else {
                            Toast.makeText(getContext(), response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                        Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                    }
                });
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();

    }

    //完善用户信息layout弹窗
    private void showUserInfoDialog(String titleText, String contentText) {
        Dialog bottomDialog = new Dialog(getContext(), R.style.dialog);
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.layout_perfect_information_dialog, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        //设置左右间距
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(getContext(), 100f);
        params.bottomMargin = DensityUtil.dp2px(getContext(), 0f);
        TextView lblTitle = contentView.findViewById(R.id.lease_immediately_bt);
        lblTitle.setText(titleText);
        TextView contentTitle = contentView.findViewById(R.id.user_info_content);
        contentTitle.setText(contentText);
        contentView.findViewById(R.id.lease_immediately_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomDialog.cancel();
//                finish();
            }
        });
        contentView.findViewById(R.id.lease_immediately_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("完善信息".equals(titleText)) {
                    if (okData) {
                        Intent intent = new Intent(getContext(), PerfectInformationActivity.class);
                        intent.putExtra("nickName", UserNickName);
                        intent.putExtra("userImg", UserImg);
                        intent.putExtra("UserRealname", UserRealname);
                        intent.putExtra("landNo", landNo);
                        intent.putExtra("landId", landId);
                        intent.putExtra("tel", UserTel);
                        intent.putExtra("UserPlotName", UserPlotName);
                        intent.putExtra("UserCertificatesNo", UserCertificatesNo);
                        intent.putExtra("UserPlotId", UserPlotId + "");
                        intent.putExtra("UserGender", UserGender + "");
                        intent.putExtra("UserBirthday", UserBirthday + "");
                        intent.putExtra("UserEmail", UserEmail + "");
                        intent.putExtra("landType", landType + "");
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(),"不能购买使看地块",Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);
                }

                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();

    }

    public void getUserInfo() {
        //获取用户信息是否为地主
        retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (("success").equals(response.body().getFlag())) {
                    UserNickName = response.body().getResult().getNickname();
                    UserImg = response.body().getResult().getHeadImg();
                    UserTel = response.body().getResult().getTel();
                    UserCertificatesNo = response.body().getResult().getCertificatesNo();
                    UserRealname = response.body().getResult().getRealname();
                    UserPlotId = response.body().getResult().getPlotId();
                    UserPlotName = response.body().getResult().getPlotName();
                    UserGender = response.body().getResult().getGender();
                    UserBirthday = response.body().getResult().getBirthday();
                    UserEmail = response.body().getResult().getEmail();
                } else {
                    Toast.makeText(getActivity(), "网络错误", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }

    public void userInfoToken() {
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("success").equals(response.body().getFlag())) {
                    if (state.equals("1")) {
                        //获取用户信息是否为地主
                        retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
                        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
                            @Override
                            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                                if (("success").equals(response.body().getFlag())) {
                                    //判断是否是地主。
                                    if (response.body().getResult().getIsLandlord() == 1) {
                                        Toast.makeText(getActivity(), "您已是地主，您的地在等待您养护哦", Toast.LENGTH_SHORT).show();
                                    } else {
                                        perfectInfoDialog();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                                showUserInfoDialog(TextString.DialogTitleText, TextString.DialogContentText);
                            }
                        });
                    } else {
                        if (startDate == null) {
                            Toast.makeText(getContext(), "请选择开始日期", Toast.LENGTH_SHORT).show();
                        } else if (endDate == null) {
                            Toast.makeText(getContext(), "请选择结束日期", Toast.LENGTH_SHORT).show();
                        } else {
                            //compareTo()方法的返回值，startDateTime小于endDateTime返回-1，startDateTime大于startDateTime返回1，相等返回0
                            int compareTo = startDateTime.compareTo(endDateTime);
                            Log.i("HistoryPage", "onResponse: " + compareTo);
                            if (1 == compareTo) {
                                Toast.makeText(getContext(), "开始时间大于结束时间", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            EventBus.getDefault().post(new VideoEvent("INFOSUCCESSFUL"));
                            Intent intent = new Intent(getContext(), HistoryPicActivity.class);
                            intent.putExtra("startDate", startDate);
                            intent.putExtra("endDate", endDate);
                            intent.putExtra("num", search_num);
                            startActivity(intent);
                        }
                    }
                } else {
                    showUserInfoDialog(TextString.DialogTitleText, TextString.DialogContentText);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }
}

