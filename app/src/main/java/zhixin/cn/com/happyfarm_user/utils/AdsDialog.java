package zhixin.cn.com.happyfarm_user.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import zhixin.cn.com.happyfarm_user.AboutUsActivity;
import zhixin.cn.com.happyfarm_user.PrizeExchangeActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.TermsActivity;
import zhixin.cn.com.happyfarm_user.UpdateVersionWebActivity;

import static zhixin.cn.com.happyfarm_user.utils.Utils.isAvilible;
import static zhixin.cn.com.happyfarm_user.utils.Utils.launchAppDetail;

public class AdsDialog extends Dialog implements View.OnClickListener {

    private ImageView imageView;
    private ImageView iconON;
    private TextView  titltTextView;
    private TextView  contentTextView;
    private Button absButton;

    public AdsDialog(@NonNull Context context,String url,String title,String content,int type) {
        super(context);
        setContentView(R.layout.ads_dialog);
        //点击外侧不消失
        setCanceledOnTouchOutside(false);
        titltTextView = findViewById(R.id.ads_dialog_text1);
        titltTextView.setText(title);
        contentTextView = findViewById(R.id.ads_dialog_text2);
        contentTextView.setText(content);
        imageView = (ImageView) findViewById(R.id.ads_dialog_iv_advertisement);
        imageView.setOnClickListener(this);
        absButton = findViewById(R.id.ads_button);
        absButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isMIUI()) {
                    launchAppDetail(context,"zhixin.cn.com.happyfarm_user","com.xiaomi.market");
                } else if (Utils.isEMUI()) {
                    launchAppDetail(context,"zhixin.cn.com.happyfarm_user","com.huawei.appmarket");
                } else {
                    if (isAvilible(context,"com.tencent.android.qqdownloader")) {
                        launchAppDetail(context,"zhixin.cn.com.happyfarm_user","com.tencent.android.qqdownloader");
                    } else {
                        Uri uri = Uri.parse("https://download.sj.qq.com/upload/connAssitantDownload/upload/MobileAssistant_1.apk");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        getContext().startActivity(intent);
                    }
                }
            }
        });
        if (1 == type) {
            System.out.println("显示");
            absButton.setVisibility(View.VISIBLE);
        } else {
            System.out.println("隐藏");
            absButton.setVisibility(View.GONE);
        }
        iconON = findViewById(R.id.icon_on);
        iconON.setOnClickListener(this);
        Picasso.get()
                .load(url)
//                .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                .into(imageView);
    }

    public void showDialog() {
        Window window = getWindow();
        window.setWindowAnimations(R.style.style_dialog);
        window.setBackgroundDrawableResource(R.color.no_background);
        WindowManager.LayoutParams wl = window.getAttributes();
        //设置弹窗位置
        wl.gravity = Gravity.CENTER;
        window.setAttributes(wl);
        show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ads_dialog_iv_advertisement) {
//            closeAnimations();
            System.out.println("点击了 图片");
        }
        if (view.getId() == R.id.icon_on) {
//            closeAnimations();
            System.out.println("点击了 关闭按钮");
            dismiss();
        }
    }
}
