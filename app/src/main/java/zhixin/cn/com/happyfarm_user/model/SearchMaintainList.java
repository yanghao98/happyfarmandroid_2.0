package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * Created by DELL on 2018/3/23.
 */

public class SearchMaintainList {


    /**
     * result : {"pageNum":1,"pageSize":1,"size":1,"orderBy":null,"startRow":0,"endRow":0,"total":1,"pages":1,"list":[{"id":3,"maintainName":"施肥","maintainImg":"http://img.trustwusee.com/17.png","diamond":10,"details":"","startTime":1548050746000,"parent":0,"remark":"","updateTime":1550217607000,"managerId":1,"priority":1,"state":1,"child":[{"id":5,"maintainName":"无机肥","maintainImg":"http://img.trustwusee.com/17.png","diamond":10,"details":"","startTime":1548050783000,"parent":3,"remark":"","updateTime":1550217746000,"managerId":1,"priority":10,"state":1,"child":null},{"id":4,"maintainName":"有机肥","maintainImg":"http://img.trustwusee.com/17.png","diamond":5,"details":"","startTime":1548050766000,"parent":3,"remark":"","updateTime":1550217743000,"managerId":1,"priority":10,"state":1,"child":null}]}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 1
         * size : 1
         * orderBy : null
         * startRow : 0
         * endRow : 0
         * total : 1
         * pages : 1
         * list : [{"id":3,"maintainName":"施肥","maintainImg":"http://img.trustwusee.com/17.png","diamond":10,"details":"","startTime":1548050746000,"parent":0,"remark":"","updateTime":1550217607000,"managerId":1,"priority":1,"state":1,"child":[{"id":5,"maintainName":"无机肥","maintainImg":"http://img.trustwusee.com/17.png","diamond":10,"details":"","startTime":1548050783000,"parent":3,"remark":"","updateTime":1550217746000,"managerId":1,"priority":10,"state":1,"child":null},{"id":4,"maintainName":"有机肥","maintainImg":"http://img.trustwusee.com/17.png","diamond":5,"details":"","startTime":1548050766000,"parent":3,"remark":"","updateTime":1550217743000,"managerId":1,"priority":10,"state":1,"child":null}]}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 3
             * maintainName : 施肥
             * maintainImg : http://img.trustwusee.com/17.png
             * diamond : 10
             * details :
             * startTime : 1548050746000
             * parent : 0
             * remark :
             * updateTime : 1550217607000
             * managerId : 1
             * priority : 1
             * state : 1
             * child : [{"id":5,"maintainName":"无机肥","maintainImg":"http://img.trustwusee.com/17.png","diamond":10,"details":"","startTime":1548050783000,"parent":3,"remark":"","updateTime":1550217746000,"managerId":1,"priority":10,"state":1,"child":null},{"id":4,"maintainName":"有机肥","maintainImg":"http://img.trustwusee.com/17.png","diamond":5,"details":"","startTime":1548050766000,"parent":3,"remark":"","updateTime":1550217743000,"managerId":1,"priority":10,"state":1,"child":null}]
             */

            private int id;
            private String maintainName;
            private String maintainImg;
            private Double diamond;
            private String details;
            private long startTime;
            private int parent;
            private String remark;
            private long updateTime;
            private int managerId;
            private int priority;
            private int state;
            private List<ChildBean> child;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getMaintainName() {
                return maintainName;
            }

            public void setMaintainName(String maintainName) {
                this.maintainName = maintainName;
            }

            public String getMaintainImg() {
                return maintainImg;
            }

            public void setMaintainImg(String maintainImg) {
                this.maintainImg = maintainImg;
            }

            public Double getDiamond() {
                return diamond;
            }

            public void setDiamond(Double diamond) {
                this.diamond = diamond;
            }

            public String getDetails() {
                return details;
            }

            public void setDetails(String details) {
                this.details = details;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public int getParent() {
                return parent;
            }

            public void setParent(int parent) {
                this.parent = parent;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public long getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(long updateTime) {
                this.updateTime = updateTime;
            }

            public int getManagerId() {
                return managerId;
            }

            public void setManagerId(int managerId) {
                this.managerId = managerId;
            }

            public int getPriority() {
                return priority;
            }

            public void setPriority(int priority) {
                this.priority = priority;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public List<ChildBean> getChild() {
                return child;
            }

            public void setChild(List<ChildBean> child) {
                this.child = child;
            }

            public static class ChildBean {
                /**
                 * id : 5
                 * maintainName : 无机肥
                 * maintainImg : http://img.trustwusee.com/17.png
                 * diamond : 10
                 * details :
                 * startTime : 1548050783000
                 * parent : 3
                 * remark :
                 * updateTime : 1550217746000
                 * managerId : 1
                 * priority : 10
                 * state : 1
                 * child : null
                 */

                private int id;
                private String maintainName;
                private String maintainImg;
                private Double diamond;
                private String details;
                private long startTime;
                private int parent;
                private String remark;
                private long updateTime;
                private int managerId;
                private int priority;
                private int state;
                private Object child;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getMaintainName() {
                    return maintainName;
                }

                public void setMaintainName(String maintainName) {
                    this.maintainName = maintainName;
                }

                public String getMaintainImg() {
                    return maintainImg;
                }

                public void setMaintainImg(String maintainImg) {
                    this.maintainImg = maintainImg;
                }

                public Double getDiamond() {
                    return diamond;
                }

                public void setDiamond(Double diamond) {
                    this.diamond = diamond;
                }

                public String getDetails() {
                    return details;
                }

                public void setDetails(String details) {
                    this.details = details;
                }

                public long getStartTime() {
                    return startTime;
                }

                public void setStartTime(long startTime) {
                    this.startTime = startTime;
                }

                public int getParent() {
                    return parent;
                }

                public void setParent(int parent) {
                    this.parent = parent;
                }

                public String getRemark() {
                    return remark;
                }

                public void setRemark(String remark) {
                    this.remark = remark;
                }

                public long getUpdateTime() {
                    return updateTime;
                }

                public void setUpdateTime(long updateTime) {
                    this.updateTime = updateTime;
                }

                public int getManagerId() {
                    return managerId;
                }

                public void setManagerId(int managerId) {
                    this.managerId = managerId;
                }

                public int getPriority() {
                    return priority;
                }

                public void setPriority(int priority) {
                    this.priority = priority;
                }

                public int getState() {
                    return state;
                }

                public void setState(int state) {
                    this.state = state;
                }

                public Object getChild() {
                    return child;
                }

                public void setChild(Object child) {
                    this.child = child;
                }
            }
        }
    }
}
