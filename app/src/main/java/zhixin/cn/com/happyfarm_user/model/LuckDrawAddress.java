package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class LuckDrawAddress {
    private String flag;
    private List<DistrictInfo> result;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<DistrictInfo> getResult() {
        return result;
    }

    public void setResult(List<DistrictInfo> result) {
        this.result = result;
    }

    public class DistrictInfo{
        private int id;
        private String region;
        private List<StreetInfo> child;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public List<StreetInfo> getChild() {
            return child;
        }

        public void setChild(List<StreetInfo> child) {
            this.child = child;
        }

        public class StreetInfo {
            private int id;
            private String region;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getRegion() {
                return region;
            }

            public void setRegion(String region) {
                this.region = region;
            }
        }
    }
}
