package zhixin.cn.com.happyfarm_user.model;

public class Audio {
    private int id;
    private String audioUrl;
    private String route;
    private String audioName;
    private int State;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getAudioName() {
        return audioName;
    }

    public void setAudioName(String audioName) {
        this.audioName = audioName;
    }

    public int getState() {
        return State;
    }

    public void setState(int state) {
        State = state;
    }
}
