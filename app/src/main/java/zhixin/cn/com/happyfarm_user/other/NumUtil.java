package zhixin.cn.com.happyfarm_user.other;

import java.util.Arrays;
import java.util.List;

/**
 * Created by HuYueling on 2018/4/10.
 */

public class NumUtil {

    private static final char[]  CHAINIESFIGURE2={'零','一','二','三','四','五','六','七','八','九','十'};


    //整数部分的转换
    public static String toChineseCharI(String intString)throws NumberFormatException {

        int i = Integer.parseInt(intString);
        int x = i/1;
        if (x >  10){
            return String.valueOf(CHAINIESFIGURE2[10]) + String.valueOf(CHAINIESFIGURE2[x-10]);
        }else {
            return String.valueOf(CHAINIESFIGURE2[x]);
        }

    }

    /**
     * 判断为空
     */
    public static boolean checkNull(Object info) {

        if (null == info) {
            return true;
        } else {

            if (info instanceof String) {
                if ("".equals(info)) {
                    return true;
                } else {
                    return false;
                }
            } else if (info instanceof Integer) {
                if (((Integer) info).intValue() == 0)
                    return true;
            }
            return false;
        }
    }
}
