package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class ShoppingCart {

    protected ShoppingBean result;
    protected String flag;

    public ShoppingBean getResult() {
        return result;
    }

    public void setResult(ShoppingBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public class ShoppingBean {
        private int pageNum;
        private int pageSize;
        private int size;
        private String orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;

        private List<ListBean> list ;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public String getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(String orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public class ListBean {
            private Integer userId;
            private String nickname;
            private List<CartList> cartList;
            private Integer state;

            public int getUserId() {
                return userId;
            }

            public void setUserId(Integer userId) {
                this.userId = userId;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public Integer getState() {
                return state;
            }

            public void setState(Integer state) {
                this.state = state;
            }

            public List<CartList> getCartList() {
                return cartList;
            }

            public void setCartList(List<CartList> cartList) {
                this.cartList = cartList;
            }

            public class CartList {
                private Integer id;
                private Integer userId;
                private Integer num;
                private String nickname;
                private String cropName;
                private String cropImg;
                private Double price;
                private Integer goodsState;
                private Integer maxNum;
                private Double bargainPrice;
                private Double discountPrice;
                private Integer goodsId;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public int getNum() {
                    return num;
                }

                public void setNum(int num) {
                    this.num = num;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }

                public String getCropName() {
                    return cropName;
                }

                public void setCropName(String cropName) {
                    this.cropName = cropName;
                }

                public String getCropImg() {
                    return cropImg;
                }

                public void setCropImg(String cropImg) {
                    this.cropImg = cropImg;
                }

                public Double getPrice() {
                    return price;
                }

                public void setPrice(Double price) {
                    this.price = price;
                }

                public Integer getGoodsState() {
                    return goodsState;
                }

                public void setGoodsState(Integer goodsState) {
                    this.goodsState = goodsState;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public void setUserId(Integer userId) {
                    this.userId = userId;
                }

                public void setNum(Integer num) {
                    this.num = num;
                }

                public Integer getMaxNum() {
                    return maxNum;
                }

                public void setMaxNum(Integer maxNum) {
                    this.maxNum = maxNum;
                }

                public Double getBargainPrice() {
                    return bargainPrice;
                }

                public void setBargainPrice(Double bargainPrice) {
                    this.bargainPrice = bargainPrice;
                }

                public Double getDiscountPrice() {
                    return discountPrice;
                }

                public void setDiscountPrice(Double discountPrice) {
                    this.discountPrice = discountPrice;
                }

                public Integer getGoodsId() {
                    return goodsId;
                }

                public void setGoodsId(Integer goodsId) {
                    this.goodsId = goodsId;
                }
            }
        }
    }
}
