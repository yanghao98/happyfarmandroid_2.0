package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class LandHistoryPic {

    /**
     * result : {"pageNum":1,"pageSize":23,"size":23,"orderBy":null,"startRow":0,"endRow":22,"total":23,"pages":1,"list":[{"startTime":1530028800000,"pictureMap":[{"name":"capture_img_192.168.1.113_2018-06-27 16","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.113_2018-06-27 16","hours":"16"},{"name":"capture_img_192.168.1.113_2018-06-27 16","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.113_2018-06-27 16","hours":"16"}]},{"startTime":1530115200000,"pictureMap":[]},{"startTime":1530374400000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-01 14","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-01 14","hours":"14"},{"name":"capture_img_192.168.1.114_2018-07-01 14","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-01 14","hours":"14"}]},{"startTime":1530460800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-02_14","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-02_14","hours":"14"},{"name":"capture_img_192.168.1.114_2018-07-02_14","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-02_14","hours":"14"}]},{"startTime":1530547200000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-03_15","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-03_15","hours":"15"},{"name":"capture_img_192.168.1.114_2018-07-03_17","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-03_17","hours":"17"}]},{"startTime":1530633600000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-04_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-04_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-04_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-04_07","hours":"7"}]},{"startTime":1530720000000,"pictureMap":[]},{"startTime":1530806400000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-06_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-06_07","hours":"7"},{"name":"capture_img_192.168.1.114_2018-07-06_09","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-06_09","hours":"9"}]},{"startTime":1530892800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-07_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-07_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-07_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-07_05","hours":"5"}]},{"startTime":1530979200000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-08_03","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-08_03","hours":"3"},{"name":"capture_img_192.168.1.114_2018-07-08_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-08_05","hours":"5"}]},{"startTime":1531065600000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-09_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-09_07","hours":"7"},{"name":"capture_img_192.168.1.114_2018-07-09_09","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-09_09","hours":"9"}]},{"startTime":1531152000000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-10_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-10_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-10_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-10_07","hours":"7"}]},{"startTime":1531238400000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-11_09","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-11_09","hours":"9"},{"name":"capture_img_192.168.1.114_2018-07-11_15","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-11_15","hours":"15"}]},{"startTime":1531324800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-12_15","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-12_15","hours":"15"}]},{"startTime":1531411200000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-13_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-13_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-13_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-13_07","hours":"7"}]},{"startTime":1531497600000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-14_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-14_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-14_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-14_07","hours":"7"}]},{"startTime":1531670400000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-16_06","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-16_06","hours":"6"},{"name":"capture_img_192.168.1.114_2018-07-16_08","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-16_08","hours":"8"}]},{"startTime":1531756800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-17_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-17_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-17_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-17_07","hours":"7"}]},{"startTime":1531843200000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-18_04","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-18_04","hours":"4"},{"name":"capture_img_192.168.1.114_2018-07-18_06","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-18_06","hours":"6"}]},{"startTime":1531929600000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-19_04","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-19_04","hours":"4"},{"name":"capture_img_192.168.1.114_2018-07-19_06","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-19_06","hours":"6"}]},{"startTime":1532016000000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-20_04","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-20_04","hours":"4"},{"name":"capture_img_192.168.1.114_2018-07-20_06","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-20_06","hours":"6"}]},{"startTime":1532102400000,"pictureMap":[]},{"startTime":1532188800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-22_09","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-22_09","hours":"9"},{"name":"capture_img_192.168.1.114_2018-07-22_11","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-22_11","hours":"11"}]}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 23
         * size : 23
         * orderBy : null
         * startRow : 0
         * endRow : 22
         * total : 23
         * pages : 1
         * list : [{"startTime":1530028800000,"pictureMap":[{"name":"capture_img_192.168.1.113_2018-06-27 16","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.113_2018-06-27 16","hours":"16"},{"name":"capture_img_192.168.1.113_2018-06-27 16","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.113_2018-06-27 16","hours":"16"}]},{"startTime":1530115200000,"pictureMap":[]},{"startTime":1530374400000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-01 14","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-01 14","hours":"14"},{"name":"capture_img_192.168.1.114_2018-07-01 14","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-01 14","hours":"14"}]},{"startTime":1530460800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-02_14","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-02_14","hours":"14"},{"name":"capture_img_192.168.1.114_2018-07-02_14","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-02_14","hours":"14"}]},{"startTime":1530547200000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-03_15","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-03_15","hours":"15"},{"name":"capture_img_192.168.1.114_2018-07-03_17","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-03_17","hours":"17"}]},{"startTime":1530633600000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-04_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-04_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-04_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-04_07","hours":"7"}]},{"startTime":1530720000000,"pictureMap":[]},{"startTime":1530806400000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-06_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-06_07","hours":"7"},{"name":"capture_img_192.168.1.114_2018-07-06_09","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-06_09","hours":"9"}]},{"startTime":1530892800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-07_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-07_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-07_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-07_05","hours":"5"}]},{"startTime":1530979200000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-08_03","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-08_03","hours":"3"},{"name":"capture_img_192.168.1.114_2018-07-08_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-08_05","hours":"5"}]},{"startTime":1531065600000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-09_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-09_07","hours":"7"},{"name":"capture_img_192.168.1.114_2018-07-09_09","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-09_09","hours":"9"}]},{"startTime":1531152000000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-10_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-10_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-10_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-10_07","hours":"7"}]},{"startTime":1531238400000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-11_09","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-11_09","hours":"9"},{"name":"capture_img_192.168.1.114_2018-07-11_15","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-11_15","hours":"15"}]},{"startTime":1531324800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-12_15","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-12_15","hours":"15"}]},{"startTime":1531411200000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-13_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-13_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-13_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-13_07","hours":"7"}]},{"startTime":1531497600000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-14_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-14_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-14_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-14_07","hours":"7"}]},{"startTime":1531670400000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-16_06","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-16_06","hours":"6"},{"name":"capture_img_192.168.1.114_2018-07-16_08","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-16_08","hours":"8"}]},{"startTime":1531756800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-17_05","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-17_05","hours":"5"},{"name":"capture_img_192.168.1.114_2018-07-17_07","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-17_07","hours":"7"}]},{"startTime":1531843200000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-18_04","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-18_04","hours":"4"},{"name":"capture_img_192.168.1.114_2018-07-18_06","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-18_06","hours":"6"}]},{"startTime":1531929600000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-19_04","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-19_04","hours":"4"},{"name":"capture_img_192.168.1.114_2018-07-19_06","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-19_06","hours":"6"}]},{"startTime":1532016000000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-20_04","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-20_04","hours":"4"},{"name":"capture_img_192.168.1.114_2018-07-20_06","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-20_06","hours":"6"}]},{"startTime":1532102400000,"pictureMap":[]},{"startTime":1532188800000,"pictureMap":[{"name":"capture_img_192.168.1.114_2018-07-22_09","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-22_09","hours":"9"},{"name":"capture_img_192.168.1.114_2018-07-22_11","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.114_2018-07-22_11","hours":"11"}]}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * startTime : 1530028800000
             * pictureMap : [{"name":"capture_img_192.168.1.113_2018-06-27 16","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.113_2018-06-27 16","hours":"16"},{"name":"capture_img_192.168.1.113_2018-06-27 16","url":"http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.113_2018-06-27 16","hours":"16"}]
             */

            private double startTime;
            private List<PictureMapBean> pictureMap;

            public double getStartTime() {
                return startTime;
            }

            public void setStartTime(double startTime) {
                this.startTime = startTime;
            }

            public List<PictureMapBean> getPictureMap() {
                return pictureMap;
            }

            public void setPictureMap(List<PictureMapBean> pictureMap) {
                this.pictureMap = pictureMap;
            }

            public static class PictureMapBean {
                /**
                 * name : capture_img_192.168.1.113_2018-06-27 16
                 * url : http://p7blgq6id.bkt.clouddn.com/capture_img_192.168.1.113_2018-06-27 16
                 * hours : 16
                 */

                private String name;
                private String url;
                private String hours;
                private String day;
                private int index;
                private boolean flag = false;

                public int getIndex() {
                    return index;
                }

                public void setIndex(int index) {
                    this.index = index;
                }

                public boolean isFlag() {
                    return flag;
                }

                public void setFlag(boolean flag) {
                    this.flag = flag;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getHours() {
                    return hours;
                }

                public void setHours(String hours) {
                    this.hours = hours;
                }
                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }
            }
        }
    }
}
