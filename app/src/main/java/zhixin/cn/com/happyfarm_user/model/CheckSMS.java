package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by DELL on 2018/3/27.
 */

public class CheckSMS {


    /**
     * result : 验证码正确
     * flag : success
     */

    private String result;
    private String flag;
    /**
     * message : 10
     */

    private int message;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result == null ? "" : result;
    }

    public String getFlag() {
        return flag ;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "CheckSMS{" +
                "result='" + result + '\'' +
                ", flag='" + flag + '\'' +
                ", message=" + message +
                '}';
    }
}
