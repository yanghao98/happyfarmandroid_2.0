package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.githang.statusbar.StatusBarCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Page.AllGoodPage;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * 由于原消息页面是一个fragment页面 所以需要一个activity来装载，所以就创建了此方法
 */
public class TheMarketActivtiy extends ABaseActivity {

    private ImageView shoppingCart;
    private String tokenStr;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.the_market_activtiy);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.personal_content,new AllGoodPage())
                .commit();
        SharedPreferences pref = TheMarketActivtiy.this.getSharedPreferences("User_data",MODE_PRIVATE);
        tokenStr = pref.getString("token","");
        shoppingCart = findViewById(R.id.shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(tokenStr)) {
                    noLoginDialog();
                } else {
                    startActivity(new Intent(TheMarketActivtiy.this, ShoppingCartActivity.class));
                }
            }
        });
    }

    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(TheMarketActivtiy.this);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }

                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(TheMarketActivtiy.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    @Override
    public void back(View view) {
        finish();
    }
}
