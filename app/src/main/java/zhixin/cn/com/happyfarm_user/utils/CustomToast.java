package zhixin.cn.com.happyfarm_user.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by HuYueling on 2019/1/2.
 */

public class CustomToast {

    private static Toast mToast;

    public static void showToast(Context mContext, String text) {

        if (null != mToast)
            mToast.cancel();

        mToast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);

        mToast.show();
    }

}
