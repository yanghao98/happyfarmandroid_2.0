package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import java.util.List;

import zhixin.cn.com.happyfarm_user.MyRecipeActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Cooking;
import zhixin.cn.com.happyfarm_user.model.MyRecipe;

public class MyRecipeAdapter extends BaseAdapter {

    private LayoutInflater context;
    private List<Cooking.ResultBean.CookingList> myRecipeList;

    public  MyRecipeAdapter (Context context,List<Cooking.ResultBean.CookingList> myRecipeList) {
        this.context = LayoutInflater.from(context);
        this.myRecipeList = myRecipeList;
        Log.i("传进来的值：", "MyRecipeAdapter: " + JSON.toJSONString(myRecipeList));
    }

    @Override
    public int getCount() {
        return myRecipeList.size();
    }

    @Override
    public Object getItem(int i) {
        return myRecipeList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = context.inflate(R.layout.my_recipe_item,viewGroup,false);
        TextView textView = view.findViewById(R.id.my_recipe_name);
        textView.setText(myRecipeList.get(i).getCookingName());
        return view;
    }
}
