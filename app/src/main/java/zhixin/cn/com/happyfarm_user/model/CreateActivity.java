package zhixin.cn.com.happyfarm_user.model;

public class CreateActivity {

//    "result": {
//        "id": 1,
//                "activityName": "植信农场南京新模范马路店开业庆典",
//                "activityAddres": "南京市鼓楼区新模范马路22-9",
//                "startTime": 1591690513000,
//                "manageId": 1,
//                "activityTime": "2020年6月10号-8月1号",
//                "activityContent": "<html><head><meta charset=\"utf-8\"></head><body><p>1.植信网会对中央门周边地区的用户派发“邀请券”。</p><p>2.每个用户凭“邀请券”可到新模范马路店换取一份免费礼品。</p><p><img style=\"display: inline-block; witch:400px;  height:200px; \" src=\"http://img.trustwusee.com/storeAddress.png\" alt=\"\"/></p></body></html>",
//                "state": 1,
//                "activityImage": "www.trustwusee.com"
//          },
//      "flag": "success"
//    message
//      }

    private ResultBean result;
    private String flag;
    private int message;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public class ResultBean {
        private int id;
        private String activityName;
        private String activityAddres;
        private long startTime;
        private int manageId;
        private String activityTime;
        private String activityContent;
        private int state;
        private String activityImage;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getActivityName() {
            return activityName;
        }

        public void setActivityName(String activityName) {
            this.activityName = activityName;
        }

        public String getActivityAddres() {
            return activityAddres;
        }

        public void setActivityAddres(String activityAddres) {
            this.activityAddres = activityAddres;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public int getManageId() {
            return manageId;
        }

        public void setManageId(int manageId) {
            this.manageId = manageId;
        }

        public String getActivityTime() {
            return activityTime;
        }

        public void setActivityTime(String activityTime) {
            this.activityTime = activityTime;
        }

        public String getActivityContent() {
            return activityContent;
        }

        public void setActivityContent(String activityContent) {
            this.activityContent = activityContent;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getActivityImage() {
            return activityImage;
        }

        public void setActivityImage(String activityImage) {
            this.activityImage = activityImage;
        }
    }
}
