package zhixin.cn.com.happyfarm_user;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.githang.statusbar.StatusBarCompat;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by DELL on 2018/3/17.
 */

public class CompletionPaymentActivity extends ABaseActivity {

    private String LandNo;
    private String LandId;
    private Integer landType;
    private static Map<String,Activity> destoryMap = new HashMap<>();
    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.completion_payment_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this,getResources().getColor(R.color.theme_color));

        Intent intent = getIntent();
        LandNo = intent.getStringExtra("landNo");
        landType = Integer.valueOf(intent.getStringExtra("landType"));
//        Log.e("response1",LandNo);


        findViewById(R.id.back_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(CompletionPaymentActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.see_landvideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.e("CESHI","地号为:"+LandNo);
//                Log.e("CESHI","地ID为:"+LandId);
                dialog = LoadingDialog.createLoadingDialog(CompletionPaymentActivity.this,"请稍后...");
                Call<GetSelfInfo> getSelfInfoCall =  HttpHelper.initHttpHelper().getSelfInfo();
                getSelfInfoCall.enqueue(new Callback<GetSelfInfo>() {
                    @Override
                    public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                        if ("success".equals(response.body().getFlag())){
                            String userId = String.valueOf(response.body().getResult().getId());
                            LandId = String.valueOf(response.body().getResult().getLandId());
                            LandNo = String.valueOf(response.body().getResult().getLandNo());
                            String isLandlord = String.valueOf(response.body().getResult().getIsLandlord());
                            String superUser = String.valueOf(response.body().getResult().getSuperUser());
//                            Log.e("LandId->",LandId);
//                            Log.e("LandNo->",LandNo);
//                            Log.e("UserId->",userId);
//                            Log.e("isLandlord->",isLandlord);
//                            Log.e("superUser->",superUser);

                            if ("".equals(LandId) || "".equals(LandNo)){
                                LoadingDialog.closeDialog(dialog);
                                showToastShort(CompletionPaymentActivity.this, getResources().getString(R.string.no_land));
                                //Toast.makeText(getActivity(),"您尚未购买租地",Toast.LENGTH_SHORT).show();
                            }else {
                                LandId = String.valueOf(Double.valueOf(LandId).intValue());//转换为Int类型
                                LandNo = String.valueOf(Double.valueOf(LandNo).intValue());//转换为Int类型
//                                Log.e("LandId->","转换后："+LandId);
//                                Log.e("LandNo->","转换后："+LandNo);
                                App.destoryActivity("LandVideoActivity");
                                Intent intent = new Intent(CompletionPaymentActivity.this,LandVideoActivity.class);
                                intent.putExtra("state", "0");
                                intent.putExtra("landNo",LandNo);
                                intent.putExtra("UserId",userId);
                                intent.putExtra("landId",LandId);
                                intent.putExtra("isLandlord",isLandlord);
                                intent.putExtra("superUser",superUser+"");
                                intent.putExtra("landType",landType+"");
                                LoadingDialog.closeDialog(dialog);
                                finish();
                                startActivity(intent);
                            }
                        }else {
                            LoadingDialog.closeDialog(dialog);
                            noLoginDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetSelfInfo> call, Throwable t) {
                        LoadingDialog.closeDialog(dialog);
                        try {
                            if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                                noLoginDialog();
                            } else {
                                showToastShort(CompletionPaymentActivity.this,TextString.NetworkRequestFailed);
                            }
                        }catch (Exception e){
//                            Log.e("FarmPage->", "onFailure: "+ e);
                        }
                    }
                });
//                HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
//                    @Override
//                    public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
//                        if (("success").equals(response.body().getFlag())){
//                            String userId = String.valueOf(response.body().getResult().getId());
//                            int landId = (int)response.body().getResult().getLandId();
//                            HttpHelper.initHttpHelper().searchLandByNo(landId).enqueue(new Callback<SearchLandByNo>() {
//                                @Override
//                                public void onResponse(Call<SearchLandByNo> call, Response<SearchLandByNo> responses) {
//                                    LandId = String.valueOf(responses.body().getResult().getId());
//                                    LandNo = String.valueOf(responses.body().getResult().getLandNo());
//                                    HttpHelper.initHttpHelper().searchLandByNos(responses.body().getResult().getLandNo()).enqueue(new Callback<SearchLandByNo>() {
//                                        @Override
//                                        public void onResponse(Call<SearchLandByNo> call, Response<SearchLandByNo> response) {
//                                            zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog.closeDialog(dialog);
//                                            App.destoryActivity("LandVideoActivity");
//                                            finish();
//                                            Intent intent = new Intent(CompletionPaymentActivity.this,LandVideoActivity.class);
//                                            intent.putExtra("state", "0");
//                                            intent.putExtra("landNo",LandNo);
//                                            intent.putExtra("UserId",userId);
//                                            intent.putExtra("landId",LandId);
//                                            startActivity(intent);
//                                        }
//
//                                        @Override
//                                        public void onFailure(Call<SearchLandByNo> call, Throwable t) {
//                                            zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog.closeDialog(dialog);
//                                        }
//                                    });
//                                }
//
//                                @Override
//                                public void onFailure(Call<SearchLandByNo> call, Throwable t) {
//                                    zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog.closeDialog(dialog);
//                                }
//                            });
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<GetSelfInfo> call, Throwable t) {
//                        zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog.closeDialog(dialog);
//                    }
//                });
            }
        });
    }

    public void back(View view){
        finish();
    }
    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog =new AlertUtilBest(CompletionPaymentActivity.this);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(CompletionPaymentActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
}
