package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class LuckDrawDeductionAmount {


    private String result;
    private String flag;
    private DeductionAmountData message;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public DeductionAmountData getMessage() {
        return message;
    }

    public void setMessage(DeductionAmountData message) {
        this.message = message;
    }

    public static class DeductionAmountData{
        int deductionAmount;
        List<LuckDrawRecordList> luckDrawRecordList;

        public int getDeductionAmount() {
            return deductionAmount;
        }

        public void setDeductionAmount(int deductionAmount) {
            this.deductionAmount = deductionAmount;
        }

        public List<LuckDrawRecordList> getLuckDrawRecordLists() {
            return luckDrawRecordList;
        }

        public void setLuckDrawRecordLists(List<LuckDrawRecordList> luckDrawRecordLists) {
            this.luckDrawRecordList = luckDrawRecordLists;
        }

        public static class LuckDrawRecordList {
            int id;
            int userId;
            int luckDrawId;
            String userName;
            String luckyDrawContent;
            int numberPrizes;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getLuckDrawId() {
                return luckDrawId;
            }

            public void setLuckDrawId(int luckDrawId) {
                this.luckDrawId = luckDrawId;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public String getLuckyDrawContent() {
                return luckyDrawContent;
            }

            public void setLuckyDrawContent(String luckyDrawContent) {
                this.luckyDrawContent = luckyDrawContent;
            }

            public int getNumberPrizes() {
                return numberPrizes;
            }

            public void setNumberPrizes(int numberPrizes) {
                this.numberPrizes = numberPrizes;
            }
        }
    }


}
