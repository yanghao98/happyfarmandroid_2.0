package zhixin.cn.com.happyfarm_user.popupMenu.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;

/**
 * PopupMenuAdapter
 *
 * @author: Administrator.
 * @date: 2019/7/12
 */
public class PopupMenuAdapter extends RecyclerView.Adapter<PopupMenuAdapter.StaggerViewHolder> {

    private PopupMenuAdapter.MyItemClickListener mItemClickListener;
    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> mList;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public PopupMenuAdapter(Context context, List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    //创建staggerViewHolder，并把ViewHolder返回出去
    public PopupMenuAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.popup_menu_item, null);
        //创建一个staggerViewHolder对象
        //StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //把staggerViewHolder对象传出去
        return new StaggerViewHolder(view);
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(@NonNull PopupMenuAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        setData(holder,dataBean);
        if (mItemClickListener != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = holder.getLayoutPosition();
                    mItemClickListener.onItemClick(mList,view,pos);
                }
            });
        }
    }

    public void setData(PopupMenuAdapter.StaggerViewHolder holder,zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu data) {
        //名字
        holder.mTextTitle.setText(data.getViewName());
        if (0 == holder.getLayoutPosition()) {
            holder.mView.setVisibility(View.GONE);//下滑线
        }
    }

    /**
     * 提供给Activity刷新数据
     * @param list
     */
    public void updateList(List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> list){
        this.mList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final RelativeLayout mItemView;
        private final TextView mTextTitle;
        private final ImageView mImageView;
        private final View mView;

        public StaggerViewHolder(View itemView) {
            super(itemView);
            mItemView = (RelativeLayout) itemView.findViewById(R.id.menu_item_layout);
            mTextTitle = (TextView) itemView.findViewById(R.id.menu_item_text_view);
            mImageView = (ImageView) itemView.findViewById(R.id.menu_item_image_view);
            mView = itemView.findViewById(R.id.menu_item_view);
        }
    }

    /**
     * 创建一个回调接口
     */
    public interface MyItemClickListener {
        void onItemClick(List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> mList, View view, int position);
    }

    /**
     * 在activity里面adapter就是调用的这个方法,将点击事件监听传递过来,并赋值给全局的监听
     *
     * @param myItemClickListener
     */
    public void setItemClickListener(PopupMenuAdapter.MyItemClickListener myItemClickListener) {
        this.mItemClickListener = myItemClickListener;
    }
}