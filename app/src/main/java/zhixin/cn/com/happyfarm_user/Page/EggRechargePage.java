package zhixin.cn.com.happyfarm_user.Page;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.RechargeAdapter;
import zhixin.cn.com.happyfarm_user.PaymentMethodActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.RechargeList;
import zhixin.cn.com.happyfarm_user.model.SearchDiamondBag;

/**
 * EggRechargePage
 *
 * @author: Administrator.
 * @date: 2019/5/17
 */
public class EggRechargePage extends Fragment {

    private List<RechargeList> mDatas;
    private RechargeAdapter rechargeAdapter;
    private ListView listView;
    private Button rechargeBtn;
    private int positions;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.egg_rech_page_layout,null, false);
        initView(view);
        searchDiamondBag();
        onClick();
        return view;
    }

    private void initView(View view) {
        listView = view.findViewById(R.id.recharge_list);
        rechargeBtn = view.findViewById(R.id.recharge_bt);
    }
    private void onClick() {
        rechargeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!rechargeAdapter.getStates(positions)){
                        Toast.makeText(getContext(),"请选择套餐",Toast.LENGTH_SHORT).show();
                    }else {
                        getActivity().finish();
                        Intent intent = new Intent(getContext(),PaymentMethodActivity.class);
                        intent.putExtra("state","1");
                        intent.putExtra("Money",mDatas.get(positions).getRechargeMoney());
                        intent.putExtra("Name",mDatas.get(positions).getRechargeDiamonds());
                        intent.putExtra("landNo","");
                        startActivity(intent);
                    }
                } catch (NullPointerException e){
                    Toast.makeText(getContext(),"当前无套餐",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    /**
     * 查询钻石套餐接口
     */
    private void searchDiamondBag() {
        Call<SearchDiamondBag> searchDiamondBag = HttpHelper.initHttpHelper().searchDiamondBag();
        searchDiamondBag.enqueue(new Callback<SearchDiamondBag>() {
            @Override
            public void onResponse(Call<SearchDiamondBag> call, Response<SearchDiamondBag> response) {
                mDatas = new ArrayList<RechargeList>();
                if (response.body().getResult().getList()!=null && !response.body().getResult().getList().isEmpty()){
                    for (int i = 0; i < response.body().getResult().getList().size(); i++){
                        RechargeList rechargeList = new RechargeList(JSON.toJSONString(response.body().getResult().getList().get(i).getMoney()),
                                JSON.toJSONString(response.body().getResult().getList().get(i).getDiamondNum()));
                        mDatas.add(rechargeList);
                        initData();
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchDiamondBag> call, Throwable t) {

                Toast.makeText(getContext(),"套餐获取失败",Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void initData(){
        rechargeAdapter = new RechargeAdapter(getContext(), mDatas);
        listView.setAdapter(rechargeAdapter);
        listView.setVisibility(View.VISIBLE);
        setListViewHeightBasedOnChildren(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                transfer(view,i);
            }
        });

    }

    private void transfer(View view,int position) {
        RadioButton mRB = view.findViewById(R.id.recharge_rbt);
        rechargeAdapter.clearStates(position);
        positions = position;
        Log.e("CZ","position:"+position+",states:"+rechargeAdapter.getStates(position));
        mRB.setChecked(true);
        mRB.setChecked(rechargeAdapter.getStates(position));
//        mRB.setText(mDatas.get(position).getMoney());
        Log.e("bool",Boolean.getBoolean(rechargeAdapter.states.toString())+"");
        rechargeAdapter.notifyDataSetChanged();

    }

    public void setListViewHeightBasedOnChildren(ListView listView) {

        Adapter listAdapter = listView.getAdapter();

        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        int viewCount = listAdapter.getCount();
        for (int i = 0; i < viewCount; i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();

        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount()-1)) + 10;//加10是为了适配自定义背景

        listView.setLayoutParams(params);
    }
}
