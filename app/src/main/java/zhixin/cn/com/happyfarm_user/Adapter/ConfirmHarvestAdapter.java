package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.ConfirmHarvest;

/**
 * Created by DELL on 2018/3/21.
 */

public class ConfirmHarvestAdapter extends RecyclerView.Adapter<ConfirmHarvestAdapter.MyViewHolder>{

    private Context context;
    private List<ConfirmHarvest> list=new ArrayList<>();
//    private List<String> lists = new ArrayList<>();
    private int num;

    public ConfirmHarvestAdapter(Context context, List<ConfirmHarvest> list) {
        this.context = context;
        this.list = list;
//        this.lists = lists;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.confirm_harvest_item, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,int position) {
        holder.tv.setText(list.get(position).getShare());
        holder.item_crop.setText(list.get(position).getCropName());
//        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if (list.size() == 1) {
////                    Snackbar.make(v, "此条目不能删除", Snackbar.LENGTH_SHORT).show();
////                } else {}
//                    // 删除口袋
////                    removeData(position);
//                }
//
//        });

        holder.reduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                num = Integer.valueOf(holder.bt_number.getText().toString());
                if (num > 0){
                    list.get(position).setNumber(list.get(position).getNumber()-1);
                    holder.bt_number.setText(String.valueOf(list.get(position).getNumber()));
                }
            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.get(position).setNumber(list.get(position).getNumber()+1);
                holder.bt_number.setText(String.valueOf(list.get(position).getNumber()));
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //  添加数据
    public void addData(View view,int position,String cropName) {
//      在list中添加数据，并通知条目加入一条
//        lists.add(position+"1");
        String isComplete[] = {"一","二","三","四","五","六","七","八","九","十","十一","十二","十三","十四","十五","十六","十七","十八","十九","二十",
                "二十一","二十二","二十三","二十四","二十五","二十六","二十七","二十八","二十九","三十",
                "三十一","三十二","三十三","三十四","三十五","三十六","三十七","三十八","三十九","四十",
                "四十一","四十二","四十三","四十四","四十五","四十六","四十七","四十八","四十九","五十",
                "五十一","五十二","五十三","五十四","五十五","五十六","五十七","五十八","五十九","六十"};

        if (position>isComplete.length){
            Toast.makeText(view.getContext(),"哎呀,口袋不够用了o(*￣▽￣*)o!",Toast.LENGTH_SHORT).show();
        }else {
            ConfirmHarvest confirmHarvest = new ConfirmHarvest();
            confirmHarvest.setShare(isComplete[position-1]+":");
            confirmHarvest.setNumber(1);
            confirmHarvest.setCropName(cropName);
            list.add(confirmHarvest);
            //添加动画
            notifyItemInserted(position);
        }
    }

    //  删除数据
    public void removeData(int position) {
        list.remove(position);
        //删除动画
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv,bt_number,item_crop;
        ImageButton reduce,plus;
        //因为删除有可能会删除中间条目，然后会造成角标越界，所以必须整体刷新一下！
        public MyViewHolder(View view) {
            super(view);
            tv = view.findViewById(R.id.confirm_number);
            reduce = view.findViewById(R.id.confirm_harvest_reduce);
            plus = view.findViewById(R.id.confirm_harvest_plus);
            bt_number = view.findViewById(R.id.harvest_itme_number);
            item_crop = view.findViewById(R.id.item_crop);
        }
    }

}
