package zhixin.cn.com.happyfarm_user.model;

/**
 * Default
 *
 * @author: Administrator.
 * @date: 2019/1/17
 */

public class Default {

    private String result;
    private String flag;
    private Integer errCode;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }
}
