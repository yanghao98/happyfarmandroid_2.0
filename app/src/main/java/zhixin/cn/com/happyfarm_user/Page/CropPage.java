package zhixin.cn.com.happyfarm_user.Page;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.MaterialDialog;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.CropAdapter;
import zhixin.cn.com.happyfarm_user.ConfirmPlantActivity;
import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.PaymentInformationActivity;
import zhixin.cn.com.happyfarm_user.PerfectInformationActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.Crop;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.SearchCropList;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by DELL on 2018/3/16.
 */

public class CropPage extends Fragment {

    private RecyclerView recyclerView;
    private String landId;
    //    private int images[] = {R.mipmap.icon_carrot,R.mipmap.icon_beans,R.mipmap.icon_radish,R.mipmap.icon_tomatoes,R.mipmap.icon_corn,R.mipmap.icon_eggplant};
    private String state;
    private List<Crop> list = new ArrayList<>();
    private String landNo;
    private String UserNickName;
    private String UserImg;
    private String UserTel;
    private String UserCertificatesNo;
    private String UserRealname;
    private int UserPlotId;
    private String UserPlotName;
    private int UserGender;
    private long UserBirthday;
    private String states;
    private RefreshLayout refreshLayout;
    private String UserEmail;
    private int landType;
    private CropAdapter adapter;
    private GridLayoutManager gridLayoutManager;

    @Override
    public void onStart() {
        super.onStart();
        //主要用于刚进来获取用户信息，好做传值
        getUserInfo();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.crop_layout, null,false);
        initView(view);
        initRecyclerView();
        if (!list.isEmpty()){
            list.clear();
        }
        StaggerLoadData(false);
        topRefreshLayout();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        state = ((LandVideoActivity) activity).getState();
        landId = ((LandVideoActivity) activity).getLandId();
        landNo = ((LandVideoActivity) activity).getLandNos();
        landType = ((LandVideoActivity)activity).getLandType();
    }

    /**
     * 初始化布局
     * @param view
     */
    private void initView(View view) {
        recyclerView = view.findViewById(R.id.crop_recycle);
        refreshLayout = (RefreshLayout) view.findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false));
    }

    /**
     * 初始化适配器
     */
    private void initRecyclerView() {
        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new CropAdapter(getContext(), list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter.setItemClickListener(new CropAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (Utils.isFastClick()) {
                    if (1 == list.get(position).state) {
                        goConfirmPlantActivity(position);
                    } else {
                        Toast.makeText(getContext(),"不是当季蔬菜，不能种植",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void goConfirmPlantActivity(int position) {
        HttpHelper.initHttpHelper().test().enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("failed").equals(response.body().getFlag())) {
                    showUserInfoDialog(TextString.DialogTitleText, TextString.DialogContentText);
//                    Toast.makeText(getContext(), "您似乎掉线了呢！", Toast.LENGTH_SHORT).show();
                } else if (state.equals("-1")) {
                    showUserInfoDialog(TextString.DialogTitleText, TextString.DialogContentText);
                } else if (state.equals("1")) {
                    //获取用户信息是否为地主
                    retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
                    getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
                        @Override
                        public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                            if (("success").equals(response.body().getFlag())) {
                                //判断是否是地主。
                                if (response.body().getResult().getIsLandlord() == 1) {
                                    Toast.makeText(getContext(), "您已是地主，您的地在等待您养护哦", Toast.LENGTH_SHORT).show();
                                } else {
                                    perfectInfoDialog();
                                }
                            } else {
                                Toast.makeText(getContext(), response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                            showUserInfoDialog(TextString.DialogTitleText, TextString.DialogContentText);
                        }
                    });
//                            showExitDialog();
                } else {
                    states();
                    if (states.equals("1")) {
                        Toast.makeText(getContext(), "您已处于托管状态", Toast.LENGTH_SHORT).show();
                    } else if (states.equals("0")) {
                        Intent intent = new Intent(getContext(), ConfirmPlantActivity.class);
                        intent.putExtra("cropImg", list.get(position).imageUrl);
                        intent.putExtra("cropName", list.get(position).cropName);
                        intent.putExtra("plantingDiamond", String.valueOf(list.get(position).plantingDiamond));
                        intent.putExtra("plantingIntegral", String.valueOf(list.get(position).plantingIntegral));
                        intent.putExtra("cycle", String.valueOf(list.get(position).cycle));
                        intent.putExtra("details", String.valueOf(list.get(position).details));
                        intent.putExtra("id", String.valueOf(list.get(position).id));
                        intent.putExtra("landId", landId);
                        intent.putExtra("listNumber", String.valueOf(position));
                        startActivity(intent);
                    }
                }
            }
            @Override
            public void onFailure(Call<Test> call, Throwable t) {

            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!list.isEmpty()){
                    list.clear();
                }
                StaggerLoadData(false);
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }

    private void StaggerLoadData(Boolean inversion) {
        Call<SearchCropList> searchCropList = HttpHelper.initHttpHelper().searchCropList(null, landType);
        searchCropList.enqueue(new Callback<SearchCropList>() {
            @Override
            public void onResponse(Call<SearchCropList> call, Response<SearchCropList> response) {
//                Log.i("种养列表", JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())) {
                    for (SearchCropList.ResultBean.ListBean resultBean : response.body().getResult().getList()) {

//                        if (resultBean.getState() == 1) {
                            Crop dataBean = new Crop();
                            dataBean.imageUrl = resultBean.getCropImg();
                            dataBean.cropName = resultBean.getCropName();
                            Log.i("蔬菜名：", "onResponse: " + resultBean.getCropName());
                            dataBean.plantingDiamond = resultBean.getPlantingDiamond();
                            dataBean.plantingIntegral = resultBean.getPlantingIntegral();
                            dataBean.cycle = resultBean.getCycle();
                            dataBean.details = resultBean.getNutritionalComponents();
                            dataBean.id = resultBean.getId();
                            dataBean.startSowing = resultBean.getStartSowing();
                            dataBean.endSeeding = resultBean.getEndSeeding();
                            dataBean.nutritionalComponents = resultBean.getNutritionalComponents();
                            dataBean.state = resultBean.getState();
                            list.add(dataBean);
//                        }
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "获取列表失败", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SearchCropList> call, Throwable t) {

            }
        });

    }

    //完善用户信息layout弹窗
    private void perfectInfoDialog() {
        Dialog bottomDialog = new Dialog(getContext(), R.style.dialog);
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.layout_perfect_information_dialog, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        //设置左右间距
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(getContext(), 100f);
        params.bottomMargin = DensityUtil.dp2px(getContext(), 0f);
        contentView.findViewById(R.id.lease_immediately_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.cancel();
            }
        });
        contentView.findViewById(R.id.lease_immediately_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //获取用户信息是否完善
                retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
                getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
                    @Override
                    public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                        if (("success").equals(response.body().getFlag())) {
                            String titleText = "完善信息";
                            String contentText = "您的信息暂未完善，是否完善您的信息";
                            if (NumUtil.checkNull(response.body().getResult().getRealname())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getCertificatesNo())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getPlotId())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (response.body().getResult().getGender() == 0) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getBirthday())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getTel())) {
                                showUserInfoDialog(titleText, contentText);
                            } else if (NumUtil.checkNull(response.body().getResult().getEmail())){
                                showUserInfoDialog(titleText, contentText);
                            }else {
                                Intent intent = new Intent(getContext(), PaymentInformationActivity.class);
                                intent.putExtra("landNo", landNo);
                                intent.putExtra("landId",landId);
                                intent.putExtra("landType",landType+"");
                                startActivity(intent);
                            }
                        } else {
                            Toast.makeText(getContext(), response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                        Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                    }
                });
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();

    }

    //完善用户信息layout弹窗
    private void showUserInfoDialog(String titleText, String contentText) {
        Dialog bottomDialog = new Dialog(getContext(), R.style.dialog);
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.layout_perfect_information_dialog, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        //设置左右间距
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(getContext(), 100f);
        params.bottomMargin = DensityUtil.dp2px(getContext(), 0f);
        TextView lblTitle = contentView.findViewById(R.id.lease_immediately_bt);
        lblTitle.setText(titleText);
        TextView contentTitle = contentView.findViewById(R.id.user_info_content);
        contentTitle.setText(contentText);
        contentView.findViewById(R.id.lease_immediately_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomDialog.cancel();
//                finish();
            }
        });
        contentView.findViewById(R.id.lease_immediately_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("完善信息".equals(titleText)) {

                    Intent intent = new Intent(getContext(), PerfectInformationActivity.class);
                    intent.putExtra("nickName", UserNickName);
                    intent.putExtra("userImg", UserImg);
                    intent.putExtra("UserRealname", UserRealname);
                    intent.putExtra("landNo", landNo);
                    intent.putExtra("landId",landId);
                    intent.putExtra("tel", UserTel);
                    intent.putExtra("UserPlotName", UserPlotName);
                    intent.putExtra("UserCertificatesNo", UserCertificatesNo);
                    intent.putExtra("UserPlotId", UserPlotId + "");
                    intent.putExtra("UserGender", UserGender + "");
                    intent.putExtra("UserBirthday", UserBirthday + "");
                    intent.putExtra("UserEmail",UserEmail+"");
                    intent.putExtra("landType",landType+"");
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);
                }

                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();

    }

    public void getUserInfo() {
        //获取用户信息是否为地主
        retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (("success").equals(response.body().getFlag())) {
                    UserNickName = response.body().getResult().getNickname();
                    UserImg = response.body().getResult().getHeadImg();
                    UserTel = response.body().getResult().getTel();
                    UserCertificatesNo = response.body().getResult().getCertificatesNo();
                    UserRealname = response.body().getResult().getRealname();
                    UserPlotId = response.body().getResult().getPlotId();
                    UserPlotName = response.body().getResult().getPlotName();
                    UserGender = response.body().getResult().getGender();
                    UserBirthday = response.body().getResult().getBirthday();
                    UserEmail = response.body().getResult().getEmail();
                } else {

                }
            }

            @Override
            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void back(View view) {
//        finish();
    }

    private String states() {
        SharedPreferences pref = getActivity().getSharedPreferences("User_data", MODE_PRIVATE);
        states = pref.getString("states", "");
        return states;
    }

}
