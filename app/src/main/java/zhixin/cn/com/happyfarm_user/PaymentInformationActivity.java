package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.PackageAdapter;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AmountConfiguration;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.LuckDrawDeductionAmount;
import zhixin.cn.com.happyfarm_user.model.LuckDrawRecord;
import zhixin.cn.com.happyfarm_user.model.Programme;
import zhixin.cn.com.happyfarm_user.other.DateUtil;

/**
 * Created by DELL on 2018/3/14.
 */

public class PaymentInformationActivity extends ABaseActivity {

    private ListView listView;
    private List<Programme> mDatas;
    private PackageAdapter packageAdapter;
    private TextView dialog_money;
    private TextView deduction_amount;
    private TextView money;
    private int positions;
    private RadioButton button;
    private String landNo;
    private String landId;
    private List<String> All_lease;
    private RadioButton mRB;
    private Integer landType;
//    private List<LuckDrawRecord.LuckDrawBean.LuckDrawList> list;
    private int deductionAmount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_information_layout);
        //查询用户是否有抽奖券
//        searchLuckDrawRecordList();
        searchLuckDrawDeductionAmount();
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("支付信息");

        View view = getLayoutInflater().inflate(R.layout.package_item,null);

        listView = findViewById(R.id.package_list);
        money = findViewById(R.id.money);
        dialog_money = findViewById(R.id.dialog_money);
        deduction_amount = findViewById(R.id.deduction_amount);
        Button information_bt = findViewById(R.id.information_bt);
        TextView textView = findViewById(R.id.buy_land_no);
        Intent intent = getIntent();
        textView.setText(intent.getStringExtra("landNo"));
        button = view.findViewById(R.id.package_name_rb);
        button.setClickable(false);
        landNo = intent.getStringExtra("landNo");
        landId = intent.getStringExtra("landId");
        landType = Integer.parseInt(intent.getStringExtra("landType"));
//        System.out.println(landType);
        mRB = view.findViewById(R.id.package_name_rb);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);//如果不使用这个设置，选项中的radiobutton无法响应选中事件

        information_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.e("eeee",String.valueOf(packageAdapter.getStates(positions)));
                if (packageAdapter.getStates(positions) == false){
                    Toast.makeText(PaymentInformationActivity.this,"请选择套餐",Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent;
                    //判断该用户有没有优惠券没有的话就直接跳到支付页面
//                    if (0 == list.size()) {
//                        Toast.makeText(PaymentInformationActivity.this,"暂无租地优惠卡",Toast.LENGTH_SHORT).show();
//                        intent = new Intent(PaymentInformationActivity.this,PaymentMethodActivity.class);
//                    } else {
//                        Toast.makeText(PaymentInformationActivity.this,"请选择租地优惠卡",Toast.LENGTH_SHORT).show();
                        intent = new Intent(PaymentInformationActivity.this,SelectDiscountActivity.class);
//                    }
//                    Toast.makeText(PaymentInformationActivity.this,"暂无租地优惠卡",Toast.LENGTH_SHORT).show();
                    Double mm = Double.valueOf(mDatas.get(positions).getMoney()) - deductionAmount;
                    System.out.println("!!!"+mm);
                    intent = new Intent(PaymentInformationActivity.this,PaymentMethodActivity.class);
                    finish();
                    intent.putExtra("state","0");
                    intent.putExtra("Name",mDatas.get(positions).getProgramme());
                    intent.putExtra("Money",mm.toString());
                    intent.putExtra("packageTimes",mDatas.get(positions).getTimes());
                    intent.putExtra("landNo",landNo);
                    intent.putExtra("landId",landId);
                    intent.putExtra("lease",All_lease.get(positions));
                    intent.putExtra("landType",landType+"");
//                    intent.putExtra("luckDrawRecordId",0);
                    intent.putExtra("deductionAmount",deductionAmount);
                    startActivity(intent);
                }
            }
        });
    }

//    private void initView(){
//        listView = findViewById(R.id.package_list);
//    }

    private void initData(){
        //获取用户信息是否完善
        retrofit2.Call<AmountConfiguration> searchAmountConfig = HttpHelper.initHttpHelper().searchAmountConfig(null,landType);
        searchAmountConfig.enqueue(new Callback<AmountConfiguration>() {
            @Override
            public void onResponse(retrofit2.Call<AmountConfiguration> call, Response<AmountConfiguration> response) {
                Log.e("PaymentInformation", JSON.toJSONString(response.body()));
                if (response.body().getFlag().equals("success")){
//                    ArrayList<AmountConfiguration> list = new ArrayList<>();
//                    Log.e("TAG",String.valueOf(response.body().getResult().getList().size()));
                    mDatas = new ArrayList<Programme>();
                    All_lease = new ArrayList<>();
                    SimpleDateFormat dff = new SimpleDateFormat("yyyy/MM/dd");
                    dff.setTimeZone(TimeZone.getTimeZone("GMT+08"));
                    String ee = dff.format(new Date());
                    System.out.print("打印网络时间："+ee);

                    DateUtil dateUtil = new DateUtil();
                    String isComplete[] = {"一","二","三","四","五","六","七","八","九","十","十一","十二","十三","十四","十五","十六","十七","十八","十九","二十",
                            "二十一","二十二","二十三","二十四","二十五","二十六","二十七","二十八","二十九","三十",
                            "三十一","三十二","三十三","三十四","三十五","三十六","三十七","三十八","三十九","四十",
                            "四十一","四十二","四十三","四十四","四十五","四十六","四十七","四十八","四十九","五十",
                            "五十一","五十二","五十三","五十四","五十五","五十六","五十七","五十八","五十九","六十"};
                    for (int i = 0;i<=response.body().getResult().getList().size()-1;i++){
                        Programme programme = new Programme("套餐"+isComplete[i],
                                "3*3㎡",dateUtil.getYearTime(String.valueOf(response.body().getResult().getList().get(i).getLease())),
                                ee + "-"+dateUtil.subMonth(ee,response.body().getResult().getList().get(i).getLease()),
                                String.valueOf(response.body().getResult().getList().get(i).getAmount()));
//                        All_lease = String.valueOf(response.body().getResult().getList().get(i).getLease());
                        All_lease.add(String.valueOf(response.body().getResult().getList().get(i).getLease()));
                        mDatas.add(programme);
                    }
//                    Log.e("TAG",String.valueOf(All_lease));
                    packageAdapter = new PackageAdapter(PaymentInformationActivity.this,mDatas);
                    //设置细胞默认选中第一个
                    packageAdapter.setStates(0,true);

//                    dialog_money.setText("价格："+mDatas.get(0).getMoney()+"元");
                    Double mm = Double.valueOf(mDatas.get(positions).getMoney()) - deductionAmount;
                    money.setText("￥" + mDatas.get(positions).getMoney());
                    dialog_money.setText("￥"+mm.toString());
                    deduction_amount.setText("- ￥"+deductionAmount+".00");

                    listView.setAdapter(packageAdapter);
                    listView.setVisibility(View.VISIBLE);

                    setListViewHeightBasedOnChildren(listView);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            transfer(view,i);
                        }
                    });
                }else {
                    Toast.makeText(PaymentInformationActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<AmountConfiguration> call, Throwable t) {
                Toast.makeText(PaymentInformationActivity.this,"网络错误",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void transfer(View view,int position) {
        packageAdapter.clearStates(position);
        positions = position;
//        Log.e("CZ","position:"+position+",states:"+packageAdapter.getStates(position));
        mRB.setChecked(true);
        mRB.setChecked(packageAdapter.getStates(position));
//        mRB.setText(mDatas.get(position).getMoney());
//        Log.e("bool",Boolean.getBoolean(packageAdapter.states.toString())+"");
        Double mm = Double.valueOf(mDatas.get(position).getMoney()) - deductionAmount;
        money.setText("￥" + mDatas.get(positions).getMoney());
        deduction_amount.setText("￥" + deductionAmount+"0");
        dialog_money.setText("- ￥" + mm.toString());
        packageAdapter.notifyDataSetChanged();

    }


    public void back(View view){
        finish();
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {

        Adapter listAdapter = listView.getAdapter();

        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        int viewCount = listAdapter.getCount();
        for (int i = 0; i < viewCount; i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();

        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount()-1)) + 10;//加10是为了适配自定义背景

        listView.setLayoutParams(params);
    }


    /**
     * 查询优惠券方法
     */
//    public void searchLuckDrawRecordList(){
//        HttpHelper.initHttpHelper().userSearchLuckDrawRecondList(1).enqueue(new Callback<LuckDrawRecord>() {
//            @Override
//            public void onResponse(Call<LuckDrawRecord> call, Response<LuckDrawRecord> response) {
//                if ("success".equals(response.body().getFlag())) {
//                    list = response.body().getResult().getList();
//                } else {
//                }
//            }
//            @Override
//            public void onFailure(Call<LuckDrawRecord> call, Throwable t) {
//            }
//        });
//    }
    private void searchLuckDrawDeductionAmount(){
        HttpHelper.initHttpHelper().searchLuckDrawDeductionAmount().enqueue(new Callback<LuckDrawDeductionAmount>() {
            @Override
            public void onResponse(Call<LuckDrawDeductionAmount> call, Response<LuckDrawDeductionAmount> response) {
                System.out.println("优惠卡");
                System.out.println(JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    deductionAmount = response.body().getMessage().getDeductionAmount();

                } else {
                    Toast.makeText(PaymentInformationActivity.this, "优惠券查询失败", Toast.LENGTH_SHORT).show();
                }
                initData();
            }
            @Override
            public void onFailure(Call<LuckDrawDeductionAmount> call, Throwable t) {

            }
        });
    }
}
