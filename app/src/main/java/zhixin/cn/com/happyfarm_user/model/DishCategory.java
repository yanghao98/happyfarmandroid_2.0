package zhixin.cn.com.happyfarm_user.model;

import android.content.Intent;

import java.util.List;

public class DishCategory {
    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }


    public class ResultBean {
        public List<ListBean> list;

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }
        public class ListBean{
            private Integer id;
            private String categoriesName;
            private int state;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getCategoriesName() {
                return categoriesName;
            }

            public void setCategoriesName(String categoriesName) {
                this.categoriesName = categoriesName;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }
        }
    }

}
