package zhixin.cn.com.happyfarm_user.Page;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.UserLogsAdapter;
import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.SearchIrrationInfo;
import zhixin.cn.com.happyfarm_user.model.UserLogs;
import zhixin.cn.com.happyfarm_user.other.DateUtil;

import static com.mob.tools.utils.DeviceHelper.getApplication;


public class UserLogsPage extends Fragment {

    private RecyclerView recyclerView;
    private RefreshLayout refreshLayout;
    private String state;
    private UserLogsAdapter adapter;
    private ArrayList<UserLogs> list = new ArrayList<>();//集合对象
    private GridLayoutManager gridLayoutManager;
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private RelativeLayout myUserLogPrompt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.userlogs_layout,null,false);
        pageNum = 1;
        pageSize = 15;
        initRecyclerView(view);
        refreshLayout = (RefreshLayout) view.findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false));
        // 设置 Footer 为 球脉冲
        refreshLayout.setRefreshFooter(new ClassicsFooter(getContext()).setSpinnerStyle(SpinnerStyle.Scale));
        state = ((LandVideoActivity)getActivity()).getState();
        myUserLogPrompt = view.findViewById(R.id.my_userlogs_prompt);
        //我的租地才请求这些
        if (state.equals("0")){
            if (list != null){
                list.clear();
            }
            StaggerLoadData(false,1);
            topRefreshLayout();
            bottomRefreshLayout();
            //autoLoadingData();
        }else {
            myUserLogPrompt.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            isLastPage = true;
        }
        return view;
    }
    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.userlogs_recycle);

        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new UserLogsAdapter(getContext(),list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(getContext(),1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
    }
    private void StaggerLoadData(Boolean inversion, int pageNum) {

        retrofit2.Call<SearchIrrationInfo> searchIrrationInfo = HttpHelper.initHttpHelper().searchIrrationInfo(pageNum, pageSize);
        searchIrrationInfo.enqueue(new Callback<SearchIrrationInfo>() {
            @Override
            public void onResponse(retrofit2.Call<SearchIrrationInfo> call, Response<SearchIrrationInfo> response) {
                Log.e("UserLogs",JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())){
                    if (response.body().getResult().getList().isEmpty()){
                        myUserLogPrompt.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        isLastPage = true;
                    }else {
                        myUserLogPrompt.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        isLastPage = response.body().getResult().isIsLastPage();
                        List<SearchIrrationInfo.ResultBean.ListBean> listBeans = response.body().getResult().getList();
                        //给DataBean类放数据，最后把装好数据的DataBean类放到集合里
                        for (int i = 0; i <= listBeans.size() - 1; i++) {
                            UserLogs dataBean = new UserLogs();

                            if ("收成".equals(listBeans.get(i).getMaintainName())) {
                                if (1 == listBeans.get(i).getHarvestWhere()) {
                                    dataBean.taskName = "挂售" + listBeans.get(i).getCropNum() + "份" + listBeans.get(i).getCropName();
                                } else {
                                    dataBean.taskName = "寄回" + listBeans.get(i).getCropNum() + "份" + listBeans.get(i).getCropName();
                                }
                            } else if ("订单".equals(listBeans.get(i).getMaintainName())) {
                                if (0 == listBeans.get(i).getCropNum() || "".equals(listBeans.get(i).getCropName()) || null == listBeans.get(i).getCropName()) {
                                    dataBean.taskName = "购买蔬菜";
                                } else {
                                    dataBean.taskName = "购买" + listBeans.get(i).getCropNum() + "份" + listBeans.get(i).getCropName();
                                }
                            } else if ("种植".equals(listBeans.get(i).getMaintainName())) {
                                dataBean.taskName = listBeans.get(i).getMaintainName() + listBeans.get(i).getCropNum() + "份" + listBeans.get(i).getCropName();
                            } else {
                                dataBean.taskName = listBeans.get(i).getMaintainName();
                            }
                            if (listBeans.get(i).getIsIrration() == 0) {
                                dataBean.isReasonable = "不合理";
                            } else if (listBeans.get(i).getIsIrration() == 1) {
                                dataBean.isReasonable = "合理";
                            } else if (listBeans.get(i).getIsIrration() == -1) {
                                dataBean.isReasonable = "";
                            }
                            if (String.valueOf(listBeans.get(i).getOperationTime()).equals("")) {
                                dataBean.repleaseTime = DateUtil.stampToDate(String.valueOf(response.body().getResult().getList().get(i).getTaskStarTime()));
                            } else {
                                dataBean.repleaseTime = DateUtil.stampToDate(String.valueOf(listBeans.get(i).getOperationTime()));
                            }
                            if (String.valueOf(listBeans.get(i).getOperationState()).equals("-1")) {
                                dataBean.isComplete = "未分配";
                            } else if (String.valueOf(listBeans.get(i).getOperationState()).equals("0")) {
                                dataBean.isComplete = "未完成";
                            } else if (String.valueOf(listBeans.get(i).getOperationState()).equals("1")) {
                                dataBean.isComplete = "已完成";
                            }
                            list.add(dataBean);

                        }
                        adapter.notifyDataSetChanged();
                    }
                }else {
                    myUserLogPrompt.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    Toast.makeText(getContext(),"日志查询失败",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<SearchIrrationInfo> call, Throwable t) {
                myUserLogPrompt.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                Toast.makeText(getContext(),"日志查询失败，请检查是否登录！",Toast.LENGTH_SHORT).show();
                Log.e("TAG",t.getMessage());
            }
        });
    }
    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (list != null) {
                    list.clear();
                }
                isLastPage = false;
                refreshlayout.setNoMoreData(false);//TODO 恢复没有更多数据的原始状态
                pageNum = 1;
                StaggerLoadData(false, 1);
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }
    //上拉加载
    public void bottomRefreshLayout() {
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (isLastPage == true) {
                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
                        } else {
//                            Toast.makeText(getApplication(), "数据加载完毕", Toast.LENGTH_SHORT).show();
                            pageNum++;
                            StaggerLoadData(false, pageNum);
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }
    public void autoLoadingData(){
        //上拉滑动自动请求数据
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                    if(lastVisiblePosition >= gridLayoutManager.getItemCount() - 1){
                        if (isLastPage == true) {
//                            Toast.makeText(getApplication(), "没有更多内容", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            StaggerLoadData(false, pageNum);

                        }
                    }
                }
            }
        });
    }

}
