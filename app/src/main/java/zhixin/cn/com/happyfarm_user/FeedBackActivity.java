package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.xl.ratingbar.MyRatingBar;
import javax.mail.Transport;

import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.mail.SendEMailUtil;
import zhixin.cn.com.happyfarm_user.other.TextChangedListener;
import zhixin.cn.com.happyfarm_user.other.Validator;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * FeedBackActivity
 * 反馈页面
 * @author: Administrator.
 * @date: 2019/4/4
 */

public class FeedBackActivity extends ABaseActivity implements MyRatingBar.OnRatingChangeListener {

    private RelativeLayout feedBackLayout;
    private EditText feedBackText;
    private EditText feedBackContact;
    private MyRatingBar myRatingBar;
    private TextView textView;
    private String startStr = "未评价";

    private RelativeLayout forgetTelLayout;
    private EditText oldTelText;
    private EditText newTelText;

    private String pageActivity;

    private Dialog mDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        TextView title = findViewById(R.id.title_name);
        Intent intent = getIntent();
        pageActivity = intent.getStringExtra("page");

        if ("SettingActivity".equals(pageActivity)) {
            title.setText(getResources().getString(R.string.feed_title));
            initView();
        } else if ("ForgotPasswordActivity".equals(pageActivity)) {
            title.setText(getResources().getString(R.string.feed_title2));
            initView2();
        } else {
            showToastShort(FeedBackActivity.this, "出现错误，返回重试");
        }

    }

    private void initView() {
        feedBackLayout = findViewById(R.id.feed_back_layout);
        feedBackLayout.setVisibility(View.VISIBLE);
        feedBackText = findViewById(R.id.fee_back_edit);
        feedBackContact = findViewById(R.id.tel_edit);
        myRatingBar = findViewById(R.id.feed_rating_bar);
        textView = findViewById(R.id.good_start_text);
        myRatingBar.setOnRatingChangeListener(this);
    }

    private void initView2() {
        forgetTelLayout = findViewById(R.id.forget_tel_layout);
        forgetTelLayout.setVisibility(View.VISIBLE);
        oldTelText = findViewById(R.id.old_tel_edit);
        newTelText = findViewById(R.id.new_tel_edit);
        //限制手机号长度
        TextChangedListener textChangedListener = new TextChangedListener(11, oldTelText, FeedBackActivity.this, TextString.PhoneLong);
        oldTelText.addTextChangedListener(textChangedListener.getmTextWatcher());
        //限制手机号长度
        TextChangedListener textChangedListener2 = new TextChangedListener(11, newTelText, FeedBackActivity.this, TextString.PhoneLong);
        newTelText.addTextChangedListener(textChangedListener2.getmTextWatcher());
    }

    public void submit(View view) {
        if ("SettingActivity".equals(pageActivity)) {
            if ("".equals(feedBackText.getText().toString())) {
                showToastShort(FeedBackActivity.this, "请输入反馈内容");
                return;
            }
            if ("".equals(feedBackContact.getText().toString())) {
                showToastShort(FeedBackActivity.this, "请输入联系方式");
                return;
            }
            sendEmail("来自Android_" + feedBackContact.getText().toString() + "用户反馈",
                    "<b>反馈内容：</b>" + feedBackText.getText().toString() + "\n<br>" + "<b>用户评价：</b>"+ startStr, null);
        } else if ("ForgotPasswordActivity".equals(pageActivity)) {
            if ("".equals(oldTelText.getText().toString())) {
                showToastShort(FeedBackActivity.this, "请输入无法使用的手机号");
                return;
            }
            if ("".equals(newTelText.getText().toString())) {
                showToastShort(FeedBackActivity.this, "请输入在用的联系方式");
                return;
            }
            if (!Validator.isChinaPhoneLegal(oldTelText.getText().toString())){
                showToastShort(FeedBackActivity.this, "无法使用的手机号格式错误");
                return;
            }
            if (!Validator.isChinaPhoneLegal(newTelText.getText().toString())){
                showToastShort(FeedBackActivity.this, "在用的手机号格式错误");
                return;
            }
            if (oldTelText.getText().toString().equals(newTelText.getText().toString())){
                showToastShort(FeedBackActivity.this, "可用和无法使用手机号相同");
                return;
            }
            mDialog = LoadingDialog.createLoadingDialog(FeedBackActivity.this, "邮件发送中...");
            sendEmail("来自Android_" + oldTelText.getText().toString() + "账号无法使用",
                    "<b>问题：</b>" + oldTelText.getText().toString() + "账号该手机号无法使用\n<br>"
                            + "<b>目前可以联系的手机号为：</b>"+ newTelText.getText().toString() + "\n<br>"
                            + "<b>联系用户号码为：</b>" + newTelText.getText().toString(), null);
        } else {
            showToastShort(FeedBackActivity.this, "出现错误，返回重试");
        }
    }

    /**
     * 发送邮件
     * @param sub 主题
     * @param contentText 中心文本
     * @param file 附件 文件举例在方法内
     */
    private void sendEmail(String sub, String contentText, String[] file) {
        new Thread(){
            public void run(){
                Message msg = new Message();
                Bundle data = new Bundle();
                try {
                    //String[] fileList = new String[1];
                    //fileList[0] = "C:\\Users\\HuYueling\\Pictures\\first.png";
                    //创建邮件
                    javax.mail.Message message = SendEMailUtil.createMixedMail(Config.ALI_EMAIL_ADDRESS,
                            sub,
                            contentText,
                            file);
                    Transport ts = SendEMailUtil.getTS();
                    //发送邮件
                    ts.sendMessage(message, message.getAllRecipients());
                    ts.close();
                    data.putString("value","true");
                    msg.setData(data);
                    handler.sendMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                    data.putString("value","false");
                    msg.setData(data);
                    handler.sendMessage(msg);
                }
            }
        }.start();

    }
    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            LoadingDialog.closeDialog(mDialog);
            Bundle data = msg.getData();
            //从data中拿出存的数据
            String val = data.getString("value");
            //将数据进行显示到界面等操作
            if ("true".equals(val)) {
                showDialog("邮件已发送，感谢您的支持");
            } else  { //("false".equals(val))
                showToastShort(FeedBackActivity.this, "邮件发送失败");
            }
        }
    };

    @Override
    public void onRatingChange(MyRatingBar bar, float RatingCount) {
        switch (bar.getId()) {
            case R.id.feed_rating_bar:
                changeByStar(bar,RatingCount,textView);
                break;
        }
    }

    private void changeByStar(MyRatingBar bar,float star,TextView textView){
        Drawable amimia = ContextCompat.getDrawable(this, R.mipmap.amimia);
        Drawable crying = ContextCompat.getDrawable(this, R.mipmap.crying);
        Drawable smiling = ContextCompat.getDrawable(this, R.mipmap.smiling);

        if (star == 1.0f) {
            chage(bar, textView, amimia, crying, getResources().getString(R.string.Worst));
        }else if (star == 2.0f) {
            chage(bar, textView, amimia, crying, getResources().getString(R.string.Bad));
        }else if (star == 3.0f) {
            chage(bar, textView, amimia, smiling, getResources().getString(R.string.Good));
        }else if (star == 4.0f) {
            chage(bar, textView, amimia, smiling, getResources().getString(R.string.Better));
        }else if (star == 5.0f) {
            chage(bar, textView, amimia, smiling, getResources().getString(R.string.Best));
        }
    }

    private void chage(MyRatingBar bar, TextView textView, Drawable empty, Drawable full, String string) {
        textView.setText(string);
        startStr = string;
        bar.setStarEmptyDrawable(empty);
        bar.setStarFillDrawable(full);
    }

    private void showDialog(String msgContext) {
        AlertDialog.Builder setDeBugDialog = new AlertDialog.Builder(this);
        // AlertDialog setDeBugDialog = new AlertDialog.Builder(this).create();//创建对话框
        //获取界面
        View dialogView = LayoutInflater.from(this).inflate(R.layout.layout_addbuddy_dialog, null);
        TextView msgText = dialogView.findViewById(R.id.nei);
        msgText.setText(msgContext);
        //将界面填充到AlertDiaLog容器
        setDeBugDialog.setView(dialogView);
        // 取消点击外部消失弹窗
        setDeBugDialog.setCancelable(false);
        //创建AlertDiaLog
        final AlertDialog customAlert = setDeBugDialog.show();
        customAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        customAlert.getWindow().setLayout(700, 500);

        //设置自定义界面的点击事件逻辑
        dialogView.findViewById(R.id.okBut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customAlert.dismiss();
                finish();
            }
        });

    }

    public void back(View view) {
        finish();
    }
}
