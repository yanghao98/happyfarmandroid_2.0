package zhixin.cn.com.happyfarm_user.utils;

import android.app.Dialog;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.AdapterDate;
import zhixin.cn.com.happyfarm_user.Adapter.AdapterWeek;
import zhixin.cn.com.happyfarm_user.Adapter.OnSignedSuccess;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.View.InnerGridView;
import zhixin.cn.com.happyfarm_user.WaitingDeliveryActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.SignMonthRecord;

/**
 * Created by Administrator on 2017/8/16.
 */

public class SignDate extends LinearLayout {

    private TextView tvYear;
    private InnerGridView gvWeek;
    private InnerGridView gvDate;
    private AdapterDate adapterDate;
    private SignDate signDate;

    public SignDate(Context context) {
        super(context);
        init();
    }

    public SignDate(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SignDate(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        searchSignMonthRecord();
        View view = View.inflate(getContext(), R.layout.layout_signdate,this);
        tvYear = view.findViewById(R.id.tvYear);
        gvWeek = view.findViewById(R.id.gvWeek);
        gvDate = view.findViewById(R.id.gvDate);
//        tvYear.setText(Utils.getCurrentYearAndMonth());
//        gvWeek.setAdapter(new AdapterWeek(getContext()));
//        adapterDate = new AdapterDate(getContext());
//        gvDate.setAdapter(adapterDate);

    }

    /**
     * 签到成功的回调
     * @param onSignedSuccess
     */
    public void setOnSignedSuccess(OnSignedSuccess onSignedSuccess){
        adapterDate.setOnSignedSuccess(onSignedSuccess);
    }

    /**
     *  查询月签到状态
     */
    public void searchSignMonthRecord() {
        HttpHelper.initHttpHelper().searchSignMonthRecord().enqueue(new Callback<SignMonthRecord>() {
            @Override
            public void onResponse(Call<SignMonthRecord> call, Response<SignMonthRecord> response) {
                Log.i("SignDate", "onResponse: "+ JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    if (response.body().getResult().getList().size() == 0) {
                        tvYear.setText(Utils.getCurrentYearAndMonth());
                        gvWeek.setAdapter(new AdapterWeek(getContext()));
                        adapterDate = new AdapterDate(getContext());
                        gvDate.setAdapter(adapterDate);
//                        Log.i("", "onResponse: 没有数据");
                    } else {
                        tvYear.setText(Utils.getCurrentYearAndMonth());
                        gvWeek.setAdapter(new AdapterWeek(getContext()));
                        adapterDate = new AdapterDate(getContext(),response.body().getResult().getList());
                        gvDate.setAdapter(adapterDate);
//                        Log.i("", "onResponse: 有数据");
                    }
//                    signDate = findViewById(R.id.signDate);
//                    signDate.setOnSignedSuccess(new OnSignedSuccess() {
//                        @Override
//                        public void OnSignedSuccess() {
//                            Log.e("wqf","Success");
//                        }
//                    });
                } else {

                }

            }

            @Override
            public void onFailure(Call<SignMonthRecord> call, Throwable t) {
            }
        });
    }
}
