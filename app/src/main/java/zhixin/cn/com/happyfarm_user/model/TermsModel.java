package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * TermsModel
 *
 * @author: Administrator.
 * @date: 2019/1/22
 */

public class TermsModel {
    /**
     * result : [{"id":1,"name":"d","terms":"d","startTime":1548124994000,"updateTime":1548124994000,"managerId":0,"state":1}]
     * flag : success
     */

    private String flag;
    private List<ResultBean> result;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * id : 1
         * name : d
         * terms : d
         * startTime : 1548124994000
         * updateTime : 1548124994000
         * managerId : 0
         * state : 1
         */

        private int id;
        private String name;
        private String terms;
        private long startTime;
        private long updateTime;
        private int managerId;
        private int state;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTerms() {
            return terms;
        }

        public void setTerms(String terms) {
            this.terms = terms;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getManagerId() {
            return managerId;
        }

        public void setManagerId(int managerId) {
            this.managerId = managerId;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }
}
