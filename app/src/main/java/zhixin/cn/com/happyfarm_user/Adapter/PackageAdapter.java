package zhixin.cn.com.happyfarm_user.Adapter;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import java.util.HashMap;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Programme;


/**
 * Created by DELL on 2018/3/14.
 */

public class PackageAdapter extends BaseAdapter{

    private LayoutInflater mInflater;
    private List<Programme> mDatas;
    public HashMap<String,Boolean> states=new HashMap<String,Boolean>();     //用于记录每个RadioButton的状态，并保证只可选一个
    private int checkedIndex = -1;

    public PackageAdapter(Context context,List<Programme> datas){

        mInflater = LayoutInflater.from(context);
        mDatas = datas;

    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null){
            convertView = mInflater.inflate(R.layout.package_item,parent,false);
            holder = new ViewHolder();

            holder.programmeTv = convertView.findViewById(R.id.programme_name);
            holder.acreageTv = convertView.findViewById(R.id.package_acreage);
            holder.termTv = convertView.findViewById(R.id.package_term);
            holder.timesTv = convertView.findViewById(R.id.package_times);
            holder.money = convertView.findViewById(R.id.package_money);

            convertView.setTag(holder);
        }else {

            holder = (ViewHolder) convertView.getTag();

        }

        RadioButton radio = convertView.findViewById(R.id.package_name_rb);
        holder.radiobt = radio;

        radio.setClickable(false);

        Programme programme = mDatas.get(position);
        holder.programmeTv.setText(programme.getProgramme());
        holder.acreageTv.setText(programme.getAcreage());
        holder.termTv.setText(programme.getTerm());
        holder.timesTv.setText(programme.getTimes());
        holder.money.setText(programme.getMoney());
        holder.radiobt.setFocusable(false);

//        holder.radiobt.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//
//                //重置，确保最多只有一项被选中
//                for(String key:states.keySet()){
//                    states.put(key, false);
//                }
//                states.put(String.valueOf(position), radio.isChecked());
////                Log.e("erro","点击列数:"+String.valueOf(position)+"  states状态:"+states.get(String.valueOf(position)));
//                PackageAdapter.this.notifyDataSetChanged();
//            }
//        });

        boolean res  = false;
        if (states.get(String.valueOf(position)) == null || states.get(String.valueOf(position))== false){
            res = false;
            states.put(String.valueOf(position),false);
        }else res = true;


        holder.radiobt.setChecked(res);

        return convertView;
    }

    public class ViewHolder{
        RadioButton radiobt;
        TextView programmeTv;
        TextView acreageTv;
        TextView termTv;
        TextView timesTv;
        TextView money;
    }

    //用于在activity中重置所有的radiobutton的状态
    public void clearStates(int position){
        // 重置，确保最多只有一项被选中
        for(String key:states.keySet()){
            states.put(key,false);
        }
        states.put(String.valueOf(position), true);

    }
    //用于获取状态值
    public Boolean getStates(int position){
        return states.get(String.valueOf(position));
    }
    //设置状态值
    public void setStates(int position,boolean isChecked){
        states.put(String.valueOf(position),isChecked);
    }

}
