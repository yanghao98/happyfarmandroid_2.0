package zhixin.cn.com.happyfarm_user;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.ConfirmHarvestAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.ListDialogAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.ConfirmHarvest;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.SearchCropList;
import zhixin.cn.com.happyfarm_user.model.searchReceivingAddress;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.utils.CashierInputFilter;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by DELL on 2018/3/21.
 */

public class ConfirmHavestActivity extends ABaseActivity {

    int number = 0;
    String num;
    private ConfirmHarvestAdapter confirmHarvestAdapter;
    private List<ConfirmHarvest> lists = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private String UserName;
    private String UserTel;
    private String plotName;
    private String cropId;
    private String cropName;
    private String landid;
    private int payType;
    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;
    private ArrayList<Integer> numberList;
    private String numberLists;
    private TextView setnumber;
    private List<Integer> Idlist;
    private List<String> addressIdList;
    private List<Integer> plotIdList;
    private Dialog mDialog;
    private TextView plotname;
    private int poss;
    private int plotId;
    //收成
    //private Double harvestIntegral;
    private int harvestDiamond;
    //挂售
    //private Double storeSaleIntegral;
    private Double storeSaleDiamond;
    private TextView username;
    private TextView usertel;
    private EditText commodityPrice;
    private int MIN_MARK = 0;
    private int MAX_MARK = 99;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_harvest_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText(getResources().getString(R.string.havest_title));

        //寄回
        setnumber = findViewById(R.id.confirm_harvest_number);
        mRecyclerView = findViewById(R.id.confirm_harvest_recyler);
//        RelativeLayout relativeLayout = findViewById(R.id.add_pocket); //添加口袋
//        commodityPrice = findViewById(R.id.commodity_price);
        commodityPrice = (EditText) findViewById(R.id.commodity_price);
        InputFilter[] filters = {new CashierInputFilter()};
        commodityPrice.setFilters(filters);

        username = findViewById(R.id.confirm_username);
        usertel = findViewById(R.id.confirm_usertel);
        plotname = findViewById(R.id.confirm_plotName);
        TextView cropname = findViewById(R.id.confirm_cropname);
        Button confirm_adress = findViewById(R.id.confirm_adress);
        Intent intent = getIntent();
        cropId = intent.getStringExtra("cropId");
        cropName = intent.getStringExtra("cropname");
        landid = intent.getStringExtra("landid");
        harvestDiamond = intent.getIntExtra("harvestDiamond",0);

        findViewById(R.id.confirm_harvest_plus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = number + 1;
                num = String.valueOf(number);
                setnumber.setText(num);
            }
        });

        findViewById(R.id.confirm_harvest_reduce).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (number >= 1){
                    number = number - 1;
                    num = String.valueOf(number);
                    setnumber.setText(num);
                }
            }
        });

        confirm_adress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog = LoadingDialog.createLoadingDialog(ConfirmHavestActivity.this,"正在加载地址...");

                HttpHelper.initHttpHelper().searchReceivingAddress().enqueue(new Callback<searchReceivingAddress>() {
                    @Override
                    public void onResponse(Call<searchReceivingAddress> call, Response<searchReceivingAddress> response) {
                        if (("success").equals(response.body().getFlag())){
                            Idlist = new ArrayList<>();
                            addressIdList = new ArrayList<>();
                            plotIdList = new ArrayList<>();
                            for (int i = 0;i<response.body().getResult().size();i++){
                                Idlist.add(response.body().getResult().get(i).getId());
                                addressIdList.add(response.body().getResult().get(i).getAddress());
                                plotIdList.add(response.body().getResult().get(i).getPlotId());
                            }
                            LoadingDialog.closeDialog(mDialog);
                            ShowDialogs(addressIdList,Idlist,plotIdList,"地址");
                        } else {
                            LoadingDialog.closeDialog(mDialog);
                            showToastShort(ConfirmHavestActivity.this, getResources().getString(R.string.loading_failed));
                        }
                    }

                    @Override
                    public void onFailure(Call<searchReceivingAddress> call, Throwable t) {
                        showToastShort(ConfirmHavestActivity.this, TextString.NetworkRequestFailed);
                    }
                });

            }
        });
        userInfo();//TODO 获取用户信息
        cropname.setText(cropName);

        if (lists.size() == 0){
            initRecycle();
        }
        //添加口袋
//        relativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                confirmHarvestAdapter.addData(view,lists.size()+1,cropName);
//            }
//        });

        findViewById(R.id.confirm_harvest_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mDialog = LoadingDialog.createLoadingDialog(ConfirmHavestActivity.this,"正在加载...");
                int ReturnDiamondtotal = 0;
                Double SellIntegral = 0.0D;
                //总金额
                Double SellDiamondtotal = 0.0D;
                if (Integer.valueOf(setnumber.getText().toString()) == 0) {
                    Log.i("没有寄回", "onClick: ");
                } else {
                    //寄回金额
                    ReturnDiamondtotal = harvestDiamond * Integer.valueOf(setnumber.getText().toString());
                }
                if (0 == lists.get(0).getNumber()) {
                    Log.i("没有挂售", "onClick: ");

                } else {
                    Log.i("商品单价", "onClick: " + commodityPrice.getText());
                    if (Double.valueOf(commodityPrice.getText().toString()) == 0) {
                        Toast.makeText(ConfirmHavestActivity.this,"商品单价不能为0！",Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        SellIntegral = Double.valueOf(commodityPrice.getText().toString()) * lists.get(0).getNumber();
                    }
                }
//                corpMoney(cropName);
                Log.i("", "寄回金额："+ ReturnDiamondtotal+"挂售价金额："+SellIntegral+"总金额："+SellDiamondtotal);
                SellDiamondtotal = ReturnDiamondtotal + SellIntegral;
                if (0 == SellDiamondtotal) {
                    Toast.makeText(ConfirmHavestActivity.this,"请选择寄回还是挂售！",Toast.LENGTH_SHORT).show();
                } else {
                    perfectInfoDialog(SellDiamondtotal);
                }

            }
        });
    }

    private void initRecycle() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        lists = initData();
        confirmHarvestAdapter = new ConfirmHarvestAdapter(ConfirmHavestActivity.this,lists);
        mRecyclerView.setAdapter(confirmHarvestAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    protected List<ConfirmHarvest> initData() {
        ArrayList<ConfirmHarvest> mDatas = new ArrayList<ConfirmHarvest>();
        int nus = 1;
//      mDatas.add(""+nus);
//        Log.e("list",String.valueOf(lists.size()));
        ConfirmHarvest confirmHarvest = new ConfirmHarvest();
        confirmHarvest.setShare("一:");
        confirmHarvest.setNumber(0);
        confirmHarvest.setCropName(cropName);
        mDatas.add(confirmHarvest);
        return mDatas;
    }

    private void userInfo(){
        Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (("success").equals(response.body().getFlag())){
                    UserName = response.body().getResult().getRealname();
                    UserTel = response.body().getResult().getTel();
                    plotName = response.body().getResult().getPlotName();

                    username.setText(UserName);
                    usertel.setText(UserTel);
                    plotname.setText(plotName);
                }else {
                    showToastShort(ConfirmHavestActivity.this, getResources().getString(R.string.get_user_info_failed));
                }
            }

            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }
    public void back(View view){
        finish();
    }

    public void Harvest(int payType,int sendBackNum,String sellNum,int landId,int cropId,int plotId,double storeSale ){
//        Log.e("ERRO",String.valueOf(sendBackNum));
        Log.e("","支付类型："+payType+" 寄回份数："+sendBackNum+" 挂售份数："+sellNum+" 菜品单价:"+storeSale+" 租地id:"+landId+" 菜品id:"+cropId+"plotId:"+plotId);
        mDialog = LoadingDialog.createLoadingDialog(ConfirmHavestActivity.this,"正在提交任务！");
        Call<CheckSMS> addUserTasks = HttpHelper.initHttpHelper().addUserTasks(payType,2,sendBackNum,sellNum,storeSale,landId,cropId,plotId);
        addUserTasks.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                Log.e("ERRO", JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())){
                    showToastShort(ConfirmHavestActivity.this, getResources().getString(R.string.tasks_success));
                    finish();
                }else {
                    if (("您的积分不足请获取").equals(response.body().getResult())){
                        showToastShort(ConfirmHavestActivity.this, getResources().getString(R.string.not_Integral));
                    }else {
                        showToastShort(ConfirmHavestActivity.this, response.body().getResult());
                    }

                }
                LoadingDialog.closeDialog(mDialog);
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
//                Log.e("ERRO",t.getMessage());
                LoadingDialog.closeDialog(mDialog);
            }
        });
    }
//    public void corpMoney(String corpName) {
//        Call<SearchCropList> searchCropList = HttpHelper.initHttpHelper().searchCropList(corpName, null);
//        searchCropList.enqueue(new Callback<SearchCropList>() {
//            @Override
//            public void onResponse(Call<SearchCropList> call, Response<SearchCropList> response) {
//                Log.e("corpMoney", JSON.toJSONString(response.body()));
//                if (("success").equals(response.body().getFlag())) {
//                    LoadingDialog.closeDialog(mDialog);
//                    if (response.body().getResult().getList().size() > 0) {
//                        //收成
//                        //harvestIntegral = response.body().getResult().getList().get(0).getHarvestIntegral();
//                        harvestDiamond = response.body().getResult().getList().get(0).getHarvestDiamond();
//                        //挂售
//                        //storeSaleIntegral = response.body().getResult().getList().get(0).getStoreSaleIntegral();
//                        storeSaleDiamond = response.body().getResult().getList().get(0).getStoreSale();
//                        numberList = new ArrayList<>();
//                        for (int j = 0;j<lists.size();j++){
//                            numberList.add(lists.get(j).getNumber());
//                        }
//                        Double ReturnDiamondtotal = harvestDiamond*Integer.valueOf(setnumber.getText().toString());
//                        //Double ReturntIntegral = harvestIntegral*Integer.valueOf(setnumber.getText().toString());
//                        Double SellIntegral = 0.0;
//                        Double SellDiamondtotal = 0.0;
////                    Log.e("numberList",String.valueOf(numberList));
//                        //Log.e("storeSaleIntegral",String.valueOf(storeSaleIntegral));
////                    Log.e("storeSaleDiamond",String.valueOf(storeSaleDiamond));
//                        for (int i = 0;i<numberList.size();i++){
////                        SellIntegral = numberList.get(i)*storeSaleIntegral+SellIntegral;
//                            SellDiamondtotal = numberList.get(i)*storeSaleDiamond+SellDiamondtotal;
//                        }
//
////                    Log.e("ss",String.valueOf(ReturnDiamondtotal)+","+SellIntegral+","+SellDiamondtotal);
//                        Double countDiamond = ReturnDiamondtotal+SellDiamondtotal;
//                        if (0 == countDiamond) {
//                            Toast.makeText(ConfirmHavestActivity.this,"请输入菜品数量",Toast.LENGTH_SHORT).show();
//                        } else {
//                            perfectInfoDialog(countDiamond);
//                        }
//                    } else  {
//                        Toast.makeText(ConfirmHavestActivity.this,"暂无此作物收货",Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//
//                }
//            }
//
//
//            @Override
//            public void onFailure(Call<SearchCropList> call, Throwable t) {
//
//            }
//        });
//    }


    //确认金额弹框
    @SuppressLint("SetTextI18n")
    private void perfectInfoDialog(Double countDiamond) {

        Dialog bottomDialog = new Dialog(ConfirmHavestActivity.this, R.style.dialog);
        View contentView = LayoutInflater.from(ConfirmHavestActivity.this).inflate(R.layout.layout_perfect_information_dialog, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        //设置左右间距
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(ConfirmHavestActivity.this, 100f);
        params.bottomMargin = DensityUtil.dp2px(ConfirmHavestActivity.this, 0f);
        TextView lblMessage = contentView.findViewById(R.id.perfect_mes);
        lblMessage.setText("提示");
        TextView lblTitle = contentView.findViewById(R.id.lease_immediately_bt);
        lblTitle.setText("确认收获");
        TextView contentTitle = contentView.findViewById(R.id.user_info_content);
        contentTitle.setText("您本次收获将消耗"+ countDiamond +" 元");

        contentView.findViewById(R.id.lease_immediately_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.cancel();
            }
        });
        contentView.findViewById(R.id.lease_immediately_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //payType为支付类型（0为钻石支付 1为积分支付）
                // ,当为2收成时sendBackNum(寄回份数）
                // sellNum(挂售份数 格式:[2,3]
                // 一份为数量为2 一份数量为3),当为1种植和3操作指令sendBackNum给0，sellNum给[] ,
                // command参数：
                // 1为种植（种植不要填写maintainId、plotId)
                // 2收成 (收成不要填maintainId 若有寄回则需填写plotId)
                // 3操作指令（养护指令）(操作指令不需要填cropNum、plotId )
//                numberList = new ArrayList<>();
//                for (int j = 0;j<lists.size();j++){
//                    numberList.add(lists.get(j).getNumber());
//                }
                numberLists = String.valueOf(numberList);
                String storeSale = commodityPrice.getText().toString();
//                if (setnumber.getText().toString().equals("0")){
//                    System.out.println("挂售价格内容打印"+storeSale+"  挂售份数内容打印"+lists.get(0).getNumber());
//                    if (!"".equals(storeSale) && lists.get(0).getNumber() != 0) {
//                        plotId = 0;
////                    Harvest(payType,Integer.valueOf(setnumber.getText().toString()),numberLists,Integer.valueOf(landid),Integer.valueOf(cropId),plotId);
//                        Harvest(payType,Integer.valueOf(setnumber.getText().toString()),String.valueOf(lists.get(0).getNumber()),Integer.valueOf(landid),Integer.valueOf(cropId),plotId,Double.valueOf(storeSale));
//                    } else {
//                        Toast.makeText(ConfirmHavestActivity.this,"挂售价格不能为空",Toast.LENGTH_SHORT).show();
//                    }
//                } else {
                    GetSelfInfo(commodityPrice.getText().toString());
//                }
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
        //从下往上的动画
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    public void ShowDialogs(List<String> list,List<Integer> idlist,List<Integer> plotIdList,String titles){
        Context context = ConfirmHavestActivity.this;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_dialog, null);
        ListView myListView =layout.findViewById(R.id.formcustomspinner_list);
        ListDialogAdapter adapter = new ListDialogAdapter(context, list);
        myListView.setAdapter(adapter);
        TextView title = layout.findViewById(R.id.label);
        title.setText(titles);
        layout.findViewById(R.id.list_dialog_bt).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mDialog = LoadingDialog.createLoadingDialog(ConfirmHavestActivity.this,"稍后");
                HttpHelper.initHttpHelper().updateReceivingAddress(idlist.get(i),plotIdList.get(i),"1").enqueue(new Callback<CheckSMS>() {
                    @Override
                    public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                        Log.e("地址。。",JSON.toJSONString(response.body()));
                        if (("success").equals(response.body().getFlag())){
                            LoadingDialog.closeDialog(mDialog);
                            showToastShort(ConfirmHavestActivity.this, getResources().getString(R.string.update_success));
                            poss = i;
                            plotname.setText(list.get(i));
                            alertDialog.cancel();
                        }else {
                            LoadingDialog.closeDialog(mDialog);
                            showToastShort(ConfirmHavestActivity.this, getResources().getString(R.string.update_failed));
                            alertDialog.cancel();
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckSMS> call, Throwable t) {
                        LoadingDialog.closeDialog(mDialog);
                    }
                });
            }
        });
        builder = new AlertDialog.Builder(context);
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void GetSelfInfo(String storeSale){
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (("success").equals(response.body().getFlag())){
                    plotId = response.body().getResult().getPlotId();
                    Harvest(payType,Integer.valueOf(setnumber.getText().toString()),String.valueOf(lists.get(0).getNumber()),Integer.valueOf(landid),Integer.valueOf(cropId),plotId,Double.valueOf(storeSale));
                }
            }

            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }
    /**
     * 点击空白区域隐藏键盘.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        if (me.getAction() == MotionEvent.ACTION_DOWN) {  //把操作放在用户点击的时候
            View v = getCurrentFocus();      //得到当前页面的焦点,ps:有输入框的页面焦点一般会被输入框占据
            if (isShouldHideKeyboard(v, me)) { //判断用户点击的是否是输入框以外的区域
                hideKeyboard(v.getWindowToken());   //收起键盘
            }
        }
        return super.dispatchTouchEvent(me);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {  //判断得到的焦点控件是否包含EditText
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],    //得到输入框在屏幕中上下左右的位置
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击位置如果是EditText的区域，忽略它，不收起键盘。
                return false;
            } else {
                return true;
            }
        }
        // 如果焦点不是EditText则忽略
        return false;
    }
    /**
     * 获取InputMethodManager，隐藏软键盘
     * @param token
     */
    private void hideKeyboard(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
