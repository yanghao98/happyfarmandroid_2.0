package zhixin.cn.com.happyfarm_user;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.HelpAdapter;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.HelpList;
import zhixin.cn.com.happyfarm_user.model.Helps;

public class HelpActivity extends AppCompatActivity {
    private String TAG = "HelpActivity";
    private List<Helps> helpsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_activity_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        Log.i(TAG, "onCreate: 来到了帮助页面");
        searchHelpList();
    }

    //查询设置列表
    public void searchHelpList() {
        HttpHelper.initHttpHelper().searchHelp().enqueue(new Callback<HelpList>() {
            @Override
            public void onResponse(Call<HelpList> call, Response<HelpList> response) {
                if ("success".equals(response.body().getFlag())) {
//                    Log.i(TAG, "onResponse: "+ JSON.toJSONString(response.body().getResult().getList()));
                    for (int i = 0;i < response.body().getResult().getList().size(); i++) {
                        Helps helps = new Helps();
                        helps.setId(response.body().getResult().getList().get(i).getId());
                        helps.setHelpName(response.body().getResult().getList().get(i).getHelpName());
                        helps.setHelpUrl(response.body().getResult().getList().get(i).getHelpUrl());
                        helps.setState(response.body().getResult().getList().get(i).getState());
                        helpsList.add(helps);
                    }
                    initHelpAdapter();
                }
            }
            @Override
            public void onFailure(Call<HelpList> call, Throwable t) {

            }
        });
    }

    public void initHelpAdapter() {
        //创建列表
        setContentView(R.layout.help_activity_layout);
        TextView titleName = findViewById(R.id.title_name);
        titleName.setText("操作说明");
        //创建适配器
        HelpAdapter helpAdapter = new HelpAdapter(HelpActivity.this,R.layout.help_item,helpsList);
        ListView listView = findViewById(R.id.help_listView);
        listView.setAdapter(helpAdapter);
    }

    public void back(View view) {
        finish();
    }
    /**
     * 重写返回键
     * 监听Back键按下事件
     * super.onBackPressed()会自动调用finish()方法,关闭当前Activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
