package zhixin.cn.com.happyfarm_user.popupMenu;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.popupMenu.adapter.PopupMenuAdapter;
import zhixin.cn.com.happyfarm_user.popupMenu.utils.RecyclerViewCornerRadius;

public class PopupMenu extends PopupWindow {

	private Activity activity;
	private View popView;
	private PopupMenuAdapter adapter;
	private List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> list = new ArrayList<>();
	private GridLayoutManager gridLayoutManager;
	private RecyclerView recyclerView;

	public PopupMenu(Activity activity , List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> list) {
		super(activity);
		this.activity = activity;
		this.list = list;
		LayoutInflater inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		popView = inflater.inflate(R.layout.popup_menu, null);// 加载菜单布局文件
		this.setContentView(popView);// 把布局文件添加到popupwindow中
		this.setWidth(dip2px(activity, 100));// 设置菜单的宽度（需要和菜单于右边距的距离搭配，可以自己调到合适的位置）
//		this.setHeight(dip2px(activity, 145));
		this.setFocusable(true);// 获取焦点
		this.setTouchable(true); // 设置PopupWindow可触摸
		this.setOutsideTouchable(true); // 设置非PopupWindow区域可触摸
		ColorDrawable dw = new ColorDrawable(0x00000000);
		this.setBackgroundDrawable(dw);
		initRecyclerView();
	}

	/**
	 * 初始化内部item布局
	 */
	private void initRecyclerView() {
		//获取recyclerView
		recyclerView = (RecyclerView) popView.findViewById(R.id.recycler_view);
		//创建适配器Adapter对象  参数：1.上下文2.数据加载集合
		adapter = new PopupMenuAdapter(activity, list);
		//设置适配器
		recyclerView.setAdapter(adapter);
		//GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
		gridLayoutManager = new GridLayoutManager(activity, 1);
		//通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
		gridLayoutManager.setReverseLayout(false);
		recyclerView.setItemAnimator(new DefaultItemAnimator());
		//设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
		recyclerView.setLayoutManager(gridLayoutManager);
		RecyclerViewCornerRadius radiusItemDecoration = new RecyclerViewCornerRadius(recyclerView);
		radiusItemDecoration.setCornerRadius(dip2px(activity, 6));
		recyclerView.addItemDecoration(radiusItemDecoration);
	}

	/**
	 * 设置布局item
	 * @param list 数组内容
	 */
	public void setPopViewItemNum(List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> list) {
		Log.i("popup->", "setPopViewItemNum: " + list);
		adapter.updateList(list);
	}

	/**
	 * 设置PopView高度
	 * @param height 高度值
	 */
	public void setPopViewHeight(int height) {
		setHeight(dip2px(activity, height));
	}

	/**
	 * 设置PopView宽度
	 * @param width 宽度值
	 */
	public void setPopViewWidth(int width) {
		setWidth(dip2px(activity, width));
	}

	/**
	 * 设置PopView宽度和高度
	 * @param width 宽度值
	 * @param height 高度值
	 */
	public void setPopViewWidthAndHeight(int width, int height) {
		setHeight(dip2px(activity, height));
		setWidth(dip2px(activity, width));
	}

	/**
	 * 设置显示的位置
	 * @param resourceId 布局元素id 固定弹出窗口的视图
	 */
	public void showLocation(int resourceId) {
		showAsDropDown(activity.findViewById(resourceId), dip2px(activity, 0),
				dip2px(activity, -8));
	}

	/**
	 * 设置显示的位置
	 * @param resourceId 布局元素id
	 * @param x 距定位点的水平偏移量（像素）
	 * @param y 距定位点的垂直偏移量（像素）
	 */
	public void showLocation(int resourceId, int x, int y) {
		showAsDropDown(activity.findViewById(resourceId), dip2px(activity, x),
				dip2px(activity, y));
	}

	/**
	 * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
	 */
	public int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	public PopupMenuAdapter getAdapter() {
		return adapter;
	}

	/**
	 * 设置添加屏幕的背景透明度
	 * @param bgAlpha
	 */
	public void backgroundAlpha(Activity context, float bgAlpha) {
		WindowManager.LayoutParams lp = context.getWindow().getAttributes();
		lp.alpha = bgAlpha;
		context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		context.getWindow().setAttributes(lp);
	}

	@Override
	public void dismiss() {
		super.dismiss();
		backgroundAlpha(activity, 1f);
	}
}
//	private PopupMenu popupMenu;
	//点击菜单图片显示
//	private void showPopBottom() {
//        zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu popupMenuModel = new zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu();
//        popupMenuModel.setViewName("我 的 租 地");
//        zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu popupMenuMode2 = new zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu();
//        popupMenuMode2.setViewName("我 的 动 态");
//        zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu popupMenuMode3 = new zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu();
//        popupMenuMode3.setViewName("我 的 收 藏");
//        List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> list = new ArrayList<>();
//        list.add(popupMenuModel);
//        list.add(popupMenuMode2);
//        list.add(popupMenuMode3);
//        popupMenu = new PopupMenu(activity, list);
//        popupMenu.showLocation(R.id.home_toolbar);//显示的位置
//        popupMenu.getAdapter().setItemClickListener(new PopupMenuAdapter.MyItemClickListener() {
//            @Override
//            public void onItemClick(List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> mList, View view, int position) {
//                CustomToast.showToast(activity,String.valueOf(position));
//                popupMenu.dismiss();//隐藏
//            }
//        });
//	}