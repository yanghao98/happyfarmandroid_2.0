package zhixin.cn.com.happyfarm_user.Page;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.youth.banner.Banner;

import java.util.ArrayList;
import java.util.List;

import me.kareluo.ui.OptionMenu;
import me.kareluo.ui.OptionMenuView;
import me.kareluo.ui.PopupMenuView;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.CookingListAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.LandInfoAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.ShadowTransformer;
import zhixin.cn.com.happyfarm_user.Adapter.VegetablesCardAdapter;
import zhixin.cn.com.happyfarm_user.BreedingTutorialPageActivity;
import zhixin.cn.com.happyfarm_user.CookingRecommendedActivity;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.HistoryPageActivity;
import zhixin.cn.com.happyfarm_user.LandListActivity;
import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.MyCollectionActivity;
import zhixin.cn.com.happyfarm_user.MyDynamicActivity;
import zhixin.cn.com.happyfarm_user.PersonalMessageActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.ShoppingCartActivity;
import zhixin.cn.com.happyfarm_user.View.ScrollTextView;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AdSeat;
import zhixin.cn.com.happyfarm_user.model.AllLand;
import zhixin.cn.com.happyfarm_user.model.Cooking;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.LandInfoCardItem;
import zhixin.cn.com.happyfarm_user.model.LatelyTrack;
import zhixin.cn.com.happyfarm_user.model.SearchLandByType;
import zhixin.cn.com.happyfarm_user.model.ShoppingCartNum;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.CustomToast;
import zhixin.cn.com.happyfarm_user.utils.GlideImageLoader;
import zhixin.cn.com.happyfarm_user.utils.RoundImageView;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

import static android.content.Context.MODE_PRIVATE;

public class NewFarmPage extends Fragment  {

    private String TAG = "NewFarmPage";
    private View myView;
    private RelativeLayout titleBarLayout;
    private ViewPager myViewPage;
    private LandInfoAdapter myLandInfoAdapter;
    private VegetablesCardAdapter vegetablesCardAdapter;
    private ShadowTransformer mCardShadowTransformer;
    private ShadowTransformer mVegetablesCardShadowTransformer;

    private GridView gridView;
    private ImageView massageButton;
    private ImageView shoppingCart;
    private ImageView toolbar;
    private RoundImageView vegetablesInfoIcon;
    private ImageView videoPlayIcon;
    private RoundImageView vegetablesInfoIcontwo;
    private Badge messageBadge;
    private Badge messageBadgeShoppingCart;
    private PopupMenuView mPopupMenuView;
    private Activity activity;
    private Dialog mDialog;
    private Dialog mDialog1;
    private String LandNo;
    private String LandId;
    private String isLandlord = "";
    private int superUser;
    private String UserNickName;
    private String UserImg;
    private String UserTel;
    private String UserCertificatesNo;
    private String UserRealname;
    private int UserPlotId;
    private String UserPlotName;
    private int UserGender;
    private long UserBirthday;
    private String UserEmail;
    private int landType;
    private ImageView landListPage;
    private ImageView myLandPage;
    private ImageView myBreedingTutorialPage;
    private ImageView daiding;
    private ViewPager vegetablesRecyclerView;
    private ScrollTextView scrolleView;
    private List<LandInfoCardItem.landInfoCardBean> landInfoList;
    private ScrollView main_scroll_view;
    //圆点
    private LinearLayout ll;
    private int llNumber;
    private List<ImageView> mDotImages;

    private HorizontalScrollView cookingLayout;
    private TextView cookingNoLayout;
    private String tokenStr;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_farm_page_layout,null,false);
        myView = view;
        initView(view);
        searchLatelyTrack(view);
        searchAdSeat();
        getUserInfo();
        return view;
    }
    private void initView(View view) {

        vegetablesInfoIcon = view.findViewById(R.id.vegetables_info_icon);
        videoPlayIcon = view.findViewById(R.id.video_play_icon);
        vegetablesInfoIcontwo = view.findViewById(R.id.vegetables_info_icon_two);
        //图片像素大小不同设置的圆角也不同
        vegetablesInfoIcontwo.setImageURL("http://img.trustwusee.com/%E6%88%90%E9%95%BF%E7%9B%B8%E5%86%8C.png");
//        vegetablesInfoIcontwo.setImageURL("http://img.trustwusee.com/capture_img_%10%0F_2019-07-03_10");
        vegetablesInfoIcontwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), HistoryPageActivity.class);
                startActivity(intent);
            }
        });

        titleBarLayout = view.findViewById(R.id.new_farm_title_layout);
//        textView1 = myView.findViewById(R.id.text1);
//        textView1.setText("第一块地里的作物");
        myViewPage = view.findViewById(R.id.land_info_card_view_pager);
        vegetablesRecyclerView = view.findViewById(R.id.vegetables_recycler_View);
        gridView = view.findViewById(R.id.cooking_list_GridView);
        setStatusBarHeight();//设置导航栏距离顶部的距离

        massageButton = view.findViewById(R.id.message_icon);
        massageButton.setOnClickListener(new View.OnClickListener() { //跳转消息页面按钮
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    Intent intent = new Intent(getActivity(), PersonalMessageActivity.class);
                    startActivity(intent);
                }
            }
        });
        shoppingCart = view.findViewById(R.id.shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = activity.getSharedPreferences("User_data",MODE_PRIVATE);
                tokenStr = pref.getString("token","");
                if ("".equals(tokenStr)) {
                    noLoginDialog();
                } else {
                    startActivity(new Intent(getContext(), ShoppingCartActivity.class));
                }

            }
        });
        // 根据menu资源文件创建
        //mPopupMenuView = new PopupMenuView(activity R.menu.menu, new MenuBuilder(activity));
        mPopupMenuView = new PopupMenuView(activity, R.layout.layout_custom_menu);
        mPopupMenuView.inflate(R.menu.menu, new MenuBuilder(activity));
        //显示样式（纵向）
        mPopupMenuView.setOrientation(LinearLayout.VERTICAL);
        //显示样式（横向）mPopupMenuView.setOrientation(LinearLayout.HORIZONTAL);
        toolbar = view.findViewById(R.id.home_toolbar);
        //菜单按钮
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopBottom();
            }
        });

        //去逛逛菜地
        landListPage = view.findViewById(R.id.land_list_page);
        landListPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    searchLandByType(1);
                }
            }
        });
        //去看看鸡舍
        myLandPage = view.findViewById(R.id.my_land_page);
        myLandPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    searchLandByType(2);
                }
            }
        });

        //去往种养教程
        myBreedingTutorialPage = view.findViewById(R.id.my_breeding_tutorial_page);
        myBreedingTutorialPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    Intent intent = new Intent(getContext(), BreedingTutorialPageActivity.class);
                    startActivity(intent);
                }

            }
        });

        //去往租地列表
        daiding = view.findViewById(R.id.daiding);
        daiding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    Intent intent = new Intent(getContext(), LandListActivity.class);
                    startActivity(intent);
                }
//                Intent intent = new Intent(getContext(), AddMakeCookingActivity.class);
//                startActivity(intent);
            }
        });

        LinearLayoutManager m=new LinearLayoutManager(getContext());
        m.setOrientation(LinearLayoutManager.HORIZONTAL);

        //TODO 判断当前是否有账号登录
        if (!EMClient.getInstance().isLoggedInBefore() || "isOneLogin".equals(Config.isOneLogin)){
            Config.isOneLogin = "noOneLogin";
            System.out.println("首页页面登录环信");
            //登录环信
            loginEM();
        }
        messageBadge = new QBadgeView(getContext())
                .bindTarget(myView.findViewById(R.id.message_icon_view_total))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
        setMsgNum(EMClient.getInstance().chatManager().getUnreadMessageCount());
        messageBadgeShoppingCart = new QBadgeView(getContext())
                .bindTarget(myView.findViewById(R.id.shopping_message_view_total))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
        main_scroll_view = view.findViewById(R.id.main_scroll_view);
        main_scroll_view.fullScroll(ScrollView.FOCUS_UP);

        //圆点集合
        mDotImages = new ArrayList<>();
        ll = view.findViewById(R.id.LL);

        cookingLayout = view.findViewById(R.id.cooking_layout);
        cookingNoLayout = view.findViewById(R.id.cooking_no_layout);
    }

//    /**设置数据*/
//    private void setData() {
//        cookingList = new ArrayList<Cooking>();
//        for (int i = 0;i <= (int) (Math.random() * 5 + 1);i++) {
//            Cooking item = new Cooking();
//            item = new Cooking();
//            item.setCookingName("青椒土豆片");
//            cookingList.add(item);
//        }
//    }

    /**
     * 查询近期公告
     */
    private void searchLatelyTrack(View myView) {
        HttpHelper.initHttpHelper().searchLatelyTrack().enqueue(new Callback<LatelyTrack>() {
            @Override
            public void onResponse(Call<LatelyTrack> call, Response<LatelyTrack> response) {
                try {
                    if ("success".equals(response.body().getFlag())) {
                        List<String> demographicsList = new ArrayList<>();
                        for (LatelyTrack.ResultBean bean : response.body().getResult()) {
                            demographicsList.add(Utils.getDateToStringMMDDHHmm(bean.getStartTime())+ " " + bean.getContent() );
                        }
                        if (0 == demographicsList.size()) {
                            demographicsList.add("暂无近期动态公告！");
                        }

                        scrolleView = myView.findViewById(R.id.scroll_view);
                        scrolleView.setList(demographicsList);
                        scrolleView.startScroll();
                        scrolleView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
    //                            Toast.makeText(getContext(),"点击了滚动通知栏",Toast.LENGTH_SHORT).show();
                                if (Utils.isFastClick()) {
                                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Intent intent3 = new Intent(activity, MyDynamicActivity.class);
                                        intent3.putExtra("UserImg", UserImg);
                                        intent3.putExtra("UserNickName", UserNickName);
                                        LoadingDialog.closeDialog(mDialog);
                                        startActivity(intent3);
                                    }
                                }
                            }
                        });
                    }
                } catch (Exception e) {

                }

            }
            @Override
            public void onFailure(Call<LatelyTrack> call, Throwable t) {

            }
        });
    }

    /**设置GirdView参数，绑定数据*/
    private void setGridView( List<Cooking.ResultBean.CookingList> cookingList) {
        int size = cookingList.size();
        int length = 112;
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;
        int gridviewWidth = (int) (size * (length + 4) * density);
        int itemWidth = (int) (length * density);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                gridviewWidth, LinearLayout.LayoutParams.MATCH_PARENT);
        gridView.setLayoutParams(params); // 设置GirdView布局参数,横向布局的关键
        gridView.setColumnWidth(itemWidth); // 设置列表项宽
        CookingListAdapter adapter = new CookingListAdapter(getContext(),cookingList);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getContext(), "菜品id" + cookingList.get(i).getId(), Toast.LENGTH_SHORT).show();
                if (Utils.isFastClick()) {
                    Intent intent = new Intent(getContext(), CookingRecommendedActivity.class);
                    intent.putExtra("cookingId",cookingList.get(i).getId());
                    startActivity(intent);
                }
            }
        });
    }

    /**
     *   查询收藏租地信息
     */
    private void getLandInfo(int landId){
        HttpHelper.initHttpHelper().searchUsersLandLeaseHappening().enqueue(new Callback<LandInfoCardItem>() {
            @Override
            public void onResponse(Call<LandInfoCardItem> call, Response<LandInfoCardItem> response) {
                try {
//                    Log.i(TAG, "onResponse: " + JSON.toJSONString(response.body()));
                    if ("success".equals(response.body().getFlag())){
                        myLandInfoAdapter = new LandInfoAdapter();
                        myLandInfoAdapter.addCardItem(response.body().getResult(),landId);
                        landInfoList = response.body().getResult();
                        mCardShadowTransformer = new ShadowTransformer(myViewPage, myLandInfoAdapter);
                        myViewPage.setAdapter(myLandInfoAdapter);
                        myViewPage.setPageTransformer(false, mCardShadowTransformer);
                        myViewPage.setOffscreenPageLimit(3);
                        mCardShadowTransformer.enableScaling(true);
                        if (null != response.body().getResult().get(0).getPhotoUrl()) {
//                            Bitmap bitmap = ImageUtils.createWaterMaskCenter(ImageUtils.returnBitMap(response.body().getResult().get(0).getAlbumFirstPicture()), BitmapFactory.decodeResource(getResources(),R.mipmap.open_play));
                            vegetablesInfoIcon.setImageURL(response.body().getResult().get(0).getAlbumFirstPicture());
                            videoPlayIcon.setVisibility(View.VISIBLE);
//                            vegetablesInfoIcon.setImageBitmap(ImageUtils.returnBitMap(response.body().getResult().get(0).getAlbumFirstPicture()));
                        } else {
                            vegetablesInfoIcon.setImageURL("http://img.trustwusee.com/capture_img_%10%0F_2019-07-03_10");
                            videoPlayIcon.setVisibility(View.GONE);
                        }
                        vegetablesInfoIcon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (Utils.isFastClick()) {
                                    if (null == response.body().getResult().get(0).getPhotoUrl()) {
                                        Toast.makeText(getContext(),"没有视频播放地址",Toast.LENGTH_SHORT).show();
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setDataAndType(Uri.parse(response.body().getResult().get(0).getPhotoUrl()), "video/mp4");
                                        startActivity(intent);
                                    }
                                }
                            }
                        });

                        llNumber = response.body().getResult().get(0).getCrop().size();
                        getCrop(response.body().getResult().get(0).getCrop(),llNumber);

                        myViewPage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int i, float v, int i1) {

                            }

                            @Override
                            public void onPageSelected(int i) {
                                llNumber = response.body().getResult().get(i).getCrop().size();
                                getCrop(response.body().getResult().get(i).getCrop(),llNumber);
                                if (null != response.body().getResult().get(i).getPhotoUrl()){
                                    vegetablesInfoIcon.setImageURL(response.body().getResult().get(i).getAlbumFirstPicture());
                                    videoPlayIcon.setVisibility(View.VISIBLE);
                                } else {
                                    vegetablesInfoIcon.setImageURL("http://img.trustwusee.com/capture_img_%10%0F_2019-07-03_10");
                                    videoPlayIcon.setVisibility(View.GONE);
                                }
                                vegetablesInfoIcon.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (Utils.isFastClick()) {
                                            if (null == response.body().getResult().get(i).getPhotoUrl()) {
                                                Toast.makeText(getContext(),"没有视频播放地址",Toast.LENGTH_SHORT).show();
                                            } else {
                                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                                intent.setDataAndType(Uri.parse(response.body().getResult().get(i).getPhotoUrl()), "video/mp4");
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                });
                            }
                            @Override
                            public void onPageScrollStateChanged(int i) {

                            }
                        });
                        myViewPage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        });
                    }
                } catch (Exception e) {
                    Toast.makeText(getContext(),"信息查询失败！",Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "onResponse: ",e );
                }

            }

            @Override
            public void onFailure(Call<LandInfoCardItem> call, Throwable t) {

            }
        });
    }

    private void getCrop(List<LandInfoCardItem.landInfoCardBean.Crop> crop,int count){
        if (0 != crop.size() ){
            drowDot(llNumber);
            vegetablesCardAdapter = new VegetablesCardAdapter();
            vegetablesCardAdapter.addCardItem(crop);
            mVegetablesCardShadowTransformer = new ShadowTransformer(vegetablesRecyclerView,vegetablesCardAdapter);
            vegetablesRecyclerView.setAdapter(vegetablesCardAdapter);
            vegetablesRecyclerView.setPageTransformer(false,mVegetablesCardShadowTransformer);
            vegetablesRecyclerView.setOffscreenPageLimit(3);
            mVegetablesCardShadowTransformer.enableScaling(true);
            searchCookingList(crop.get(0).getId());
            vegetablesRecyclerView.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
//                    setData();
//                    setGridView();
                    searchCookingList(crop.get(i).getId());
                    for (int a = 0; a < count; a++) {
                        if(a == i){
                            mDotImages.get(a).setImageResource(R.drawable.dot_selected);
                        }else{
                            mDotImages.get(a).setImageResource(R.drawable.dot_normal);
                        }
                    }
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });

        }
    }

    /**
     * 绘制圆点
     */
    private void drowDot(int count) {
        Log.i(TAG, "drowDot: " + count);
        if (!mDotImages.isEmpty()) {
            mDotImages.clear();
            ll.removeAllViews();
        }
        for (int i = 0; i < count ; i++) {
            ImageView iv=new ImageView(getContext());
            //设置圆点的尺寸
            LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(20,20);
            params.leftMargin=5;
            params.rightMargin=5;
            iv.setLayoutParams(params);

            mDotImages.add(iv);

            if(i==0){
                iv.setImageResource(R.drawable.dot_selected);
            }else{
                iv.setImageResource(R.drawable.dot_normal);
            }
            ll.addView(iv);
        }


    }
    /**
     * 获取广告栏图片
     */
    private void searchAdSeat() {
        HttpHelper.initHttpHelper().searchAdSeat(3).enqueue(new Callback<AdSeat>() {
            @Override
            public void onResponse(Call<AdSeat> call, Response<AdSeat> response) {
                try {
                    if ("success".equals(response.body().getFlag())) {
                        List images = new ArrayList();
                        List<AdSeat.ResultBean.ListBean> obj = response.body().getResult().getList();
//                    images.add("http://image14.m1905.cn/uploadfile/2018/0907/thumb_1_1380_460_20180907013518839623.jpg");
                        for (int i = 0;i < obj.size();i++) {
                            images.add(obj.get(i).getSeatImg());
                        }
                        Banner banner = (Banner) getActivity().findViewById(R.id.ad_seat);
                        //设置图片加载器
                        banner.setImageLoader(new GlideImageLoader());
                        //设置图片集合
                        banner.setImages(images);
                        //scrollview 数据加载完成自动滑动到底部到问题
                        banner.setFocusable(true);
                        banner.setFocusableInTouchMode(true);
                        banner.requestFocus();
                        //banner设置方法全部调用完毕时最后调用
                        banner.start();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "菜场banner没来的急加载 ", e);
                }
            }

            @Override
            public void onFailure(Call<AdSeat> call, Throwable t) {

            }
        });
    }

    /**
     * 查询菜谱列表
     * @param id
     */
    private void searchCookingList(int id){
        HttpHelper.initHttpHelper().searchCookingMethods(id).enqueue(new Callback<Cooking>() {
            @Override
            public void onResponse(Call<Cooking> call, Response<Cooking> response) {
//                System.out.println("searchCookingList:"+JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    if (response.body().getResult().getList().size() > 0) {
                        cookingLayout.setVisibility(View.VISIBLE);
                        cookingNoLayout.setVisibility(View.GONE);
                        setGridView(response.body().getResult().getList());
                    } else {
                        cookingLayout.setVisibility(View.GONE);
                        cookingNoLayout.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Cooking> call, Throwable t) {

            }
        });
    }

    /**
     * 获取用户信息
     */
    public void getUserInfo() {
        //获取用户信息是否为地主
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
//                Log.e("state", JSON.toJSONString(response.body().getResult()));
                if (("success").equals(response.body().getFlag())) {
                    GetSelfInfo.ResultBean resultBean = response.body().getResult();
                    UserNickName = resultBean.getNickname();
                    isLandlord = String.valueOf(resultBean.getIsLandlord());
                    superUser = resultBean.getSuperUser();
                    UserImg = resultBean.getHeadImg();
                    UserTel = resultBean.getTel();
                    UserCertificatesNo = resultBean.getCertificatesNo();
                    UserRealname = resultBean.getRealname();
                    UserPlotId = resultBean.getPlotId();
                    UserPlotName = resultBean.getPlotName();
                    UserGender = resultBean.getGender();
                    UserBirthday = resultBean.getBirthday();
                    UserEmail = resultBean.getEmail();
                    LandId = String.valueOf(resultBean.getLandId());
                    LandNo = String.valueOf(resultBean.getLandNo());
                    getLandInfo(Integer.valueOf(LandId));
                } else {
                    superUser = 0;
                    isLandlord = "0";
//                    Toast.makeText(activity,"网络错误",Toast.LENGTH_SHORT).show();
                    getLandInfo(0);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                superUser = 0;
                isLandlord = "0";
                getLandInfo(0);
//                Toast.makeText(activity, "网络连接失败,请检查一下网络哦~", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //点击菜单图片显示
    private void showPopBottom() {
//        popupMenu1();
        popupMenu2();
    }

    private void popupMenu2() {
        // 设置点击监听事件
        mPopupMenuView.setOnMenuClickListener(new OptionMenuView.OnOptionMenuClickListener() {
            @Override
            public boolean onOptionMenuClick(int position, OptionMenu menu) {
                String token = activity.getSharedPreferences("User_data", MODE_PRIVATE).getString("token", "");
                if ("".equals(token)) {
                    noLoginDialog();
                    return true;
                }
                if (!NetWorkUtils.isNetworkAvalible(activity)) {
                    CustomToast.showToast(activity, "请检查网络，稍后再试");
                    return true;
                }
                mDialog = LoadingDialog.createLoadingDialog(activity, "");
                Call<GetSelfInfo> getSelfInfoCall = HttpHelper.initHttpHelper().getSelfInfo();
                getSelfInfoCall.enqueue(new Callback<GetSelfInfo>() {
                    @Override
                    public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                        if ("success".equals(response.body().getFlag())) {
                            LandId = String.valueOf(response.body().getResult().getLandId());
                            LandNo = String.valueOf(response.body().getResult().getLandNo());
                            Log.e("LandId", LandId);
                            Log.e("LandNo", LandNo);
                            switch (position) {
                                case 0:
                                    if ("".equals(LandId) || "".equals(LandNo) || "0".equals(LandId) || "0".equals(LandNo)) {
                                        CustomToast.showToast(activity, "您尚未购买租地");
                                        //Toast.makeText(activity,"您尚未购买租地",Toast.LENGTH_SHORT).show();
                                    } else {
                                        for (LandInfoCardItem.landInfoCardBean landInfoCardBean : landInfoList) {
                                            if (landInfoCardBean.getId() == response.body().getResult().getLandId()) {
                                                landType = landInfoCardBean.getLandType();
                                            }
                                        }
                                        Intent intent1 = new Intent(activity, LandVideoActivity.class);
                                        intent1.putExtra("state", "0");
                                        intent1.putExtra("landNo", LandNo);
                                        intent1.putExtra("landId", LandId);
                                        intent1.putExtra("UserId", "0");
                                        intent1.putExtra("isLandlord", isLandlord);
                                        intent1.putExtra("superUser", superUser + "");
                                        intent1.putExtra("landType", landType + "");
                                        LoadingDialog.closeDialog(mDialog);
                                        startActivity(intent1);
                                    }
                                    LoadingDialog.closeDialog(mDialog);
                                    break;
                                case 1:
                                    Intent intent3 = new Intent(activity, MyDynamicActivity.class);
                                    intent3.putExtra("UserImg", UserImg);
                                    intent3.putExtra("UserNickName", UserNickName);
                                    intent3.putExtra("UserNickName", UserNickName);
                                    LoadingDialog.closeDialog(mDialog);
                                    startActivity(intent3);
                                    break;
                                case 2:
                                    Intent intent = new Intent(activity, MyCollectionActivity.class);
                                    LoadingDialog.closeDialog(mDialog);
                                    startActivity(intent);
                                    break;
                                default:
                                    LoadingDialog.closeDialog(mDialog);
                                    break;
                            }
                        } else {
                            LoadingDialog.closeDialog(mDialog);
                            noLoginDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetSelfInfo> call, Throwable t) {
                        LoadingDialog.closeDialog(mDialog);
                        try {
                            if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                                noLoginDialog();
                            } else {
                                CustomToast.showToast(activity, TextString.NetworkRequestFailed);
                            }
                        } catch (Exception e) {
                            Log.e("FarmPage->", "onFailure: " + e);
                        }
                    }
                });
                return true;
            }
        });
        // 显示在mButtom控件的周围
        mPopupMenuView.show(toolbar);
    }


    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(activity);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    /**
     * 获取购物车数量
     */
    public void searchShoppingCartNum() {
        HttpHelper.initHttpHelper().searchShoppingCartNum().enqueue(new Callback<ShoppingCartNum>() {
            @Override
            public void onResponse(Call<ShoppingCartNum> call, Response<ShoppingCartNum> response) {
                if ("success".equals(response.body().getFlag())) {
                    Config.shoppingCartNum = response.body().getResult();
                    messageBadgeShoppingCart.setBadgeNumber(Config.shoppingCartNum);
                }
            }
            @Override
            public void onFailure(Call<ShoppingCartNum> call, Throwable t) {

            }
        });
    }

    /**
     * 设置消息角标
     */
    public void getEMNumber(int number){
        setMsgNum(number);
    }

    /**
     * 设置消息显示数量
     * @param num 消息数量
     * position 设置显示tablayout的item 暂时未时候，考虑后期可能所有的item都需要
     */
    private void setMsgNum( int num){
        if (messageBadge != null){
//            Log.i(TAG, "消息设置成功setMsgNum: "+num);
            messageBadge.setBadgeNumber(num);
        } else {
//            Log.i(TAG, "消息设置失败setMsgNum: "+num);
        }
    }

    /**
     * 直接登录环信
     */
    private void loginEM(){
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (response.isSuccessful() && response.body().getFlag().equals("success")) {
                    GetSelfInfo.ResultBean resultBean = response.body().getResult();
                    loginEM(String.valueOf(resultBean.getId()),resultBean.getPassword());
                }
            }
            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }
    //login环信
    private void loginEM(String user, String pwd){
        EMClient.getInstance().login(user,pwd,new EMCallBack() {//回调
            @Override
            public void onSuccess() {
//                Log.d("EMLogmain","登录环信成功");
                //TODO 以下两个方法是为了保证进入主页面后本地会话和群组都 load 完毕。
                EMClient.getInstance().chatManager().loadAllConversations();
                EMClient.getInstance().groupManager().loadAllGroups();
                // 获取华为 HMS 推送 token
//                HMSPushHelper.getInstance().connectHMS(getActivity());
//                HMSPushHelper.getInstance().getHMSPushToken();
                setMsgNum(EMClient.getInstance().chatManager().getUnreadMessageCount());
//                Log.i("打印消息数量1", "数量" + EMClient.getInstance().chatManager().getUnreadMessageCount());
            }

            @Override
            public void onProgress(int progress, String status) {}

            @Override
            public void onError(int code, String message) {
//                Log.d("EMLogmain", "登录聊天服务器失败！"+code+"信息："+message);
                try {
                    Toast.makeText(getContext(),"登录聊天服务器失败！",Toast.LENGTH_SHORT).show();
                }catch (Exception e) {
                }
            }
        });
    }

    /**
     * 验证token是否有效
     */
    public void userInfoTokenTest() {
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
//                Log.i(TAG, "onResponse: " +JSON.toJSONString(response.body().getFlag()));
                if ("failed".equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(getContext(), "User_data", strArr);
//                    allLand();
                    massageButton.setVisibility(View.GONE);
                } else {
                    massageButton.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                massageButton.setVisibility(View.GONE);
            }
        });
    }
    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(getContext());
        //相对布局使用方法
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        titleBarLayout.setLayoutParams(params);
    }

    /**
     * 根据type 查看随机菜地和鸡窝
     * @param type
     */
    private void searchLandByType(int type) {
        mDialog1 = LoadingDialog.createLoadingDialog(activity, "正在跳转");
        HttpHelper.initHttpHelper().searchLandByType(type).enqueue(new Callback<SearchLandByType>() {
            @Override
            public void onResponse(Call<SearchLandByType> call, Response<SearchLandByType> response) {
//                Log.i(TAG, "onResponse: " + JSON.toJSONString(response.body()));
               if ("success".equals(response.body().getFlag())) {
                   SearchLandByType.ResultBean obj = response.body().getResult().get(0);
                    Intent intent = new Intent(activity, LandVideoActivity.class);
                    intent.putExtra("state", "1");
                    intent.putExtra("landNo", obj.getLandNo()+"");
                    intent.putExtra("landId", obj.getId()+"");
                    intent.putExtra("alias",obj.getAlias()+"");
//                   Log.i(TAG, "租地新编号: " + obj.getAlias());
                    intent.putExtra("LeaseTime", obj.getLeaseTime()+"");
                    intent.putExtra("ExpirationTime", obj.getExpirationTime()+"");
                    intent.putExtra("UserId", "0");
                    intent.putExtra("isLandlord", isLandlord);
                    intent.putExtra("superUser", superUser + "");
                    intent.putExtra("landType", obj.getLandType() + "");
                    startActivity(intent);
               } else {
                   Toast.makeText(getContext(), "跳转失败", Toast.LENGTH_SHORT).show();
               }
                LoadingDialog.closeDialog(mDialog1);
            }

            @Override
            public void onFailure(Call<SearchLandByType> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog1);
                Toast.makeText(getContext(), "跳转失败，网络失败", Toast.LENGTH_SHORT).show();
            }
        });
    }
    /**
     * 获取状态栏高度
     *
     * @param context 上下文
     * @return int 状态栏高度值
     */
    protected static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        //主要用于刚进来获取用户信息，好做传值
//        getUserInfo();
//    }

    @Override
    public void onResume() {
        super.onResume();
        userInfoTokenTest();
        searchShoppingCartNum();
    }
}
