package zhixin.cn.com.happyfarm_user.base;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

import com.alibaba.android.arouter.launcher.ARouter;
import com.facebook.stetho.Stetho;
import com.hyphenate.chat.EMOptions;
import com.hyphenate.easeui.EaseUI;
import com.vondear.rxtool.RxTool;
import com.xiaomi.mipush.sdk.MiPushClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.bingoogolapple.swipebacklayout.BGASwipeBackHelper;
//import zhixin.cn.com.happyfarm_user.HMSPushHelper;
import zhixin.cn.com.happyfarm_user.BuildConfig;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.other.OSUtils;
import zhixin.cn.com.happyfarm_user.utils.CrashHandler;

public class App extends Application {
    private final String TAG = "App->";
    public static Context CONTEXT;
    public static GetSelfInfo.ResultBean USER;
    private static Map<String,Activity> destoryMap = new HashMap<>();
    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        //初始化异常捕获类
        CrashHandler.getInstance().init(this);
        Stetho.initializeWithDefaults(this);
        RxTool.init(this);
        CONTEXT = this.getApplicationContext();
        instance = this;
        // 置入一个不设防的VmPolicy（不设置的话 7.0以上一调用拍照功能就崩溃了）
        // 还有一种方式：manifest中加入provider然后修改intent代码
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }
        /**
         * 必须在 Application 的 onCreate 方法中执行 BGASwipeBackHelper.init 来初始化滑动返回
         * 第一个参数：应用程序上下文
         * 第二个参数：如果发现滑动返回后立即触摸界面时应用崩溃，请把该界面里比较特殊的 View 的 class 添加到该集合中，目前在库中已经添加了 WebView 和 SurfaceView
         */
        //BGASwipeBackHelper.init(this, null);

        initPush();
        initEaseUI();

        initRouter(App.this);
    }

    public static void initRouter(Application application) {
        if (BuildConfig.DEBUG) {
            ARouter.openLog();     // 打印日志
            ARouter.openDebug();   // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
        }
        ARouter.init(application);
    }

    public static App getInstance() {
        return instance;
    }

    /**
     * 初始化推送服务
     */
    private void initPush() {
        if(shouldInit()) {
            String osName = OSUtils.getDeviceBrand();
            switch (OSUtils.getDeviceBrand()) {
                case "Xiaomi":
                    //初始化miPush推送服务
                    MiPushClient.registerPush(this, "2882303761517821004", "5641782143004");
                    break;
                case "":
                    //初始化huaWeiPush推送服务
//                    HMSAgent.init(this);
                    break;
                default:
                    Log.d(TAG, "onCreate: " + osName);
            }
        }
    }

    /**
     * 初始化环信服务
     */
    private void initEaseUI() {
        EMOptions options = new EMOptions();
        // 收到好友申请是否自动同意，如果是自动同意就不会收到好友请求的回调，因为sdk会自动处理，默认为true
        options.setAcceptInvitationAlways(false);
        // set if you need read ack
        options.setRequireAck(true);
        // set if you need delivery ack
        options.setRequireDeliveryAck(false);
        // 设置小米推送 appID 和 appKey
        options.setMipushConfig("2882303761517821004", "5641782143004");
        options.setAutoTransferMessageAttachments(true);
        options.setAutoDownloadThumbnail(true);
        //设置自动登录
        options.setAutoLogin(false);
        EaseUI.getInstance().init(CONTEXT,options);//在做打包混淆时，要关闭debug模式，避免消耗不必要的资源
        // 开启 debug 模式
        //EMClient.getInstance().setDebugMode(false);
        // 初始化华为 HMS 推送服务
//        HMSPushHelper.getInstance().initHMSAgent(instance);
    }

    private boolean shouldInit() {
        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
        List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainProcessName = getPackageName();
        int myPid = android.os.Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }
    /**
     * 添加到销毁队列
     * 在A创建的时候，调用 add方法把当前的A添加进去。
     * @param activity 要销毁的activity
     */
    public static void addDestoryActivity(Activity activity, String activityName) {
        destoryMap.put(activityName, activity);
    }

    /**
     * 销毁指定Activity
     * 当需要结束的时候，在B中调用 destoryActivity方法，指定添加A时的Key值来finish 掉A
     */
    public static void destoryActivity(String activityName) {
        Set<String> keySet = destoryMap.keySet();
        for (String key : keySet){
            destoryMap.get(key).finish();
        }
    }


//设置字体为默认大小，不随系统字体大小改而改变
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1)//非默认值
            getResources();
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        if (res.getConfiguration().fontScale != 1) {//非默认值
            Configuration newConfig = new Configuration();
            newConfig.setToDefaults();//设置默认
            res.updateConfiguration(newConfig, res.getDisplayMetrics());
        }
        return res;
    }

}
