package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class SignRecord {
    private int day;
    private int type;

    public  SignRecord(int day,int type) {
        this.day = day;
        this.type = type;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    //    private ResultBean result;
//    private String flag;
//
//    public ResultBean getResult() {
//        return result;
//    }
//
//    public void setResult(ResultBean result) {
//        this.result = result;
//    }
//
//    public String getFlag() {
//        return flag;
//    }
//
//    public void setFlag(String flag) {
//        this.flag = flag;
//    }
//
//    public static class ResultBean {
//        private int today;
//        private int isSign;
//        private List<ResultBeanList> attendanceRecord;
//
//        public int getToday() {
//            return today;
//        }
//
//        public void setToday(int today) {
//            this.today = today;
//        }
//
//        public int getIsSign() {
//            return isSign;
//        }
//
//        public void setIsSign(int isSign) {
//            this.isSign = isSign;
//        }
//
//        public List<ResultBeanList> getAttendanceRecord() {
//            return attendanceRecord;
//        }
//
//        public void setAttendanceRecord(List<ResultBeanList> attendanceRecord) {
//            this.attendanceRecord = attendanceRecord;
//        }
//
//        public static class ResultBeanList {
//            private int id;
//            private int userId;
//            private long startTime;
//            private String username;
//            private int fewDay;
//            private int state;
//            private String week;
//            private int type;
//
//            public int getId() {
//                return id;
//            }
//
//            public void setId(int id) {
//                this.id = id;
//            }
//
//            public int getUserId() {
//                return userId;
//            }
//
//            public void setUserId(int userId) {
//                this.userId = userId;
//            }
//
//            public long getStartTime() {
//                return startTime;
//            }
//
//            public void setStartTime(long startTime) {
//                this.startTime = startTime;
//            }
//
//            public String getUsername() {
//                return username;
//            }
//
//            public void setUsername(String username) {
//                this.username = username;
//            }
//
//            public int getFewDay() {
//                return fewDay;
//            }
//
//            public void setFewDay(int fewDay) {
//                this.fewDay = fewDay;
//            }
//
//            public int getState() {
//                return state;
//            }
//
//            public void setState(int state) {
//                this.state = state;
//            }
//
//            public String getWeek() {
//                return week;
//            }
//
//            public void setWeek(String week) {
//                this.week = week;
//            }
//
//            public int getType() {
//                return type;
//            }
//
//            public void setType(int type) {
//                this.type = type;
//            }
//        }
//    }

}
