package zhixin.cn.com.happyfarm_user.model;

import com.google.gson.Gson;

public class SearchLandByNo {

    /**
     * result : {"id":324,"landNo":309,"userId":21,"details":"","x":0,"y":-1,"rent":0.01,"leaseTime":1534837295000,"leaseTerm":3,"isHosted":0,"loginHeader":null,"startTime":1532939284000,"updateTime":1532939284000,"managerId":1,"state":1,"expirationTime":1542786095000,"isShed":0,"isUsed":null,"userName":"雷诺","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head211531126200","tel":null,"isCollections":null,"isFriend":null}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public static SearchLandByNo objectFromData(String str) {

        return new Gson().fromJson(str, SearchLandByNo.class);
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * id : 324
         * landNo : 309
         * userId : 21
         * details :
         * x : 0
         * y : -1
         * rent : 0.01
         * leaseTime : 1534837295000
         * leaseTerm : 3
         * isHosted : 0
         * loginHeader : null
         * startTime : 1532939284000
         * updateTime : 1532939284000
         * managerId : 1
         * state : 1
         * expirationTime : 1542786095000
         * isShed : 0
         * isUsed : null
         * userName : 雷诺
         * headImg : http://7xtaye.com2.z0.glb.clouddn.com/head211531126200
         * tel : null
         * isCollections : null
         * isFriend : null
         */

        private int id;
        private int landNo;
        private int userId;
        private String details;
        private int x;
        private int y;
        private double rent;
        private long leaseTime;
        private int leaseTerm;
        private int isHosted;
        private Object loginHeader;
        private long startTime;
        private long updateTime;
        private int managerId;
        private int state;
        private long expirationTime;
        private int isShed;
        private Object isUsed;
        private String userName;
        private String headImg;
        private Object tel;
        private Object isCollections;
        private Object isFriend;
        //开水编号
        private int waterNumber;
        //开灯编号
        private int lampNumber;
        //继电器ip
        private String relayIp;



        public static ResultBean objectFromData(String str) {

            return new Gson().fromJson(str, ResultBean.class);
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLandNo() {
            return landNo;
        }

        public void setLandNo(int landNo) {
            this.landNo = landNo;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public double getRent() {
            return rent;
        }

        public void setRent(double rent) {
            this.rent = rent;
        }

        public long getLeaseTime() {
            return leaseTime;
        }

        public void setLeaseTime(long leaseTime) {
            this.leaseTime = leaseTime;
        }

        public int getLeaseTerm() {
            return leaseTerm;
        }

        public void setLeaseTerm(int leaseTerm) {
            this.leaseTerm = leaseTerm;
        }

        public int getIsHosted() {
            return isHosted;
        }

        public void setIsHosted(int isHosted) {
            this.isHosted = isHosted;
        }

        public Object getLoginHeader() {
            return loginHeader;
        }

        public void setLoginHeader(Object loginHeader) {
            this.loginHeader = loginHeader;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getManagerId() {
            return managerId;
        }

        public void setManagerId(int managerId) {
            this.managerId = managerId;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public long getExpirationTime() {
            return expirationTime;
        }

        public void setExpirationTime(long expirationTime) {
            this.expirationTime = expirationTime;
        }

        public int getIsShed() {
            return isShed;
        }

        public void setIsShed(int isShed) {
            this.isShed = isShed;
        }

        public Object getIsUsed() {
            return isUsed;
        }

        public void setIsUsed(Object isUsed) {
            this.isUsed = isUsed;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getHeadImg() {
            return headImg;
        }

        public void setHeadImg(String headImg) {
            this.headImg = headImg;
        }

        public Object getTel() {
            return tel;
        }

        public void setTel(Object tel) {
            this.tel = tel;
        }

        public Object getIsCollections() {
            return isCollections;
        }

        public void setIsCollections(Object isCollections) {
            this.isCollections = isCollections;
        }

        public Object getIsFriend() {
            return isFriend;
        }

        public void setIsFriend(Object isFriend) {
            this.isFriend = isFriend;
        }

        public int getWaterNumber() {
            return waterNumber;
        }

        public void setWaterNumber(int waterNumber) {
            this.waterNumber = waterNumber;
        }

        public int getLampNumber() {
            return lampNumber;
        }

        public void setLampNumber(int lampNumber) {
            this.lampNumber = lampNumber;
        }

        public String getRelayIp() {
            return relayIp;
        }

        public void setRelayIp(String relayIp) {
            this.relayIp = relayIp;
        }
    }
}
