package zhixin.cn.com.happyfarm_user.utils;

import android.os.Environment;
import android.util.Log;
import android.widget.Button;

import com.alibaba.fastjson.JSON;
import com.hyphenate.easeui.domain.EaseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.LabelBean;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.LuckDraw;
import zhixin.cn.com.happyfarm_user.model.PushToken;

public class Config {
    public static final String SDCARD_DIR = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String DEFAULT_CACHE_DIR = SDCARD_DIR + "/PLDroidPlayer";

    public static final String MOB_SDK_S = "25515d53c905f";
    public static final String MOB_SDK_S1 = "808c4873e09c22e755625130756b5feb";

    public static Map<String,EaseUser> AllEMFriendInfo = new HashMap<>();

    public static int myLandWatch; //TODO 我的租地观看时间
    public static int otherLandWatch; //TODO 他人租地观看时间

    public static int rtspStartTime;//TODO 推流开始时间
    public static int rtspEndTime;//TODO 推流结束时间

    public static String eChatId;

    public static String isOneLogin = "isOneLogin";//TODO 判断环信是否为第一次登录

    public static boolean isLogin = false;//TODO 判断是否还是登录状态，用在设置页面判断是否需要显示退出登录 false为未登录 true为已登录

    public static boolean isManageAddressRefresh = false;//TODO 管理收货地址页面  false为不需要 true为需要

    public static boolean personalCenterDialogShow = false;

    //七牛
    //七牛上传图片使用的token
    public static String qiuNiuToken = "";
    //"domain":"happyfarm"
    public static String qiuNiuDomain = "";
    //"imgUrl":"http://p7blgq6id.bkt.clouddn.com/"
    public static String qiuNiuImgUrl = "";

    //记录当前选中的item 1,2,3,4,5 分别对应农场，菜场，消息，社区，我的
    public static int bottomBarItem = 1;

    //记录菜场页面是否需要每次来到页面都要刷新
    public static boolean allGoodPageRefresh;

    //记录成长相册是否需要每次来到页面都要刷新
    public static boolean myAlbumRefresh;

    //previousCommand 记录上一次摄像头操作
    public static int previousCommand = -1;
    //设备号
    public static String devNo = "";
    public static String ALI_EMAIL_ADDRESS = "service@trustwusee.com";

    //个人中心
    //直播音频状态
    public static boolean AUDIO_STATE;
    //LandVideoActivity
    //视频页
    public static boolean audioIsPlay;//是否播放音频声音


    //小米或者華爲推送token
    private static String PUSHTOKEN = "";
    public static String getPUSHTOKEN() {
        return PUSHTOKEN;
    }

    //正在查看的租地编号
    public static Integer landNumber;

    //去往奖品说明页面弹窗
    public static Boolean isNoGoLuckDrawExplain = true;

    //分享截图用户名
    public static String nickname;

    //是否需要更新弹窗
    public static Boolean isAbout = false;

    //邀请码
    public static String invitationCode = "";

    //购物车数量
    public static Integer shoppingCartNum = 0;

    //第一次生成的视频链接

    public static String photoAlubmURL ="";


    public static void setPUSHTOKEN(String PUSHTOKEN) {
        Log.d("Config->", "setPUSHTOKEN: " + PUSHTOKEN);
        Config.PUSHTOKEN = PUSHTOKEN;
        settingPushToken();
    }

    /**
     * 设置推送token
     */
    public static void settingPushToken() {
        Call<PushToken> pushToken = HttpHelper.initHttpHelper().setUserPushToken(Config.getPUSHTOKEN());
        pushToken.enqueue(new Callback<PushToken>() {
            @Override
            public void onResponse(Call<PushToken> call, Response<PushToken> response) {
//                Log.i("Config->", JSON.toJSONString(response.body()));
                if (null != response.body()) {
                    if ("success".equals(response.body().getFlag())) {
                        Log.i("Config->", "onResponse: "+"token设置成功");
                    } else {
                        Log.i("Config->", "onResponse: "+"token设置失败");
                    }
                }
            }

            @Override
            public void onFailure(Call<PushToken> call, Throwable t) {
                Log.i("LoginActivity->", "onResponse: "+"token设置异常");
            }
        });
    }

}
