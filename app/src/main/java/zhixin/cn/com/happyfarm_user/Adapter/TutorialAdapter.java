package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.util.List;
import java.util.logging.Logger;

import zhixin.cn.com.happyfarm_user.MyImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Tutorial;
import zhixin.cn.com.happyfarm_user.utils.GlideRoundTransform;


/**
 * Created by Administrator on 2018/3/21.
 */

public class TutorialAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Tutorial> mDatas;

    public TutorialAdapter(Context context, List<Tutorial> datas){
        mInflater = LayoutInflater.from(context);
        mDatas = datas;
    }
    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ViewHolder holder = null;
            if (convertView == null){
                convertView = mInflater.inflate(R.layout.tutorial_item,parent,false);
                holder = new ViewHolder();
                holder.tutorialName = convertView.findViewById(R.id.tutorial_name);
                holder.tutorialImage = convertView.findViewById(R.id.tutorial_image);
                convertView.setTag(holder);
            }else {
                holder = (ViewHolder) convertView.getTag();
            }
            Tutorial tutorial = mDatas.get(position);
            holder.tutorialName.setText(tutorial.getName());
            Glide.with(convertView.getContext())
                    .load(mDatas.get(position).getImage())
                    .transform(new CenterCrop(convertView.getContext()),new GlideRoundTransform(convertView.getContext(),5))
                    .into(holder.tutorialImage);

        } catch (Exception e) {

        }
        return convertView;
    }
    public class ViewHolder{
        TextView tutorialName;
        MyImageView tutorialImage;
    }
}
