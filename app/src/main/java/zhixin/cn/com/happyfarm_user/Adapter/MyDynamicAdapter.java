package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.MyDynamic;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.utils.Utils;


/**
 * Created by DELL on 2018/3/9.
 */

//RecycleView的适配器，要注意指定的泛型，一般我们就是类名的ViewHolder继承ViewHolder（内部已经实现了复用优化机制）
public class MyDynamicAdapter extends RecyclerView.Adapter<MyDynamicAdapter.StaggerViewHolder> {

    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<MyDynamic> mList;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public MyDynamicAdapter(Context context, List<MyDynamic> list) {
        mContext = context;
        mList = list;
    }

    @Override
    //创建staggerViewHolder，并把ViewHolder返回出去
    public MyDynamicAdapter.StaggerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.my_dynamic_item, null);
        //创建一个staggerViewHolder对象
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(MyDynamicAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        MyDynamic dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        setData(holder,dataBean);
    }

    public void setData(MyDynamicAdapter.StaggerViewHolder holder,MyDynamic data) {
        if (data.textTitle.equals("农民伯伯")){
            holder.circleImageView.setImageResource(R.mipmap.workers_leave_message);
            holder.cardView.setCardBackgroundColor(Color.parseColor("#e7eded"));
            if (("不合理").equals(data.isIrrational)){
                holder.reasonable.setVisibility(View.GONE);
                holder.unreasonable.setVisibility(View.VISIBLE);

            }else if (("合理").equals(data.isIrrational)){
                holder.reasonable.setVisibility(View.VISIBLE);
                holder.unreasonable.setVisibility(View.GONE);
            }else {
                holder.reasonable.setVisibility(View.GONE);
                holder.unreasonable.setVisibility(View.GONE);
            }
        }else if (data.textTitle.equals("系统消息")){
            holder.circleImageView.setImageResource(R.mipmap.system_message);
            holder.reasonable.setVisibility(View.GONE);
            holder.unreasonable.setVisibility(View.GONE);
            holder.cardView.setCardBackgroundColor(Color.parseColor("#e7eded"));
        }else {
            if (NumUtil.checkNull(data.userImg)){
                holder.circleImageView.setImageResource(R.drawable.icon_user_img);
            }else {
                //Picasso使用了流式接口的调用方式
                //Picasso类是核心实现类。
                //实现图片加载功能至少需要三个参数：
                Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                        .load(data.userImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                        .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                        .into(holder.circleImageView);//into(ImageView targetImageView)：图片最终要展示的地方。
            }
            holder.reasonable.setVisibility(View.GONE);
            holder.unreasonable.setVisibility(View.GONE);
            holder.cardView.setCardBackgroundColor(Color.parseColor("#92dbca"));
        }
        //名字
        holder.mTextTitle.setText(data.textTitle);
        //内容
        holder.mTextContent.setText(data.textContent);
        //时间
        holder.dynamicTime.setText( Utils.getDateToString(data.dynamicTime));
    }


    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final TextView mTextTitle;
        private final TextView mTextContent;
        private final TextView reasonable;
        private final TextView unreasonable;
        private final CardView cardView;
        private final ImageView circleImageView;
        private final TextView dynamicTime;

        public StaggerViewHolder(View itemView) {
            super(itemView);
            mTextTitle = itemView.findViewById(R.id.text_title);
            mTextContent = itemView.findViewById(R.id.text_content);
            reasonable = itemView.findViewById(R.id.reasonable_text);
            unreasonable = itemView.findViewById(R.id.unreasonable_text);
            cardView = itemView.findViewById(R.id.card_view);
            circleImageView = itemView.findViewById(R.id.img_msg_icon);
            dynamicTime = itemView.findViewById(R.id.dynamic_time);

        }


    }
}
