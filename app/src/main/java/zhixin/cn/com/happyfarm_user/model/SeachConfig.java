package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by Administrator on 2018/7/18.
 */

public class SeachConfig {

    /**
     * result : {"id":1,"adSeatCapped":5,"offsaleTime":10,"exchange":10,"diamond":10,"watchTime":5,"myWatchTime":10,"state":1}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * id : 1
         * adSeatCapped : 5
         * offsaleTime : 10
         * exchange : 10
         * diamond : 10
         * watchTime : 5
         * myWatchTime : 10
         *  "rtspStartTime": 7,
         *   "rtspEndTime": 20,
         * state : 1
         */

        private int id;
        private int adSeatCapped;
        private int offsaleTime;
        private int exchange;
        private int diamond;
        private int watchTime;
        private int myWatchTime;
        private int state;

        private int rtspStartTime;
        private int rtspEndTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAdSeatCapped() {
            return adSeatCapped;
        }

        public void setAdSeatCapped(int adSeatCapped) {
            this.adSeatCapped = adSeatCapped;
        }

        public int getOffsaleTime() {
            return offsaleTime;
        }

        public void setOffsaleTime(int offsaleTime) {
            this.offsaleTime = offsaleTime;
        }

        public int getExchange() {
            return exchange;
        }

        public void setExchange(int exchange) {
            this.exchange = exchange;
        }

        public int getDiamond() {
            return diamond;
        }

        public void setDiamond(int diamond) {
            this.diamond = diamond;
        }

        public int getWatchTime() {
            return watchTime;
        }

        public void setWatchTime(int watchTime) {
            this.watchTime = watchTime;
        }

        public int getMyWatchTime() {
            return myWatchTime;
        }

        public void setMyWatchTime(int myWatchTime) {
            this.myWatchTime = myWatchTime;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public int getRtspStartTime() {
            return rtspStartTime;
        }

        public void setRtspStartTime(int rtspStartTime) {
            this.rtspStartTime = rtspStartTime;
        }

        public int getRtspEndTime() {
            return rtspEndTime;
        }

        public void setRtspEndTime(int rtspEndTime) {
            this.rtspEndTime = rtspEndTime;
        }
    }
}
