package zhixin.cn.com.happyfarm_user;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.TutorialAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.SearchTutorial;
import zhixin.cn.com.happyfarm_user.model.Tutorial;

import static com.mob.tools.utils.DeviceHelper.getApplication;

public class BreedingTutorialPageActivity extends AppCompatActivity {

    private ListView listView;
    private RefreshLayout refreshLayout;
    private ArrayList<Tutorial> tutorials = new ArrayList<>();
    private TutorialAdapter adapter;
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private ArrayList<String> tutorialName = new ArrayList<>();
    private ArrayList<Integer> tutorialId = new ArrayList<>();
    private Tutorial tutorial;
    private RelativeLayout tutorialListPrompt;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.breeding_tutorial_page_activity);
        pageNum = 1;
        pageSize = 15;
        refreshLayout = findViewById(R.id.refreshLayout);
        tutorialListPrompt = findViewById(R.id.tutorial_list_prompt);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(BreedingTutorialPageActivity.this).setShowBezierWave(false));
        // 设置 Footer 为 球脉冲
        refreshLayout.setRefreshFooter(new ClassicsFooter(BreedingTutorialPageActivity.this).setSpinnerStyle(SpinnerStyle.Scale));
        if (!tutorials.isEmpty()){
            tutorials.clear();
        }
        StaggerLoadData(false, 1);
        initRecyclerView();
        topRefreshLayout();
        bottomRefreshLayout();
    }



    private void initRecyclerView() {
        listView = findViewById(R.id.tutorial_list);
        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new TutorialAdapter(BreedingTutorialPageActivity.this, tutorials);
        //设置适配器
        listView.setAdapter(adapter);
        //ListView item的点击事件
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(BreedingTutorialPageActivity.this, TutorialDetailsActivity.class);
//                        intent.putExtra("tutorialId",response.body().getResult().getList().get(i).getId());
                intent.putExtra("tutorialName", tutorialName.get(i) + "");
                intent.putExtra("tutorialId", tutorialId.get(i) + "");
                Log.i("传过去得id：", "onItemClick: " + tutorialId.get(i));
                startActivity(intent);
            }
        });

    }

    private void StaggerLoadData(Boolean inversion, int pageNum) {
        dialog = LoadingDialog.createLoadingDialog(BreedingTutorialPageActivity.this, "正在加载数据");
        Call<SearchTutorial> searchTutorial = HttpHelper.initHttpHelper().searchTutorial();
        searchTutorial.enqueue(new Callback<SearchTutorial>() {
            @Override
            public void onResponse(Call<SearchTutorial> call, Response<SearchTutorial> response) {
                if (("success").equals(response.body().getFlag())) {
                    if (response.body().getResult().getList().isEmpty()){
                        tutorialListPrompt.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        isLastPage = true;
                    }else {
                        tutorialListPrompt.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        isLastPage = response.body().getResult().isIsLastPage();
                        Log.e("data", JSON.toJSONString(response.body()));
                        for (int i = 0; i <= response.body().getResult().getList().size() - 1; i++) {
                            tutorial = new Tutorial(response.body().getResult().getList().get(i).getTutorialName(),response.body().getResult().getList().get(i).getCropImg());
                            tutorialId.add(response.body().getResult().getList().get(i).getId());
                            tutorialName.add(response.body().getResult().getList().get(i).getTutorialName());
                            tutorials.add(tutorial);
                        }
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    tutorialListPrompt.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    Toast.makeText(BreedingTutorialPageActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(Call<SearchTutorial> call, Throwable t) {
                tutorialListPrompt.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                LoadingDialog.closeDialog(dialog);
            }
        });


//        //集合对象
//        ArrayList<Tutorial> tutorials = new ArrayList<>();
//        //给DataBean类放数据，最后把装好数据的DataBean类放到集合里
//        for(int i=0;i<10;i++){
//
//            Tutorial tutorial = new Tutorial("列表："+i);
//            tutorials.add(tutorial);
//
//        }


    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!tutorials.isEmpty()){
                    tutorials.clear();
                }
                isLastPage = false;
                refreshlayout.setNoMoreData(false);//TODO 恢复没有更多数据的原始状态
                pageNum = 1;
                StaggerLoadData(false, 1);
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }

    //上拉加载
    public void bottomRefreshLayout() {
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (isLastPage) {
                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
                        } else {
//                            Toast.makeText(getApplication(), "数据加载完毕", Toast.LENGTH_SHORT).show();
                            pageNum++;
                            StaggerLoadData(false, pageNum);
//                            adapter.loadMore(initData());
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }
    public void back(View view) {
        finish();
    }
}
