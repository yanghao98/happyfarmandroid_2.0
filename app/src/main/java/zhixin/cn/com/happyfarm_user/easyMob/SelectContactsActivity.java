package zhixin.cn.com.happyfarm_user.easyMob;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.githang.statusbar.StatusBarCompat;
import com.google.gson.Gson;
import cc.solart.wave.WaveSideBarView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.EventBus.RefreshGroup;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.InvitationFriend;
import zhixin.cn.com.happyfarm_user.model.SeachFriend;
import zhixin.cn.com.happyfarm_user.other.PinyinComparator;
import zhixin.cn.com.happyfarm_user.other.PinyinUtils;

/**
 * Created by Administrator on 2018/6/11.
 */

public class SelectContactsActivity extends ABaseActivity {
    private InputMethodManager inputMethodManager;
    private SelectContactsAdapter adapter;
    private RecyclerView recyclerView;
    private ArrayList<SelectContact> list = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;
    private RelativeLayout selectContactsPrompt;
    protected ImageButton clearSearch;
    protected EditText query;
    private WaveSideBarView sideBar;
    private TextView dialog;
    private Button completeBtn;
    private String groupName;
    private Dialog mDialog;
    private String activityName;
    private ArrayList<String> friendList = new ArrayList<>();
    private String groupId;
    /**
     * 根据拼音来排列RecyclerView里面的数据类
     */
    private PinyinComparator pinyinComparator;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //启动activity时不自动弹出软键盘 //TODO 选择联系人页面软键盘
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_contacts_layout);
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        selectContactsPrompt = findViewById(R.id.select_contacts_prompt);
        completeBtn = findViewById(R.id.select_save_btn);
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        Intent intent = getIntent();
        groupName = intent.getStringExtra("groupName");
        activityName = intent.getStringExtra("activity");
        friendList = intent.getStringArrayListExtra("friendList");
        groupId = intent.getStringExtra("groupId");
        //search
        query = findViewById(com.hyphenate.easeui.R.id.query);
        clearSearch = findViewById(com.hyphenate.easeui.R.id.search_clear);
        initRecyclerView();
        if ("GroupInfo".equals(activityName))
            selectFriendJoin();
        else
            AllFriend();
        CompleteBtn();
    }

    //TODO 查询所有好友
    public void AllFriend(){
        mDialog = LoadingDialog.createLoadingDialog(SelectContactsActivity.this,null);
        HttpHelper.initHttpHelper().seachFriend(0).enqueue(new Callback<SeachFriend>() {
            @Override
            public void onResponse(Call<SeachFriend> call, Response<SeachFriend> response) {
                Log.e("好友：", JSON.toJSONString(response.body()));
                if (response.body().getFlag().equals("success")) {
                    LoadingDialog.closeDialog(mDialog);
                    if (String.valueOf(response.body().getResult().getList()).equals("[]")) {
                        selectContactsPrompt.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        selectContactsPrompt.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        String sortString = null;
                        for (int i = 0; i <= response.body().getResult().getList().size() - 1; i++) {
                            SelectContact dataBean = new SelectContact();
                            dataBean.contactImg = String.valueOf(response.body().getResult().getList().get(i).getFriendHeadImg());
                            dataBean.contactName = response.body().getResult().getList().get(i).getFriendNickname();
                            dataBean.contactId = String.valueOf(response.body().getResult().getList().get(i).getToId());
                            //汉字转换成拼音
                            String pinyin = PinyinUtils.getPingYin(response.body().getResult().getList().get(i).getFriendNickname());
                            //TODO 判断是否为空，否则无法截取拼音
                            if ("".equals(pinyin) || pinyin == null){
                                sortString = "#";
                            }else {
                                sortString = pinyin.substring(0, 1).toUpperCase();
                            }

                            // 正则表达式，判断首字母是否是英文字母
                            if (sortString.matches("[A-Z]")) {
                                dataBean.setLetters(sortString.toUpperCase());
                            } else {
                                dataBean.setLetters("#");
                            }
                            list.add(dataBean);
                            // 根据a-z进行排序源数据
                            Collections.sort(list, pinyinComparator);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(SelectContactsActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                    selectContactsPrompt.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<SeachFriend> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                selectContactsPrompt.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        });
    }

    //TODO　选择好友加入群聊
    public void selectFriendJoin(){
        JSON g = new JSONObject();
        String jsonString = g.toJSONString(friendList);
        mDialog = LoadingDialog.createLoadingDialog(SelectContactsActivity.this,null);
        HttpHelper.initHttpHelper().searchInvitationFriend(jsonString).enqueue(new Callback<InvitationFriend>() {
            @Override
            public void onResponse(Call<InvitationFriend> call, Response<InvitationFriend> response) {
                Log.e("好友：", JSON.toJSONString(response.body()));
                if (response.body().getFlag().equals("success")) {
                    LoadingDialog.closeDialog(mDialog);
                    if (("[]").equals(String.valueOf(response.body().getResult()))) {
                        selectContactsPrompt.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        selectContactsPrompt.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        String sortString = null;
                        for (int i = 0; i <= response.body().getResult().size() - 1; i++) {
                            SelectContact dataBean = new SelectContact();
                            dataBean.contactImg = String.valueOf(response.body().getResult().get(i).getFriendHeadImg());
                            dataBean.contactName = response.body().getResult().get(i).getFriendNickname();
                            dataBean.contactId = String.valueOf(response.body().getResult().get(i).getToId());
                            //汉字转换成拼音
                            String pinyin = PinyinUtils.getPingYin(response.body().getResult().get(i).getFriendNickname());
                            //TODO 判断是否为空，否则无法截取拼音
                            if ("".equals(pinyin) || pinyin == null){
                                sortString = "#";
                            }else {
                                sortString = pinyin.substring(0, 1).toUpperCase();
                            }

                            // 正则表达式，判断首字母是否是英文字母
                            if (sortString.matches("[A-Z]")) {
                                dataBean.setLetters(sortString.toUpperCase());
                            } else {
                                dataBean.setLetters("#");
                            }
                            list.add(dataBean);
                            // 根据a-z进行排序源数据
                            Collections.sort(list, pinyinComparator);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(SelectContactsActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                    selectContactsPrompt.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<InvitationFriend> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                selectContactsPrompt.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        });
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
//        recyclerView.setBackground(R.layout);
        pinyinComparator = new PinyinComparator();
        sideBar = findViewById(R.id.select_contacts_sidebar);
        sideBar.setLetters(Arrays.asList(getResources().getStringArray(R.array.waveSideBarLetter)));
        dialog = findViewById(R.id.floating_header);
//        sideBar.setTextView(dialog);

        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new SelectContactsAdapter(SelectContactsActivity.this, list);
        //设置右侧SideBar触摸监听
        sideBar.setOnTouchLetterChangeListener(new WaveSideBarView.OnTouchLetterChangeListener() {
            @Override
            public void onLetterChange(String letter) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(letter.charAt(0));
                if (position != -1) {
                    gridLayoutManager.scrollToPositionWithOffset(position, 0);
                }
            }
        });
        query.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
                filterData(s.toString());
                if (s.length() > 0) {
                    clearSearch.setVisibility(View.VISIBLE);
                } else {
                    clearSearch.setVisibility(View.INVISIBLE);

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        clearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query.getText().clear();
                hideSoftKeyboard();
            }
        });
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard();
                return false;
            }
        });
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(SelectContactsActivity.this, 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
//        ((EaseSidebar) findViewById(R.id.select_contacts_sidebar)).setListView(recyclerView);
    }
    //TODO 完成点击事件
    public void CompleteBtn(){
        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Set<String> id = adapter.contactIdList();
                JSON g = new JSONObject();
                String jsonString = g.toJSONString(id);
                if (id.size() == 0){
                    finish();
                    //Toast.makeText(SelectContactsActivity.this,"您还未选择好友",Toast.LENGTH_SHORT).show();
                } else {
                    if ("GroupInfo".equals(activityName))
                        joinGroups(jsonString);
                    else
                        CreateGroup(groupName,jsonString);
                }
            }
        });
    }
    //TODO　创建群组接口
    public void CreateGroup(String groupName,String userId){
        mDialog = LoadingDialog.createLoadingDialog(SelectContactsActivity.this,"创建中...");
        HttpHelper.initHttpHelper().createGroup(groupName,groupName,userId).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                Log.e("好友：", JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())) {
                    new Handler().postDelayed(new Runnable(){
                        public void run() {
                            //execute the task
                            LoadingDialog.closeDialog(mDialog);
                            finish();
                            Toast.makeText(SelectContactsActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                        }
                    }, 1500);
                }else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(SelectContactsActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
            }
        });
    }

    //TODO　批量加人接口
    public void joinGroups(String userList){
        mDialog = LoadingDialog.createLoadingDialog(SelectContactsActivity.this,"拉取中...");
        HttpHelper.initHttpHelper().joinGroups(groupId,userList).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                Log.i("joinGroups", "onResponse: "+userList);
                Log.e("joinGroups好友：", JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())) {
                    new Handler().postDelayed(new Runnable(){
                        public void run() {
                            //execute the task
                            LoadingDialog.closeDialog(mDialog);
                            EventBus.getDefault().post(new RefreshGroup("isRefresh"));
                            finish();
                            Toast.makeText(SelectContactsActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                        }
                    }, 1000);
                }else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(SelectContactsActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
            }
        });
    }
    /**
     * 根据输入框中的值来过滤数据并更新RecyclerView
     *
     * @param filterStr
     */
    private void filterData(String filterStr) {
        List<SelectContact> filterDateList = new ArrayList<>();

        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = list;
        } else {
            filterDateList.clear();
            for (SelectContact selectContact : list) {
                String name = selectContact.getContactName();
                if (name.indexOf(filterStr.toString()) != -1 ||
                        PinyinUtils.getFirstSpell(name).startsWith(filterStr.toString())
                        //不区分大小写
                        || PinyinUtils.getFirstSpell(name).toLowerCase().startsWith(filterStr.toString())
                        || PinyinUtils.getFirstSpell(name).toUpperCase().startsWith(filterStr.toString())
                        ) {
                    filterDateList.add(selectContact);
                }
            }
        }
        // 根据a-z进行排序
        Collections.sort(filterDateList, pinyinComparator);
        adapter.updateList(filterDateList);
    }
    protected void hideSoftKeyboard() {
        if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
            if (getCurrentFocus() != null)
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
    public void back(View v) {
        finish();
    }
}
