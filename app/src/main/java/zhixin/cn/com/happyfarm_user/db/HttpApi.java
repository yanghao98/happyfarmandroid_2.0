package zhixin.cn.com.happyfarm_user.db;


import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;
import zhixin.cn.com.happyfarm_user.easyMob.SearchMyApply;
import zhixin.cn.com.happyfarm_user.easyMob.searchGroup;
import zhixin.cn.com.happyfarm_user.model.AdSeat;
import zhixin.cn.com.happyfarm_user.model.AliPay;
import zhixin.cn.com.happyfarm_user.model.AllLand;
import zhixin.cn.com.happyfarm_user.model.AllUserInfo;
import zhixin.cn.com.happyfarm_user.model.AmountConfiguration;
import zhixin.cn.com.happyfarm_user.model.AreaAddress;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.CheckSMSByResultIsList;
import zhixin.cn.com.happyfarm_user.model.ChickenCoopController;
import zhixin.cn.com.happyfarm_user.model.Cooking;
import zhixin.cn.com.happyfarm_user.model.CookingInfo;
import zhixin.cn.com.happyfarm_user.model.CreateActivity;
import zhixin.cn.com.happyfarm_user.model.CropGoodsDetails;
import zhixin.cn.com.happyfarm_user.model.CusChat;
import zhixin.cn.com.happyfarm_user.model.Default;
import zhixin.cn.com.happyfarm_user.model.DishCategory;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.GetVideoSize;
import zhixin.cn.com.happyfarm_user.model.HelpList;
import zhixin.cn.com.happyfarm_user.model.ISRegister;
import zhixin.cn.com.happyfarm_user.model.InvitationFriend;
import zhixin.cn.com.happyfarm_user.model.LandHistoryPic;
import zhixin.cn.com.happyfarm_user.model.LandInfoCardItem;
import zhixin.cn.com.happyfarm_user.model.LatelyTrack;
import zhixin.cn.com.happyfarm_user.model.LoginList;
import zhixin.cn.com.happyfarm_user.model.LuckDraw;
import zhixin.cn.com.happyfarm_user.model.LuckDrawAddress;
import zhixin.cn.com.happyfarm_user.model.LuckDrawDeductionAmount;
import zhixin.cn.com.happyfarm_user.model.LuckDrawModel;
import zhixin.cn.com.happyfarm_user.model.LuckDrawRecord;
import zhixin.cn.com.happyfarm_user.model.PlotPlantingStandard;
import zhixin.cn.com.happyfarm_user.model.PublicKeyList;
import zhixin.cn.com.happyfarm_user.model.PushToken;
import zhixin.cn.com.happyfarm_user.model.QiNiu;
import zhixin.cn.com.happyfarm_user.model.RegisterList;
import zhixin.cn.com.happyfarm_user.model.SeachConfig;
import zhixin.cn.com.happyfarm_user.model.SeachFriend;
import zhixin.cn.com.happyfarm_user.model.SeachTrack;
import zhixin.cn.com.happyfarm_user.model.SearchAllGoods;
import zhixin.cn.com.happyfarm_user.model.SearchCropList;
import zhixin.cn.com.happyfarm_user.model.SearchDiamondBag;
import zhixin.cn.com.happyfarm_user.model.SearchIrrationInfo;
import zhixin.cn.com.happyfarm_user.model.SearchLandByNo;
import zhixin.cn.com.happyfarm_user.model.SearchLandByType;
import zhixin.cn.com.happyfarm_user.model.SearchMaintainList;
import zhixin.cn.com.happyfarm_user.model.SearchPhotoAlbum;
import zhixin.cn.com.happyfarm_user.model.SearchPlotManagements;
import zhixin.cn.com.happyfarm_user.model.SearchTutorial;
import zhixin.cn.com.happyfarm_user.model.SearchTutorialContent;
import zhixin.cn.com.happyfarm_user.model.SearchUserLandcrop;
import zhixin.cn.com.happyfarm_user.model.SelectLandCollection;
import zhixin.cn.com.happyfarm_user.model.ShoppingCart;
import zhixin.cn.com.happyfarm_user.model.ShoppingCartNum;
import zhixin.cn.com.happyfarm_user.model.SignIn;
import zhixin.cn.com.happyfarm_user.model.SignMonthRecord;
import zhixin.cn.com.happyfarm_user.model.SignRecord;
import zhixin.cn.com.happyfarm_user.model.TermsModel;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.model.TransactionDetails;
import zhixin.cn.com.happyfarm_user.model.WeChat;
import zhixin.cn.com.happyfarm_user.model.friendRecommendation;
import zhixin.cn.com.happyfarm_user.model.searchDetail;
import zhixin.cn.com.happyfarm_user.model.searchFriendDetail;
import zhixin.cn.com.happyfarm_user.model.searchMyGood;
import zhixin.cn.com.happyfarm_user.model.searchOrderlogList;
import zhixin.cn.com.happyfarm_user.model.searchReceivingAddress;

/**
 * Created by Administrator on 2017/8/22.
 */

public interface HttpApi {

//    //findHotelById
//    @GET("land/searchLandList") //获取酒店详情
//    Call<LandList> searchLandList(@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);

    @POST("user/register") //用户注册
    Call<RegisterList> register(@Query("nickname") String nickname, @Query("password") String password, @Query("tel") String tel, @Query("registerIp") String registerIp);

    @POST("rsa/publicKey") //获取公钥
    Call<PublicKeyList> publicKey(@Query("userInfo") String userInfo);

    @POST("user/login") //用户登录
    Call<LoginList> login(@Query("tel")String tel, @Query("pwd")String pwd, @Query("loginIp")String loginIp);

    @GET("DiamondBagController/searchDiamondBag") //获取钻石列表
    Call<SearchDiamondBag> searchDiamondBag(@Header("x-auth-token") String token);

    @GET("maintain/searchMaintainList") //查询养护操作
    Call<SearchMaintainList> searchMaintainList(@Query("maintainType")Integer maintainType);

    @POST("user/getSelfInfo") //获取用户信息
    Call<GetSelfInfo> getSelfInfo(@Header("x-auth-token") String token);

    @POST("captcha/getSMS") //发送手机验证码
    Call<CheckSMS> getSMS(@Query("tel")String tel,@Query("info") String info);

    @POST("user/isRegister")  //判断用户是否已注册
    Call<ISRegister> isRegister(@Query("tel")String tel);

    @POST("captcha/checkSMS") //校验短信验证码
    Call<CheckSMS> checkSMS(@Query("tel")String tel, @Query("captcha")String captcha);

    /**
     * 重置密码  用户丢失密码后,需先验证手机号,后重置
     * @param tel 手机号
     * @param pwd 新密码
     * @return
     */
    @POST("user/resetPassword")
    Call<CheckSMS> resetPassword(@Query("tel")String tel,@Query("pwd")String pwd);

    @GET("crop/searchCropList")  //查询农作物   新增，添加作物名字、查询作物收成、或者、寄回价格
    Call<SearchCropList> searchCropList(@Query("name")String cropName,@Query("cropType")Integer cropType);

    @GET("crop/searchCropList")  //查询全部农作物
    Call<SearchCropList> searchAllCropList();

    @GET("task/searchUserLandcrop")  //收成列表
    Call<SearchUserLandcrop> searchUserLandcrop(@Query("landId")int landId,@Query("userId")int userId);

    @GET("tutorial/searchTutorial")  //查询全部养护教程名
    Call<SearchTutorial> searchTutorial();

    @GET("tutorial/searchTutorial")  //我的租地查询养护教程名
    Call<SearchTutorial> LandVideoSearchTutorial(@Header("x-auth-token") String token);

    @GET("tutorial/searchTutorialContent") //查询养护教程内容
    Call<SearchTutorialContent> searchTutorialContent(@Query("tutorialid")int tutorialid);

    @GET("land/searchLandList") //查询租地信息
    Call<AllLand> landList(@Header("x-auth-token") String token);

    @GET("land/searchLandList") //根据地块id查询租地信息
    Call<AllLand> landListByLandId(@Header("x-auth-token") String token,@Query("landId") int landId);

    @POST("task/addUserTask")  //地主发布种植
    Call<CheckSMS> addUserTask(@Header("x-auth-token")String token, @Query("payType")int payType, @Query("command")int command, @Query("sendBackNum")int sendBackNum, @Query("sellNum")String sellNum, @Query("landId")int landId, @Query("cropId")int cropId, @Query("cropNum")int cropNum);

    @GET("land/searchLandByNo")  //查看租地详情
    Call<SearchLandByNo> searchLandByNo(@Header("x-auth-token")String token, @Query("landId")int landId);

    @GET("land/hostingLand")  //托管接口
    Call<CheckSMS> hostingLand(@Header("x-auth-token")String token,@Query("default_harvest")String default_harvest,@Query("is_hosted")int is_hosted,@Query("landNo")int landNo);

    @GET("friend/addFriend")  //添加好友
    Call<CheckSMS> addFriend(@Header("x-auth-token")String token,@Query("friendId")int friendId);

    @GET("landCollection/addCollect") //租地收藏
    Call<CheckSMS> addCollect(@Header("x-auth-token")String token,@Query("userId")int userId,@Query("landId")int landId);

    @GET("landCollection/selectLandCollection")  //查询租地收藏
    Call<SelectLandCollection> selectLandCollection(@Header("x-auth-token")String token,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);

    @GET("landCollection/selectLandCollection")  //查询租地收藏
    Call<SelectLandCollection> selectLandCollectionVideo(@Header("x-auth-token")String token);

    @GET("landCollection/deleteCollect")  //取消租地收藏
    Call<CheckSMS> deleteCollect(@Header("x-auth-token")String token,@Query("userId")int userId,@Query("landId")int landId);

    @GET("picture/searchPicture") //查询土地历史图片
    Call<LandHistoryPic> landHistroy(@Header("x-auth-token") String token,
                                     @Query("landNo")String landNo,
                                     @Query("startTime") String startTime,
                                     @Query("endTime") String endTime,
                                     @Query("number") int num);

    @GET("PlotManagement/searchPlotManagement")  //获取收获地址
    Call<SearchPlotManagements> searchPlotManagements(@Header("x-auth-token")String token);

    @GET("OperationLog/searchIrration")
    Call<SearchIrrationInfo> searchIrrationInfo(@Header("x-auth-token")String token,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);

    @POST("user/updateUserInfo")//完善用户信息
    Call<CheckSMS> updateUserInfo(@Header("x-auth-token")String token,@Query("nickname")String nickname,@Query("gender") int gender,@Query("birthday")String birthday,@Query("realname")String realname,@Query("certificatesNo")String certificatesNo,
                                   @Query("email")String email);

    @POST("task/addUserTask")  //地主发布收获
    Call<CheckSMS> addUserTasks(@Header("x-auth-token")String token,@Query("payType")int payType,@Query("command")int command,@Query("sendBackNum")int sendBackNum,@Query("sellNum")String sellNum,@Query("storeSale") double storeSale,@Query("landId")int landId,@Query("cropId")int cropId,@Query("plotId")int plotId);

    @GET("amountConfiguration/searchAmountConfig")//查找租地套餐
    Call<AmountConfiguration> searchAmountConfig(@Header("x-auth-token")String token, @Query("lease")String lease,@Query("landType") int landType);//amountConfiguration

    @POST("task/addUserTask") //添加操作指令
    Call<CheckSMS> addOperation(@Header("x-auth-token")String token,@Query("payType")int payType,@Query("command")int command,@Query("sendBackNum")int sendBackNum,@Query("sellNum")String sellNum,@Query("landId")int landId,@Query("maintainId")int maintainId);

    @GET("track/seachTrack")//查找我的动态
    Call<SeachTrack> seachTrack (@Header("x-auth-token")String token,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);

    @GET("user/loginOut")  //退出登录
    Call<CheckSMS> loginOut(@Header("x-auth-token")String token);

    /**
     *
     * @param token 用户token
     * @param amount 交易金额
     * @param payType 支付方式1钻石2积分3现金
     * @param payState 交易类型1收款2支出
     * @param userId 用户id
     * @param scene 使用场景(1充值2操作3租地4购买商品)
     * @return
     */
    @GET("payment/addPayment")  //添加收支记录
    Call<CheckSMS> addPayment(@Header("x-auth-token")String token,@Query("amount")int amount,@Query("payType")String payType, @Query("payState")String payState,@Query("userId")int userId,@Query("scene")String scene);

    @GET("land/buyLand") //购买租地
    Call<CheckSMS> buyLand(@Header("x-auth-token")String token,@Query("rent")int rent,@Query("leaseTerm")int leaseTerm,@Query("landNo")int landNo);

    @POST("user/updateUserInfo")  //修改收获地址
    Call<CheckSMS> updateUserInfos(@Header("x-auth-token")String token,@Query("plotId")int plotId);

    @POST("user/test") //验证token是否有效
    Call<Test> test(@Header("x-auth-token")String token);

    @GET("land/searchLandByNo")  //查看租地详情
    Call<SearchLandByNo> searchLandByNos(@Header("x-auth-token")String token, @Query("landNo")int landNo);

//    @GET("alipay/generatePaymentOrder")//支付宝支付 租地
//    Call<AliPay> generatePaymentOrder(@Query("userId") int userId,
//                                        @Query("landId") int landId,
//                                        @Query("lease")int lease,
//                                        @Query("amount")String amount,
//                                        @Query("subject")String subject,
//                                        @Query("luckDrawRecordId") int luckDrawRecordId);
    @GET("alipay/generatePaymentOrder")//支付宝支付 租地
    Call<AliPay> generatePaymentOrderNoluckDrawRecordId(@Query("userId") int userId,
                                                        @Query("landId") int landId,
                                                        @Query("lease")int lease,
                                                        @Query("amount")String amount,
                                                        @Query("subject")String subject,
                                                        @Query("deductionAmount") int deductionAmount);

    @GET("alipay/generatePaymentOrder")//支付宝支付 余额
    Call<AliPay> generatePaymentOrderD(@Query("userId") int userId,
                                       @Query("getDiamond")int getDiamond,
                                       @Query("amount")String amount,
                                       @Query("subject")String subject);

    @GET("alipay/generatePaymentOrder")//支付宝支付 买菜
    Call<AliPay> generatePaymentOrderVegetables(@Query("userId") int userId,
                                                @Query("amount")String amount,
                                                @Query("subject")String subject,
                                                @Query("orderId") String orderId,
                                                @Query("plotId") int plotId,
                                                @Query("price") String price,
                                                @Query("sum") String sum,
                                                @Query("goodsId") String goodsId);
    @POST("alipay/setSign") //支付宝加签操作
    Call<String> setSign(@Body Map<String,String> params);

    @GET("user/addMoney") //鸡蛋充值
    Call<CheckSMS> addMoney(@Header("x-auth-token")String token,@Query("money")int money,@Query("diamond")int diamond);

    @POST("Qiniu/getQiniuToken")//获取七牛token
    Call<QiNiu> getQiniuToken(@Header("x-auth-token")String token);

    @GET("weChatPay/generatePaymentOrder")//微信支付 钻石
    Call<WeChat> weChatPayD(@Query("userId") int userId,
                            @Query("getDiamond")int getDiamond,
                            @Query("ip")String ip,
                            @Query("amount")Double amount,
                            @Query("subject")String subject);

    @GET("weChatPay/generatePaymentOrder")//微信支付 租地
    Call<WeChat> weChatPay(@Query("userId") int userId,
                           @Query("landId") int landId,
                           @Query("lease")int lease,
                           @Query("ip")String ip,
                           @Query("amount")Double amount,
                           @Query("subject")String subject,
                           @Query("deductionAmount") int deductionAmount);

    @GET("weChatPay/generatePaymentOrder")//微信支付 买菜
    Call<WeChat> weChatPayVegetables(@Query("ip")String ip,
                                     @Query("userId") int userId,
                                     @Query("amount")Double amount,
                                     @Query("subject")String subject,
                                     @Query("orderId") String orderId,
                                     @Query("plotId") int plotId,
                                     @Query("price") String price,
                                     @Query("sum") String sum,
                                     @Query("goodsId") String goodsId);


//    @GET("weChatPay/generatePaymentOrder")//微信支付 租地
//    Call<WeChat> weChatPayNoluckDrawRecordId(@Query("userId") int userId,
//                           @Query("landId") int landId,
//                           @Query("lease")int lease,
//                           @Query("ip")String ip,
//                           @Query("amount")Double amount,
//                           @Query("subject")String subject,
//                           @Query("luckDrawRecordId") int luckDrawRecordId);

    @GET("videoQueue/getVideoSize") //用户进入视频页
    Call<GetVideoSize> getVideoSize(@Header("x-auth-token")String token,@Query("landNo")int landNo);

    @GET("videoQueue/getVideoSize") //用户进入视频页
    Call<Object> getVideoSize2(@Header("x-auth-token")String token,@Query("landNo")int landNo);

    @GET("videoQueue/stopVideo") //用户停止视频
    Call<Object> stopVideo(@Header("x-auth-token")String token,@Query("landNo")int landNo);


    @POST("user/updatePassword")//用户修改密码
    Call<CheckSMS>updatePassword(@Header("x-auth-token")String token,@Query("pwd")String pwd,@Query("oldPwd")String oldPwd);

    @GET("friend/seachFriend") //查询好友
    Call<SeachFriend> seachFriend(@Header("x-auth-token")String token, @Query("friendId")int friendId);

    @POST("user/searchSign") //查询用户是否签到
    Call<CheckSMS> searchSign(@Header("x-auth-token")String token);

    @GET("user/updateIsSign") //用户签到
    Call<SignIn> updateIsSign(@Header("x-auth-token")String token);

    @POST("photoAlbum/addPhotoAlbum") //添加成长相册
    Call<CheckSMS> addPhotoAlbum(@Header("x-auth-token")String token,@Query("albumName") String albumName,@Query("photoAbstract") String photoAbstract,@Query("photoUrl") String photoUrl,@Query("isPublic")String isPublic);

    @GET("receivingAddress/addReceivingAddress")//添加收获地址
    Call<CheckSMS>addReceivingAddress(@Header("x-auth-token")String token,@Query("plotId")int plotId,@Query("isDefault")String isDefault);

    @GET("receivingAddress/searchReceivingAddress")//查询收获地址
    Call<searchReceivingAddress>searchReceivingAddress(@Header("x-auth-token")String token);

    @GET("receivingAddress/deleteReceivingAddress")//删除小区地址
    Call<CheckSMS>deleteReceivingAddress(@Header("x-auth-token")String token,@Query("id")int addressId);

    @GET("receivingAddress/updateReceivingAddress")//修改收获地址
    Call<CheckSMS>updateReceivingAddress(@Header("x-auth-token")String token,@Query("id")int addressId,@Query("plotId")int plotId,@Query("isDefault")String isDefault);

    @GET("receivingAddress/updateReceivingAddress")//修改收获地址
    Call<CheckSMS>updateReceivingAddres(@Header("x-auth-token")String token,@Query("plotId")int plotId,@Query("isDefault")String isDefault);

    @POST("user/updateUserInfo")//完善用户信息
    Call<CheckSMS> updateUserImg(@Header("x-auth-token")String token,@Query("headImg")String headImg);

    @POST("photoAlbum/searchPhotoAlbum")  //查询成长相册
    Call<SearchPhotoAlbum> searchPhotoAlbum(@Header("x-auth-token")String token,@Query("userId")int userId,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);

    @POST("photoAlbum/searchPhotoAlbum")  //查询成长相册
    Call<SearchPhotoAlbum> searchPhotoAlbumPublic(@Header("x-auth-token")String token,@Query("userId")int userId,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize, @Query("isPublic") String isPublic);

    @POST("user/updateUserInfo")//完善用户信息
    Call<CheckSMS> updateUserPhone(@Header("x-auth-token")String token,@Query("tel")String userPhone);

    @POST("photoAlbum/updatePhotoAlbum") //修改成长相册
    Call<Default> updatePhotoAlbum(@Header("x-auth-token")String token, @Query("id")int id, @Query("albumName")String albumName, @Query("photoAbstract")String photoAbstract, @Query("isPublic")String isPublic);

    @POST("photoAlbum/deletePhotoAlbum")//删除成长相册
    Call<CheckSMS> deletePhotoAlbum(@Header("x-auth-token")String token,@Query("id")int id);

    @GET("orderlog/searchOrderlogList")//查询订单(已买到）select=1,state = 4,已买到的。
    Call<searchOrderlogList> searchOrderlogListBuy(@Header("x-auth-token")String token,@Query("select")int select,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize, @Query("state") int state);

    @GET("orderlog/searchOrderlogList")//查询订单(已售出）select=2。已售出的
    Call<searchOrderlogList> searchOrderlogList(@Header("x-auth-token")String token,@Query("select")int select,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);

    @GET("orderlog/searchOrderlogList")//查询订单GET /api/goods/searchMyGoods
    Call<searchOrderlogList> searchOrderlogListByState (@Header("x-auth-token")String token,@Query("state")int state,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);

    @GET("goods/searchMyGoods")//我的蔬菜
    Call<searchMyGood> searchMyGoods(@Header("x-auth-token")String token,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);

    @GET("goods/searchAllGoods") //查询商品
    Call<SearchAllGoods> searchAllGoods(@Header("x-auth-token")String token,@Query("sortId")int sortId,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize,@Query("sortDay")int sortDay,@Query("sortNum")int sortNum,@Query("cropName")String cropName,@Query("dishCategoryId") int dishCategoryId);

    @GET("goods/searchAllGoods") //根据商品类别查询商品
    Call<SearchAllGoods> searchAllGoodsByPriceState(@Header("x-auth-token")String token,@Query("priceState")int priceState,@Query("pageNum") int pageNum, @Query("pageSize") int pageSize);


    @GET("goods/updateGoodsState") //下架商品
    Call<CheckSMS> updateGoodsState(@Header("x-auth-token")String token,@Query("id")int goodId,@Query("state")String state);

    @GET("orderlog/addOrderLog") //添加订单
    Call<CheckSMS> addOrderLog(@Header("x-auth-token")String token,@Query("goodsId")int goodsId,@Query("finalPrice")Double finalPrice,@Query("plotId")int plotId);

    @GET("orderlog/addOrderLogGeneration")//新的添加订单
    Call<CheckSMS> addOrderLogGeneration(@Query("orderId") String orderId,
                                         @Query("plotId") int plotId,
                                         @Query("finalPrice") double finalPrice,
                                         @Query("price") String price,
                                         @Query("sum") String sum,
                                         @Query("goodsId") String goodsId,
                                         @Query("userId") int userId,
                                         @Query("cashOrderLogId") int cashOrderLogId,
                                         @Query("payType") int payType);

    @GET("user/searchFriend")//查询好友
    Call<searchDetail> searchDetail(@Query("keyWord")String keyWord);

    @GET("friend/deleteFriend")//删除好友
    Call<CheckSMS> deleteFriend(@Header("x-auth-token")String token,@Query("friendId")int friendId);

    @GET("user/searchDetail") //用户信息
    Call<searchFriendDetail> searchFriendDetail(@Query("id")int id);

    @GET("friendApply/addApply") //添加好友申请
    Call<CheckSMS> addApply(@Header("x-auth-token")String token,@Query("friendId")int friendId);

    @GET("friendApply/searchMyApply") //查询我的申请
    Call<SearchMyApply> searchMyApply(@Header("x-auth-token")String token);

    @GET("friendApply/updateApplyResult") //同意或拒绝好友请求
    Call<CheckSMS> updateApplyResult(@Header("x-auth-token")String token,@Query("id")int id,@Query("isAgree")String isAgree);

    @GET("friendApply/updateApplyState") //删除好友申请
    Call<CheckSMS> updateApplyState(@Header("x-auth-token")String token,@Query("id")int id,@Query("state")String state);

    @GET("group/createGroup") //创建群组
    Call<CheckSMS> createGroup(@Header("x-auth-token")String token,@Query("groupName")String groupName,@Query("desc")String desc,@Query("members")String members);

    @GET("group/dissolveGroup")//解散群组
    Call<CheckSMS> dissolveGroup(@Header("x-auth-token")String token,@Query("uuGroupId")String uuGroupId);

    @GET("chatgroup/deleteGroup")//用户退出群
    Call<CheckSMS> deleteGroup(@Header("x-auth-token")String token,@Query("uuGroupId")String uuGroupId);

    @GET("group/searchGroupMembers")//查询群成员
    Call<searchGroup> searchGroupMembers(@Header("x-auth-token")String token, @Query("uuGroupId")String uuGroupId);

    @GET("group/updateGroupInfo")//修改群信息
    Call<CheckSMS> updateGroupInfo(@Header("x-auth-token")String token, @Query("uuGroupId")String uuGroupId,@Query("groupName")String groupName);

    @GET("camera/cameraController") //摄像头相关接口
    Call<CheckSMS> cameraController(@Header("x-auth-token")String token,
                                    @Query("landNo")int landNo,
                                    @Query("dwPTZCommand")int dwPTZCommand,
                                    @Query("dwStop")int dwStop,
                                    @Query("previousCommand")int previousCommand,
                                    @Query("clientTime")long clientTime,
                                    @Query("clientType")int clientType,
                                    @Query("networkType")int networkType,
                                    @Query("devNo")String devNo);

    @GET("camera/landlordLogin") //地主登录摄像头
    Call<CheckSMS> landlordLogin(@Header("x-auth-token")String token);

    @GET("camera/cleanup") //退出摄像头控制
    Call<CheckSMS> cleanup(@Header("x-auth-token")String token,@Query("landNo")int landNo);

    @GET("user/searchLandlord") //全部用户
    Call<AllUserInfo> searchLandlord(@Header("x-auth-token")String token);

    @GET("CusChat/addCusChatList") //客服聊天
    Call<CusChat> addCusChatList(@Header("x-auth-token")String token);

    @GET("friend/friendRecommendation") //好友推荐
    Call<friendRecommendation> friendRecommendation(@Header("x-auth-token")String token);

    @GET ("config/seachConfig") //配置表
    Call<SeachConfig> seachConfig();

    @GET ("chatgroup/searchInvitationFriend")//查询可拉取群好友
    Call<InvitationFriend> searchInvitationFriend(@Header("x-auth-token")String token,@Query("groupMembers")String groupMembers);

    @GET ("chatgroup/joinGroups") //批量加入群组
    Call<CheckSMS> joinGroups(@Header("x-auth-token")String token, @Query("uuGroupId")String uuGroupId,@Query("user") String userList);

    @POST ("user/setUserPushToken") //设置用户推送token
    Call<PushToken> setUserPushToken(@Header("x-auth-token")String token, @Query("token")String pushToken);

    @Multipart
    @POST("logUpload/androidLogUpload")//上传日志文件
    Call<CheckSMS>androidLogUpload(@Query("tel")String tel,@Part MultipartBody.Part file);

    @GET("terms/searchTerms")//查询使用条款
    Call<TermsModel>searchTerms();

    @GET("user/updateVoice")
    Call<CheckSMS> updateVoice(@Header("x-auth-token")String token, @Query("isVoice") Integer isVoice);

    //鸡棚开水开电API
    //开水 开灯
    @GET("usr/usrOneController")
    Call<CheckSMS> usrOneController(@Query("number") int number, @Query("type") int type, @Query("ip") String ip, @Query("loopClose") int loopClose);

    //查询视频制作背景音乐
    @GET("photoAlbum/searchAudio")
    Call<CheckSMSByResultIsList> searchAudio();

    @POST("photoAlbum/addPhoto")//添加相册
    Call<CheckSMS> addPhoto(
            @Header("x-auth-token")String token,
            @Query("urlString") String urlString,
            @Query("text") String text,
            @Query("audioUrl") String audioUrl,
            @Query("albumName") String albumName,
            @Query("photoAbstract") String photoAbstract,
            @Query("isPublic")String isPublic);

    @GET("photoAlbum/addGrowthAlbum")
    Call<CheckSMS> addGrowthAlbum(@Header("x-auth-token")String token,@Query("equipmentType") int equipmentType,@Query("photoAlubmURL") String photoAlubmURL,@Query("photoUrl") String photoUrl);


    @GET("helpOperation/searchHelpOperationState")//查询操作帮助列表
    Call<HelpList> searchHelp();

//    @POST("user/searchSignRecord")//查询签到信息
//    Call<SignRecord> searchSignRecord(@Header("x-auth-token")String token);

    @GET("luckdraw/searchLuckDraw")//查询奖品
    Call<LuckDraw> searchLuckDraw();

    @POST("luckdraw/userStartRaffle")//开始抽奖
    Call<LuckDrawModel> userStartRaffle(@Header("x-auth-token")String token);

    @GET("luckdraw/searchLuckDrawRecondList")//查询奖品记录列表
    Call<LuckDrawRecord> searchLuckDrawRecondList(@Header("x-auth-token")String token,@Query("state") int state);

    @GET("luckdraw/searchLuckDrawRecondList")//查询奖品记录列表
    Call<LuckDrawRecord> userSearchLuckDrawRecondList (@Header("x-auth-token")String token,@Query("type") int type);

    @GET("AreaAddress/searchAreaAddress")//查询地址
    Call<AreaAddress> searchAreaAddress();

    @GET("receivingDetailedAddress/addReceivingDetailedAddress")//添加兑换奖品信息
    Call<CheckSMS> addReceivingDetailedAddress(
            @Header("x-auth-token")String token,
            @Query("areaAddressId") int areaAddressId,
            @Query("luckDrawRecordId") int luckDrawRecordId,
            @Query("detailedAddress") String detailedAddress,
            @Query("consigneeNumber") String consigneeNumber,
            @Query("consignee") String consignee);

    @GET("luckdraw/searchLuckDrawAddress")//查询抽奖地址（用于获取用户地址信息）
    Call<LuckDrawAddress> searchLuckDrawAddress();

    @POST("luckdraw/addLuckDrawCollectAddress")//抽奖前添加用户地址信息
    Call<CheckSMS> addLuckDrawCollectAddress(
            @Header("x-auth-token") String token,
            @Query("areaAddressId") int areaAddressId ,
            @Query("detailedAddress") String detailedAddress);

    @GET("invitation/addInvitation")//用户邀请码
    Call<CheckSMS> addInvitation(@Header("x-auth-token") String token,@Query("invitationCode") String invitationCode);

    @POST("luckdraw/searchLuckDrawDeductionAmount")//用戶查询自己优惠卡（总金额）
    Call<LuckDrawDeductionAmount> searchLuckDrawDeductionAmount(@Header("x-auth-token") String token);

    @GET("ProjectileDisplay/searchPlotPlantingStandard")//首页查询弹框 和 版本升级判接口
    Call<PlotPlantingStandard> searchPlotPlantingStandard();

    @GET("land/searchUsersLandLeaseHappening")
    Call<LandInfoCardItem> searchUsersLandLeaseHappening(@Header("x-auth-token") String token);

    @GET("CookingMethods/searchCookingMethods") //查询烹饪列表
//    Call<Cooking> searchCookingMethods();
    Call<Cooking> searchCookingMethods(@Query("cropId") int cropId);

    @GET("CookingMethods/searchCookingMethods") //查询用户上传烹饪列表
    Call<Cooking> searchCookingMethodsByUserId(@Header("x-auth-token") String token,@Query("state") int state);

    @GET("CookingMethods/searchCookingMethodsDetails") //查询烹饪详情
    Call<CookingInfo> searchCookingMethodsDetails(@Header("x-auth-token") String token,@Query("cookingId") int cookingId);

    @GET("CookingMethods/searchCookingMethodsDetails")//查询烹饪详情
    Call<CookingInfo> noLoginSearchCookingMethodsDetails(@Header("x-auth-token") String token,@Query("cookingId") int cookingId);

    @POST("CookingMethods/addCookingMethodsPraise")
    Call<CheckSMS> addCookingMethodsPraise(@Header("x-auth-token") String token,@Query("CookingId") int cookingId);

    @GET("ShoppingCart/searchShoppingCart") //查询购物车列表
    Call<ShoppingCart> searchShoppingCart(@Header("x-auth-token") String token);

    @PUT("ShoppingCart/updateShoppingCart") //修改购物车商品数量
    Call<CheckSMS> updateShoppingCart(@Header("x-auth-token") String token,@Query("goodsId") Integer id,@Query("num")Integer num);

    @PUT("ShoppingCart/updateShoppingCartState") //删除购物车商品
    Call<CheckSMS> updateShoppingCartState(@Header("x-auth-token") String token,@Query("goodsId") String goodsId,@Query("state")String state);

    @GET("DishCategory/searchDishCategory")
    Call<DishCategory> searchDishCategory();

    @POST("ShoppingCart/addShoppingCart") //添加购物车
    Call<CheckSMS> addShoppingCart(@Header("x-auth-token") String token,@Query("goodsId")Integer goodsId);

    @GET("adseat/searchAdSeat")//查询广告栏
    Call<AdSeat> searchAdSeat(@Query("type") int type);

    @GET("CropGoodsDetails/searchCropGoodsDetails") //查询商品详情
    Call<CropGoodsDetails> searchCropGoodsDetails(@Query("goodsId") int goodsId);

    @GET("ShoppingCart/searchShoppingCartNum") //查询购物车角标
    Call<ShoppingCartNum> searchShoppingCartNum(@Header("x-auth-token") String token);

    @POST("user/searchSignMonthRecord") //查询月签到信息
    Call<SignMonthRecord> searchSignMonthRecord(@Header("x-auth-token") String token);

    @POST("CookingMethods/addCookingMethods") //添加烹饪菜谱
    Call<CheckSMS> addCookingMethods(@Header("x-auth-token") String token,@Query("CropId")int CropId,@Query("CookingName")String CookingName,@Query("CookingHomePageImg")String CookingHomePageImg,@Query("CookingIngredients")String CookingIngredients,@Query("cookingImg")String cookingImg,@Query("cookingExplain")String cookingExplain);

    @POST("land/searchLandType") //首页根据类型（type 1蔬菜，2家禽）分配租地
    Call<SearchLandByType> searchLandByType (@Header("x-auth-token") String token,@Query("type")int type);

    @GET("track/searchLatelyTrack")//查询近期动态，用于首页公告显示
    Call<LatelyTrack> searchLatelyTrack(@Header("x-auth-token") String token);

    @GET("createActivity/selectCreateActivity")//查询活动
    Call<CreateActivity> selectCreateActivity(@Header("x-auth-token") String token);

    @POST("createActivity/addParticipationActivity")
    Call<CheckSMS> addParticipationActivity(@Header("x-auth-token") String token,@Query("activityId") int activityId);

    @GET("diamond/searchUserConsumptionRecord")//查询消费记录
    Call<TransactionDetails> searchUserConsumptionRecord(@Header("x-auth-token") String token,@Query("pageNum") int pageNum,@Query("pageSize") int pageSize);
}


