package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class CropGoodsDetails {
    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public class ResultBean {
        private int cropId;
        private int num;
        private String basicIntroduction;
        private String cropName;
        private Double discountPrice;

        private String latinName;
        private String edibleAttention;
        private Double bargainPrice;
        private String otherNames;
        private Double price;

        private String nickname;
        private String mainValue;
        private int id;
        private String nutritionalComponents;

        private List<CropGoodsDepictList> cropGoodsDepict;

        private List<CropGoodsDetailsList> cropGoodsDetails;

        public int getCropId() {
            return cropId;
        }

        public void setCropId(int cropId) {
            this.cropId = cropId;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public String getBasicIntroduction() {
            return basicIntroduction;
        }

        public void setBasicIntroduction(String basicIntroduction) {
            this.basicIntroduction = basicIntroduction;
        }

        public String getCropName() {
            return cropName;
        }

        public void setCropName(String cropName) {
            this.cropName = cropName;
        }

        public Double getDiscountPrice() {
            return discountPrice;
        }

        public void setDiscountPrice(Double discountPrice) {
            this.discountPrice = discountPrice;
        }

        public Double getBargainPrice() {
            return bargainPrice;
        }

        public void setBargainPrice(Double bargainPrice) {
            this.bargainPrice = bargainPrice;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Double getPrice() {
            return price;
        }

        public String getLatinName() {
            return latinName;
        }

        public void setLatinName(String latinName) {
            this.latinName = latinName;
        }

        public String getEdibleAttention() {
            return edibleAttention;
        }

        public void setEdibleAttention(String edibleAttention) {
            this.edibleAttention = edibleAttention;
        }


        public String getOtherNames() {
            return otherNames;
        }

        public void setOtherNames(String otherNames) {
            this.otherNames = otherNames;
        }


        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getMainValue() {
            return mainValue;
        }

        public void setMainValue(String mainValue) {
            this.mainValue = mainValue;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNutritionalComponents() {
            return nutritionalComponents;
        }

        public void setNutritionalComponents(String nutritionalComponents) {
            this.nutritionalComponents = nutritionalComponents;
        }

        public List<CropGoodsDepictList> getCropGoodsDepict() {
            return cropGoodsDepict;
        }

        public void setCropGoodsDepict(List<CropGoodsDepictList> cropGoodsDepict) {
            this.cropGoodsDepict = cropGoodsDepict;
        }

        public List<CropGoodsDetailsList> getCropGoodsDetails() {
            return cropGoodsDetails;
        }

        public void setCropGoodsDetails(List<CropGoodsDetailsList> cropGoodsDetails) {
            this.cropGoodsDetails = cropGoodsDetails;
        }

        public class CropGoodsDepictList {
            private int id;
            private int cropId;
            private String depictImg;
            private int state;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getCropId() {
                return cropId;
            }

            public void setCropId(int cropId) {
                this.cropId = cropId;
            }

            public String getDepictImg() {
                return depictImg;
            }

            public void setDepictImg(String depictImg) {
                this.depictImg = depictImg;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }
        }

        public class CropGoodsDetailsList {
            private int id;
            private int cropId;
            private int number;
            private String detailsImg;
            private int state;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getCropId() {
                return cropId;
            }

            public void setCropId(int cropId) {
                this.cropId = cropId;
            }

            public int getNumber() {
                return number;
            }

            public void setNumber(int number) {
                this.number = number;
            }

            public String getDetailsImg() {
                return detailsImg;
            }

            public void setDetailsImg(String detailsImg) {
                this.detailsImg = detailsImg;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }
        }

    }
}
