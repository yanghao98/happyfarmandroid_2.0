package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.MyDynamic;
import zhixin.cn.com.happyfarm_user.model.OrderRecord;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by HuYueling on 2018/5/7.
 */


//RecycleView的适配器，要注意指定的泛型，一般我们就是类名的ViewHolder继承ViewHolder（内部已经实现了复用优化机制）
public class OrderRecordAdapter extends RecyclerView.Adapter<OrderRecordAdapter.StaggerViewHolder> {

    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<OrderRecord> mList;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public OrderRecordAdapter(Context context, List<OrderRecord> list) {
        mContext = context;
        mList = list;
    }

    @Override
    //创建staggerViewHolder，并把ViewHolder返回出去
    public OrderRecordAdapter.StaggerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.order_record_items, null);
        //创建一个staggerViewHolder对象
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(OrderRecordAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        OrderRecord dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        setData(holder, dataBean);
    }

    public void setData(OrderRecordAdapter.StaggerViewHolder holder, OrderRecord data) {
        holder.foodTrading.setText(data.foodTrading);//交易是否成功
        holder.foodDiamond.setText(data.foodDiamond);//交易钻石
        holder.foodBuyName.setText(data.foodBuyName);//买卖家姓名
        holder.foodBuy.setText("卖家：");//买卖家
        holder.foodFertilizer.setText(data.foodFertilizer);//是否有机肥
        holder.foodMedicine.setText("无生物药;");//是否生物药
        holder.foodNum.setText(data.foodNum);//份数
        holder.foodName.setText(data.foodName);//名字
        holder.foodTime.setText(data.foodTime);//时间
        if (NumUtil.checkNull(data.foodImg)){
            holder.foodImg.setImageResource(R.drawable.maintain);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(data.foodImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .error(R.drawable.maintain)//如果请求图像无法加载，则可以使用一个错误绘制。
                    .into(holder.foodImg);//into(ImageView targetImageView)：图片最终要展示的地方。
        }//图片
    }


    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final TextView foodTime;//交易时间
        private final TextView foodTrading;//交易是否成功
        private final TextView foodDiamond;//交易钻石
        private final TextView foodBuyName;//买卖家姓名
        private final TextView foodBuy;//买卖家
        private final TextView foodFertilizer;//是否有机肥
        private final TextView foodMedicine;//是否生物药
        private final TextView foodNum;//份数
        private final TextView foodName;//名字
        private final ImageView foodImg;//图片

        public StaggerViewHolder(View itemView) {
            super(itemView);
            foodTrading = itemView.findViewById(R.id.food_trading);
            foodDiamond = itemView.findViewById(R.id.food_diamond);
            foodBuyName = itemView.findViewById(R.id.food_seller_name);
            foodBuy = itemView.findViewById(R.id.food_seller);
            foodFertilizer = itemView.findViewById(R.id.food_fertilizer);
            foodMedicine = itemView.findViewById(R.id.food_medicine);
            foodNum = itemView.findViewById(R.id.food_num);
            foodName = itemView.findViewById(R.id.food_name);
            foodImg = itemView.findViewById(R.id.food_img);
            foodTime = itemView.findViewById(R.id.order_time);
        }


    }
}
