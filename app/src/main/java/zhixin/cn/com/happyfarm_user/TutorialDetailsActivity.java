package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.SearchTutorialContent;
import zhixin.cn.com.happyfarm_user.other.MyWebChromeClient;

/**
 * Created by Administrator on 2018/3/22.
 */

public class TutorialDetailsActivity extends ABaseActivity {

    private WebView webView;
    private String tutorialId;
    private String tutorialName;
    private int id;
    private String TutorialContent;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_details_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("养护教程");
        progressBar = findViewById(R.id.progressBar1);
        webView = findViewById(R.id.webView);

        /**************************关于图片不能显示暂时加的内容***************************/
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // TODO Auto-generated method stub
                // handler.cancel();// Android默认的处理方式
                handler.proceed();// 接受所有网站的证书
                // handleMessage(Message msg);// 进行其他处理
            }
        });
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(
                    WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }
        /**************************关于图片不能显示暂时加的内容***************************/
        Intent intent = getIntent();
        tutorialId = intent.getStringExtra("tutorialId");
        tutorialName = intent.getStringExtra("tutorialName");
        Log.i("传过去得id：", "onItemClick: " + tutorialId);

        title.setText(tutorialName);
        id = Integer.valueOf(tutorialId).intValue();
//        Log.e("id", String.valueOf(id));
        Call<SearchTutorialContent> searchTutorialContent = HttpHelper.initHttpHelper().searchTutorialContent(id);
        searchTutorialContent.enqueue(new Callback<SearchTutorialContent>() {
            @Override
            public void onResponse(Call<SearchTutorialContent> call, Response<SearchTutorialContent> response) {
                Log.e("data",response.body().getResult().getTutorialContent());
                TutorialContent = response.body().getResult().getTutorialContent();
                webView.getSettings().setJavaScriptEnabled(true);//启用js
                /**************************关于图片不能显示暂时加的内容***************************/
                webView.getSettings().setBlockNetworkImage(false);//解决图片不显示
                webView.clearCache(true); //加载的之前调用即可
                /**************************关于图片不能显示暂时加的内容***************************/
                webView.loadDataWithBaseURL(null,TutorialContent,"text/html","UTF-8",null);
//                webView.loadDataWithBaseURL(null,obj,"text/html","UTF-8",null);

                //开启缓存
                webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                //判断页面加载过程
                webView.setWebChromeClient(new MyWebChromeClient() {
                    @Override
                    public void onProgressChanged(WebView view, int newProgress) {
                        // TODO Auto-generated method stub
                        if (newProgress == 100) {
                            // 网页加载完成
                            progressBar.setVisibility(View.GONE);//加载完网页进度条消失
                        } else {
                            // 加载中
                            progressBar.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                            progressBar.setProgress(newProgress);//设置进度值
                        }

                    }

                });
            }

            @Override
            public void onFailure(Call<SearchTutorialContent> call, Throwable t) {

            }
        });



    }
    public void back(View view){
        finish();
    }
}
