package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.MaterialDialog;
import com.githang.statusbar.StatusBarCompat;
import com.yanzhenjie.recyclerview.swipe.SwipeItemClickListener;
import com.yanzhenjie.recyclerview.swipe.SwipeMenu;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuBridge;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuCreator;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuItem;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuItemClickListener;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.ManageAddressAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.ManageAddress;
import zhixin.cn.com.happyfarm_user.model.searchReceivingAddress;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static zhixin.cn.com.happyfarm_user.utils.Config.isManageAddressRefresh;

/**
 * Created by Administrator on 2018/5/9.
 */

public class ManageAddressActivity extends ABaseActivity {

    private LinearLayout addAddressBtn;
    private SwipeMenuRecyclerView recyclerView;
    private ManageAddressAdapter adapter;
    private ArrayList<ManageAddress> list = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;
    private RelativeLayout relativeLayoutView;
    private String UserRealname;
    private String UserTel;
    private ArrayList<Integer> plotIdList = new ArrayList<>();
    private ArrayList<Integer> addressIdList = new ArrayList<>();
    private ArrayList<Integer> isDefaultList = new ArrayList<>();
    private ArrayList<String> addressList = new ArrayList<>();
    private ArrayList<String> addressTel = new ArrayList<>();
    private ArrayList<String> addressRealname = new ArrayList<>();
    private Dialog mDialog;
    private String orderString;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_address_layout);
        TextView title = findViewById(R.id.title_name);
        title.setText("管理收货地址");
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        Intent intent =getIntent();
        UserRealname = intent.getStringExtra("UserRealname");
        UserTel = intent.getStringExtra("tel");
        orderString = intent.getStringExtra("OrderActivity");
        isManageAddressRefresh = false;
        addAddressBtn = findViewById(R.id.layout_add_address);
        relativeLayoutView =findViewById(R.id.relativeLayout_view);
        getUserInfo();
        addAddressBtn();
        initRecyclerView();
        StaggerLoadData();
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.manage_address_recycler);
        if (!("OrderActivity".equals(orderString))){
            // 设置菜单创建器。
            recyclerView.setSwipeMenuCreator(swipeMenuCreator);
            // 设置菜单Item点击监听。
            recyclerView.setSwipeMenuItemClickListener(mMenuItemClickListener);
        }
        recyclerView.setSwipeItemClickListener(mItemClickListener);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                                           @Override
                                           public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                                               super.getItemOffsets(outRect, view, parent, state);
                                               outRect.set(4, 30, 4, 0);//设置itemView中内容相对边框左，上，右，下距离
                                           }});
//        recyclerView.setBackground(R.layout);
        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new ManageAddressAdapter(ManageAddressActivity.this, list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(ManageAddressActivity.this, 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);

    }
    /**
     * Todo 菜单创建器。在Item要创建菜单的时候调用。
     */
    private SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
        @Override
        public void onCreateMenu(SwipeMenu swipeLeftMenu, SwipeMenu swipeRightMenu, int viewType) {
            int height = AbsListView.LayoutParams.MATCH_PARENT;//.setHeight(ViewGroup.LayoutParams.MATCH_PARENT)
//            Log.i("item高度打印数据：", String.valueOf(height)+String.valueOf(heightView));
            SwipeMenuItem deleteItem = new SwipeMenuItem(ManageAddressActivity.this)
                    .setText("删除") // 文字。
                    .setHeight(height)//设置高，这里使用match_parent，就是与item的高相同
                    .setWidth(200)//设置宽
                    .setBackground(new ColorDrawable(0xFFFF0000))
                    .setTextColor(Color.WHITE) // 文字颜色。
                    .setTextSize(16);// 文字大小。
            swipeRightMenu.addMenuItem(deleteItem);// 添加一个按钮到右侧侧菜单。
        }
    };
    /**
     * Todo item点击事件
     */
    private SwipeItemClickListener mItemClickListener = new SwipeItemClickListener(){
        @Override
        public void onItemClick(View itemView, int position) {
            int addressId = addressIdList.get(position);
            int isDefault = isDefaultList.get(position);
            String address = addressList.get(position);
            int plotId = plotIdList.get(position);
            String addressPhone = addressTel.get(position);
            String userRealname = addressRealname.get(position);
            if ("OrderActivity".equals(orderString)){
                Intent intent = new Intent();
                intent.putExtra("UserRealname", userRealname);
                intent.putExtra("address", address);
                intent.putExtra("plotId", plotId + "");
                intent.putExtra("addressId", addressId + "");
                intent.putExtra("isDefault", isDefault + "");
                intent.putExtra("tel", addressPhone);
                setResult(RESULT_OK,intent);
                finish();
            }else {
                //取消点击item实现修改地址
//                Intent intent = new Intent(ManageAddressActivity.this, ModifyAddressActivity.class);
//                intent.putExtra("newAddress", "修改收货地址");
//                intent.putExtra("UserRealname", userRealname);
//                intent.putExtra("address", address);
//                intent.putExtra("plotId", plotId + "");
//                intent.putExtra("addressId", addressId + "");
//                intent.putExtra("isDefault", isDefault + "");
//                intent.putExtra("tel", addressPhone);
//                startActivity(intent);
            }
        }
    };
    /**
     *TODO 菜单点击监听。
     */
    private SwipeMenuItemClickListener mMenuItemClickListener = new SwipeMenuItemClickListener() {
        @Override
        public void onItemClick(SwipeMenuBridge menuBridge) {
            // 任何操作必须先关闭菜单，否则可能出现Item菜单打开状态错乱。
            menuBridge.closeMenu();
            int direction = menuBridge.getDirection(); // 左侧还是右侧菜单。
            int adapterPosition = menuBridge.getAdapterPosition(); // RecyclerView的Item的position。
            int menuPosition = menuBridge.getPosition(); // 菜单在RecyclerView的Item中的Position。
            switch (menuPosition){
                case 0 :
                    MaterialDialogDefault(adapterPosition);
                    break;
            }
        }
    };
    //TODO item删除弹框
    private void MaterialDialogDefault(int adapterPosition) {
        final AlertUtilBest diyDialog =new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk("确定")
                .setContent("您确定要删除该地址吗？")
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        mDialog = LoadingDialog.createLoadingDialog(ManageAddressActivity.this,"");
                        HttpHelper.initHttpHelper().deleteReceivingAddress(addressIdList.get(adapterPosition)).enqueue(new Callback<CheckSMS>() {
                            @Override
                            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                Log.e("ManageAddress",JSON.toJSONString(response.body()));
                                LoadingDialog.closeDialog(mDialog);
                                if ("success".equals(response.body().getFlag())){
                                    isRefresh(true);
//                                adapter.notifyItemRemoved(adapterPosition);
                                    Toast.makeText(ManageAddressActivity.this,"删除成功",Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(ManageAddressActivity.this,response.body().getResult(),Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckSMS> call, Throwable t) {
                                LoadingDialog.closeDialog(mDialog);
                            }
                        });
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    private void StaggerLoadData() {
        HttpHelper.initHttpHelper().searchReceivingAddress().enqueue(new Callback<searchReceivingAddress>() {
            @Override
            public void onResponse(Call<searchReceivingAddress> call, Response<searchReceivingAddress> response) {
//                Log.i(TAG, "onResponse: "+JSON.toJSONString(response.body().getResult()));
                if ("success".equals(response.body().getFlag())){
                    LoadingDialog.closeDialog(mDialog);
//                    Log.i("地址数据打印：", JSON.toJSONString(response.body()));
                    if (!response.body().getResult().isEmpty()) {
                        for (int i = 0; i <= response.body().getResult().size() - 1; i++) {
                            ManageAddress dataBean = new ManageAddress();
//                        dataBean.textTitle = String.valueOf(response.body().getResult().getList().get(i).getCommenterId());
                            if (response.body().getResult().get(i).getIsDefault() == 1) {
                                dataBean.defaultAddress = "(默认地址)";
                            } else {
                                dataBean.defaultAddress = "";
                            }
                            dataBean.addressId = String.valueOf(response.body().getResult().get(i).getId());
                            dataBean.plotId = String.valueOf(response.body().getResult().get(i).getPlotId());
                            dataBean.isDefault = String.valueOf(response.body().getResult().get(i).getIsDefault());
                            dataBean.manageAddressNum = "自提点地址 " + (i + 1) + " :";
                            dataBean.manageAddressName = response.body().getResult().get(i).getRealname();
                            dataBean.manageAddressDetail = String.valueOf(response.body().getResult().get(i).getAddress());
                            dataBean.manageAddressPhone = String.valueOf(response.body().getResult().get(i).getTel());
                            addressIdList.add(response.body().getResult().get(i).getId());
                            plotIdList.add(response.body().getResult().get(i).getPlotId());
                            isDefaultList.add(response.body().getResult().get(i).getIsDefault());
                            addressList.add(response.body().getResult().get(i).getAddress());
                            addressRealname.add(response.body().getResult().get(i).getRealname());
                            addressTel.add(response.body().getResult().get(i).getTel());
                            list.add(dataBean);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }else {
                    LoadingDialog.closeDialog(mDialog);
                }
            }

            @Override
            public void onFailure(Call<searchReceivingAddress> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
            }
        });
    }
    //TODO 添加收获地址点击事件
    public void addAddressBtn(){
        addAddressBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!NetWorkUtils.isNetworkAvalible(ManageAddressActivity.this)){
                    Toast.makeText(ManageAddressActivity.this,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
                }else if (App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", "").equals("")) {
                    Toast.makeText(ManageAddressActivity.this,"您还未登录",Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(ManageAddressActivity.this, ModifyAddressActivity.class);
                    intent.putExtra("newAddress","新增收货地址");
                    intent.putExtra("UserRealname",UserRealname);
                    intent.putExtra("tel",UserTel);
                    startActivity(intent);
                }
            }
        });
    }
    public void getUserInfo(){
        //TODO 获取用户信息 方便传值
        retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if ("success".equals(response.body().getFlag())){
                    UserRealname = response.body().getResult().getRealname();
                    UserTel = response.body().getResult().getTel();
                }else {
                    Toast.makeText(ManageAddressActivity.this,"网络错误",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                showToastShort(ManageAddressActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        isRefresh(isManageAddressRefresh);
    }

    /**
     * 是否需要刷新数据
     * @param refresh true需要刷新 false不需要
     */
    private void isRefresh(boolean refresh){
        if (refresh) {
            if (!list.isEmpty())
                list.clear();
            if (!isDefaultList.isEmpty())
                isDefaultList.clear();
            if (!addressList.isEmpty())
                addressList.clear();
            if (!plotIdList.isEmpty())
                plotIdList.clear();
            if (!addressIdList.isEmpty())
                addressIdList.clear();
            mDialog = LoadingDialog.createLoadingDialog(ManageAddressActivity.this, "加载数据中");
            StaggerLoadData();
            isManageAddressRefresh = false;
        }
    }

    public void back(View view) {
        finish();
    }

    /**
     * 重写返回键
     * 监听Back键按下事件
     * super.onBackPressed()会自动调用finish()方法,关闭当前Activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
