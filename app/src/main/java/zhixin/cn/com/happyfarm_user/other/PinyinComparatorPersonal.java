package zhixin.cn.com.happyfarm_user.other;

import java.util.Comparator;

import zhixin.cn.com.happyfarm_user.model.PersonalMessage;

/**
 * Created by Administrator on 2018/6/13.
 */

public class PinyinComparatorPersonal implements Comparator<PersonalMessage> {

public int compare(PersonalMessage o1, PersonalMessage o2) {
        if (o1.getLetters().equals("@")
        || o2.getLetters().equals("#")) {
        return -1;
        } else if (o1.getLetters().equals("#")
        || o2.getLetters().equals("@")) {
        return 1;
        } else {
        return o1.getLetters().compareTo(o2.getLetters());
        }
        }
}
