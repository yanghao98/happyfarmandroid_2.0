package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by HuYueling on 2018/5/7.
 */

public class OrderRecord {
    public String foodTime;//时间
    public String foodImg;//图片
    public String foodName;//名字
    public String foodNum;//份数
    public String foodMedicine;//是否生物药
    public String foodFertilizer;//是否有机肥
    public String foodBuy;//买卖家
    public String foodBuyName;//买卖家姓名
    public String foodDiamond;//交易钻石
    public String foodTrading;//交易是否成功
}
