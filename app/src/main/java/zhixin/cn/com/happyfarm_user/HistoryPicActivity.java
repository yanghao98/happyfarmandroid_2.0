package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.squareup.picasso.Picasso;
import com.vondear.rxui.view.dialog.RxDialogEditSureCancel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.LandHistoryPic;
import zhixin.cn.com.happyfarm_user.other.AnimatedGifEncoder;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.other.ImageUtil;
import zhixin.cn.com.happyfarm_user.utils.Utils;

public class HistoryPicActivity extends ABaseActivity {
    private static final String TAG = "HistoryPicActivity";
    private RecyclerView mRecyclerView;
    private List<Data> mData = new ArrayList<>();
    private LinearLayout prompt;
    private Dialog mDialog;
    private ImageView historyImg;
    private List<String> imgArr = new ArrayList<>();
    private List<Bitmap> bitmaps = new ArrayList<>();
    /**
     * 为了让图片添加内容为有序 使用TreeMap
     */
    private Map<String, String> mSelected = new TreeMap<>();
    /**
     * mSelected 与 mSelected2 由于选中图片是根据图片的getStartTime来选择内部的图片，所以mSelected
     * 在实际数据操作时会在图片链接后面拼接递增字段，主要这种做法是为了避免相同时间内的相同图片url都被选中
     * mSelected2主要是记录图片链接
     */
    private Map<String, String> mSelected2 = new TreeMap<>();
    private List<String> data;
    private List<String> Route;
    private Dialog dialog;
    private Dialog myDialog;
    private String path;
    private String photoName;
    private GridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_screenshot);
        int color = Color.parseColor("#2d6d9b");
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        TextView title_name = findViewById(R.id.title_name);
        mRecyclerView = findViewById(R.id.recyclerView);
        prompt = findViewById(R.id.prompt);
        historyImg = findViewById(R.id.hisory_image);
        title_name.setText("历史截屏");
        initData();
        initView();
        //gif生成按钮点击事件
        historyImg.setOnClickListener(view -> {
            if (mSelected.size() < 20) {
                Toast.makeText(HistoryPicActivity.this, "请选择20张照片以上，效果更佳！", Toast.LENGTH_SHORT).show();
            } else {
                EditTextDialog();
            }
        });
        initPath();
    }

    /**
     * 初始化文件夹
     */
    private void initPath() {
        String path1 = Environment.getExternalStorageDirectory().getPath() + "/HappyFarm";
        File file = new File(path1);
        if (!file.exists()) file.mkdir();
        String path2 = Environment.getExternalStorageDirectory().getPath() + "/HappyFarm/Gif";
        File file2 = new File(path2);
        if (!file2.exists()) file2.mkdir();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initView() {
        layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return mData.get(position).type == Type.date ? 2 : 1;
            }
        });
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(new RecyclerView.Adapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                if (viewType == Type.date.ordinal()) {
                    return new DateViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_date, parent, false));
                } else if (viewType == Type.pic.ordinal()) {
                    return new PicViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_picture, parent, false));
                }
                return null;
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                Data data = mData.get(position);
                if (holder instanceof DateViewHolder) {
                    ((DateViewHolder) holder).bindView(data.date);
                } else if (holder instanceof PicViewHolder) {
                    ((PicViewHolder) holder).bindView(data.pic);
                }
            }

            @Override
            public int getItemCount() {
                return mData.size();
            }

            @Override
            public int getItemViewType(int position) {
                return mData.get(position).type.ordinal();
            }
        });
    }

    public void back(View v) {
        finish();
    }

    private void initData() {
//        Log.e("start", getIntent().getStringExtra("startDate"));
        String startDate = getIntent().getStringExtra("startDate");
        String endDate = getIntent().getStringExtra("endDate");
        Integer num = getIntent().getIntExtra("num", 0);
        mDialog = LoadingDialog.createLoadingDialog(HistoryPicActivity.this, "正在加载...");
        Log.e("startDate", startDate);
        Log.e("endDate", endDate);
        Log.e("num", String.valueOf(num));
        HttpHelper.initHttpHelper().landHistroy(startDate, endDate, num).enqueue(new Callback<LandHistoryPic>() {
            @Override
            public void onResponse(Call<LandHistoryPic> call, Response<LandHistoryPic> response) {
                Log.e("History1", JSON.toJSONString(response.body()));
                if (response.isSuccessful() && ("success").equals(response.body().getFlag())) {
                    Log.e("History2", JSON.toJSONString(response.body().getResult()));
                    if (response.body().getResult().getList().isEmpty()) {
                        prompt.setVisibility(View.VISIBLE);
                    } else {
//                        state = true;
//                        mData.clear();
                        mData = new ArrayList<>();

                        for (LandHistoryPic.ResultBean.ListBean resultBean : response.body().getResult().getList()) {
                            String date = DateUtil.formatYYYYMMdd(new Date((long) resultBean.getStartTime()));
                            for (int i = 0; i < resultBean.getPictureMap().size(); i++) {
                                imgArr.add(resultBean.getPictureMap().get(i).getUrl());
                            }
                            mData.add(new Data(date));
                            int index = 0;
                            int flag = 0;
                            for (LandHistoryPic.ResultBean.ListBean.PictureMapBean pictureMapBean : resultBean.getPictureMap()) {
                                flag++;
                                pictureMapBean.setHours(pictureMapBean.getHours());
                                pictureMapBean.setDay(date);
                                pictureMapBean.setIndex(flag);
                                if (index == resultBean.getPictureMap().size() / 2) {
                                    String text = String.format("%s %s:00", pictureMapBean.getDay(), pictureMapBean.getHours());
                                    pictureMapBean.setFlag(true);
                                    mSelected.put(pictureMapBean.getDay(), pictureMapBean.getUrl() + pictureMapBean.getIndex());//由于选中时根据是根据图片时间选图片链接，所以在图片链接后面多拼接一个字段
                                    mSelected2.put(pictureMapBean.getDay(), pictureMapBean.getUrl());//
                                }
                                index++;
                                mData.add(new Data(pictureMapBean));
                            }
                        }
//                        System.out.println("打印历史图片数据：" + imgArr);
                        mRecyclerView.getAdapter().notifyDataSetChanged();
                    }
                    LoadingDialog.closeDialog(mDialog);
                }
            }

            @Override
            public void onFailure(Call<LandHistoryPic> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                Toast.makeText(HistoryPicActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private enum Type {
        date, pic
    }

    private class Data {
        public Data(String date) {
            this.type = Type.date;
            this.date = date;
        }

        public Data(LandHistoryPic.ResultBean.ListBean.PictureMapBean pic) {
            this.type = Type.pic;
            this.pic = pic;
        }

        private String date;

        public String getDate() {
            return date;
        }

        public Type getType() {
            return type;
        }

        public LandHistoryPic.ResultBean.ListBean.PictureMapBean getPic() {
            return pic;
        }

        private Type type;
        private LandHistoryPic.ResultBean.ListBean.PictureMapBean pic;
    }

    private class DateViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_Date;

        DateViewHolder(View itemView) {
            super(itemView);
            tv_Date = itemView.findViewById(R.id.tv_date);
        }

        void bindView(String date) {
            tv_Date.setText(date);
        }
    }

    private class PicViewHolder extends RecyclerView.ViewHolder {
        private RadioButton radioButton;
        private ImageView imageView;
        private TextView tv_time;

        PicViewHolder(View itemView) {
            super(itemView);
            tv_time = itemView.findViewById(R.id.tv_time);
            imageView = itemView.findViewById(R.id.pic);
            radioButton = itemView.findViewById(R.id.history_radio_bt);
        }

        void bindView(LandHistoryPic.ResultBean.ListBean.PictureMapBean pic) {
            String text = String.format("%s %s:00", pic.getDay(), pic.getHours());
            tv_time.setText(text);
            Picasso.get().load(pic.getUrl()).into(imageView);
            String url = mSelected.get(pic.getDay());
            radioButton.setChecked(url != null && url.equals(pic.getUrl() + pic.getIndex()));
            radioButton.setOnClickListener(v -> {
                boolean checked = ((RadioButton) v).isChecked();
                if (checked) {
                    mSelected.put(pic.getDay(), pic.getUrl() + pic.getIndex());
                    mSelected2.put(pic.getDay(), pic.getUrl());
                    mRecyclerView.getAdapter().notifyDataSetChanged();
                }
            });
            imageView.setOnClickListener(v -> {
                ViewCompat.setTransitionName(v, "imageView");
                ActivityOptionsCompat option = ActivityOptionsCompat.makeSceneTransitionAnimation(HistoryPicActivity.this, v, "imageView");
                Intent intent = new Intent(HistoryPicActivity.this, ReviewImageActivity.class);
                intent.putExtra("image_url", pic.getUrl());
                HistoryPicActivity.this.startActivity(intent, option.toBundle());
            });
        }
    }

    /**
     * 制作gif方法
     *
     * @param filename 图片名称
     * @param paths    图片路径
     * @param fps      图片之间间隔时间
     * @param width    宽
     * @param height   高
     * @return String
     * @throws IOException 解析异常
     */
    private String createGif(String filename, List<Bitmap> paths, int fps, int width, int height) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        AnimatedGifEncoder localAnimatedGifEncoder = new AnimatedGifEncoder();
        localAnimatedGifEncoder.start(baos);//start
        localAnimatedGifEncoder.setRepeat(0);//设置生成gif的开始播放时间。0为立即开始播放
        localAnimatedGifEncoder.setDelay(fps);//设置gif动画间隔时间
//        Log.d("相册所选图片路径", String.valueOf(paths));
        if (paths.size() > 0) {
            for (int i = 0; i < paths.size(); i++) {
                //Bitmap bitmap = BitmapFactory.decodeFile(paths.get(i));
                Bitmap resizeBm = ImageUtil.resizeImage(paths.get(i), width, height);
                if (null != resizeBm) {
                    localAnimatedGifEncoder.addFrame(resizeBm);
//                    Log.i(TAG, "createGif: 完成" + i);
                } else {
//                    Log.e(TAG, "下标是第" + i + "张图片是空的");
                }
            }
        }
        localAnimatedGifEncoder.finish();//finish
        String path = Environment.getExternalStorageDirectory().getPath() + "/HappyFarm/Gif/" + filename + ".gif";
        FileOutputStream fos = new FileOutputStream(path);
        baos.writeTo(fos);
        baos.flush();
        fos.flush();
        baos.close();
        fos.close();
//        Log.i(TAG, "createGif: 制作完成");
        return path;
    }





    private void EditTextDialog() {
        final RxDialogEditSureCancel rxDialogEditSureCancel = new RxDialogEditSureCancel(HistoryPicActivity.this);
        rxDialogEditSureCancel.getTitleView().setText("您共选择了" + mSelected.size() + "张图片，" + "请输入相册名");
        rxDialogEditSureCancel.getSureView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("".equals(String.valueOf(rxDialogEditSureCancel.getEditText().getText()))) {
                    Toast.makeText(HistoryPicActivity.this, "请输入相册名", Toast.LENGTH_SHORT).show();
                } else {
                    rxDialogEditSureCancel.cancel();
                    photoName = String.valueOf(rxDialogEditSureCancel.getEditText().getText());
                    myDialog = LoadingDialog.createLoadingDialog(HistoryPicActivity.this, "请耐心等待");
                    data = new ArrayList<>();
                    data.addAll(mSelected2.values());
                    new Thread(t).start();
                }
            }
        });
        rxDialogEditSureCancel.getCancelView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rxDialogEditSureCancel.cancel();
            }
        });
        rxDialogEditSureCancel.show();
    }

    //为了下载图片资源，开辟一个新的子线程
    Thread t = new Thread() {
        public void run() {
            Route = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                //下载图片的路径
                String iPath = String.valueOf(data.get(i));
                Bitmap bitmap = Utils.downloadBitmap(iPath);
                bitmaps.add(bitmap);
//                Log.i(TAG, "run: " + iPath);
            }
            handler.sendEmptyMessage(111);
        }
    };
    //手柄更新的作用
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
//            LoadingDialog.closeDialog(myDialog);
            if (msg.what == 111) {
                dialog = LoadingDialog.createLoadingDialog(HistoryPicActivity.this, "正在制作");
                WindowManager wm = (WindowManager) HistoryPicActivity.this.getSystemService(Context.WINDOW_SERVICE);
                try {
                    path = createGif(photoName, bitmaps, 1000, wm.getDefaultDisplay().getWidth() - 200, 500);
                    LoadingDialog.closeDialog(dialog);
                    Intent intent = new Intent(HistoryPicActivity.this, GrowthAlbumActivity.class);
                    intent.putExtra("path", path);
                    intent.putExtra("name", photoName);
                    intent.putStringArrayListExtra("imgArr", (ArrayList<String>) data);
                    finish();
                    startActivity(intent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
