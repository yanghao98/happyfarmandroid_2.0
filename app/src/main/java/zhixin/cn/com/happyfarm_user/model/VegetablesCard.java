package zhixin.cn.com.happyfarm_user.model;

public class VegetablesCard {
    private String vegetablesIcon;
    private String vegetablesName;
    private String otherNames;
    private String time;
    private String growth;

    public VegetablesCard(String vegetablesIcon, String vegetablesName,String otherNames) {
        this.vegetablesIcon = vegetablesIcon;
        this.vegetablesName = vegetablesName;
        this.otherNames = otherNames;
    }

    public String getVegetablesIcon() {
        return vegetablesIcon;
    }

    public void setVegetablesIcon(String vegetablesIcon) {
        this.vegetablesIcon = vegetablesIcon;
    }

    public String getVegetablesName() {
        return vegetablesName;
    }

    public void setVegetablesName(String vegetablesName) {
        this.vegetablesName = vegetablesName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGrowth() {
        return growth;
    }

    public void setGrowth(String growth) {
        this.growth = growth;
    }
}
