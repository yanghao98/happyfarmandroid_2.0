package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.CuringAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.MyCollectionAdapter;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.MyCollection;
import zhixin.cn.com.happyfarm_user.model.SelectLandCollection;
import zhixin.cn.com.happyfarm_user.other.DateUtil;

import static com.mob.tools.utils.DeviceHelper.getApplication;

public class MyCollectionActivity extends ABaseActivity {

    private ArrayList<MyCollection> list = new ArrayList<>();
    private RecyclerView collection_recycle;
    private RefreshLayout refreshLayout;
    private RelativeLayout myCollectionPrompt;
    private String TAG = "MyCollectionActivity->";
    private int pageNum;
    private int pageSize;
    private MyCollectionAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private Boolean isLastPage = false;
    private ArrayList<String> collUserName = new ArrayList<>();
    private ArrayList<String> collUserId = new ArrayList<>();
    private ArrayList<String> collLandNo = new ArrayList<>();
    private ArrayList<String> collLandId = new ArrayList<>();
    private ArrayList<String> collLeaseTime = new ArrayList<>();
    private ArrayList<String> collOthersTel = new ArrayList<>();
    private ArrayList<String> collOtherUserImg = new ArrayList<>();
    private ArrayList<Integer> collLandType = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_collection_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("我的收藏");

        refreshLayout = findViewById(R.id.collection_refreshLayout);
        collection_recycle = findViewById(R.id.collection_recycle);
        myCollectionPrompt = findViewById(R.id.my_collection_prompt);
        initView();
        pageNum = 1;
        pageSize = 15;
        StaggerLoadData(false,1);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
//        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(MyCollectionActivity.this).setShowBezierWave(false));
        // 设置 Footer 为 球脉冲
        refreshLayout.setRefreshFooter(new ClassicsFooter(MyCollectionActivity.this).setSpinnerStyle(SpinnerStyle.Scale));
        //下拉刷新
        topRefreshLayout();
        bottomRefreshLayout();
//        int spanCount = 1;
//        int spacing = 50;
//        boolean includeEdge = true;
//        collection_recycle.addItemDecoration(new GridSpacingItemDecoration(spanCount,spacing,includeEdge));

    }

    @Override
    protected void onStart() {
        super.onStart();

    }
    private void initView(){
        adapter = new MyCollectionAdapter(MyCollectionActivity.this,list);
        collection_recycle.setAdapter(adapter);
        adapter.setItemClickListener(new CuringAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(MyCollectionActivity.this,LandVideoActivity.class);
                intent.putExtra("UserName",collUserName.get(position));
                intent.putExtra("UserId",collUserId.get(position));
                intent.putExtra("landNo",collLandNo.get(position));
                intent.putExtra("landId",collLandId.get(position));
                String StartTime = DateUtil.getStrTime(collLeaseTime.get(position));
                intent.putExtra("LeaseTime",StartTime);
                intent.putExtra("ExpirationTime",StartTime);
                intent.putExtra("OthersTel",collOthersTel.get(position));
                intent.putExtra("OtherUserImg",collOtherUserImg.get(position));
                intent.putExtra("state","2");
                intent.putExtra("landType", String.valueOf(collLandType.get(position)));
//                Log.e(TAG,collUserId.get(position));
                startActivity(intent);
            }
        });
        gridLayoutManager = new GridLayoutManager(MyCollectionActivity.this,1);
        gridLayoutManager.setReverseLayout(false);
        collection_recycle.setLayoutManager(gridLayoutManager);
    }

    private void StaggerLoadData(Boolean inversion,int pageNum) {
        HttpHelper.initHttpHelper().selectLandCollection(pageNum,pageSize).enqueue(new Callback<SelectLandCollection>() {
            @Override
            public void onResponse(Call<SelectLandCollection> call, Response<SelectLandCollection> response) {
                Log.e(TAG, JSON.toJSONString(response.body()));
                if (response.body().getFlag().equals("success")){
                    if (String.valueOf(response.body().getResult().getList()).equals("[]")){
                        myCollectionPrompt.setVisibility(View.VISIBLE);
                        collection_recycle.setVisibility(View.GONE);
                    }else {
                        myCollectionPrompt.setVisibility(View.GONE);
                        collection_recycle.setVisibility(View.VISIBLE);
                        isLastPage = response.body().getResult().isIsLastPage();
                        for (int i = 0;i<response.body().getResult().getList().size();i++){
                            MyCollection data = new MyCollection();
                            data.UserImg = response.body().getResult().getList().get(i).getHeadImg();
                            data.LandNo = String.valueOf(response.body().getResult().getList().get(i).getLandNo());
                            data.UserName = response.body().getResult().getList().get(i).getNickname();
                            data.StartTime = stampToDate(String.valueOf(response.body().getResult().getList().get(i).getStartTime()));
                            data.EndTime = stampToDate(String.valueOf(response.body().getResult().getList().get(i).getStartTime()));
                            collUserName.add(response.body().getResult().getList().get(i).getNickname());
                            collUserId.add(String.valueOf(response.body().getResult().getList().get(i).getLandUserId()));
                            collLandNo.add(String.valueOf(response.body().getResult().getList().get(i).getLandNo()));
                            collLandId.add(String.valueOf(response.body().getResult().getList().get(i).getLandId()));
                            collLeaseTime.add(String.valueOf(response.body().getResult().getList().get(i).getStartTime()));
                            collOthersTel.add(response.body().getResult().getList().get(i).getTel());
                            collOtherUserImg.add(response.body().getResult().getList().get(i).getHeadImg());
                            collLandType.add(response.body().getResult().getList().get(i).getLandType());
                            list.add(data);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }else {
                    myCollectionPrompt.setVisibility(View.VISIBLE);
                    collection_recycle.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<SelectLandCollection> call, Throwable t) {
                myCollectionPrompt.setVisibility(View.VISIBLE);
                collection_recycle.setVisibility(View.GONE);
            }
        });
    }

    public void back(View view){
        finish();
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (list != null) {
                    list.clear();
                }
                isLastPage = false;
                refreshlayout.setNoMoreData(false);//TODO 恢复没有更多数据的原始状态
                pageNum = 1;
                StaggerLoadData(false,1);
                refreshlayout.finishRefresh(1500/*,false*/);//传入false表示刷新失败
            }
        });
    }
    //上拉加载
    public void bottomRefreshLayout() {
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (isLastPage == true) {
                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
                        } else {
//                            Toast.makeText(getApplication(), "数据加载完毕", Toast.LENGTH_SHORT).show();
                            pageNum++;
                            StaggerLoadData(false, pageNum);
//                            adapter.loadMore(initData());
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }
    public static String stampToDate(String s){
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

}
