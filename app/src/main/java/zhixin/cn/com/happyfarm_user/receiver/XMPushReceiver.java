package zhixin.cn.com.happyfarm_user.receiver;

import android.app.ActivityManager;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.hyphenate.chat.EMMipushReceiver;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;

import java.util.List;

import zhixin.cn.com.happyfarm_user.MainActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.utils.SystemHelper;


/**
 * XMPushReceiver
 *
 * @author: Administrator.
 * @date: 2019/1/30
 */

public class XMPushReceiver extends EMMipushReceiver {

    @Override
    public void onNotificationMessageClicked(Context var1, MiPushMessage var2) {
        Log.i("XMPushReceiver->", "onNotificationMessageClicked: " + var2);
        //用来接收服务器向客户端发送的通知消息，这个回调方法会在用户手动点击通知后触发。
//        super.onNotificationMessageClicked(var1, var2);
        if (SystemHelper.isRun(App.CONTEXT)) {
            SystemHelper.setTopApp(App.getInstance());
            Log.i("XMPushReceiver->", "onNotificationMessageClicked: 后台存活");
            return;
        }
        if (SystemHelper.isRunningForeground(App.CONTEXT)) {
            SystemHelper.setTopApp(App.getInstance());
            Log.i("XMPushReceiver->", "onNotificationMessageClicked: 前台存活");
            return;
        }
        Log.i("XMPushReceiver->", "onNotificationMessageClicked: 不存活");
        Intent var3 = var1.getPackageManager().getLaunchIntentForPackage(var1.getPackageName());
        var1.startActivity(var3);
    }

    @Override
    public void onReceiveRegisterResult(Context var1, MiPushCommandMessage var2) {
        //用来接收客户端向服务器发送注册命令后的响应结果
        super.onReceiveRegisterResult(var1, var2);
        if ("register".equals(var2.getCommand())) {
            List var4 = var2.getCommandArguments();
            String var5 = var4 != null && var4.size() > 0 ? (String) var4.get(0) : null;
            if (var2.getResultCode() == 0L) {
                Log.i("MIPUSH", "mi push reigster success: " + var5);
            } else {
                Log.i("MIPUSH", "mi push register fail: " + var5);
            }
        }
    }

    @Override
    public void onReceivePassThroughMessage(Context context, MiPushMessage miPushMessage) {
        //用来接收服务器向客户端发送的透传消息，收到透传消息会触发。
        Log.i("XMPushReceiver->", "onReceivePassThroughMessage: " + miPushMessage);
    }

    @Override
    public void onNotificationMessageArrived(Context context, MiPushMessage miPushMessage) {
        //用来接收服务器向客户端发送的通知消息，这个回调方法是在通知消息到达客户端时触发。另外应用在前台时不弹出通知的通知消息到达客户端也会触发这个回调函数
        Log.i("XMPushReceiver->", "onNotificationMessageArrived: " + miPushMessage);
    }


    @Override
    public void onCommandResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        //用来接收客户端向服务器发送命令后的响应结果。这里可以收到到各种命令的返回结果，例如注册服务、设置别名之类的结果，在这里可以实现初始化错误然后重启之类的功能。
        Log.i("XMPushReceiver->", "onCommandResult: " + miPushCommandMessage);
    }

    private boolean shouldInit(Context context) {
        ActivityManager am = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE));
        List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainProcessName = context.getPackageName();
        int myPid = android.os.Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断手机当前前台显示的APP
     *
     * @param context
     * @return
     */
    private boolean isAction(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }


}
