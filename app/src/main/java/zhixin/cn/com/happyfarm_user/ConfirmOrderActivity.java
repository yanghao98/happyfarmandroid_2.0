package zhixin.cn.com.happyfarm_user;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alipay.sdk.app.PayTask;
import com.githang.statusbar.StatusBarCompat;
import com.qiniu.android.utils.StringUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.EventBus.ConfirmOrder;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AliPay;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.GoodsInfo;
import zhixin.cn.com.happyfarm_user.model.ShoppingCart;
import zhixin.cn.com.happyfarm_user.model.searchReceivingAddress;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.other.AlertUtilOneButton;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.PayResult;
import zhixin.cn.com.happyfarm_user.utils.Utils;

public class ConfirmOrderActivity extends ABaseActivity {

    private String ImageUrl;
    private String CropName;
    private String isOrganic;
    private String Num;
    private String sellerNickname;
    private String prices;
    private Dialog mDialog;
    private TextView address;
    private TextView addressName;
    private TextView addressPhone;
    private int addressId;
    private TextView totalDiamond;
    private Button submitOrderBtn;
    private int goodId;
    private Double Prices;
    private Dialog dialog;
    private RelativeLayout layoutConfirmOrderAddress;//收货地址布局
    protected List<GoodsInfo> goodsInfoList;
    protected List<Integer> orderIdlist = new ArrayList();
    protected List<Double> priceList = new ArrayList();
    protected List<Integer> sumList = new ArrayList();
    protected List<Integer> goodsId = new ArrayList();
    protected Double mtotalPrice;
    protected int mtotalCount;
    private  String TAG = "ConfirmOrderActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_order_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        TextView title = findViewById(R.id.title_name);
        title.setText(getResources().getString(R.string.confirm_order_title));

        address = findViewById(R.id.confirm_address_name);
        addressName = findViewById(R.id.confirm_order_renalname);
        addressPhone = findViewById(R.id.confirm_address_phone);
        totalDiamond = findViewById(R.id.confirm_order_total_diamond);
        submitOrderBtn = findViewById(R.id.confirm_submit_order_btn);

        Intent intent = getIntent();
        mtotalPrice = intent.getDoubleExtra("mtotalPrice",0.0);
        mtotalCount = intent.getIntExtra("mtotalCount",0);
        goodsInfoList = ( List<GoodsInfo>)intent.getSerializableExtra("goodsInfoList");
//        Log.i("传入到结算页面的参数：","总价：" + mtotalPrice+"几种商品："+mtotalCount+"商品有哪些："+JSON.toJSONString(goodsInfoList));
//        ImageUrl = intent.getStringExtra("imgUrl");
//        CropName = intent.getStringExtra("cropName");
//        isOrganic = intent.getStringExtra("isOrganic");
//        Num = intent.getStringExtra("Num");
//        sellerNickname = intent.getStringExtra("sellerNickname");
//        prices = intent.getStringExtra("prices");
//        goodId = Integer.parseInt(intent.getStringExtra("CorpId"));

//        myImageView = findViewById(R.id.my_vegetables_img);//
//        all_goods_name = findViewById(R.id.all_goods_name);//
//        all_good_fat = findViewById(R.id.all_good_fat);//
//        food_seller_name = findViewById(R.id.food_seller_name);//
//        food_num = findViewById(R.id.food_num);//
//        confirm_good_diamond = findViewById(R.id.confirm_good_diamond);//

        layoutConfirmOrderAddress = findViewById(R.id.layout_confirm_order);

        for (int i = 0; i < goodsInfoList.size(); i++) {
            GoodsInfo goodsInfo = goodsInfoList.get(i);
            orderIdlist.add(Integer.valueOf(goodsInfo.getId()));
            priceList.add(goodsInfo.getPrice());
            sumList.add(goodsInfo.getCount());
            goodsId.add(goodsInfo.getGoodsId());
            LayoutInflater inflater = LayoutInflater.from(ConfirmOrderActivity.this);
            // 获取需要被添加控件的布局
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.gridView_list);
            // 获取需要添加的布局
            LinearLayout layout = (LinearLayout) inflater.inflate(
                    R.layout.confirm_order_item, null).findViewById(R.id.confirm_order_adapter);
            MyImageView goodsImg = layout.findViewById(R.id.my_vegetables_img);
            goodsImg.setImageURL(goodsInfoList.get(i).getGoodsImg());
            TextView allGoodsName = layout.findViewById(R.id.all_goods_name);
            allGoodsName.setText(goodsInfoList.get(i).getDesc());
            TextView foodSellerName = layout.findViewById(R.id.food_seller_name);
            foodSellerName.setText(goodsInfoList.get(i).getNickname());
            TextView foodNum = layout.findViewById(R.id.food_num);
            foodNum.setText(String.valueOf(goodsInfoList.get(i).getCount()));
            TextView confirmGoodDiamond = layout.findViewById(R.id.confirm_good_diamond);
            confirmGoodDiamond.setText(String.valueOf(goodsInfoList.get(i).getPrice()));
            // 将布局加入到当前布局中
            linearLayout.addView(layout);
        }
//        Log.i("新的添加订单", "["+Utils.listToInteger(orderIdlist)+"]"+":"+addressId+":"+mtotalPrice+":"+"["+Utils.listToDouble(priceList)+"]"+":"+"["+Utils.listToInteger(sumList)+"]"+":"+"["+Utils.listToInteger(goodsId)+"]");

//        myImageView.setImageURL(ImageUrl);
//        all_goods_name.setText(CropName);
//        all_good_fat.setText(isOrganic);
//        food_seller_name.setText(sellerNickname);
//        food_num.setText(Num);
//        confirm_good_diamond.setText(prices);

        totalDiamond.setText(String.valueOf(mtotalPrice));
        Prices = mtotalPrice;
        StaggerLoadData(false);
        submitOrderBtn();
    }

    @Override
    protected void onStart() {
        super.onStart();
        changeAddress();
    }

    //查询地址
    private void StaggerLoadData(Boolean inversion) {
        mDialog = LoadingDialog.createLoadingDialog(ConfirmOrderActivity.this, null);
        HttpHelper.initHttpHelper().searchReceivingAddress().enqueue(new Callback<searchReceivingAddress>() {
            @Override
            public void onResponse(Call<searchReceivingAddress> call, Response<searchReceivingAddress> response) {
                Log.i(TAG, "onResponse: " + JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())){
                    LoadingDialog.closeDialog(mDialog);
                    if (null != response.body().getResult() && !response.body().getResult().isEmpty()) {
                        for (int i = 0; i < response.body().getResult().size(); i++) {
                            if (response.body().getResult().get(i).getIsDefault() == 1) {
                                addressId = response.body().getResult().get(i).getPlotId();
                                address.setText(response.body().getResult().get(i).getAddress());
                                addressName.setText(response.body().getResult().get(i).getRealname());
                                addressPhone.setText(response.body().getResult().get(i).getTel());
                            }
                        }

                    }
                }else {
                    LoadingDialog.closeDialog(mDialog);
                }
            }

            @Override
            public void onFailure(Call<searchReceivingAddress> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
            }
        });
    }
    //TODO 更改地址点击事件
    public void changeAddress(){
        layoutConfirmOrderAddress.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!NetWorkUtils.isNetworkAvalible(ConfirmOrderActivity.this)){
                    showToastShort(ConfirmOrderActivity.this, getResources().getString(R.string.no_network));
                }else {
                    Intent intent = new Intent(ConfirmOrderActivity.this, ManageAddressActivity.class);
                    intent.putExtra("OrderActivity","OrderActivity");
                    startActivityForResult(intent,9890); //TODO 往上一页传值
                }
            }
        });
    }
    //TODO 提交订单点击事件
    public void submitOrderBtn(){
        submitOrderBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!NetWorkUtils.isNetworkAvalible(ConfirmOrderActivity.this)){
                    showToastShort(ConfirmOrderActivity.this, getResources().getString(R.string.no_network));
                }else {
//                    Log.i("地址数据打印：", String.valueOf(addressId));
                    if (addressId == 0){
                        showToastShort(ConfirmOrderActivity.this, getResources().getString(R.string.address_no_null));
                    }else {
                        confirm();
                    }

                }
            }
        });
    }
    //TODO 传值回调
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 9890:
                if (resultCode == RESULT_OK){
                    address.setText(data.getStringExtra("address"));
                    addressName.setText(data.getStringExtra("UserRealname"));
                    addressPhone.setText(data.getStringExtra("tel"));
                    addressId = Integer.parseInt(data.getStringExtra("plotId"));
                    Log.i("回调地址id：", "onActivityResult: "+addressId);
                }
                break;
        }
    }

    //TODO 订单确认
    //完善用户信息layout弹窗
    @SuppressLint("SetTextI18n")
    private void confirm(){
        Dialog bottomDialog = new Dialog(ConfirmOrderActivity.this, R.style.dialog);
        View contentView = LayoutInflater.from(ConfirmOrderActivity.this).inflate(R.layout.layout_perfect_information_dialog, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        //设置左右间距
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(ConfirmOrderActivity.this, 100f);
        params.bottomMargin = DensityUtil.dp2px(ConfirmOrderActivity.this, 0f);
        TextView lblTitle = contentView.findViewById(R.id.lease_immediately_bt);
        lblTitle.setText("确认消费");
        TextView contentTitle = contentView.findViewById(R.id.user_info_content);
        contentTitle.setText("您本次将消费 "+Prices+" 元");
        contentView.findViewById(R.id.lease_immediately_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomDialog.cancel();
//                finish();
            }
        });
        contentView.findViewById(R.id.lease_immediately_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                dialog = LoadingDialog.createLoadingDialog(ConfirmOrderActivity.this,"正在加载");
                Intent intent = new Intent(ConfirmOrderActivity.this,PaymentMethodActivity.class);
                intent.putExtra("state","1");
                intent.putExtra("stateType",1);
                intent.putExtra("Money",mtotalPrice+"");
                intent.putExtra("orderId","["+Utils.listToInteger(orderIdlist)+"]");
                intent.putExtra("plotId",addressId);
                intent.putExtra("price","["+Utils.listToDouble(priceList)+"]");
                intent.putExtra("sum","["+Utils.listToInteger(sumList)+"]");
                intent.putExtra("goodsId","["+Utils.listToInteger(goodsId)+"]");
                startActivity(intent);
                //发布任务购买
//                HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
//                    @Override
//                    public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
//                        if (("success").equals(response.body().getFlag())){
//                            if (response.body().getResult().getDiamond()>Prices){
//                                HttpHelper.initHttpHelper().addOrderLogGeneration("["+Utils.listToInteger(orderIdlist)+"]",addressId,mtotalPrice,"["+Utils.listToDouble(priceList)+"]","["+Utils.listToInteger(sumList)+"]","["+Utils.listToInteger(goodsId)+"]").enqueue(new Callback<CheckSMS>() {
//                                    @Override
//                                    public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
////                                        Log.e("11223344", JSON.toJSONString(response.body()));
//                                        if ("success".equals(response.body().getFlag())){
//                                            LoadingDialog.closeDialog(dialog);
//                                            //发送eventbuscode,如果发货页面收到，会执行自动销毁。
//                                            EventBus.getDefault().post(new ConfirmOrder("RefreshGood"));
//                                            //showToastShort(ConfirmOrderActivity.this, getResources().getString(R.string.submit_success));
//                                            NormalDialogOneBtn(getResources().getString(R.string.submit_success), true);
//                                        }else {
//                                            LoadingDialog.closeDialog(dialog);
//                                            NormalDialogOneBtn(response.body().getResult(), false);
//                                            //showToastShort(ConfirmOrderActivity.this, getResources().getString(R.string.submit_failed));
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<CheckSMS> call, Throwable t) {
//                                        LoadingDialog.closeDialog(dialog);
//                                    }
//                                });
//                            }else {
//                                LoadingDialog.closeDialog(dialog);
//                                showToastShort(ConfirmOrderActivity.this, getResources().getString(R.string.not_egg));
//                            }
//                        }else {
//                            LoadingDialog.closeDialog(dialog);
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<GetSelfInfo> call, Throwable t) {
//                        LoadingDialog.closeDialog(dialog);
//                    }
//                });
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    /**
     * 提交订单成功/失败弹框
     * @param pickupCode 中心内容
     * @param isFinish 是否结束页面 true结束
     */
    private void NormalDialogOneBtn(String pickupCode, boolean isFinish) {
        final AlertUtilOneButton diyDialog = new AlertUtilOneButton(this);
        diyDialog.setOk("确定")
                .setContent(pickupCode)
                .setDialogClickListener(new AlertUtilOneButton.DialogClickListener() {
                    @Override
                    public void ok() {
                        diyDialog.cancle();
                        if (isFinish) {
                            finish();
                        }
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    private int getUserId(){
        int userId;
        if (getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }

    public void back(View view){
        finish();
    }

}
