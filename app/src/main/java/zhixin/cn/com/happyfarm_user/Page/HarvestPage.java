package zhixin.cn.com.happyfarm_user.Page;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.HarvestAdapter;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.ConfirmHavestActivity;
import zhixin.cn.com.happyfarm_user.EventBus.VideoEvent;
import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.MainActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.Harvest;
import zhixin.cn.com.happyfarm_user.model.SearchUserLandcrop;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by DELL on 2018/3/21.
 */

public class HarvestPage extends Fragment {

    private List<Harvest> mDatas = new ArrayList<Harvest>();
    private HarvestAdapter harvestAdapter;
    private ListView listView;
//    private String LandNo;
//    private String token;
    private String landId;
    private String states;
    private RefreshLayout refreshLayout;
    private String state;
    private RelativeLayout myHarvestPrompt;
    private LinearLayout enterMarket;
    private int landType;
    private String TAG = "HarvestPage";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.harvest_layout,null,false);

        listView = view.findViewById(R.id.harvest_list);
        refreshLayout = view.findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false));
        myHarvestPrompt = view.findViewById(R.id.my_harvest_prompt);
        enterMarket = view.findViewById(R.id.enter_market_layout);
        //我的租地才请求这些
        if (state.equals("0")){
            if (!mDatas.isEmpty()){
                mDatas.clear();
            }
            harvestNet();
            topRefreshLayout();
        }else {
            myHarvestPrompt.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
        EnterMarket();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        LandNo = ((LandVideoActivity)activity).getLandNos();
//        token = ((LandVideoActivity)activity).getToken();
        landId = ((LandVideoActivity)activity).getLandId();
        state = ((LandVideoActivity)activity).getState();
        landType = ((LandVideoActivity)activity).getLandType();
    }
    //TODO 进入菜场点击事件
    public void EnterMarket(){
        enterMarket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //发送eventbuscode,如果发货页面收到，会执行自动销毁。
                Log.d(TAG, "EnterMarket: 点击了这个进入菜场");
                EventBus.getDefault().post(new VideoEvent("INFOSUCCESSFUL"));
                Intent intent = new Intent(getContext(), MainActivity.class);
                intent.putExtra("page",1);
                startActivity(intent);
            }
        });
    }
    //网络请求
    private void harvestNet(){
        Call<SearchUserLandcrop> searchUserLandcrop = HttpHelper.initHttpHelper().searchUserLandcrop(Integer.valueOf(landId), getUserId());
        searchUserLandcrop.enqueue(new Callback<SearchUserLandcrop>() {
            @Override
            public void onResponse(Call<SearchUserLandcrop> call, Response<SearchUserLandcrop> response) {
                Log.e("HarvestResponse",JSON.toJSONString(response.body()));

                if (("success").equals(response.body().getFlag())){
                    if (response.body().getResult().size() == 0){
                        myHarvestPrompt.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                    }else {
                        myHarvestPrompt.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        for (int i = 0; i <= response.body().getResult().size() - 1; i++) {
                            Harvest harvest = new Harvest();
                            harvest.setCropName(response.body().getResult().get(i).getCropName());
                            harvest.setCropId(String.valueOf(response.body().getResult().get(i).getId()));
                            harvest.setHarvestDiamond(response.body().getResult().get(i).getHarvestDiamond());
                            mDatas.add(harvest);
                            initData(mDatas);
                        }
                    }
                }else {
                    myHarvestPrompt.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    Toast.makeText(getContext(),"获取列表失败",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SearchUserLandcrop> call, Throwable t) {
                myHarvestPrompt.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                Log.e("HarvestResponse","接口错误");
            }
        });
    }
    private void initData(List<Harvest> mDatas){

        harvestAdapter = new HarvestAdapter(getContext(),mDatas);
        listView.setAdapter(harvestAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                userInfoToken(i);
            }
        });
    }
    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!mDatas.isEmpty())
                    mDatas.clear();
                harvestNet();
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }
    private String states(){
        SharedPreferences pref = getActivity().getSharedPreferences("User_data",MODE_PRIVATE);
        states = pref.getString("states","");
        return states;
    }
    public void userInfoToken(int i){

        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("success").equals(response.body().getFlag())) {
                    states();
                    if (states.equals("1")){
                        Toast.makeText(getContext(),"您已处于托管状态",Toast.LENGTH_SHORT).show();
                    }else if (states.equals("0")){
                        Intent intent = new Intent(getContext(),ConfirmHavestActivity.class);
                        intent.putExtra("cropId",mDatas.get(i).getCropId());
                        intent.putExtra("cropname",mDatas.get(i).getCropName());
                        intent.putExtra("landid",landId);
                        intent.putExtra("harvestDiamond",mDatas.get(i).getHarvestDiamond());
                        Log.e("harvestDiamond",mDatas.get(i).getHarvestDiamond()+"");
                        startActivity(intent);
                    }
                }else {
                    noLoginDialog();
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog =new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        intent.putExtra("isAllGoodPage","isAllGoodPage");
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }
}
