package zhixin.cn.com.happyfarm_user.easyMob;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;
import com.squareup.picasso.Picasso;

import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.other.NumUtil;


/**
 * Created by Administrator on 2018/5/23.
 */

public class FriendsProfileActivity extends ABaseActivity {

    private TextView friendsPhone;
    private TextView friendsLandId;
    private ImageView friendsImage;
    private Button addFriendBtn;
    private String phone;
    private String landId;
    private String userId;
    private String userImg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.em_activity_friends_profile);
        TextView title = findViewById(R.id.title_name);
        title.setText("详细资料");

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        Intent intent = getIntent();
        phone = intent.getStringExtra("friendPhone");
        landId = intent.getStringExtra("friendLandId");
        userId = intent.getStringExtra("friendId");
        userImg = intent.getStringExtra("friendImg");
        friendsPhone = findViewById(R.id.friends_profile_phone);
        friendsLandId = findViewById(R.id.friends_profile_landId);
        friendsImage = findViewById(R.id.friends_profile_image);
        addFriendBtn = findViewById(R.id.add_friends_btn);
        friendsPhone.setText(phone);
        friendsLandId.setText(landId);
        if (userId.equals(String.valueOf(getUserId()))){
            addFriendBtn.setVisibility(View.GONE);
        }else {
            addFriendBtn.setVisibility(View.VISIBLE);
        }
        if (NumUtil.checkNull(userImg)){
            friendsImage.setImageResource(R.drawable.icon_user_img);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(userImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                    .into(friendsImage);//into(ImageView targetImageView)：图片最终要展示的地方。
        }
        AddFriend();
    }
    //TODO 添加朋友点击事件
    public void AddFriend(){
        addFriendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //参数为要添加的好友的username和添加理由
                try {
                    EMClient.getInstance().contactManager().addContact(userId,null);
                    Toast.makeText(FriendsProfileActivity.this,"申请已发送！",Toast.LENGTH_SHORT).show();
                } catch (HyphenateException e) {
                    e.printStackTrace();
                    Toast.makeText(FriendsProfileActivity.this,e.getDescription(),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void back(View view) {
        finish();
    }
    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }
}
