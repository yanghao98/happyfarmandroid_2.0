package zhixin.cn.com.happyfarm_user.Page;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.EaseUser;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.HelpAdapter;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.easyMob.ChatActivity;
import zhixin.cn.com.happyfarm_user.model.CusChat;
import zhixin.cn.com.happyfarm_user.model.HelpList;
import zhixin.cn.com.happyfarm_user.model.Helps;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.MODE_PRIVATE;

public class CustomerServicePage extends Fragment {

    private View myView;
    private Button tiwenButton;
    private String TAG = "CustomerServicePage";
    private List<Helps> helpsList = new ArrayList<>();
    private Context myContext;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(getContext(), R.layout.customer_service_page_activity,null);
        myView = view;
        view.findViewById(R.id.customer_service_prompt).setVisibility(View.VISIBLE);
        view.findViewById(R.id.customer_service_content).setVisibility(View.GONE);
        initView(view);
        buttonListener();
        searchHelpList();

        return view;
    }
    private void initView(View view) {
        tiwenButton = view.findViewById(R.id.customer_service_button);
    }
    private void buttonListener() {
        tiwenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customerAllot();
            }
        });
    }


    /**
     * 验证token是否有效
     */
    public  void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                System.out.println("###"+ JSON.toJSONString(response.body()));
                if (("failed").equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(getActivity(), "User_data", strArr);
                } else if (("success").equals(response.body().getFlag())){
                    myView.findViewById(R.id.customer_service_prompt).setVisibility(View.GONE);
                    myView.findViewById(R.id.customer_service_content).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }

    //TODO 客服分配接口
    public void customerAllot(){
        Call<CusChat> cusAllot = HttpHelper.initHttpHelper().addCusChatList();
        cusAllot.enqueue(new Callback<CusChat>() {
            @Override
            public void onResponse(Call<CusChat> call, Response<CusChat> response) {
                if (("failed").equals(response.body().getFlag())) {
                    Toast.makeText(getContext(),response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                } else {
                    for (CusChat.ResultBean user : response.body().getResult()) {
                        if (null != user.getCusUuid()) {
                            EaseUser u = new EaseUser(user.getCusUuid().toString());
                            u.setAvatar("http://www.xtai.com/static/img/side_s01.gif");
                            u.setNickname("客服" + "（" + user.getCusServiceName() + "）");
                            if (!Config.AllEMFriendInfo.containsKey("" + user.getEChatId())) {
                                Config.AllEMFriendInfo.put("" + user.getEChatId(), u);
                            }
                            Config.eChatId = String.valueOf(user.getEChatId());
                        }
                    }
                    //TODO 这里主要是根据用户uuid 存放自己的id 好填入自己的头像
                    EaseUser u = new EaseUser(getUuId());
                    if (!Config.AllEMFriendInfo.containsKey("" + getUserId())) {
                        Config.AllEMFriendInfo.put("" + getUserId(), u);
                    }
                    //TODO 进入聊天
                    Intent intent = new Intent(getContext(), ChatActivity.class);
                    intent.putExtra("customer","customer");
                    intent.putExtra(EaseConstant.EXTRA_USER_ID, String.valueOf(response.body().getResult().get(0).getEChatId()));
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<CusChat> call, Throwable t) {
                try {
                    if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                        Toast.makeText(getContext(), "客服暂时不在线", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Log.e(TAG, "onFailure: "+ e);
                }
            }
        });
    }

    //TODO 获得用户uuid
    private String getUuId() {
        return getActivity().getSharedPreferences("User_data", MODE_PRIVATE).getString("uuid", "");
    }

    //TODO 获得用户id
    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", "").equals("")) {
            userId = 0;
            return userId;
        } else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", ""));
        }
        return userId;
    }

    //查询设置列表
    public void searchHelpList() {
        if (helpsList != null)
            helpsList.clear();
        HttpHelper.initHttpHelper().searchHelp().enqueue(new Callback<HelpList>() {
            @Override
            public void onResponse(Call<HelpList> call, Response<HelpList> response) {
                if ("success".equals(response.body().getFlag())) {
//                    Log.i(TAG, "onResponse: "+ JSON.toJSONString(response.body().getResult().getList()));
                    for (int i = 0;i < response.body().getResult().getList().size(); i++) {
                        Helps helps = new Helps();
                        helps.setId(response.body().getResult().getList().get(i).getId());
                        helps.setHelpName(response.body().getResult().getList().get(i).getHelpName());
                        helps.setHelpUrl(response.body().getResult().getList().get(i).getHelpUrl());
                        helps.setState(response.body().getResult().getList().get(i).getState());
                        helpsList.add(helps);
                    }
                    initHelpAdapter();
                }
            }
            @Override
            public void onFailure(Call<HelpList> call, Throwable t) {

            }
        });
    }

    public void initHelpAdapter() {
        //创建适配器
        HelpAdapter helpAdapter = new HelpAdapter(getActivity(), R.layout.help_item,helpsList);
        ListView listView = myView.findViewById(R.id.help_layout_listView);
        listView.setAdapter(helpAdapter);
    }


    @Override
    public void onStart() {
        super.onStart();
        userInfoTokenTest();
    }
}