package zhixin.cn.com.happyfarm_user.model;

import com.google.gson.Gson;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by HuYueling on 2018/4/9.
 */

public class AmountConfiguration {


    /**
     * result : {"pageNum":1,"pageSize":7,"size":7,"orderBy":null,"startRow":0,"endRow":6,"total":7,"pages":1,"list":[{"id":1,"lease":3,"amount":30,"giveDiamond":10,"startTime":1523949041000,"state":1},{"id":2,"lease":6,"amount":580,"giveDiamond":20,"startTime":1523949064000,"state":1},{"id":3,"lease":12,"amount":1000,"giveDiamond":30,"startTime":1523949114000,"state":1},{"id":4,"lease":24,"amount":1968,"giveDiamond":40,"startTime":1523949155000,"state":1},{"id":5,"lease":60,"amount":4688,"giveDiamond":50,"startTime":1523949264000,"state":1},{"id":6,"lease":99,"amount":9999,"giveDiamond":60,"startTime":1524188135000,"state":1},{"id":7,"lease":120,"amount":123456,"giveDiamond":70,"startTime":1524188211000,"state":1}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public static AmountConfiguration objectFromData(String str) {

        return new Gson().fromJson(str, AmountConfiguration.class);
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 7
         * size : 7
         * orderBy : null
         * startRow : 0
         * endRow : 6
         * total : 7
         * pages : 1
         * list : [{"id":1,"lease":3,"amount":30,"giveDiamond":10,"startTime":1523949041000,"state":1},{"id":2,"lease":6,"amount":580,"giveDiamond":20,"startTime":1523949064000,"state":1},{"id":3,"lease":12,"amount":1000,"giveDiamond":30,"startTime":1523949114000,"state":1},{"id":4,"lease":24,"amount":1968,"giveDiamond":40,"startTime":1523949155000,"state":1},{"id":5,"lease":60,"amount":4688,"giveDiamond":50,"startTime":1523949264000,"state":1},{"id":6,"lease":99,"amount":9999,"giveDiamond":60,"startTime":1524188135000,"state":1},{"id":7,"lease":120,"amount":123456,"giveDiamond":70,"startTime":1524188211000,"state":1}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public static ResultBean objectFromData(String str) {

            return new Gson().fromJson(str, ResultBean.class);
        }

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 1
             * lease : 3
             * amount : 30
             * giveDiamond : 10
             * startTime : 1523949041000
             * state : 1
             */

            private int id;
            private int lease;
            private BigDecimal amount;
            private int giveDiamond;
            private long startTime;
            private int state;

            public static ListBean objectFromData(String str) {

                return new Gson().fromJson(str, ListBean.class);
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getLease() {
                return lease;
            }

            public void setLease(int lease) {
                this.lease = lease;
            }

            public BigDecimal getAmount() {
                return amount;
            }

            public void setAmount(BigDecimal amount) {
                this.amount = amount;
            }

            public int getGiveDiamond() {
                return giveDiamond;
            }

            public void setGiveDiamond(int giveDiamond) {
                this.giveDiamond = giveDiamond;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }
        }
    }
}
