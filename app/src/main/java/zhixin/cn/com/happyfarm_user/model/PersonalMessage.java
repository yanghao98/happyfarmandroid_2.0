package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by Administrator on 2018/6/13.
 */

public class PersonalMessage {

    private String personalImg;
    private String personalName;
    private String personalId;
    private String letters;//显示拼音的首字母

    public String getPersonalImg() {
        return personalImg;
    }

    public void setPersonalImg(String personalImg) {
        this.personalImg = personalImg;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }
}
