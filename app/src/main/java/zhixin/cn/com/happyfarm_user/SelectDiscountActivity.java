package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.WelfareAdapter;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.LuckDrawRecord;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

public class SelectDiscountActivity extends ABaseActivity {

    private RecyclerView select_discount_recyclerView;
    private RefreshLayout select_discount_refreshLayout;
    private WelfareAdapter welfareAdapter;
    private List<LuckDrawRecord.LuckDrawBean.LuckDrawList> list;
    private String TAG = "SelectDiscountActivity";
    private GridLayoutManager gridLayoutManager;

    //要传给下一个页面的信息
    private String landNo;
    private String landId;
    private String lease;
    private String landType;
    String Name;
    String Money;
    String packageTimes;
    String state;
    private Double myMoney;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_discount_activity);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        //设置title文字
        TextView title = findViewById(R.id.title_name);
        title.setText("租地优惠卡");
        searchLuckDrawRecordList();
        initView();
        topRefreshLayout();
    }

    public void initView() {
        select_discount_recyclerView = findViewById(R.id.select_discount_recycler_view);
        select_discount_refreshLayout = findViewById(R.id.select_discount_refreshLayout);
        select_discount_refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
        select_discount_refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        select_discount_refreshLayout.setRefreshHeader(new MaterialHeader(this).setShowBezierWave(false));

        //要传给下一个页面的信息
        Intent intent = getIntent();
        Name = intent.getStringExtra("Name");
        Money = intent.getStringExtra("Money");
        packageTimes = intent.getStringExtra("packageTimes");
        state = intent.getStringExtra("state");
        landNo = intent.getStringExtra("landNo");
        lease = intent.getStringExtra("lease");
        landId = intent.getStringExtra("landId");
        landType = intent.getStringExtra("landType");

    }


    private void initWelfareAdapter () {
//        System.out.println("实例化了Adapter");
        welfareAdapter = new WelfareAdapter(this,list);
        select_discount_recyclerView.setAdapter(welfareAdapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(SelectDiscountActivity.this, 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        select_discount_recyclerView.setLayoutManager(gridLayoutManager);
        welfareAdapter.buttonSetOnclick(new WelfareAdapter.ButtonInterface() {
                @Override
                public void onclick(View view, int position, int luckDrawRecordId,int luckyDrawType) {
                    list.get(position).getDiscount();
//                    System.out.println("格式转关");
//                    System.out.println(Double.valueOf(Money));
//                    System.out.println(list.get(position).getDiscount());
//                    myMoney =Float.valueOf(Money) * Float.valueOf(list.get(position).getDiscount());
                    myMoney =Float.valueOf(Money) * list.get(position).getDiscount();
//                    System.out.println("计算出来的值是：" + myMoney);
                    Toast.makeText(SelectDiscountActivity.this,"优惠卡使用成功",Toast.LENGTH_SHORT).show();
                    finish();
                    Intent intent = new Intent(SelectDiscountActivity.this,PaymentMethodActivity.class);
                    intent.putExtra("state",state);
                    intent.putExtra("Name",Name);
                    intent.putExtra("Money",myMoney.toString());
                    intent.putExtra("packageTimes",packageTimes);
                    intent.putExtra("landNo",landNo);
                    intent.putExtra("landId",landId);
                    intent.putExtra("lease",lease);
                    intent.putExtra("landType",landType);
                    intent.putExtra("luckDrawRecordId",luckDrawRecordId);
                    startActivity(intent);
                }
            });
        }

    /**
     * 查询优惠券方法
     */
        public void searchLuckDrawRecordList(){
            HttpHelper.initHttpHelper().userSearchLuckDrawRecondList(1).enqueue(new Callback<LuckDrawRecord>() {
                @Override
                public void onResponse(Call<LuckDrawRecord> call, Response<LuckDrawRecord> response) {
                    Log.i(TAG, "onResponse: "+ JSON.toJSONString(response.body()));
                    if ("success".equals(response.body().getFlag())) {
                        list = response.body().getResult().getList();
                        initWelfareAdapter();
//                        Toast.makeText(SelectDiscountActivity.this,"数据查询成功",Toast.LENGTH_SHORT).show();
                    } else {
//                        Toast.makeText(SelectDiscountActivity.this,"数据查询失败",Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<LuckDrawRecord> call, Throwable t) {
                    Toast.makeText(SelectDiscountActivity.this,getResources().getString(R.string.no_network),Toast.LENGTH_SHORT).show();
                }
            });
        }
        /**
         * 验证token是否有效
         */
        public  void userInfoTokenTest(){
            Call<Test> test = HttpHelper.initHttpHelper().test();
            test.enqueue(new Callback<Test>() {
                @Override
                public void onResponse(Call<Test> call, Response<Test> response) {
                    if (("failed").equals(response.body().getFlag())) {
                        String[] strArr = {"token", "userId", "userImg", "uuid"};
                        SharedPreferencesUtils.cleanShared(SelectDiscountActivity.this, "User_data", strArr);
                    } else if (("success").equals(response.body().getFlag())){
                        initWelfareAdapter();
                        searchLuckDrawRecordList();
                    }
                }

                @Override
                public void onFailure(Call<Test> call, Throwable t) {
                    Toast.makeText(SelectDiscountActivity.this, TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
                }
            });
        }


    /**
     * 下拉刷新方法
     */
    public void topRefreshLayout() {
        select_discount_refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        select_discount_refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                System.out.println("进来到刷新方法了");
                //判断有没有数据,有的话清零
                if (!list.isEmpty()){
                    list.clear();
                }
                //下拉查询优惠券方法

                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }


    @Override
    public void back(View view) {
        finish();
    }
}
