package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * Created by Administrator on 2018/7/4.
 */

public class CusChat {

    /**
     * result : [{"id":9,"cusServiceId":3,"eChatId":"C3","userId":32,"usersName":"黑不溜秋的","startTime":1530710168000,"cusServiceName":"Thanks","cusUuid":null,"userUuid":null,"state":1}]
     * flag : success
     */

    private String flag;
    private List<ResultBean> result;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * id : 9
         * cusServiceId : 3
         * eChatId : C3
         * userId : 32
         * usersName : 黑不溜秋的
         * startTime : 1530710168000
         * cusServiceName : Thanks
         * cusUuid : null
         * userUuid : null
         * state : 1
         */

        private int id;
        private int cusServiceId;
        private String eChatId;
        private int userId;
        private String usersName;
        private long startTime;
        private String cusServiceName;
        private Object cusUuid;
        private Object userUuid;
        private int state;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCusServiceId() {
            return cusServiceId;
        }

        public void setCusServiceId(int cusServiceId) {
            this.cusServiceId = cusServiceId;
        }

        public String getEChatId() {
            return eChatId;
        }

        public void setEChatId(String eChatId) {
            this.eChatId = eChatId;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUsersName() {
            return usersName;
        }

        public void setUsersName(String usersName) {
            this.usersName = usersName;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public String getCusServiceName() {
            return cusServiceName;
        }

        public void setCusServiceName(String cusServiceName) {
            this.cusServiceName = cusServiceName;
        }

        public Object getCusUuid() {
            return cusUuid;
        }

        public void setCusUuid(Object cusUuid) {
            this.cusUuid = cusUuid;
        }

        public Object getUserUuid() {
            return userUuid;
        }

        public void setUserUuid(Object userUuid) {
            this.userUuid = userUuid;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }
}
