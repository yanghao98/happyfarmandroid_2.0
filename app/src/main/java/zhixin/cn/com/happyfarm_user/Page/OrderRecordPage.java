package zhixin.cn.com.happyfarm_user.Page;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.Adapter.ViewPagerAdapter;
import zhixin.cn.com.happyfarm_user.R;

/**
 * OrderRecordPage
 *
 * @author: Administrator.
 * @date: 2019/4/16
 */
public class OrderRecordPage extends Fragment {

    private List<Fragment> frags;
    private List<String> titles;
    private ViewPager pager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_record_page_layout,null,false);
        initData();
        initPager(view);
        initIndicator(view);
        return view;
    }

    private void initData() {
        frags = new ArrayList<>();
        frags.add(new OrderBuyPage());
        frags.add(new OrderSellPage());

        titles=new ArrayList<>();
        titles.add("已买到商品");
        titles.add("已售出商品");
    }

    private void initPager(View view) {
        pager = view.findViewById(R.id.order_record_viewpage);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager(), frags);
        pager.setAdapter(adapter);
    }

    private void initIndicator(View view) {
        MagicIndicator indicator = view.findViewById(R.id.top_indicator);
        CommonNavigator navigator = new CommonNavigator(getContext());
        navigator.setAdjustMode(true);//设置平分屏幕宽度
        navigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return frags.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int i) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(titles.get(i));
                titleView.setTextSize(14);
                TextPaint tp = titleView.getPaint();
                tp.setFakeBoldText(true);
                titleView.setNormalColor(Color.parseColor("#9a9a9a"));//未选中颜色
                titleView.setSelectedColor(Color.parseColor("#1DAC6D"));//选中颜色
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pager.setCurrentItem(i);
                    }
                });
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);//设置下滑线  自适应文字长度MODE_WRAP_CONTENT MODE_MATCH_EDGE
                linePagerIndicator.setColors(Color.parseColor("#1DAC6D"));
                return linePagerIndicator;
            }
        });
        indicator.setNavigator(navigator);
        /**设置中间竖线**/
//        LinearLayout titleContainer = navigator.getTitleContainer(); // must after setNavigator
//        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
//        titleContainer.setDividerPadding(UIUtil.dip2px(this, 20));
//        titleContainer.setDividerDrawable(getResources().getDrawable(R.drawable.simple_splitter));
        /**设置中间竖线**/
        //绑定viewpager滚动事件
        ViewPagerHelper.bind(indicator, pager);
    }

}
