package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class PlotPlantingStandard {
    private String flag;
    private List<PlotPlantingStandardBean> result;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<PlotPlantingStandardBean> getResult() {
        return result;
    }

    public void setResult(List<PlotPlantingStandardBean> result) {
        this.result = result;
    }

    public static class PlotPlantingStandardBean {
        private int id;
        private String imageUrl;
        private String title;
        private String content;
        private String versionNumber;
        private int type;
        private int state;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getVersionNumber() {
            return versionNumber;
        }

        public void setVersionNumber(String versionNumber) {
            this.versionNumber = versionNumber;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }
}
