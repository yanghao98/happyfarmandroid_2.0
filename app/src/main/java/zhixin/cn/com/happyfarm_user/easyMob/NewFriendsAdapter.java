package zhixin.cn.com.happyfarm_user.easyMob;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Harvest;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by Administrator on 2018/6/6.
 */

public class NewFriendsAdapter extends BaseAdapter implements View.OnClickListener ,Filterable {

    private LayoutInflater mInflater;
    private List<NewFriends> mDatas;
    private Context mContext;
    private InnerItemOnclickListener mListener;
    private ArrayList<NewFriends> mOriginalValues;
    private final Object mLock = new Object();
    private ArrayFilter mFilter;

    public NewFriendsAdapter(Context context, List<NewFriends> datas){
        mContext = context;
        mInflater = LayoutInflater.from(context);
        this.mDatas = datas;

    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.new_friends_items,null);
            holder.nickname = convertView.findViewById(R.id.new_friends_nickname);
            holder.imageView = convertView.findViewById(R.id.new_friends_image);
            holder.agreeBtn = convertView.findViewById(R.id.new_friends_add);
            holder.rejectBtn = convertView.findViewById(R.id.new_friends_refuse);
            holder.isAgree = convertView.findViewById(R.id.new_friends_isAgree);
            convertView.setTag(holder);
        }else {
            holder = (NewFriendsAdapter.ViewHolder) convertView.getTag();
        }
        NewFriends newFriends = mDatas.get(position);
        holder.nickname.setText(newFriends.friendsName);
        //是否同意（-1拒绝 0未处理 1同意）
        if (("1").equals(newFriends.friendsIsAgree)){
            holder.agreeBtn.setVisibility(View.GONE);
            holder.rejectBtn.setVisibility(View.GONE);
            holder.isAgree.setText("已同意");
            holder.isAgree.setVisibility(View.VISIBLE);
        }else if (("-1").equals(newFriends.friendsIsAgree)){
            holder.agreeBtn.setVisibility(View.GONE);
            holder.rejectBtn.setVisibility(View.GONE);
            holder.isAgree.setText("已拒绝");
            holder.isAgree.setVisibility(View.VISIBLE);
        }else if (("拒绝").equals(newFriends.friendsIsAgree)){
            holder.agreeBtn.setVisibility(View.GONE);
            holder.rejectBtn.setVisibility(View.GONE);
            holder.isAgree.setText("申请被拒绝");
            holder.isAgree.setVisibility(View.VISIBLE);
        }else if(("未处理").equals(newFriends.friendsIsAgree)){
            holder.agreeBtn.setVisibility(View.GONE);
            holder.rejectBtn.setVisibility(View.GONE);
            holder.isAgree.setText("申请未处理");
            holder.isAgree.setVisibility(View.VISIBLE);
        }else if(("同意").equals(newFriends.friendsIsAgree)){
            holder.agreeBtn.setVisibility(View.GONE);
            holder.rejectBtn.setVisibility(View.GONE);
            holder.isAgree.setText("申请已同意");
            holder.isAgree.setVisibility(View.VISIBLE);
        } else{
            holder.agreeBtn.setVisibility(View.VISIBLE);
            holder.rejectBtn.setVisibility(View.VISIBLE);
            holder.isAgree.setVisibility(View.GONE);
        }
        if (NumUtil.checkNull(newFriends.friendsImg)){
            holder.imageView.setImageResource(R.drawable.icon_user_img);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
//            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
//                    .load(newFriends.friendsImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
//                    .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
//                    .into(holder.imageView);//into(ImageView targetImageView)：图片最终要展示的地方。
            Glide.with(mContext)
                    .load(newFriends.friendsImg)
                    .into(holder.imageView);
        }
        holder.agreeBtn.setOnClickListener(this);
        holder.rejectBtn.setOnClickListener(this);
        holder.agreeBtn.setTag(position);
        holder.rejectBtn.setTag(position);
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }

    interface InnerItemOnclickListener {
        void itemClick(View v);
    }

    public void setOnInnerItemOnClickListener(InnerItemOnclickListener listener){
        this.mListener=listener;
    }
    @Override
    public void onClick(View v) {
        mListener.itemClick(v);
    }

    public class ViewHolder{
        TextView nickname;
        TextView isAgree;
        CircleImageView imageView;
        Button agreeBtn;
        Button rejectBtn;
    }
    /**
     * 过滤数据的类
     */
    /**
     * <p>An array filter constrains the content of the array adapter with
     * a prefix. Each item that does not start with the supplied prefix
     * is removed from the list.</p>
     * <p/>
     * 一个带有首字母约束的数组过滤器，每一项不是以该首字母开头的都会被移除该list。
     */
    private class ArrayFilter extends Filter {
        //执行刷选
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();//过滤的结果
            //原始数据备份为空时，上锁，同步复制原始数据
            if (mOriginalValues == null) {
                synchronized (mLock) {
                    mOriginalValues = new ArrayList<>(mDatas);
                }
            }
            //当首字母为空时
            if (prefix == null || prefix.length() == 0) {
                ArrayList<NewFriends> list;
                synchronized (mLock) {//同步复制一个原始备份数据
                    list = new ArrayList<>(mOriginalValues);
                }
                results.values = list;
                results.count = list.size();//此时返回的results就是原始的数据，不进行过滤
            } else {
                String prefixString = prefix.toString().toLowerCase();//转化为小写

                ArrayList<NewFriends> values;
                synchronized (mLock) {//同步复制一个原始备份数据
                    values = new ArrayList<>(mOriginalValues);
                }
                final int count = values.size();
                final ArrayList<NewFriends> newValues = new ArrayList<>();

                for (int i = 0; i < count; i++) {
                    final NewFriends value = values.get(i);//从List<User>中拿到User对象
//                    final String valueText = value.toString().toLowerCase();
                    final String valueText = value.friendsName.toString().toLowerCase();//User对象的name属性作为过滤的参数
                    // First match against the whole, non-splitted value
                    if (valueText.startsWith(prefixString) || valueText.indexOf(prefixString.toString()) != -1) {//第一个字符是否匹配
                        newValues.add(value);//将这个item加入到数组对象中
                    } else {//处理首字符是空格
                        final String[] words = valueText.split(" ");
                        final int wordCount = words.length;

                        // Start at index 0, in case valueText starts with space(s)
                        for (int k = 0; k < wordCount; k++) {
                            if (words[k].startsWith(prefixString)) {//一旦找到匹配的就break，跳出for循环
                                newValues.add(value);
                                break;
                            }
                        }
                    }
                }
                results.values = newValues;//此时的results就是过滤后的List<User>数组
                results.count = newValues.size();
            }
            return results;
        }

        //刷选结果
        @Override
        protected void publishResults(CharSequence prefix, FilterResults results) {
            //noinspection unchecked
            mDatas = (List<NewFriends>) results.values;//此时，Adapter数据源就是过滤后的Results
            if (results.count > 0) {
                notifyDataSetChanged();//这个相当于从mDatas中删除了一些数据，只是数据的变化，故使用notifyDataSetChanged()
            } else {
                /**
                 * 数据容器变化 ----> notifyDataSetInValidated

                 容器中的数据变化  ---->  notifyDataSetChanged
                 */
                notifyDataSetInvalidated();//当results.count<=0时，此时数据源就是重新new出来的，说明原始的数据源已经失效了
            }
        }
    }
}
