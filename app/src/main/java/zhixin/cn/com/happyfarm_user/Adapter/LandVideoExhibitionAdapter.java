package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.felipecsl.gifimageview.library.GifImageView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.MyAlbum;
import zhixin.cn.com.happyfarm_user.other.GifDataDownloader;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

public class LandVideoExhibitionAdapter extends RecyclerView.Adapter<LandVideoExhibitionAdapter.StaggerViewHolder>{

//    private LandVideoExhibitionAdapter.MyItemClickListener mItemClickListener;
    private CuringAdapter.MyItemClickListener mItemClickListener;

    private Context mContext;
    private List<MyAlbum> mList;

    public LandVideoExhibitionAdapter(Context context, List<MyAlbum> list){
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public LandVideoExhibitionAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.land_video_exhibition_item, null);
        LandVideoExhibitionAdapter.StaggerViewHolder staggerViewHolder = new LandVideoExhibitionAdapter.StaggerViewHolder(view);
        return staggerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull StaggerViewHolder holder, int position) {
        MyAlbum dataBean = mList.get(position);
        holder.setData(dataBean);
        if (mItemClickListener != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = holder.getLayoutPosition();
                    mItemClickListener.onItemClick(view,pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(mList!=null&&mList.size()>0){
            return mList.size();
        }
        return 0;
    }

//    /**
//     * 在activity里面adapter就是调用的这个方法,将点击事件监听传递过来,并赋值给全局的监听
//     *
//     * @param myItemClickListener
//     */
//    public void setItemClickListener(CuringAdapter.MyItemClickListener myItemClickListener) {
//        this.mItemClickListener = myItemClickListener;
//    }

    /**
     * 在activity里面adapter就是调用的这个方法,将点击事件监听传递过来,并赋值给全局的监听
     *
     * @param myItemClickListener
     */
//    public void setItemClickListener(LandVideoExhibitionAdapter.MyItemClickListener myItemClickListener) {
//        this.mItemClickListener = myItemClickListener;
//    }

    public void setItemClickListener(CuringAdapter.MyItemClickListener myItemClickListener) {
        this.mItemClickListener = myItemClickListener;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView imageName;
//        private TextView gifNum;

        public StaggerViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.exhibition_gifImageView);
            imageName = itemView.findViewById(R.id.exhibition_name);
//            gifNum = itemView.findViewById(R.id.exhibition_num);
        }

        public void setData(MyAlbum data) {
//            gifNum.setText(data.num);
            Log.d("视频路径", "setData: " + data.albumFirstPicture);
            if (NumUtil.checkNull(data.albumFirstPicture)) {
                imageView.setImageResource(R.drawable.icon_test);
            } else {
                //Picasso使用了流式接口的调用方式
                Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                        .load(data.albumFirstPicture)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                        .error(R.drawable.icon_user_img)
                        .into(imageView);//into(ImageView targetImageView)：图片最终要展示的地方。
            }
            imageName.setText(data.albumName);

        }
    }

    /**
     * 创建一个回调接口
     */
    public interface MyItemClickListener {
        void onItemClick(View view, int position);
    }



}
