package zhixin.cn.com.happyfarm_user.model;

import com.google.gson.Gson;

import java.util.List;

public class SearchAllGoods {


    /**
     * result : {"pageNum":1,"pageSize":4,"size":4,"orderBy":null,"startRow":0,"endRow":3,"total":4,"pages":1,"list":[{"id":4,"num":1,"price":12,"remark":"","sellerId":12,"isDeal":0,"startTime":1526387169000,"state":1,"sellerNickname":"你是一个超级神奇的人物","cropName":"萝卜","cropImg":"http://p7blgq6id.bkt.clouddn.com/05.png","isOrganic":1},{"id":2,"num":5,"price":20,"sellerId":2,"isDeal":0,"startTime":1526386242000,"state":1,"sellerNickname":"use2","cropName":"茄子","cropImg":"http://p7blgq6id.bkt.clouddn.com/03.png","isOrganic":0},{"id":3,"num":4,"price":21,"sellerId":4,"isDeal":0,"startTime":1526349544000,"state":1,"sellerNickname":"小哒哒","cropName":"萝卜","cropImg":"http://p7blgq6id.bkt.clouddn.com/05.png","isOrganic":0},{"id":1,"num":2,"price":10,"remark":"备注","sellerId":1,"isDeal":0,"startTime":1526349542000,"state":1,"sellerNickname":"use1","cropName":"白菜","cropImg":"http://p7blgq6id.bkt.clouddn.com/09.png","isOrganic":0}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public static SearchAllGoods objectFromData(String str) {

        return new Gson().fromJson(str, SearchAllGoods.class);
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 4
         * size : 4
         * orderBy : null
         * startRow : 0
         * endRow : 3
         * total : 4
         * pages : 1
         * list : [{"id":4,"num":1,"price":12,"remark":"","sellerId":12,"isDeal":0,"startTime":1526387169000,"state":1,"sellerNickname":"你是一个超级神奇的人物","cropName":"萝卜","cropImg":"http://p7blgq6id.bkt.clouddn.com/05.png","isOrganic":1},{"id":2,"num":5,"price":20,"sellerId":2,"isDeal":0,"startTime":1526386242000,"state":1,"sellerNickname":"use2","cropName":"茄子","cropImg":"http://p7blgq6id.bkt.clouddn.com/03.png","isOrganic":0},{"id":3,"num":4,"price":21,"sellerId":4,"isDeal":0,"startTime":1526349544000,"state":1,"sellerNickname":"小哒哒","cropName":"萝卜","cropImg":"http://p7blgq6id.bkt.clouddn.com/05.png","isOrganic":0},{"id":1,"num":2,"price":10,"remark":"备注","sellerId":1,"isDeal":0,"startTime":1526349542000,"state":1,"sellerNickname":"use1","cropName":"白菜","cropImg":"http://p7blgq6id.bkt.clouddn.com/09.png","isOrganic":0}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public static ResultBean objectFromData(String str) {

            return new Gson().fromJson(str, ResultBean.class);
        }

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 4
             * num : 1
             * price : 12
             * remark :
             * sellerId : 12
             * isDeal : 0
             * startTime : 1526387169000
             * state : 1
             * sellerNickname : 你是一个超级神奇的人物
             * cropName : 萝卜
             * cropImg : http://p7blgq6id.bkt.clouddn.com/05.png
             * isOrganic : 1
             */

            private int id;
            private int num;
            private double price;
            private String remark;
            private int sellerId;
            private int isDeal;
            private long startTime;
            private int state;
            private String sellerNickname;
            private String cropName;
            private String cropImg;
            private int isOrganic;

            public static ListBean objectFromData(String str) {

                return new Gson().fromJson(str, ListBean.class);
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getNum() {
                return num;
            }

            public void setNum(int num) {
                this.num = num;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public int getSellerId() {
                return sellerId;
            }

            public void setSellerId(int sellerId) {
                this.sellerId = sellerId;
            }

            public int getIsDeal() {
                return isDeal;
            }

            public void setIsDeal(int isDeal) {
                this.isDeal = isDeal;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public String getSellerNickname() {
                return sellerNickname;
            }

            public void setSellerNickname(String sellerNickname) {
                this.sellerNickname = sellerNickname;
            }

            public String getCropName() {
                return cropName;
            }

            public void setCropName(String cropName) {
                this.cropName = cropName;
            }

            public String getCropImg() {
                return cropImg;
            }

            public void setCropImg(String cropImg) {
                this.cropImg = cropImg;
            }

            public int getIsOrganic() {
                return isOrganic;
            }

            public void setIsOrganic(int isOrganic) {
                this.isOrganic = isOrganic;
            }
        }
    }
}
