package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by DELL on 2018/3/18.
 */

public class RechargeList {

    String RechargeMoney;

    String RechargeDiamonds;

    public RechargeList(String RechargeMoney,String RechargeDiamonds){

        this.RechargeMoney = RechargeMoney;
        this.RechargeDiamonds = RechargeDiamonds;

    }

    public String getRechargeMoney() {
        return RechargeMoney;
    }

    public void setRechargeMoney(String rechargeMoney) {
        RechargeMoney = rechargeMoney;
    }

    public String getRechargeDiamonds() {
        return RechargeDiamonds;
    }

    public void setRechargeDiamonds(String rechargeDiamonds) {
        RechargeDiamonds = rechargeDiamonds;
    }
}
