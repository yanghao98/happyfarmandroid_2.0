package zhixin.cn.com.happyfarm_user.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import java.util.List;

import zhixin.cn.com.happyfarm_user.MyImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.AllGoods;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;

public class AllGoodAdapter extends RecyclerView.Adapter<AllGoodAdapter.StaggerViewHolder> {

    private AllGoodAdapter.MyItemClickListener mItemClickListener;
    private ButtonInterface buttonInterface;
    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<AllGoods> mList;
    private HolderClickListener mHolderClickListener;

    final class ViewHolder {
        MyImageView imgview;
        LinearLayout button;
    }

    public AllGoodAdapter(Context context, List<AllGoods> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public AllGoodAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.all_good_items, null);
        //创建一个staggerViewHolder对象
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //加入购物车点击事件
        staggerViewHolder.viewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = staggerViewHolder.getAdapterPosition();

//                if (buttonInterface != null) {
////                  接口实例化后的而对象，调用重写后的方法
//                    buttonInterface.onclick(v, position);
//                }
                if(mHolderClickListener!=null){
                    int[] start_location = new int[2];
                    staggerViewHolder.viewHolder.imgview.getLocationInWindow(start_location);//获取点击商品图片的位置
                    Drawable drawable = staggerViewHolder.viewHolder.imgview.getDrawable();//复制一个新的商品图标
                    mHolderClickListener.onHolderClick(drawable,start_location,mList.get(position));
                }
            }
        });
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AllGoodAdapter.StaggerViewHolder holder, int position) {

        //从集合里拿对应item数据对象
        AllGoods dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        holder.setData(dataBean);
        if (mItemClickListener != null) {
            holder.itemView.setOnClickListener(new OnMultiClickListener() {
                @Override
                public void onMultiClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mItemClickListener.onItemClick(v, pos);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final TextView cropNames;
        private final TextView isOrganic;
        private final TextView nums;
        private final TextView sellerNickname;
        private final TextView prices;
        final ViewHolder viewHolder;

        @SuppressLint("WrongViewCast")
        public StaggerViewHolder(View itemView) {
            super(itemView);
            viewHolder = new ViewHolder();
            viewHolder.imgview = itemView.findViewById(R.id.my_vegetables_img);

            cropNames = itemView.findViewById(R.id.all_goods_name);
            isOrganic = itemView.findViewById(R.id.all_good_fat);
            nums = itemView.findViewById(R.id.sales_num);
            sellerNickname = itemView.findViewById(R.id.food_seller_name);
            prices = itemView.findViewById(R.id.my_vegetables_diamond);
            viewHolder.button = itemView.findViewById(R.id.all_good_buy_bt);

        }

        public void setData(AllGoods data) {
            viewHolder.imgview.setImageURL(data.cropImg);
            cropNames.setText(data.cropNames);
            isOrganic.setText(data.isOrganic);
            nums.setText(data.nums);
            sellerNickname.setText(data.sellerNickname);
            prices.setText(data.prices);
        }
    }

    /**
     * 创建一个回调接口
     */
    public interface MyItemClickListener {
        void onItemClick(View view, int position);
    }

    /**
     * 在activity里面adapter就是调用的这个方法,将点击事件监听传递过来,并赋值给全局的监听
     *
     * @param myItemClickListener
     */
    public void setItemClickListener(AllGoodAdapter.MyItemClickListener myItemClickListener) {
        this.mItemClickListener = myItemClickListener;
    }

    /**
     * 按钮点击事件需要的方法
     */
    public void buttonSetOnclick(ButtonInterface buttonInterface) {
        this.buttonInterface = buttonInterface;
    }

    /**
     * 按钮点击事件对应的接口
     */
    public interface ButtonInterface {
        void onclick(View view, int position);
    }

    public void SetOnSetHolderClickListener(HolderClickListener holderClickListener){
        this.mHolderClickListener = holderClickListener;
    }
    public interface HolderClickListener{
        public void onHolderClick(Drawable drawable, int[] start_location,AllGoods goodsInfo);
    }

}
