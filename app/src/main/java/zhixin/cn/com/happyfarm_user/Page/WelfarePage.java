package zhixin.cn.com.happyfarm_user.Page;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.WelfareAdapter;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.PrizeExchangeActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.LuckDrawRecord;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.MODE_PRIVATE;
import static zhixin.cn.com.happyfarm_user.utils.CreateQRCode.createQRCodeBitmap;

public class WelfarePage extends Fragment {

    private RecyclerView recyclerView;
    private RefreshLayout refreshLayout;
    private WelfareAdapter welfareAdapter;
    private GridLayoutManager gridLayoutManager;
    private View myView;
    private String TAG = "WelfarePage";
    private List<LuckDrawRecord.LuckDrawBean.LuckDrawList> list;
    private int luckDrewNumber = 0;
    //二维码用到参数
    private ImageView ZXingIcon;
    private RelativeLayout ZXingLayout;
    private ImageView OnZXingLayout;
    private int width = 800;
    private int height = 800;
    private String character_set = "UTF-8";
    private String error_correction_level = "H";
    private String margin = "1";
    private float logoPercent = 0.2F;
    private Map<String,Object> order = new HashMap<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        userInfoTokenTest();
        View view = inflater.inflate(R.layout.welfare_page_activity,null,false);
        view.findViewById(R.id.my_prompt2).setVisibility(View.VISIBLE);
        view.findViewById(R.id.welfare_recycler_view).setVisibility(View.GONE);
        myView = view;
        initView(view);
        topRefreshLayout();
        return view;
    }

    private void initView(View view) {
        recyclerView = view.findViewById(R.id.welfare_recycler_view);
        refreshLayout = view.findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
//        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()).setShowBezierWave(false));

        ZXingIcon = view.findViewById(R.id.ZXing_icon);
        ZXingLayout = view.findViewById(R.id.ZXing_layout);
        OnZXingLayout = view.findViewById(R.id.on_ZXing_layout);
        OnZXingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ZXingLayout.setVisibility(View.GONE);
            }
        });
    }

    private void initWelfareAdapter() {
//        System.out.println(JSON.toJSONString(list));
        welfareAdapter = new WelfareAdapter(getContext(),list);
        recyclerView.setAdapter(welfareAdapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        try {
            recyclerView.setLayoutManager(gridLayoutManager);
        } catch (Exception e) {
            Log.e(TAG, "initWelfareAdapter: " + e );
        }

        welfareAdapter.buttonSetOnclick(new WelfareAdapter.ButtonInterface() {
            @Override
            public void onclick(View view, int position, int luckDrawRecordId,int luckyDrawType) {
                if (1 == luckyDrawType) {
//                    Toast.makeText(getContext(),luckyDrawType+"我要使用这张打折卡" + luckDrawRecordId,Toast.LENGTH_SHORT).show();
                    noLoginDialog();
                } else if (5 == luckyDrawType){
                    //判断螃蟹数量够不够8只
                    if (8 > luckDrewNumber) {
                        noLoginDialog1();
                    } else {
                        Intent intent = new Intent(getContext(), PrizeExchangeActivity.class);
                        intent.putExtra("luckDrawRecordId",luckDrawRecordId);
                        startActivity(intent);
                    }
                } else {
                    if (6 == luckyDrawType) {
                        int userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
                        order.put("userId",userId);
                        order.put("luckRecord", luckDrawRecordId);
//                        order.put("orderNumber",list.get(position).getOrderNum());
                        Bitmap qrCodeBitmap = createQRCodeBitmap(JSON.toJSONString(order), width, height, character_set, error_correction_level, margin, Color.BLACK, Color.WHITE, BitmapFactory.decodeResource(getResources(), R.mipmap.logo, null), logoPercent);
                        ZXingIcon.setImageBitmap(qrCodeBitmap);
                        ZXingLayout.setVisibility(View.VISIBLE);
                    } else {
                        Intent intent = new Intent(getContext(), PrizeExchangeActivity.class);
                        intent.putExtra("luckDrawRecordId",luckDrawRecordId);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    /**
     * 查询抽奖记录 （福利卡）
     */
    public void searchLuckDrawRecordList(){
        HttpHelper.initHttpHelper().searchLuckDrawRecondList(1).enqueue(new Callback<LuckDrawRecord>() {
            @Override
            public void onResponse(Call<LuckDrawRecord> call, Response<LuckDrawRecord> response) {
//                Log.i(TAG, "onResponse: "+ JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    list = response.body().getResult().getList();
                    luckDrewNumber = response.body().getMessage();
//                    System.out.println(JSON.toJSONString(list));
                    if (0 == list.size()) {
                        myView.findViewById(R.id.my_prompt2).setVisibility(View.VISIBLE);
                        myView.findViewById(R.id.welfare_recycler_view).setVisibility(View.GONE);
                    } else {
                        myView.findViewById(R.id.my_prompt2).setVisibility(View.GONE);
                        myView.findViewById(R.id.welfare_recycler_view).setVisibility(View.VISIBLE);
                    }
                    initWelfareAdapter();
                } else {
                    myView.findViewById(R.id.my_prompt2).setVisibility(View.VISIBLE);
                    myView.findViewById(R.id.welfare_recycler_view).setVisibility(View.GONE);
                    Toast.makeText(myView.getContext(),"数据查询失败",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LuckDrawRecord> call, Throwable t) {
                Toast.makeText(myView.getContext(),getResources().getString(R.string.no_network),Toast.LENGTH_SHORT).show();
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                System.out.println("进来到刷新方法了");
                 //判断有没有数据,有的话清零
                if (!list.isEmpty()){
                    list.clear();
                }
                searchLuckDrawRecordList();
                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }
    //上拉加载
//    public void bottomRefreshLayout() {
//        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
//                refreshLayout.getLayout().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (isLastPage == true) {
//                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
//                            refreshLayout.finishLoadMore();
//                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
//                        } else {
////                            Toast.makeText(getApplication(), "数据加载完毕", Toast.LENGTH_SHORT).show();
//                            pageNum++;
//                            StaggerLoadData(false, pageNum);
////                            adapter.loadMore(initData());
//                            refreshLayout.finishLoadMore();
//                        }
//                    }
//                }, 0);
//            }
//        });
//    }

    /**
     * 验证token是否有效
     */
    public  void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("failed").equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(getContext(), "User_data", strArr);
//                    myView.findViewById(R.id.my_prompt2).setVisibility(View.VISIBLE);
//                    myView.findViewById(R.id.refreshLayout).setVisibility(View.GONE);
                } else if (("success").equals(response.body().getFlag())){
                    myView.findViewById(R.id.my_prompt2).setVisibility(View.GONE);
                    myView.findViewById(R.id.welfare_recycler_view).setVisibility(View.VISIBLE);
                    initWelfareAdapter();
                    searchLuckDrawRecordList();
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 使用打折卡提示弹窗
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog = new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(TextString.welfarePageDialogOkButton)
                .setContent(TextString.welfarePageDialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    /**
     * 使用打折卡提示弹窗
     */
    private void noLoginDialog1(){
        final AlertUtilBest diyDialog = new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(TextString.welfarePageDialogOkButton)
                .setContent("8只螃蟹一箱，礼盒尚未装满，继续加油！")
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }


    @Override
    public void onStart() {
        super.onStart();
        userInfoTokenTest();
    }
    @Override
    public void onResume() {
        super.onResume();
    }
}
