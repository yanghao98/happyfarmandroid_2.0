package zhixin.cn.com.happyfarm_user.model;

/**
 * AliPay
 *
 * @author: Administrator.
 * @date: 2019/8/5
 */
public class AliPay {

    /**
     * result : alipay_sdk=alipay-sdk-java-dynamicVersionNo&app_id=2018041002529973&biz_content=%7B%22body%22%3A%22%7B%5C%22getDiamond%5C%22%3A100.0%2C%5C%22landId%5C%22%3A44%2C%5C%22lease%5C%22%3A12%2C%5C%22userId%5C%22%3A51%7D%22%2C%22out_trade_no%22%3A%22080517412712012%22%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%2C%22subject%22%3A%22%E8%B4%AD%E4%B9%B0%E7%A7%9F%E5%9C%B0%22%2C%22timeout_express%22%3A%2230m%22%2C%22total_amount%22%3A%220.01%22%7D&charset=UTF-8&format=JSON&method=alipay.trade.app.pay&notify_url=http%3A%2F%2Fl19j813973.51mypc.cn%3A20476%2Fapi%2Falipay%2FalipayNotify&sign=fxM4yhRU5N5GaLm%2FvcbCTSFUqaaIGLNOnObBPg72gmZHHjrPU0v9JC%2FQBj10PSKFfc0bgkPdYsND8UqrB8kz5zhhdOz5NETiD%2FDH4xo61pZvoVeAINp3v267CSmNsO7Z5BiVcQtybkH6I0uv%2Bw3wHsbOCsrejgEV%2FlHF5JpNuynZ%2BP9N7H2%2BGsK4woNf%2BPzw6bOyCiw8cXc48Hh3rYXrk95fjorW9RCM16zzYKdMysnW%2FliUyaEuUj3j0%2FesT5xYxRyD1wVse%2BuFgKSAC2QVkuSHixDvWQObywBjMTpMfw0v9ivimmrMZm53jk7NGEuK6NiLkCzotHNWFEbBRhusoQ%3D%3D&sign_type=RSA2&timestamp=2019-08-05+17%3A41%3A28&version=1.0
     * flag : success
     * code : 200
     * message : 订单创建成功
     */

    private String result;
    private String flag;
    private int code;
    private String message;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
