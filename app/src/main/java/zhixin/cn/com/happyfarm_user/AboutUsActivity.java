package zhixin.cn.com.happyfarm_user;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.common.LogUtil;
import com.githang.statusbar.StatusBarCompat;

import java.util.ArrayList;
import java.util.List;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.PackageUtils;
import zhixin.cn.com.happyfarm_user.utils.Utils;

import static zhixin.cn.com.happyfarm_user.utils.Utils.isAvilible;
import static zhixin.cn.com.happyfarm_user.utils.Utils.launchAppDetail;

/**
 * Created by HuYueling on 2018/4/16.
 */
public class AboutUsActivity extends ABaseActivity {

    private ImageView imageView;
    private ImageView aboutUsIcon1;
    private TextView aboutUsVersion;
    private String TAG = "AboutUsActivity";
    private RelativeLayout aboutLayout;
    private Badge messageBadge;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us_layout);
        TextView title = findViewById(R.id.title_name);
        title.setText(getResources().getString(R.string.about_us));
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        initView();
        String versionName = PackageUtils.getVersionName(AboutUsActivity.this);
        aboutUsVersion.setText(getResources().getString(R.string.about_us_version)+ " " + versionName);
    }

    private void initView() {
        imageView = findViewById(R.id.zhixinwang_id);
        aboutUsVersion = findViewById(R.id.about_us_version);
        aboutUsIcon1 = findViewById(R.id.about_us_icon1);
        aboutUsIcon1.setVisibility(View.GONE);
        aboutLayout = findViewById(R.id.about_layout);
        aboutLayout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {
                if (Config.isAbout) {
                    if (Utils.isMIUI()) {
                        launchAppDetail(AboutUsActivity.this,"zhixin.cn.com.happyfarm_user","com.xiaomi.market");
                    } else if (Utils.isEMUI()) {
                        launchAppDetail(AboutUsActivity.this,"zhixin.cn.com.happyfarm_user","com.huawei.appmarket");
                    } else {
                        if (isAvilible(AboutUsActivity.this,"com.tencent.android.qqdownloader")) {
                            launchAppDetail(AboutUsActivity.this,"zhixin.cn.com.happyfarm_user","com.tencent.android.qqdownloader");
                        } else {
                            Uri uri = Uri.parse("https://download.sj.qq.com/upload/connAssitantDownload/upload/MobileAssistant_1.apk");
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    }

                } else {
                    Toast.makeText(AboutUsActivity.this,"已是最新版本！",Toast.LENGTH_LONG).show();
                }
            }
        });

        if (Config.isAbout) {
            aboutUsIcon1.setVisibility(View.VISIBLE);
        }
    }



    public void back(View view) {
        finish();
    }

}
