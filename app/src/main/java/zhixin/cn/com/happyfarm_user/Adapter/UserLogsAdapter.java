package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.UserLogs;

/**
 * Created by DELL on 2018/3/9.
 */

//RecycleView的适配器，要注意指定的泛型，一般我们就是类名的ViewHolder继承ViewHolder（内部已经实现了复用优化机制）
public class UserLogsAdapter extends RecyclerView.Adapter<UserLogsAdapter.StaggerViewHolder> {

    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<UserLogs> mList;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public UserLogsAdapter(Context context, List<UserLogs> list) {
        mContext = context;
        mList = list;
    }

    @Override
    //创建staggerViewHolder，并把ViewHolder返回出去
    public UserLogsAdapter.StaggerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.userlogs_items, null);
        //创建一个staggerViewHolder对象
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(UserLogsAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        UserLogs dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        holder.setData(dataBean);
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final TextView mTaskname;
        private final TextView misReasonable;
        private final TextView mreleaseTime;
        private final TextView misComplete;

        public StaggerViewHolder(View itemView) {
            super(itemView);
            mTaskname = itemView.findViewById(R.id.taskname);
            misReasonable = itemView.findViewById(R.id.isreasonable);
            mreleaseTime = itemView.findViewById(R.id.releasetime);
            misComplete = itemView.findViewById(R.id.iscomplete);
        }

        public void setData(UserLogs data) {
            if (data.isReasonable.equals("")){
                misReasonable.setVisibility(View.INVISIBLE);
            }
            if (data.isReasonable.equals("合理")){
                misReasonable.setVisibility(View.VISIBLE);
                misReasonable.setBackgroundResource(R.drawable.isreasonable_green);
                misReasonable.setTextColor(Color.parseColor("#7dbaa7"));

            }else if (data.isReasonable.equals("不合理")){
                misReasonable.setVisibility(View.VISIBLE);
                misReasonable.setBackgroundResource(R.drawable.isreasonable_red);
                misReasonable.setTextColor(Color.parseColor("#eb9896"));
            }else {
                misReasonable.setVisibility(View.GONE);
            }
            if (data.isComplete.equals("已完成")){
                misComplete.setBackgroundResource(R.drawable.iscomplete_white);
                misComplete.setTextColor(Color.parseColor("#ffffff"));
            }else if (data.isComplete.equals("未完成")){
                misComplete.setBackgroundResource(R.drawable.iscomplete_black);
                misComplete.setTextColor(Color.parseColor("#737373"));
            }else if (data.isComplete.equals("未分配")){
                misComplete.setBackgroundResource(R.drawable.iscomplete_black);
                misComplete.setTextColor(Color.parseColor("#737373"));
            }
            //任务名
            mTaskname.setText(data.taskName);
            //是否合理
            misReasonable.setText(data.isReasonable);
            //创建时间
            mreleaseTime.setText(data.repleaseTime);
            //是否完成
            misComplete.setText(data.isComplete);
        }
    }
}
