package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class SearchLandByType {
    private List<ResultBean> result;
    private String flag;

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public class ResultBean {
//        "id": 314,
//                "landNo": 314,
//                "userId": 1,
//                "details": "",
//                "alias": "C7-SE",
//                "landType": 2,
//                "x": 24,
//                "y": 2,
//                "rent": 0,
//                "leaseTime": 1573136585000,
//                "leaseTerm": 12,
//                "isHosted": 0,
//                "defaultHarvest": 0,
//                "cameraIp": "192.168.2.83",
//                "waterNumber": 1,
//                "lampNumber": 2,
//                "relayIp": "192.168.4.22",
//                "loginHeader": 139901987052720,
//                "startTime": 1562199435000,
//                "updateTime": 1588835707000,
//                "managerId": 1,
//                "state": 1,
//                "expirationTime": 1604758985000,
//                "lastOffTime": 1588836764000,
//                "isShed": 0,
//                "recommend": 1,
        private int id;
        private int landNo;
        private int userId;
        private String details;
        private String alias;
        private int landType;
        private int x;
        private int y;
        private int rent;
        private long leaseTime;
        private int leaseTerm;
        private int isHosted;
        private int defaultHarvest;
        private String cameraIp;
        private int waterNumber;
        private int lampNumber;
        private String relayIp;
        private long loginHeader;
        private long startTime;
        private long updateTime;
        private int managerId;
        private int state;
        private long expirationTime;
        private long lastOffTime;
        private int isShed;
        private int recommend;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLandNo() {
            return landNo;
        }

        public void setLandNo(int landNo) {
            this.landNo = landNo;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public int getLandType() {
            return landType;
        }

        public void setLandType(int landType) {
            this.landType = landType;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getRent() {
            return rent;
        }

        public void setRent(int rent) {
            this.rent = rent;
        }

        public long getLeaseTime() {
            return leaseTime;
        }

        public void setLeaseTime(long leaseTime) {
            this.leaseTime = leaseTime;
        }

        public int getLeaseTerm() {
            return leaseTerm;
        }

        public void setLeaseTerm(int leaseTerm) {
            this.leaseTerm = leaseTerm;
        }

        public int getIsHosted() {
            return isHosted;
        }

        public void setIsHosted(int isHosted) {
            this.isHosted = isHosted;
        }

        public int getDefaultHarvest() {
            return defaultHarvest;
        }

        public void setDefaultHarvest(int defaultHarvest) {
            this.defaultHarvest = defaultHarvest;
        }

        public String getCameraIp() {
            return cameraIp;
        }

        public void setCameraIp(String cameraIp) {
            this.cameraIp = cameraIp;
        }

        public int getWaterNumber() {
            return waterNumber;
        }

        public void setWaterNumber(int waterNumber) {
            this.waterNumber = waterNumber;
        }

        public int getLampNumber() {
            return lampNumber;
        }

        public void setLampNumber(int lampNumber) {
            this.lampNumber = lampNumber;
        }

        public String getRelayIp() {
            return relayIp;
        }

        public void setRelayIp(String relayIp) {
            this.relayIp = relayIp;
        }

        public long getLoginHeader() {
            return loginHeader;
        }

        public void setLoginHeader(long loginHeader) {
            this.loginHeader = loginHeader;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getManagerId() {
            return managerId;
        }

        public void setManagerId(int managerId) {
            this.managerId = managerId;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public long getExpirationTime() {
            return expirationTime;
        }

        public void setExpirationTime(long expirationTime) {
            this.expirationTime = expirationTime;
        }

        public long getLastOffTime() {
            return lastOffTime;
        }

        public void setLastOffTime(long lastOffTime) {
            this.lastOffTime = lastOffTime;
        }

        public int getIsShed() {
            return isShed;
        }

        public void setIsShed(int isShed) {
            this.isShed = isShed;
        }

        public int getRecommend() {
            return recommend;
        }

        public void setRecommend(int recommend) {
            this.recommend = recommend;
        }
    }
}
