package zhixin.cn.com.happyfarm_user.easyMob;

/**
 * Created by Administrator on 2018/6/11.
 */

public class SelectContact {

    public String contactImg;
    public String contactName;
    public String contactId;
    public String letters;//显示拼音的首字母

    public String getContactImg() {
        return contactImg;
    }

    public void setContactImg(String contactImg) {
        this.contactImg = contactImg;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }
}
