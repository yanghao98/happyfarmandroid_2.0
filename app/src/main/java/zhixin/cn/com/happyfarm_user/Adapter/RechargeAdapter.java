package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Programme;
import zhixin.cn.com.happyfarm_user.model.RechargeList;

/**
 * Created by DELL on 2018/3/18.
 */

public class RechargeAdapter extends BaseAdapter{

    private LayoutInflater mInflater;
    private List<RechargeList> mDatas;
    public HashMap<String,Boolean> states=new HashMap<String,Boolean>();
    private int checkedIndex = -1;

    public RechargeAdapter(Context context, List<RechargeList> datas){
        mInflater = LayoutInflater.from(context);
        mDatas = datas;
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null){
            convertView = mInflater.inflate(R.layout.recharge_item,parent,false);
            holder = new ViewHolder();
            holder.rechargeMoney = convertView.findViewById(R.id.recharge_money);
            holder.rechargeDiamonds = convertView.findViewById(R.id.recharge_diamonds);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        RadioButton radio = convertView.findViewById(R.id.recharge_rbt);
        holder.rechargeRbt = radio;
        radio.setClickable(false);

        RechargeList recharge = mDatas.get(position);
        holder.rechargeMoney.setText(recharge.getRechargeMoney());
        holder.rechargeDiamonds.setText(recharge.getRechargeDiamonds());
        holder.rechargeRbt.setFocusable(false);

//        holder.rechargeRbt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //重置，确保最多只有一项被选中
//                for(String key:states.keySet()){
//                    states.put(key, false);
//                }
//                states.put(String.valueOf(position), radio.isChecked());
//                Log.e("erro","点击列数:"+String.valueOf(position)+"  states状态:"+states.get(String.valueOf(position)));
//                RechargeAdapter.this.notifyDataSetChanged();
//            }
//        });

        boolean res  = false;
        if (states.get(String.valueOf(position)) == null || states.get(String.valueOf(position))== false){
            res = false;
            states.put(String.valueOf(position),false);
        }else res = true;

        holder.rechargeRbt.setChecked(res);

        return convertView;

    }

    public class ViewHolder{
        RadioButton rechargeRbt;
        TextView rechargeMoney;
        TextView rechargeDiamonds;
    }

    //用于在activity中重置所有的radiobutton的状态
    public void clearStates(int position){
        // 重置，确保最多只有一项被选中
        for(String key:states.keySet()){
            states.put(key,false);
        }
        states.put(String.valueOf(position), true);
    }
    //用于获取状态值
    public Boolean getStates(int position){
        return states.get(String.valueOf(position));
    }
    //设置状态值
    public void setStates(int position,boolean isChecked){
        states.put(String.valueOf(position),false);
    }

}
