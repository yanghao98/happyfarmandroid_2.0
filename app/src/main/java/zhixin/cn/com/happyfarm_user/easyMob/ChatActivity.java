package zhixin.cn.com.happyfarm_user.easyMob;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMGroup;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.hyphenate.easeui.widget.EaseTitleBar;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.R;

public class ChatActivity extends ABaseActivity {

    protected EaseTitleBar titleBar;
//    protected EaseChatInputMenu inputMenu;
    private static final int ITEM_VIDEO = 11;
//    protected EaseChatFragment.MyItemClickListener extendMenuItemClickListener;
    protected int chatType; //聊天类型
    protected String toChatUsername; //群组id 或 用户id
    protected String customer; //群组id 或 用户id
    private static Map<String,Activity> destoryMap = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        titleBar = findViewById(R.id.title_bar);
        chatType = getIntent().getIntExtra(EaseConstant.EXTRA_CHAT_TYPE,EaseConstant.CHATTYPE_SINGLE);
        toChatUsername = getIntent().getStringExtra(EaseConstant.EXTRA_USER_ID);
        customer = getIntent().getStringExtra("customer");
        if ("customer".equals(customer)){
            titleBar.getRightLayout().setVisibility(View.INVISIBLE);
        } else
            titleBar.getRightLayout().setVisibility(View.VISIBLE);
        Log.i("chatType群聊或者单聊类型", "onCreate: "+chatType);
        titleBar.setBackgroundColor(0xff1DAC6D);
        //TODO　聊天页右上角点击事件　需把继承EaseChatFragment的titleBar点击事件取消
        titleBar.setRightLayoutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chatType == EaseConstant.CHATTYPE_SINGLE) {
                    startActivity(new Intent(ChatActivity.this, FriendsDetailsActivity.class).putExtra(EaseConstant.EXTRA_USER_ID, toChatUsername));
//                    Toast.makeText(getApplicationContext(),"单聊",Toast.LENGTH_SHORT).show();
                } else if (chatType == EaseConstant.CHATTYPE_GROUP){
                    startActivity(new Intent(ChatActivity.this, GroupInfoActivity.class).putExtra(EaseConstant.EXTRA_USER_ID, toChatUsername));
//                    Toast.makeText(getApplicationContext(),"群聊",Toast.LENGTH_SHORT).show();
                }
            }
        });
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        EaseChatFragment chatFragment = (EaseChatFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_chat);
        Bundle args = new Bundle();
        args.putInt(EaseConstant.EXTRA_CHAT_TYPE, getIntent().getIntExtra(EaseConstant.EXTRA_CHAT_TYPE,EaseConstant.CHATTYPE_SINGLE));
        args.putString(EaseConstant.EXTRA_USER_ID, getIntent().getStringExtra(EaseConstant.EXTRA_USER_ID));
        chatFragment.setArguments(args);
        addDestoryActivity(this, String.valueOf(ChatActivity.this));
//        inputMenu = (EaseChatInputMenu) findViewById(com.hyphenate.easeui.R.id.input_menu);
//        registerExtendMenuItem();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (chatType == EaseConstant.CHATTYPE_GROUP) {
            if (getGroupNames().equals("")){
                EMGroup group = EMClient.getInstance().groupManager().getGroup(toChatUsername);
                Log.e("EMUIs",group.getGroupName());
                titleBar.setTitle(group.getGroupName());
            }else {
                titleBar.setTitle(getGroupNames());
            }
        }
}

    //TODO　聊天页面下面的功能.
//    protected void registerExtendMenuItem(){
//        inputMenu.registerExtendMenuItem(com.hyphenate.easeui.R.string.attach_video, com.hyphenate.easeui.R.drawable.em_chat_video_selector,ITEM_VIDEO, extendMenuItemClickListener);
//    }
    /**
     * 添加到销毁队列
     * @param activity 要销毁的activity
     */
    public static void addDestoryActivity(Activity activity, String activityName) {
        destoryMap.put(activityName,activity);
    }
    /**
     *销毁指定Activity
     */
    public static void destoryActivity(String activityName) {
        Set<String> keySet=destoryMap.keySet();
        for (String key:keySet){
            destoryMap.get(key).finish();
        }
    }

    //TODO 获取群名片
    private String getGroupNames(){
        String  GroupNames;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            GroupNames = "";
            return GroupNames;
        }else {
            GroupNames = App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("groupNames","");
        }
        return GroupNames;
    }

    @Override
    public void back(View view) {

    }
}
