package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by Administrator on 2018/5/17.
 */

public class OrderSell {

    public String sellAllTime;//交易时间
    public String sellAllImg;//作物图片
    public String sellAllCropName;//作物名字
    public String sellAllFertilizer;//是否有机肥
    public String sellAllBuyName;//买卖家姓名
    public String sellAllNum;//份数
    public String sellAllOnePrice;//单份价格
    public String sellConsuAmoutP;//合计金额价格
    public String sellOrderNumber;//订单号

}
