package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import zhixin.cn.com.happyfarm_user.ManageAddressActivity;
import zhixin.cn.com.happyfarm_user.ModifyAddressActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.ManageAddress;
import zhixin.cn.com.happyfarm_user.model.MyGoods;

/**
 * Created by Administrator on 2018/5/9.
 */

public class ManageAddressAdapter extends RecyclerView.Adapter<ManageAddressAdapter.StaggerViewHolder>{

    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<ManageAddress> mList;
    private AdapterView.OnItemClickListener itemClickListener;
    private int position;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public ManageAddressAdapter(Context context, List<ManageAddress> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ManageAddressAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.manage_address_items, null);
        //创建一个staggerViewHolder对象
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //立即下架点击事件
        staggerViewHolder.manageAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = staggerViewHolder.getAdapterPosition();
                ManageAddress manageAddress = mList.get(position);
                Intent intent = new Intent(mContext, ModifyAddressActivity.class);
                intent.putExtra("newAddress", "修改收货地址");
                intent.putExtra("UserRealname",manageAddress.manageAddressName);
                intent.putExtra("address", manageAddress.manageAddressDetail);
                intent.putExtra("plotId",  manageAddress.plotId + "");
                intent.putExtra("addressId", manageAddress.addressId + "");
                intent.putExtra("isDefault", manageAddress.isDefault+"");
                intent.putExtra("tel", manageAddress.manageAddressPhone);
                mContext.startActivity(intent);
            }
        });
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(ManageAddressAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        ManageAddress dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        setData(holder,dataBean);
    }

    public void setData(ManageAddressAdapter.StaggerViewHolder holder,ManageAddress data) {
        holder.defaultAddress.setText(data.defaultAddress);
        holder.manageAddressName.setText(data.manageAddressName);
        holder.manageAddressPhone.setText(data.manageAddressPhone);
        holder.manageAddressDetail.setText(data.manageAddressDetail);
        holder.manageAddressNum.setText(data.manageAddressNum);
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {

        private final Button manageAddressBtn;
        private final TextView manageAddressNum;
        private final TextView defaultAddress;
        private final TextView manageAddressName;
        private final TextView manageAddressPhone;
        private final TextView manageAddressDetail;

        public StaggerViewHolder(View itemView) {
            super(itemView);
            defaultAddress = itemView.findViewById(R.id.default_address_view);
            manageAddressName = itemView.findViewById(R.id.manage_address_user_name);
            manageAddressPhone = itemView.findViewById(R.id.manage_address_phone);
            manageAddressDetail = itemView.findViewById(R.id.manage_address_detail);
            manageAddressNum = itemView.findViewById(R.id.layout_manage_address_num);
            manageAddressBtn = itemView.findViewById(R.id.modify_address_btn);
        }

    }
    /**
     * 列表点击事件
     *
     * @param itemClickListener
     */
    public void setOnItemClickListener(AdapterView.OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
