package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class LuckDrawRecord {
    private LuckDrawBean result;
    private String flag;
    private int message;

    public LuckDrawBean getResult() {
        return result;
    }

    public void setResult(LuckDrawBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public static class  LuckDrawBean {
        private int pageNum;
        private int pageSize;
        private int size;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private List<LuckDrawList> list;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public List<LuckDrawList> getList() {
            return list;
        }

        public void setList(List<LuckDrawList> list) {
            this.list = list;
        }

        public static class LuckDrawList {
            private int id;
            private int userId;
            private int luckyDrawType;
            private String userName;
            private String tel;
            private String luckyDrawContent;
            private String imageUrl;
            private int douDay;
            private String startTime;
            private String dueTime;
            private int state;
            private double discount;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getLuckyDrawType() {
                return luckyDrawType;
            }

            public void setLuckyDrawType(int luckyDrawType) {
                this.luckyDrawType = luckyDrawType;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public String getTel() {
                return tel;
            }

            public void setTel(String tel) {
                this.tel = tel;
            }

            public String getLuckyDrawContent() {
                return luckyDrawContent;
            }

            public void setLuckyDrawContent(String luckyDrawContent) {
                this.luckyDrawContent = luckyDrawContent;
            }

            public String getImageUrl() {
                return imageUrl;
            }

            public void setImageUrl(String imageUrl) {
                this.imageUrl = imageUrl;
            }

            public String getStartTime() {
                return startTime;
            }

            public void setStartTime(String startTime) {
                this.startTime = startTime;
            }

            public int getDouDay() {
                return douDay;
            }

            public void setDouDay(int douDay) {
                this.douDay = douDay;
            }

            public String getDueTime() {
                return dueTime;
            }

            public void setDueTime(String dueTime) {
                this.dueTime = dueTime;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public double getDiscount() {
                return discount;
            }

            public void setDiscount(double discount) {
                this.discount = discount;
            }
        }
    }




}
