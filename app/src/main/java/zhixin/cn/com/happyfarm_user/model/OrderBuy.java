package zhixin.cn.com.happyfarm_user.model;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Administrator on 2018/5/17.
 */

public class OrderBuy {

    public String buyAllTime;//交易时间
    public String buyAllImg;//作物图片
    public String buyAllCropName;//作物名字
    public String buyAllFertilizer;//是否有机肥
    public String buyAllBuyName;//买卖家姓名
    public String buyAllNum;//份数
    public String buyAllOnePrice;//单份价格
    public String buyConsuAmoutP;//合计金额价格
    public String buyViewDetail;//查看提货码
    public String buyOrderNumber;//订单号

}
