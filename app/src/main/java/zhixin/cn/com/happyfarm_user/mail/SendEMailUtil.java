package zhixin.cn.com.happyfarm_user.mail;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;
import java.util.Properties;

/**
 * SendEMailUtil
 *
 * @author: Administrator.
 * @date: 2019/4/8
 */

public class SendEMailUtil {

    /**
     * 接收方邮件host
     */
    private static final String HOST = "smtp.mxhichina.com";
    private static final String PROTOCOL = "smtp";

    /**
     * 是否需要身份验证
     */
    private static boolean VALIDATE = true;

    /**
     * 发送方邮箱HOST
     */
    private static final String FROM_HOST = "smtp.qq.com";
    /**
     * 发送方端口
     */
    private static final Integer FROM_PORT = 587;
    /**
     * 发送方邮箱授账号
     */
    private static final String FROM_ADD = "414791185@qq.com";
    /**
     * 发送方邮箱授权码
     */
    private static final String FROM_PSW = "dfacmdsbwiuzbghf";

    private static Transport TS;

    private static Session setProp() throws Exception {
        Properties prop = new Properties();
        prop.setProperty("mail.host", HOST);
        prop.setProperty("mail.transport.protocol", PROTOCOL);
        prop.setProperty("mail.smtp.auth", VALIDATE ? "true" : "false");
        prop.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");//设置为SSL协议
        //使用JavaMail发送邮件的5个步骤
        //1、创建session
        Session session = Session.getInstance(prop);
        //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
        session.setDebug(true);
        return session;
    }

    private static void setTransport(Session session) throws Exception {
        //2、通过session得到transport对象
        TS = session.getTransport();
        //3、连上邮件服务器
        TS.connect(FROM_HOST, FROM_PORT, FROM_ADD, FROM_PSW);
    }

    public static Transport getTS() {
        return TS;
    }

    /**
     * 发送邮件
     * @param toAddress 接收邮件方
     * @param setSubject 主题
     * @param setContent 中心文本
     * @param fileList 附件
     * @return
     * @throws Exception
     */
    public static MimeMessage createMixedMail(String toAddress, String setSubject, String setContent, String[] fileList) throws Exception {
        Session session = setProp();
        setTransport(session);
        //创建邮件
        MimeMessage message = new MimeMessage(session);

        //设置邮件的基本信息
        message.setFrom(new InternetAddress(FROM_ADD));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
        //主题
        message.setSubject(setSubject);

        //正文
        MimeBodyPart text = new MimeBodyPart();
        text.setContent(setContent, "text/html;charset=UTF-8");

        //描述关系:正文和图片
        MimeMultipart mp1 = new MimeMultipart();
        mp1.addBodyPart(text);
        // 设置附件
        if (fileList != null && fileList.length > 0) {
            for (int i = 0; i < fileList.length; i++) {
                MimeBodyPart html = new MimeBodyPart();
                FileDataSource fds = new FileDataSource(fileList[i]);
                html.setDataHandler(new DataHandler(fds));
                html.setFileName(MimeUtility.encodeText(fds.getName(), "UTF-8", "B"));
                mp1.addBodyPart(html);
            }
        }
        mp1.setSubType("related");

        //描述关系:正文和附件
        MimeMultipart mp2 = new MimeMultipart();
        // mp2.addBodyPart(attach);


        //代表正文的bodypart
        MimeBodyPart content = new MimeBodyPart();
        content.setContent(mp1);
        mp2.addBodyPart(content);
        mp2.setSubType("mixed");

        message.setContent(mp2);
        message.saveChanges();
        //返回创建好的的邮件
        return message;
    }

}
