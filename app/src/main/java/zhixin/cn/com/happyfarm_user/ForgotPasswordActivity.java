package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import org.apaches.commons.codec.binary.Base64;

import java.security.PublicKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.ISRegister;
import zhixin.cn.com.happyfarm_user.model.PublicKeyList;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.other.RSAUtil;
import zhixin.cn.com.happyfarm_user.other.TextChangedListener;
import zhixin.cn.com.happyfarm_user.other.Validator;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by DELL on 2018/3/5.
 */

public class ForgotPasswordActivity extends ABaseActivity {

    private Button forgot_bt;
    private String tel_unencrypted;
    private String tel_encryption;
    private String captcha;
    private byte[] tel2;
    private String publicKey1;
    private int count = 60;
    private int COUNT_TIME = 0;
    private Dialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("忘记密码");

        EditText phone = findViewById(R.id.forgot_phone);
        EditText code = findViewById(R.id.forgot_verification_code);

        forgot_bt = findViewById(R.id.forgot_bt);
        forgot_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tel_unencrypted = phone.getText().toString();
//                tel = tel_unencrypted;
                tel2 = tel_unencrypted.getBytes();
                if (Validator.isChinaPhoneLegal(tel_unencrypted)) {
                    mDialog = LoadingDialog.createLoadingDialog(ForgotPasswordActivity.this, "");
                    //验证该手机号是否注册
                    Call<ISRegister> isRegister = HttpHelper.initHttpHelper().isRegister(tel_unencrypted);
                    isRegister.enqueue(new Callback<ISRegister>() {
                        @Override
                        public void onResponse(Call<ISRegister> call, Response<ISRegister> response) {
//                            Log.e("boole", JSON.toJSONString(response.body()));
                            if (("failed").equals(response.body().getFlag())) {
                                //手机号加密
                                Call<PublicKeyList> publicKey = HttpHelper.initHttpHelper().publicKey(tel_unencrypted);
                                publicKey.enqueue(new Callback<PublicKeyList>() {
                                    @Override
                                    public void onResponse(Call<PublicKeyList> call, Response<PublicKeyList> response) {
                                        publicKey1 = response.body().getResult();
                                        try {
                                            PublicKey publickey = RSAUtil.getPublicKey(publicKey1);
                                            tel2 = RSAUtil.encrypt(tel2, publickey);
                                            tel_encryption = Base64.encodeBase64String(tel2);
//                                        Log.e("tel",tel_encryption+"\n"+tel_unencrypted);
                                            //发送验证码
                                            Call<CheckSMS> getSMS = HttpHelper.initHttpHelper().getSMS(tel_encryption, tel_unencrypted);
                                            getSMS.enqueue(new Callback<CheckSMS>() {
                                                @Override
                                                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                                    Log.e("state", JSON.toJSONString(response.body()));
                                                    if (("success").equals(response.body().getFlag())) {
                                                        LoadingDialog.closeDialog(mDialog);
                                                        clickButton(view);
                                                        Toast.makeText(ForgotPasswordActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        LoadingDialog.closeDialog(mDialog);
                                                        Toast.makeText(ForgotPasswordActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
                                                    }

                                                }

                                                @Override
                                                public void onFailure(Call<CheckSMS> call, Throwable t) {
                                                    LoadingDialog.closeDialog(mDialog);
                                                }
                                            });

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<PublicKeyList> call, Throwable t) {
                                        LoadingDialog.closeDialog(mDialog);
                                    }
                                });
                            } else {
                                LoadingDialog.closeDialog(mDialog);
                                Toast.makeText(ForgotPasswordActivity.this, "该手机号尚未注册", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ISRegister> call, Throwable t) {
                            LoadingDialog.closeDialog(mDialog);
                        }
                    });
                } else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(ForgotPasswordActivity.this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
                }

            }
        });


        findViewById(R.id.forgot_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tel_unencrypted = phone.getText().toString();
                captcha = code.getText().toString();
                if (NumUtil.checkNull(tel_unencrypted)) {
                    Toast.makeText(ForgotPasswordActivity.this, "请输入手机号", Toast.LENGTH_SHORT).show();
                } else if (NumUtil.checkNull(captcha)) {
                    Toast.makeText(ForgotPasswordActivity.this, "请输入验证码", Toast.LENGTH_SHORT).show();
                } else {
                    mDialog = LoadingDialog.createLoadingDialog(ForgotPasswordActivity.this, "");
                    //验证该手机号是否注册
                    Call<ISRegister> isRegister = HttpHelper.initHttpHelper().isRegister(tel_unencrypted);
                    isRegister.enqueue(new Callback<ISRegister>() {
                        @Override
                        public void onResponse(Call<ISRegister> call, Response<ISRegister> response) {
//                            Log.e("state", JSON.toJSONString(response.body()));
                            if (("failed").equals(response.body().getFlag())) {
                                Call<CheckSMS> checkSMS = HttpHelper.initHttpHelper().checkSMS(tel_unencrypted, captcha);
                                checkSMS.enqueue(new Callback<CheckSMS>() {
                                    @Override
                                    public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                        Log.e("state", JSON.toJSONString(response.body()));
                                        if (("success").equals(response.body().getFlag())) {
                                            LoadingDialog.closeDialog(mDialog);
                                            Intent intent = new Intent(ForgotPasswordActivity.this, ModifyPasswordActivity.class);
                                            intent.putExtra("tel", tel_unencrypted);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            LoadingDialog.closeDialog(mDialog);
                                            Toast.makeText(ForgotPasswordActivity.this, "验证码错误", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CheckSMS> call, Throwable t) {
                                        LoadingDialog.closeDialog(mDialog);
                                    }
                                });
                            } else {
                                LoadingDialog.closeDialog(mDialog);
                                Toast.makeText(ForgotPasswordActivity.this, "该手机号尚未注册", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ISRegister> call, Throwable t) {
                            t.printStackTrace();
                            LoadingDialog.closeDialog(mDialog);
                        }
                    });
                }
//
            }
        });

        //点击空白处退出键盘
        findViewById(R.id.forgotPwdBackView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        //限制手机号长度
        TextChangedListener textChangedListener = new TextChangedListener(11, phone, ForgotPasswordActivity.this, TextString.PhoneLong);
        phone.addTextChangedListener(textChangedListener.getmTextWatcher());

    }


    public void back(View view) {
        finish();
    }

    public void Bullet(View view) {
        //showMessageDialog();
        Intent intent = new Intent(ForgotPasswordActivity.this, FeedBackActivity.class);
        intent.putExtra("page", "ForgotPasswordActivity");
        startActivity(intent);
    }

    //联系客服弹窗
    private void showMessageDialog() {
        Dialog bottomDialog = new Dialog(ForgotPasswordActivity.this, R.style.dialog);
        View contentView = LayoutInflater.from(ForgotPasswordActivity.this).inflate(R.layout.layout_perfect_information_dialog, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        //设置左右间距
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(ForgotPasswordActivity.this, 100f);
        params.bottomMargin = DensityUtil.dp2px(ForgotPasswordActivity.this, 0f);
        TextView lblMessage = contentView.findViewById(R.id.perfect_mes);
        lblMessage.setText("提示");
        TextView lblTitle = contentView.findViewById(R.id.lease_immediately_bt);
        lblTitle.setText("发送邮件");
        TextView contentTitle = contentView.findViewById(R.id.user_info_content);
        contentTitle.setText("发送邮件至邮箱zhixin@126.com与客服取得联系");
        contentView.findViewById(R.id.lease_immediately_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomDialog.cancel();
//                finish();
            }
        });
        contentView.findViewById(R.id.lease_immediately_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ForgotPasswordActivity.this, "已发送邮件", Toast.LENGTH_SHORT).show();
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();

    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (count == 0) {
                count = 60;
                forgot_bt.setText("重新发送");
                forgot_bt.setClickable(true);
                return;
            }
            count--;
            forgot_bt.setText(count + "s后重新发送");
            sendEmptyMessageDelayed(COUNT_TIME, 1000);
        }

        ;
    };

    public void clickButton(View view) {
        handler.sendEmptyMessage(COUNT_TIME);
        forgot_bt.setClickable(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}
