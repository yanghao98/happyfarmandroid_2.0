package zhixin.cn.com.happyfarm_user;

import android.app.slice.Slice;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AreaAddress;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.LuckDrawAddress;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.TextString;

public class SelectBranchActivity extends ABaseActivity {
    private Button button;
    private EditText address;
    private String[] strings_district;
    private String[] strings_street;
    private ArrayAdapter<String> adapter_district;
    private ArrayAdapter<String> adapter_street;
    private Spinner spinner_district;
    private EditText street;
    private List<AreaAddress.AreaAddressBean> list;
    private List<LuckDrawAddress.DistrictInfo.StreetInfo> streetInfoslist;
    private int district_id ;
    private String TAG = "SelectBranchActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_branch_activity);
        initData();
        initView();
    }

    private void initView() {
        TextView titleName = findViewById(R.id.title_name);
        titleName.setText("地址填写");
        spinner_district = findViewById(R.id.select_branch_district_spin);
        street = findViewById(R.id.select_branch_street);
        address = findViewById(R.id.select_branch_address);
        button = findViewById(R.id.select_branch_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( "".equals(street.getText().toString())) {
                    Toast.makeText(SelectBranchActivity.this,"请填写街道名",Toast.LENGTH_SHORT).show();
                }else if ("".equals(address.getText().toString()) ){
                    Toast.makeText(SelectBranchActivity.this,"请填写门牌号或小区名",Toast.LENGTH_SHORT).show();
                } else {
                    final AlertUtilBest diyDialog = new AlertUtilBest(view.getContext());
                    diyDialog.setCancel("取消")
                            .setOk(TextString.selectBranchOkButton)
                            .setContent(TextString.selectBranchContentText)
                            .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                                @Override
                                public void cancel() {
                                    diyDialog.cancel();
                                }
                                @Override
                                public void ok() {
                                    diyDialog.cancel();
                                    addLuckDrawCollectAddress(district_id,street.getText().toString(),address.getText().toString());
                                }
                            });
                    diyDialog.builder();
                    diyDialog.show();

//                    Toast.makeText(SelectBranchActivity.this,"区Id为："+district_id+"街道Id为："+street_id+"详细街道信息为："+address.getText().toString(),Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void initData() {
        HttpHelper.initHttpHelper().searchAreaAddress().enqueue(new Callback<AreaAddress>() {
            @Override
            public void onResponse(Call<AreaAddress> call, Response<AreaAddress> response) {
                Log.i(TAG, "收货地址: "+ JSON.toJSONString(response.body()));
                List<String> strings = new ArrayList<String>();
                if ("success".equals(response.body().getFlag())) {
                    for (int i = 0;i < response.body().getResult().size(); i++) {
                        strings.add(response.body().getResult().get(i).getAwardRegion());
//                        areaAddressData[i] = response.body().getResult().get(i).getAwardRegion();
                        strings_district = (String[]) strings.toArray(new String[0]);
                        list = response.body().getResult();
                    }
                    System.out.println(JSON.toJSONString(strings_district));
                    initSpinner();
                }
            }
            @Override
            public void onFailure(Call<AreaAddress> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t );
            }
        });
    }

    public void initSpinner () {
        adapter_district = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, strings_district);
        adapter_district.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_district.setAdapter(adapter_district);
        //选择监听
        spinner_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //parent就是父控件spinner
            //view就是spinner内填充的textview,id=@android:id/text1
            //position是值所在数组的位置
            //id是值所在行的位置，一般来说与positin一致
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {
//                List<String> strings1 = new ArrayList<>();
                for (int i = 0; i < list.size();i++) {
                    if (pos == i) {
                        district_id = list.get(pos).getId();
//                        streetInfoslist = list.get(pos).getChild();
//                        Toast.makeText(SelectBranchActivity.this,"+"+district_id,Toast.LENGTH_SHORT).show();
//                        for (int a = 0;a < list.get(pos).getChild().size();a++) {
//                            strings1.add(list.get(pos).getChild().get(a).getRegion());
//                        }
//                        strings_street = strings1.toArray(new String[0]);
                    }
                }
//                spinner_street.setVisibility(View.VISIBLE);
//                initSpinnerStreet();
                //设置spinner内的填充文字居中
                //((TextView)view).setGravity(Gravity.CENTER);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
            }
        });
    }
//    public void initSpinnerStreet() {
//        adapter_street = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, strings_street);
//        adapter_street.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner_street.setAdapter(adapter_street);
//        //选择监听
//        spinner_street.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            //parent就是父控件spinner
//            //view就是spinner内填充的textview,id=@android:id/text1
//            //position是值所在数组的位置
//            //id是值所在行的位置，一般来说与positin一致
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {
//                for (int i = 0;i < streetInfoslist.size();i++) {
//                    if (i == pos) {
//                        street_id = streetInfoslist.get(pos).getId();
////                        Toast.makeText(SelectBranchActivity.this,streetInfoslist.get(pos).getRegion()+"+"+street_id,Toast.LENGTH_SHORT).show();
//                    }
//                }
//                //设置spinner内的填充文字居中
//                //((TextView)view).setGravity(Gravity.CENTER);
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // Another interface callback
//            }
//        });
//    }

    /**
     * 抽奖前添加用户地址信息
     * @param
     */
    public void addLuckDrawCollectAddress( int luckDrawAddressId ,String luckDrawStreet,String detailedAddress) {
        System.out.println(luckDrawStreet+detailedAddress);
        HttpHelper.initHttpHelper().addLuckDrawCollectAddress(luckDrawAddressId,luckDrawStreet+detailedAddress).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                System.out.println(JSON.toJSONString(response.body()));
                Toast.makeText(SelectBranchActivity.this,response.body().getResult(),Toast.LENGTH_SHORT).show();
                if ("success".equals(response.body().getFlag())) {
                    Intent intent = new Intent(SelectBranchActivity.this,MainActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(SelectBranchActivity.this,getResources().getString(R.string.no_network),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void back(View view) {
            finish();
    }
    /**
     * 重写返回键
     * 监听Back键按下事件
     * super.onBackPressed()会自动调用finish()方法,关闭当前Activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
