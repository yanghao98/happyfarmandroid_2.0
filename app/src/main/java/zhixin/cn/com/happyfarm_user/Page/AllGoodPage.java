package zhixin.cn.com.happyfarm_user.Page;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.baidu.platform.comapi.map.E;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import android.view.animation.Animation.AnimationListener;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.AllGoodAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.FilterAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.LabelBean;
import zhixin.cn.com.happyfarm_user.DetailsOfVegetablesActivity;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.MyImageView;
import zhixin.cn.com.happyfarm_user.TheMarketActivtiy;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.ConfirmOrderActivity;
import zhixin.cn.com.happyfarm_user.EventBus.ConfirmOrder;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AllGoods;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.SearchAllGoods;
import zhixin.cn.com.happyfarm_user.model.SearchCropList;
import zhixin.cn.com.happyfarm_user.model.ShoppingCartNum;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Administrator on 2018/5/14.
 */

public class AllGoodPage extends Fragment {

    private RecyclerView recyclerView;
    private FilterAdapter filterAdapter;
    private List<LabelBean> labelBeanData;
    //    private TextView textView;
    private DrawerLayout drawerLayout;
    private Button screenBtn;
    private Button Reset;
    private Button complete;
    private String TAG = "AllGoodPage->";
    private View view;
    private RelativeLayout shelf_time;
    private RelativeLayout price;
    private int gray = Color.parseColor("#999999");
    private int green = Color.parseColor("#1DAC6D");
    private ArrayList<AllGoods> list = new ArrayList<>();
    private ArrayList<AllGoods> list111 = new ArrayList<>();
    private RecyclerView recyclerlist;
    private RelativeLayout myGoodPrompt;
    private TextView timeText;
    private TextView priceText;
    private LinearLayout price_select1;
    private LinearLayout price_select2;
    private LinearLayout time_select1;
    private LinearLayout time_select2;
    private int TIME_STATE = 1;
    private int PRICE_STATE = 1;
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private GridLayoutManager gridLayoutManager;
    private AllGoodAdapter adapter;
    private int auto;
    private RefreshLayout refreshLayout;
    private int sortId;
    private int sortDay;
    private int sortNum;
    private String cropName = "";
    private ProgressBar allGoodProgress;
    private Badge messageBadgeShoppingCart;
    private int messageBadgeNum = 0;
    private Dialog mDialog;
    //动画时间
    private int AnimationDuration = 1000;
    //正在执行的动画数量
    private int number = 0;
    //是否完成清理
    private boolean isClean = false;
    private FrameLayout animation_viewGroup;
    private Handler myHandler = new Handler(){
        public void handleMessage(Message msg){
            switch(msg.what){
                case 0:
                    //用来清除动画后留下的垃圾
                    try{
                        animation_viewGroup.removeAllViews();
                    }catch(Exception e){
                    }
                    isClean = false;
                    break;
                default:
                    break;
            }
        }
    };
    private int dishCategoryId = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.all_good_page, null, false);
        Intent intent = getActivity().getIntent();
        dishCategoryId = intent.getIntExtra("dishCategoryId",0);
        initView(view);
        allGoodScreenBtn();
        //ShelfTtime();
        //Price();
        initRecyclerView();
        pageNum = 1;
        pageSize = 15;
        if (!list.isEmpty()) {
            list.clear();
        }

        auto = 1;
        sortId = 1;
        CommodityInformation(sortId, 1, sortDay, sortNum, cropName,dishCategoryId);
        time_select2.setVisibility(View.VISIBLE);
        TIME_STATE = 2;
        AutoLoading();
        topRefreshLayout();
        bottomRefreshLayout();
        initData();//筛选内菜名等数据加载初始化
        screeningEvent();//筛选内确认与清除点击事件
        searchShoppingCartNum();
        return view;
    }

    /**
     * 初始化頁面佈局
     * @param view view為上文中的layout 在碎片中于需要拿到對應的view試圖才能找到元素
     */
    private void initView(View view) {
        screenBtn = view.findViewById(R.id.all_good_screen_btn);
        recyclerView = view.findViewById(R.id.recycler);
        allGoodProgress = view.findViewById(R.id.all_good_progress_bar);
        Reset = view.findViewById(R.id.Reset);
        complete = view.findViewById(R.id.complete);
        drawerLayout = view.findViewById(R.id.drawer_layouts);
        shelf_time = view.findViewById(R.id.shelf_time);
        price = view.findViewById(R.id.price);
        myGoodPrompt = view.findViewById(R.id.my_goods_prompt);
        timeText = view.findViewById(R.id.time_text);
        priceText = view.findViewById(R.id.price_text);
        price_select1 = view.findViewById(R.id.price_select1);
        price_select2 = view.findViewById(R.id.price_select2);
        time_select1 = view.findViewById(R.id.time_select1);
        time_select2 = view.findViewById(R.id.time_select2);
        refreshLayout = view.findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
//        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()).setShowBezierWave(false));
        //设置 Footer 为 球脉冲 样式
        refreshLayout.setRefreshFooter(new ClassicsFooter(getContext()).setSpinnerStyle(SpinnerStyle.Scale));


//        //模拟数据
//        for (int i = 0; i < 15; i++) {
//            AllGoods dataBean = new AllGoods();
//            dataBean.cropId = i;
//            dataBean.sellerId = i+1;
//            dataBean.cropImg = "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1252808148,3420157609&fm=26&gp=0.jpg";
//            dataBean.cropNames = "蔬菜";
////            dataBean.shelfTime = DateUtil.getStrTime(String.valueOf(listBean.getStartTime()));
////                        dataBean.isOrganic = response.body().getResult().getList().get(i).getIsOrganic();
//            dataBean.isOrganic = "(有机肥)";
//            dataBean.nums = "20";
//            dataBean.sellerNickname = "hello";
//            dataBean.prices = "10";
//            list111.add(dataBean);
//        }


        messageBadgeShoppingCart = new QBadgeView(getContext())
                .bindTarget(getActivity().findViewById(R.id.shopping_message_view_total))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);

    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)){//加上判断
            EventBus.getDefault().register(this);
        }
        ShelfTime();
        Price();
    }

    @Override
    public void onResume() {
        super.onResume();
        searchShoppingCartNum();
        if (Config.allGoodPageRefresh){
            Config.allGoodPageRefresh = false;
            if (!list.isEmpty())
                list.clear();
            CommodityInformation(sortId, 1, sortDay, sortNum, cropName,dishCategoryId);
        }
    }

    /**
     * 事件响应方法
     * 接收消息
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ConfirmOrder event) {

        String msg = event.getRefreshGood();
        if ("RefreshGood".equals(msg) ){
            if (!list.isEmpty()) {
                list.clear();
            }
            adapter.notifyDataSetChanged();
            sortId = 1;
            Log.i(TAG, "onClick: " + sortDay + "，" + sortId + "，" + sortNum + "，" + cropName);
            CommodityInformation(sortId, 1, sortDay, sortNum, cropName,dishCategoryId);
        }
    }

    /**
     * 筛选按钮点击后弹框内确认与清除点击事件
     */
    private void screeningEvent(){
        //TODO 清除点击事件
        Reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterAdapter.clearCheck();
            }
        });
        //TODO 筛选确认事件
        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                String crop = "";
                String time = "";
                String number = "";
                try {
                    crop = filterAdapter.getCheckLabel().get("商品种类").getLabel();
                    cropName = crop;
                } catch (NullPointerException e) {
                    Log.i(TAG, "onClick: " + cropName);
                }
                try {
                    time = filterAdapter.getCheckLabel().get("上架时间").getLabel();
                    switch (time) {
                        case "一天以内":
                            sortDay = 1;
                            break;
                        case "三天以内":
                            sortDay = 2;
                            break;
                        default:
                            sortDay = 3;
                            break;
                    }
                } catch (NullPointerException e) {
                    Log.i(TAG, "onClick: " + sortDay);
                }
                try {
                    number = filterAdapter.getCheckLabel().get("买卖份数").getLabel();
                    switch (number) {
                        case "5份以下":
                            sortNum = 1;
                            break;
                        case "5到10份":
                            sortNum = 2;
                            break;
                        default:
                            sortNum = 3;
                            break;
                    }
                } catch (NullPointerException e) {
                    Log.i(TAG, "onClick: " + sortDay);
                }
                if (crop.equals("") && time.equals("") && number.equals("")) {
                    sortDay = 0;
                    sortNum = 0;
                    cropName = "";
                    sortId = 1;
                    if (!list.isEmpty()) {
                        list.clear();
                    }
                    CommodityInformation(sortId, 1, sortDay, sortNum, cropName,dishCategoryId);
                } else {
                    if (!list.isEmpty()) {
                        list.clear();
                    }
                    sortId = 1;
                    Log.i(TAG, "onClick: " + sortDay + "，" + sortId + "，" + sortNum + "，" + cropName);
                    CommodityInformation(sortId, 1, sortDay, sortNum, cropName,dishCategoryId);
                }
            }
        });
    }

    //TODO 上架时间点击事件
    private void ShelfTime() {
        shelf_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!list.isEmpty()) {
                    list.clear();
                }
                adapter.notifyDataSetChanged();
                timeText.setTextColor(green);
                timeText.setTextSize(14);
                priceText.setTextColor(gray);
                priceText.setTextSize(12);

                if (TIME_STATE == 2) {
                    sortId = 2;
                    CommodityInformation(sortId, 1, sortDay, sortNum, cropName,dishCategoryId);
                    time_select1.setVisibility(View.VISIBLE);
                    time_select2.setVisibility(View.GONE);
                    price_select1.setVisibility(View.GONE);
                    price_select2.setVisibility(View.GONE);
                    auto = 2;
                    pageNum = 1;
                    TIME_STATE = 1;
                } else {
                    sortId = 1;
                    CommodityInformation(sortId, 1, sortDay, sortNum, cropName,dishCategoryId);
                    time_select1.setVisibility(View.GONE);
                    time_select2.setVisibility(View.VISIBLE);
                    price_select1.setVisibility(View.GONE);
                    price_select2.setVisibility(View.GONE);
                    auto = 1;
                    pageNum = 1;
                    TIME_STATE = 2;
                }
            }
        });
    }

    /**
     * 价格点击事件
     */
    private void Price() {
        price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!list.isEmpty()) {
                    list.clear();
                }
                adapter.notifyDataSetChanged();
                priceText.setTextColor(green);
                priceText.setTextSize(14);
                timeText.setTextColor(gray);
                timeText.setTextSize(12);
                if (PRICE_STATE == 2) {
                    sortId = 4;
                    CommodityInformation(sortId, 1, sortDay, sortNum, cropName,dishCategoryId);
                    price_select2.setVisibility(View.GONE);
                    price_select1.setVisibility(View.VISIBLE);
                    time_select1.setVisibility(View.GONE);
                    time_select2.setVisibility(View.GONE);
                    auto = 4;
                    pageNum = 1;
//                    AutoLoading();
                    PRICE_STATE = 1;
                } else {
                    sortId = 3;
                    CommodityInformation(sortId, 1, sortDay, sortNum, cropName,dishCategoryId);
                    price_select2.setVisibility(View.VISIBLE);
                    price_select1.setVisibility(View.GONE);
                    time_select1.setVisibility(View.GONE);
                    time_select2.setVisibility(View.GONE);
                    auto = 3;
                    pageNum = 1;
//                    AutoLoading();
                    PRICE_STATE = 2;
                }
            }
        });
    }

    //TODO 筛选点击事件
    public void allGoodScreenBtn() {
        screenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getContext(), SecondActivity.class));
                if (drawerLayout.isDrawerOpen(Gravity.END)) {
                    Log.i(TAG, "onClick: "+"关闭");
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(Gravity.END);
                }
            }
        });
    }

    private void initData() {
        String name = "";
        HttpHelper.initHttpHelper().searchCropList(name, null).enqueue(new Callback<SearchCropList>() {
            @Override
            public void onResponse(Call<SearchCropList> call, Response<SearchCropList> response) {
                Log.e(TAG, JSON.toJSONString(response.body()));
                try {
                    labelBeanData = new ArrayList<>();
                    labelBeanData.add(new LabelBean("商品种类", LabelBean.HEADER));
                    List<SearchCropList.ResultBean.ListBean> resultBeans = response.body().getResult().getList();
                    if (!resultBeans.isEmpty()) {
                        for (SearchCropList.ResultBean.ListBean resultBean : resultBeans) {
                            Log.e(TAG, resultBean.getCropName());
                            labelBeanData.add(new LabelBean(resultBean.getCropName(), "商品种类"));
                        }
                    } else {
                        labelBeanData.add(new LabelBean("暂无商品", "商品种类"));
                    }
                    labelBeanData.add(new LabelBean("上架时间", LabelBean.HEADER));
                    labelBeanData.add(new LabelBean("一天以内", "上架时间"));
                    labelBeanData.add(new LabelBean("三天以内", "上架时间"));
                    labelBeanData.add(new LabelBean("七天以内", "上架时间"));
                    labelBeanData.add(new LabelBean("买卖份数", LabelBean.HEADER));
                    labelBeanData.add(new LabelBean("5份以下", "买卖份数"));
                    labelBeanData.add(new LabelBean("5到10份", "买卖份数"));
                    labelBeanData.add(new LabelBean("10份以上", "买卖份数"));
                    recyclerView.setAdapter(filterAdapter = new FilterAdapter(getContext(), labelBeanData));
                    GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
                    layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int i) {
                            if (labelBeanData.get(i).getType() == LabelBean.HEADER)
                                return 3;//如果为header，把三格全占满
                            else
                                return 1;
                        }
                    });
                    recyclerView.setLayoutManager(layoutManager);
                    filterAdapter.setOnCheckListener(new FilterAdapter.OnCheckListener() {
                        @Override
                        public void onChargeCheck(LabelBean item) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SearchCropList> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        drawerLayout.closeDrawers();//TODO 关闭筛选
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))//加上判断
            EventBus.getDefault().unregister(this);
    }

    //TODO 获取全部商品信息
    private void CommodityInformation(int sortId, int pageNum, int sortDay, int sortNum, String cropName,int dishCategoryId) {
        HttpHelper.initHttpHelper().searchAllGoods(sortId, pageNum, pageSize, sortDay, sortNum, cropName,dishCategoryId).enqueue(new Callback<SearchAllGoods>() {
            @Override
            public void onResponse(Call<SearchAllGoods> call, Response<SearchAllGoods> response) {
//                Log.e("AllGoodPage-->", JSON.toJSONString(response.body()));
                allGoodProgress.setVisibility(View.VISIBLE);
                if ("success".equals(response.body().getFlag())) {
//                    Log.e(TAG,JSON.toJSONString(response.body()));
                    isLastPage = response.body().getResult().isIsLastPage();
                    if (response.body().getResult().getList().isEmpty()) {
                        allGoodProgress.setVisibility(View.GONE);
                        myGoodPrompt.setVisibility(View.VISIBLE);
                        recyclerlist.setVisibility(View.GONE);
                    } else {
                        allGoodProgress.setVisibility(View.GONE);
                        for (int i = 0; i < response.body().getResult().getList().size(); i++) {
                            SearchAllGoods.ResultBean.ListBean listBean = response.body().getResult().getList().get(i);
                            AllGoods dataBean = new AllGoods();
                            dataBean.cropId = listBean.getId();
                            dataBean.sellerId = listBean.getSellerId();
                            dataBean.cropImg = listBean.getCropImg();
                            dataBean.cropNames = listBean.getCropName();
                            dataBean.shelfTime = DateUtil.getStrTime(String.valueOf(listBean.getStartTime()));
//                        dataBean.isOrganic = response.body().getResult().getList().get(i).getIsOrganic();
                            if (listBean.getIsOrganic() == 1) {
                                dataBean.isOrganic = "(有机肥)";
                            } else if (listBean.getIsOrganic() == 2) {
                                dataBean.isOrganic = "(无机肥)";
                            } else {
                                dataBean.isOrganic = "";
                            }
                            dataBean.nums = String.valueOf(listBean.getNum());
                            dataBean.sellerNickname = listBean.getSellerNickname();
                            dataBean.prices = String.valueOf(listBean.getPrice());
                            list.add(dataBean);
                        }
                        myGoodPrompt.setVisibility(View.GONE);
                        recyclerlist.setVisibility(View.VISIBLE);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    allGoodProgress.setVisibility(View.GONE);
                    myGoodPrompt.setVisibility(View.VISIBLE);
                    recyclerlist.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<SearchAllGoods> call, Throwable t) {
                allGoodProgress.setVisibility(View.GONE);
                myGoodPrompt.setVisibility(View.VISIBLE);
                recyclerlist.setVisibility(View.GONE);
            }
        });
    }

    private void initRecyclerView() {
        int spanCount = 1;
        int spacing = 50;
        boolean includeEdge = true;
        recyclerlist = view.findViewById(R.id.all_good_page_recycler);
        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new AllGoodAdapter(getContext(), list);
        //设置适配器
        recyclerlist.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerlist.setLayoutManager(gridLayoutManager);

        //TODO 设置一行一个 ，左右间距 50
//        collection_recycle.addItemDecoration(new GridSpacingItemDecoration(spanCount,spacing,includeEdge));
        //item点击事件
        adapter.setItemClickListener(new AllGoodAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int positions) {
                Log.e(TAG, "onItemClick: "+positions );
                Intent intent = new Intent(getContext(), DetailsOfVegetablesActivity.class);
                intent.putExtra("corpId",list.get(positions).cropId);
                startActivity(intent);
            }
        });
        //button点击事件

        animation_viewGroup = createAnimLayout();
        adapter.buttonSetOnclick(new AllGoodAdapter.ButtonInterface() {
            @Override
            public void onclick(View view, int position) {
                String token = getActivity().getSharedPreferences("User_data",MODE_PRIVATE).getString("token","");
                if ("".equals(token)){
                    noLoginDialog();
                    return;
                }

//                HttpHelper.initHttpHelper().test().enqueue(new Callback<Test>() {
//                    @Override
//                    public void onResponse(Call<Test> call, Response<Test> responses) {
//                        if (("success").equals(responses.body().getFlag())) {
//                            interceptDialog(position);
//                        } else {
//                            noLoginDialog();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<Test> call, Throwable t) {
//                        Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
//                    }
//                });

            }
        });

        adapter.SetOnSetHolderClickListener(new AllGoodAdapter.HolderClickListener() {
            @Override
            public void onHolderClick(Drawable drawable, int[] start_location,AllGoods goods) {
                HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
                    @Override
                    public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                        if ("success".equals(response.body().getFlag())) {
                            addShoppingCart(drawable,start_location,goods.cropId);
                        } else {
                            noLoginDialog();
                        }
                    }
                    @Override
                    public void onFailure(Call<GetSelfInfo> call, Throwable t) {
                        noLoginDialog();
                    }
                });

            }
        });

    }

    /**
     * 添加购物车
     */
    private void addShoppingCart(Drawable drawable,int[] start_location,Integer goodsId) {
        Log.i("", "addShoppingCart: "+goodsId+",");
        mDialog = LoadingDialog.createLoadingDialog(getContext(), "添加中...");
        HttpHelper.initHttpHelper().addShoppingCart(goodsId).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if ("success".equals(response.body().getFlag())) {
                    doAnim(drawable,start_location);
                    messageBadgeNum++;
                    messageBadgeShoppingCart.setBadgeNumber(messageBadgeNum);
                    Toast.makeText(getContext(),"加入购物车成功",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(),response.body().getResult(),Toast.LENGTH_SHORT).show();
                }
                LoadingDialog.closeDialog(mDialog);
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(getContext(),"加入购物车失败",Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(mDialog);

            }
        });
    }


    /**
     * 获取购物车数量
     */
    public void searchShoppingCartNum() {
        HttpHelper.initHttpHelper().searchShoppingCartNum().enqueue(new Callback<ShoppingCartNum>() {
            @Override
            public void onResponse(Call<ShoppingCartNum> call, Response<ShoppingCartNum> response) {
                System.out.println("购物车数量"+JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    messageBadgeNum = response.body().getResult();
                    messageBadgeShoppingCart.setBadgeNumber(response.body().getResult());
                }
            }
            @Override
            public void onFailure(Call<ShoppingCartNum> call, Throwable t) {

            }
        });
    }

    /**
     * 从此开始制作动画
     * @param drawable
     * @param start_location
     */
    private void doAnim(Drawable drawable,int[] start_location){
        if(!isClean){
            setAnim(drawable,start_location);
        }else{
            try{
                animation_viewGroup.removeAllViews();
                isClean = false;
                setAnim(drawable,start_location);
            }catch(Exception e){
                e.printStackTrace();
            }
            finally{
                isClean = true;
            }
        }
    }

    /**
     * @Description: 创建动画层
     * @param
     * @return void
     * @throws
     */
    private FrameLayout createAnimLayout(){
        ViewGroup rootView = (ViewGroup)getActivity().getWindow().getDecorView();
        FrameLayout animLayout = new FrameLayout(getContext());
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,FrameLayout.LayoutParams.MATCH_PARENT);
        animLayout.setLayoutParams(lp);
        animLayout.setBackgroundResource(android.R.color.transparent);
        rootView.addView(animLayout);
        return animLayout;

    }

    /**
     * @deprecated 将要执行动画的view 添加到动画层
     * @param vg
     *        动画运行的层 这里是frameLayout
     * @param view
     *        要运行动画的View
     * @param location
     *        动画的起始位置
     * @return
     */
    private View addViewToAnimLayout(ViewGroup vg,View view,int[] location){
        int x = location[0];
        int y = location[1];
        vg.addView(view);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                dip2px(getContext(),90),dip2px(getContext(),90));
        lp.leftMargin = x;
        lp.topMargin = y;
        view.setPadding(5, 5, 5, 5);
        view.setLayoutParams(lp);

        return view;
    }
    /**
     * dip，dp转化成px 用来处理不同分辨路的屏幕
     * @param context
     * @param dpValue
     * @return
     */
    private int dip2px(Context context, float dpValue){
        float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dpValue*scale +0.5f);
    }

    /**
     * 动画效果设置
     * @param drawable
     *       将要加入购物车的商品
     * @param start_location
     *        起始位置
     */
    private void setAnim(Drawable drawable,int[] start_location){


        Animation mScaleAnimation = new ScaleAnimation(1.5f,0.0f,1.5f,0.0f,Animation.RELATIVE_TO_SELF,0.1f,Animation.RELATIVE_TO_SELF,0.1f);
        mScaleAnimation.setDuration(AnimationDuration);
        mScaleAnimation.setFillAfter(true);


        final ImageView iview = new ImageView(getContext());
        iview.setImageDrawable(drawable);
        final View view = addViewToAnimLayout(animation_viewGroup,iview,start_location);
        view.setAlpha(0.6f);

        int[] end_location = new int[2];
        //结束点控件位置
        getActivity().findViewById(R.id.shopping_cart).getLocationInWindow(end_location);
        int endX = end_location[0];
        int endY = end_location[1]-start_location[1];

        Animation mTranslateAnimation = new TranslateAnimation(0,endX,0,endY);
        Animation mRotateAnimation = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mRotateAnimation.setDuration(AnimationDuration);
        mTranslateAnimation.setDuration(AnimationDuration);
        AnimationSet mAnimationSet = new AnimationSet(true);

        mAnimationSet.setFillAfter(true);
        mAnimationSet.addAnimation(mRotateAnimation);
        mAnimationSet.addAnimation(mScaleAnimation);
        mAnimationSet.addAnimation(mTranslateAnimation);

        mAnimationSet.setAnimationListener(new AnimationListener(){


            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub
                number++;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub

                number--;
                if(number==0){
                    isClean = true;
                    myHandler.sendEmptyMessage(0);
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

        });
        view.startAnimation(mAnimationSet);

    }


    /**
     * 内存过低时及时处理动画产生的未处理冗余
     */
    @Override
    public void onLowMemory() {
        // TODO Auto-generated method stub
        isClean = true;
        try{
            animation_viewGroup.removeAllViews();
        }catch(Exception e){
            e.printStackTrace();
        }
        isClean = false;
        super.onLowMemory();
    }
    //动画制作完成


    /**
     * 租地拦截弹框
     * 由于业务限制，需要租地时告诉用户目前只对南京开放
     */
    private void interceptDialog(int position){
        final AlertUtilBest diyDialog = new AlertUtilBest(getActivity());
        diyDialog.setCancel("取消")
                .setOk(getResources().getString(R.string.intercept_ok))
                .setContent(getResources().getString(R.string.intercept_good_content))
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(getActivity(), ConfirmOrderActivity.class);
                        Log.e("11223344", list.get(position).cropId + "");
                        intent.putExtra("CorpId", list.get(position).cropId + "");
                        intent.putExtra("imgUrl", list.get(position).cropImg);
                        intent.putExtra("cropName", list.get(position).cropNames);
                        intent.putExtra("Num", list.get(position).nums + "");
                        intent.putExtra("isOrganic", list.get(position).isOrganic);
                        intent.putExtra("sellerNickname", list.get(position).sellerNickname);
                        intent.putExtra("prices", list.get(position).prices + "");
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog =new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        intent.putExtra("isAllGoodPage","isAllGoodPage");
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    public void AutoLoading() {
        //上拉滑动自动请求数据
        recyclerlist.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                    if (lastVisiblePosition >= gridLayoutManager.getItemCount() - 1) {
                        if (isLastPage) {
//                            Toast.makeText(getApplication(), "没有更多数据", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            CommodityInformation(auto, pageNum, sortDay, sortNum, cropName,dishCategoryId);
                        }
                    }
                }
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!list.isEmpty()) {
                    list.clear();
                }
                isLastPage = false;
                pageNum = 1;
                refreshLayout.finishLoadMoreWithNoMoreData();////TODO 将不会再次触发加载更多事件
                CommodityInformation(auto, 1, sortDay, sortNum, cropName,dishCategoryId);
                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }
    //上拉加载
    public void bottomRefreshLayout() {
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("info", "上拉刷新: "+isLastPage);
                        if (isLastPage) {
                            Toast.makeText(getContext(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();////TODO 将不会再次触发加载更多事件
                        } else {
                            pageNum++;
                            CommodityInformation(auto, pageNum, sortDay, sortNum, cropName,dishCategoryId);
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }

    public void back(View v) {
        getActivity().finish();
    }


}
