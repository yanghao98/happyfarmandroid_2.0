package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.http.SslError;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.SignInAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.SignIn;
import zhixin.cn.com.happyfarm_user.model.SignMonthRecord;
import zhixin.cn.com.happyfarm_user.model.SignRecord;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.MyWebChromeClient;
import zhixin.cn.com.happyfarm_user.utils.SignInDialog;
import zhixin.cn.com.happyfarm_user.utils.TextString;

public class SignInActivity extends AppCompatActivity {
    private WebView webView;
    private String tokenStr;
    private TextView signDay;
    private TextView totalAttendance;
    private TextView signInReward;
    private TextView signInCycle;
    private Dialog dialog;
    private Dialog myDialog;
    private SignInAdapter adapter;
    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private TextView signInButton;
    private TextView yesSignInButton;
    private int isLandlord;
    private String[] strings = {"一","二","三","四","五","六","七","八","九","十","十一","十二"};
    private SignInDialog signInDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_activity);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        initView();
        getUserInfo();
    }

    private void initView() {
        SharedPreferences pref = this.getSharedPreferences("User_data",MODE_PRIVATE);
        tokenStr = pref.getString("token","");
        signDay = findViewById(R.id.sign_day);
        webView = findViewById(R.id.webView_sign_in);
        totalAttendance = findViewById(R.id.total_attendance);
        signInReward = findViewById(R.id.sign_in_reward);
        signInCycle = findViewById(R.id.sign_in_cycle);
        webView.getSettings().setJavaScriptEnabled(true);//启用js
        /**************************关于图片不能显示暂时加的内容***************************/
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // TODO Auto-generated method stub
                // handler.cancel();// Android默认的处理方式
                handler.proceed();// 接受所有网站的证书
                // handleMessage(Message msg);// 进行其他处理
            }
        });
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(
                    WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }
        webView.getSettings().setBlockNetworkImage(false);//解决图片不显示
        webView.clearCache(true); //加载的之前调用即可
        /**************************关于图片不能显示暂时加的内容***************************/
        if ("".equals(tokenStr)) {
            noLoginDialog();
        }
        signInButton = findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInClick();
            }
        });
        yesSignInButton = findViewById(R.id.yes_sign_in_button);
        recyclerView = findViewById(R.id.sign_in_recycle);
    }

    /**
     *  查询月签到状态
     */
    public void searchSignMonthRecord() {
        dialog = LoadingDialog.createLoadingDialog(SignInActivity.this, "正在加载数据");
        HttpHelper.initHttpHelper().searchSignMonthRecord().enqueue(new Callback<SignMonthRecord>() {
            @Override
            public void onResponse(Call<SignMonthRecord> call, Response<SignMonthRecord> response) {
                Log.i("SignDate", "onResponse: "+ JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    SignMonthRecord.ResultBean resultBean = response.body().getResult();
                    signDay.setText("连续签到："+resultBean.getSignDay()+"天");
                    totalAttendance.setText("您是今天第"+resultBean.getTotalAttendance()+"位签到用户");
                    signInCycle.setText("第"+strings[resultBean.getCycle() -1]+"周期");
                    signInReward.setText(resultBean.getReward());
                    webView.loadDataWithBaseURL(null,resultBean.getSignInRules(),"text/html","UTF-8",null);
//                webView.loadDataWithBaseURL(null,obj,"text/html","UTF-8",null);
                    //开启缓存
                    webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                    //判断页面加载过程
                    webView.setWebChromeClient(new MyWebChromeClient() {
                        @Override
                        public void onProgressChanged(WebView view, int newProgress) {
                            // TODO Auto-generated method stub
//                            if (newProgress == 100) {
//                                // 网页加载完成
//                                progressBar.setVisibility(View.GONE);//加载完网页进度条消失
//                            } else {
//                                // 加载中
//                                progressBar.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
//                                progressBar.setProgress(newProgress);//设置进度值
//                            }
                        }

                    });
                    List<SignRecord> list = new ArrayList<>();
                    if (1 == isLandlord) {
                        if (1 == resultBean.getCycle()) {
                            for (int i = 0; i < 28; i++) {
                                if (i < resultBean.getWhatDay()) {
                                    if (i == 6) {
                                        list.add(new SignRecord(1 + i, 3));//第一周期 第七天鸡窝搭好了 已签到
                                    } else if (i == 27) {
                                        list.add(new SignRecord(1 + i, 5));//第一周期 鸡蛋孵化 已签到
                                    } else {
                                        list.add(new SignRecord(1 + i, 1));//已签到
                                    }
                                } else {
                                    if (i == 6) {
                                        list.add(new SignRecord(1 + i, 4));//第一周期 第七天鸡窝图片 未签到
                                    } else if (i == 27) {
                                        list.add(new SignRecord(1 + i, 6));//第一周期 鸡蛋孵化图片 未签到
                                    } else {
                                        list.add(new SignRecord(1 + i, 2));//未签到
                                    }
                                }
                            }
                        }

                        if (2 <= resultBean.getCycle() && resultBean.getCycle() <= 11) {
                            for (int i = 0; i < 28; i++) {
                                if (i < resultBean.getWhatDay()) {
                                    if (i == 27) {
                                        list.add(new SignRecord(1 + i, 7));//第二~十一周期 饲料图片 已签到
                                    } else {
                                        list.add(new SignRecord(1 + i, 1));//已签到

                                    }
                                } else {
                                    if (i == 27) {
                                        list.add(new SignRecord(1 + i, 8));//第二~十一周期 饲料图片 未签到
                                    } else {
                                        list.add(new SignRecord(1 + i, 2));//未签到
                                    }
                                }
                            }
                        }

                        if (12 == resultBean.getCycle()) {
                            for (int i = 0; i < 28; i++) {
                                if (i < resultBean.getWhatDay()) {
                                    if (i == 27) {
                                        list.add(new SignRecord(1 + i, 9));//第十二周期 成品鸡图片 已签到
                                    } else {
                                        list.add(new SignRecord(1 + i, 1));//已签到
                                    }
                                } else {
                                    if (i == 27) {
                                        list.add(new SignRecord(1 + i, 10));//第十二周期 成品鸡图片 未签到
                                    } else {
                                        list.add(new SignRecord(1 + i, 2));//未签到
                                    }
                                }
                            }
                        }
                    } else {
                        for (int i = 0; i < 28; i++) {
                            if (i < resultBean.getWhatDay()) {
                                list.add(new SignRecord(1 + i, 1));//已签到
                            } else {
                                list.add(new SignRecord(1 + i, 2));//未签到
                            }
                        }
                    }

//                    Log.i("参数", "onResponse: " + JSON.toJSONString(list));
                    adapter = new SignInAdapter(SignInActivity.this,list);
                    //设置适配器
                    recyclerView.setAdapter(adapter);
                    //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
                    gridLayoutManager = new GridLayoutManager(SignInActivity.this, 7);
                    //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
                    gridLayoutManager.setReverseLayout(false);
                    //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
                    recyclerView.setLayoutManager(gridLayoutManager);
                    adapter.notifyDataSetChanged();
                } else {

                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(Call<SignMonthRecord> call, Throwable t) {
                LoadingDialog.closeDialog(dialog);
            }
        });
    }

    /**
     * 签到点击事件
     */
    private void signInClick() {
        myDialog = LoadingDialog.createLoadingDialog(SignInActivity.this, "正在签到");
        HttpHelper.initHttpHelper().updateIsSign().enqueue(new Callback<SignIn>() {
            @Override
            public void onResponse(Call<SignIn> call, Response<SignIn> response) {
                if (null != response.body()){
                    if (("success").equals(response.body().getFlag())){
                        if (0 < response.body().getResult().getGiftList().size()) {
//                            Log.i("这里应该弹一个弹窗", "onResponse: ");
//                            signReward(response.body().getResult().getGiftList().get(0).getGitfName());
                            //卡片弹出框设置
                            signInDialog = new SignInDialog(SignInActivity.this,response.body().getResult().getSignGift());
                            signInDialog.setCanceledOnTouchOutside(true);
                            signInDialog.showDialog();
                        } else {
                            Toast toast = Toast.makeText(SignInActivity.this, "签到成功！植信蛋+"+response.body().getResult().getEggs()+"个", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                        getUserInfo();
                    }else {
                        int result = response.body().getErrCode();
                        if ( -103 == result) {
                            Toast.makeText(SignInActivity.this, "今日已签到！",Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignInActivity.this, "签到失败",Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {

                }
                LoadingDialog.closeDialog(myDialog);
            }

            @Override
            public void onFailure(Call<SignIn> call, Throwable t) {
                Toast.makeText(SignInActivity.this, TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(myDialog);
            }
        });
    }

    public void getUserInfo() {
        //TODO 获取用户信息 方便传值
        retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (("success").equals(response.body().getFlag())) {
                    isLandlord = response.body().getResult().getIsLandlord();
                    if (1 == response.body().getResult().getIsSign()) {
                        signInButton.setVisibility(View.GONE);
                        yesSignInButton.setVisibility(View.VISIBLE);
                        totalAttendance.setVisibility(View.VISIBLE);
                        signInReward.setVisibility(View.VISIBLE);
                    } else {
                        signInButton.setVisibility(View.VISIBLE);
                        yesSignInButton.setVisibility(View.GONE);
                        totalAttendance.setVisibility(View.GONE);
                        signInReward.setVisibility(View.GONE);
                    }
                    searchSignMonthRecord();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                //Toast.makeText(getContext(), "网络连接失败,请检查一下网络", Toast.LENGTH_SHORT).show();
                signInButton.setVisibility(View.VISIBLE);
                yesSignInButton.setVisibility(View.GONE);
            }
        });
    }
    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog = new AlertUtilBest(SignInActivity.this);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        startActivity(new Intent(SignInActivity.this,MainActivity.class));
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(SignInActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //在此删除上一个SignInActivity 在重新跳转一个SignInActivity.
        finish();
        startActivity(new Intent(this,SignInActivity.class));
    }

    public void back(View view) {
        finish();
    }
}
