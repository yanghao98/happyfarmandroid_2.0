package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AllGoods;
import zhixin.cn.com.happyfarm_user.model.AreaAddress;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.TextString;

public class PrizeExchangeActivity extends AppCompatActivity {

    private Spinner spinner;
    private List<AreaAddress.AreaAddressBean> beanList;
    private ArrayAdapter<String> adapter;
    private String[] areaAddressData ;
    private EditText phone ;
    private EditText username ;
    private EditText address ;
    private Button exchangeButton;
    private String okSpinnerText;
    private int okSpinnerId;
    private String TAG = "PrizeExchangeActivity";
    private int luckDrawRecordId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prize_exchange_activity);
        Intent intent = getIntent();
        luckDrawRecordId = intent.getIntExtra("luckDrawRecordId",0);
//        Toast.makeText(this,"我是从welf页面传过来的luckDrawRecordId："+ luckDrawRecordId,Toast.LENGTH_SHORT).show();
        initView();
        searchAreaAddress();
    }
    public void initView() {
        TextView titleName = findViewById(R.id.title_name);
        titleName.setText("兑换奖品");
        username = findViewById(R.id.prizeExchange_username);
        phone = findViewById(R.id.prizeExchange_tel);
        address = findViewById(R.id.prizeExchange_address);
        spinner = (Spinner) findViewById(R.id.prizeExchange_spin);
        exchangeButton =findViewById(R.id.prizeExchange_button);
        exchangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertUtilBest diyDialog = new AlertUtilBest(view.getContext());
                diyDialog.setCancel("取消")
                        .setOk(TextString.prizeExchangesOkButton)
                        .setContent(TextString.prizeExchangesContentText)
                        .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                            @Override
                            public void cancel() {
                                diyDialog.cancel();
                            }
                            @Override
                            public void ok() {
                                diyDialog.cancel();
                                if (judPhone()){
//                                    Toast.makeText(view.getContext(),"参数无误可以走接口了"+okSpinnerId+":"+luckDrawRecordId,Toast.LENGTH_SHORT).show();
                                    addReceivingDetailedAddress();
                                }
                            }
                        });
                diyDialog.builder();
                diyDialog.show();
            }
        });
    }
    public void initSpinner () {
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, areaAddressData);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        //选择监听
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //parent就是父控件spinner
            //view就是spinner内填充的textview,id=@android:id/text1
            //position是值所在数组的位置
            //id是值所在行的位置，一般来说与positin一致
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                okSpinnerText = areaAddressData[pos];
                for (int i = 0; i < beanList.size();i++) {
                    if (pos == i){
                        okSpinnerId = beanList.get(i).getId();
                    }
                }
                //设置spinner内的填充文字居中
                //((TextView)view).setGravity(Gravity.CENTER);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
            }
        });
    }

    /**
     * 查询区名字
     */

    public void searchAreaAddress() {

        HttpHelper.initHttpHelper().searchAreaAddress().enqueue(new Callback<AreaAddress>() {
            @Override
            public void onResponse(Call<AreaAddress> call, Response<AreaAddress> response) {
                Log.i(TAG, "onResponse: "+ JSON.toJSONString(response.body()));
                List<String> strings = new ArrayList<String>();
                if ("success".equals(response.body().getFlag())) {
                    for (int i = 0;i < response.body().getResult().size(); i++) {
                        strings.add(response.body().getResult().get(i).getAwardRegion());
//                        areaAddressData[i] = response.body().getResult().get(i).getAwardRegion();
                        areaAddressData = (String[]) strings.toArray(new String[0]);
                        beanList = response.body().getResult();
                    }
                    System.out.println(JSON.toJSONString(areaAddressData));
                    initSpinner();
                }
            }
            @Override
            public void onFailure(Call<AreaAddress> call, Throwable t) {
            }
        });
    }

    /**
     * 验证信息是否为空
     * @param
     */
    private boolean judPhone() {
        if ("".equals(username.getText().toString())) {
            Toast.makeText(this, "请输入收件人", Toast.LENGTH_LONG).show();
            return false;
        }
        if ("".equals(okSpinnerText)) {
            Toast.makeText(this, "请选择区名", Toast.LENGTH_LONG).show();
            return false;
        }
        if ("".equals(address.getText().toString())) {
            Toast.makeText(this, "请输入地址", Toast.LENGTH_LONG).show();
            return false;
        }
        if ("".equals(phone.getText().toString())){
            Toast.makeText(this, "请输入联系电话", Toast.LENGTH_LONG).show();
            return false;
        } else if (phone.getText().toString().length() != 11){
            Toast.makeText(this, "请输入11位有效联系电话", Toast.LENGTH_LONG).show();
            return false;
        }else {
            return true;
        }
    }


    //兑换奖品
    public void addReceivingDetailedAddress () {
//        System.out.println("%%%%%"+ okSpinnerId+"%%%%%"+ luckDrawRecordId+"%%%%%"+address.getText().toString()+"%%%%%"+phone.getText().toString()+"%%%%%"+username.getText().toString());
        HttpHelper.initHttpHelper().addReceivingDetailedAddress(okSpinnerId,luckDrawRecordId,address.getText().toString(),phone.getText().toString(),username.getText().toString()).enqueue( new retrofit2.Callback<CheckSMS>() {

            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if ("success".equals(response.body().getFlag())) {
                    Toast.makeText(PrizeExchangeActivity.this,"兑换成功",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PrizeExchangeActivity.this,MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(PrizeExchangeActivity.this,"兑换失败",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(PrizeExchangeActivity.this,"网络错误",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void back(View view) {
        finish();
    }
    /**
     * 重写返回键
     * 监听Back键按下事件
     * super.onBackPressed()会自动调用finish()方法,关闭当前Activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
