package zhixin.cn.com.happyfarm_user.model;

import android.content.Intent;

import java.util.List;

public class LandInfoCardItem {

    private List<landInfoCardBean> result;
    private String flag;

    public List<landInfoCardBean> getResult() {
        return result;
    }

    public void setResult(List<landInfoCardBean> result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }



    public class landInfoCardBean {

        private Integer id;
        private Integer landNo;
        private String userName;
        private String headImg;
        private String cropName;
        private String photoUrl;
        private String albumFirstPicture;
        private List<Crop> Crop;

        private Long leaseTime;
        private Long expirationTime;
        private Integer userId;
        private Integer landType;
        private String alias;



        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getLandNo() {
            return landNo;
        }

        public void setLandNo(Integer landNo) {
            this.landNo = landNo;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getHeadImg() {
            return headImg;
        }

        public void setHeadImg(String headImg) {
            this.headImg = headImg;
        }

        public String getCropName() {
            return cropName;
        }

        public void setCropName(String cropName) {
            this.cropName = cropName;
        }

        public String getPhotoUrl() {
            return photoUrl;
        }

        public void setPhotoUrl(String photoUrl) {
            this.photoUrl = photoUrl;
        }

        public String getAlbumFirstPicture() {
            return albumFirstPicture;
        }

        public void setAlbumFirstPicture(String albumFirstPicture) {
            this.albumFirstPicture = albumFirstPicture;
        }

        public List<landInfoCardBean.Crop> getCrop() {
            return Crop;
        }

        public void setCrop(List<landInfoCardBean.Crop> crop) {
            Crop = crop;
        }

        public Long getLeaseTime() {
            return leaseTime;
        }

        public void setLeaseTime(Long leaseTime) {
            this.leaseTime = leaseTime;
        }

        public Long getExpirationTime() {
            return expirationTime;
        }

        public void setExpirationTime(Long expirationTime) {
            this.expirationTime = expirationTime;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getLandType() {
            return landType;
        }

        public void setLandType(Integer landType) {
            this.landType = landType;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public class Crop {
            private int id;
            private String cropName;
            private String otherNames;
            private int cycle;
            private String cropImg;
            private long startSowing;
            private long endSeeding;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCropName() {
                return cropName;
            }

            public void setCropName(String cropName) {
                this.cropName = cropName;
            }

            public String getOtherNames() {
                return otherNames;
            }

            public void setOtherNames(String otherNames) {
                this.otherNames = otherNames;
            }

            public int getCycle() {
                return cycle;
            }

            public void setCycle(int cycle) {
                this.cycle = cycle;
            }

            public String getCropImg() {
                return cropImg;
            }

            public void setCropImg(String cropImg) {
                this.cropImg = cropImg;
            }

            public long getStartSowing() {
                return startSowing;
            }

            public void setStartSowing(long startSowing) {
                this.startSowing = startSowing;
            }

            public long getEndSeeding() {
                return endSeeding;
            }

            public void setEndSeeding(long endSeeding) {
                this.endSeeding = endSeeding;
            }
        }
    }


}
