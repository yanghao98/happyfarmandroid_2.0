package zhixin.cn.com.happyfarm_user;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMContactListener;
import com.hyphenate.EMError;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMCmdMessageBody;;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMPushConfigs;
import com.hyphenate.util.EMLog;
import com.hyphenate.util.NetUtils;
import com.qiniu.android.common.AutoZone;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import q.rorbin.badgeview.Badge;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.ViewPagerAdapter;
import zhixin.cn.com.happyfarm_user.Page.ForumPage;
import zhixin.cn.com.happyfarm_user.Page.NewFarmPage;
import zhixin.cn.com.happyfarm_user.Page.NewMarketPage;
import zhixin.cn.com.happyfarm_user.Page.NewVipPage;
import zhixin.cn.com.happyfarm_user.Page.PersonalCenterPage;
import zhixin.cn.com.happyfarm_user.Page.PersonalMessagePage;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.easyMob.ContactFragment;
import zhixin.cn.com.happyfarm_user.entity.TabEntity;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.PlotPlantingStandard;
import zhixin.cn.com.happyfarm_user.model.QiNiu;
import zhixin.cn.com.happyfarm_user.model.SeachConfig;
import zhixin.cn.com.happyfarm_user.model.ShoppingCartNum;
import zhixin.cn.com.happyfarm_user.other.OSUtils;
import zhixin.cn.com.happyfarm_user.utils.AdsDialog;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.GuideView;

@Route(path = "/app/MainActivity")
public class MainActivity extends ABaseActivity {
    private static final String TAG = "MainActivity->";
    private AHBottomNavigationViewPager viewPager;
    private CommonTabLayout mTabLayout;
    private String[] mTitles = {"菜园", "菜场", "专享", "社区", "我的"};
    private int[] mIconUnselectIds = {
            R.mipmap.homepage_false, R.mipmap.the_market_false,
            R.mipmap.vip_no_click, R.mipmap.forum_false, R.mipmap.my_false};
    private int[] mIconSelectIds = {
            R.mipmap.homepage_true, R.mipmap.the_market_true,
            R.mipmap.vip_yes_click, R.mipmap.forum_true, R.mipmap.my_true};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private Badge messageBadge;

    private ViewPagerAdapter viewPagerAdapter;
    private List<Fragment> fragmentPages;
    private NewFarmPage newFarmPage;
    private NewMarketPage newMarketPage;
    private PersonalMessagePage personalMessagePage;
    private NewVipPage newVipPage;
    private ForumPage forumPage;
    private PersonalCenterPage personalCenterPage;
    //退出时的时间
    private long mExitTime;
    private int page;
    private Vibrator vibrator;

    boolean mIsSupportedBade = true;
    public static Activity activity;

    private GuideView guideView1;
    private GuideView guideView2;
    private GuideView guideView3;
    private GuideView guideView4;
    private GuideView guideView5;

    private AdsDialog dialog;

    @Autowired(name = "name")
    String name = "";
    private UploadManager uploadManager1;

    //消息数量
    private int totalNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTransparent();
        //查询应用程序可用的最大内存
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        Log.e(TAG, "Max memory is " + maxMemory + "KB");
        setContentView(R.layout.activity_main);
        activity = this;
        viewPager = findViewById(R.id.viewpager);
        mTabLayout = findViewById(R.id.tabview);
        showDialog();
        initTabLayout();
        initFragments();
        //通过注册消息监听来接收消息。
        EMClient.getInstance().chatManager().addMessageListener(msgListener);
        //TODO 通过注册消息监听好友请求。
        EMClient.getInstance().contactManager().setContactListener(mContactListener);
        guide();//引导蒙层
        searchConfig();//TODO 租地配置
        //注册一个监听连接状态的listener
        EMClient.getInstance().addConnectionListener(new MyConnectionListener());
        // 获取华为 HMS 推送 token
//        HMSPushHelper.getInstance().connectHMS(this);
//        HMSPushHelper.getInstance().getHMSPushToken();
        initMIPUSH();
//        initHUAWEIPUSH();
        /**
         * 设置是否接受Push通知栏调用示例。
         */
//        HmsMessaging.getInstance(MainActivity.this).turnOnPush().addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(Task<Void> task) {
//                if (task.isSuccessful()) {
//                    Log.i(TAG, "turnOnPush Complete");
//                } else {
//                    Log.e(TAG, "turnOnPush failed: ret=" + task.getException().getMessage());
//                }
//            }
//        });

    }

    /**
     * 首页广告弹窗
     */
    public void showDialog() {
        HttpHelper.initHttpHelper().searchPlotPlantingStandard().enqueue(new Callback<PlotPlantingStandard>() {
            @Override
            public void onResponse(Call<PlotPlantingStandard> call, Response<PlotPlantingStandard> response) {
                System.out.println("弹窗DATA：" + JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    for (int i = 0;i < response.body().getResult().size();i++) {
                        PlotPlantingStandard.PlotPlantingStandardBean bean = response.body().getResult().get(i);
                        System.out.println("11："+bean.getVersionNumber());
                        if (!"".equals(bean.getVersionNumber())) {
//                            System.out.println("22："+bean.getVersionNumber());
//                            System.out.println(getLocalVersionName(MainActivity.this));
//                            System.out.println(response.body().getResult().get(i).getVersionNumber());
                            if (getLocalVersionName(MainActivity.this).replace(".",  "").equals(response.body().getResult().get(i).getVersionNumber())) {
                                System.out.println("版本号相同");
                            } else {
                                System.out.println("版本号不同");
                            }
                          System.out.println("版本号对比结果：" + Config.isAbout);
                        } else {
                          System.out.println("33："+bean.getVersionNumber());
                        }
                    }
                    for (int i = 0;i < response.body().getResult().size();i++) {
                        PlotPlantingStandard.PlotPlantingStandardBean bean = response.body().getResult().get(i);
                        String url = "";
                        String title = "";
                        String content = "";
                        int type;
                        int version = Integer.valueOf(getLocalVersionName(MainActivity.this).replace(".",  ""));
                        String getVerision = bean.getVersionNumber().equals("") ? 0 + "" : bean.getVersionNumber();
                        int versionSql = Integer.valueOf(getVerision);
                        System.out.println("当前版本：" + version);
                        System.out.println("数据库版本：" + versionSql);
                        if (version < versionSql) {
                            Config.isAbout = true;
                            System.out.println("数据库大于当前版本");
                        } else {
                            Config.isAbout = false;
                            System.out.println("数据库小于等于当前版本");
                        }
                        if (version == versionSql) {
                            url = bean.getImageUrl();
                            title = bean.getTitle();
                            content = bean.getContent();
                            type = 2;
                            //广告弹出框设置
                            dialog = new AdsDialog(MainActivity.this,url,title,content,type);
                            dialog.showDialog();
                            break;
                        } else if (bean.getState() == 2 && version < versionSql && bean.getType() == 1){
                            url = bean.getImageUrl();
                            title = bean.getTitle();
                            content = bean.getContent();
                            type = 1;
                            //升级弹出框设置
                            dialog = new AdsDialog(MainActivity.this,url,title,content,type);
                            dialog.showDialog();
                            break;
                        } else if (bean.getState() == 2 && bean.getType() == 2) {
                            url = bean.getImageUrl();
                            title = bean.getTitle();
                            content = bean.getContent();
                            type = 2;
                            //广告弹出框设置
                            dialog = new AdsDialog(MainActivity.this,url,title,content,type);
                            dialog.showDialog();
                            break;
                        } else if (bean.getState() == 1 && version < versionSql) {
                            url = bean.getImageUrl();
                            title = bean.getTitle();
                            content = bean.getContent();
                            type = 1;
                            //升级弹出框设置
                            dialog = new AdsDialog(MainActivity.this,url,title,content,type);
                            dialog.showDialog();
                            break;
                        } else {
                            url = bean.getImageUrl();
                            title = bean.getTitle();
                            content = bean.getContent();
                            type = 2;
                            //广告弹出框设置
                            dialog = new AdsDialog(MainActivity.this,url,title,content,type);
                            dialog.showDialog();
                            break;
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<PlotPlantingStandard> call, Throwable t) {

            }
        });

    }

    /**
     * 获取小米推送token
     */
    private void initMIPUSH() {
        if ("Xiaomi".equals(OSUtils.getDeviceBrand())) {
            //获取小米推送token
            String miToken = MiPushClient.getRegId(this);
            Config.setPUSHTOKEN("miPush_" + miToken);
        }
//        Log.i("PushService", "MiPush_Token: " + Config.getPUSHTOKEN());
    }

    /**
     * 获取华为推送token
     * 1. 调用getToken方法后获得的token一定要做判空处理。
     * 2. 在调用getToken方法外一定要增加异常捕获处理。
     */
//    public void initHUAWEIPUSH() {
//        new Thread() {
//            @Override
//            public void run() {
//                try {
//                    // read from agconnect-services.json
//                    String appId = AGConnectServicesConfig.fromContext(MainActivity.this).getString("client/app_id");
//                    String token = HmsInstanceId.getInstance(MainActivity.this).getToken(appId, "HCM");
//                    Log.i(TAG, "get token:" + token);
//                    if(!TextUtils.isEmpty(token)) {
//                        sendRegTokenToServer(token);
//                    }
//
//                } catch (com.huawei.hms.common.ApiException e) {
//                    Log.e(TAG, "get token failed, " + e);
//                }
//            }
//        }.start();
//    }
//    private void sendRegTokenToServer(String token) {
//        Log.i(TAG, "sending token to server. token:" + token);
//    }
    // This method callback must be completed in 10 seconds. Otherwise, you need to start a new Job for callback processing.

    /**
     * Token发生变化时或者EMUI版本低于10.0以 onNewToken 方法返回
     * @param token
     */
//    @Override
    public void onNewToken(String token) {
        Log.i(TAG, "received refresh token:" + token);
        // send the token to your app server.
        if (!TextUtils.isEmpty(token)) {
            refreshedTokenToServer(token);
        }
    }
    private void refreshedTokenToServer(String token) {
        Log.i(TAG, "sending token to server. token:" + token);
    }
    //注销华为PUSH token
//    private void deleteToken() {
//        new Thread() {
//            @Override
//            public void run() {
//                try {
//                    // read from agconnect-services.json
//                    String appId = AGConnectServicesConfig.fromContext(MainActivity.this).getString("client/app_id");
//                    HmsInstanceId.getInstance(MainActivity.this).deleteToken(appId, "HCM");
//                    Log.i(TAG, "deleteToken success.");
//                } catch (com.huawei.hms.common.ApiException e) {
//                    Log.e(TAG, "deleteToken failed." + e);
//                }
//            }
//        }.start();
//    }
//
//    /**
//     *通过 onMessageReceived 回调来接受消息，接收后应用自己处理。
//     * @param message
//     */
////    @Override
//    public void onMessageReceived(RemoteMessage message) {
//
//        Log.i(TAG, "onMessageReceived is called");
//        if (message == null) {
//            Log.e(TAG, "Received message entity is null!");
//            return;
//        }
//        Log.i(TAG, "getCollapseKey: " + message.getCollapseKey()
//                + "\n getData: " + message.getData()
//                + "\n getFrom: " + message.getFrom()
//                + "\n getTo: " + message.getTo()
//                + "\n getMessageId: " + message.getMessageId()
//                + "\n getOriginalUrgency: " + message.getOriginalUrgency()
//                + "\n getUrgency: " + message.getUrgency()
//                + "\n getSendTime: " + message.getSentTime()
//                + "\n getMessageType: " + message.getMessageType()
//                + "\n getTtl: " + message.getTtl());
//        RemoteMessage.Notification notification = message.getNotification();
//        if (notification != null) {
//            Log.i(TAG, "\n getImageUrl: " + notification.getImageUrl()
//                    + "\n getTitle: " + notification.getTitle()
//                    + "\n getTitleLocalizationKey: " + notification.getTitleLocalizationKey()
//                    + "\n getTitleLocalizationArgs: " + Arrays.toString(notification.getTitleLocalizationArgs())
//                    + "\n getBody: " + notification.getBody()
//                    + "\n getBodyLocalizationKey: " + notification.getBodyLocalizationKey()
//                    + "\n getBodyLocalizationArgs: " + Arrays.toString(notification.getBodyLocalizationArgs())
//                    + "\n getIcon: " + notification.getIcon()
//                    + "\n getSound: " + notification.getSound()
//                    + "\n getTag: " + notification.getTag()
//                    + "\n getColor: " + notification.getColor()
//                    + "\n getClickAction: " + notification.getClickAction()
//                    + "\n getChannelId: " + notification.getChannelId()
//                    + "\n getLink: " + notification.getLink()
//                    + "\n getNotifyId: " + notification.getNotifyId());
//        }
//        Boolean judgeWhetherIn10s = false;
//        // If the messages are not processed in 10 seconds, the app needs to use WorkManager for processing.
//        if (judgeWhetherIn10s) {
//            startWorkManagerJob(message);
//        } else {
//            // Process message within 10s
//            processWithin10s(message);
//        }
//    }
//    private void startWorkManagerJob(RemoteMessage message) {
//        Log.d(TAG, "Start new job processing.");
//    }
//    private void processWithin10s(RemoteMessage message) {
//        Log.d(TAG, "Processing now.");
//    }
    /**
     *
     */


    /**
     * 对返回键进行监听
     *
     * @param keyCode 手机按钮监听码
     * @param event   响应事件
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //TODO 这个判断主要是让点击返回先到选地列表才能退出
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (viewPager.getCurrentItem() != 0) {
                viewPager.setCurrentItem(0, false);
                mTabLayout.setCurrentTab(0);
                return true;
            } else {
                exit();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 返回按钮触发事件
     */
    public void exit() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            Toast.makeText(MainActivity.this, "再次点击退出应用", Toast.LENGTH_SHORT).show();
            mExitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }

    /**
     * 初始化tabLayout
     */
    private void initTabLayout() {
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }
        //设置tablayout数据
        mTabLayout.setTabData(mTabEntities);
        for (int i = 0; i < mTitles.length; i++) {
            //此处为骚操作，由于下方设置badge显示的位置不太理想 后来移动它的位置，结果会被遮盖，主要原因是由于图片的宽高所影响
            //于是就在activity_main布局文件将图片宽高调大
            //再次设置内边距即可
            mTabLayout.getIconView(i).setPadding(5, 0, 5, 0);
        }
        mTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                //第一次选中
                Log.i(TAG, "onTabSelect: " + position);

                switch (position) {
                    case 0:
                        Config.bottomBarItem = 1;
                        newFarmPage.userInfoTokenTest();
                        viewPager.setCurrentItem(position,false);
                        newMarketPage.selectedCropId = 0;//清空加入购物车的菜品的id，
                        newMarketPage.shoppingCartNumber = 0;//清空加入购物车菜品的数量
                        newFarmPage.searchShoppingCartNum();
                        break;
                    case 1:
                        Config.bottomBarItem = 2;
                        viewPager.setCurrentItem(position, false);
                        newMarketPage.searchShoppingCartNum();
                        newMarketPage.getEMNumber(EMClient.getInstance().chatManager().getUnreadMessageCount());
                        break;
                    case 2:
                        Config.bottomBarItem = 3;
                        newVipPage.userInfoTokenTest();//专享
                        viewPager.setCurrentItem(position, false);
                        newMarketPage.selectedCropId = 0;//清空加入购物车的菜品的id，
                        newMarketPage.shoppingCartNumber = 0;//清空加入购物车菜品的数量
                        newVipPage.searchShoppingCartNum();
                        break;
                    case 3:
                        Config.bottomBarItem = 4;
                        forumPage.userInfoTokenTest();
                        viewPager.setCurrentItem(position, false);
                        newMarketPage.selectedCropId = 0;//清空加入购物车的菜品的id，
                        newMarketPage.shoppingCartNumber = 0;//清空加入购物车菜品的数量
                        break;
                    case 4:
                        Config.bottomBarItem = 5;
                        personalCenterPage.userInfoTokenTest();//滚动到个人中心加载token是否失效
                        viewPager.setCurrentItem(position, false);
                        newMarketPage.selectedCropId = 0;//清空加入购物车的菜品的id，
                        newMarketPage.shoppingCartNumber = 0;//清空加入购物车菜品的数量
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onTabReselect(int position) {
                //已经选中的item再次被点击调用此方法
//                Log.i(TAG, "onTabReselect: "+position);
                if (position == 0) {

                }
            }
        });

        viewPager.setCurrentItem(1);
    }

    /**
     * 获取购物车数量
     */
    public void searchShoppingCartNum() {
        HttpHelper.initHttpHelper().searchShoppingCartNum().enqueue(new Callback<ShoppingCartNum>() {
            @Override
            public void onResponse(Call<ShoppingCartNum> call, Response<ShoppingCartNum> response) {
//                System.out.println("购物车数量"+JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {

                }
            }
            @Override
            public void onFailure(Call<ShoppingCartNum> call, Throwable t) {

            }
        });
    }

    /**
     * 设置消息显示数量
     *
     * @param num      消息数量
     * @param position 设置显示tablayout的item 暂时未时候，考虑后期可能所有的item都需要
     */
    private void setMsgNum(int position, int num) {
        if (messageBadge != null) {
//            Log.i(TAG, "消息设置成功setMsgNum: "+num);
            messageBadge.setBadgeNumber(num);
        } else {
//            Log.i(TAG, "消息设置失败setMsgNum: "+num);
        }

    }

    /**
     * 初始化viewpager
     */
    private void initFragments() {
        fragmentPages = new ArrayList<>();
//        farmPage = new FarmPage();
        newFarmPage = new NewFarmPage();
        newMarketPage = new NewMarketPage();
//        theMarketPage = new TheMarketPage();
        personalMessagePage = new PersonalMessagePage();
//        vipPage = new VipPage();//专享
        newVipPage = new NewVipPage();
        Fragment contactFragment = new ContactFragment();
        forumPage = new ForumPage();
        personalCenterPage = new PersonalCenterPage();

//        fragmentPages.add(farmPage);
        fragmentPages.add(newFarmPage);
//        fragmentPages.add(theMarketPage);
        fragmentPages.add(newMarketPage);
        fragmentPages.add(newVipPage);//专享页面
//        fragmentPages.add(contactFragment);
        fragmentPages.add(forumPage);
        fragmentPages.add(personalCenterPage);

//        fragmentPages.add(personalMessagePage);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragmentPages);
        //TODO 设置缓存页
        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void back(View view) {

    }

    //TODO 实现ConnectionListener接口
    private class MyConnectionListener implements EMConnectionListener {
        @Override
        public void onConnected() {
        }

        @Override
        public void onDisconnected(final int error) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (error == EMError.USER_REMOVED) {
                        // 显示帐号已经被移除
//                        Log.i("MainActivity->", "run: 显示帐号已经被移除");
                        EMClient.getInstance().logout(true);
                    } else if (error == EMError.USER_LOGIN_ANOTHER_DEVICE) {
                        // 显示帐号在其他设备登录
//                        Log.i("MainActivity->", "run: 显示帐号在其他设备登录");
                        EMClient.getInstance().logout(true);
                    } else {
                        if (NetUtils.hasNetwork(MainActivity.this)) {
                            //连接不到聊天服务器
//                            Log.i("MainActivity->", "run: 连接不到聊天服务器");
                            //EMClient.getInstance().logout(true);
                        } else {
                            //当前网络不可用，请检查网络设置
//                            Log.i("MainActivity->", "run: 当前网络不可用，请检查网络设置");
                            //EMClient.getInstance().logout(true);
                        }
                    }
                }
            });
        }
    }

    //TODO 查询配置
    private void searchConfig() {
        Call<SeachConfig> seachConfig = HttpHelper.initHttpHelper().seachConfig();
        seachConfig.enqueue(new Callback<SeachConfig>() {
            @Override
            public void onResponse(Call<SeachConfig> call, Response<SeachConfig> response) {
//                Log.e(TAG, "查询配置 onResponse: "+ JSON.toJSONString(response.body()));
                try {
                    if ("success".equals(response.body().getFlag())) {
                        Config.myLandWatch = response.body().getResult().getMyWatchTime();
                        Config.otherLandWatch = response.body().getResult().getWatchTime();
                        Config.rtspStartTime = response.body().getResult().getRtspStartTime();
                        Config.rtspEndTime = response.body().getResult().getRtspEndTime();
                    } else {
                        Config.myLandWatch = 10;
                        Config.otherLandWatch = 5;
                    }
                } catch (Exception e) {
                    e.getMessage();
                    Config.myLandWatch = 10;
                    Config.otherLandWatch = 5;
                }
            }

            @Override
            public void onFailure(Call<SeachConfig> call, Throwable t) {
                Config.myLandWatch = 10;
                Config.otherLandWatch = 5;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ("".equals(getToken())) {
            setMsgNum(2, 0);
        } else {
            int total = EMClient.getInstance().chatManager().getUnreadMessageCount();
            setMsgNum(2, total);
        }

//        Log.i(TAG, "最终可以上传七牛的视频链接: " + getIntent().getStringExtra("name"));
        if (null != getIntent().getStringExtra("name")) {
            Configuration config = new Configuration.Builder().zone(AutoZone.autoZone).build();
            uploadManager1 = new UploadManager(config);
            //TODO 获取七牛token
                if ("".equals(Config.qiuNiuToken)){
//                    Log.i(TAG, "onResume: 七牛token是空的");
                    HttpHelper.initHttpHelper().getQiniuToken().enqueue(new Callback<QiNiu>() {
                        @Override
                        public void onResponse(Call<QiNiu> call, Response<QiNiu> response) {
                            Log.e("qiniuTokenData", JSON.toJSONString(response.body()));
                            if ("success".equals(response.body().getFlag())) {
                                Config.qiuNiuToken = response.body().getResult().getUploadToken();
                                Config.qiuNiuDomain = response.body().getResult().getDomain();
                                Config.qiuNiuImgUrl = response.body().getResult().getImgUrl();
                                Date d = new Date();
                                uploadManager1.put(getIntent().getStringExtra("name"), "new_video_" + getTimestamp(d), Config.qiuNiuToken,
                                        new UpCompletionHandler() {
                                            @Override
                                            public void complete(String key, ResponseInfo info, org.json.JSONObject response) {
                                                //res包含hash、key等信息，具体字段取决于上传策略的设置
                                                if(info.isOK()) {
                                                    String headImg = Config.qiuNiuImgUrl+"new_video_" + getTimestamp(d);
                                                    Log.i("视频路径", "Upload Success" + headImg);
                                                    addGrowthAlbum(headImg);
                                                } else {

                                                }
                                                Log.i("qiniu", key + ",\r\n " + info + ",\r\n " + response);
                                            }
                                        },new UploadOptions(null, null, false,
                                                new UpProgressHandler(){
                                                    public void progress(String key, double percent){
                                                        Log.i("qiniu", key + ": " + percent);
                                                    }
                                                }, null));
                            }else {

                            }
                        }

                        @Override
                        public void onFailure(Call<QiNiu> call, Throwable t) {

                        }
                    });
                } else {
                    Log.i(TAG, "onResume: 七牛token存在");
                    Date d = new Date();
                    uploadManager1.put(getIntent().getStringExtra("name"), "new_video_" + getTimestamp(d), Config.qiuNiuToken,
                            new UpCompletionHandler() {
                                @Override
                                public void complete(String key, ResponseInfo info, org.json.JSONObject response) {
                                    //res包含hash、key等信息，具体字段取决于上传策略的设置
                                    if(info.isOK()) {
                                        String headImg = Config.qiuNiuImgUrl+"new_video_" + getTimestamp(d);
                                        Log.i("视频路径", "Upload Success" + headImg);
                                        addGrowthAlbum(headImg);
                                    }
                                    Log.i("qiniu", key + ",\r\n " + info + ",\r\n " + response);
                                }
                            },new UploadOptions(null, null, false,
                                    new UpProgressHandler(){
                                        public void progress(String key, double percent){
                                            Log.i("qiniu", key + ": " + percent);
                                        }
                                    }, null));
                }

        }

    }

    private void addGrowthAlbum(String photoUrl) {
//        Log.i(TAG, "上传的视频原路径addGrowthAlbum: " + Config.photoAlubmURL +"，上传的视频现路径photoUrl: "+photoUrl);
        if (!"".equals(Config.photoAlubmURL)) {
            HttpHelper.initHttpHelper().addGrowthAlbum(Config.photoAlubmURL,photoUrl).enqueue(new Callback<CheckSMS>() {
                @Override
                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                    Log.i(TAG, "onResponse: " + JSON.toJSONString(response.body()));
                    Toast.makeText(MainActivity.this,response.body().getResult(),Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onFailure(Call<CheckSMS> call, Throwable t) {
//                    Log.i(TAG, "onFailure: " + JSON.toJSONString(t));
//                    Log.i(TAG, "onResponse: 替换视频路径失败");
                }
            });
            Config.photoAlubmURL = "";
        } else {
            Log.i(TAG, "原视频路径没传过来 " );
        }

    }

    /**
     获取精确到毫秒的时间戳
     * @param date
     * @return
     **/
    public static Long getTimestamp(Date date){
        if (null == date) {
            return (long) 0;
        }
        String timestamp = String.valueOf(date.getTime());
        return Long.valueOf(timestamp);
    }

    private String getToken() {
        return App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("token", "");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        Log.d("切到其他页面", ">>>>>>>>>>>>>>>>>>>切到后台 activity process");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        newMarketPage.selectedCropId = 0;//清空加入购物车的菜品的id，
        newMarketPage.shoppingCartNumber = 0;//清空加入购物车菜品的数量
//        Log.d("切到main页面", ">>>>>>>>>>>>>>>>>>>切到前台 activity process");
        searchUserInfo();
        //TODO 这里主要判断消息页面点击登录返回请求
        if (viewPager.getCurrentItem() == 2) {
        }
    }

    EMMessageListener msgListener = new EMMessageListener() {

        @Override
        public void onMessageReceived(List<EMMessage> messages) {
            //收到消息
            Log.i("messages收到消息", JSON.toJSONString(messages));
            String chatType = String.valueOf(messages.get(0).getChatType());
            Log.i("messages收到消息类型", chatType);
            if ("GroupChat".equals(chatType)) {
                //收到群组消息
                saveGroupBadge("-1");
            }
            int total = EMClient.getInstance().chatManager().getUnreadMessageCount();
            totalNumber = total;
            //TODO 收到消息重新获取未读数量  并判断是否再main页面
//            if (away == 3){
            setMsgNum(2, total);
//            farmPage.getEMNumber(total);
//            theMarketPage.getEMNumber(total);
            newFarmPage.getEMNumber(total);
            newMarketPage.getEMNumber(total);
            newVipPage.getEMNumber(total);
            forumPage.getEMNumber(total);
            personalCenterPage.getEMNumber(total);
//            }
            /*
             * 想设置震动大小可以通过改变pattern来设定，如果开启时间太短，震动效果可能感觉不到
             * */
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            long[] pattern = {100, 400, 100, 400}; // 停止 开启 停止 开启
            if (vibrator != null)
                vibrator.vibrate(pattern, -1); //重复两次上面的pattern 如果只想震动一次，index设为-1
//            Log.i(TAG, "onMessageReceived: " + getIsCurren());
            if (getIsCurren() == 2) {
//                Log.e("手机厂商：", "手机厂商：" + OSUtils.getDeviceBrand());
//                Log.e("手机型号：", "手机型号：" + OSUtils.getSystemModel());
//                Log.e("手机当前系统语言：", "手机当前系统语言：" + OSUtils.getSystemLanguage());
//                Log.e("Android系统版本号：", "Android系统版本号：" + OSUtils.getSystemVersion());
                if ("HUAWEI".equals(OSUtils.getDeviceBrand()))
                    SetBadge(total);
                sendNotification(messages.get(0).getFrom()); //TODO 通知栏显示
            } else {
                Log.i(TAG, "onMessageReceived: 没有弹出通知栏提示框");
            }
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
            //收到透传消息
//            Log.i("收到透传消息", String.valueOf(messages));
            for (EMMessage message : messages) {
                EMLog.d("收到透传消息", "receive command message");
//                get message body
                EMCmdMessageBody cmdMsgBody = (EMCmdMessageBody) message.getBody();
                final String action = cmdMsgBody.action();//获取自定义action
                //获取扩展属性 此处省略
                //maybe you need get extension of your message
                //message.getStringAttribute("");
                EMLog.d("收到透传消息", String.format("Command：action:%s,message:%s", action, message.toString()));
            }
        }

        @Override
        public void onMessageRead(List<EMMessage> messages) {
            //收到已读回执
//            Log.i("收到已读回执", String.valueOf(messages));
        }

        @Override
        public void onMessageDelivered(List<EMMessage> message) {
            //收到已送达回执
//            Log.i("收到已送达回执", String.valueOf(message));
        }

        @Override
        public void onMessageRecalled(List<EMMessage> messages) {
            //消息被撤回
//            Log.i("消息被撤回", String.valueOf(messages));
        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
            //消息状态变动
//            Log.i("消息被撤回", String.valueOf(message));
        }
    };

    private EMContactListener mContactListener = new EMContactListener() {
        @Override
        public void onContactInvited(String username, String reason) {
//            Log.i("想要添加您为好友:", username);
            //收到好友邀请
            saveBadge("-1");
        }

        @Override
        public void onFriendRequestAccepted(String s) {

        }

        @Override
        public void onFriendRequestDeclined(String s) {
            //好友请求被拒绝
        }

        @Override
        public void onContactDeleted(String username) {
//            Log.i("想要删除您为好友:", "onContactDeleted: " + username);
            //被删除时回调此方法
            //删除和某个user会话，如果需要保留聊天记录，传false
            EMClient.getInstance().chatManager().deleteConversation(username, true);
        }

        @Override
        public void onContactAdded(String username) {
//            Log.i("想要增加您为好友:", "onContactAdded: " + username);
            //增加了联系人时回调此方法
        }
    };

    //TODO 保存添加好友total
    public void saveBadge(String badge) {
        SharedPreferences.Editor editor = getSharedPreferences("User_data", MODE_PRIVATE).edit();
        editor.putString("badge", badge);
        editor.apply();
    }

    /**
     * 设置群聊布局消息
     *
     * @param badge
     */
    public void saveGroupBadge(String badge) {
        SharedPreferences.Editor editor = getSharedPreferences("User_data", MODE_PRIVATE).edit();
        editor.putString("groupBadge", badge);
        editor.apply();
    }

    private void searchUserInfo() {
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if ("success".equals(response.body().getFlag())) {
                    App.USER = response.body().getResult();
//                    Log.d(TAG, "onResponse: " + response.body().getResult().getIsVoice());
                    Config.AUDIO_STATE = response.body().getResult().getIsVoice() == 1;
                }
            }

            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }

    /**
     * 由于从菜场返回过来的值intent接收不到，所以重写onNewIntent方法
     **/
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        //TODO 视频页进入菜场设置
        page = intent.getIntExtra("page", 0);//第一个参数是取值的key,第二个参数是默认值
//        Log.d(TAG, "onCreate1: "+page);
        if (page != 0) {
            viewPager.setCurrentItem(page, false);
            mTabLayout.setCurrentTab(1);
        }
    }

    /**
     * 引导蒙层
     */
    public void guide() {
        //判断是否第一次登陆
        if (("isFirst").equals(getGuide())) {//不是第一次加载初始页面
//            System.out.println("不是第一次登陆");
        } else {
//            System.out.println("是第一次登陆");
            // 使用文字
            TextView tv1 = new TextView(this);
            tv1.setText(R.string.tv1);
            tv1.setTextColor(getResources().getColor(R.color.white));
            tv1.setTextSize(20);
            tv1.setGravity(Gravity.CENTER);
            TextView tv2 = new TextView(this);
            tv2.setText(R.string.tv2);
            tv2.setTextColor(getResources().getColor(R.color.white));
            tv2.setTextSize(20);
            tv2.setGravity(Gravity.CENTER);
            TextView tv3 = new TextView(this);
            tv3.setText(R.string.tv3);
            tv3.setTextColor(getResources().getColor(R.color.white));
            tv3.setTextSize(20);
            tv3.setGravity(Gravity.CENTER);
            TextView tv4 = new TextView(this);
            tv4.setText(R.string.tv4);
            tv4.setTextColor(getResources().getColor(R.color.white));
            tv4.setTextSize(20);
            tv4.setGravity(Gravity.CENTER);
            TextView tv5 = new TextView(this);
            tv5.setText(R.string.tv5);
            tv5.setTextColor(getResources().getColor(R.color.white));
            tv5.setTextSize(20);
            tv5.setGravity(Gravity.CENTER);

            // 使用图片
            final ImageView iv = new ImageView(this);
            iv.setImageResource(R.mipmap.wozhidaole);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            iv.setLayoutParams(params);

            guideView1 = GuideView.Builder
                    .newInstance(this)
                    .setTargetView(mTabLayout.getIconView(0))
                    .setCustomGuideView(tv1)
                    .setTextGuideView(iv)
                    .setDirction(GuideView.Direction.TOP)
                    .setShape(GuideView.MyShape.CIRCULAR)   // 设置矩形显示区域，
                    .setRadius(80)          // 设置圆形或矩形透明区域半径，默认是targetView的显示矩形的半径，如果是矩形，这里是设置矩形圆角大小
                    .setBgColor(getResources().getColor(R.color.shadow))
                    .setOnclickListener(new GuideView.OnClickCallback() {
                        @Override
                        public void onClickedGuideView() {
                            guideView1.hide();
                            guideView2.show();
                            saveGuide("isFirst");
                        }
                    })
                    .build();
            guideView2 = GuideView.Builder
                    .newInstance(this)
                    .setTargetView(mTabLayout.getIconView(1))
                    .setCustomGuideView(tv2)
                    .setTextGuideView(iv)
                    .setDirction(GuideView.Direction.TOP)
                    .setShape(GuideView.MyShape.CIRCULAR)   // 设置矩形显示区域，
                    .setRadius(80)          // 设置圆形或矩形透明区域半径，默认是targetView的显示矩形的半径，如果是矩形，这里是设置矩形圆角大小
                    .setBgColor(getResources().getColor(R.color.shadow))
                    .setOnclickListener(new GuideView.OnClickCallback() {
                        @Override
                        public void onClickedGuideView() {
                            guideView2.hide();
                            guideView3.show();
                        }
                    })
                    .build();
            guideView3 = GuideView.Builder
                    .newInstance(this)
                    .setTargetView(mTabLayout.getIconView(2))
                    .setCustomGuideView(tv3)
                    .setTextGuideView(iv)
                    .setDirction(GuideView.Direction.TOP)
                    .setShape(GuideView.MyShape.CIRCULAR)   // 设置矩形显示区域，
                    .setRadius(80)          // 设置圆形或矩形透明区域半径，默认是targetView的显示矩形的半径，如果是矩形，这里是设置矩形圆角大小
                    .setBgColor(getResources().getColor(R.color.shadow))
                    .setOnclickListener(new GuideView.OnClickCallback() {
                        @Override
                        public void onClickedGuideView() {
                            guideView3.hide();
                            guideView4.show();
                        }
                    })
                    .build();
            guideView4 = GuideView.Builder
                    .newInstance(this)
                    .setTargetView(mTabLayout.getIconView(3))
                    .setCustomGuideView(tv4)
                    .setTextGuideView(iv)
                    .setDirction(GuideView.Direction.TOP)
                    .setShape(GuideView.MyShape.CIRCULAR)   // 设置矩形显示区域，
                    .setRadius(80)          // 设置圆形或矩形透明区域半径，默认是targetView的显示矩形的半径，如果是矩形，这里是设置矩形圆角大小
                    .setBgColor(getResources().getColor(R.color.shadow))
                    .setOnclickListener(new GuideView.OnClickCallback() {
                        @Override
                        public void onClickedGuideView() {
                            guideView4.hide();
                            guideView5.show();
                        }
                    })
                    .build();
            guideView5 = GuideView.Builder
                    .newInstance(this)
                    .setTargetView(mTabLayout.getIconView(4))
                    .setCustomGuideView(tv5)
                    .setTextGuideView(iv)
                    .setDirction(GuideView.Direction.TOP)
                    .setShape(GuideView.MyShape.CIRCULAR)   // 设置矩形显示区域，
                    .setRadius(80)          // 设置圆形或矩形透明区域半径，默认是targetView的显示矩形的半径，如果是矩形，这里是设置矩形圆角大小
                    .setBgColor(getResources().getColor(R.color.shadow))
                    .setOnclickListener(new GuideView.OnClickCallback() {
                        @Override
                        public void onClickedGuideView() {
                            guideView5.hide();
                            mTabLayout.setCurrentTab(4);
                            Config.bottomBarItem = 5;
                            personalCenterPage.userInfoTokenTest();//滚动到个人中心加载token是否失效
                            viewPager.setCurrentItem(4, false);
                            saveGuide("isFirst");
                        }
                    })
                    .build();
            guideView1.show();
        }
    }

    /**
     * 使用SharedPreferences保存导航页第几次
     */
    public void saveGuide(String isFirst) {
        //获取SharedPreferences对象
        SharedPreferences.Editor editor = getSharedPreferences("MainActivity", MODE_PRIVATE).edit();
        //设置参数
        editor.putString("isFirst", isFirst);
        //提交
        editor.apply();
    }

    /**
     * 是否是第一次安装
     *
     * @return
     */
    private String getGuide() {
        return getSharedPreferences("MainActivity", MODE_PRIVATE).getString("isFirst", "");
    }
    /**
     * 获取本地软件版本号名称
     */
    public static String getLocalVersionName(Context ctx) {
        String localVersion = "";
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }
}
