package zhixin.cn.com.happyfarm_user.easyMob;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.EaseConstant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.EventBus.RefreshGroup;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;

/**
 * Created by Administrator on 2018/6/14.
 */

public class GroupInfoActivity extends ABaseActivity {

    private LinearLayout deleteRecords;
    private LinearLayout modifyGroupName;
    private LinearLayout modifyGroupFriend;
    private Button dissolveBtn;
    private String groupId;
    private Dialog mDialog;
    private GroupInfoAdapter adapter;
    private ArrayList<GroupInfo> list = new ArrayList<>();
    private String groupNames;
    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerView;
    private int userId;
    private ArrayList<String> friendList = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.em_group_info_layout);

        TextView title = findViewById(R.id.title_name);
        title.setText("聊天信息");
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        EventBus.getDefault().register(this);
        Intent intent = getIntent();
        groupId = intent.getStringExtra(EaseConstant.EXTRA_USER_ID);
        deleteRecords = findViewById(R.id.delete_layout);
        dissolveBtn = findViewById(R.id.dissolve_btn);
        modifyGroupName = findViewById(R.id.modify_group_name);
        modifyGroupFriend = findViewById(R.id.modify_group_friend);
        init();
        DeleteRecords();
        modifyGroupName();
        searchGroupMembers();
        modifyGroupFriend();
        DissolveBtn();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RefreshGroup event) {
        /* Do something */
        if ("isRefresh".equals(event.getRefreshGroup())){
            if (!list.isEmpty())
                list.clear();
            if (friendList != null)
                friendList.clear();
            searchGroupMembers();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void init(){
        recyclerView = findViewById(R.id.recyclerView);
        //LinearLayoutMannager 是一个布局排列 ， 管理的接口,子类都都需要按照接口的规范来实现。
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);// 设置 recyclerview 布局方式为横向布局
        //LinearLayoutManager 种 含有3 种布局样式  第一个就是最常用的 1.横向 , 2. 竖向,3.偏移
        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new GroupInfoAdapter(GroupInfoActivity.this, list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(linearLayoutManager);
    }
    //TODO 拉取群员弹框
    public void modifyGroupFriend(){
        modifyGroupFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupInfoActivity.this,SelectContactsActivity.class);
                intent.putExtra("activity","GroupInfo");
                intent.putExtra("groupId",groupId);
                intent.putStringArrayListExtra("friendList",friendList);
                startActivity(intent);
//                ModifyGroupNameDialog("添加群成员","好友手机号");
            }
        });
    }
    //修改群名
    public void modifyGroupName(){
        modifyGroupName.setOnClickListener(v->{
            ModifyGroupNameDialog("修改群名","群组名");
        });
    }
    //解散群组
    public void DissolveBtn(){
        dissolveBtn.setOnClickListener(v->{
            if (("解散群组").equals(dissolveBtn.getText()))
                showDialog("确认解散","解散群组会导致消息记录被清空！清空以后是没有办法找回的！");
            else if (("退出群组").equals(dissolveBtn.getText()))
                showDialog("确认退出","退出群组会导致消息记录被清空！退出以后是没有办法找回的！");
        });
    }
    //删除聊天记录
    public void DeleteRecords(){
        deleteRecords.setOnClickListener(v->{
            showDialog("确认清除","您确定清除你与该群组的聊天信息吗？清除以后是没有办法找回的！");
        });
    }
    //删除好友 或者 清除聊天记录
    private void showDialog(String LblTitle,String ContentTitle) {
        final AlertUtilBest diyDialog =new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk(LblTitle)
                .setContent(ContentTitle)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        if ("确认解散".equals(LblTitle)){
                            ungroup();
                        }
                        if ("确认退出".equals(LblTitle)){
                            deleteGroup();
                        }
                        if ("确认清除".equals(LblTitle)){
                            //删除和某个user会话，如果需要保留聊天记录，传false
                            EMClient.getInstance().chatManager().deleteConversation(groupId, true);
                        }
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    //TODO 修改群组名弹框
    private void ModifyGroupNameDialog(String textMsg,String textInfoMsg) {
        AlertDialog.Builder setDeBugDialog = new AlertDialog.Builder(this);
        // AlertDialog setDeBugDialog = new AlertDialog.Builder(this).create();//创建对话框
        //获取界面
        View dialogView = LayoutInflater.from(this).inflate(R.layout.em_create_group_dialog, null);
        //获取文本框输入的值
        EditText text = dialogView.findViewById(R.id.group_name_editText);
        //将界面填充到AlertDiaLog容器
        setDeBugDialog.setView(dialogView);
        TextView textView = dialogView.findViewById(R.id.new_group_view);
        textView.setText(textMsg);
        TextView textInfo = dialogView.findViewById(R.id.group_name_view);
        textInfo.setText(textInfoMsg);
        Button button = dialogView.findViewById(R.id.group_next_btn);
        button.setText("确定");
        // 取消点击外部消失弹窗
        setDeBugDialog.setCancelable(true);
        //创建AlertDiaLog
        final AlertDialog customAlert = setDeBugDialog.show();

        customAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        customAlert.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(this, 100f), 700);
        customAlert.getWindow().setGravity(Gravity.CENTER);
        //下一步
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("修改群名".equals(textMsg)){
                    //判断是否为空
                    if (TextUtils.isEmpty(text.getText())){
                        Toast.makeText(GroupInfoActivity.this,"请输入群名称",Toast.LENGTH_SHORT).show();
                    }else {
                        groupName(text.getText().toString());
                        customAlert.dismiss();
                    }
                }else {

                }
            }
        });
        //取消
        dialogView.findViewById(R.id.group_cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customAlert.dismiss();
            }
        });
    }
    //TODO 修改群组名
    public void groupName(String groupName){
        mDialog = LoadingDialog.createLoadingDialog(GroupInfoActivity.this, null);
        HttpHelper.initHttpHelper().updateGroupInfo(groupId,groupName).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if ("success".equals(response.body().getFlag())){
                    new Handler().postDelayed(new Runnable(){
                        public void run() {
                            //execute the task
                            LoadingDialog.closeDialog(mDialog);
//                            groupNames = groupName;
                            saveGroupName(groupName);
                            //TODO 刷新群名称。
                            Toast.makeText(GroupInfoActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                        }
                    }, 500);
                }else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(GroupInfoActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                Toast.makeText(GroupInfoActivity.this,"连接超时",Toast.LENGTH_SHORT).show();
            }
        });
    }
    //TODO 退出群组
    public void deleteGroup(){
        mDialog = LoadingDialog.createLoadingDialog(GroupInfoActivity.this, null);
        HttpHelper.initHttpHelper().deleteGroup(groupId).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if ("success".equals(response.body().getFlag())){
                    new Handler().postDelayed(new Runnable(){
                        public void run() {
                            //execute the task
                            //删除和某个user会话，如果需要保留聊天记录，传false
                            EMClient.getInstance().chatManager().deleteConversation(groupId, true);
                            Toast.makeText(GroupInfoActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                            ChatActivity.destoryActivity(String.valueOf(ChatActivity.class)); //TODO 销毁上个聊天页面
                            finish();
                            LoadingDialog.closeDialog(mDialog);
                        }
                    }, 1500);
                }else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(GroupInfoActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                Toast.makeText(GroupInfoActivity.this,"连接超时",Toast.LENGTH_SHORT).show();
            }
        });
    }
    //TODO 解散群组
    public void ungroup(){
        mDialog = LoadingDialog.createLoadingDialog(GroupInfoActivity.this, null);
        HttpHelper.initHttpHelper().dissolveGroup(groupId).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if ("success".equals(response.body().getFlag())){
                    new Handler().postDelayed(new Runnable(){
                        public void run() {
                            //execute the task
                            //删除和某个user会话，如果需要保留聊天记录，传false
                            EMClient.getInstance().chatManager().deleteConversation(groupId, true);
                            Toast.makeText(GroupInfoActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                            ChatActivity.destoryActivity(String.valueOf(ChatActivity.class)); //TODO 销毁上个聊天页面
                            finish();
                            LoadingDialog.closeDialog(mDialog);
                        }
                    }, 1500);
                }else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(GroupInfoActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                Toast.makeText(GroupInfoActivity.this,"连接超时",Toast.LENGTH_SHORT).show();
            }
        });
    }
    //TODO 查询群成员
    public void searchGroupMembers(){
        mDialog = LoadingDialog.createLoadingDialog(GroupInfoActivity.this, "");
        HttpHelper.initHttpHelper().searchGroupMembers(groupId).enqueue(new Callback<searchGroup>() {
            @Override
            public void onResponse(Call<searchGroup> call, Response<searchGroup> response) {
                if ("success".equals(response.body().getFlag())){
                    if (!response.body().getResult().getListOwners().isEmpty()) {
                        for (int i = 0; i < response.body().getResult().getListOwners().size(); i++) {
                            GroupInfo dataBean = new GroupInfo();
                            dataBean.groupName  = response.body().getResult().getListOwners().get(i).getNickname() + "(群主)";
                            dataBean.groupImg  = response.body().getResult().getListOwners().get(i).getHeadImg();
                            userId = response.body().getResult().getListOwners().get(i).getUserId();
                            list.add(dataBean);
                            friendList.add(String.valueOf(userId));
                        }
                    }
//                    if (!("[]").equals(response.body().getResult().getListAdmins().size())) {
//                        for (int i = 0; i <= response.body().getResult().getListAdmins().size() - 1; i++) {
//                            GroupInfo dataBean = new GroupInfo();
//                            dataBean.groupName = response.body().getResult().getListAdmins().get(i).getNickname() + "(管理员)";
//                            dataBean.groupImg = response.body().getResult().getListAdmins().get(i).getHeadImg();
//                            list.add(dataBean);
//                        }
//                    }
                    if (!response.body().getResult().getListMembers().isEmpty()) {
                        for (int i = 0; i < response.body().getResult().getListMembers().size(); i++) {
                            GroupInfo dataBean = new GroupInfo();
                            dataBean.groupName = response.body().getResult().getListMembers().get(i).getNickname();
                            dataBean.groupImg = response.body().getResult().getListMembers().get(i).getHeadImg();
                            list.add(dataBean);
                            friendList.add(String.valueOf(response.body().getResult().getListMembers().get(i).getUserId()));
                        }
                    }
                    if (userId == getUserId())
                        dissolveBtn.setText("解散群组");
                    else
                        dissolveBtn.setText("退出群组");
                    adapter.notifyDataSetChanged();
                    LoadingDialog.closeDialog(mDialog);
                }else{
                    LoadingDialog.closeDialog(mDialog);
                }
                Log.i("GroupInfoActivity->", JSON.toJSONString(response.body()));
            }

            @Override
            public void onFailure(Call<searchGroup> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                Toast.makeText(GroupInfoActivity.this,"连接超时",Toast.LENGTH_SHORT).show();
            }
        });
    }
    private int getUserId(){
        int userId;
        if (getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }
    public void back(View view) {
        finish();
    }

    public void saveGroupName(String groupNames){
        SharedPreferences.Editor editor = getSharedPreferences("User_data",MODE_PRIVATE).edit();
        editor.putString("groupNames",groupNames);
        editor.apply();
    }

}
