package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.searchOrderlogList;
import zhixin.cn.com.happyfarm_user.utils.RoundImageView;
import zhixin.cn.com.happyfarm_user.utils.Utils;


public class WaitingForGoodsAdapter extends RecyclerView.Adapter<zhixin.cn.com.happyfarm_user.Adapter.WaitingForGoodsAdapter.StaggerViewHolder> {

    private Context myContext;
    private List<searchOrderlogList.ResultBean.ListBean> myList;
    private WaitingForGoodsAdapter.MyItemClickListener myItemClickListener;

    public WaitingForGoodsAdapter(Context context, List<searchOrderlogList.ResultBean.ListBean> list) {
        this.myContext = context;
        this.myList = list;
    }

    @NonNull
    @Override
    public zhixin.cn.com.happyfarm_user.Adapter.WaitingForGoodsAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(myContext, R.layout.waiting_for_goods_item,null);

        return new zhixin.cn.com.happyfarm_user.Adapter.WaitingForGoodsAdapter.StaggerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull zhixin.cn.com.happyfarm_user.Adapter.WaitingForGoodsAdapter.StaggerViewHolder staggerViewHolder, int i) {

        searchOrderlogList.ResultBean.ListBean listBean = myList.get(i);
        staggerViewHolder.setData(listBean);

        if (myItemClickListener != null)
            staggerViewHolder.button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = staggerViewHolder.getLayoutPosition();
                    myItemClickListener.onItemClick(view,position);
                }
            });

    }


    @Override
    public int getItemCount() {
        return myList.size();
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private TextView orderTime;
        private RoundImageView imageView;
        private TextView textView1;
        private TextView textView2;
        private TextView textView3;
        private TextView textView4;
        private TextView button1;
        private TextView button2;
        private TextView order_number;
        private TextView allPrice;


        public StaggerViewHolder(@NonNull View itemView) {
            super(itemView);
            orderTime = itemView.findViewById(R.id.order_time);
            imageView = itemView.findViewById(R.id.waiting_for_goods_image);
            textView1 = itemView.findViewById(R.id.waiting_for_goods_name);
            textView2 = itemView.findViewById(R.id.waiting_for_goods_number);
            textView3 = itemView.findViewById(R.id.waiting_for_goods_price);
            textView4 = itemView.findViewById(R.id.waiting_for_goods_username);
            allPrice = itemView.findViewById(R.id.waiting_for_goods_all_price);
            button1 = itemView.findViewById(R.id.transport);
            button2 = itemView.findViewById(R.id.view_number);
            order_number = itemView.findViewById(R.id.order_number);
        }

        public void setData(searchOrderlogList.ResultBean.ListBean data) {
            Log.i("查到的代发货订单", "setData: " + JSON.toJSONString(data));
            orderTime.setText("订单日期：" + Utils.getDateToString(data.getStartTime()));
            imageView.setImageURL(data.getOrderImg());
            textView1.setText("商品名：" + data.getCropName());
            textView2.setText("发货数量："+data.getClosing()+"份");
            textView3.setText(data.getPrice()+"元/份");
            textView4.setText("卖家："+data.getSellerNickname());
            order_number.setText("订单编号："+data.getOrderNum());
            allPrice.setText(data.getFinalPrice() + "元");
            if (2 == data.getState()) {
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.GONE);
            } else if (3 == data.getState()) {
                button1.setVisibility(View.GONE);
                button2.setVisibility(View.VISIBLE);
            }
        }
    }

    public interface MyItemClickListener {
        void onItemClick(View view,int position);
    }

    public void  setItemClickListener (zhixin.cn.com.happyfarm_user.Adapter.WaitingForGoodsAdapter.MyItemClickListener myItemClickListener) {
        this.myItemClickListener = myItemClickListener;
    }
}
