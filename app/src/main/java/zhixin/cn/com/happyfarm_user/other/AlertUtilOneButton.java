package zhixin.cn.com.happyfarm_user.other;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import zhixin.cn.com.happyfarm_user.R;

/**
 * AlertUtilOneButton
 *
 * @author: Administrator.
 * @date: 2019/5/10
 */

public class AlertUtilOneButton {
    private Context context;
    private Dialog dialog;

    private String content;

    private String ok;

    private DialogClickListener dialogClickListener;

    public void setDialogClickListener(DialogClickListener dialogClickListener) {
        this.dialogClickListener = dialogClickListener;
    }

    public AlertUtilOneButton setContent(String content) {
        this.content = content;
        return this;
    }

    public AlertUtilOneButton setOk(String ok) {
        this.ok = ok;
        return this;
    }

    public AlertUtilOneButton(Context context) {
        this.context = context;
    }

    public AlertUtilOneButton builder() {
        dialog = new Dialog(context, R.style.dialog);
        View view = LayoutInflater.from(context).inflate(
                R.layout.layout_alert_1button, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        //设置左右间距
        params.width = context.getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(context, 100f);
        params.bottomMargin = DensityUtil.dp2px(context, 0f);

        TextView tv_dialog_content = (TextView) view.findViewById(R.id.user_info_content);

        TextView tv_ok = (TextView) view.findViewById(R.id.lease_immediately_bt);
        if (!TextUtils.isEmpty(content))
            tv_dialog_content.setText(content);

        if (!TextUtils.isEmpty(ok))
            tv_ok.setText(ok);

//        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                Log.e("Dialog", "onCancel: "+"消失");
//            }
//        });

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogClickListener != null)
                    dialogClickListener.ok();
            }
        });
        view.setLayoutParams(params);
        return this;
    }

    public void show() {
        if (dialog != null)
            dialog.show();
    }

    public void cancle() {
        if (dialog != null)
            dialog.cancel();
    }

    public interface DialogClickListener {

        void ok();
    }
}
