package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import zhixin.cn.com.happyfarm_user.MyImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Crop;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by DELL on 2018/3/16.
 */

public class CropAdapter extends RecyclerView.Adapter<CropAdapter.StaggerViewHolder>{

    private CropAdapter.MyItemClickListener mItemClickListener;
    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<Crop> mList;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public CropAdapter(Context context, List<Crop> list) {
        mContext = context;
        mList = list;
//        for (Crop name : list) {
//            Log.i(name.cropName, "CropAdapter: ");
//        }
    }

    @NonNull
    @Override
    //创建staggerViewHolder，并把ViewHolder返回出去
    public CropAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.crop_item, null);
        //创建一个staggerViewHolder对象
        //CropAdapter.StaggerViewHolder staggerViewHolder = new CropAdapter.StaggerViewHolder(view);
        //把staggerViewHolder对象传出去
        return new CropAdapter.StaggerViewHolder(view);
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(@NonNull CropAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        Crop dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
            holder.setData(dataBean);
            if (mItemClickListener != null){
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = holder.getLayoutPosition();
                        mItemClickListener.onItemClick(view,pos);

                    }
                });
            }
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if(mList!=null&&mList.size()>0){
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final MyImageView mIcon;
        private final TextView mCropName;
        private final TextView mCycle;
        private final TextView mStartSowing;
        private final TextView mNutritionalComponents;
        private final RelativeLayout noPlanting;
        public StaggerViewHolder(View itemView) {
            super(itemView);
            mIcon = itemView.findViewById(R.id.crop_image);
            mCropName = itemView.findViewById(R.id.crop_name_view);
            mCycle = itemView.findViewById(R.id.cycle);
            mStartSowing = itemView.findViewById(R.id.start_sowing);
            mNutritionalComponents = itemView.findViewById(R.id.nutritional_components);
            noPlanting = itemView.findViewById(R.id.no_planting);
        }

        public void setData(Crop data) {
            //给ImageView设置图片数据
//            mIcon.setImageResource(data.imageId);
            if (1 == data.state){
                noPlanting.setVisibility(View.GONE);
            } else {
                noPlanting.setVisibility(View.VISIBLE);
            }
            if (NumUtil.checkNull(data.imageUrl)){
                mIcon.setImageResource(R.drawable.maintain);
            }else {
                Glide.with(mContext)
                        .load(data.imageUrl)
                        .into(mIcon);
            }
            mCropName.setText(data.cropName);

            if (0 != data.cycle) {
                mCycle.setText("生长周期："+data.cycle+"个月");
            } else {
                mCycle.setText("生长周期：正在收集");
            }

            if (0 != data.startSowing || 0 != data.endSeeding) {
                SimpleDateFormat sdf1 = new SimpleDateFormat("MM 月 dd 日");
                String sd1 = sdf1.format(new Date(Long.parseLong(String.valueOf(data.startSowing))));
                SimpleDateFormat sdf2 = new SimpleDateFormat("MM 月 dd 日");
                String sd2 = sdf2.format(new Date(Long.parseLong(String.valueOf(data.endSeeding))));
                mStartSowing.setText("适宜种植时间："+sd1+" ~ "+sd2);
            } else {
                mStartSowing.setText("适宜种植时间：正在收集");
            }

            if (null != data.nutritionalComponents) {
                mNutritionalComponents.setText("营养价值：" + data.nutritionalComponents);
            } else {
                mNutritionalComponents.setText("营养价值：正在收集中" );
            }


        }

    }

    /**
     * 创建一个回调接口
     */
    public interface MyItemClickListener {
        void onItemClick(View view, int position);
    }

    /**
     * 在activity里面adapter就是调用的这个方法,将点击事件监听传递过来,并赋值给全局的监听
     *
     * @param myItemClickListener
     */
    public void setItemClickListener(CropAdapter.MyItemClickListener myItemClickListener) {
        this.mItemClickListener = myItemClickListener;
    }

}
