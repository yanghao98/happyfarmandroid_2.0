package zhixin.cn.com.happyfarm_user.model;

import com.google.gson.Gson;

import java.util.List;

public class SelectLandCollection {


    /**
     * result : {"pageNum":1,"pageSize":5,"size":5,"orderBy":null,"startRow":0,"endRow":4,"total":5,"pages":1,"list":[{"id":31,"userId":7,"landId":94,"startTime":1524452062000,"nickname":"123","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/headid1525955745","landNo":94,"state":1,"landUserId":18,"leaseTime":1524441600000},{"id":207,"userId":7,"landId":50,"startTime":1525674764000,"nickname":"育碧程序员","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_111525675571526","landNo":50,"state":1,"landUserId":11,"leaseTime":1524206268000},{"id":208,"userId":7,"landId":180,"startTime":1525677327000,"nickname":"十个九坑a","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/headid1526260680","landNo":180,"state":1,"landUserId":35,"leaseTime":1524634350000},{"id":210,"userId":7,"landId":244,"startTime":1525685767000,"nickname":"Jdld","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_31524837367918","landNo":5,"state":1,"landUserId":3,"leaseTime":1524528000000},{"id":211,"userId":7,"landId":128,"startTime":1525685773000,"nickname":"18270685275","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_531524918274258","landNo":128,"state":1,"landUserId":53,"leaseTime":1524917911000}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public static SelectLandCollection objectFromData(String str) {

        return new Gson().fromJson(str, SelectLandCollection.class);
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 5
         * size : 5
         * orderBy : null
         * startRow : 0
         * endRow : 4
         * total : 5
         * pages : 1
         * list : [{"id":31,"userId":7,"landId":94,"startTime":1524452062000,"nickname":"123","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/headid1525955745","landNo":94,"state":1,"landUserId":18,"leaseTime":1524441600000},{"id":207,"userId":7,"landId":50,"startTime":1525674764000,"nickname":"育碧程序员","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_111525675571526","landNo":50,"state":1,"landUserId":11,"leaseTime":1524206268000},{"id":208,"userId":7,"landId":180,"startTime":1525677327000,"nickname":"十个九坑a","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/headid1526260680","landNo":180,"state":1,"landUserId":35,"leaseTime":1524634350000},{"id":210,"userId":7,"landId":244,"startTime":1525685767000,"nickname":"Jdld","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_31524837367918","landNo":5,"state":1,"landUserId":3,"leaseTime":1524528000000},{"id":211,"userId":7,"landId":128,"startTime":1525685773000,"nickname":"18270685275","headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_531524918274258","landNo":128,"state":1,"landUserId":53,"leaseTime":1524917911000}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public static ResultBean objectFromData(String str) {

            return new Gson().fromJson(str, ResultBean.class);
        }

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 31
             * userId : 7
             * landId : 94
             * startTime : 1524452062000
             * nickname : 123
             * headImg : http://7xtaye.com2.z0.glb.clouddn.com/headid1525955745
             * landNo : 94
             * state : 1
             * landUserId : 18
             * leaseTime : 1524441600000
             */

            private int id;
            private int userId;
            private int landId;
            private long startTime;
            private String nickname;
            private String headImg;
            private int landNo;
            private int state;
            private int landUserId;
            private long leaseTime;
            private int landType;
            private String tel;

            public static ListBean objectFromData(String str) {

                return new Gson().fromJson(str, ListBean.class);
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getLandId() {
                return landId;
            }

            public void setLandId(int landId) {
                this.landId = landId;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getHeadImg() {
                return headImg;
            }

            public void setHeadImg(String headImg) {
                this.headImg = headImg;
            }

            public int getLandNo() {
                return landNo;
            }

            public void setLandNo(int landNo) {
                this.landNo = landNo;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public int getLandUserId() {
                return landUserId;
            }

            public void setLandUserId(int landUserId) {
                this.landUserId = landUserId;
            }

            public String getTel() {
                return tel;
            }

            public void setTel(String tel) {
                this.tel = tel;
            }

            public long getLeaseTime() {
                return leaseTime;
            }

            public void setLeaseTime(long leaseTime) {
                this.leaseTime = leaseTime;
            }

            public int getLandType() {
                return landType;
            }

            public void setLandType(int landType) {
                this.landType = landType;
            }
        }
    }
}
