package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * Created by Administrator on 2018/6/25.
 */

public class AllUserInfo {

    /**
     * result : {"pageNum":1,"pageSize":2,"size":2,"orderBy":null,"startRow":1,"endRow":2,"total":20,"pages":10,"list":[{"id":22,"uuid":"f5244d80-781a-11e8-a948-83bcc59539f3","nickname":"13771438778","password":null,"headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head1529898810","tel":"13771438778","email":"qwer@qq.com","realname":"qwer","gender":1,"birthday":-28800000,"plotId":null,"plotName":null,"receiverName":null,"receiverTel":null,"certificates":1,"certificatesNo":"321023111122223333","diamond":6,"integral":10,"exp":0,"isLandlord":1,"registerTime":1529891781000,"state":1,"landId":null,"landNo":null},{"id":23,"uuid":"381a2570-7829-11e8-a721-171b3adf63da","nickname":"15852789007","password":null,"headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head231529907314","tel":"15852789007","email":"1234@qq.con","realname":"@@@","gender":1,"birthday":1529856000000,"plotId":null,"plotName":null,"receiverName":null,"receiverTel":null,"certificates":1,"certificatesNo":"123456111122223333","diamond":30,"integral":10,"exp":0,"isLandlord":1,"registerTime":1529897906000,"state":1,"landId":null,"landNo":null}],"firstPage":1,"prePage":0,"nextPage":2,"lastPage":8,"isFirstPage":true,"isLastPage":false,"hasPreviousPage":false,"hasNextPage":true,"navigatePages":8,"navigatepageNums":[1,2,3,4,5,6,7,8]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 2
         * size : 2
         * orderBy : null
         * startRow : 1
         * endRow : 2
         * total : 20
         * pages : 10
         * list : [{"id":22,"uuid":"f5244d80-781a-11e8-a948-83bcc59539f3","nickname":"13771438778","password":null,"headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head1529898810","tel":"13771438778","email":"qwer@qq.com","realname":"qwer","gender":1,"birthday":-28800000,"plotId":null,"plotName":null,"receiverName":null,"receiverTel":null,"certificates":1,"certificatesNo":"321023111122223333","diamond":6,"integral":10,"exp":0,"isLandlord":1,"registerTime":1529891781000,"state":1,"landId":null,"landNo":null},{"id":23,"uuid":"381a2570-7829-11e8-a721-171b3adf63da","nickname":"15852789007","password":null,"headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head231529907314","tel":"15852789007","email":"1234@qq.con","realname":"@@@","gender":1,"birthday":1529856000000,"plotId":null,"plotName":null,"receiverName":null,"receiverTel":null,"certificates":1,"certificatesNo":"123456111122223333","diamond":30,"integral":10,"exp":0,"isLandlord":1,"registerTime":1529897906000,"state":1,"landId":null,"landNo":null}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 2
         * lastPage : 8
         * isFirstPage : true
         * isLastPage : false
         * hasPreviousPage : false
         * hasNextPage : true
         * navigatePages : 8
         * navigatepageNums : [1,2,3,4,5,6,7,8]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 22
             * uuid : f5244d80-781a-11e8-a948-83bcc59539f3
             * nickname : 13771438778
             * password : null
             * headImg : http://7xtaye.com2.z0.glb.clouddn.com/head1529898810
             * tel : 13771438778
             * email : qwer@qq.com
             * realname : qwer
             * gender : 1
             * birthday : -28800000
             * plotId : null
             * plotName : null
             * receiverName : null
             * receiverTel : null
             * certificates : 1
             * certificatesNo : 321023111122223333
             * diamond : 6
             * integral : 10
             * exp : 0
             * isLandlord : 1
             * registerTime : 1529891781000
             * state : 1
             * landId : null
             * landNo : null
             */

            private int id;
            private String uuid;
            private String nickname;
            private Object password;
            private String headImg;
            private String tel;
            private String email;
            private String realname;
            private int gender;
            private int birthday;
            private Object plotId;
            private Object plotName;
            private Object receiverName;
            private Object receiverTel;
            private int certificates;
            private String certificatesNo;
            private int diamond;
            private int integral;
            private int exp;
            private int isLandlord;
            private long registerTime;
            private int state;
            private Object landId;
            private Object landNo;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public Object getPassword() {
                return password;
            }

            public void setPassword(Object password) {
                this.password = password;
            }

            public String getHeadImg() {
                return headImg;
            }

            public void setHeadImg(String headImg) {
                this.headImg = headImg;
            }

            public String getTel() {
                return tel;
            }

            public void setTel(String tel) {
                this.tel = tel;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getRealname() {
                return realname;
            }

            public void setRealname(String realname) {
                this.realname = realname;
            }

            public int getGender() {
                return gender;
            }

            public void setGender(int gender) {
                this.gender = gender;
            }

            public int getBirthday() {
                return birthday;
            }

            public void setBirthday(int birthday) {
                this.birthday = birthday;
            }

            public Object getPlotId() {
                return plotId;
            }

            public void setPlotId(Object plotId) {
                this.plotId = plotId;
            }

            public Object getPlotName() {
                return plotName;
            }

            public void setPlotName(Object plotName) {
                this.plotName = plotName;
            }

            public Object getReceiverName() {
                return receiverName;
            }

            public void setReceiverName(Object receiverName) {
                this.receiverName = receiverName;
            }

            public Object getReceiverTel() {
                return receiverTel;
            }

            public void setReceiverTel(Object receiverTel) {
                this.receiverTel = receiverTel;
            }

            public int getCertificates() {
                return certificates;
            }

            public void setCertificates(int certificates) {
                this.certificates = certificates;
            }

            public String getCertificatesNo() {
                return certificatesNo;
            }

            public void setCertificatesNo(String certificatesNo) {
                this.certificatesNo = certificatesNo;
            }

            public int getDiamond() {
                return diamond;
            }

            public void setDiamond(int diamond) {
                this.diamond = diamond;
            }

            public int getIntegral() {
                return integral;
            }

            public void setIntegral(int integral) {
                this.integral = integral;
            }

            public int getExp() {
                return exp;
            }

            public void setExp(int exp) {
                this.exp = exp;
            }

            public int getIsLandlord() {
                return isLandlord;
            }

            public void setIsLandlord(int isLandlord) {
                this.isLandlord = isLandlord;
            }

            public long getRegisterTime() {
                return registerTime;
            }

            public void setRegisterTime(long registerTime) {
                this.registerTime = registerTime;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public Object getLandId() {
                return landId;
            }

            public void setLandId(Object landId) {
                this.landId = landId;
            }

            public Object getLandNo() {
                return landNo;
            }

            public void setLandNo(Object landNo) {
                this.landNo = landNo;
            }
        }
    }
}
