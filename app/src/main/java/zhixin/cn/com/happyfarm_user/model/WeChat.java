package zhixin.cn.com.happyfarm_user.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HuYueling on 2018/4/26.
 */

public class WeChat {

    /**
     * result : {"appid":"wxc646adf8f926e4ed","noncestr":"ZEK2QsHGEku2sWwnrtH8usn8KgAZmlxW","package":"Sign=WXPay","partnerid":"1502161451","prepayid":"wx26204006319454eee3021c932537579535","sign":"D5B22807F1E8EEAC3A0DCD314CC72789","timestamp":1524746400}
     * flag : success
     */

    private ResultBean result;
    private String flag;
    private String message;
    private int code;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static class ResultBean {
        /**
         * appid : wxc646adf8f926e4ed
         * noncestr : ZEK2QsHGEku2sWwnrtH8usn8KgAZmlxW
         * package : Sign=WXPay
         * partnerid : 1502161451
         * prepayid : wx26204006319454eee3021c932537579535
         * sign : D5B22807F1E8EEAC3A0DCD314CC72789
         * timestamp : 1524746400
         */

        private String appid;
        private String noncestr;
        @SerializedName("package")
        private String packageX;
        private String partnerid;
        private String prepayid;
        private String sign;
        private int timestamp;

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getNoncestr() {
            return noncestr;
        }

        public void setNoncestr(String noncestr) {
            this.noncestr = noncestr;
        }

        public String getPackageX() {
            return packageX;
        }

        public void setPackageX(String packageX) {
            this.packageX = packageX;
        }

        public String getPartnerid() {
            return partnerid;
        }

        public void setPartnerid(String partnerid) {
            this.partnerid = partnerid;
        }

        public String getPrepayid() {
            return prepayid;
        }

        public void setPrepayid(String prepayid) {
            this.prepayid = prepayid;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }
    }
}
