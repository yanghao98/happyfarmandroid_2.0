package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class SeachFriend {

    /**
     * result : {"pageNum":1,"pageSize":10,"size":10,"orderBy":null,"startRow":0,"endRow":9,"total":10,"pages":1,"list":[{"id":64,"userId":12,"friendId":14,"friendNickname":"张","toId":14,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_14_1528166021432","uuid":null,"tel":"17621978355","state":1},{"id":76,"userId":12,"friendId":42,"friendNickname":"me","toId":42,"friendHeadImg":"","uuid":null,"tel":"17625018173","state":1},{"id":80,"userId":12,"friendId":18,"friendNickname":"1","toId":18,"friendHeadImg":"","uuid":null,"tel":"11111111111","state":1},{"id":100,"userId":12,"friendId":39,"friendNickname":"\"野猪佩奇1\u201d","toId":39,"friendHeadImg":"","uuid":null,"tel":"13103399787","state":1},{"id":102,"userId":12,"friendId":51,"friendNickname":"12305","toId":51,"friendHeadImg":"","uuid":null,"tel":"18915299392","state":1},{"id":37,"userId":4,"friendId":12,"friendNickname":"小哒哒","toId":4,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_4_1528164826828","uuid":null,"tel":"18360632509","state":1},{"id":84,"userId":84,"friendId":12,"friendNickname":"13196560702","toId":84,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_84_1528106282166","uuid":null,"tel":"13196560702","state":1},{"id":88,"userId":2,"friendId":12,"friendNickname":"use2","toId":2,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_21526637577256","uuid":null,"tel":"18621969442","state":1},{"id":107,"userId":74,"friendId":12,"friendNickname":"17605249527","toId":74,"friendHeadImg":"","uuid":"ca6b14b0-5cc8-11e8-b6c2-319a0078d56b","tel":"17605249527","state":1},{"id":111,"userId":75,"friendId":12,"friendNickname":"13456156795","toId":75,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_751528095681585","uuid":"ab2d3d70-5d91-11e8-8d95-5d926d6cae07","tel":"13456156795","state":1}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 10
         * size : 10
         * orderBy : null
         * startRow : 0
         * endRow : 9
         * total : 10
         * pages : 1
         * list : [{"id":64,"userId":12,"friendId":14,"friendNickname":"张","toId":14,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_14_1528166021432","uuid":null,"tel":"17621978355","state":1},{"id":76,"userId":12,"friendId":42,"friendNickname":"me","toId":42,"friendHeadImg":"","uuid":null,"tel":"17625018173","state":1},{"id":80,"userId":12,"friendId":18,"friendNickname":"1","toId":18,"friendHeadImg":"","uuid":null,"tel":"11111111111","state":1},{"id":100,"userId":12,"friendId":39,"friendNickname":"\"野猪佩奇1\u201d","toId":39,"friendHeadImg":"","uuid":null,"tel":"13103399787","state":1},{"id":102,"userId":12,"friendId":51,"friendNickname":"12305","toId":51,"friendHeadImg":"","uuid":null,"tel":"18915299392","state":1},{"id":37,"userId":4,"friendId":12,"friendNickname":"小哒哒","toId":4,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_4_1528164826828","uuid":null,"tel":"18360632509","state":1},{"id":84,"userId":84,"friendId":12,"friendNickname":"13196560702","toId":84,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_84_1528106282166","uuid":null,"tel":"13196560702","state":1},{"id":88,"userId":2,"friendId":12,"friendNickname":"use2","toId":2,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_21526637577256","uuid":null,"tel":"18621969442","state":1},{"id":107,"userId":74,"friendId":12,"friendNickname":"17605249527","toId":74,"friendHeadImg":"","uuid":"ca6b14b0-5cc8-11e8-b6c2-319a0078d56b","tel":"17605249527","state":1},{"id":111,"userId":75,"friendId":12,"friendNickname":"13456156795","toId":75,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head_751528095681585","uuid":"ab2d3d70-5d91-11e8-8d95-5d926d6cae07","tel":"13456156795","state":1}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 64
             * userId : 12
             * friendId : 14
             * friendNickname : 张
             * toId : 14
             * friendHeadImg : http://7xtaye.com2.z0.glb.clouddn.com/headImg_14_1528166021432
             * uuid : null
             * tel : 17621978355
             * state : 1
             */

            private int id;
            private int userId;
            private int friendId;
            private String friendNickname;
            private int toId;
            private String friendHeadImg;
            private Object uuid;
            private String tel;
            private int state;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getFriendId() {
                return friendId;
            }

            public void setFriendId(int friendId) {
                this.friendId = friendId;
            }

            public String getFriendNickname() {
                return friendNickname;
            }

            public void setFriendNickname(String friendNickname) {
                this.friendNickname = friendNickname;
            }

            public int getToId() {
                return toId;
            }

            public void setToId(int toId) {
                this.toId = toId;
            }

            public String getFriendHeadImg() {
                return friendHeadImg;
            }

            public void setFriendHeadImg(String friendHeadImg) {
                this.friendHeadImg = friendHeadImg;
            }

            public Object getUuid() {
                return uuid;
            }

            public void setUuid(Object uuid) {
                this.uuid = uuid;
            }

            public String getTel() {
                return tel;
            }

            public void setTel(String tel) {
                this.tel = tel;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }
        }
    }
}
