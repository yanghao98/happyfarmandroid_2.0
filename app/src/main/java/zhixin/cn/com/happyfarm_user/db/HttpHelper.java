package zhixin.cn.com.happyfarm_user.db;

import android.util.Log;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.easyMob.SearchMyApply;
import zhixin.cn.com.happyfarm_user.easyMob.searchGroup;
import zhixin.cn.com.happyfarm_user.model.AdSeat;
import zhixin.cn.com.happyfarm_user.model.AliPay;
import zhixin.cn.com.happyfarm_user.model.AllLand;
import zhixin.cn.com.happyfarm_user.model.AllUserInfo;
import zhixin.cn.com.happyfarm_user.model.AmountConfiguration;
import zhixin.cn.com.happyfarm_user.model.AreaAddress;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.CheckSMSByResultIsList;
import zhixin.cn.com.happyfarm_user.model.Cooking;
import zhixin.cn.com.happyfarm_user.model.CookingInfo;
import zhixin.cn.com.happyfarm_user.model.CreateActivity;
import zhixin.cn.com.happyfarm_user.model.CropGoodsDetails;
import zhixin.cn.com.happyfarm_user.model.CusChat;
import zhixin.cn.com.happyfarm_user.model.Default;
import zhixin.cn.com.happyfarm_user.model.DishCategory;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.GetVideoSize;
import zhixin.cn.com.happyfarm_user.model.HelpList;
import zhixin.cn.com.happyfarm_user.model.ISRegister;
import zhixin.cn.com.happyfarm_user.model.InvitationFriend;
import zhixin.cn.com.happyfarm_user.model.LandHistoryPic;
import zhixin.cn.com.happyfarm_user.model.LandInfoCardItem;
import zhixin.cn.com.happyfarm_user.model.LatelyTrack;
import zhixin.cn.com.happyfarm_user.model.LoginList;
import zhixin.cn.com.happyfarm_user.model.LuckDraw;
import zhixin.cn.com.happyfarm_user.model.LuckDrawAddress;
import zhixin.cn.com.happyfarm_user.model.LuckDrawDeductionAmount;
import zhixin.cn.com.happyfarm_user.model.LuckDrawModel;
import zhixin.cn.com.happyfarm_user.model.LuckDrawRecord;
import zhixin.cn.com.happyfarm_user.model.PlotPlantingStandard;
import zhixin.cn.com.happyfarm_user.model.PublicKeyList;
import zhixin.cn.com.happyfarm_user.model.PushToken;
import zhixin.cn.com.happyfarm_user.model.QiNiu;
import zhixin.cn.com.happyfarm_user.model.RegisterList;
import zhixin.cn.com.happyfarm_user.model.SeachConfig;
import zhixin.cn.com.happyfarm_user.model.SeachFriend;
import zhixin.cn.com.happyfarm_user.model.SeachTrack;
import zhixin.cn.com.happyfarm_user.model.SearchAllGoods;
import zhixin.cn.com.happyfarm_user.model.SearchCropList;
import zhixin.cn.com.happyfarm_user.model.SearchDiamondBag;
import zhixin.cn.com.happyfarm_user.model.SearchIrrationInfo;
import zhixin.cn.com.happyfarm_user.model.SearchLandByNo;
import zhixin.cn.com.happyfarm_user.model.SearchLandByType;
import zhixin.cn.com.happyfarm_user.model.SearchMaintainList;
import zhixin.cn.com.happyfarm_user.model.SearchPhotoAlbum;
import zhixin.cn.com.happyfarm_user.model.SearchPlotManagements;
import zhixin.cn.com.happyfarm_user.model.SearchTutorial;
import zhixin.cn.com.happyfarm_user.model.SearchTutorialContent;
import zhixin.cn.com.happyfarm_user.model.SearchUserLandcrop;
import zhixin.cn.com.happyfarm_user.model.SelectLandCollection;
import zhixin.cn.com.happyfarm_user.model.ShoppingCart;
import zhixin.cn.com.happyfarm_user.model.ShoppingCartNum;
import zhixin.cn.com.happyfarm_user.model.SignIn;
import zhixin.cn.com.happyfarm_user.model.SignMonthRecord;
import zhixin.cn.com.happyfarm_user.model.TermsModel;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.model.TransactionDetails;
import zhixin.cn.com.happyfarm_user.model.WeChat;
import zhixin.cn.com.happyfarm_user.model.friendRecommendation;
import zhixin.cn.com.happyfarm_user.model.searchDetail;
import zhixin.cn.com.happyfarm_user.model.searchFriendDetail;
import zhixin.cn.com.happyfarm_user.model.searchMyGood;
import zhixin.cn.com.happyfarm_user.model.searchOrderlogList;
import zhixin.cn.com.happyfarm_user.model.searchReceivingAddress;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Administrator on 2017/8/22.
 */

public class HttpHelper {

    private static HttpHelper httpHelper = new HttpHelper();

    private HttpHelper() {
    }

    public static HttpHelper initHttpHelper() {
        return httpHelper;
    }

    private OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)//链接超时
            .writeTimeout(30, TimeUnit.SECONDS)//写入超时
            .readTimeout(30, TimeUnit.SECONDS)//读取超时
            .addNetworkInterceptor(new StethoInterceptor())//网络拦截器
            .build();
    private Retrofit retrofit = new Retrofit.Builder()
//            .baseUrl("http://192.168.50.112:9890/api/")
            .baseUrl("http://49.84.226.78:9890/api/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();

    private Retrofit aaa = new Retrofit.Builder()
            .baseUrl("http://61.132.81.59:8866/api/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();

    //用户测试制作相册
//    private Retrofit aaa1 = new Retrofit.Builder()
//            .baseUrl("http://49.84.226.78:9890/api/")
//            .addConverterFactory(ScalarsConverterFactory.create())
//            .addConverterFactory(GsonConverterFactory.create())
//            .client(client)
//            .build();

    private HttpApi chickenCoopApi = aaa.create(HttpApi.class);

    private HttpApi api = retrofit.create(HttpApi.class);

//    private HttpApi testApi = aaa1.create(HttpApi.class);


    private String getToken(){
        return App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("token","");
    }
    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }

//    public retrofit2.Call<LandList> searchLandList(int pageNum, int pageSize){
//        return api.searchLandList(pageNum,pageSize);w
//    }

    public retrofit2.Call<RegisterList> register(String nickname, String password, String tel, String registerIp){
        return api.register(nickname,password,tel,registerIp);
    }

    public  retrofit2.Call<PublicKeyList> publicKey(String userInfo){
        return api.publicKey(userInfo);
    }

    public retrofit2.Call<LoginList> login(String tel, String pwd,String loginIp){
        return api.login(tel,pwd,loginIp);
    }

    public retrofit2.Call<SearchDiamondBag> searchDiamondBag(){
        return api.searchDiamondBag(getToken());
    }

    public retrofit2.Call<SearchMaintainList> searchMaintainList(Integer maintainType){
        return api.searchMaintainList(maintainType);
    }

    public Call<GetSelfInfo> getSelfInfo(){
        Log.e("LandVideoNo",getToken());
        return api.getSelfInfo(getToken());
    }

    public Call<CheckSMS> getSMS(String tel,String info){
        return api.getSMS(tel,info);
    }

    public Call<ISRegister> isRegister(String tel){
        return api.isRegister(tel);
    }

    public Call<CheckSMS> checkSMS(String tel, String captcha){
        return api.checkSMS(tel,captcha);
    }

    public Call<CheckSMS> resetPassword(String tel,String pwd){
        return api.resetPassword(tel,pwd);
    }

    public Call<SearchCropList> searchCropList(String cropName, Integer cropType){
        return api.searchCropList(cropName, cropType);
    }

    public Call<SearchCropList> searchAllCropList(){
        return api.searchAllCropList();
    }

    public Call<SearchUserLandcrop> searchUserLandcrop(int landId, Integer userId){
        return api.searchUserLandcrop(landId, userId);
    }

    public Call<SearchTutorial> searchTutorial() {
        return api.searchTutorial();
    }
    public Call<SearchTutorial> LandVideoSearchTutorial() {
        return api.LandVideoSearchTutorial(getToken());
    }

    public Call<SearchTutorialContent> searchTutorialContent(int tutorialid){
        return api.searchTutorialContent(tutorialid);
    }

    public Call<AllLand> landList() {
        return api.landList(getToken());
    }

    public Call<AllLand> landListByLandId(int landId) {
        return api.landListByLandId(getToken(),landId);
    }

    public Call<CheckSMS> addUserTask(int payType, int command, int sendBackNum, String sellNum, int landId, int cropId, int cropNum){
        Log.e("response",getToken());
        Log.e("response",String.valueOf(payType));
        Log.e("response",String.valueOf(command));
        Log.e("response",String.valueOf(sendBackNum));
        Log.e("response",String.valueOf(sellNum));
        Log.e("response",String.valueOf(landId));
        Log.e("response",String.valueOf(cropId));
        Log.e("response",String.valueOf(cropNum));
        return api.addUserTask(getToken(),payType,command,sendBackNum,sellNum,landId,cropId,cropNum);
    }

    public Call<SearchLandByNo> searchLandByNo(int landId){
        return api.searchLandByNo(getToken(),landId);
    }

    public Call<CheckSMS> hostingLand(String default_harvest,int is_hosted,int landNo){
        return api.hostingLand(getToken(),default_harvest,is_hosted,landNo);
    }

    public Call<CheckSMS> addFriend(int friendId){
        return api.addFriend(getToken(),friendId);
    }

    public Call<CheckSMS> addCollect(int userId,int landId){
        return api.addCollect(getToken(),userId,landId);
    }

    public Call<SelectLandCollection> selectLandCollection(int pageNum,int pageSize){
        return api.selectLandCollection(getToken(),pageNum,pageSize);
    }

    public Call<SelectLandCollection> selectLandCollectionVideo(){
        return api.selectLandCollectionVideo(getToken());
    }

    public Call<CheckSMS> deleteCollect(int userId,int landId){
        return api.deleteCollect(getToken(),userId,landId);
    }
    public Call<LandHistoryPic> landHistroy(String startTime, String endTime, int num){
        return api.landHistroy(getToken(),"",startTime,endTime,num);

    }

    public Call<SearchPlotManagements> searchPlotManagements(){
        Log.e("token",getToken());
        return api.searchPlotManagements(getToken());
    }


    public Call<SearchIrrationInfo> searchIrrationInfo(int pageNum,int pageSize) {
        return api.searchIrrationInfo(getToken(),pageNum,pageSize);
    }
    //完善用户信息
    public Call<CheckSMS> updateUserInfo(String nickname,int gender,String birthday,String realname,String certificatesNo,String email){
        return api.updateUserInfo(getToken(),nickname,gender,birthday,realname,certificatesNo,email);
    }

    public Call<CheckSMS> addUserTasks(int payType,int command,int sendBackNum,String sellNum,double storeSale,int landId,int cropId,int plotId){
        return api.addUserTasks(getToken(),payType,command,sendBackNum,sellNum,storeSale,landId,cropId,plotId);
    }
    public Call<AmountConfiguration> searchAmountConfig(String lease,int landType){
        return api.searchAmountConfig(getToken(),lease,landType);
    }

    public Call<CheckSMS> addOperation(int payType,int command,int sendBackNum,String sellNum,int landId,int maintainId){
        return api.addOperation(getToken(),payType,command,sendBackNum,sellNum,landId,maintainId);
    }

    public Call<SeachTrack> seachTrack(int pageNum,int pageSize){
        return api.seachTrack(getToken(),pageNum,pageSize);
    }

    public Call<CheckSMS> loginOut(){
        return api.loginOut(getToken());
    }

    public Call<CheckSMS> addPayment(int amount,String payType,String payState,int userId,String scene){
        return api.addPayment(getToken(),amount,payType,payState,userId,scene);
    }

    public Call<CheckSMS> buyLand(int rent,int leaseTerm,int landNo){
        return api.buyLand(getToken(),rent,leaseTerm,landNo);
    }

    public Call<CheckSMS> updateUserInfos(int plotId){
//        Log.e("地址。。",getToken() + "  "+String.valueOf(plotId));
        return api.updateUserInfos(getToken(),plotId);
    }

    public Call<Test> test(){
        return api.test(getToken());
    }

    public Call<SearchLandByNo> searchLandByNos(int landNo){
        Log.e("response1",getToken());
        Log.e("response1",String.valueOf(landNo));
        return api.searchLandByNos(getToken(),landNo);
    }

//    public Call<AliPay> generatePaymentOrder(int userId, int landId, int lease,String amount,String subject,int luckDrawRecordId){
//        return api.generatePaymentOrder(userId,landId,lease,amount,subject,luckDrawRecordId);
//    }

    public Call<AliPay> generatePaymentOrderNoluckDrawRecordId(int userId, int landId, int lease,String amount,String subject,int deductionAmount){
        return api.generatePaymentOrderNoluckDrawRecordId(userId,landId,lease,amount,subject,deductionAmount);
    }

    public Call<AliPay> generatePaymentOrderD(int userId, int getDiamond, String amount, String subject){
        return api.generatePaymentOrderD(userId,getDiamond,amount,subject);
    }

    public Call<AliPay> generatePaymentOrderVegetables(int userId, String amount, String subject,String orderId,int plotId,String price,String sum,String goodsId){
        return api.generatePaymentOrderVegetables(userId,amount,subject,orderId,plotId,price,sum,goodsId);
    }

    public Call<String> setSign(Map<String,String> setSign){
        return api.setSign(setSign);
    }

    public Call<CheckSMS> addMoney(int money,int diamond){
        return api.addMoney(getToken(),money,diamond);
    }

    public Call<QiNiu> getQiniuToken(){
        return api.getQiniuToken(getToken());
    }

    public Call<WeChat> weChatPayD(int getDiamond, String ip, Double amount, String subject){
        return api.weChatPayD(getUserId(),getDiamond,ip,amount,subject);
    }

    public Call<WeChat> weChatPay(int landId,int lease,String ip, Double amount, String subject,int deductionAmount ){
        return api.weChatPay(getUserId(),landId,lease,ip,amount,subject,deductionAmount);
    }

    public Call<WeChat> weChatPayVegetables(String ip,int userId, Double amount, String subject,String orderId,int plotId,String price,String sum,String goodsId){
        return api.weChatPayVegetables(ip,userId,amount,subject,orderId,plotId,price,sum,goodsId);
    }
//    public Call<WeChat> weChatPayNoluckDrawRecordId(int landId,int lease,String ip, Double amount, String subject,int luckDrawRecordId){
//        return api.weChatPayNoluckDrawRecordId(getUserId(),landId,lease,ip,amount,subject, luckDrawRecordId);
//    }

    public Call<GetVideoSize> getVideoSize(int landNo){
//        Log.e("token",getToken());
        return api.getVideoSize(getToken(),landNo);
    }

    public Call<Object> getVideoSize2(int landNo){
//        Log.e("token",getToken());
        return api.getVideoSize2(getToken(),landNo);
    }

    public Call<Object> stopVideo(int landNo){
        return api.stopVideo(getToken(),landNo);
    }


    public Call<CheckSMS> updatePassword(String pwd,String oldPwd) {
        return api.updatePassword(getToken(), pwd, oldPwd);
    }

    public Call<SeachFriend> seachFriend(int friendId){
        return api.seachFriend(getToken(),friendId);

    }

    public Call<CheckSMS> deleteFriend(int friendId){
        return api.deleteFriend(getToken(),friendId);

    }

    public Call<CheckSMS> searchSign(){
        return api.searchSign(getToken());
    }

    public Call<SignIn> updateIsSign(){
        return api.updateIsSign(getToken());
    }

    public Call<CheckSMS> addPhotoAlbum(String albumName,String photoAbstract,String photoUrl,String isPublic) {
        return api.addPhotoAlbum(getToken(), albumName,photoAbstract, photoUrl,isPublic);
    }

    public Call<CheckSMS> addReceivingAddress(int plotId,String isDefault){
        return api.addReceivingAddress(getToken(),plotId,isDefault);
    }


    public Call<searchReceivingAddress> searchReceivingAddress(){
        return api.searchReceivingAddress(getToken());
    }

    public Call<CheckSMS> deleteReceivingAddress(int addressId){
        return api.deleteReceivingAddress(getToken(),addressId);
    }

    public Call<CheckSMS> updateReceivingAddress(int addressId,int plotId,String isDefault){
        return api.updateReceivingAddress(getToken(),addressId,plotId,isDefault);
    }

    public Call<CheckSMS> updateReceivingAddress(int plotId,String isDefault){
//        Log.e("地址。。",getToken()+"+"+String.valueOf(plotId)+"+"+isDefault);
        return api.updateReceivingAddres(getToken(),plotId,isDefault);
    }

    //上传头像
    public Call<CheckSMS> updateUserImg(String headImg){
        return api.updateUserImg(getToken(),headImg);
    }

    public Call<SearchPhotoAlbum> searchPhotoAlbum(int userId,int pageNum,int pageSize) {
        return api.searchPhotoAlbum(getToken(),userId,pageNum,pageSize);
    }

    public Call<SearchPhotoAlbum> searchPhotoAlbumPublic(int userId,int pageNum,int pageSize,String isPublic) {
        return api.searchPhotoAlbumPublic(getToken(),userId,pageNum,pageSize,isPublic);
    }

    //重置手机号
    public Call<CheckSMS> updateUserPhone(String userPhone){
        return api.updateUserPhone(getToken(),userPhone);
    }

    public Call<Default> updatePhotoAlbum(int id, String albumName, String photoAbstract, String isPublic){
        return api.updatePhotoAlbum(getToken(),id,albumName,photoAbstract,isPublic);
    }

    public Call<CheckSMS> deletePhotoAlbum(int id) {
        return api.deletePhotoAlbum(getToken(),id);
    }

    public Call<searchOrderlogList> searchOrderlogListBuy(int select,int pageNum,int pageSize,int state){
        return api.searchOrderlogListBuy(getToken(),select,pageNum,pageSize,state);
    }

    public Call<searchOrderlogList> searchOrderlogList(int select,int pageNum,int pageSize){
        return api.searchOrderlogList(getToken(),select,pageNum,pageSize);
    }

    public Call<searchOrderlogList> searchOrderlogListByState(int state,int pageNum,int pageSize) {
        return api.searchOrderlogListByState(getToken(),state,pageNum,pageSize);
    }

    public Call<searchMyGood> searchMyGoods(int pageNum,int pageSize){
        return api.searchMyGoods(getToken(),pageNum,pageSize);
    }

    public Call<SearchAllGoods> searchAllGoods(int sortId,int pageNum,int pageSize,int sortDay,int sortNum,String cropName,int dishCategoryId){
        return api.searchAllGoods(getToken(),sortId,pageNum,pageSize,sortDay,sortNum,cropName,dishCategoryId);
    }

    public Call<SearchAllGoods> searchAllGoodsByPriceState(int priceState,int pageNum,int pageSize){
        return api.searchAllGoodsByPriceState(getToken(),priceState,pageNum,pageSize);
    }

    public Call<CheckSMS> updateGoodsState(int goodId,String state){
        return api.updateGoodsState(getToken(),goodId,state);
    }

    public Call<CheckSMS> addOrderLog(int goodsId,Double finalPrice,int plotId){
        return api.addOrderLog(getToken(),goodsId,finalPrice,plotId);
    }

    public Call<CheckSMS> addOrderLogGeneration(String orderId,int plotId,double finalPrice,String price,String sum,String goodsId,int userId){
        return api.addOrderLogGeneration(orderId,plotId,finalPrice,price,sum,goodsId,userId,0,2);
    }

    public Call<searchDetail> searchDetail(String keyWord){
        return api.searchDetail(keyWord);
    }

    public Call<searchFriendDetail> searchFriendDetail(int id){
        return api.searchFriendDetail(id);
    }

    public Call<CheckSMS> addApply(int friendId){
        return api.addApply(getToken(),friendId);
    }

    public Call<SearchMyApply> searchMyApply(){
        return api.searchMyApply(getToken());
    }

    public Call<CheckSMS> updateApplyResult(int id,String isAgree){
        return api.updateApplyResult(getToken(),id,isAgree);
    }

    public Call<CheckSMS> updateApplyState(int id,String state){
        return api.updateApplyState(getToken(),id,state);
    }

    public Call<CheckSMS> createGroup(String groupName,String desc,String members){
        return api.createGroup(getToken(),groupName,desc,members);
    }

    public Call<CheckSMS> dissolveGroup(String uuGroupId){
        return api.dissolveGroup(getToken(),uuGroupId);
    }

    public Call<CheckSMS> deleteGroup(String uuGroupId){
        return api.deleteGroup(getToken(),uuGroupId);
    }

    public Call<searchGroup> searchGroupMembers(String uuGroupId){
        return api.searchGroupMembers(getToken(),uuGroupId);
    }

    public Call<CheckSMS> updateGroupInfo(String uuGroupId,String groupName){
        return api.updateGroupInfo(getToken(),uuGroupId,groupName);
    }
    public Call<CheckSMS> cameraController(int landNo,int dwPTZCommand,int dwStop,int previousCommand,long clientTime,int clientType,int networkType,String devNo){
        return api.cameraController(getToken(),
                landNo,
                dwPTZCommand,
                dwStop,
                previousCommand,
                clientTime,
                clientType,
                networkType,
                devNo);
    }

    public Call<CheckSMS> landlordLogin(){
        return api.landlordLogin(getToken());
    }

    public Call<CheckSMS> cleanup(int landNo){
        return api.cleanup(getToken(),landNo);
    }

    public Call<AllUserInfo> searchLandlord(){
        return api.searchLandlord(getToken());
    }

    public Call<CusChat> addCusChatList(){
        return api.addCusChatList(getToken());
    }

    public Call<friendRecommendation> friendRecommendation(){
        return api.friendRecommendation(getToken());
    }

    public Call<SeachConfig> seachConfig(){
        return api.seachConfig();
    }

    public Call<InvitationFriend> searchInvitationFriend(String groupMembers){
        return api.searchInvitationFriend(getToken(),groupMembers);
    }

    public Call<CheckSMS> joinGroups(String uuGroupId, String userList){
        return api.joinGroups(getToken(),uuGroupId,userList);
    }

    public Call<PushToken> setUserPushToken(String pushToken){
        return api.setUserPushToken(getToken(),pushToken);
    }

    public Call<CheckSMS> androidLogUpload(String tel, MultipartBody.Part file){
        return api.androidLogUpload(tel,file);
    }

    public Call<TermsModel> searchTerms(){
        return api.searchTerms();
    }

    public Call<CheckSMS> updateVoice(Integer isVoice) {
        return api.updateVoice(getToken(), isVoice);
    }

    public Call<CheckSMS> usrOneController(Integer number, Integer type, String ip, Integer loopClose){
        return chickenCoopApi.usrOneController(number,type,ip,loopClose);
    }

    public Call<CheckSMSByResultIsList> searchAudio(){
        return api.searchAudio();
    }

    public Call<CheckSMS> addPhoto(String urlString,String text,String audioUrl,String albumName,String photoAbstract,String isPublic){
        return api.addPhoto(getToken(),urlString,text,audioUrl,albumName,photoAbstract,isPublic);
    }

    public Call<CheckSMS> addGrowthAlbum(String photoAlubmURL,String photoUrl) {
        return api.addGrowthAlbum(getToken(),2,photoAlubmURL,photoUrl);
    }

    public Call<HelpList> searchHelp(){
        return api.searchHelp();
    }

//    public  Call<SignRecord> searchSignRecord() {
//        return api.searchSignRecord(getToken());
//    }

    public Call<LuckDraw> searchLuckDraw(){
        return api.searchLuckDraw();
    }

    public Call<LuckDrawModel> userStartRaffle() {
        return api.userStartRaffle(getToken());
    }

    public Call<LuckDrawRecord> searchLuckDrawRecondList(int state) {
        return api.searchLuckDrawRecondList(getToken(),state);
    }

    public Call<LuckDrawRecord> userSearchLuckDrawRecondList(int type) {
        return api.userSearchLuckDrawRecondList(getToken(),type);
    }

    public Call<AreaAddress> searchAreaAddress() {
        return api.searchAreaAddress();
    }

    public Call<CheckSMS> addReceivingDetailedAddress(int areaAddressId,int luckDrawRecordId,String detailedAddress,String consigneeNumber,String consignee){
        return api.addReceivingDetailedAddress(getToken(),areaAddressId,luckDrawRecordId,detailedAddress,consigneeNumber,consignee);
    }

    public Call<LuckDrawAddress> searchLuckDrawAddress(){
        return api.searchLuckDrawAddress();
    }

    public Call<CheckSMS> addLuckDrawCollectAddress( int areaAddressId ,String detailedAddress) {
        return api.addLuckDrawCollectAddress(getToken(), areaAddressId , detailedAddress);
    }

    public Call<CheckSMS> addInvitation(String invitationCode){
        return api.addInvitation(getToken(),invitationCode);
    }

    public Call<LuckDrawDeductionAmount> searchLuckDrawDeductionAmount(){
        System.out.println("tokenaaa"+getToken());
        return api.searchLuckDrawDeductionAmount(getToken());
    }

    public Call<PlotPlantingStandard> searchPlotPlantingStandard() {
        return api.searchPlotPlantingStandard();
    }

    public Call<LandInfoCardItem> searchUsersLandLeaseHappening(){
        return api.searchUsersLandLeaseHappening(getToken());
    }

    public Call<Cooking> searchCookingMethods(int id) {
        return api.searchCookingMethods(id);
    }



    public Call<Cooking> searchCookingMethodsByUserId() {
        return api.searchCookingMethodsByUserId(getToken(),1);
    }


    public Call<CookingInfo> searchCookingMethodsDetails(int id) {
//        Log.i("检查token", "searchCookingMethodsDetails: "+getToken());
        return api.searchCookingMethodsDetails(getToken(),id);
    }

    public Call<CheckSMS>addCookingMethodsPraise(int id) {
        return api.addCookingMethodsPraise(getToken(),id);
    }
    //查询购物车
    public Call<ShoppingCart> searchShoppingCart() {
        return api.searchShoppingCart(getToken());
    }

    //购物车数量加减
    public Call<CheckSMS> updateShoppingCart(Integer id,Integer num) {
        return api.updateShoppingCart(getToken(),id,num);
    }

    public Call<CheckSMS> updateShoppingCartState(String goodsId) {
        return api.updateShoppingCartState(getToken(),goodsId,"-1");
    }

    //查询菜品类别
    public Call<DishCategory> searchDishCategory() {
        return api.searchDishCategory();
    }

    public Call<CheckSMS> addShoppingCart(Integer goodsId) {
        return api.addShoppingCart(getToken(),goodsId);
    }

    public Call<AdSeat> searchAdSeat(int type){
        return api.searchAdSeat(type);
    }

    public Call<CropGoodsDetails> searchCropGoodsDetails(int id) {
        return api.searchCropGoodsDetails(id);
    }

    public Call<ShoppingCartNum> searchShoppingCartNum() {
        return api.searchShoppingCartNum(getToken());
    }

    public Call<SignMonthRecord> searchSignMonthRecord() {
        return api.searchSignMonthRecord(getToken());
    }

    public Call<CheckSMS>  addCookingMethods(int CropId,String CookingName,String CookingHomePageImg,String CookingIngredients,String cookingImg,String cookingExplain) {
        return api.addCookingMethods(getToken(),CropId,CookingName,CookingHomePageImg,CookingIngredients,cookingImg,cookingExplain);
    }

    public Call<SearchLandByType> searchLandByType(int type) {
        return api.searchLandByType(getToken(),type);
    }

    public Call<LatelyTrack> searchLatelyTrack() {
        return api.searchLatelyTrack(getToken());
    }

    public Call<CreateActivity> selectCreateActivity() {
        return api.selectCreateActivity(getToken());
    }

    public Call<CheckSMS> addParticipationActivity(int activityId) {
        return api.addParticipationActivity(getToken(),activityId);
    }

    public Call<TransactionDetails> searchUserConsumptionRecord(int pageNum,int pageSize){
        return api.searchUserConsumptionRecord(getToken(),pageNum,pageSize);
    }
}


