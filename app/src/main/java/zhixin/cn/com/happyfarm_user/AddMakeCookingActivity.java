package zhixin.cn.com.happyfarm_user;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hyphenate.util.FileUtils;
import com.qiniu.android.common.AutoZone;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.KeyGenerator;
import com.qiniu.android.storage.Recorder;
import com.qiniu.android.storage.UpCancellationSignal;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;
import com.qiniu.android.storage.persistent.FileRecorder;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.zelory.compressor.Compressor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Url;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.View.PickerScrollView;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.QiNiu;
import zhixin.cn.com.happyfarm_user.model.SearchCropList;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.utils.Config;

public class AddMakeCookingActivity extends AppCompatActivity {

    private LinearLayout addCoverImage;
    private RelativeLayout coverLayout;
    private ImageView makeCookingCoverImage;
    private ImageView onAddCoverImage;
    private UploadManager uploadManager;
    private UploadManager uploadManager1;
    private EditText cookingName;
    private LinearLayout practiceLayout;
    private TextView practiceAdd;
    private TextView practiceUpdate;
    private LinearLayout ingredientsLayout;
    private TextView ingredientsAdd;
    private TextView ingredientsUpdate;
    private TextView getData;
    private byte [] cookingHomePageImg;
    private String cookingHomePageImgPath;
    private String cookingHomeName;
    private TextView selectCropId;
    private PickerScrollView pickerScrollView;
    private RelativeLayout selectCropLayout;
    private TextView Guanbi;

    private LinearLayout makeCookingImageLayout;
    private RelativeLayout makeCookingCoverLayout;
    private ImageView make_cooking_image;

    private int type = 0;
    private Integer clickeNumber = 0;
    private boolean ingredientsUpdateOnClick = false;
    private boolean practiceUpdateOnClick = false;
    private Map<String,byte[]> cookingMap = new HashMap<>();
    private List<SearchCropList.ResultBean.ListBean> cropList;
    private SearchCropList.ResultBean.ListBean crop;
//    private Map<String,String> cookingStepsTextMap = new HashMap<>();
//    private Map<String,String> cookingStepsImageMap = new HashMap<>();
    private String cookingIngredients = "";


    //最终要上传的数据
    private int CorpId = 0;
    private String CookingName;
    private String CookingHomePageImg;
    private String CookingIngredients;
    private String CookingImg;
    private String CookingExplain;
    private Dialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_make_cooking_activity);
        initView();
        searchAllCropList();
        addViewItem(null);
        addIngredientsView(null);
    }
    private void initView() {
        TextView title = findViewById(R.id.title_name);
        title.setText("创建菜谱");
        //自动识别上传区域
        Configuration config = new Configuration.Builder().zone(AutoZone.autoZone).build();
        uploadManager = new UploadManager(config);
        uploadManager1 = new UploadManager(config);
        addCoverImage = findViewById(R.id.add_cover_image);
        coverLayout = findViewById(R.id.cover_layout);
        makeCookingCoverImage = findViewById(R.id.make_cooking_cover_image);
        onAddCoverImage = findViewById(R.id.on_add_cover_image);
        cookingName = findViewById(R.id.cooking_name);
        practiceLayout = findViewById(R.id.practice);
        practiceAdd = findViewById(R.id.add_practice);
        practiceUpdate = findViewById(R.id.practice_update);
        ingredientsLayout = findViewById(R.id.ingredients);
        ingredientsAdd = findViewById(R.id.ingredients_add);
        ingredientsUpdate = findViewById(R.id.ingredients_update);
        getData = findViewById(R.id.get_data);

        selectCropId = findViewById(R.id.select_crop_id);
        selectCropLayout = findViewById(R.id.select_crop_layout);
        Guanbi = findViewById(R.id.img_guanbi);
        pickerScrollView = findViewById(R.id.select_crop_list);
        pickerScrollView.setOnSelectListener(new PickerScrollView.onSelectListener() {
            @Override
            public void onSelect(SearchCropList.ResultBean.ListBean pickers) {
                System.out.println("选择的菜品的名字："+pickers.getCropName());
                crop = pickers;
            }
        });

        addCoverImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCookingCoverImage.setImageBitmap(null);
                type = 0;
                selectPic();
            }
        });
        onAddCoverImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                coverLayout.setVisibility(View.GONE);
                addCoverImage.setVisibility(View.VISIBLE);
            }
        });
        practiceAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addViewItem(view);
            }
        });
        ingredientsAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addIngredientsView(view);
            }
        });
        ingredientsUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ingredientsUpdateOnClick) {
                    Log.i("通过按钮控制删除按钮", "让删除按钮隐藏");
                    for (int i = 0; i < ingredientsLayout.getChildCount(); i++) {
                        final View childAt = ingredientsLayout.getChildAt(i);
                        final TextView btn_remove = (TextView) childAt.findViewById(R.id.remove_make_cooking_material_item);
                        btn_remove.setVisibility(View.GONE);
                    }
                    ingredientsUpdate.setText("调整步骤");
                    ingredientsUpdateOnClick = false;
                } else {
                    Log.i("通过按钮控制删除按钮", "让删除按钮显示");
                    for (int i = 0; i < ingredientsLayout.getChildCount(); i++) {
                        final View childAt = ingredientsLayout.getChildAt(i);
                        final TextView btn_remove = (TextView) childAt.findViewById(R.id.remove_make_cooking_material_item);
                        btn_remove.setVisibility(View.VISIBLE);
                    }
                    ingredientsUpdate.setText("完成");
                    ingredientsUpdateOnClick = true;
                }

            }
        });
        practiceUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (practiceUpdateOnClick) {
                    Log.i("通过按钮控制删除按钮", "让删除按钮隐藏");
                    for (int i = 0; i < practiceLayout.getChildCount(); i++) {
                        final View childAt = practiceLayout.getChildAt(i);
                        final TextView btn_remove = (TextView) childAt.findViewById(R.id.btn_addHotel);
                        btn_remove.setVisibility(View.GONE);

                    }
                    practiceUpdate.setText("调整步骤");
                    practiceUpdateOnClick = false;
                } else {
                    Log.i("通过按钮控制删除按钮", "让删除按钮显示");
                    for (int i = 0; i < practiceLayout.getChildCount(); i++) {
                        final View childAt = practiceLayout.getChildAt(i);
                        final TextView btn_remove = (TextView) childAt.findViewById(R.id.btn_addHotel);
                        btn_remove.setVisibility(View.VISIBLE);
                    }
                    practiceUpdate.setText("完成");
                    practiceUpdateOnClick = true;
                }
            }
        });
        getData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printData();
            }
        });
        selectCropId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("显示选择器", "onClick: ");
                selectCropLayout.setVisibility(View.VISIBLE);
            }
        });
        Guanbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("最终选择的菜品", "onClick: " + JSON.toJSONString(crop));
                selectCropId.setText(crop.getCropName());
                selectCropLayout.setVisibility(View.GONE);
            }
        });

    }

    private void addViewItem(View view) {
        if (practiceLayout.getChildCount() == 0) {//如果一个都没有，就添加一个
            View hotelEvaluateView = View.inflate(this, R.layout.make_cooking_item, null);
            practiceLayout.addView(hotelEvaluateView);
            sortHotelViewItem();
        } else {//如果有一个以上的Item,点击为添加的Item则添加
            View hotelEvaluateView = View.inflate(this, R.layout.make_cooking_item, null);
            practiceLayout.addView(hotelEvaluateView);
            sortHotelViewItem();
        }
    }

    /**
     * 查询全部作物
     */

    private void searchAllCropList() {
        HttpHelper.initHttpHelper().searchAllCropList().enqueue(new Callback<SearchCropList>() {
            @Override
            public void onResponse(Call<SearchCropList> call, Response<SearchCropList> response) {
                Log.i("查询全部作物", "onResponse: " + JSON.toJSONString(response.body().getResult().getList()));
                if ("success".equals(response.body().getFlag())) {
                    cropList = response.body().getResult().getList();
                    pickerScrollView.setData(cropList);
                    pickerScrollView.setSelected(0);
//                    crop = cropList.get(0);
                } else {

                }
            }

            @Override
            public void onFailure(Call<SearchCropList> call, Throwable t) {

            }
        });
    }

    /**
     * 获取所有动态添加的Item，找到控件的id，获取数据
     */
    private void printData() {
        mDialog = LoadingDialog.createLoadingDialog(AddMakeCookingActivity.this, "添加菜谱中...");
        List<String> cookingStepsText = new ArrayList<>();
        cookingIngredients = "";
        Integer[] a = {1};
        //菜谱名字
//        System.out.println(cookingName.getText().toString());
        if ("".equals(cookingName.getText().toString())) {
            Toast.makeText(AddMakeCookingActivity.this,"请输入菜谱名称",Toast.LENGTH_SHORT).show();
            LoadingDialog.closeDialog(mDialog);
            return;
        } else {
            cookingHomeName = cookingName.getText().toString();
        }
        Log.i(JSON.toJSONString(crop), "printData: ");
        if (null == crop) {
            Toast.makeText(AddMakeCookingActivity.this,"请选择菜品",Toast.LENGTH_SHORT).show();
            LoadingDialog.closeDialog(mDialog);
            return;
        }
        //菜谱材料
        for (int j = 0; j < ingredientsLayout.getChildCount(); j++) {
            View childAt = ingredientsLayout.getChildAt(j);
            EditText hotelName = (EditText) childAt.findViewById(R.id.material);
            EditText dosage = childAt.findViewById(R.id.dosage);
//            Log.e("获取数据", "食谱：" + hotelName.getText().toString() + dosage.getText().toString());
            cookingIngredients = cookingIngredients + "\t\t\t\t" + hotelName.getText().toString() +"："+ dosage.getText().toString()+"\n";
            if ("".equals(hotelName.getText().toString()) && "".equals(dosage.getText().toString())){
                Toast.makeText(AddMakeCookingActivity.this,"请按规则填写食谱",Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(mDialog);
                return;
            }
        }


        //菜谱步骤
        for (int i = 0; i < practiceLayout.getChildCount(); i++) {
            View childAt = practiceLayout.getChildAt(i);
            EditText hotelName = (EditText) childAt.findViewById(R.id.add_content);
//          Log.e("获取数据", "步骤文字：" + hotelName.getText().toString());
            cookingStepsText.add(hotelName.getText().toString());
            if ("".equals(hotelName.getText().toString()) ) {
                Toast.makeText(AddMakeCookingActivity.this,"请按规则填写做法步骤",Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(mDialog);
                return;
            }
        }
//        Log.i("图和文 的个数", cookingMap.size()+"printData: "+practiceLayout.getChildCount());
        if (cookingMap.size() == practiceLayout.getChildCount()) {
            List<String> cookingStepsImage = new ArrayList<>();
            for(int i = 0; i < cookingMap.size() ; i ++){
                cookingStepsImage.add("1");
            }
            //菜谱步骤配图
            int number = 0;
            for (Map.Entry<String, byte []> entry : cookingMap.entrySet()) {
    //            System.out.println("key= " + entry.getKey() + " and value= " + entry.getValue());
    //            AddQiNiu(entry.getValue() , entry.getKey());
                Date d = new Date();
                //因为要根据下标去替换cookingStepsImage中的数据 ，MAp便利没有下标 所以自己创建一个来代替
                int stepsNumber = ++number ;
                uploadManager.put(entry.getValue(), stepsNumber + "make_cooking_" + getTimestamp(d), Config.qiuNiuToken, new UpCompletionHandler() {
                    @Override
                    public void complete(String key, ResponseInfo info, org.json.JSONObject response) {
                        //res包含hash、key等信息，具体字段取决于上传策略的设置
                        if(info.isOK()) {
                            String headImg = Config.qiuNiuImgUrl + stepsNumber + "make_cooking_" + getTimestamp(d);
    //                                    Log.i("下标", "complete: " +stepsNumber );
                            cookingStepsImage.set(stepsNumber - 1 ,headImg);
    //                                    cookingStepsImage.add(headImg);
    //                                    Log.i("步骤图片", "Upload Success" + headImg);
    //                                    System.out.println("a:"+ a[0] +"VS 数据量" + cookingMap.size());

    //                                        System.out.println(JSON.toJSONString(cookingStepsText));
    //                                        System.out.println(JSON.toJSONString(cookingStepsImage));

                            List<Map<String,String>> cookingOne = new ArrayList<>();
                            List<Map<String,String>> cookingTwe = new ArrayList<>();


                            for (int i = 0;i < cookingStepsText.size(); i++) {
                                Map<String,String> cookingStepsTextMap = new HashMap<>();
                                Map<String,String> cookingStepsImageMap = new HashMap<>();
    //                                            Log.i("", cookingStepsText.get(i)+"complete: " + cookingStepsImage.get(i));
                                cookingStepsTextMap.put("text",cookingStepsText.get(i));
                                cookingStepsImageMap.put("image",cookingStepsImage.get(i));
                                cookingOne.add(cookingStepsTextMap);
                                cookingTwe.add(cookingStepsImageMap);
                            }
                            CookingImg = JSON.toJSONString(cookingTwe);
                            CorpId = crop.getId();
                            CookingName = cookingHomeName;
                            CookingHomePageImg = cookingHomePageImgPath;
                            CookingIngredients = cookingIngredients;
                            CookingExplain = JSON.toJSONString(cookingOne);
    //                                        System.out.println( "我从这里进来的" );

                            if (a[0] == cookingMap.size() + 1) {
                                addCookingMethods(CorpId,CookingName,CookingHomePageImg,CookingIngredients,CookingImg,CookingExplain);
                            }
    //                                    System.out.println("成功了"+ a[0] +"数据");
                            a[0]++;
                        } else {
                            Config.qiuNiuToken = "";
                            Config.qiuNiuDomain = "";
    //                                    Log.i("qiniu", "Upload Fail");
                            //如果失败，这里可以把info信息上报自己的服务器，便于后面分析上传错误原因
                        }
    //                                Log.i("qiniu", key + ",\r\n " + info + ",\r\n " + response);
                    }
                },new UploadOptions(null, null, false, new UpProgressHandler(){
                    public void progress(String key, double percent){
    //                                Log.i("qiniu", key + ": " + percent);
                    }
                }, null));
            }
        } else {
            Toast.makeText(AddMakeCookingActivity.this,"请重新填写菜谱内容",Toast.LENGTH_SHORT).show();
            LoadingDialog.closeDialog(mDialog);
            return;
        }

        //封面图
        if (cookingHomePageImg != null) {
            Date d = new Date();
            uploadManager1.put(cookingHomePageImg, "cooking_home_page_image" + getTimestamp(d), Config.qiuNiuToken,
                    new UpCompletionHandler() {
                        @Override
                        public void complete(String key, ResponseInfo info, org.json.JSONObject response) {
                            //res包含hash、key等信息，具体字段取决于上传策略的设置
                            if(info.isOK()) {
                                String headImg = Config.qiuNiuImgUrl+"cooking_home_page_image" + getTimestamp(d);
                                cookingHomePageImgPath = headImg;
//                                Log.i("菜谱封面图", "Upload Success" + headImg);
                                if (a[0] == cookingMap.size() + 1) {
                                    CookingHomePageImg = headImg;
//                                    System.out.println("我从header进来的");
                                    addCookingMethods(CorpId,CookingName,CookingHomePageImg,CookingIngredients,CookingImg,CookingExplain);
                                }

                                a[0]++;
                            } else {
                                Config.qiuNiuToken = "";
                                Config.qiuNiuDomain = "";
//                                Log.i("cookingHomePageImg", "Upload Fail");
                                //如果失败，这里可以把info信息上报自己的服务器，便于后面分析上传错误原因
                            }
//                            Log.i("qiniu", key + ",\r\n " + info + ",\r\n " + response);
                        }
                    },new UploadOptions(null, null, false,
                            new UpProgressHandler(){
                                public void progress(String key, double percent){
//                                    Log.i("qiniu", key + ": " + percent);
                                }
                            }, null));
        } else {
            Toast.makeText(AddMakeCookingActivity.this,"请选择菜谱封面",Toast.LENGTH_SHORT).show();
            LoadingDialog.closeDialog(mDialog);
            return;
        }
    }

    /**
     * Item排序
     */
    private void sortHotelViewItem() {
        //获取LinearLayout里面所有的view
        for (int i = 0; i < practiceLayout.getChildCount(); i++) {
            final View childAt = practiceLayout.getChildAt(i);
            final TextView btn_remove = (TextView) childAt.findViewById(R.id.btn_addHotel);
            btn_remove.setText("删除");
            btn_remove.setTag("remove");//设置删除标记
            if (practiceUpdateOnClick) {
                btn_remove.setVisibility(View.VISIBLE);
            } else {
                btn_remove.setVisibility(View.GONE);
            }
            final TextView textView = childAt.findViewById(R.id.steps_number);
            int aa = i+1;
            int finalI = i;
            textView.setText("步骤" + aa);
            btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //从LinearLayout容器中删除当前点击到的ViewItem
                    practiceLayout.removeView(childAt);
                    System.out.println("viewItem" + finalI);
                    cookingMap.remove("" + finalI);
//                    cookingStepsImage.remove(clickeNumber);
//                    cookingStepsText.remove(clickeNumber);
                    sortHotelViewItem();
                }
            });
            makeCookingImageLayout = childAt.findViewById(R.id.make_cooking_image_layout);
            makeCookingCoverLayout = childAt.findViewById(R.id.make_cooking_cover_layout);
            make_cooking_image = childAt.findViewById(R.id.make_cooking_image);
            makeCookingImageLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    type = 1;
                    clickeNumber = finalI;
                    Log.i("点击的", "onClick: "+ finalI);
                    final View childAt = practiceLayout.getChildAt(finalI);
                    make_cooking_image = childAt.findViewById(R.id.make_cooking_image);
                    make_cooking_image.setImageBitmap(null);
                    selectPic();
                }
            });
            final ImageView remove_image = childAt.findViewById(R.id.remove_image);
            remove_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    for (Map.Entry<String, byte []> entry : cookingMap.entrySet()) {
//                        System.out.println("要替换的 key= " + entry.getKey() + " and value= " + entry.getValue()+"下标"+clickeNumber);
//                        if (entry.getKey().equals(clickeNumber.toString())) {
//                            cookingMap.put("" + clickeNumber,null);
//                            System.out.println("替换成空的" + cookingMap.size());
//                        }
//                    }
                    final View childAt = practiceLayout.getChildAt(finalI);
                    makeCookingImageLayout = childAt.findViewById(R.id.make_cooking_image_layout);
                    makeCookingCoverLayout = childAt.findViewById(R.id.make_cooking_cover_layout);
                    makeCookingCoverLayout.setVisibility(View.GONE);
                    makeCookingImageLayout.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void addIngredientsView(View view) {
        View hotelEvaluateView = View.inflate(this, R.layout.make_cooking_material_item, null);
        ingredientsLayout.addView(hotelEvaluateView);
        ingredientsViewItem();
    }

    /**
     * 排序食谱item
     */
    private void ingredientsViewItem() {
        //获取LinearLayout里面所有的view
        for (int i = 0; i < ingredientsLayout.getChildCount(); i++) {
            final View childAt = ingredientsLayout.getChildAt(i);
            final TextView btn_remove = (TextView) childAt.findViewById(R.id.remove_make_cooking_material_item);
            btn_remove.setText("删除");
            btn_remove.setTag("remove");//设置删除标记
            if (ingredientsUpdateOnClick) {
                btn_remove.setVisibility(View.VISIBLE);
            } else {
                btn_remove.setVisibility(View.GONE);
            }
            btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //从LinearLayout容器中删除当前点击到的ViewItem
                    ingredientsLayout.removeView(childAt);

                    for (Map.Entry<String, byte []> entry : cookingMap.entrySet()) {
                        System.out.println("要删除的 key= " + entry.getKey() + " and value= " + entry.getValue());
                        if (entry.getKey().equals(clickeNumber.toString())) {
                            cookingMap.remove("" + clickeNumber);
                            System.out.println("删除成功" + cookingMap.size());
                        }
                    }
                    ingredientsViewItem();
                }
            });
        }
    }


    /**
     * 打开本地相册选择图片
     */
    private void selectPic(){
        //intent可以应用于广播和发起意图，其中属性有：ComponentName,action,data等
        Intent intent=new Intent();
        intent.setType("image/*");
        //action表示intent的类型，可以是查看、删除、发布或其他情况；我们选择ACTION_GET_CONTENT，系统可以根据Type类型来调用系统程序选择Type
        //类型的内容给你选择
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //如果第二个参数大于或等于0，那么当用户操作完成后会返回到本程序的onActivityResult方法
        startActivityForResult(intent, 1);
    }

    /**
     *把用户选择的图片显示在imageview中
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //用户操作完成，结果码返回是-1，即RESULT_OK
        if(resultCode==RESULT_OK){
            if (data == null) {//这里判断主要测试与小米手机，点击相册允许后，再点取消会崩溃
                return;
            }
            if (null != data) {
                try {
                    //获取选中文件的定位符
                    Uri uri = data.getData();
                    Log.e("uri", uri.toString());
                    //使用content的接口
                    ContentResolver cr = this.getContentResolver();
                    //获取图片
                    Bitmap bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri));
                    if (type == 0) {
                        cookingHomePageImg = toByteArray(cr.openInputStream(uri));
                        makeCookingCoverImage.setImageBitmap(bitmap);
                        coverLayout.setVisibility(View.VISIBLE);
                        addCoverImage.setVisibility(View.GONE);
                    } else {
                        Log.i("我选择所要加入图片的view下标：", ""+clickeNumber);

                        if (cookingMap.isEmpty()){
                            Log.i("是空的,第一次添加", "onActivityResult: ");
                            cookingMap.put(""+clickeNumber,toByteArray(cr.openInputStream(uri)));
                        } else {
                            Log.i("不是空的", "onActivityResult: ");
                            int a = 0;
                            for (Map.Entry<String, byte []> entry : cookingMap.entrySet()) {
                                System.out.println("key= " + entry.getKey() + " and value= "
                                        + entry.getValue());
                                if (entry.getKey().equals(clickeNumber.toString())) {
                                    Log.i("存在相同位置的图片，需要替换", "onActivityResult: ");
                                    cookingMap.put("" + clickeNumber,toByteArray(cr.openInputStream(uri)));
                                    a++;
                                }
                            }
                            if (a == 0) {
                                cookingMap.put(""+clickeNumber,toByteArray(cr.openInputStream(uri)));
                                Log.i("如果没有被修改过则需要加上", "onActivityResult: ");
                            }
                        }


                        final View childAt = practiceLayout.getChildAt(clickeNumber);
                        makeCookingImageLayout = childAt.findViewById(R.id.make_cooking_image_layout);
                        makeCookingCoverLayout = childAt.findViewById(R.id.make_cooking_cover_layout);
                        make_cooking_image = childAt.findViewById(R.id.make_cooking_image);

                        make_cooking_image.setImageBitmap(bitmap);
                        makeCookingCoverLayout.setVisibility(View.VISIBLE);
                        makeCookingImageLayout.setVisibility(View.GONE);
                    }
                } catch (FileNotFoundException e) {
                    Log.e("Exception", e.getMessage(),e);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else{
            //操作错误或没有选择图片
            Log.i("MainActivtiy", "operation error");
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * InputStream 转换成byte[]
     * @param input
     * @return
     * @throws IOException
     */
    private static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024*4];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }

    //TODO 获取七牛token
    private void qiniuToken(){
        if ("".equals(Config.qiuNiuToken)){
            HttpHelper.initHttpHelper().getQiniuToken().enqueue(new Callback<QiNiu>() {
                @Override
                public void onResponse(Call<QiNiu> call, Response<QiNiu> response) {
//                    Log.e("qiniuTokenData", JSON.toJSONString(response.body()));
                    if ("success".equals(response.body().getFlag())) {
                        Config.qiuNiuToken = response.body().getResult().getUploadToken();
                        Config.qiuNiuDomain = response.body().getResult().getDomain();
                        Config.qiuNiuImgUrl = response.body().getResult().getImgUrl();
                    }
                }
                @Override
                public void onFailure(Call<QiNiu> call, Throwable t) {

                }
            });
        }
    }


    private void addCookingMethods(int CropId,String CookingName,String CookingHomePageImg,String CookingIngredients,String cookingImg,String cookingExplain) {
        HttpHelper.initHttpHelper().addCookingMethods(CropId,CookingName,CookingHomePageImg,CookingIngredients,cookingImg,cookingExplain).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                System.out.println(JSON.toJSONString(response.body().getResult()));
                if ("success".equals(response.body().getFlag())) {
                    Toast.makeText(AddMakeCookingActivity.this,"菜谱添加成功",Toast.LENGTH_SHORT).show();
                    AddMakeCookingActivity.this.finish();
                } else {
                    Toast.makeText(AddMakeCookingActivity.this,"菜谱添加失败",Toast.LENGTH_SHORT).show();
                }
                LoadingDialog.closeDialog(mDialog);
            }
            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(AddMakeCookingActivity.this,"网络错误，菜谱添加失败",Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(mDialog);
            }
        });
    }
    /**
     获取精确到毫秒的时间戳
     * @param date
     * @return
     **/
    public static Long getTimestamp(Date date){
        if (null == date) {
            return (long) 0;
        }
        String timestamp = String.valueOf(date.getTime());
        return Long.valueOf(timestamp);
    }


    @Override
    protected void onStart() {
        super.onStart();
        qiniuToken();
    }

    public void back(View view) {
        finish();
    }


}
