package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * Created by Administrator on 2018/7/24.
 */

public class InvitationFriend {

    /**
     * result : [{"id":3,"userId":5,"friendId":7,"friendNickname":"完善个人信息","toId":7,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head71530150175","uuid":"7757a310-7483-11e8-8e40-6d6fa622be8c","tel":"13196560702","landNo":null,"state":1},{"id":24,"userId":5,"friendId":22,"friendNickname":"可乐","toId":22,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head1529898810","uuid":"f5244d80-781a-11e8-a948-83bcc59539f3","tel":"13771438778","landNo":null,"state":0},{"id":25,"userId":5,"friendId":15,"friendNickname":"啾咪","toId":15,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_11_1530779419272","uuid":"bb57b940-74f8-11e8-9fc8-8905e47feecf","tel":"18912370142","landNo":null,"state":1},{"id":27,"userId":5,"friendId":23,"friendNickname":"雪碧","toId":23,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head231529907314","uuid":"381a2570-7829-11e8-a721-171b3adf63da","tel":"15852789007","landNo":null,"state":1},{"id":36,"userId":5,"friendId":25,"friendNickname":"橙汁","toId":25,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_25_1530002811863","uuid":"47496e40-791c-11e8-8916-4537f9dd065b","tel":"15852839556","landNo":null,"state":1},{"id":40,"userId":5,"friendId":18,"friendNickname":"jdld","toId":18,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_18_1531477393129","uuid":"c7105220-754c-11e8-97b1-490140287d2e","tel":"17625010326","landNo":null,"state":1},{"id":80,"userId":5,"friendId":38,"friendNickname":"gdd","toId":38,"friendHeadImg":"","uuid":"e244fb20-8b56-11e8-816d-a3e8d173e9ed","tel":"13456156795","landNo":null,"state":0},{"id":85,"userId":5,"friendId":42,"friendNickname":"小萝卜","toId":42,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_42_1532415684673","uuid":"e2890360-8e53-11e8-b5bb-59bceb1e3b77","tel":"15312293017","landNo":null,"state":1},{"id":1,"userId":6,"friendId":5,"friendNickname":"周子龙","toId":6,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_6_1532435680506","uuid":"5190b3f0-747a-11e8-8dc2-67b65029b5dc","tel":"17621978355","landNo":null,"state":1},{"id":6,"userId":8,"friendId":5,"friendNickname":"杨枢应","toId":8,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_8_1529501279860","uuid":"865aaba0-748d-11e8-aaf4-85814947c7e8","tel":"13400005345","landNo":null,"state":1},{"id":33,"userId":9,"friendId":5,"friendNickname":"余枢合","toId":9,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head91529503164","uuid":"b8bc7ac0-7491-11e8-bb24-a18b08ffcaca","tel":"13306184950","landNo":null,"state":1},{"id":38,"userId":11,"friendId":5,"friendNickname":"wxxyy","toId":11,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_11_1532433594111","uuid":"dd0100e0-74a0-11e8-beaa-c18b0a0cc6fd","tel":"15358932117","landNo":null,"state":1},{"id":47,"userId":31,"friendId":5,"friendNickname":"腾讯的卧底","toId":31,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/userHead/head_311532409998041","uuid":"cb4b0600-7d99-11e8-a4e0-4dcba930818e","tel":"15061881827","landNo":null,"state":1},{"id":48,"userId":21,"friendId":5,"friendNickname":"雷诺","toId":21,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head211531126200","uuid":"a4cf5200-75de-11e8-b068-8bac8cecb32e","tel":"16605106547","landNo":null,"state":1},{"id":77,"userId":12,"friendId":5,"friendNickname":"wjt","toId":12,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_12_1530761958140","uuid":"c0ad9540-74ee-11e8-be63-6fac9d43708f","tel":"18861876108","landNo":null,"state":1},{"id":81,"userId":39,"friendId":5,"friendNickname":"陈志林","toId":39,"friendHeadImg":"","uuid":"ff4fbee0-8bc3-11e8-bbf2-3b8e2ae77dcc","tel":"18296784905","landNo":null,"state":0},{"id":84,"userId":40,"friendId":5,"friendNickname":"YGJ","toId":40,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/head401532072969","uuid":"81f418b0-8bf0-11e8-adce-9be19c53d868","tel":"15306168717","landNo":null,"state":1},{"id":87,"userId":14,"friendId":5,"friendNickname":"文成呗","toId":14,"friendHeadImg":"http://7xtaye.com2.z0.glb.clouddn.com/userHead/head_161531188854096","uuid":"a851d700-74f7-11e8-833d-a51f7ce1ba39","tel":"15180150746","landNo":null,"state":1}]
     * flag : success
     */

    private String flag;
    private List<ResultBean> result;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * id : 3
         * userId : 5
         * friendId : 7
         * friendNickname : 完善个人信息
         * toId : 7
         * friendHeadImg : http://7xtaye.com2.z0.glb.clouddn.com/head71530150175
         * uuid : 7757a310-7483-11e8-8e40-6d6fa622be8c
         * tel : 13196560702
         * landNo : null
         * state : 1
         */

        private int id;
        private int userId;
        private int friendId;
        private String friendNickname;
        private int toId;
        private String friendHeadImg;
        private String uuid;
        private String tel;
        private Object landNo;
        private int state;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getFriendId() {
            return friendId;
        }

        public void setFriendId(int friendId) {
            this.friendId = friendId;
        }

        public String getFriendNickname() {
            return friendNickname;
        }

        public void setFriendNickname(String friendNickname) {
            this.friendNickname = friendNickname;
        }

        public int getToId() {
            return toId;
        }

        public void setToId(int toId) {
            this.toId = toId;
        }

        public String getFriendHeadImg() {
            return friendHeadImg;
        }

        public void setFriendHeadImg(String friendHeadImg) {
            this.friendHeadImg = friendHeadImg;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public Object getLandNo() {
            return landNo;
        }

        public void setLandNo(Object landNo) {
            this.landNo = landNo;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }
}
