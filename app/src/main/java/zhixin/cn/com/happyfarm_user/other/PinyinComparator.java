package zhixin.cn.com.happyfarm_user.other;

import java.util.Comparator;

import zhixin.cn.com.happyfarm_user.easyMob.SelectContact;

public class PinyinComparator implements Comparator<SelectContact> {

	public int compare(SelectContact o1, SelectContact o2) {
		if (o1.getLetters().equals("@")
				|| o2.getLetters().equals("#")) {
			return -1;
		} else if (o1.getLetters().equals("#")
				|| o2.getLetters().equals("@")) {
			return 1;
		} else {
			return o1.getLetters().compareTo(o2.getLetters());
		}
	}

}
