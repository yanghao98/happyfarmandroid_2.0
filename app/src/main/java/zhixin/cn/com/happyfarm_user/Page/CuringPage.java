package zhixin.cn.com.happyfarm_user.Page;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import retrofit2.Call;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.CuringAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.ListDialogAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.PaymentInformationActivity;
import zhixin.cn.com.happyfarm_user.PerfectInformationActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AllLand;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.Curing;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.SearchMaintainList;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.AlertUtilOneButton;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by DELL on 2018/3/8.
 */

public class CuringPage extends Fragment {

    private RecyclerView recyclerView;
    private String state;
    private List<String> fertilizer = new ArrayList<String>();
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    private List<Curing> list = new ArrayList<Curing>();
    private List<Double> balances;
    private int payType;
    private List<Integer> maintainId;
    private List<Integer> sellNum = new ArrayList<>();
    private ProgressBar progressBar;
    private String landId;
    private String landNo;
    private Dialog mDialog;
    private String UserNickName;
    private String UserImg;
    private String UserTel;
    private String UserCertificatesNo;
    private String UserRealname;
    private int UserPlotId;
    private String UserPlotName;
    private int UserGender;
    private long UserBirthday;
    private String UserEmail;
    private String states;
    private RefreshLayout refreshLayout;
    private int landType;
    private CuringAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private List<SearchMaintainList.ResultBean.ListBean> listBeanList;
    //开水
    private int lampNumber;
    //开灯
    private int waterNumber;
    //继电器ip
    private String relayIp;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        landId = ((LandVideoActivity)activity).getLandId();
        landNo = ((LandVideoActivity)activity).getLandNos();
        state = ((LandVideoActivity)activity).getState();
        landType = ((LandVideoActivity)activity).getLandType();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.curing_layout,null,false);
//        Log.e("states",states);
        initView(view);
        initRecyclerView();
        //下拉刷新
        topRefreshLayout();
        if (!list.isEmpty()){
            list.clear();
        }
        StaggerLoadData(false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getLandInfo();
        getUserInfo();
    }

    /**
     * 初始化佈局
     * @param view
     */
    private void initView(View view) {
        recyclerView = view.findViewById(R.id.curing_recycle);
        progressBar = view.findViewById(R.id.progressBar);
        //state = ((LandVideoActivity)getActivity()).getState();

        refreshLayout = view.findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false));
    }

    /**
     * 初始化适配器
     */
    private void initRecyclerView() {
        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new CuringAdapter(getContext(),list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(getContext(), 3);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter.setItemClickListener(new CuringAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                GetSelfInfo();
                HttpHelper.initHttpHelper().test().enqueue(new Callback<Test>() {
                    @Override
                    public void onResponse(Call<Test> call, Response<Test> response) {
//                                        Log.e("DeBug",JSON.toJSONString(response.body())+","+state);
                        if (("failed").equals(response.body().getFlag())) {
                            showUserInfoDialog(TextString.DialogTitleText, TextString.DialogContentText);
                        } else {
                            if (state.equals("1")) {
                                //获取用户信息是否为地主
                                retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
                                getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
                                    @Override
                                    public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                                        Log.e("Curing", JSON.toJSONString(response.body()));
                                        System.out.println(JSON.toJSONString(response.body()));
                                        if (("success").equals(response.body().getFlag())) {
                                            //判断是否是地主。
                                            if (response.body().getResult().getIsLandlord() == 1) {
                                                Toast.makeText(getContext(), "您已是地主，您的地在等待您养护哦", Toast.LENGTH_SHORT).show();
                                            } else {
                                                perfectInfoDialog();
                                            }
                                        } else {
                                            Toast.makeText(getContext(), response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                                        showUserInfoDialog(TextString.DialogTitleText, TextString.DialogContentText);
                                    }
                                });
                            } else if (state.equals("0")) {
                                Log.e("DeBug", String.valueOf(states()));
//                                                Log.e("DeBug",String.valueOf(responses.body().getResult().getList().get(position).getChild()));
                                if (listBeanList.get(position).getChild().isEmpty()) {
                                    maintainId = new ArrayList<>();
                                    int maintainIds = listBeanList.get(position).getId();
                                    double money = listBeanList.get(position).getDiamond();
                                    states();

                                    if (states.equals("1")) {
                                        Toast.makeText(getContext(), "您已处于托管状态", Toast.LENGTH_SHORT).show();
                                    } else if (states.equals("0")) {
                                        Log.e("asdasd", String.valueOf(listBeanList.get(position)));
                                        payDialog(money, "0", position, maintainIds, maintainId,listBeanList.get(position).getMaintainName());
                                    }
                                } else {
                                    fertilizer = new ArrayList<>();
                                    maintainId = new ArrayList<>();
                                    for (int i = 0; i <= listBeanList.get(position).getChild().size() - 1; i++) {
                                        fertilizer.add(listBeanList.get(position).getChild().get(i).getMaintainName());
                                        maintainId.add(listBeanList.get(position).getChild().get(i).getId());
                                    }
                                    states();
                                    if (states.equals("1")) {
                                        Toast.makeText(getContext(), "您已处于托管状态", Toast.LENGTH_SHORT).show();
                                    } else if (states.equals("0")) {
                                        ShowDialog1(fertilizer, "请选择", position, maintainId);
                                    }
                                }

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Test> call, Throwable t) {
                        Log.e("CE", t.getMessage());
                    }
                });
            }
        });
    }

    private void StaggerLoadData(Boolean inversion) {
        //集合对象
        progressBar.setVisibility(View.VISIBLE);
        //给DataBean类放数据，最后把装好数据的DataBean类放到集合里
            Call<SearchMaintainList> searchMaintainList = HttpHelper.initHttpHelper().searchMaintainList(landType);
            searchMaintainList.enqueue(new Callback<SearchMaintainList>() {
                @Override
                public void onResponse(Call<SearchMaintainList> call, Response<SearchMaintainList> responses) {
                    listBeanList = new ArrayList<>();
                    if (("success").equals(responses.body().getFlag())){
                        progressBar.setVisibility(View.GONE);
                        Log.e("ListSize",String.valueOf(responses.body().getResult().getList().size()));
                        for(int i=0; i < responses.body().getResult().getList().size(); i++){
                            Curing dataBean=new Curing();
                            dataBean.imageId = responses.body().getResult().getList().get(i).getMaintainImg();
                            dataBean.name = responses.body().getResult().getList().get(i).getMaintainName();
                            dataBean.diamond = String.valueOf(responses.body().getResult().getList().get(i).getDiamond());
                            list.add(dataBean);
                        }
                        listBeanList = responses.body().getResult().getList();
                        adapter.notifyDataSetChanged();
                    }else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "列表加载失败",Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<SearchMaintainList> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "列表加载失败",Toast.LENGTH_SHORT).show();
                }
            });

    }
    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!list.isEmpty())
                    list.clear();
                StaggerLoadData(false);
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }

    //完善用户信息layout弹窗
    private void perfectInfoDialog() {
        Dialog bottomDialog = new Dialog(getContext(), R.style.dialog);
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.layout_perfect_information_dialog, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        //设置左右间距
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(getContext(), 100f);
        params.bottomMargin = DensityUtil.dp2px(getContext(), 0f);
        contentView.findViewById(R.id.lease_immediately_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomDialog.cancel();
//                finish();
            }
        });
        contentView.findViewById(R.id.lease_immediately_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //获取用户信息是否完善
                retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
                getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
                    @Override
                    public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                        if (("success").equals(response.body().getFlag())){
                            String titleText = "完善信息";
                            String contentText = "您的信息暂未完善，是否完善您的信息";
                            if (NumUtil.checkNull(response.body().getResult().getRealname())){
                                showUserInfoDialog(titleText,contentText);
                            }else if (NumUtil.checkNull(response.body().getResult().getCertificatesNo())){
                                showUserInfoDialog(titleText,contentText);
                            }else if (NumUtil.checkNull(response.body().getResult().getPlotId())){
                                showUserInfoDialog(titleText,contentText);
                            }else if (response.body().getResult().getGender() == 0){
                                showUserInfoDialog(titleText,contentText);
                            }else if (NumUtil.checkNull(response.body().getResult().getBirthday())){
                                showUserInfoDialog(titleText,contentText);
                            }else if (NumUtil.checkNull(response.body().getResult().getTel())){
                                showUserInfoDialog(titleText,contentText);
                            }else if (NumUtil.checkNull(response.body().getResult().getEmail())){
                                showUserInfoDialog(titleText, contentText);
                            }else {
//                                getActivity().finish();
                                Intent intent = new Intent(getContext(),PaymentInformationActivity.class);
                                intent.putExtra("landNo",landNo);
                                intent.putExtra("landId",landId);
                                intent.putExtra("landType",landType+"");
                                startActivity(intent);
                            }

                        }else {
                            Toast.makeText(getContext(),response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                        Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                    }
                });
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();

    }
    //完善用户信息layout弹窗
    private void showUserInfoDialog(String titleText,String contentText) {
        Dialog bottomDialog = new Dialog(getContext(), R.style.dialog);
        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.layout_perfect_information_dialog, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        //设置左右间距
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(getContext(), 100f);
        params.bottomMargin = DensityUtil.dp2px(getContext(), 0f);
        TextView lblTitle = contentView.findViewById(R.id.lease_immediately_bt);
        lblTitle.setText(titleText);
        TextView contentTitle = contentView.findViewById(R.id.user_info_content);
        contentTitle.setText(contentText);
        contentView.findViewById(R.id.lease_immediately_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bottomDialog.cancel();
//                finish();
            }
        });
        contentView.findViewById(R.id.lease_immediately_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("完善信息".equals(titleText)){
//                    getActivity().finish();
                    Intent intent = new Intent(getContext(),PerfectInformationActivity.class);
                    intent.putExtra("nickName",UserNickName);
                    intent.putExtra("userImg",UserImg);
                    intent.putExtra("UserRealname",UserRealname);
                    intent.putExtra("landNo",landNo);
                    intent.putExtra("landId",landId);
                    intent.putExtra("tel",UserTel);
                    intent.putExtra("UserPlotName",UserPlotName);
                    intent.putExtra("UserCertificatesNo",UserCertificatesNo);
                    intent.putExtra("UserPlotId",UserPlotId+"");
                    intent.putExtra("UserGender",UserGender+"");
                    intent.putExtra("UserBirthday",UserBirthday+"");
                    intent.putExtra("UserEmail",UserEmail+"");
                    intent.putExtra("landType",landType+"");
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(getContext(),LoginActivity.class);
                    startActivity(intent);
                }

                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();

    }
    public void ShowDialog1(List<String> list,String titles,int positions, List<Integer> maintainId) {
        Log.e("list",String.valueOf(list));
//        Log.e("state",String.valueOf(maintainId.get(position)));
        Context context = getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_dialog, null);
        ListView myListView =layout.findViewById(R.id.formcustomspinner_list);
        ListDialogAdapter adapter = new ListDialogAdapter(context, list);
        myListView.setAdapter(adapter);
        TextView title = layout.findViewById(R.id.label);
        title.setText(titles);
        layout.findViewById(R.id.list_dialog_bt).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int positon, long id) {
                GetSelfInfo();
                if (alertDialog != null) {
//                    Log.e("balances_required",JSON.toJSONString(response.body()));
                    alertDialog.cancel();
                    double money = listBeanList.get(positions).getChild().get(positon).getDiamond();
                    states();
                    if (states.equals("1")){
                        Toast.makeText(getContext(),"您已处于托管状态",Toast.LENGTH_SHORT).show();
                    } else if (states.equals("0")){
                        payDialog(money, "1", positon, 0, maintainId,listBeanList.get(positions).getMaintainName());
                    }

                }

            }
        });
        builder = new AlertDialog.Builder(context);
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.show();
    }

    //确认金额弹框
    @SuppressLint("SetTextI18n")
    private void payDialog(Double money, String state, int positions, int maintainIds, List<Integer> maintainId,String maintainName) {

        final AlertUtilBest diyDialog =new AlertUtilBest(getActivity());
        diyDialog.setCancel("取消")
                .setOk("确认")
                //.setContent("您本次养护将消耗"+ money +" 元")
                .setContent("您本次养护将消耗"+ money +" 元")
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        Log.d("CuringPage", "ok: "+money+"ok: "+state+"ok: "+positions+"ok: "+maintainId+"ok: "+maintainName);
                        payType = 0;
                        mDialog = LoadingDialog.createLoadingDialog(getContext(),"正在操作");
                                if (state.equals("0")){
                                    addOperation(payType,Integer.valueOf(landId),maintainIds,maintainName);
                                }else {
                                    addOperation(payType,Integer.valueOf(landId),maintainId.get(positions),maintainName);
                                }
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    //操作成功弹窗
    private void NormalDialogOneBtn(String content) {

        final AlertUtilOneButton diyDialog = new AlertUtilOneButton(getContext());
        diyDialog.setOk("我知道了")
                .setContent(content)
                .setDialogClickListener(new AlertUtilOneButton.DialogClickListener() {
                    @Override
                    public void ok() {
                        diyDialog.cancle();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    public List<Double> GetSelfInfo(){
        Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                balances = new ArrayList<>();
//                Log.e("getSelfInfo",String.valueOf(response.body().getResult().getDiamond()));
                balances.add(response.body().getResult().getDiamond());
                //Log.e("getSelfInfo","钻石:"+String.valueOf(balances.get(0))+",积分:"+String.valueOf(balances.get(1))+",用户id:"+balances.get(2));
            }

            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });

        return balances;
    }
    public void getUserInfo(){
        //获取用户信息是否为地主
        retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (("success").equals(response.body().getFlag())){
                    Log.d("UserInfo", JSON.toJSONString( response.body().getResult()));
                    UserNickName = response.body().getResult().getNickname();
                    UserImg = response.body().getResult().getHeadImg();
                    UserTel = response.body().getResult().getTel();
                    UserCertificatesNo = response.body().getResult().getCertificatesNo();
                    UserRealname = response.body().getResult().getRealname();
                    UserPlotId = response.body().getResult().getPlotId();
                    UserPlotName = response.body().getResult().getPlotName();
                    UserGender = response.body().getResult().getGender();
                    UserBirthday = response.body().getResult().getBirthday();
                    UserEmail = response.body().getResult().getEmail();
                    //继电器ip

                    //电lampNumber
                    //水waterNumber
                }else {
                    Toast.makeText(getContext(), "网络错误",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }

    /**
     * 發佈操作指令
     * @param payType 支付類型
     * @param landId 地號
     * @param maintainId 作物id
     */
    public void addOperation(int payType,int landId,int maintainId,String maintainName){
        Log.d("Curing", "addOperation: "+maintainId + maintainName);
        String sellNums = String.valueOf(sellNum);
        Call<CheckSMS> addOperation = HttpHelper.initHttpHelper().addOperation(payType,3,0,null,landId,maintainId);
        addOperation.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                Log.e("response",JSON.toJSONString(response.body()));
                if ("null".equals(JSON.toJSONString(response.body()))){
                    LoadingDialog.closeDialog(mDialog);
                    NormalDialogOneBtn("操作异常，请稍后在试");
                }else {
                    if (("success").equals(response.body().getFlag())){
//                    ShowConfirmDialog("操作成功");
                        LoadingDialog.closeDialog(mDialog);
                        Log.e("Curing",JSON.toJSONString(response.body()));
                        NormalDialogOneBtn("操作指令发布成功");
                        if ("开灯".equals(maintainName) || "开水".equals(maintainName)) {
                            operationChickenCoop(maintainName);
                        }
                    }else{
                        Log.e("cz","钻石不足");
//                    ShowRechargeDialog(content2,0);
                        LoadingDialog.closeDialog(mDialog);
                        NormalDialogOneBtn(response.body().getResult().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                NormalDialogOneBtn("操作异常，请稍后再试");
//                ShowConfirmDialog("操作异常，请稍后在试");
            }
        });

    }

    private String states(){
        SharedPreferences pref = getActivity().getSharedPreferences("User_data",MODE_PRIVATE);
        states = pref.getString("states","");
        return states;
    }


    // 获取租地信息
    public void getLandInfo(){
        retrofit2.Call<AllLand> landInfo = HttpHelper.initHttpHelper().landListByLandId(Integer.parseInt(landId));
        landInfo.enqueue(new Callback<AllLand>() {
            @Override
            public void onResponse(Call<AllLand> call, Response<AllLand> response) {
                Log.d("getLandInfo****",JSON.toJSONString(response.body()));
                if ("null".equals(JSON.toJSONString(response.body()))){
                    Log.e("getLandInfo****","查询失败，请重新查询！！！");
                }else {
                    if (("success").equals(response.body().getFlag())){
                    Log.d("getLandInfo****",JSON.toJSONString(response.body()));
                        lampNumber = response.body().getResult().getList().get(0).getLampNumber();
                        waterNumber = response.body().getResult().getList().get(0).getWaterNumber();
                        relayIp = response.body().getResult().getList().get(0).getRelayIp();
                    }else{

                    }
                }
            }

            @Override
            public void onFailure(Call<AllLand> call, Throwable t) {

            }
        });
    }

    //操作鸡棚
    public void operationChickenCoop(String maintainName) {
        retrofit2.Call<CheckSMS> smsCall = null;
        Log.d("operationChickenCoop", "operationChickenCoop: " + maintainName);
        if ("开灯".equals(maintainName)){
            Log.e("开灯",maintainName);
            Log.e("参数为",lampNumber+"$$$$$"+relayIp +"$$$$$"+ lampNumber);
            smsCall = HttpHelper.initHttpHelper().usrOneController(lampNumber,1,relayIp,1);
        } else if ("注水".equals(maintainName)){
            Log.e("注水",maintainName);
            smsCall = HttpHelper.initHttpHelper().usrOneController(waterNumber,1,relayIp,1);
            Log.e("参数为",relayIp +"$$$$$"+ waterNumber);
        }

        smsCall.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                Log.e("成功了",JSON.toJSONString(response));

                if ("null".equals(JSON.toJSONString(response.body()))){
                    LoadingDialog.closeDialog(mDialog);
                    NormalDialogOneBtn("操作异常，请稍后在试");
                }else {
                    if (("success").equals(response.body().getFlag())){
                        LoadingDialog.closeDialog(mDialog);
                        NormalDialogOneBtn("操作指令发布成功");
                    }else{
                        Log.e("cz","钻石不足");
                        LoadingDialog.closeDialog(mDialog);
                        NormalDialogOneBtn(response.body().getResult().toString());
                    }
                }
                LoadingDialog.closeDialog(mDialog);
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                Log.e("失败了",""+t);
            }
        });

    }




}
