package zhixin.cn.com.happyfarm_user.other;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    private static SimpleDateFormat formatCn = new SimpleDateFormat("yyyy年MM月dd日", Locale.getDefault());
    private static SimpleDateFormat formatY = new SimpleDateFormat("yyyy", Locale.getDefault());
    private static SimpleDateFormat formatM = new SimpleDateFormat("MM", Locale.getDefault());
    private static SimpleDateFormat formatD = new SimpleDateFormat("dd", Locale.getDefault());
    private static SimpleDateFormat format_ = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static SimpleDateFormat strTimeSdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.getDefault());
    private static SimpleDateFormat spotYMD = new SimpleDateFormat("yyyy.MM.dd", Locale.getDefault());
    private static SimpleDateFormat _YMD_HMS = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault());
    private static SimpleDateFormat YMD_HMS = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.getDefault());

    private static long minute = 1000 * 60;
    private static long hour = minute * 60;
    private static long day = hour * 24;
    private static long halfamonth = day * 15;
    private static long month = day * 30;
    public static String formatCn(Date date){
        return formatCn.format(date);
    }
    public static String formatY(Date date){
        return formatY.format(date);
    }
    public static String formatM(Date date){
        return formatM.format(date);
    }
    public static String formatD(Date date){
        return formatD.format(date);
    }
    public static String formatYYYYMMdd(Date date){
        return format_.format(date);
    }
    public  static String getDateDiff(long dateTimeStamp){
        String result;
        long now = new Date().getTime();
        long diffValue = now - dateTimeStamp;
        if(diffValue < 0){
            //toast("结束日期不能小于开始日期！");
        }
        long monthC = diffValue/month;
        long weekC = diffValue/(7*day);
        long dayC = diffValue/day;
        long hourC = diffValue/hour;
        long minC = diffValue/minute;
        if(monthC >= 1) {
            result = Integer.parseInt(monthC+"") + "个月前";
            return result;
        } else if(weekC >= 1) {
            result = Integer.parseInt(weekC+"") + "周前";
            return result;
        } else if(dayC >= 1) {
            result = Integer.parseInt(dayC+"") +"天前";
            return result;
        } else if(hourC >= 1) {
            result = Integer.parseInt(hourC+"") +"个小时前";
            return result;
        } else if(minC >= 1) {
            result = Integer.parseInt(minC+"") +"分钟前";
            return result;
        } else {
            result = "刚刚发表";
            return result;
        }
    }
    //根据时间加月份生成时间
    public static String subMonth(String date,int time){
        Date dt = null;
        try {
            dt = strTimeSdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(dt);
        rightNow.add(Calendar.MONTH, time);
        Date dt1 = rightNow.getTime();
        return strTimeSdf.format(dt1);
    }
    //字符串转时间戳
    public static String getTime(String timeString){
        String timeStamp = null;
        Date d;
        try{
            d = formatCn.parse(timeString);
            long l = d.getTime();
            timeStamp = String.valueOf(l);
        } catch(ParseException e){
            e.printStackTrace();
        }
        return timeStamp;
    }

    //时间戳转字符串
    public static String pointTime(String timeStamp){
        String timeString = null;
        long  l = Long.valueOf(timeStamp);
        timeString = spotYMD.format(new Date(l));//单位秒
        return timeString;
    }
    //时间戳转字符串
    public static String getStrTimeYear(String timeStamp){
        String timeString = null;
        long  l = Long.valueOf(timeStamp);
        timeString = formatCn.format(new Date(l));//单位秒
        return timeString;
    }

    public static String getYearTime(String monthTime){
        if (Integer.parseInt(monthTime) < 12){
            String month = NumUtil.toChineseCharI(monthTime);
            return month+"个月";
        }else {
            int all = Integer.parseInt(monthTime);
            int year = all/12;
            int month = all%12;
            if (month == 0){
                String years = NumUtil.toChineseCharI(String.valueOf(year));
                return years+"年";
            } else{
                String years = NumUtil.toChineseCharI(String.valueOf(year));
                String months = NumUtil.toChineseCharI(String.valueOf(month));
                return years+"年"+months+"个月";
            }
        }
    }
    public static String stampToDate(String s){
        String res;
        long lt = Long.valueOf(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

    //时间戳转字符串
    public static String getStrTime(String timeStamp){
        String timeString = null;
        long  l = Long.valueOf(timeStamp);
        timeString = strTimeSdf.format(new Date(l));//单位秒
        return timeString;
    }

    public static String dateYMD_HMS() {
        return _YMD_HMS.format(new Date());
    }

    public static String dateYMD_HMS(long current) {
        return YMD_HMS.format(new Date(current));
    }
}

