package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class CookingInfo {

    protected ResultBean result;
    protected String flag;
    private int errCode;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public class ResultBean {
        protected int id;
        protected String cookingName;
        protected String cookingImg;
        protected String material;
        protected String nickname;
        protected long startTime;
        protected long updateTime;
        protected Integer praise;
        protected String headImg;
        protected Integer praiseState;
        protected List<CookingDetails> CookingDetails;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCookingName() {
            return cookingName;
        }

        public void setCookingName(String cookingName) {
            this.cookingName = cookingName;
        }

        public String getCookingImg() {
            return cookingImg;
        }

        public void setCookingImg(String cookingImg) {
            this.cookingImg = cookingImg;
        }

        public String getMaterial() {
            return material;
        }

        public void setMaterial(String material) {
            this.material = material;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public Integer getPraise() {
            return praise;
        }

        public void setPraise(Integer praise) {
            this.praise = praise;
        }

        public List<CookingDetails> getCookingDetails() {
            return CookingDetails;
        }

        public void setCookingDetails(List<CookingDetails> cookingDetails) {
            CookingDetails = cookingDetails;
        }

        public String getHeadImg() {
            return headImg;
        }

        public void setHeadImg(String headImg) {
            this.headImg = headImg;
        }

        public Integer getPraiseState() {
            return praiseState;
        }

        public void setPraiseState(Integer praiseState) {
            this.praiseState = praiseState;
        }

        public class  CookingDetails{
            protected int id;
            protected int stepNum;
            protected String cookingExplain;
            protected String cookingImg;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getStepNum() {
                return stepNum;
            }

            public void setStepNum(int stepNum) {
                this.stepNum = stepNum;
            }

            public String getCookingExplain() {
                return cookingExplain;
            }

            public void setCookingExplain(String cookingExplain) {
                this.cookingExplain = cookingExplain;
            }

            public String getCookingImg() {
                return cookingImg;
            }

            public void setCookingImg(String cookingImg) {
                this.cookingImg = cookingImg;
            }


        }
    }

}