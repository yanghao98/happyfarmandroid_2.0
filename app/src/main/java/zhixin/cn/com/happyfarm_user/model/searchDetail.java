package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * Created by Administrator on 2018/5/23.
 */

public class searchDetail {

    /**
     * result : {"pageNum":1,"pageSize":1,"size":1,"orderBy":null,"startRow":0,"endRow":0,"total":1,"pages":1,"list":[{"id":12,"uuid":null,"nickname":"你是一个超级神奇的人物","password":null,"headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head121528108618","tel":"15170029037","email":null,"realname":null,"gender":null,"birthday":null,"plotId":null,"plotName":null,"receiverName":null,"receiverTel":null,"certificates":null,"certificatesNo":null,"diamond":null,"integral":null,"exp":null,"isLandlord":null,"state":null,"landId":null}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 1
         * size : 1
         * orderBy : null
         * startRow : 0
         * endRow : 0
         * total : 1
         * pages : 1
         * list : [{"id":12,"uuid":null,"nickname":"你是一个超级神奇的人物","password":null,"headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head121528108618","tel":"15170029037","email":null,"realname":null,"gender":null,"birthday":null,"plotId":null,"plotName":null,"receiverName":null,"receiverTel":null,"certificates":null,"certificatesNo":null,"diamond":null,"integral":null,"exp":null,"isLandlord":null,"state":null,"landId":null}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 12
             * uuid : null
             * nickname : 你是一个超级神奇的人物
             * password : null
             * headImg : http://7xtaye.com2.z0.glb.clouddn.com/head121528108618
             * tel : 15170029037
             * email : null
             * realname : null
             * gender : null
             * birthday : null
             * plotId : null
             * plotName : null
             * receiverName : null
             * receiverTel : null
             * certificates : null
             * certificatesNo : null
             * diamond : null
             * integral : null
             * exp : null
             * isLandlord : null
             * state : null
             * landId : null
             */

            private int id;
            private Object uuid;
            private String nickname;
            private Object password;
            private String headImg;
            private String tel;
            private Object email;
            private Object realname;
            private Object gender;
            private Object birthday;
            private Object plotId;
            private Object plotName;
            private Object receiverName;
            private Object receiverTel;
            private Object certificates;
            private Object certificatesNo;
            private Object diamond;
            private Object integral;
            private Object exp;
            private Object isLandlord;
            private Object state;
            private Object landId;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public Object getUuid() {
                return uuid;
            }

            public void setUuid(Object uuid) {
                this.uuid = uuid;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public Object getPassword() {
                return password;
            }

            public void setPassword(Object password) {
                this.password = password;
            }

            public String getHeadImg() {
                return headImg;
            }

            public void setHeadImg(String headImg) {
                this.headImg = headImg;
            }

            public String getTel() {
                return tel;
            }

            public void setTel(String tel) {
                this.tel = tel;
            }

            public Object getEmail() {
                return email;
            }

            public void setEmail(Object email) {
                this.email = email;
            }

            public Object getRealname() {
                return realname;
            }

            public void setRealname(Object realname) {
                this.realname = realname;
            }

            public Object getGender() {
                return gender;
            }

            public void setGender(Object gender) {
                this.gender = gender;
            }

            public Object getBirthday() {
                return birthday;
            }

            public void setBirthday(Object birthday) {
                this.birthday = birthday;
            }

            public Object getPlotId() {
                return plotId;
            }

            public void setPlotId(Object plotId) {
                this.plotId = plotId;
            }

            public Object getPlotName() {
                return plotName;
            }

            public void setPlotName(Object plotName) {
                this.plotName = plotName;
            }

            public Object getReceiverName() {
                return receiverName;
            }

            public void setReceiverName(Object receiverName) {
                this.receiverName = receiverName;
            }

            public Object getReceiverTel() {
                return receiverTel;
            }

            public void setReceiverTel(Object receiverTel) {
                this.receiverTel = receiverTel;
            }

            public Object getCertificates() {
                return certificates;
            }

            public void setCertificates(Object certificates) {
                this.certificates = certificates;
            }

            public Object getCertificatesNo() {
                return certificatesNo;
            }

            public void setCertificatesNo(Object certificatesNo) {
                this.certificatesNo = certificatesNo;
            }

            public Object getDiamond() {
                return diamond;
            }

            public void setDiamond(Object diamond) {
                this.diamond = diamond;
            }

            public Object getIntegral() {
                return integral;
            }

            public void setIntegral(Object integral) {
                this.integral = integral;
            }

            public Object getExp() {
                return exp;
            }

            public void setExp(Object exp) {
                this.exp = exp;
            }

            public Object getIsLandlord() {
                return isLandlord;
            }

            public void setIsLandlord(Object isLandlord) {
                this.isLandlord = isLandlord;
            }

            public Object getState() {
                return state;
            }

            public void setState(Object state) {
                this.state = state;
            }

            public Object getLandId() {
                return landId;
            }

            public void setLandId(Object landId) {
                this.landId = landId;
            }
        }
    }
}
