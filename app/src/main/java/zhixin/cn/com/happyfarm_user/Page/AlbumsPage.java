package zhixin.cn.com.happyfarm_user.Page;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.CuringAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.MyAlbumAdapter;
import zhixin.cn.com.happyfarm_user.AlbumDetailsActivity;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.MyAlbum;
import zhixin.cn.com.happyfarm_user.model.SearchPhotoAlbum;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.other.GridSpacingItemDecoration;
import zhixin.cn.com.happyfarm_user.utils.Config;

public class AlbumsPage extends Fragment {

    private ArrayList<MyAlbum> mdata = new ArrayList<>();
    private RecyclerView collection_recycle;
    private Dialog dialog;
    private RefreshLayout refreshLayout;
    private RelativeLayout myAlbumPrompt;
    private MyAlbumAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private View myView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(getContext(), R.layout.albums_page_activity,null);
        myView = view;
        //加载没有内容提示布局
        myAlbumPrompt = view.findViewById(R.id.vip_album_prompt);
        //加载之前先让暂时还没有内容不提示
        myAlbumPrompt.setVisibility(View.GONE);
        getActivity();
        pageNum = 1;
        pageSize = 5;

        refreshLayout = (RefreshLayout) view.findViewById(R.id.vip_refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
//        refreshLayout.setRefreshHeader(new MaterialHeader(getActivity()).setShowBezierWave(false));

        initRecyclerView();
//        StaggerLoadData(false,1);
        if (!mdata.isEmpty()) {
            mdata.clear();
        }
        StaggerLoadData(false, 1);
        topRefreshLayout();
        AutoLoading();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Config.myAlbumRefresh){
            Config.myAlbumRefresh = false;
            if (!mdata.isEmpty()) {
                mdata.clear();
            }
            StaggerLoadData(false, 1);
        }
    }

    private void StaggerLoadData(Boolean inversion, int pageNum) {
        dialog = LoadingDialog.createLoadingDialog(getActivity(), "数据正在加载");
//        Log.d(TAG, "StaggerLoadData: "+"数据正在加载");
        HttpHelper.initHttpHelper().searchPhotoAlbum(0, pageNum, 5).enqueue(new Callback<SearchPhotoAlbum>() {
            @Override
            public void onResponse(Call<SearchPhotoAlbum> call, Response<SearchPhotoAlbum> response) {
//                Log.e("MyAlbumActivity->", JSON.toJSONString(response.body().getResult().getList()));
                if (("success").equals(response.body().getFlag())) {
                    isLastPage = response.body().getResult().isIsLastPage();
                    if (String.valueOf(response.body().getResult().getList()).equals("[]")) {
                        myAlbumPrompt.setVisibility(View.VISIBLE);
                        collection_recycle.setVisibility(View.GONE);
                        LoadingDialog.closeDialog(dialog);
                    } else {

                        myAlbumPrompt.setVisibility(View.GONE);
                        collection_recycle.setVisibility(View.VISIBLE);
                        for (int i = 0; i < response.body().getResult().getList().size(); i++) {
//                            Log.d(TAG,"成长相册里的相册详情："+ JSON.toJSONString(response.body().getResult().getList().get(i).getAlbumFirstPicture()));
                            MyAlbum myAlbum = new MyAlbum();
                            myAlbum.albumName = response.body().getResult().getList().get(i).getAlbumName();
                            myAlbum.photoAbstract = String.valueOf(response.body().getResult().getList().get(i).getPhotoAbstract());
                            myAlbum.isPublic = String.valueOf(response.body().getResult().getList().get(i).getIsPublic());
                            myAlbum.videoUrl = response.body().getResult().getList().get(i).getPhotoUrl();
                            myAlbum.Times = DateUtil.getStrTime(String.valueOf(response.body().getResult().getList().get(i).getStartTime()));
                            myAlbum.albumFirstPicture = response.body().getResult().getList().get(i).getAlbumFirstPicture();
//                            Log.e("MyAlbumActivity->", DateUtil.getStrTime(String.valueOf(response.body().getResult().getList().get(i).getStartTime())));
                            myAlbum.id = String.valueOf(response.body().getResult().getList().get(i).getId());
                            mdata.add(myAlbum);
                        }
                        LoadingDialog.closeDialog(dialog);
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    myAlbumPrompt.setVisibility(View.VISIBLE);
                    collection_recycle.setVisibility(View.GONE);
                    LoadingDialog.closeDialog(dialog);
                }
            }

            @Override
            public void onFailure(Call<SearchPhotoAlbum> call, Throwable t) {
                myAlbumPrompt.setVisibility(View.VISIBLE);
                collection_recycle.setVisibility(View.GONE);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }

    private void initRecyclerView() {

        int spanCount = 1;
        int spacing = 50;
        boolean includeEdge = true;
        collection_recycle = myView.findViewById(R.id.vip_album_recycle);

        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new MyAlbumAdapter(getContext(), mdata);
        //设置适配器
        collection_recycle.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);

        collection_recycle.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        collection_recycle.setLayoutManager(gridLayoutManager);
        //TODO 设置一行一个 ，左右间距 50
        collection_recycle.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        adapter.setItemClickListener(new CuringAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getContext(), AlbumDetailsActivity.class);
                intent.putExtra("albumName", mdata.get(position).albumName);
                intent.putExtra("photoAbstract", mdata.get(position).photoAbstract);
                intent.putExtra("isPublic", mdata.get(position).isPublic);
                intent.putExtra("videoUrl", mdata.get(position).videoUrl);
                intent.putExtra("id", mdata.get(position).id);
                intent.putExtra("userGrowthAlbum",mdata.get(position).albumFirstPicture);
                startActivity(intent);
            }
        });
    }

    public void AutoLoading() {
        //上拉滑动自动请求数据
        collection_recycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                    if (lastVisiblePosition >= gridLayoutManager.getItemCount() - 1) {
                        if (isLastPage == true) {
//                            Toast.makeText(getApplication(), "没有更多数据", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            StaggerLoadData(false, pageNum);
                        }
                    }
                }
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (mdata != null)
                    mdata.clear();
                StaggerLoadData(false, 1);
                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }

//    public void back(View view) {
//        finish();
//    }
}
