package zhixin.cn.com.happyfarm_user.Page;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.WelfareUsedAdapter;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.LuckDrawRecord;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;

public class WelfareUsedPage extends Fragment {

    private View myView;
    private RecyclerView recyclerView;
    private RefreshLayout refreshLayout;
    private WelfareUsedAdapter welfareUsedAdapter;
    private List<LuckDrawRecord.LuckDrawBean.LuckDrawList> list;
    private GridLayoutManager gridLayoutManager;
    private String TAG = "WelfareUsedPage";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.welfare_used_page_activity,null,false);
        view.findViewById(R.id.welfare_used_my_prompt).setVisibility(View.VISIBLE);
        view.findViewById(R.id.welfare_used_recycler_view).setVisibility(View.GONE);
        myView = view;
        initView(view);
        return view;
    }

    private void initView(View view){
        recyclerView = view.findViewById(R.id.welfare_used_recycler_view);
        refreshLayout = view.findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()).setShowBezierWave(false));
        topRefreshLayout();
        tokenTest();
    }

    private void initWelfreUsedAdapter() {
        welfareUsedAdapter = new WelfareUsedAdapter(getContext(),list);
        recyclerView.setAdapter(welfareUsedAdapter);
        gridLayoutManager = new GridLayoutManager(getContext(),1);
        gridLayoutManager.setReverseLayout(false);
        recyclerView.setLayoutManager(gridLayoutManager);
    }
    /**
     * 查询抽奖记录 （福利卡）
     */
    public void searchUsedLuckDrawRecordList(){
        HttpHelper.initHttpHelper().searchLuckDrawRecondList(0).enqueue(new Callback<LuckDrawRecord>() {
            @Override
            public void onResponse(Call<LuckDrawRecord> call, Response<LuckDrawRecord> response) {
//                Log.i(TAG, "onResponse: "+ JSON.toJSONString(response.body().getResult()));
                if ("success".equals(response.body().getFlag())) {
                    list = response.body().getResult().getList();
//                    System.out.println(JSON.toJSONString(list));
                    if (0 == list.size()) {
                        myView.findViewById(R.id.welfare_used_my_prompt).setVisibility(View.VISIBLE);
                        myView.findViewById(R.id.welfare_used_recycler_view).setVisibility(View.GONE);
                    } else {
                        myView.findViewById(R.id.welfare_used_my_prompt).setVisibility(View.GONE);
                        myView.findViewById(R.id.welfare_used_recycler_view).setVisibility(View.VISIBLE);
                    }
                    initWelfreUsedAdapter();
                } else {
                    myView.findViewById(R.id.welfare_used_my_prompt).setVisibility(View.VISIBLE);
                    myView.findViewById(R.id.welfare_used_recycler_view).setVisibility(View.GONE);
                    Toast.makeText(myView.getContext(),"数据查询失败",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LuckDrawRecord> call, Throwable t) {
                Toast.makeText(myView.getContext(),getResources().getString(R.string.no_network),Toast.LENGTH_SHORT).show();
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                System.out.println("进来到刷新方法了");
                //判断有没有数据,有的话清零
                if (!list.isEmpty()){
                    list.clear();
                }
                searchUsedLuckDrawRecordList();
                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }
    public void tokenTest() {
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("failed".equals(response.body().getFlag()))) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(getContext(), "User_data", strArr);
                } else if (("success").equals(response.body().getFlag())) {
                    myView.findViewById(R.id.welfare_used_my_prompt).setVisibility(View.GONE);
                    myView.findViewById(R.id.welfare_used_recycler_view).setVisibility(View.VISIBLE);
                    initWelfreUsedAdapter();
                    searchUsedLuckDrawRecordList();
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        tokenTest();
    }
}
