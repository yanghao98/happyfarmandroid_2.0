package zhixin.cn.com.happyfarm_user.utils;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import zhixin.cn.com.happyfarm_user.R;

public class SignInDialog extends Dialog implements View.OnClickListener {

    private ImageView imageView;
//    private ImageView iconON;
    private Context myContext;
    private TextView text;

    public SignInDialog(@NonNull Context context, int type) {
        super(context);
        setContentView(R.layout.sign_in_dialog);
        myContext = context;
        imageView = (ImageView) findViewById(R.id.sign_in_image);
        text = findViewById(R.id.sign_in_content);
//        iconON = findViewById(R.id.sign_in_icon_on);
//        iconON.setOnClickListener(this);
        switch (type) {
            case 1:
                Glide.with(myContext).load(R.mipmap.sign_in_henhouse_yes).into(imageView);
                text.setText("鸡窝搭建成功了，\n继续连签获得小鸡");
                break;
            case 2:
                Glide.with(myContext).load(R.mipmap.sign_in_incubation_yes).into(imageView);
                text.setText("小鸡孵化成功了，\n继续连签攒月度饲料");
                break;
            case 3:
                Glide.with(myContext).load(R.mipmap.sign_in_feed_yes).into(imageView);
                text.setText("获得月度饲料包了，\n继续加油");
                break;
            case 4:
                Glide.with(myContext).load(R.mipmap.sign_in_chicken_yes).into(imageView);
                text.setText("获得一年期植信鸡，\n请联系客服领取");
                break;
        }
    }
    public void showDialog() {
        Window window = getWindow();
        window.setWindowAnimations(R.style.style_dialog);
        window.setBackgroundDrawableResource(R.color.no_background);
        WindowManager.LayoutParams wl = window.getAttributes();
        //设置弹窗位置
        wl.gravity = Gravity.CENTER;
        window.setAttributes(wl);
        show();
    }

    @Override
    public void onClick(View view) {
//        if (view.getId() == R.id.sign_in_icon_on) {
////            closeAnimations();
//            System.out.println("点击了 关闭按钮");
//            dismiss();
//        }
    }
}
