package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import zhixin.cn.com.happyfarm_user.R;

public class TransactionDetailsAdapter extends RecyclerView.Adapter<TransactionDetailsAdapter.StaggerViewHolder> {


    private List<Map<String,Object>> myList = new ArrayList<>();
    private Context mContext;

    public TransactionDetailsAdapter(Context context,List<Map<String,Object>> list){
        this.mContext = context;
        this.myList = list;
    }
    @NonNull
    @Override
    public TransactionDetailsAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.transaction_details_adapter, null);
        //创建一个staggerViewHolder对象
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull StaggerViewHolder staggerViewHolder, int i) {
//从集合里拿对应item数据对象
        Map<String,Object> dataBean = myList.get(i);
        //给Holder里面的控件对象设置数据
        setData(staggerViewHolder,dataBean);
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (myList != null && myList.size() > 0) {
            return myList.size();
        }
        return 0;
    }

    public void setData(TransactionDetailsAdapter.StaggerViewHolder holder,Map<String,Object> data) {
//        Log.i("得到的数据类型：", "setData: " + JSON.toJSONString(data));
        for (String key : data.keySet()) {
            System.out.println("key= " + key + " and value= " + data.get(key));
            holder.dynamicTime.setText(data.get("time")+"");
            holder.mTextContent.setText(data.get("content")+"");
            if (1 == Integer.valueOf(data.get("type").toString())){
                holder.increase_text.setVisibility(View.VISIBLE);
                holder.reduce_text.setVisibility(View.GONE);
            } else {
                holder.increase_text.setVisibility(View.GONE);
                holder.reduce_text.setVisibility(View.VISIBLE);
            }
        }
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextContent;
        private TextView dynamicTime;
        private TextView increase_text;
        private TextView reduce_text;
        public StaggerViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextContent = itemView.findViewById(R.id.text_content);
            dynamicTime = itemView.findViewById(R.id.dynamic_time);
            increase_text = itemView.findViewById(R.id.increase_text);
            reduce_text = itemView.findViewById(R.id.reduce_text);
        }
    }
}
