package zhixin.cn.com.happyfarm_user.EventBus;

/**
 * Created by Administrator on 2018/7/25.
 */

public class RefreshGroup {

    public String refreshGroup;

    public RefreshGroup(String refreshGroup) {
        this.refreshGroup = refreshGroup;
    }

    public String getRefreshGroup() {
        return refreshGroup;
    }

    public void setRefreshGroup(String refreshGroup) {
        this.refreshGroup = refreshGroup;
    }

}
