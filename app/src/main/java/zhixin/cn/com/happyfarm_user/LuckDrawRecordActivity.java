package zhixin.cn.com.happyfarm_user;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.githang.statusbar.StatusBarCompat;

import zhixin.cn.com.happyfarm_user.Page.AllGoodPage;
import zhixin.cn.com.happyfarm_user.Page.AllWelfarePage;

public class LuckDrawRecordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lucke_draw_record);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.personal_content,new AllWelfarePage())
                .commit();
    }

    public void back(View view) {
        finish();
    }

}
