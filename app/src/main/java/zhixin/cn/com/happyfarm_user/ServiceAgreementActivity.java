package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;

import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;

/**
 * ServiceAgreementActivity
 *
 * @author: Administrator.
 * @date: 2019/7/4
 */
public class ServiceAgreementActivity extends ABaseActivity {

    private RelativeLayout titleBarLayout;
    private RelativeLayout aggServiceLayout;
    private RelativeLayout privacyServiceLayout;
    private RelativeLayout useServiceLayout;
    private RelativeLayout memberServiceLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTransparent();
        setContentView(R.layout.service_agreement_layout);
        initView();
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        setStatusBarHeight();
        aggServiceClick();
        privacyServiceClick();
        useServiceClick();
        memberServiceClick();
    }
    private void initView() {
        TextView title = findViewById(R.id.title_name);
        title.setText(getResources().getString(R.string.service_agreement));
        titleBarLayout = findViewById(R.id.title_bar_layout);
        aggServiceLayout = findViewById(R.id.agg_service_layout);
        privacyServiceLayout = findViewById(R.id.privacy_service_layout);
        useServiceLayout = findViewById(R.id.use_service_layout);
        memberServiceLayout = findViewById(R.id.member_service_layout);
    }

    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(this);
        //相对布局使用方法
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        titleBarLayout.setLayoutParams(params);
    }

    /**
     * 鸡蛋币使用协议
     */
    private void aggServiceClick() {
        aggServiceLayout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                Intent intent = new Intent(ServiceAgreementActivity.this,TermsActivity.class);
                intent.putExtra("termsName", "egg");//鸡蛋
                intent.putExtra("url","");//因为帮助里面的操作说明也用到了TermsActivity页面所以穿个空url进去进行区分
                startActivity(intent);
            }
        });
    }

    /**
     * 隐私协议
     */
    private void privacyServiceClick() {
        privacyServiceLayout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                Intent intent = new Intent(ServiceAgreementActivity.this,TermsActivity.class);
                intent.putExtra("termsName", "privacy");//隐私
                intent.putExtra("url","");//因为帮助里面的操作说明也用到了TermsActivity页面所以穿个空url进去进行区分
                startActivity(intent);
            }
        });
    }

    /**
     * 用户协议
     */
    private void useServiceClick() {
        useServiceLayout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                Intent intent = new Intent(ServiceAgreementActivity.this,TermsActivity.class);
                intent.putExtra("termsName", "agreement");//用户
                intent.putExtra("url","");//因为帮助里面的操作说明也用到了TermsActivity页面所以穿个空url进去进行区分
                startActivity(intent);
            }
        });
    }

    /**
     * 会员协议
     */
    private void memberServiceClick() {
        memberServiceLayout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                Intent intent = new Intent(ServiceAgreementActivity.this,TermsActivity.class);
                intent.putExtra("termsName", "member");//会员
                intent.putExtra("url","");//因为帮助里面的操作说明也用到了TermsActivity页面所以穿个空url进去进行区分
                startActivity(intent);
            }
        });
    }

    @Override
    public void back(View view) {
        finish();
    }
}
