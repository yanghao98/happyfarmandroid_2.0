package zhixin.cn.com.happyfarm_user;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pili.pldroid.player.AVOptions;
import com.pili.pldroid.player.widget.PLVideoTextureView;
import com.pili.pldroid.player.widget.PLVideoView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.EventBus.InfoEvent;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.other.OSUtils;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.TextString;


public class FullScreenActivity extends ABaseActivity {

    private PLVideoTextureView mVideoView;
    private LinearLayout prompt;
    private Button prompt_btn;
    private ImageButton seekBarAdd;
    private ImageButton seekBarDelete;
    private TextView resetBtn;
    private TextView settingBtn;
    private String videoPath;
    private ImageButton upper;
    private ImageButton left;
    private ImageButton right;
    private ImageButton lower;
    private int landNo;
    private int networkType = 1; //networkType:网络类型(1 4G,2 wifi)
    AVOptions options = new AVOptions();
    private final String TAG = "FullScreenActivity->";
    private RelativeLayout videoBigTipsLayout;
    private RelativeLayout loadingView;
    private ImageView audioButton;
    private TextView audioText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_screen_layout);

        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //注册
        EventBus.getDefault().register(this);
        initView();

        Intent intent = getIntent();
        //获取
        Bundle data2=intent.getExtras();//获得Bundle对象
        videoPath = intent.getStringExtra("videoPath");
        landNo = Integer.parseInt(intent.getStringExtra("landNo"));
//        Log.i(TAG, String.valueOf(landNo));
//        Log.i(TAG,videoPath);
        findViewById(R.id.back_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoView.stopPlayback();//释放资源
                mVideoView.pause();//暂停播放
                finish();
            }
        });
        mVideoView.setBufferingIndicator(loadingView);
//        String path = "rtmp://live.hkstv.hk.lxdns.com/live/hks";
        mVideoView.setVideoPath(videoPath);
        mVideoView.setDisplayAspectRatio(PLVideoView.ASPECT_RATIO_PAVED_PARENT);
        mVideoView.start();
        options.setInteger(AVOptions.KEY_OPEN_RETRY_TIMES, 5);
        hideBottomUIMenu();

        checkAudio();

        //滑动条相关方法
        addSeekBar();
        deleteSeekBar();
        //mySeekBar();
        //滑动条相关方法

        resetButton();
        settingButton();
        upperButton();
        lowerButton();
        leftButton();
        rightButton();
        audioSwitch();

        if(NumUtil.checkNull(Config.devNo)) {
            //TODO 6.0之后需要向用户请求授权
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.READ_PHONE_STATE},1);
            } else {
                try {
                    Config.devNo = OSUtils.getIMEI(this);
                } catch (Exception e) {
                    Config.devNo = "";
                    e.printStackTrace();
                }
            }
        }
        if (NetWorkUtils.isWifiConnected(this)) {
            networkType = 2;
        }
    }

    private void initView() {
        mVideoView = findViewById(R.id.full_PLVideoTextureView);
        loadingView = findViewById(R.id.full_LoadingView);
        prompt = findViewById(R.id.prompt);
        prompt_btn = findViewById(R.id.prompt_btn);
        seekBarAdd = findViewById(R.id.add_seekBar);
        seekBarDelete = findViewById(R.id.delete_seekBar);
        resetBtn = findViewById(R.id.reset_text);
        settingBtn = findViewById(R.id.setting_text);
        upper = findViewById(R.id.upper);
        left = findViewById(R.id.left);
        right = findViewById(R.id.right);
        lower = findViewById(R.id.lower);
        videoBigTipsLayout = findViewById(R.id.video_big_tips_layout);
        audioButton = findViewById(R.id.audio_big_button);
        audioText = findViewById(R.id.audio_big_text);
    }

    @Override
    protected void onStart() {
        super.onStart();
        showToast(getResources().getString(R.string.video_big_tips));
    }

    private synchronized void showToast(String text) {
        audioText.setText(text);
        ObjectAnimator.ofFloat(videoBigTipsLayout,"alpha",0,1,1,0)
                .setDuration(8000)
                .start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            //此处1 ，2  与上方打开的1，2对应
            case 1:
                //判断是否有权限
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Config.devNo = OSUtils.getIMEI(this);
                } else {
                    Config.devNo = "";
                }
                break;
            default:
                break;
        }
    }
    /**
     * 检查音频声音是否开启
     */
    private void checkAudio() {
        if (Config.audioIsPlay) {
            audioButton.setImageResource(R.drawable.audio_open);
        } else {
            audioButton.setImageResource(R.drawable.audio_close);
        }
    }

    /**
     * 直播声音开关
     */
    private void audioSwitch(){
        audioButton.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (Config.audioIsPlay) {
                    //播放
                    Config.audioIsPlay = false;
                    audioButton.setImageResource(R.drawable.audio_close);
                    //mVideoView2.pause();
                    LandVideoActivity.mVideoView2.setVolume(0,0);
                    showToast("声音已关闭");
                } else {
                    //未播放
                    Config.audioIsPlay = true;
                    audioButton.setImageResource(R.drawable.audio_open);
                    //mVideoView2.start();
                    LandVideoActivity.mVideoView2.setVolume(10,10);
                    showToast("声音已开启");
                }
            }
        });
    }
    //上
    private void upperButton(){
        upper.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        upper.setBackgroundResource(R.drawable.icon_upper_selected);
                        cameraController(0,0, Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        upper.setBackgroundResource(R.drawable.icon_upper);
                        cameraController(0,1,Config.previousCommand);
                        Config.previousCommand = 0;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }
    //下
    private void lowerButton(){
        lower.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        lower.setBackgroundResource(R.drawable.icon_lower_selected);
                        cameraController(1,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        lower.setBackgroundResource(R.drawable.icon_lower);
                        cameraController(1,1,Config.previousCommand);
                        Config.previousCommand = 1;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }
    //左
    private void leftButton(){
        left.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        left.setBackgroundResource(R.drawable.icon_left_selected);
                        cameraController(2,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        left.setBackgroundResource(R.drawable.icon_left);
                        cameraController(2,1,Config.previousCommand);
                        Config.previousCommand = 2;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }
    //右
    private void rightButton(){
        right.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        right.setBackgroundResource(R.drawable.icon_right_selected);
                        cameraController(3,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        right.setBackgroundResource(R.drawable.icon_right);
                        cameraController(3,1,Config.previousCommand);
                        Config.previousCommand = 3;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }
    //复位
    private void resetButton(){
        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraController(10,0,-1);
                //seekBar.setProgress(0);
            }
        });
    }
    //设置视角settingBtn
    private void settingButton(){
        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraController(11,0,-1);
            }
        });
    }
    //滑动条加
    private void addSeekBar(){
//        seekBarAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int i = seekBar.getProgress();
//                seekBar.setProgress(i + 10);
//            }
//        });
        seekBarAdd.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        seekBarAdd.setImageResource(R.mipmap.add_selected);
                        cameraController(4,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        seekBarAdd.setImageResource(R.mipmap.icon_web_42);
                        cameraController(4,1,Config.previousCommand);
                        Config.previousCommand = 4;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }
    //滑动条减
    private void deleteSeekBar(){
//        seekBarDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int i = seekBar.getProgress();
//                seekBar.setProgress(i - 10);
//            }
//        });
        seekBarDelete.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        seekBarDelete.setImageResource(R.mipmap.delete_selected);
                        cameraController(5,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        seekBarDelete.setImageResource(R.mipmap.icon_web_43);
                        cameraController(5,1,Config.previousCommand);
                        Config.previousCommand = 5;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }

    private void cameraController(int dwPTZCommand,int dwStop,int previousCommand){
        long clientTime = System.currentTimeMillis(); //客户端点击按钮时间
        int clientType = 2; //客户端类型(1 Web,2 Android,3 IOS)
        //int networkType = 0; //网络类型(1 4G,2 wifi)
        HttpHelper.initHttpHelper().cameraController(landNo,dwPTZCommand,dwStop,previousCommand,clientTime,clientType,networkType,Config.devNo).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                String rescult = "";
                try{
                    rescult = response.body().getFlag();
                }catch (Exception e){
                    showToastShort(FullScreenActivity.this, "请求错误");
                    return;
                }
                if ("success".equals(rescult)) {
//                    Log.i(TAG, "onResponse: "+"摄像头操作成功");
//                    if (dwPTZCommand == 4 && dwStop == 0){
//                        int i = seekBar.getProgress();
//                        seekBar.setProgress(i + 10);
//                    }
//                    if (dwPTZCommand == 5 && dwStop == 0){
//                        int i = seekBar.getProgress();
//                        seekBar.setProgress(i - 10);
//                    }
//                    if (dwStop == 0){
//                        Toast.makeText(FullScreenActivity.this, "操作进行中", Toast.LENGTH_SHORT).show();
//                    }else {
//                        Toast.makeText(FullScreenActivity.this, "操作已结束", Toast.LENGTH_SHORT).show();
//                    }

                } else {
//                    Log.e(TAG, "onResponse: "+"摄像头操作失败");
                    showToastShort(FullScreenActivity.this, response.body().getResult().toString());
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
//                Log.e(TAG, "onFailure: "+"摄像头操作异常"+t);
                showToastShort(FullScreenActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }
    /**
     * 事件响应方法
     * 接收消息
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(InfoEvent event) {

        String msg = event.getINFOSUCCESSFUL();
        if ("进入排队".equals(msg) ){
            mVideoView.pause();//暂停播放
            mVideoView.stopPlayback();//释放资源
            finish();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
//        recLen = Integer.parseInt(intent.getStringExtra("recLen"));
//        timer.schedule(task, 1000, 1000);
//        Log.i("放大视频页数字打印：",String.valueOf(recLen));
//        if (isWifi(FullScreenActivity.this)){
            mVideoView.start();
//        }else {
//            prompt.setVisibility(View.VISIBLE);
//            prompt_btn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
                    prompt.setVisibility(View.GONE);
//                    mVideoView.start();
//                }
//            });
//        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mVideoView.stopPlayback();//释放资源
        //释放通知
        EventBus.getDefault().unregister(this);
    }

    protected void hideBottomUIMenu() {
        //隐藏虚拟按键，并且全屏
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }
    /**
     * 重写返回键
     * 监听Back键按下事件,方法1:
     * 注意:
     * super.onBackPressed()会自动调用finish()方法,关闭
     * 当前Activity.
     * 若要屏蔽Back键盘,注释该行代码即可
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mVideoView.stopPlayback();//释放资源
        mVideoView.pause();//暂停播放
    }

    @Override
    public void back(View view) {

    }
}
