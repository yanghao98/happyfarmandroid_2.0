package zhixin.cn.com.happyfarm_user.easyMob;

import java.util.List;

/**
 * Created by Administrator on 2018/6/22.
 */

public class searchGroup {

    /**
     * result : {"listMembers":[{"headImg":"http://7xtaye.com2.z0.glb.clouddn.com/headImg_6_1529500216206","nickname":"周子龙","userId":6}],"listAdmins":[],"listOwners":[{"headImg":"http://7xtaye.com2.z0.glb.clouddn.com/head51529492725","nickname":"胡月玲","userId":5}]}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        private List<ListMembersBean> listMembers;
        private List<ListAdminsBean> listAdmins;
        private List<ListOwnersBean> listOwners;

        public List<ListMembersBean> getListMembers() {
            return listMembers;
        }

        public void setListMembers(List<ListMembersBean> listMembers) {
            this.listMembers = listMembers;
        }

        public List<ListAdminsBean> getListAdmins() {
            return listAdmins;
        }

        public void setListAdmins(List<ListAdminsBean> listAdmins) {
            this.listAdmins = listAdmins;
        }

        public List<ListOwnersBean> getListOwners() {
            return listOwners;
        }

        public void setListOwners(List<ListOwnersBean> listOwners) {
            this.listOwners = listOwners;
        }

        public static class ListMembersBean {
            /**
             * headImg : http://7xtaye.com2.z0.glb.clouddn.com/headImg_6_1529500216206
             * nickname : 周子龙
             * userId : 6
             */

            private String headImg;
            private String nickname;
            private int userId;
            private String uuid;

            public String getHeadImg() {
                return headImg;
            }

            public void setHeadImg(String headImg) {
                this.headImg = headImg;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }
        }
        public static class ListAdminsBean {
            /**
             * headImg : http://7xtaye.com2.z0.glb.clouddn.com/headImg_6_1529500216206
             * nickname : 周子龙
             * userId : 6
             */

            private String headImg;
            private String nickname;
            private int userId;
            private String uuid;

            public String getHeadImg() {
                return headImg;
            }

            public void setHeadImg(String headImg) {
                this.headImg = headImg;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }
        }
        public static class ListOwnersBean {
            /**
             * headImg : http://7xtaye.com2.z0.glb.clouddn.com/head51529492725
             * nickname : 胡月玲
             * userId : 5
             */

            private String headImg;
            private String nickname;
            private int userId;
            private String uuid;

            public String getHeadImg() {
                return headImg;
            }

            public void setHeadImg(String headImg) {
                this.headImg = headImg;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }
        }
    }
}
