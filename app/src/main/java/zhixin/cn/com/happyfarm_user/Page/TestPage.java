package zhixin.cn.com.happyfarm_user.Page;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import zhixin.cn.com.happyfarm_user.MainActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.base.BaseFragment;

/**
 * TestPage
 * 使用基类
 * @author: Administrator.
 * @date: 2019/5/31
 */
public class TestPage extends BaseFragment {
    private static final String TAG = TestPage.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {

    }

    @Override
    protected void onFragmentVisibleChange(boolean isVisible) {
        if (isVisible) {
            //更新界面数据，如果数据还在下载中，就显示加载框

        } else {
            //关闭加载框

        }
    }

    @Override
    protected void onFragmentFirstVisible() {
        //去服务器下载数据

    }
}
