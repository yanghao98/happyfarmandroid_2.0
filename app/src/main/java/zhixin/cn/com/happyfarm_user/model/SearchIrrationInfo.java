package zhixin.cn.com.happyfarm_user.model;

import com.google.gson.Gson;

import java.util.List;

public class SearchIrrationInfo {

    private ResultBean result;
    private String flag;

    public static SearchIrrationInfo objectFromData(String str) {

        return new Gson().fromJson(str, SearchIrrationInfo.class);
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {

        private int pageNum;
        private int pageSize;
        private int size;
        private Object orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public static ResultBean objectFromData(String str) {

            return new Gson().fromJson(str, ResultBean.class);
        }

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Object orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * taskId : 281
             * landNo : 182
             * userName : XiaoMing
             * staffNo : null
             * maintainName : 无机肥
             * cropName : 白菜
             * cropNum : 0
             * harvestWhere : -1
             * isIrration : -1
             * irrationReason : null
             * taskStarTime : 1524542973000
             * operationTime : 1524542973000
             * operationState : -1
             */

            private int taskId;
            private int landNo;
            private String userName;
            private Object staffNo;
            private String maintainName;
            private String cropName;
            private int cropNum;
            private int harvestWhere;
            private int isIrration;
            private Object irrationReason;
            private long taskStarTime;
            private long operationTime;
            private int operationState;

            public static ListBean objectFromData(String str) {

                return new Gson().fromJson(str, ListBean.class);
            }

            public int getTaskId() {
                return taskId;
            }

            public void setTaskId(int taskId) {
                this.taskId = taskId;
            }

            public int getLandNo() {
                return landNo;
            }

            public void setLandNo(int landNo) {
                this.landNo = landNo;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public Object getStaffNo() {
                return staffNo;
            }

            public void setStaffNo(Object staffNo) {
                this.staffNo = staffNo;
            }

            public String getMaintainName() {
                return maintainName;
            }

            public void setMaintainName(String maintainName) {
                this.maintainName = maintainName;
            }

            public String getCropName() {
                return cropName;
            }

            public void setCropName(String cropName) {
                this.cropName = cropName;
            }

            public int getCropNum() {
                return cropNum;
            }

            public void setCropNum(int cropNum) {
                this.cropNum = cropNum;
            }

            public int getHarvestWhere() {
                return harvestWhere;
            }

            public void setHarvestWhere(int harvestWhere) {
                this.harvestWhere = harvestWhere;
            }

            public int getIsIrration() {
                return isIrration;
            }

            public void setIsIrration(int isIrration) {
                this.isIrration = isIrration;
            }

            public Object getIrrationReason() {
                return irrationReason;
            }

            public void setIrrationReason(Object irrationReason) {
                this.irrationReason = irrationReason;
            }

            public long getTaskStarTime() {
                return taskStarTime;
            }

            public void setTaskStarTime(long taskStarTime) {
                this.taskStarTime = taskStarTime;
            }

            public long getOperationTime() {
                return operationTime;
            }

            public void setOperationTime(long operationTime) {
                this.operationTime = operationTime;
            }

            public int getOperationState() {
                return operationState;
            }

            public void setOperationState(int operationState) {
                this.operationState = operationState;
            }
        }
    }
}
