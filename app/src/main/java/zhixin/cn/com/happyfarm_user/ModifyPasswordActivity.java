package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import org.apaches.commons.codec.binary.Base64;

import java.security.PublicKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.PublicKeyList;
import zhixin.cn.com.happyfarm_user.other.AlertUtilOneButton;
import zhixin.cn.com.happyfarm_user.other.RSAUtil;
import zhixin.cn.com.happyfarm_user.other.TextChangedListener;
import zhixin.cn.com.happyfarm_user.utils.Config;

/**
 * Created by DELL on 2018/3/5.
 */

public class ModifyPasswordActivity extends ABaseActivity {

    private String psw1;
    private String psw2;
    String psw_encryption;
    private byte[] pwd;
    private String publicKey1;
    private Dialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_password_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("修改密码");
        EditText editTextPwd = findViewById(R.id.modify_password1);
        EditText editTextPwd2 = findViewById(R.id.modify_password2);
        findViewById(R.id.modify_password_determine).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                psw1 = editTextPwd.getText().toString();
                psw2 = editTextPwd2.getText().toString();
                pwd = psw2.getBytes();
                Intent intent = getIntent();
                String tel = intent.getStringExtra("tel");
//                Log.e("tel", tel);
                if ("".equals(psw1)) {
                    Toast.makeText(ModifyPasswordActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
                } else {
                    if (psw2.equals(psw1)) {
                        Call<PublicKeyList> publicKey = HttpHelper.initHttpHelper().publicKey(tel);
                        publicKey.enqueue(new Callback<PublicKeyList>() {
                            @Override
                            public void onResponse(Call<PublicKeyList> call, Response<PublicKeyList> response) {
                                publicKey1 = response.body().getResult();
                                try {
                                    PublicKey publickey = RSAUtil.getPublicKey(publicKey1);
                                    pwd = RSAUtil.encrypt(pwd, publickey);
                                    psw_encryption = Base64.encodeBase64String(pwd);
                                    mDialog = LoadingDialog.createLoadingDialog(ModifyPasswordActivity.this, "");
                                    Call<CheckSMS> resetPassword = HttpHelper.initHttpHelper().resetPassword(tel, psw_encryption);
                                    resetPassword.enqueue(new Callback<CheckSMS>() {
                                        @Override
                                        public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                            Log.e("state", JSON.toJSONString(response.body()));
                                            if ("success".equals(response.body().getFlag())) {
                                                LoadingDialog.closeDialog(mDialog);
                                                Toast.makeText(ModifyPasswordActivity.this, "修改成功", Toast.LENGTH_SHORT).show();
                                                finish();
                                            } else {
                                                LoadingDialog.closeDialog(mDialog);
                                                Toast.makeText(ModifyPasswordActivity.this, response.body().getResult(), Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CheckSMS> call, Throwable t) {
                                            LoadingDialog.closeDialog(mDialog);
                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<PublicKeyList> call, Throwable t) {
                                LoadingDialog.closeDialog(mDialog);
                            }
                        });
                    } else {
                        Toast.makeText(ModifyPasswordActivity.this, "请确认您输入的密码是否一致", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        //点击空白处退出键盘
        findViewById(R.id.modifyPwdBackView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        //限制密码长度
        TextChangedListener textChangedListener = new TextChangedListener(16, editTextPwd, ModifyPasswordActivity.this, "密码最大长度为16位");
        editTextPwd.addTextChangedListener(textChangedListener.getmTextWatcher());
        //限制密码长度
        TextChangedListener textChangedListener2 = new TextChangedListener(16, editTextPwd2, ModifyPasswordActivity.this, "密码最大长度为16位");
        editTextPwd2.addTextChangedListener(textChangedListener2.getmTextWatcher());
    }

    public void back(View view) {
        hideKeyboard();
        finish();
    }

    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}
