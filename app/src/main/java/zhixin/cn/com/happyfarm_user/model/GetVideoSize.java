package zhixin.cn.com.happyfarm_user.model;


public class GetVideoSize {

    /**
     * result : {"queueNum":0,"isWatch":1}
     * flag : success
     */
    private String flag;
    private ResultBean result;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * queueNum : 0
         * isWatch : 1
         */

        private int queueNum;
        private int isWatch;

        public int getQueueNum() {
            return queueNum;
        }

        public void setQueueNum(int queueNum) {
            this.queueNum = queueNum;
        }

        public int getIsWatch() {
            return isWatch;
        }

        public void setIsWatch(int isWatch) {
            this.isWatch = isWatch;
        }
    }
}
