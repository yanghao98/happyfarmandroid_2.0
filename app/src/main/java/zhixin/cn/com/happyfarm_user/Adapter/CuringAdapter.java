package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import zhixin.cn.com.happyfarm_user.MyImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Curing;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by DELL on 2018/3/9.
 */

//RecycleView的适配器，要注意指定的泛型，一般我们就是类名的ViewHolder继承ViewHolder（内部已经实现了复用优化机制）
public class CuringAdapter extends RecyclerView.Adapter<CuringAdapter.StaggerViewHolder>{
    private MyItemClickListener mItemClickListener;
    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<Curing> mList;
    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public CuringAdapter(Context context, List<Curing> list) {
        mContext = context;
        mList = list;
    }

    @Override
    //创建staggerViewHolder，并把ViewHolder返回出去
    public CuringAdapter.StaggerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.curing_item, null);
        //创建一个staggerViewHolder对象
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(CuringAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        Curing dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        holder.setData(dataBean);

        if (mItemClickListener != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = holder.getLayoutPosition();
                    mItemClickListener.onItemClick(view,pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if(mList!=null&&mList.size()>0){
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final MyImageView mIcon;
        private final TextView mTextView;
//        private final TextView mDiamond;
//        private final TextView mIntegral;
        public StaggerViewHolder(View itemView) {
            super(itemView);
            mIcon = itemView.findViewById(R.id.curing_image);
            mTextView = itemView.findViewById(R.id.curing_name);
//            mDiamond = itemView.findViewById(R.id.curing_diamond);
//            mIntegral = itemView.findViewById(R.id.curing_integral);
        }

        public void setData(Curing data) {
            //给ImageView设置图片数据
            if (NumUtil.checkNull(data.imageId)){
                mIcon.setImageResource(R.drawable.maintain);
            }else {
//                mIcon.setImageURL(data.imageId);


                //Picasso使用了流式接口的调用方式
                //Picasso类是核心实现类。
                //实现图片加载功能至少需要三个参数：
//                Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
//                        .load(data.imageId)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
//                        .error(R.drawable.maintain)//如果请求图像无法加载，则可以使用一个错误绘制。
//                        .resize(80,60)
//                        .centerInside()
//                        .into(mIcon);//into(ImageView targetImageView)：图片最终要展示的地方。

                Glide.with(mContext)
                        .load(data.imageId)
                        .into(mIcon);
            }
            //给养护名称数据
            mTextView.setText(data.name);
//            //给钻石数据
//            mDiamond.setText(data.diamond);
//            //给积分数据
//            mIntegral.setText(data.integral);
        }
    }

    /**
     * 创建一个回调接口
     */
    public interface MyItemClickListener {
        void onItemClick(View view, int position);
    }

    /**
     * 在activity里面adapter就是调用的这个方法,将点击事件监听传递过来,并赋值给全局的监听
     *
     * @param myItemClickListener
     */
    public void setItemClickListener(MyItemClickListener myItemClickListener) {
        this.mItemClickListener = myItemClickListener;
    }

}