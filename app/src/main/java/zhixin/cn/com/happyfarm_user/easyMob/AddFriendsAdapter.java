package zhixin.cn.com.happyfarm_user.easyMob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by Administrator on 2018/6/4.
 */

public class AddFriendsAdapter extends RecyclerView.Adapter<AddFriendsAdapter.StaggerViewHolder>{
    private AddFriendsAdapter.ButtonInterface buttonInterface;
    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<AddFriends> mList;
    private AdapterView.OnItemClickListener itemClickListener;
    private int position;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public AddFriendsAdapter(Context context, List<AddFriends> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public AddFriendsAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.add_friends_item, null);
        //创建一个staggerViewHolder对象
        AddFriendsAdapter.StaggerViewHolder staggerViewHolder = new AddFriendsAdapter.StaggerViewHolder(view);
        //立即下架点击事件
        staggerViewHolder.AddFriendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = staggerViewHolder.getAdapterPosition();
                AddFriends addFriends = mList.get(position);
                int addFriendsId = Integer.parseInt(addFriends.AddFriendsId);
//                //参数为要添加的好友的username和添加理由
//                try {
//                    EMClient.getInstance().contactManager().addContact(addFriends.AddFriendsId, "");
//                    Toast.makeText(mContext, "申请已发送", Toast.LENGTH_SHORT).show();
//                } catch (HyphenateException e) {
//                    e.printStackTrace();
//                    Toast.makeText(mContext, "申请发送失败", Toast.LENGTH_SHORT).show();
//                }
                if(buttonInterface!=null) {
//                  接口实例化后的而对象，调用重写后的方法
                    buttonInterface.onclick(v,position,addFriendsId);
                }
            }
        });
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(AddFriendsAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        AddFriends dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        setData(holder,dataBean);
    }

    public void setData(AddFriendsAdapter.StaggerViewHolder holder,AddFriends data) {
        holder.AddFriendsNickname.setText(data.AddFriendsNickname);
        if (NumUtil.checkNull(data.AddFriendsImage)){
            holder.AddFriendsImage.setImageResource(R.drawable.icon_user_img);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
//            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
//                    .load(data.AddFriendsImage)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
//                    .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
//                    .into(holder.AddFriendsImage);//into(ImageView targetImageView)：图片最终要展示的地方。
            Glide.with(mContext)
                    .load(data.AddFriendsImage)
                    .into(holder.AddFriendsImage);
        }
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {

        private final Button AddFriendBtn;
        private final TextView AddFriendsNickname;
        private final ImageView AddFriendsImage;


        public StaggerViewHolder(View itemView) {
            super(itemView);
            AddFriendsImage = itemView.findViewById(R.id.add_friends_image);
            AddFriendsNickname = itemView.findViewById(R.id.add_friends_nickname);
            AddFriendBtn = itemView.findViewById(R.id.add_friends_button);
        }

    }
    /**
     *按钮点击事件需要的方法
     */
    public void buttonSetOnclick(AddFriendsAdapter.ButtonInterface buttonInterface){
        this.buttonInterface=buttonInterface;
    }

    /**
     * 按钮点击事件对应的接口
     */
    public interface ButtonInterface{
        public void onclick( View view,int position,int addFriendsId);
    }
}
