package zhixin.cn.com.happyfarm_user.easyMob;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMContact;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.domain.EaseUser;
import com.hyphenate.easeui.ui.EaseContactListFragment;
import com.hyphenate.easeui.widget.EaseContactList;
import com.hyphenate.easeui.widget.EaseTitleBar;
import com.hyphenate.exceptions.HyphenateException;
import com.qiniu.android.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.SeachFriend;


public class ContactFragment extends EaseContactListFragment {

    private List<String> allFriendId = new ArrayList<>();
    private List<String> allFriendHead = new ArrayList<>();
    private List<String> allFriendName = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  super.onCreateView(inflater, container, savedInstanceState);
//        LoginActivity.loginEM();
        AllFriend();
        setView(view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setView(View view){
//        setContactListFragmentData(this);
        EaseTitleBar easeTitleBar = view.findViewById(R.id.title_bar);
        easeTitleBar.setBackgroundColor(0xff1DAC6D);
        StatusBarCompat.setStatusBarColor(getActivity(), getResources().getColor(R.color.theme_color));
        setContactListItemClickListener(new EaseContactListFragment.EaseContactListItemClickListener() {
            @Override
            public void onListItemClicked(EaseUser user) {
                System.out.println(user);
                ArrayList<String> friendArr = new ArrayList<>();
                friendArr.add(user.getUsername());
                friendArr.add("测试");
                friendArr.add("http://img0.imgtn.bdimg.com/it/u=1781615267,834481015&fm=27&gp=0.jpg");
                String friendStr = "";
                String s = friendArr.toString();
                String imageLink = s.substring(1,s.length()-1);
                for (String str:friendArr){
                    friendStr += str+",";
                }
//                friendStr = StringUtils.join((String[]) friendArr.toArray(), ",");
                startActivity(new Intent(getActivity(), ChatActivity.class).putExtra(EaseConstant.EXTRA_USER_ID, user.getUsername()));
                Toast.makeText(getActivity(), "进入聊天", Toast.LENGTH_SHORT).show();
            }
            //TODO 通讯录长按删除点击事件
            @Override
            public void onListItemLongClicked(View view, EaseUser user) {
                PopupMenu menu = new PopupMenu(getActivity(), view);
                menu.getMenuInflater().inflate(R.menu.contact_long_click_menu, menu.getMenu());
                menu.setOnMenuItemClickListener(item -> {
                    if (item.getItemId() == R.id.delete) {
                        try {
                            EMClient.getInstance().contactManager().deleteContact(user.getUsername());
                            setContactListFragmentData(ContactFragment.this);
                        } catch (HyphenateException e) {
                            e.printStackTrace();
                        }
                    }
                    return true;
                });
                menu.show();
            }
        });

        @SuppressLint("InflateParams")
        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.em_contacts_header, null);
        headerView.findViewById(R.id.application_item).setOnClickListener(v -> {
            //TODO 新的朋友点击
            startActivity(new Intent(getActivity(), NewFriendsMsgActivity.class));
        });
        //TODO　群聊点击
        headerView.findViewById(R.id.group_item).setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), GroupsActivity.class));
        });
        //TODO 跳转会话页
        view.findViewById(R.id.btn_conver).setOnClickListener(v ->{
            startActivity(new Intent(getActivity(),ConverActivity.class));
        });
        EaseContactList contactListLayout = view.findViewById(com.hyphenate.easeui.R.id.contact_list);
        contactListLayout.getListView().addHeaderView(headerView);
    }
    //TODO 查询全部好友接口
    public void AllFriend(){
        HttpHelper.initHttpHelper().seachFriend(0).enqueue(new Callback<SeachFriend>() {
            @Override
            public void onResponse(Call<SeachFriend> call, Response<SeachFriend> response) {
                Log.e("好友：", JSON.toJSONString(response.body()));
                if (response.body().getFlag().equals("success")) {
                    if (String.valueOf(response.body().getResult().getList()).equals("[]")) {

                    } else {
                        for (int i = 0; i <= response.body().getResult().getList().size() - 1; i++) {
                            String FriendHeadImg = response.body().getResult().getList().get(i).getFriendHeadImg();
                            String FriendId = String.valueOf(response.body().getResult().getList().get(i).getToId());
                            String FriendNickname = response.body().getResult().getList().get(i).getFriendNickname();
                            allFriendId.add(FriendId);
                            allFriendHead.add(FriendHeadImg);
                            allFriendName.add(FriendNickname);
                        }
                        setContactListFragmentData(ContactFragment.this);
                    }
                }
            }

            @Override
            public void onFailure(Call<SeachFriend> call, Throwable t) {

            }
        });
    }

    public void setContactListFragmentData(EaseContactListFragment contactListFragment) {
        HashMap<String, EaseUser> contactList = new HashMap<>();
        AsyncTask.execute(() -> {
            try {
                List<String> allFriend = EMClient.getInstance().contactManager().getAllContactsFromServer();
                Log.i("info", "setContactListFragmentData: "+allFriend);
                for (String userName : allFriendId) {
                    contactList.put(userName, new EaseUser(userName));
                }
//                for (int i = 0; i<=allFriendId.size(); i++){
//                    EaseUser user = new EaseUser(allFriendId.get(i));
//                    user.setNickname(allFriendName.get(i));
//                    user.setAvatar(allFriendHead.get(i));
//                    contactList.put(allFriendId.get(i), user);
//                }
                getActivity().runOnUiThread(() -> {
                    contactListFragment.setContactsMap(contactList);
                    contactListFragment.refresh();
                });
            } catch (HyphenateException e) {
                e.printStackTrace();
            }
        });
    }
}
