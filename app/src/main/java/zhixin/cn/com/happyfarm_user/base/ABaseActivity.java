package zhixin.cn.com.happyfarm_user.base;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.hyphenate.chat.EMClient;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.MainActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.other.OSUtils;
import zhixin.cn.com.happyfarm_user.receiver.NotificationClickReceiver;

/**
 * Created by Administrator on 2019/1/4.
 * 当Activity跳转偶遇单身多年的老汉
 * by : https://mp.weixin.qq.com/s/WfkfoKULWsL7xWC9rMyaqA
 */

public abstract class ABaseActivity extends AppCompatActivity {

    private static Toast mToast;

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(setLayout());
//        initView();
//        initData();
//    }
//
//    /**
//     * 绑定布局
//     * @return
//     */
//    protected abstract int setLayout();
//
//    /**
//     * 初始化组件
//     */
//    protected abstract void initView();
//
//    /**
//     * 设置数据等逻辑代码
//     */
//    protected abstract void initData();
//
//    /**
//     * 简化findViewById()
//     * @param resId
//     * @param <T>
//     * @return
//     */
//    protected <T extends View> T fvbi(int resId){
//        return (T) findViewById(resId);
//    }
//
//    /**
//     * Intent跳转
//     * @param context
//     * @param clazz
//     */
//    protected void toClass(Context context,Class<? extends ABaseActivity> clazz){
//        toClass(context,clazz,null);
//    }
//
//    /**
//     * Intent带值跳转
//     * @param context
//     * @param clazz
//     * @param bundle
//     */
//    protected void toClass(Context context, Class<? extends ABaseActivity> clazz, Bundle bundle){
//        Intent intent = new Intent(context,clazz);
//        intent.putExtras(bundle);
//        startActivity(intent);
//    }
//
//    /**
//     * 带返回值的跳转
//     * @param context
//     * @param clazz
//     * @param bundle
//     * @param reuqestCode
//     */
//    protected void toClass(Context context,Class<? extends ABaseActivity> clazz,Bundle bundle,int reuqestCode){
//        Intent intent = new Intent(context,clazz);
//        intent.putExtras(bundle);
//        startActivityForResult(intent,reuqestCode);
//    }

    @SuppressLint("RestrictedApi")
    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        if (startActivitySelfCheck(intent)) {
            // 查看源码得知 startActivity 最终也会调用 startActivityForResult
            super.startActivityForResult(intent, requestCode, options);
        }
    }

    private String mActivityJumpTag;
    private long mActivityJumpTime;

    /**
     * 检查当前 Activity 是否重复跳转了，不需要检查则重写此方法并返回 true 即可
     *
     * @param intent          用于跳转的 Intent 对象
     * @return                检查通过返回true, 检查不通过返回false
     */
    protected boolean startActivitySelfCheck(Intent intent) {
        // 默认检查通过
        boolean result = true;
        // 标记对象
        String tag;
        if (intent.getComponent() != null) { // 显式跳转
            tag = intent.getComponent().getClassName();
        }else if (intent.getAction() != null) { // 隐式跳转
            tag = intent.getAction();
        }else {
            return result;
        }
        if (tag.equals(mActivityJumpTag) && mActivityJumpTime >= SystemClock.uptimeMillis() - 500) {
            // 检查不通过
            result = false;
        }

        // 记录启动标记和时间
        mActivityJumpTag = tag;
        mActivityJumpTime = SystemClock.uptimeMillis();
        return result;
    }

    // This method callback must be completed in 10 seconds. Otherwise, you need to start a new Job for callback processing.


//    public abstract void onMessageReceived(RemoteMessage message);

    public abstract void back(View view);

    protected void showToastLong(Context mContext, String message) {
        if (null != mToast)
            mToast.cancel();
        mToast = Toast.makeText(mContext, message, Toast.LENGTH_LONG);
        mToast.show();
    }

    protected void showToastShort(Context mContext, String message) {
        if (null != mToast)
            mToast.cancel();
        mToast = Toast.makeText(mContext, message, Toast.LENGTH_SHORT);
        mToast.show();
    }

    protected void showToastTopShort(Context mContext, String message) {
        if (null != mToast)
            mToast.cancel();
        mToast = Toast.makeText(mContext, message, Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.TOP, 0, 0);
        mToast.show();
    }

    protected void showToastTopLong(Context mContext, String message) {
        if (null != mToast)
            mToast.cancel();

        mToast = Toast.makeText(mContext, message, Toast.LENGTH_LONG);
        mToast.setGravity(Gravity.TOP, 0, 0);
        mToast.show();
    }

    /**
     * 设置透明状态栏
     */
    protected void setStatusBarTransparent(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    /**
     * 获取状态栏高度
     * @param context 上下文
     * @return int 状态栏高度值
     */
    protected static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    /**
     * 状态栏显示或隐藏
     * https://www.cnblogs.com/muhuacat/p/7447484.html
     * @param show true 显示
     */
    protected void setStatusBarVisible(boolean show) {
        if (show) {
            int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            uiFlags |= 0x00001000;
            getWindow().getDecorView().setSystemUiVisibility(uiFlags);
        } else {
            int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            uiFlags |= 0x00001000;
            getWindow().getDecorView().setSystemUiVisibility(uiFlags);
        }
    }

    /**
     * 导航栏和状态栏
     * @param show true 显示
     */
    protected void setSystemUIVisible(boolean show) {
        if (show) {
            int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            uiFlags |= 0x00001000;
            getWindow().getDecorView().setSystemUiVisibility(uiFlags);
            setStatusBarTransparent();
        } else {
            int uiFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
            uiFlags |= 0x00001000;
            getWindow().getDecorView().setSystemUiVisibility(uiFlags);
        }
    }

    private NotificationManager mNotificationManager;
    final String CHANNEL_ID = "channel_id_1";
    final String CHANNEL_NAME = "channel_name_1";
    //用来控制应用前后台切换的逻辑
    public boolean isCurrentRunningForeground = true;
    private static int isCurren;//判断前后台赋值

    public void sendNotification(String fromId) {
        mNotificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            //只在Android O之上需要渠道
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            //如果这里用IMPORTANCE_NOENE就需要在系统的设置里面开启渠道，
            //通知才能正常弹出
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        //定义点击通知栏要进入的activity，若想不启动新的activity，
        //将activity设置为 android:launchMode="singleTask"
        Intent intent = new Intent(this, NotificationClickReceiver.class);//自定义广播接受者，静态注册
        @SuppressLint("WrongConstant")
        PendingIntent contentIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0x102, new Intent(this, MainActivity.class), Notification.FLAG_ONGOING_EVENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.mipmap.notice_logo)
                .setContentTitle("植信")
                .setContentText("您有新的消息提醒")
                .setAutoCancel(true)
                .setContentIntent(contentIntent);//设置点击通知后将要启动的程序组件对应的PendingIntent;
        mNotificationManager.notify(1, builder.build());
    }

    public int getIsCurren() {
        return isCurren;
    }

    /**
     * 华为设置角标
     */
    public void SetBadge(int num) {
        try {
            String pName = getPackageName();
            Bundle bunlde = new Bundle();
            bunlde.putString("package", pName); // com.test.badge is your package name
            bunlde.putString("class", "zhixin.cn.com.happyfarm_user.Guide"); // com.test. badge.MainActivity is your apk main activity
            bunlde.putInt("badgenumber", num);
            this.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", null, bunlde);
        } catch (Exception e) {
            Log.e("HUAWEI", "HUAWEI：" + e);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //TODO 前后台
    public boolean isRunningForeground() {
        ActivityManager activityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcessInfos = activityManager.getRunningAppProcesses();
        if (null != appProcessInfos) {  //appProcessInfos是空的则会引发空指针
            // 枚举进程
            for (ActivityManager.RunningAppProcessInfo appProcessInfo : appProcessInfos) {
                if (appProcessInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    if (appProcessInfo.processName.equals(this.getApplicationInfo().processName)) {
                        Log.d("EntryActivity", "EntryActivity isRunningForeGround");
                        return true;
                    }
                }
            }
        }
        Log.d("EntryActivity", "EntryActivity isRunningBackGround");
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!isCurrentRunningForeground) {
            Log.d("切到前台", ">>>>>>>>>>>>>>>>>>>切到前台 activity process");
            isCurren = 1;
        }
        try {
            mNotificationManager.cancel(1);//此id与下方通知id对应
        } catch (NullPointerException e) {
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isCurrentRunningForeground = isRunningForeground();
        int total = EMClient.getInstance().chatManager().getUnreadMessageCount();
        if (!isCurrentRunningForeground) {
            Log.d("切到后台", ">>>>>>>>>>>>>>>>>>>切到后台 activity process");
            isCurren = 2;
            if ("HUAWEI".equals(OSUtils.getDeviceBrand()))
                SetBadge(total);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("切到其他页面", ">>>>>>>>>>>>>>>>>>>切到后台 activity process");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("切到main页面", ">>>>>>>>>>>>>>>>>>>切到前台 activity process");
    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    //设置字体为默认大小，不随系统字体大小改而改变
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1)//非默认值
            getResources();
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        if (res.getConfiguration().fontScale != 1) {//非默认值
            Configuration newConfig = new Configuration();
            newConfig.setToDefaults();//设置默认
            res.updateConfiguration(newConfig, res.getDisplayMetrics());
        }
        return res;
    }

    /**
     * 点击空白区域隐藏键盘.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        if (me.getAction() == MotionEvent.ACTION_DOWN) {  //把操作放在用户点击的时候
            View v = getCurrentFocus();      //得到当前页面的焦点,ps:有输入框的页面焦点一般会被输入框占据
            if (isShouldHideKeyboard(v, me)) { //判断用户点击的是否是输入框以外的区域
                hideKeyboard(v.getWindowToken());   //收起键盘
            }
        }
        return super.dispatchTouchEvent(me);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {  //判断得到的焦点控件是否包含EditText
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],    //得到输入框在屏幕中上下左右的位置
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击位置如果是EditText的区域，忽略它，不收起键盘。
                return false;
            } else {
                return true;
            }
        }
        // 如果焦点不是EditText则忽略
        return false;
    }
    /**
     * 获取InputMethodManager，隐藏软键盘
     * @param token
     */
    private void hideKeyboard(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
