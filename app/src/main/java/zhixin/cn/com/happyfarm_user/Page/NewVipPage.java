package zhixin.cn.com.happyfarm_user.Page;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.hyphenate.chat.EMClient;
import com.lansosdk.videoeditor.LanSoEditor;
import com.lansosdk.videoeditor.LanSongFileUtil;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.EventDetailsActivity;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.LuckyDrawActivity;
import zhixin.cn.com.happyfarm_user.MyAlbumActivity;
import zhixin.cn.com.happyfarm_user.PersonalMessageActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.ShoppingCartActivity;
import zhixin.cn.com.happyfarm_user.SignInActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AdSeat;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.ShoppingCartNum;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.GlideImageLoader;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

import static android.content.Context.MODE_PRIVATE;

public class NewVipPage extends Fragment {

    private Activity activity;
    private ImageView shoppingCart;
    private Badge messageBadge;
    private Badge messageBadgeShoppingCart;
    private View myView;
    private RelativeLayout titleBarLayout;
    private String TAG = "NewVipPage";
    private String tokenStr;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_vip_page_layout, null, false);
        myView = view;
        searchUserInfo();
        initPage(view);
        setStatusBarHeight();
        searchAdSeat();
        return view;
    }

    private void initPage(View view) {

        TextView titleName = view.findViewById(R.id.main_page_title_name);
        titleName.setText("专享");
        titleBarLayout = view.findViewById(R.id.title_bar_layout);
        shoppingCart = view.findViewById(R.id.shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = activity.getSharedPreferences("User_data",MODE_PRIVATE);
                tokenStr = pref.getString("token","");
                if ("".equals(tokenStr)) {
                    noLoginDialog();
                } else {
                    startActivity(new Intent(getContext(), ShoppingCartActivity.class));
                }
            }
        });
        ImageView imageView = view.findViewById(R.id.main_page_message_icon);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    Intent intent = new Intent(getActivity(), PersonalMessageActivity.class);
                    startActivity(intent);
                }
            }
        });
        ImageView goSignIn = view.findViewById(R.id.sign_in_page);
        goSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    startActivity(new Intent(getContext(), SignInActivity.class));
                }
            }
        });
        ImageView goEventDetails = view.findViewById(R.id.event_details);
        goEventDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    startActivity(new Intent(getContext(), EventDetailsActivity.class));
                }
            }
        });
        ImageView goLuckyDraw = view.findViewById(R.id.go_lucky_draw);
        goLuckyDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    startActivity(new Intent(getContext(), LuckyDrawActivity.class));
                }
            }
        });

        ImageView editVideo = view.findViewById(R.id.edit_video);
        editVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(getContext())) {
                        Toast.makeText(getContext(), "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(getContext(), MyAlbumActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });

        messageBadge = new QBadgeView(getContext())
                .bindTarget(view.findViewById(R.id.main_page_icon_view_total))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
        setMsgNum(EMClient.getInstance().chatManager().getUnreadMessageCount());

        messageBadgeShoppingCart = new QBadgeView(getContext())
                .bindTarget(view.findViewById(R.id.shopping_cart_icon))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
    }
    public void getEMNumber(int number){
        setMsgNum(number);
    }

    /**
     * 获取广告栏图片
     */
    private void searchAdSeat() {
        HttpHelper.initHttpHelper().searchAdSeat(2).enqueue(new Callback<AdSeat>() {
            @Override
            public void onResponse(Call<AdSeat> call, Response<AdSeat> response) {
                try {
                    if ("success".equals(response.body().getFlag())) {
                        List images = new ArrayList();
                        List<AdSeat.ResultBean.ListBean> obj = response.body().getResult().getList();
//                    images.add("http://image14.m1905.cn/uploadfile/2018/0907/thumb_1_1380_460_20180907013518839623.jpg");
//                    images.add("http://image14.m1905.cn/uploadfile/2018/0906/thumb_1_1380_460_20180906040153529630.jpg");
//                    images.add("http://image13.m1905.cn/uploadfile/2018/0907/thumb_1_1380_460_20180907114844929630.jpg");
                        for (int i = 0;i < obj.size();i++) {
                            images.add(obj.get(i).getSeatImg());
                        }
                        Banner banner = (Banner) getActivity().findViewById(R.id.vip_banner);
                        //设置图片加载器
                        banner.setImageLoader(new GlideImageLoader());
                        //设置图片集合
                        banner.setImages(images);
                        //banner设置方法全部调用完毕时最后调用
                        banner.start();
                        //增加点击事件
                        banner.setOnBannerListener(new OnBannerListener() {
                            @Override
                            public void OnBannerClick(int position) {
//                            Toast.makeText(getContext(), "position"+position, Toast.LENGTH_SHORT).show();
                                if (0 == position) {
                                    startActivity(new Intent(getContext(), LuckyDrawActivity.class));
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    Log.e(TAG, "菜场banner没来的急加载 ", e);
                }

            }

            @Override
            public void onFailure(Call<AdSeat> call, Throwable t) {

            }
        });
    }
    /**
     * 设置消息显示数量
     * @param num 消息数量
     * position 设置显示tablayout的item 暂时未时候，考虑后期可能所有的item都需要
     */
    private void setMsgNum( int num){
        if (messageBadge != null){
//            Log.i(TAG, "消息设置成功setMsgNum: "+num);
            messageBadge.setBadgeNumber(num);
        }else {
//            Log.i(TAG, "消息设置失败setMsgNum: "+num);
        }
    }

    /**
     * 获取购物车数量
     */
    public void searchShoppingCartNum() {
        HttpHelper.initHttpHelper().searchShoppingCartNum().enqueue(new Callback<ShoppingCartNum>() {
            @Override
            public void onResponse(Call<ShoppingCartNum> call, Response<ShoppingCartNum> response) {
                if ("success".equals(response.body().getFlag())) {
                    messageBadgeShoppingCart.setBadgeNumber(response.body().getResult());
                }
            }
            @Override
            public void onFailure(Call<ShoppingCartNum> call, Throwable t) {

            }
        });
    }

    /**
     * 验证token是否有效
     */
    public  void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("failed").equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(getContext(), "User_data", strArr);
                    if ("用户信息失效".equals(response.body().getResult())){
                    }
                    myView.findViewById(R.id.main_page_message_icon).setVisibility(View.GONE);
                } else if (("success").equals(response.body().getFlag())){
                    myView.findViewById(R.id.main_page_message_icon).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 查询邀请码
     */
    private void searchUserInfo() {
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if ("success".equals(response.body().getFlag())) {
                    Config.invitationCode = response.body().getResult().getInvitationCode();
                }
            }
            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }

    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }

                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(getActivity());
        //相对布局使用方法
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        titleBarLayout.setLayoutParams(params);
    }
    /**
     * 获取状态栏高度
     * @param context 上下文
     * @return int 状态栏高度值
     */
    protected static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        searchShoppingCartNum();
        userInfoTokenTest();
    }
}
