package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import zhixin.cn.com.happyfarm_user.ModifyAddressActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.ManageAddress;
import zhixin.cn.com.happyfarm_user.model.OrderBuy;
import zhixin.cn.com.happyfarm_user.other.AlertUtilOneButton;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by Administrator on 2018/5/17.
 */

public class OrderBuyPageAdapter extends RecyclerView.Adapter<OrderBuyPageAdapter.StaggerViewHolder> {
    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<OrderBuy> mList;
    private int position;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public OrderBuyPageAdapter(Context context, List<OrderBuy> list) {
        mContext = context;
        mList = list;
    }

    @Override
    //创建staggerViewHolder，并把ViewHolder返回出去
    public OrderBuyPageAdapter.StaggerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.order_buy_items, null);
        //创建一个staggerViewHolder对象
        OrderBuyPageAdapter.StaggerViewHolder staggerViewHolder = new OrderBuyPageAdapter.StaggerViewHolder(view);
//        staggerViewHolder.buyViewDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                position = staggerViewHolder.getAdapterPosition();
//                OrderBuy OrderBuy = mList.get(position);
//                if ("正在配送".equals(OrderBuy.buyViewDetail)){
//                    Toast.makeText(mContext, "正在配送中...", Toast.LENGTH_SHORT).show();
//                }else if ("待发货".equals(OrderBuy.buyViewDetail)){
//                    Toast.makeText(mContext, "正在等待发货...", Toast.LENGTH_SHORT).show();
//                } else{
//                    NormalDialogOneBtn("9527835");
//                }
//            }
//        });
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(OrderBuyPageAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        OrderBuy dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        setData(holder, dataBean);
    }

    public void setData(OrderBuyPageAdapter.StaggerViewHolder holder, OrderBuy data) {
        holder.buyAllTime.setText(data.buyAllTime);//交易钻石
        holder.buyAllCropName.setText(data.buyAllCropName);//作物名字
//        holder.buyAllFertilizer.setText(data.buyAllFertilizer);//是否有机肥
        holder.buyAllBuyName.setText(data.buyAllBuyName);//买卖家姓名
        holder.buyAllNum.setText(data.buyAllNum);//份数
        Log.i("单份价格", "setData: " + data.buyAllOnePrice);
        holder.buyAllOnePrice.setText(data.buyAllOnePrice);//单份价格
        holder.buyConsuAmoutP.setText(data.buyConsuAmoutP);//合计金额价格
//        holder.buyViewDetail.setText(data.buyViewDetail);//正在配送或查看提货码
        holder.order_number.setText("订单编号：" + data.buyOrderNumber);
//        if ("正在配送".equals(data.buyViewDetail)){
//            holder.buyViewDetail.setBackground(mContext.getResources().getDrawable(R.drawable.btn_bg_gary));
//        }else {
//            holder.buyViewDetail.setBackground(mContext.getResources().getDrawable(R.drawable.btn_circular_bead_white));
//        }

        if (NumUtil.checkNull(data.buyAllImg)){
            holder.buyAllImg.setImageResource(R.drawable.maintain);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(data.buyAllImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .error(R.drawable.maintain)//如果请求图像无法加载，则可以使用一个错误绘制。
                    .into(holder.buyAllImg);//into(ImageView targetImageView)：图片最终要展示的地方。
        }//图片
    }


    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final TextView buyAllTime;//交易时间
        private final ImageView buyAllImg;//作物图片
        private final TextView buyAllCropName;//作物名字
//        private final TextView buyAllFertilizer;//是否有机肥
        private final TextView buyAllBuyName;//买卖家姓名
        private final TextView buyAllNum;//份数
        private final TextView buyAllOnePrice;//单份价格
        private final TextView buyConsuAmoutP;//合计金额价格
//        private final Button buyViewDetail;//查看提货码
        private final TextView order_number;//订单号

        public StaggerViewHolder(View itemView) {
            super(itemView);
            buyAllBuyName = itemView.findViewById(R.id.order_buy_seller_name);
//            buyAllFertilizer = itemView.findViewById(R.id.order_buy_fat);
            buyAllNum = itemView.findViewById(R.id.order_buy_good_num);
            buyAllCropName = itemView.findViewById(R.id.order_buy_crop_name);
            buyAllImg = itemView.findViewById(R.id.order_buy_img);
            buyAllTime = itemView.findViewById(R.id.order_buy_all_time);
            buyConsuAmoutP = itemView.findViewById(R.id.order_buy_total_amount);
//            buyViewDetail = itemView.findViewById(R.id.order_buy_view_details);
            buyAllOnePrice = itemView.findViewById(R.id.order_buy_diamond);
            order_number = itemView.findViewById(R.id.order_number);
        }
    }

    private void NormalDialogOneBtn(String pickupCode) {
        final AlertUtilOneButton diyDialog = new AlertUtilOneButton(mContext);
        diyDialog.setOk("我知道了")
                .setContent("取货码为："+pickupCode)
                .setDialogClickListener(new AlertUtilOneButton.DialogClickListener() {
                    @Override
                    public void ok() {
                        diyDialog.cancle();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
}
