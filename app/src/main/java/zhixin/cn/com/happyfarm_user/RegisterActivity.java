package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import org.apaches.commons.codec.binary.Base64;

import java.security.PublicKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.PublicKeyList;
import zhixin.cn.com.happyfarm_user.model.RegisterList;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.RSAUtil;
import zhixin.cn.com.happyfarm_user.other.TextChangedListener;

/**
 * Created by DELL on 2018/2/1.
 */

public class RegisterActivity extends ABaseActivity {

    private String publicKey1;
    private String nickname;
    private String password;
    private byte[] password2;
    private byte[] phones;
    private String phone_encryption;
    private String tel;
    private String registerIp;
    private String code;
    private Button register_code_bt;
    private int count = 60;
    private int COUNT_TIME = 0;
    private Dialog mDialog;
    private CheckBox checkBox;
    private Button termsBtn;
    private Button privacypolicytermsBtn;
    private EditText phoneEdit;
    private EditText codeEdit;
    private EditText pwd1;
    private EditText pwd2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("注册账号");

        initView();

        Intent intent = getIntent();
        String phone = intent.getStringExtra("tel");
        phoneEdit.setText(phone);

        register_code_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog = LoadingDialog.createLoadingDialog(RegisterActivity.this, "");
                Call<PublicKeyList> publicKey = HttpHelper.initHttpHelper().publicKey(phone);
                publicKey.enqueue(new Callback<PublicKeyList>() {
                    @Override
                    public void onResponse(Call<PublicKeyList> call, Response<PublicKeyList> response) {
                        phones = phone.getBytes();
                        publicKey1 = response.body().getResult();
                        try {
                            PublicKey publickey = RSAUtil.getPublicKey(publicKey1);
                            phones = RSAUtil.encrypt(phones, publickey);
                            phone_encryption = Base64.encodeBase64String(phones);

                            //发送验证码
//                            Log.e("tel", phone);
//                            Log.e("tel", phone_encryption);
                            Call<CheckSMS> getSMS = HttpHelper.initHttpHelper().getSMS(phone_encryption, phone);
                            getSMS.enqueue(new Callback<CheckSMS>() {
                                @Override
                                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                    Log.e("state", response.body().getFlag());
                                    if ("success".equals(response.body().getFlag())) {
                                        LoadingDialog.closeDialog(mDialog);
                                        clickButton(view);
                                        Toast.makeText(RegisterActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                    } else {
                                        LoadingDialog.closeDialog(mDialog);
                                        Toast.makeText(RegisterActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<CheckSMS> call, Throwable t) {
                                    LoadingDialog.closeDialog(mDialog);
                                    Toast.makeText(RegisterActivity.this, "网络错误", Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<PublicKeyList> call, Throwable t) {

                    }
                });
            }
        });

        findViewById(R.id.register_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(phoneEdit.getText().toString())) {
                    Toast.makeText(RegisterActivity.this, "请输入手机号", Toast.LENGTH_SHORT).show();
                } else if ("".equals(codeEdit.getText().toString())) {
                    Toast.makeText(RegisterActivity.this, "请输入验证码", Toast.LENGTH_SHORT).show();
                } else if ("".equals(pwd1.getText().toString())) {
                    Toast.makeText(RegisterActivity.this, "请输入您的登录密码", Toast.LENGTH_SHORT).show();
                } else if ("".equals(pwd2.getText().toString())) {
                    Toast.makeText(RegisterActivity.this, "请确认您的登录密码", Toast.LENGTH_SHORT).show();
                } else if (!(checkBox.isChecked())) {
                    Toast.makeText(RegisterActivity.this, "您需要同意使用服务条款", Toast.LENGTH_SHORT).show();
                } else if (pwd1.getText().toString().equals(pwd2.getText().toString())) {
                    nickname = phoneEdit.getText().toString();
                    password = pwd2.getText().toString();
                    password2 = password.getBytes();
                    tel = phoneEdit.getText().toString();
                    code = codeEdit.getText().toString();
                    registerIp = NetWorkUtils.getIPAddress(getApplicationContext());
                    if (registerIp.equals("-1")) {
                        Toast.makeText(RegisterActivity.this, registerIp, Toast.LENGTH_SHORT).show();
                    } else {
                        mDialog = LoadingDialog.createLoadingDialog(RegisterActivity.this, "");
                        Call<CheckSMS> checkSMS = HttpHelper.initHttpHelper().checkSMS(tel, code);
                        checkSMS.enqueue(new Callback<CheckSMS>() {
                            @Override
                            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                mDialog = LoadingDialog.createLoadingDialog(RegisterActivity.this,null);
                                Log.e("state", response.body().getFlag());
                                if ("success".equals(response.body().getFlag())) {
                                    LoadingDialog.closeDialog(mDialog);
                                    Call<PublicKeyList> publicKey = HttpHelper.initHttpHelper().publicKey(tel);
                                    publicKey.enqueue(new Callback<PublicKeyList>() {
                                        @Override
                                        public void onResponse(Call<PublicKeyList> call, Response<PublicKeyList> response) {
                                            Log.i("info", response.body().getResult());
                                            publicKey1 = response.body().getResult();
                                            PublicKey publicKey = null;

                                            try {
                                                publicKey = RSAUtil.getPublicKey(publicKey1);
                                                password2 = RSAUtil.encrypt(password2, publicKey);
                                                password = Base64.encodeBase64String(password2);
//                                                mDialog = LoadingDialog.createLoadingDialog(RegisterActivity.this,null);
                                                Call<RegisterList> register = HttpHelper.initHttpHelper().register(nickname, password, tel, registerIp);
                                                register.enqueue(new Callback<RegisterList>() {
                                                    @Override
                                                    public void onResponse(Call<RegisterList> call, Response<RegisterList> response) {
                                                        Log.i("infossss", JSON.toJSONString(response.body()));
                                                        if ("success".equals(response.body().getFlag())) {
                                                            LoadingDialog.closeDialog(mDialog);
                                                            Toast.makeText(RegisterActivity.this, "注册成功", Toast.LENGTH_SHORT).show();
//                                                        Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
//                                                        intent.putExtra("tel",tel);
//                                                        startActivity(intent);
                                                            finish();
                                                        } else if (response.body().getErrCode() == -103) {
                                                            LoadingDialog.closeDialog(mDialog);
                                                            Toast.makeText(RegisterActivity.this, "该用户已存在", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            LoadingDialog.closeDialog(mDialog);
                                                            Toast.makeText(RegisterActivity.this, response.body().getResult(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<RegisterList> call, Throwable t) {
//                                                        Log.e("12331312321",t.getMessage());
                                                        LoadingDialog.closeDialog(mDialog);
                                                        Toast.makeText(RegisterActivity.this, "注册失败", Toast.LENGTH_SHORT).show();
                                                    }
                                                });

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<PublicKeyList> call, Throwable t) {
                                            LoadingDialog.closeDialog(mDialog);
                                        }
                                    });
                                } else {
                                    LoadingDialog.closeDialog(mDialog);
                                    Toast.makeText(RegisterActivity.this, "验证码输入错误", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckSMS> call, Throwable t) {
                                LoadingDialog.closeDialog(mDialog);
                            }
                        });
                    }
                } else {
                    Toast.makeText(RegisterActivity.this, "请确认您输入的密码是否一致", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //点击空白处退出键盘
        findViewById(R.id.registerBackView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        //限制密码长度
        TextChangedListener textChangedListener = new TextChangedListener(16, pwd1, RegisterActivity.this, "密码最大长度为16位");
        pwd1.addTextChangedListener(textChangedListener.getmTextWatcher());
        //限制密码长度
        TextChangedListener textChangedListener2 = new TextChangedListener(16, pwd2, RegisterActivity.this, "密码最大长度为16位");
        pwd2.addTextChangedListener(textChangedListener2.getmTextWatcher());

        termsOnClick();
    }

    private void initView() {
        phoneEdit = findViewById(R.id.phone_edit);
        codeEdit = findViewById(R.id.register_code);
        pwd1 = findViewById(R.id.register_pwd1);
        pwd2 = findViewById(R.id.register_pwd2);
        checkBox = findViewById(R.id.register_check);
        termsBtn = findViewById(R.id.register_terms_btn);
        privacypolicytermsBtn = findViewById(R.id.privacypolicy_terms_btn);
        register_code_bt = findViewById(R.id.register_code_bt);
    }

    /**
     * 服务条款按钮点击事件
     */
    private void termsOnClick() {
        termsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, TermsActivity.class);
                intent.putExtra("termsName", "agreement");//用户协议
                intent.putExtra("url","");//因为帮助里面的操作说明也用到了TermsActivity页面所以穿个空url进去进行区分
                startActivity(intent);
            }
        });
        privacypolicytermsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, TermsActivity.class);
                intent.putExtra("termsName", "privacy");//用户协议
                intent.putExtra("url","");//因为帮助里面的操作说明也用到了TermsActivity页面所以穿个空url进去进行区分
                startActivity(intent);
            }
        });
    }

    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    public void back(View view) {
        finish();
        hideKeyboard();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (count == 0) {
                count = 60;
                register_code_bt.setText("重新发送");
                register_code_bt.setClickable(true);
                return;
            }
            count--;
            register_code_bt.setText(count + "s后重新发送");
            sendEmptyMessageDelayed(COUNT_TIME, 1000);
        }

        ;
    };

    public void clickButton(View view) {
        handler.sendEmptyMessage(COUNT_TIME);
        register_code_bt.setClickable(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
