package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by DELL on 2018/3/18.
 */

public class Crop {

    public int id;

    public String imageUrl;

    public String cropName;

    public Double plantingDiamond;

    public Double plantingIntegral;

    public int cycle;

    public String details;

    public long startSowing;

    public long endSeeding;

    public String nutritionalComponents;

    public int state;
}
