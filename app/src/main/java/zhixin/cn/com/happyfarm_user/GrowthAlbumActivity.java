package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.kyleduo.switchbutton.SwitchButton;
import com.lansosdk.videoeditor.LanSoEditor;
import com.lansosdk.videoeditor.LanSongFileUtil;
import com.qiniu.android.common.AutoZone;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.zhaoss.weixinrecorded.activity.BaseActivity;
import com.zhaoss.weixinrecorded.activity.EditVideoActivity;
import com.zhaoss.weixinrecorded.activity.RecordedActivity;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpApi;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.Audio;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.CheckSMSByResultIsList;
import zhixin.cn.com.happyfarm_user.model.QiNiu;
import zhixin.cn.com.happyfarm_user.model.SeachTrack;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.widget.Toast.LENGTH_SHORT;
import static zhixin.cn.com.happyfarm_user.utils.Utils.objectToMap;

/**
 * Created by HuYueling on 2018/4/16.
 */

public class GrowthAlbumActivity extends ABaseActivity {
    private String data;
    private GifImageView gifImageView;
    private static final String TAG = "GrowthAlbumActivity-->";
    private String photoName;
    private Button saveBtn;
    private SwitchButton defaultSwitch;
    private int isDefault = 0;
    private EditText growthName;
    private EditText growthAbstract;
    private Dialog dialog;
    //自动识别上传区域
    Configuration config = new Configuration.Builder().zone(AutoZone.autoZone).build();
    UploadManager uploadManager = new UploadManager(config);

    private RadioGroup radioGroup;
    private RadioButton radioButton[];

    //音频播放
    private MediaPlayer mediaPlayer;
    private int AAA = 1;
    private List audioLists;
    private String audioUrl = "";
    private List imgArr = null;

    private long mTaskId;
    public static final String INTENT_PATH = "intent_path";
    public static final int REQUEST_CODE_KEY = 100;
    private boolean isOK = false;

    private BroadcastReceiver mRefreshBroadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.growth_album_layout);
        TextView title = findViewById(R.id.title_name);
        title.setText("成长相册");
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        gifImageView = findViewById(R.id.gifImageView);
        saveBtn = findViewById(R.id.growth_save_btn);
        defaultSwitch = findViewById(R.id.growth_switch);
        growthName = findViewById(R.id.growth_name);
        growthAbstract = findViewById(R.id.growth_photoAbstract);

        data = getIntent().getStringExtra("path");
        photoName = getIntent().getStringExtra("name");
        imgArr = getIntent().getStringArrayListExtra("imgArr");
//        Log.d(TAG, "onCreate: " + getIntent().getStringArrayListExtra("imgArr"));
        if (photoName != null)
            growthName.setText(photoName);
        GifDrawable gifFromPath = null;
        try {
            gifFromPath = new GifDrawable( data );
        } catch (IOException e) {
            e.printStackTrace();
        }
        gifImageView.setImageDrawable(gifFromPath);
//        GifDrawable gifDrawable = (GifDrawable) gifImageView.getDrawable();
//        gifDrawable.setLoopCount(1);//设置动画播放次数
//        //gifDrawable.start();
//        Log.e("gif", String.valueOf(gifDrawable.getDuration()));
//        Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                //gif 执行完毕
//            }
//        }, gifDrawable.getDuration() * 1);// 延时。

        //判断有没有七牛token
//        if ("".equals(Config.qiuNiuToken)) {
//            getQiniuToken();
//        }
        saveBtnClick();
        defaultAddressSwitch();
        searchAudio();

    }

    //查询制作视频背景音乐
    public void searchAudio() {
        retrofit2.Call<CheckSMSByResultIsList> audioList = HttpHelper.initHttpHelper().searchAudio();
        audioList.enqueue(new Callback<CheckSMSByResultIsList>() {
            @Override
            public void onResponse(Call<CheckSMSByResultIsList> call, Response<CheckSMSByResultIsList> response) {
//                Log.d(TAG, "背景音乐: "+ JSON.toJSONString(response.body().getResult()));
                if ("success".equals(response.body().getFlag()) ){
                    audioLists = response.body().getResult().getList();
                }
//                Log.d(TAG, JSON.toJSONString(audioLists));
                //创建背景音乐选择列表
                radioGroup = findViewById(R.id.radioGroups);
                radioButton = new RadioButton[4];
                for (Object audio : audioLists) {
                    String text = "";
                    int radioId = 0;
                    try {
//                        Log.d("音频文件", String.valueOf(objectToMap(audio)));
                        for (String key : objectToMap(audio).keySet()) {
//                            Log.d(TAG, "" +objectToMap(audio).get("audioName"));
//                            Log.d(TAG, "onResponse: "+(int)objectToMap(audio).get("id"));
                            text = (String)objectToMap(audio).get("audioName");
                            radioId = (int)objectToMap(audio).get("id");
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    radioButton[radioId] = new RadioButton(GrowthAlbumActivity.this);
                    radioButton[radioId].setLayoutParams(new LinearLayout.LayoutParams(400,100));
                    radioButton[radioId].setText(text);
                    radioButton[radioId].setId(radioId);
                    radioGroup.addView(radioButton[radioId]);
                }

                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        if (AAA != 1) {
                            try {
                                mediaPlayer.stop();//停止播放
//                    mediaPlayer.release();//释放资源
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                            mediaPlayer=null;
                        } else {
                            AAA = 2;
                        }
                        RadioButton radioButtonOne = (RadioButton)findViewById(i);
//                        Log.d(TAG, "onCheckedChanged: 你点击了第"+ i +"各个按钮" + radioButtonOne.getText());
                        boolean createState = false;

                        if (null == mediaPlayer) {
                            mediaPlayer = createNetMp3(i,audioLists);
//                            Log.d(TAG, "onCheckedChanged: 播放了");
                            createState = true;
                        }
                        //当播放完音频资源时，会触发onCompletion时间，可以在该事件中释放音频资源
                        //以便其他程序可以使用该资源
//                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mediaPlayer) {
//                        mediaPlayer.release();//释放音频资源
//                        Log.d(TAG, "资源已经被释放了");
//                    }
//                });

                        //在播放音频资源之前必须调用prepare方法完成准备工作
                        if (null != mediaPlayer) {
                            try {
                                if (createState) mediaPlayer.prepare();
                                mediaPlayer.start();
                            } catch (IllegalStateException e){
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
            @Override
            public void onFailure(Call<CheckSMSByResultIsList> call, Throwable t) {
                Toast.makeText(GrowthAlbumActivity.this,"暂无可用背景音频", LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 创建网络mp3
     * @return
     */
    public MediaPlayer createNetMp3(int i,List audioLists){
        MediaPlayer mp=new MediaPlayer();
        String url = "";
        for (Object audio : audioLists) {
            try {
                for (String key : objectToMap(audio).keySet()) {
                    if (i == (int)objectToMap(audio).get("id")) {
                        url = (String)objectToMap(audio).get("audioUrl");
                        audioUrl = (String)objectToMap(audio).get("route");
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        try {
            if (url == "") {
                Toast.makeText(this,"此音频文件丢失！", LENGTH_SHORT).show();
                mp = null;
            } else {
                mp.setDataSource(url);
            }
        } catch (IllegalArgumentException e) {
            return null;
        } catch (IllegalStateException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
        return mp;
    }

    //    private int currentPlay = 0;
    //    private Timer timer = new Timer();
    public void defaultAddressSwitch() {
        defaultSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isDefault = 1;
                } else {
                    isDefault = 0;
                }

            }
        });
    }
    public void saveBtnClick() {
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("".equals(growthName.getText().toString())){
                    Toast.makeText(GrowthAlbumActivity.this, "请输入相册名", LENGTH_SHORT).show();
                }else {
                    try {
                        mediaPlayer.release();
                    } catch (NullPointerException e) {
                        Log.d(TAG, "" + e);
                    }
                    dialog = LoadingDialog.createLoadingDialog(GrowthAlbumActivity.this, "正在制作，请稍后");
                    addPhoto();
                }
            }
        });
    }
    /**
     * 获取七牛信息
     */
//    private void getQiniuToken() {
//        HttpHelper.initHttpHelper().getQiniuToken().enqueue(new Callback<QiNiu>() {
//            @Override
//            public void onResponse(Call<QiNiu> call, Response<QiNiu> response) {
//                Log.e("data", JSON.toJSONString(response.body()));
//                if ("success".equals(response.body().getFlag())) {
//                    Config.qiuNiuToken = response.body().getResult().getUploadToken();
//                    Config.qiuNiuDomain = response.body().getResult().getDomain();
//                    Config.qiuNiuImgUrl = response.body().getResult().getImgUrl();
//                    Log.i("qiuNiuToken", "Upload Success" + Config.qiuNiuToken);
//                } else {
//                    Log.i("qiuNiuToken", "Upload failure" + Config.qiuNiuToken);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<QiNiu> call, Throwable t) {
//                Log.e("qiNiuToken", "onFailure: " + t);
//            }
//        });
//    }
//
//    private void uploadGif(String photoName){
//        Log.i(TAG, "uploadGif: " + data);
//        uploadManager.put(data, photoName, Config.qiuNiuToken, new UpCompletionHandler() {
//            @Override
//            public void complete(String key, ResponseInfo info, org.json.JSONObject response) {
//                //res包含hash、key等信息，具体字段取决于上传策略的设置
//                if (info.isOK()) {
//                    Log.i("Imagedata", "Upload Success");
//                    Log.i("Imagedata", Config.qiuNiuImgUrl + photoName);
//
//                } else {
//                    Config.qiuNiuToken = "";
//                    Config.qiuNiuDomain = "";
//                    Config.qiuNiuDomain = "";
//                    getQiniuToken();
//                    Log.i("Imagedata", "Upload Fail");
//                    if (("file exists").equals(info.error))
//                        Toast.makeText(GrowthAlbumActivity.this, "相册名重复", LENGTH_SHORT).show();
//                    LoadingDialog.closeDialog(dialog);
//                    //如果失败，这里可以把info信息上报自己的服务器，便于后面分析上传错误原因
//                }
//                Log.i("qiniu", key + ",\r\n " + info + ",\r\n " + response);
//            }
//        }, null);
//    }

    //制作相册
    private void addPhoto(){
        if (null != audioUrl && !"".equals(audioUrl)) {

            Call<CheckSMS> addPhoto = HttpHelper.initHttpHelper().addPhoto(JSON.toJSONString(imgArr), growthName.getText().toString(),audioUrl,growthName.getText().toString(),growthAbstract.getText().toString(),String.valueOf(isDefault));
            addPhoto.enqueue(new retrofit2.Callback<CheckSMS>() {
                @Override
                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                    Log.d(TAG, "addPhoto: " + JSON.toJSONString(response));
                    Log.d(TAG, "addPhoto: " + JSON.toJSONString(response));
                    if (("success").equals(response.body().getFlag())) {

                        Config.photoAlubmURL = response.body().getResult();
                        Log.i(TAG, "要下载的视频路径: " + Config.photoAlubmURL);
                        isOK = false;
                        //开启lansoEditor SDK 插件 （一定要在进入SDK前开启）
                        LanSoEditor.initSDK(GrowthAlbumActivity.this, null);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Long time = System.currentTimeMillis();
                                Log.i(TAG, "run: " + LanSongFileUtil.DEFAULT_DIR + time);
                                //创建下载任务,downloadUrl就是下载链接
                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Config.photoAlubmURL));
//                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse("https://v-cdn.zjol.com.cn/276992.mp4"));
                                //指定下载路径和下载文件名
                                request.setDestinationInExternalPublicDir(LanSongFileUtil.DEFAULT_DIR + time, growthName.getText().toString()+ ".mp4");
                                Log.i(TAG, "用相册名做视频名: " + growthName.getText().toString()+ ".mp4");
                                //获取下载管理器
                                DownloadManager downloadManager = (DownloadManager) GrowthAlbumActivity.this.getSystemService(Context.DOWNLOAD_SERVICE);
                                //将下载任务加入下载队列，否则不会进行下载
                                //加入下载队列后会给该任务返回一个long型的id，
                                mTaskId = downloadManager.enqueue(request);
                                for (int i = 0 ; i <=1000; i++) {
                                    if (isOK) {
                                        Log.i(TAG, "run: 走完了" );
                                        LoadingDialog.closeDialog(dialog);
                                        break;
                                    } else {
                                        try {
                                            Thread.sleep(1000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        Log.i(TAG, "run: 一秒钟走一次" );
                                        checkDownloadStatus(downloadManager,LanSongFileUtil.DEFAULT_DIR + time,growthName.getText().toString()+ ".mp4");
                                    }
                                }

                            }

                        }).start();
//                        showSetDeBugDialog(getResources().getString(R.string.grow_album_success));//视频制作成功弹窗

                    }else {
                        LoadingDialog.closeDialog(dialog);
                        Toast.makeText(GrowthAlbumActivity.this, response.body().getResult().toString(), LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CheckSMS> call, Throwable t) {
                    LoadingDialog.closeDialog(dialog);
                    Toast.makeText(GrowthAlbumActivity.this, TextString.NetworkRequestFailed, LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(GrowthAlbumActivity.this,"请选择背景音乐", LENGTH_SHORT).show();
            LoadingDialog.closeDialog(dialog);
        }
    }

    //检查下载状态
    private void checkDownloadStatus(DownloadManager downloadManager,String result,String photoName) {
        Log.i(TAG, "存checkDownloadStatus: " + result);
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(mTaskId);//筛选下载任务，传入任务ID，可变参数
        Cursor c = downloadManager.query(query);
        if (c.moveToFirst()) {
            int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
            switch (status) {
                case DownloadManager.STATUS_PAUSED:
                    Log.i(TAG,">>>下载暂停");
                    break;
                case DownloadManager.STATUS_PENDING:
                    Log.i(TAG,">>>下载延迟");
                    break;
                case DownloadManager.STATUS_RUNNING:
                    Log.i(TAG,">>>正在下载");
                    break;
                case DownloadManager.STATUS_SUCCESSFUL:
                    Log.i(TAG,">>>下载完成");
                    isOK = true;
                    Intent intent = new Intent(GrowthAlbumActivity.this, EditVideoActivity.class);
                    intent.putExtra(INTENT_PATH, Environment.getExternalStorageDirectory().getPath()+result + "/" +photoName);
                    startActivityForResult(intent, REQUEST_CODE_KEY);
                    LoadingDialog.closeDialog(dialog);
                    break;
                case DownloadManager.STATUS_FAILED:
                    Log.i(TAG,">>>下载失败");
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //    private void saveBtnNet(String imgUrl) {
//        Log.d(TAG, "saveBtnNet: "+growthName.getText().toString()+"+++"+growthAbstract.getText().toString()+"++++++"+ imgUrl+"++++++"+String.valueOf(isDefault));
//        Call<CheckSMS> addPhotoAlbum = HttpHelper.initHttpHelper().addPhotoAlbum(growthName.getText().toString(),growthAbstract.getText().toString(), imgUrl,String.valueOf(isDefault));
//        addPhotoAlbum.enqueue(new retrofit2.Callback<CheckSMS>() {
//            @Override
//            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                Log.d(TAG, "saveBtnNet: " + JSON.toJSONString(response.body()));
//                if (("success").equals(response.body().getFlag())) {
//                    showSetDeBugDialog(getResources().getString(R.string.grow_album_success));
//                    LoadingDialog.closeDialog(dialog);
//                } else {
//                    LoadingDialog.closeDialog(dialog);
//                    Toast.makeText(GrowthAlbumActivity.this, response.body().getResult().toString(), LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CheckSMS> call, Throwable t) {
//                LoadingDialog.closeDialog(dialog);
//                Toast.makeText(GrowthAlbumActivity.this, TextString.NetworkRequestFailed, LENGTH_SHORT).show();
//            }
//        });
//    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void back(View view) {
        noUploadDialog();
    }
    /**
     * 弹框点击事件
     */
    private void noUploadDialog(){

        final AlertUtilBest diyDialog = new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk(getResources().getString(R.string.grow_album_ok))
                .setContent(getResources().getString(R.string.grow_album_content_text))
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        try {
                            mediaPlayer.release();
                        } catch (NullPointerException e) {
                            Log.d(TAG, "noUploadDialog" + e);
                        }
                        diyDialog.cancel();
                        finish();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    //TODO 弹窗
    private void showSetDeBugDialog(String msgContext) {
        AlertDialog.Builder setDeBugDialog = new AlertDialog.Builder(this);
        // AlertDialog setDeBugDialog = new AlertDialog.Builder(this).create();//创建对话框

        //获取界面
        View dialogView = LayoutInflater.from(this).inflate(R.layout.layout_addbuddy_dialog, null);
        TextView msgText = dialogView.findViewById(R.id.nei);
        msgText.setText(msgContext);
        //将界面填充到AlertDiaLog容器
        setDeBugDialog.setView(dialogView);

        // 取消点击外部消失弹窗
        setDeBugDialog.setCancelable(false);
        //创建AlertDiaLog
        final AlertDialog customAlert = setDeBugDialog.show();

        customAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        customAlert.getWindow().setLayout(700, 500);

        //设置自定义界面的点击事件逻辑
        dialogView.findViewById(R.id.okBut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customAlert.dismiss();
                finish();
            }
        });

    }


}
