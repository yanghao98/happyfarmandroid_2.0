package zhixin.cn.com.happyfarm_user;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.astuetz.PagerSlidingTabStrip;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.EaseUI;
import com.hyphenate.easeui.domain.EaseUser;
import com.hyphenate.exceptions.HyphenateException;
import com.mob.MobSDK;
import com.pili.pldroid.player.AVOptions;
import com.pili.pldroid.player.PLOnErrorListener;
import com.pili.pldroid.player.PLOnInfoListener;
import com.pili.pldroid.player.widget.PLVideoTextureView;
import com.pili.pldroid.player.widget.PLVideoView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.handshake.ServerHandshake;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.onekeyshare.ShareContentCustomizeCallback;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.CuringAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.LandVideoExhibitionAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.ListDialogAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.TabPagerAdapter;
import zhixin.cn.com.happyfarm_user.EventBus.VideoEvent;
import zhixin.cn.com.happyfarm_user.Page.CropPage;
import zhixin.cn.com.happyfarm_user.Page.CuringPage;
import zhixin.cn.com.happyfarm_user.Page.HarvestPage;
import zhixin.cn.com.happyfarm_user.Page.HistoryPage;
import zhixin.cn.com.happyfarm_user.Page.TutorialPage;
import zhixin.cn.com.happyfarm_user.Page.UserLogsPage;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.easyMob.ChatActivity;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.CusChat;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.GetVideoSize;
import zhixin.cn.com.happyfarm_user.model.MyAlbum;
import zhixin.cn.com.happyfarm_user.model.SeachFriend;
import zhixin.cn.com.happyfarm_user.model.SearchLandByNo;
import zhixin.cn.com.happyfarm_user.model.SearchPhotoAlbum;
import zhixin.cn.com.happyfarm_user.model.SelectLandCollection;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.model.searchFriendDetail;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.AlertUtilOneButton;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.ImageUtils;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.VideoConfig;


/**
 * Created by DELL on 2018/3/7.
 */

public class LandVideoActivity extends ABaseActivity {

    /**
     * 用户对租地的状态 0：我的租地；1：空地；2：他人租地
     */
    private String state;
    private String alias;//他人租地时用到名字
    private String landNo;
    private String landId;
    private String token = App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("token", "");
    private int isHosted;
    private Button determine;
    private Button addBut;
    private ImageView amplification;//放大按钮
    private ImageView screenshots;//截图按钮
    private ImageView collection;//收藏按钮
    private ImageButton informationBut;
    private ImageView customerService;
    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;
    private List<String> pm = new ArrayList<>();
    private PLVideoTextureView mVideoView;
    @SuppressLint("StaticFieldLeak")
    public static PLVideoView mVideoView2;
    private boolean isWifi;
    private RelativeLayout other;
    private LinearLayout myLand;
    private String username;
    private String userId;
    private String LeaseTime;
    private String ExpirationTime;
    private String UserImg;
    private String OthersTel;
    private RelativeLayout loadingView;
    private Button login;
    private TextView video_text_view;
    private Button prompt_btn;

    private LinearLayout prompt;
    private TextView number_people;
    private boolean isCollect = false;
    private boolean mIsLiveStreaming;
    private LinearLayout line_up;
    private int num1;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private WebSocketClient mSocketClient;
    private String url;
    private URI uri;
    private String TAG = "LandVideoActivity->";
    private int recLen;

    private Boolean videoFlag = true;
    private Handler handlerVideo = new Handler();
    private PagerSlidingTabStrip tabStrip;
    private int poss;
    private ArrayList<MyAlbum> mdata = new ArrayList<>();;
    private RecyclerView exhibition;
    private String videoPath;
    private AVOptions options = new AVOptions();
    private RelativeLayout videoViewLayout;
    private RelativeLayout videoBgViewLayout;
    private String isLandlord;
    private long resultLong = 0;
    private int superUser;
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private GridLayoutManager gridLayoutManager;
    private LandVideoExhibitionAdapter adapter;
    private int photoNum = 0;
    private TextView publicPhoto;
    private TextView noPhotoGif;
    private TextView landnumber;
    private TextView title;
    private TextView nickname;
    private TextView otherLeaseTime;
    private TextView otherExpirationTime;
    private ImageView otherUserImg;
    private TextView LandNo;
    private boolean isShowPrompt = true; //是否显示流量按钮 true 显示 false 不显示

    private Integer landType;//租地类型
    private ImageView audioButton;

    private boolean audioPlay = true;
    private RelativeLayout audioTipsLayout;//声音开启关闭提示布局
    private TextView audioText;//声音开启关闭提示文字
    private RelativeLayout plview;//视频布局layout
    private RelativeLayout noPlayPlview;//不开视频流时视频布局
    private RelativeLayout titleLayout;//标题栏布局layout
    private RelativeLayout landVideoBar;//标题栏内部bar
    private RelativeLayout fullScreenLayout;//全屏显示布局

    /**
     * isFullScreen设置视频页面的大小状态，横屏还是竖屏
     * true初始：竖屏状态；false：横屏状态
     */
    private boolean isFullScreen = true;
    private ImageButton seekBarAdd;//焦距加
    private ImageButton seekBarDelete;//焦距减
    private TextView resetBtn;//复位
    private TextView settingBtn;//设置预设
    private ImageButton upper;//上
    private ImageButton left;//左
    private ImageButton right;//右
    private ImageButton lower;//下
    private ImageView fullScreenAudioButton;//大屏音频控制按钮
    private RelativeLayout fullScreenBack;//大屏返回
    private boolean isMyLandLayout = false;

    //用于他人租地页面时影藏按钮
    private RelativeLayout top_layout;
    private RelativeLayout aa;
    private LinearLayout relativeLayout6;
    private RelativeLayout aaa;

    //app前后台状态
    public boolean isForeground = false;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Integer integer = null;
//            Log.i("排队人数打印",JSONObject.toJSONString(msg.obj));
            try {
                Map<String,Object> map = JSON.parseObject(String.valueOf(msg.obj),Map.class);
                integer = (Integer) map.get("beforeQueue");
            }catch (Exception e){
//                Log.i("排队人数打印1",String.valueOf(e));
                return;
            }
//            Log.i("排队人数打印1",String.valueOf(integer));
            //String obj = String.valueOf(msg.obj);
            //String i = obj.substring(15, 16);
            if (integer < 0) {
                mVideoView.setVisibility(View.VISIBLE);
                line_up.setVisibility(View.GONE);
                //开始播放视频
                mVideoView.start();
                //开始播放音频
                mVideoView2.start();
                switch (state) {
                    case "0":
                        //我的租地观看时长
                        recLen = Config.myLandWatch * 60;
                        handlerVideo.postDelayed(splashhandler, 1000);
                        startAmplification();
                        break;
                    case "1":
                        //可选租地观看限制时长
                        recLen = Config.otherLandWatch * 60;
                        handlerVideo.postDelayed(splashhandler, 1000);
                        break;
                    case "2":
                        //他人租地观看限制时长
                        recLen = Config.otherLandWatch * 60;
                        handlerVideo.postDelayed(splashhandler, 1000);
                        break;
                    default:
                        //默认观看时长
                        recLen = Config.otherLandWatch * 60;
                        handlerVideo.postDelayed(splashhandler, 1000);
                }
            } else {
                handlerVideo.removeCallbacks(splashhandler);
                number_people.setText(String.valueOf(integer+1));
            }

        }
//            if ((Integer) msg.obj == 0) {
//                mVideoView.start();
//            }
//        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTransparent();
        getSelfInfo();
        setContentView(R.layout.land_video_layout);
        //此句爲了解決mob分享后返回時在8.0崩潰問題
        // 并且需要去除Manifest下 .wxapi.WXEntryActivity的android:screenOrientation="portrait"
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //注册
        EventBus.getDefault().register(this);
        Intent intent = getIntent();
        state = intent.getStringExtra("state");
        alias = intent.getStringExtra("alias");
        landNo = intent.getStringExtra("landNo");
        Config.landNumber = Integer.valueOf(landNo);
        landId = intent.getStringExtra("landId");
        username = intent.getStringExtra("UserName");
        userId = intent.getStringExtra("UserId");
        LeaseTime = intent.getStringExtra("LeaseTime");
        ExpirationTime = intent.getStringExtra("ExpirationTime");
        UserImg = intent.getStringExtra("OtherUserImg");
        OthersTel = intent.getStringExtra("OthersTel");
        isLandlord = intent.getStringExtra("isLandlord");
//        Log.i(TAG, "superUserId: "+intent.getStringExtra("superUser"));
        String superUserId = intent.getStringExtra("superUser");
//        Log.i(TAG, "landType: "+intent.getStringExtra("landType"));
        landType = Integer.parseInt(intent.getStringExtra("landType"));

//        Log.e("DeBug", state);
        if ("".equals(superUserId) || null == superUserId){
            superUser = 0;
        }else {
            superUser = Integer.parseInt(intent.getStringExtra("superUser"));
        }

//        Log.i(TAG, "state: "+state+"landNo: "+landNo+"landId: "+landId+"UserName: "+username+"UserId: "+userId
//                +"LeaseTime: "+LeaseTime+"ExpirationTime: "+ExpirationTime+"OtherUserImg: "+UserImg+"OthersTel: "+OthersTel+"isLandlord: "+isLandlord+
//                "superUser: "+superUserId+"landType: "+landType+"superUser: "+superUser);

        App.addDestoryActivity(LandVideoActivity.this, "LandVideoActivity");
        pageNum = 1;
        pageSize = 2;
        initView();
        initViewFullScreen();
        setStatusBarHeight();
        checkUserIsOpenAudio();

        // 解码方式:
        // codec＝AVOptions.MEDIA_CODEC_HW_DECODE，硬解
        // codec=AVOptions.MEDIA_CODEC_SW_DECODE, 软解
        // codec=AVOptions.MEDIA_CODEC_AUTO, 硬解优先，失败后自动切换到软解
        // 默认值是：MEDIA_CODEC_SW_DECODE
        options.setInteger(AVOptions.KEY_MEDIACODEC, AVOptions.MEDIA_CODEC_AUTO);
        // 是否开启直播优化，1 为开启，0 为关闭。若开启，视频暂停后再次开始播放时会触发追帧机制
        // 默认为 0
        options.setInteger(AVOptions.KEY_LIVE_STREAMING,1);

        mVideoView.setBufferingIndicator(loadingView);
        loadingView.setVisibility(View.GONE);
        //初始化mob_sdk
        MobSDK.init(getApplicationContext(), Config.MOB_SDK_S, Config.MOB_SDK_S1);
        albumInit();//加载他人租地视频页面成功相册布局

        setLandInfo();

        pm.add("寄回");
        pm.add("挂售");

        EaseUI.getInstance().setUserProfileProvider(new EaseUI.EaseUserProfileProvider() {
            @Override
            public EaseUser getUser(String username) {

//                Log.i("info", "用户信息: " + username);
                //TODO 这里用来填入用户的昵称和头像
                EaseUser user = Config.AllEMFriendInfo.get(username);
                //TODO 这里是设置自己的头像
                if (user != null && username.equals(EMClient.getInstance().getCurrentUser())) {
                    user.setAvatar(getHeadImg());
                    user.setNickname(username);
                }
                return user;
            }
        });

        setTabStrip();
        setOnClick();

        audioSwitch();
        startAmplification();
    }

    /**
     * 初始化布局
     */
    private void initView(){
        determine = findViewById(R.id.button0);
        amplification = findViewById(R.id.amplification);
        screenshots = findViewById(R.id.imageButton);
        myLand = findViewById(R.id.myLand);
        other = findViewById(R.id.otherland);
        mVideoView = findViewById(R.id.PLVideoTextureView);
        mVideoView2 = findViewById(R.id.PLVideoTextureView2);
        customerService = findViewById(R.id.customer_service);
        addBut = findViewById(R.id.addbutton);
        informationBut = findViewById(R.id.informationBut);
        loadingView = findViewById(R.id.LoadingView);
        prompt = findViewById(R.id.prompt);
        prompt_btn = findViewById(R.id.prompt_btn);
        line_up = findViewById(R.id.line_up);
        number_people = findViewById(R.id.number_people);
        collection = findViewById(R.id.collection);
        landnumber = findViewById(R.id.land_number);
        title = findViewById(R.id.title_name);
        nickname = findViewById(R.id.nickname);
        otherLeaseTime = findViewById(R.id.otherLeaseTime);
        otherExpirationTime = findViewById(R.id.otherExpirationTime);
        otherUserImg = findViewById(R.id.other_username);
        LandNo = findViewById(R.id.landNo);
        login = findViewById(R.id.login);
        video_text_view = findViewById(R.id.video_text_view);
        videoViewLayout = findViewById(R.id.video_view_layout);
//        videoBgViewLayout = findViewById(R.id.video_bg_view_layout);
        audioButton = findViewById(R.id.audio_button);
        audioTipsLayout = findViewById(R.id.audio_tips_layout);
        audioText = findViewById(R.id.audio_text);
        plview = findViewById(R.id.PLview);//视频布局layout
        noPlayPlview = findViewById(R.id.noPlay_PLview);
        TextView noPlayViewText = findViewById(R.id.noPlay_PLview_text);
        noPlayViewText.setText(Config.rtspEndTime+":00-"+Config.rtspStartTime+":00时段停止观看视频!");
        if (isCurrentInTimeScope(20,0,7,0)){
            plview.setVisibility(View.GONE);
            noPlayPlview.setVisibility(View.VISIBLE);
            System.out.println("当前时间在时间区间里面");
        } else {
            plview.setVisibility(View.VISIBLE);
            noPlayPlview.setVisibility(View.GONE);
            System.out.println("当前时间不在时间区间里面");
        }
        titleLayout = findViewById(R.id.relativeLayout2);//标题栏布局layout
        landVideoBar = findViewById(R.id.land_video_bar);//标题bar

        top_layout = findViewById(R.id.top_layout);
        aa = findViewById(R.id.aa);
        relativeLayout6 = findViewById(R.id.relativeLayout6);
        aaa = findViewById(R.id.aaa);
    }

    /**
     * 初始化全屏view
     */
    private void initViewFullScreen() {
        fullScreenLayout = findViewById(R.id.full_screen_layout);//全屏布局样式
        fullScreenBack = findViewById(R.id.full_screen_back_layout);
        seekBarAdd = findViewById(R.id.add_seekBar);
        seekBarDelete = findViewById(R.id.delete_seekBar);
        resetBtn = findViewById(R.id.reset_text);
        settingBtn = findViewById(R.id.setting_text);
        upper = findViewById(R.id.upper);
        left = findViewById(R.id.left);
        right = findViewById(R.id.right);
        lower = findViewById(R.id.lower);
        fullScreenAudioButton = findViewById(R.id.audio_big_button);
        //滑动条相关方法
        addSeekBar();
        deleteSeekBar();
        resetButton();
        settingButton();
        upperButton();
        lowerButton();
        leftButton();
        rightButton();
        fullScreenAudioSwitch();
        fullScreenBackClick();
    }

    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(this);
        //相对布局使用方法
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        landVideoBar.setLayoutParams(params);
        //线性布局使用方法
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        landVideoBar.setLayoutParams(params);
    }
    /**
     * 判断用户是否开启直播音频声音
     */
    private void checkUserIsOpenAudio() {
        if (Config.AUDIO_STATE) {
            //open
            Config.audioIsPlay = true;
        } else {
            //close
            Config.audioIsPlay = false;
        }
        checkAudio();
    }

    /**
     * 设置页面数据显示
     */
    @SuppressLint("SetTextI18n")
    private void setLandInfo(){
        switch (state){
            case "0": //我的租地
                determine.setVisibility(View.VISIBLE);
                amplification.setVisibility(View.VISIBLE);
                screenshots.setVisibility(View.VISIBLE);
                collection.setVisibility(View.GONE);
                exhibition.setVisibility(View.GONE);
                publicPhoto.setVisibility(View.GONE);
                myLand.setVisibility(View.VISIBLE);
                other.setVisibility(View.GONE);
                LandNo.setText("我的租地");
                checkPermission();
                isMyLandLayout = true;
                break;
            case "1": //空地
                determine.setVisibility(View.GONE);
                amplification.setVisibility(View.VISIBLE);
                screenshots.setVisibility(View.GONE);
                collection.setVisibility(View.GONE);
                exhibition.setVisibility(View.GONE);
                publicPhoto.setVisibility(View.GONE);
                myLand.setVisibility(View.VISIBLE);
                other.setVisibility(View.GONE);
                LandNo.setText(alias + "号租地");
                break;
            case "2": //他人租地
                determine.setVisibility(View.GONE);
                amplification.setVisibility(View.VISIBLE);
                screenshots.setVisibility(View.GONE);
                collection.setVisibility(View.VISIBLE);
                exhibition.setVisibility(View.VISIBLE);
                publicPhoto.setVisibility(View.VISIBLE);
//            albumInit();
                //加载租地页面他人成长相册
                getAlbum(false,1);
                other.setVisibility(View.VISIBLE);
                myLand.setVisibility(View.GONE);
                LandNo.setText(username + "的租地");
                customerService.setVisibility(View.GONE);

                landnumber.setText(landNo);
                nickname.setText(username);
                otherLeaseTime.setText(LeaseTime);
                otherExpirationTime.setText(ExpirationTime);

                Queryfriends(userId);

                if (NumUtil.checkNull(UserImg)) {
                    otherUserImg.setImageResource(R.drawable.icon_user_img);
                } else {
                    //Picasso使用了流式接口的调用方式
                    //Picasso类是核心实现类。
                    //实现图片加载功能至少需要三个参数：
                    Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                            .load(UserImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                            .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                            .into(otherUserImg);//into(ImageView targetImageView)：图片最终要展示的地方。
                }
                AddFriend();
                LandCollection();
                AddCollection();
                break;
            default:
                break;
        }
    }

    /**
     * 设置viewpager与选项卡
     */
    private void setTabStrip(){
        tabStrip = findViewById(R.id.land_video_tab);
        ViewPager viewPager = findViewById(R.id.land_video_viewpage);
        viewPager.setOffscreenPageLimit(0); //取消原来缓存6页  ，数据太多 视频无法加载  部分页面加上 TODO if (list != null){list.clear();} 预防数据重复
        Fragment[] fragments = {new CuringPage(), new CropPage(), new HarvestPage(), new HistoryPage(), new UserLogsPage(), new TutorialPage()};
        //landType == 1 是蔬菜租地   == 2 是家禽租地
        String[] titles = {"养护", landType == 1 ? "种植" : "饲养", "收成", "历史", "日志", "教程"};
        TabPagerAdapter adapter = new TabPagerAdapter(getSupportFragmentManager(), fragments, titles);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //此方法是页面跳转完后得到调用，position是你当前选中的页面的Position（位置编号）(从A滑动到B，就是B的position)
                viewPager.setCurrentItem(position, false);
                updateTextStyle(position);
                poss = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabStrip.setViewPager(viewPager);
    }

    /**
     * 开启或关闭音频流声音
     */
    private void audioSwitch() {
        audioButton.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (Config.audioIsPlay) {
                    //播放
                    Config.audioIsPlay = false;
                    audioButton.setImageResource(R.drawable.audio_close);
                    fullScreenAudioButton.setImageResource(R.drawable.audio_close);
                    //mVideoView2.pause();
                    mVideoView2.setVolume(0,0);
                    setAudioTipsLayout("声音已关闭", 5000);
                } else {
                    //未播放
                    Config.audioIsPlay = true;
                    audioButton.setImageResource(R.drawable.audio_open);
                    fullScreenAudioButton.setImageResource(R.drawable.audio_open);
                    //mVideoView2.start();
                    mVideoView2.setVolume(10,10);
                    setAudioTipsLayout("声音已开启",5000);
                }
            }
        });
    }
    /**
     * 检查音频声音是否开启
     */
    private void checkAudio() {
        if (Config.audioIsPlay) {
            mVideoView2.setVolume(10,10);
            audioButton.setImageResource(R.drawable.audio_open);
            fullScreenAudioButton.setImageResource(R.drawable.audio_open);
        } else {
            mVideoView2.setVolume(0,0);
            audioButton.setImageResource(R.drawable.audio_close);
            fullScreenAudioButton.setImageResource(R.drawable.audio_close);
        }
    }

    /**
     * 声音提示布局动画设置
     * @param text  提示文字内容
     */
    private void setAudioTipsLayout(String text, long duration){
        audioText.setText(text);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(audioTipsLayout,"alpha",0,1,1,1,1,0);
        objectAnimator.setDuration(duration);
        objectAnimator.start();
    }
    /**
     * 点击事件
     */
    private void setOnClick(){
        //TODO 截图点击事件
        screenshots.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (!mVideoView.isPlaying()) {
                    showToastShort(LandVideoActivity.this, TextString.SCREENSHOTS);
                    return;
                }
                mVideoView.captureImage(0);
            }
        });
        //TODO 客服聊天点击事件
        customerService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userInfoTokenCus();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateTextStyle(poss);
        userInfoToken();
//        playerState();
        VideoClass();
    }

    /**
     * 获取用户信息
     */
    private void getSelfInfo(){
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if ("success".equals(response.body().getFlag())){
                    if (response.body().getResult().getLandId() == Integer.valueOf(landNo)
                            || 1 == response.body().getResult().getSuperUser()){
                        state = "0";
                        userId = String.valueOf(response.body().getResult().getId());
                        myLand.setVisibility(View.VISIBLE);
                        other.setVisibility(View.GONE);
                        determine.setVisibility(View.VISIBLE);
                        amplification.setVisibility(View.VISIBLE);
                        screenshots.setVisibility(View.VISIBLE);
                        collection.setVisibility(View.GONE);
                        isLandlord = String.valueOf(response.body().getResult().getIsLandlord());
                        superUser = response.body().getResult().getSuperUser();
                    }
                    audioButton.setVisibility(View.VISIBLE);
                    switch (state) {
                        case "0":
                            int landid = Integer.valueOf(landId);
                            hosting(landid);//查询租地是否处于托管
//                            getLandInfo(landid);//查询当前租地信息
                            break;
                        default:
                            determine.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    showToastShort(LandVideoActivity.this, "您尚未购买此地");
                                }
                            });
                    }
//                    if ("0".equals(state)) {
//                        Log.e(TAG,"托管租地id："+landId);
//                        int landid = Integer.valueOf(landId);
//                        hosting(landid);//查询租地是否处于托管
//                    } else if (!("0".equals(state))){
//                        determine.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Toast.makeText(LandVideoActivity.this, "您尚未购买此地", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                    }

                }
            }

            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }

    /**
     * 查询地号是否托管
     * @param hostLandId
     */
    private void hosting(int hostLandId){
        Call<SearchLandByNo> searchLandByNo = HttpHelper.initHttpHelper().searchLandByNo(hostLandId);
        searchLandByNo.enqueue(new Callback<SearchLandByNo>() {
            @Override
            public void onResponse(Call<SearchLandByNo> call, Response<SearchLandByNo> response) {
                Log.e("DeBug",JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    isHosted = response.body().getResult().getIsHosted();
                    saveStates(String.valueOf(response.body().getResult().getIsHosted()));
                    if (isHosted == 0) {
                        determine.setText("托管");
                    } else if (isHosted == 1) {
                        determine.setText("取消托管");
                    }
                    determine.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String hostingText = (String) determine.getText();
                            showDialog1(hostingText, determine);
                        }
                    });
                } else {
                    determine.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showToastShort(LandVideoActivity.this, "托管状态获取异常");
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<SearchLandByNo> call, Throwable t) {
                Log.e(TAG,"托管接口查询失败");
            }
        });
    }

    //TODO 身份信息
    public void userInfoTokenCus(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if ("failed".equals(response.body().getFlag())) {
                    noLoginDialog();
                } else {
                    //TODO 判断当前是否有账号登录
                    if (!EMClient.getInstance().isLoggedInBefore() || "isOneLogin".equals(Config.isOneLogin)){
                        Config.isOneLogin = "noOneLogin";
                        loginEM(2);
                    }else {
                        customerAllot();
                    }
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                showToastShort(LandVideoActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }
    //TODO 客服分配接口
    public void customerAllot(){
        Call<CusChat> cusAllot = HttpHelper.initHttpHelper().addCusChatList();
        cusAllot.enqueue(new Callback<CusChat>() {
            @Override
            public void onResponse(Call<CusChat> call, Response<CusChat> response) {
                if ("failed".equals(response.body().getFlag())) {
                    noLoginDialog();
                } else {
                    for (CusChat.ResultBean user : response.body().getResult()) {
                        if (null != user.getCusUuid()) {
                            EaseUser u = new EaseUser(user.getCusUuid().toString());
                            u.setAvatar("http://www.xtai.com/static/img/side_s01.gif");
                            u.setNickname("客服" + "（" + user.getCusServiceName() + "）");
                            if (!Config.AllEMFriendInfo.containsKey("" + user.getEChatId())) {
                                Config.AllEMFriendInfo.put("" + user.getEChatId(), u);
                            }
                            Config.eChatId = String.valueOf(user.getEChatId());
                        }
                    }
                    //TODO 这里主要是根据用户uuid 存放自己的id 好填入自己的头像
                    EaseUser u = new EaseUser(getUuId());
                    if (!Config.AllEMFriendInfo.containsKey("" + getUserId())) {
                        Config.AllEMFriendInfo.put("" + getUserId(), u);
                    }
                    //TODO 进入聊天
                    Intent intent = new Intent(LandVideoActivity.this, ChatActivity.class);
                    intent.putExtra("customer","customer");
                    intent.putExtra(EaseConstant.EXTRA_USER_ID, String.valueOf(response.body().getResult().get(0).getEChatId()));
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<CusChat> call, Throwable t) {
                try {
                    if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                        noLoginDialog();
                    } else {
                        showToastShort(LandVideoActivity.this, TextString.NetworkRequestFailed);
                    }
                }catch (Exception e){
                    Log.e("FarmPage->", "onFailure: "+ e);
                }
            }
        });
    }

    //TODO 监听播放器状态
    private void playerState(){
        if (getUserId() != 0){
            mVideoView.setVideoPath(videoPath);
            mVideoView.setOnInfoListener(new PLOnInfoListener() {
                @Override
                public void onInfo(int i, int i1) {
                    Log.i("播放器当前状态", String.valueOf(i1));
                    Log.i("播放器当前状态2", String.valueOf(i));
                    if (i == 340){
                        stopAmplification("视频正在缓冲");
//                        loadingView.setVisibility(View.VISIBLE);
                    }
                    if (i == 200 || i == 10004){ //连接成功
                        mVideoView.start();
                        loadingView.setVisibility(View.GONE);
                        startAmplification();
                    }
                }
            });
            mVideoView.setOnErrorListener(new PLOnErrorListener(){
                @Override
                public boolean onError(int i) {
                    Log.i("播放器当前状态错误码", String.valueOf(i));
                    if (i == -3){ //网络异常
                        mVideoView.start();
//                        mVideoView.setVisibility(View.GONE);
                        loadingView.setVisibility(View.VISIBLE);
                        options.setInteger(AVOptions.KEY_OPEN_RETRY_TIMES, 5);
//                        handlerVideo.removeCallbacks(splashhandler);//TODO 移除定时器
                        stopAmplification("网络连接异常");
                    } else if (i == -2	|| i == -1){ //播放器打开失败与未知错误
                        mVideoView.start();
//                        mVideoView.setVisibility(View.GONE);
                        loadingView.setVisibility(View.VISIBLE);
                        options.setInteger(AVOptions.KEY_OPEN_RETRY_TIMES, 5);
//                        handlerVideo.removeCallbacks(splashhandler);//TODO 移除定时器
                        stopAmplification("视频连接失败");
                    } else {
                        loadingView.setVisibility(View.GONE);
                    }
                    return false;
                }
            });
        }
    }

    /**
     * TODO 地主登录摄像头
     * 由於後端已經會自動進行攝像頭登錄
     */
    @Deprecated
    private void landlordLogin(){
        HttpHelper.initHttpHelper().landlordLogin().enqueue(new Callback<CheckSMS>(){
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                Log.e("landlordLogin：", JSON.toJSONString(response.body()));
                try{
                    if ("success".equals(response.body().getFlag())) {
                        resultLong = Long.parseLong(response.body().getResult());
                    } else {

                    }
                }catch (NullPointerException e){
                    return;
                }

            }
            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t){

            }
        });
    }
    //TODO 地主退出摄像头
    private void cleanup(){
        HttpHelper.initHttpHelper().cleanup(Integer.parseInt(landNo)).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                Log.e("landlordLogin：", JSON.toJSONString(response.body()));
            }
            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {

            }
        });
    }

    /**
     * 添加好友点击事件
     */
    private void AddFriend(){
        addBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetWorkUtils.isNetworkAvalible(LandVideoActivity.this)) {
                    showToastShort(LandVideoActivity.this, getResources().getString(R.string.no_network));
                } else if (App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", "").equals("")) {
                    noLoginDialog();
                } else {
                    addFriendNet();
                }
            }
        });
    }

    /**
     * 添加好友接口
     */
    private void addFriendNet() {
        HttpHelper.initHttpHelper().addApply(Integer.parseInt(userId)).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                Log.e("好友：", JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())) {
                    try {
                        //参数为要添加的好友的username和添加理由
                        EMClient.getInstance().contactManager().addContact(userId, "");
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                    }
                    showSetDeBugDialog("好友申请已发送，等待对方确认");
                } else {
                    showSetDeBugDialog(response.body().getResult());
                }
            }
            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                showToastShort(LandVideoActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }

    /**
     * TODO 收藏租地点击事件
     */
    private void AddCollection(){
        collection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int UserId = Integer.valueOf(userId);
                int LandId = Integer.valueOf(landId);
                if (!NetWorkUtils.isNetworkAvalible(LandVideoActivity.this)) {
                    showToastShort(LandVideoActivity.this, getResources().getString(R.string.no_network));
                } else if (App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", "").equals("")) {
                    showToastShort(LandVideoActivity.this, "您尚未登录，无法收藏租地");
                } else if (!isCollect) {
                    addCollectNet(UserId, LandId);
                } else if (isCollect){
                    delCollect(UserId, LandId);
                }
            }
        });
    }

    /**
     * 收藏租地接口
     */
    private void addCollectNet(int userId, int landId) {
        Call<CheckSMS> addCollect = HttpHelper.initHttpHelper().addCollect(userId, landId);
        addCollect.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if (("success").equals(response.body().getFlag())) {
                    showToastShort(LandVideoActivity.this, "收藏成功");
                    collection.setBackgroundResource(R.mipmap.icon_collection2);
                    isCollect = true;
                } else {
                    showToastShort(LandVideoActivity.this, "收藏失败");
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                showToastShort(LandVideoActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }

    /**
     * 取消租地收藏接口
     */
    private void delCollect(int userId, int landId){
        Call<CheckSMS> deleteCollect = HttpHelper.initHttpHelper().deleteCollect(userId, landId);
        deleteCollect.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                Log.e(TAG, response.body().getFlag());
                if (("success").equals(response.body().getFlag())) {
                    showToastShort(LandVideoActivity.this, "取消收藏成功");
                    collection.setBackgroundResource(R.mipmap.icon_collection1);
                    isCollect = false;
                } else {
                    showToastShort(LandVideoActivity.this, "取消收藏失败");
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                showToastShort(LandVideoActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }

    /**
     * TODO 查询租地收藏
     */
    private void LandCollection(){
        Call<SelectLandCollection> selectLandCollection = HttpHelper.initHttpHelper().selectLandCollectionVideo();
        selectLandCollection.enqueue(new Callback<SelectLandCollection>() {
            @Override
            public void onResponse(Call<SelectLandCollection> call, Response<SelectLandCollection> response) {
//                    Log.e(TAG, JSON.toJSONString(response.body()));
                for (int i = 0; i < response.body().getResult().getList().size(); i++) {
                    if (response.body().getResult().getList().get(i).getLandId() == Integer.valueOf(landId)) {
                        collection.setBackgroundResource(R.mipmap.icon_collection2);
                        isCollect = true;
                    }
                }
            }

            @Override
            public void onFailure(Call<SelectLandCollection> call, Throwable t){

            }
        });
    }
    private void updateTextStyle(int position){
        LinearLayout tabsContainer = (LinearLayout) tabStrip.getChildAt(0);
        for(int i=0; i< tabsContainer.getChildCount(); i++){
            TextView textView = (TextView) tabsContainer.getChildAt(i);
            if(position == i) {
                textView.setTextSize(14);
                textView.setTextColor(getResources().getColor(R.color.theme_color));
            } else {
                textView.setTextSize(12);
                textView.setTextColor(getResources().getColor(R.color.color_5b5b5b));
            }
        }
    }

    private void checkPermission() {
        //检查权限（NEED_PERMISSION）是否被授权 PackageManager.PERMISSION_GRANTED表示同意授权
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            //用户已经拒绝过一次，再次弹出权限申请对话框需要给用户一个解释
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission
                    .WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "请开通相关权限，否则无法正常使用本应用", Toast.LENGTH_SHORT).show();
            }
            //申请权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE);

        } else {
//            Toast.makeText(this, "授权成功！", Toast.LENGTH_SHORT).show();
//            Log.e("权限：", "checkPermission: 已经授权！");
        }
    }

    @Override
    protected void onResume() {
        if (!isForeground) {
            //app 从后台唤醒，进入前台
            isForeground = true;
            Log.i("ACTIVITY", "程序从后台唤醒");
        }
        super.onResume();
        checkAudio();//检查声音是否开启
//        Log.i("reclen打印：",String.valueOf(recLen));
        if (recLen != 0 || superUser == 1){
            mVideoView.start();
            mVideoView2.start();
        }
        //TODO 截图回调
        mVideoView.setOnImageCapturedListener(data -> {
            runOnUiThread(() -> {
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                if (bitmap == null) {
                    showToastShort(this, "截图失败");
                    return;
                }

                photoDialog(bitmap);
            });
//                Log.i("TAG", "onResume: data");
        });
    }

    public void VideoClass(){
        isWifi = NetWorkUtils.isWifiConnected(LandVideoActivity.this);
//        url = "ws://" + "192.168.2.30" + ":9293";
        url = VideoConfig.VideoWS;
        uri = URI.create(url);
        String userToken = getUserToken();
        setVideoPath();//设置推流地址
        if (!"".equals(userToken)) {
            if (videoFlag) {
                videoFlag = false;
                if (isWifi || "trafficYes".equals(getTraffic())) {
//                    Log.e("state", state);
                    if (("0").equals(state) && superUser == 1) {
                        superLandlordWatch(false);
                    } else if (("0").equals(state)){
                        myLandWatch(Config.myLandWatch);
                    }else if (("2").equals(state)) {
                        myLandWatch(Config.otherLandWatch);
                    } else {
                        myLandWatch(Config.otherLandWatch);
                    }
                } else {
                    stopAmplification("您需要允许流量播放哦");
//                    mVideoView.pause();
                    if (getUserId() != 0 || !("".equals(userToken))) {
                        HttpHelper.initHttpHelper().test().enqueue(new Callback<Test>() {
                            @Override
                            public void onResponse(Call<Test> call, Response<Test> response) {
                                if (("success").equals(response.body().getFlag())){
                                    if (isShowPrompt) {
                                        prompt.setVisibility(View.VISIBLE);
                                    }
                                    if (("0").equals(state) && superUser == 1) {
                                        flowPlay(Config.myLandWatch, true);//流量观看按钮点击事件
                                    } else if ("0".equals(state) || userId .equals(App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", ""))) {
                                        flowPlay(Config.myLandWatch, false);//流量观看按钮点击事件
                                    } else if ("2".equals(state)) {
                                        flowPlay(Config.otherLandWatch, false);
                                    } else {
                                        flowPlay(Config.otherLandWatch, false);
                                    }
                                }else if (("failed").equals(response.body().getFlag())){
                                    noLoginDialog();
                                }
                            }
                            @Override
                            public void onFailure(Call<Test> call, Throwable t) {
                                Toast.makeText(LandVideoActivity.this, TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        }
    }

    /**
     * TODO 流量播放按钮点击事件
     * @param watchTime 观看时长
     * @param superUser 是否超级地主
     */
    private void flowPlay(int watchTime, boolean superUser){
        prompt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (superUser) {
                    superLandlordWatch(true);
                } else {
                    myLandWatch(watchTime);
                }
                isShowPrompt = !isShowPrompt;
                prompt.setVisibility(View.GONE);
            }
        });
    }

    //TODO 我的租地观看视频
    private void myLandWatch(int watch){
        HttpHelper.initHttpHelper().getVideoSize(Integer.parseInt(landNo)).enqueue(new Callback<GetVideoSize>() {
            @Override
            public void onResponse(Call<GetVideoSize> call, Response<GetVideoSize> response) {
//                Log.e("picher_log", JSON.toJSONString(response.body()));
                try{
                    if (("success").equals(response.body().getFlag())) {
                        loadingView.setVisibility(View.VISIBLE);
                        if (1 == response.body().getResult().getIsWatch()) {
                            //            mVideoView.pause();//暂停播放
                            //他人租地观看限制时长
                            recLen = watch * 60;
                            setVideoPath();//设置推流地址
                            mVideoView.setVisibility(View.VISIBLE);
                            mVideoView.start();//开始播放
                            mVideoView2.start();
                            // 打开重试次数，设置后若打开流地址失败，则会进行重试
                            options.setInteger(AVOptions.KEY_OPEN_RETRY_TIMES, 5);
//                                                    timer.schedule(task, 1000, 1000);
                            handlerVideo.postDelayed(splashhandler, 1000);
                            if (("0").equals(state)) {
                                startAmplification();
                            }
//                            Log.e("state", "进入");
//                                mVideoView.start();
                            //mVideoView.pause();//暂停播放
                        } else if (0 == response.body().getResult().getIsWatch()) {
                            handlerVideo.removeCallbacks(splashhandler);
                            stopAmplification("您视频还处于排队哦");
                            line_up.setVisibility(View.VISIBLE);
                            number_people.setText(String.valueOf(response.body().getResult().getQueueNum()+1));
                            num1 = response.body().getResult().getQueueNum();
                            loadingView.setVisibility(View.GONE);
                            String titleText = "立即排队";
                            String contentText = "您前面还有" + (num1+1) + "个人排队";
                            showUserInfoDialog(titleText, contentText);
                        }
                    }
                }catch (NullPointerException e){
//                    Log.e("myLandWatch", String.valueOf(e));
                }
            }

            @Override
            public void onFailure(Call<GetVideoSize> call, Throwable t) {
//                Log.e(TAG, t.getMessage());
                mVideoView.pause();
                mVideoView2.pause();
                Toast.makeText(LandVideoActivity.this,TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 超级地主使用 播放视频
     * @param isWifi 是否为wifi true是 反之
     */
    private void superLandlordWatch(boolean isWifi){
        HttpHelper.initHttpHelper().getVideoSize(Integer.parseInt(landNo)).enqueue(new Callback<GetVideoSize>() {
            @Override
            public void onResponse(Call<GetVideoSize> call, Response<GetVideoSize> response) {
//                Log.e("picher_log", JSON.toJSONString(response.body()));
                try{
                    if (1 == response.body().getResult().getIsWatch()) {
                        startAmplification();//开始放大功能
                        loadingView.setVisibility(View.VISIBLE);
                        Log.e(TAG, "VideoClass: " + videoPath);
                        mVideoView.setVisibility(View.VISIBLE);
                        mVideoView.start();//开始播放

                        mVideoView2.start();
                        // 打开重试次数，设置后若打开流地址失败，则会进行重试
                        options.setInteger(AVOptions.KEY_OPEN_RETRY_TIMES, 5);
                        if (isWifi){
                            prompt.setVisibility(View.GONE);
                        }
                    } else if (0 == response.body().getResult().getIsWatch()) {
                        Toast.makeText(LandVideoActivity.this,"观看人数上限",Toast.LENGTH_SHORT).show();
                    } else {
//                        Log.e(TAG, "onResponse: " + "超级地主观看视频请求接口判断失败");
                    }
                }catch (NullPointerException e){
                    Log.e("myLandWatch", String.valueOf(e));
                }
            }

            @Override
            public void onFailure(Call<GetVideoSize> call, Throwable t) {
//                Log.e(TAG, t.getMessage());
                mVideoView.pause();
                mVideoView2.pause();
                showToastShort(LandVideoActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }

    //TODO 他人租地和空地
    private void otherSpace(){
        HttpHelper.initHttpHelper().getVideoSize(Integer.parseInt(landNo)).enqueue(new Callback<GetVideoSize>() {
            @Override
            public void onResponse(Call<GetVideoSize> call, Response<GetVideoSize> response) {
//                Log.e("picher_log", JSON.toJSONString(response.body()));
                try{
                    if (("success").equals(response.body().getFlag())){
                        if (response.body().getResult().getIsWatch() == 1) {
                            //他人租地观看限制时长
                            recLen = Config.otherLandWatch * 60;
                            mVideoView.start();//开始播放
                            handlerVideo.postDelayed(splashhandler, 1000);
                            startAmplification();
//                            Log.e("state", "进入");
                        } else if (response.body().getResult().getIsWatch() == 0) {
                            handlerVideo.removeCallbacks(splashhandler);
                            line_up.setVisibility(View.VISIBLE);
                            number_people.setText(String.valueOf(response.body().getResult().getQueueNum()+1));
                            num1 = response.body().getResult().getQueueNum();
                            String titleText = "立即排队";
                            String contentText = "您前面还有" + (num1+1) + "个人排队";
                            showUserInfoDialog(titleText, contentText);
                        }
                    }
                }catch (NullPointerException e){
                    Log.e("otherLandWatch", String.valueOf(e));
                }
            }

            @Override
            public void onFailure(Call<GetVideoSize> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    /**
     * 设置推流地址
     */
    private void setVideoPath(){
        //设置音频流地址
        mVideoView2.setVideoPath("rtmp://222.93.107.75:1935/live/hf");
        switch (landNo.length()){
            case 1:
                videoPath = VideoConfig.VideoUrl+"000"+landNo;
                break;
            case 2:
                videoPath = VideoConfig.VideoUrl+"00"+landNo;
                break;
            case 3:
                videoPath = VideoConfig.VideoUrl+"0"+landNo;
                break;
            case 4:
                videoPath = VideoConfig.VideoUrl+landNo;
                break;
            default:
                break;
        }
        mVideoView.setVideoPath(videoPath);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        Log.e("state", "暂停" + isAppOnForeground());
////        mVideoView.pause();//暂停播放
//        if (isAppOnForeground()) {
//
//        }
    }

    @Override
    protected void onStop() {
        if (!isAppOnForeground()) {
            //app 进入后台
            isForeground = false;//记录当前已经进入后台
            Log.i("ACTIVITY", "程序进入后台");
            Log.i("由前台切换到后台：", "onPause: " );
            //需要关闭用户查看的摄像头
            if (Config.landNumber != null) {
                Log.i("由前台切换到后台：", "关闭视频流" );
                stopVideoView();
            } else {
                Log.i("由前台切换到后台：", "暂无视频流开启 " );
            }
        } else {
            Log.i(TAG, "onStop: ");
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //反注册
        EventBus.getDefault().unregister(this);
//        Log.e("state", "释放");
    }

    public String getToken() {
        return token;
    }

    public String getLandNos() {
        return landNo;
    }

    public String getState() {
        return state;
    }

    public String getLandId() {
        return landId;
    }

    public int getLandType() {
//        Log.d(TAG, "getLandType: " + landType);
        return landType;
    }

    public void getState(String state) {
        this.state = state;
    }


    public void back(View view) {
        stopVideoView();
        stopMP4View();
//        finish();//使用这个导致第二次再次进入无法观看视频
        super.onBackPressed();
    }

    /**
     * 重写返回键
     * 监听Back键按下事件,方法1:
     * 注意:
     * super.onBackPressed()会自动调用finish()方法,关闭
     * 当前Activity.
     * 若要屏蔽Back键盘,注释该行代码即可
     */
    @Override
    public void onBackPressed() {
        if (!isFullScreen) {
            videoViewFullScreen(false);
            return;
        }
        super.onBackPressed();
        stopVideoView();
        stopMP4View();
    }


    private void stopMP4View() {
        audioPlay = false;
        if (null != mVideoView2) {
            mVideoView2.stopPlayback();//释放资源
        }
    }

    /**
     * 停止直播与释放
     */
    private void stopVideoView() {
        mVideoView.stopPlayback();//释放资源
        stopVideo();
        recLen = 0;
//        flag = false;
        //停止定时器
        handlerVideo.removeCallbacks(splashhandler);
        if (mSocketClient != null) {
            mSocketClient.close();
        }
        cleanup();
    }
    //TODO 托管弹框
    private void showDialog1(String hostingText, Button button) {
        Dialog bottomDialog = new Dialog(this, R.style.dialog);
        if (hostingText.equals("托管")) {
            View contentView = LayoutInflater.from(this).inflate(R.layout.layout_hosting_dialog, null);
            bottomDialog.setContentView(contentView);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
            params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(this, 100f);
//            params.bottomMargin = getResources().getDisplayMetrics().heightPixels/2;
            contentView.findViewById(R.id.host_sure_bt).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowDialog2(pm, "请选择托管后作物去向", 1);
//                    trusteeship(1,"取消托管","托管失败");
                    bottomDialog.cancel();
                }
            });
            contentView.findViewById(R.id.host_cancel_bt).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomDialog.cancel();
                }
            });
            contentView.setLayoutParams(params);
            bottomDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
            bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
            bottomDialog.show();
        } else {
            View contentView = LayoutInflater.from(this).inflate(R.layout.layout_nohosting_dialog, null);
            bottomDialog.setContentView(contentView);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
            params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(this, 100f);
//            params.bottomMargin = DensityUtil.dp2px(this, 250f);
            contentView.findViewById(R.id.nohost_sure_bt).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    trusteeship(0, "托管", "取消托管失败", String.valueOf(-1));
                    bottomDialog.cancel();
                }
            });
            contentView.findViewById(R.id.nohost_cancel_bt).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomDialog.cancel();
                }
            });
            contentView.setLayoutParams(params);
            bottomDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
            bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
            bottomDialog.show();
        }

    }

    public void ShowDialog2(List<String> list, String titles, int pos) {
//        Log.e("state",String.valueOf(maintainId.get(position)));
        Context context = LandVideoActivity.this;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_dialog, null);
        ListView myListView = layout.findViewById(R.id.formcustomspinner_list);
        ListDialogAdapter adapter = new ListDialogAdapter(context, list);
        myListView.setAdapter(adapter);
        TextView title = layout.findViewById(R.id.label);
        title.setText(titles);
        layout.findViewById(R.id.list_dialog_bt).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int positon, long id) {
                trusteeship(pos, "取消托管", "托管失败", String.valueOf(positon));
                alertDialog.cancel();
            }
        });
        builder = new AlertDialog.Builder(context);
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void trusteeship(int state, String trusteeshipName, String failName, String default_harvest) {
        int landNos = Integer.valueOf(landNo);
        Call<CheckSMS> hostingLand = HttpHelper.initHttpHelper().hostingLand(default_harvest, state, landNos);
        hostingLand.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                Log.e("状态", JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    determine.setText(trusteeshipName);
                    if ("-1".equals(default_harvest)) {
                        saveStates("0");
                        isHosted = 0;
                    } else {
                        saveStates("1");
                        isHosted = 1;
                    }
                } else {
                    showToastShort(LandVideoActivity.this, failName);
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {

            }
        });
    }
    /**
     * 事件响应方法  进入菜场停止视频
     * 接收消息
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(VideoEvent event) {
        String msg = event.getINFOSUCCESSFUL();
        if ("INFOSUCCESSFUL".equals(msg) ){
            super.onBackPressed();
            stopVideoView();
            stopMP4View();
        }
    }
    /**
     * onekeyshare分享调用九宫格方法
     */
    private void showShare() {
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
        // title标题，印象笔记、邮箱、信息、微信、人人网、QQ和QQ空间使用
        oks.setTitle(null);
        // text是分享文本，所有平台都需要这个字段
        oks.setText(null);
        // titleUrl是标题的网络链接，仅在Linked-in,QQ和QQ空间使用
        oks.setTitleUrl(null);
        //分享的图片  （这里是网络图片的地址）
        oks.setImageUrl(Environment.getExternalStorageDirectory()+ "/screenshot.png");
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
//        oks.setImagePath(Environment.getExternalStorageDirectory()+ "/sdcard/test.jpg");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setShareContentCustomizeCallback(new ShareContentCustomizeCallback() {
            @Override
            public void onShare(Platform platform, Platform.ShareParams paramsToShare) {
                if (platform.getName().equalsIgnoreCase(QQ.NAME)) {
                    paramsToShare.setText(null);
                    paramsToShare.setTitle(null);
                    paramsToShare.setTitleUrl(null);
                    //paramsToShare.setImagePath("/sdcard/screenshot.png");
                    paramsToShare.setImagePath(Environment.getExternalStorageDirectory()+ "/screenshot.png");
                } else if (platform.getName().equalsIgnoreCase(QZone.NAME)) {
                    paramsToShare.setText(null);
                    paramsToShare.setTitle(null);
                    paramsToShare.setTitleUrl(null);
                    paramsToShare.setImagePath(Environment.getExternalStorageDirectory()+ "/screenshot.png");
//                    paramsToShare.setImagePath(Environment.getExternalStorageDirectory()+ "/sdcard/test.jpg");
                } else if (platform.getName().equalsIgnoreCase(Wechat.NAME)) {
                    paramsToShare.setText(null);
                    paramsToShare.setTitle(null);
                    paramsToShare.setTitleUrl(null);
                    paramsToShare.setImagePath(Environment.getExternalStorageDirectory()+ "/screenshot.png");
//                    paramsToShare.setImagePath(Environment.getExternalStorageDirectory()+ "/sdcard/test.jpg");
                } else if (platform.getName().equalsIgnoreCase(WechatMoments.NAME)) {
                    paramsToShare.setText(null);
                    paramsToShare.setTitle(null);
                    paramsToShare.setTitleUrl(null);
                    paramsToShare.setImagePath(Environment.getExternalStorageDirectory()+ "/screenshot.png");
//                    paramsToShare.setImagePath(Environment.getExternalStorageDirectory()+ "/sdcard/test.jpg");
                } else {
                    paramsToShare.setText(null);
                    paramsToShare.setTitle(null);
                    paramsToShare.setTitleUrl(null);
                    paramsToShare.setImagePath(Environment.getExternalStorageDirectory()+ "/screenshot.png");
//                    paramsToShare.setImagePath(Environment.getExternalStorageDirectory()+ "/sdcard/test.jpg");
                }

            }
        });
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment(null);
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite("happyfarm");
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
//        oks.setTitleUrl("http://sharesdk.cn");

        // 启动分享GUI
        oks.show(this);
    }

    //TODO 分享弹框
    private void photoDialog(Bitmap bitmap) {
        Dialog bottomDialog = new Dialog(this, R.style.dialog);
        View contentView = LayoutInflater.from(this).inflate(R.layout.layout_photo_dialog, null);
        bottomDialog.setContentView(contentView);
        ImageView imageView = contentView.findViewById(R.id.photo_image);
        //添加logo到右下角
        Bitmap watermarkBitmap = ImageUtils.createWaterMaskRightBottom(bitmap, BitmapFactory.decodeResource(getResources(),R.mipmap.logo_watermark), 10, 10);
        try {
            // 获取内置SD卡路径
            String sdCardPath = Environment.getExternalStorageDirectory().getPath();
            // 图片文件路径
            String filePath = sdCardPath + File.separator + "screenshot.png";
            File file = new File(filePath);
            FileOutputStream os = new FileOutputStream(file);
            watermarkBitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.flush();
            os.close();
//                    Log.d("a7888", "存储完成");
        } catch (Exception e) {
//                    Log.d("a7888", String.valueOf(e));
        }
        imageView.setImageBitmap(watermarkBitmap); //设置Bitmap
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(this, 100f);
//        params.bottomMargin = DensityUtil.dp2px(this, 220f);
        contentView.findViewById(R.id.photo_sure_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showShare();
                bottomDialog.cancel();
            }
        });
        contentView.findViewById(R.id.photo_cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.CENTER);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    public void saveStates(String states) {
        SharedPreferences.Editor editor = getSharedPreferences("User_data", MODE_PRIVATE).edit();
        editor.putString("states", states);
        editor.apply();
    }

    //TODO 好友申请弹窗
    private void showSetDeBugDialog(String msgContext) {
        final AlertUtilOneButton diyDialog = new AlertUtilOneButton(this);
        diyDialog.setOk("确定")
                .setContent(msgContext)
                .setDialogClickListener(new AlertUtilOneButton.DialogClickListener() {
                    @Override
                    public void ok() {
                        diyDialog.cancle();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    //TODO WebSocket通信
    private void init() {
        uri = URI.create(url);
//        Log.e("url", String.valueOf(uri));
        new Thread(new Runnable() {
            @Override
            public void run() {
                mSocketClient = new WebSocketClient(uri, new Draft_6455()) {
                    @Override
                    public void onOpen(ServerHandshake handshakedata) {
//                        Log.d("picher_log", "打开通道" + handshakedata.getHttpStatus());
//                        handler.obtainMessage(0, handshakedata).sendToTarget();
                        Message message = new Message();
                        message.what = 0;
                        message.obj = handshakedata;
                        handler.sendMessage(message);
                        String int_token = "inUserId_" + App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", "");
                        mSocketClient.send(int_token);
                    }

                    @Override
                    public void onMessage(String message) {
//                        Log.d("picher_log", "接收消息" + message);
//                        Log.d("picher_log", "排队人数" + num1);
//                        handler.obtainMessage(0, message).sendToTarget();

                        Message message1 = new Message();
                        message1.what = 0;
                        message1.obj = message;
                        handler.sendMessage(message1);
                    }

                    @Override
                    public void onClose(int code, String reason, boolean remote) {
//                        Log.d("picher_log", "通道关闭");
                        String outQueue_token = "outQueue_" + App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", "");
                        mSocketClient.send(outQueue_token);
//                        handler.obtainMessage(0, reason).sendToTarget();

                        Message message = new Message();
                        message.what = 0;
                        message.obj = reason;
                        handler.sendMessage(message);

                    }

                    @Override
                    public void onError(Exception ex) {
                        Log.d("picher_log", "链接错误");
                    }
                };
                mSocketClient.connect();
            }
        }).start();
    }

    //用户停止视频
    public void stopVideo() {
        HttpHelper.initHttpHelper().stopVideo(Integer.parseInt(landNo)).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
//                Log.e("cccccc", JSON.toJSONString(response.body()));
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }
    Runnable splashhandler = new Runnable(){
        public void run() {
            recLen--;
            if (recLen != 0){
                handlerVideo.postDelayed(this, 1000);
            }
//            Log.i("倒计时数字打印：",String.valueOf(recLen));
            if (recLen == 0) {
                mVideoView.pause();//暂停播放
//            mVideoView.stopPlayback();//释放资源
                stopVideo();
                if (mSocketClient != null) {
                    mSocketClient.close();
                }
                if (("0").equals(state) && superUser == 1) {
                    loadingView.setVisibility(View.VISIBLE);
                    mVideoView.start();//开始播放

                    mVideoView2.start();
                    // 打开重试次数，设置后若打开流地址失败，则会进行重试
                    options.setInteger(AVOptions.KEY_OPEN_RETRY_TIMES, 5);
                } else if (("0").equals(state)){
                    /***   购买完租地返回自己租地会崩  **********/
                    HttpHelper.initHttpHelper().getVideoSize2(Integer.parseInt(landNo)).enqueue(new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {
                            Log.e("picher_log11", JSON.toJSONString(response.body()));
                        }
                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            Log.e("picher_log", t.getMessage());
                        }
                    });
                    /***   END **********/
                    myLandWatch(Config.myLandWatch);
                }else if (("2").equals(state)) {
                    myLandWatch(Config.otherLandWatch);
                } else {
                    myLandWatch(Config.otherLandWatch);
                }
//                Toast.makeText(LandVideoActivity.this, "测试一下", Toast.LENGTH_SHORT).show();
            }
        }
    };

    //TODO 开启视频放大
    private void startAmplification(){
        amplification.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
////                mVideoView.stopPlayback();//释放资源
//                mVideoView.pause();//暂停播放
//                Intent intent = new Intent(LandVideoActivity.this, FullScreenActivity.class);
//                intent.putExtra("recLen",""+recLen);
//                intent.putExtra("videoPath",videoPath);
//                intent.putExtra("landNo",landNo);
//                Log.i(TAG, "onClick: "+resultLong);
//                startActivity(intent);
                videoViewFullScreen(isFullScreen);
            }
        });
    }
    //关闭视频放大
    private void stopAmplification(String videoStr){
        amplification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToastShort(LandVideoActivity.this, videoStr);
            }
        });
    }

    /**
     * 视频播放器全屏切换显示
     * @param isFullScreen true 全屏 反之
     */
    private void videoViewFullScreen(boolean isFullScreen) {
            if(isFullScreen) {
                this.isFullScreen = false;
                setSystemUIVisible(false);
                hideSmallScreen(true);
                //mVideoView.setDisplayOrientation(270);//支持播放画面以 0度，90度，180度，270度旋转
                //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//横屏模式
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);//横屏模式，支持正反切换
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                plview.setLayoutParams(params);
                fullScreenLayout.setVisibility(View.VISIBLE);
                setAudioTipsLayout(getResources().getString(R.string.video_big_tips), 10000);
                if (!isMyLandLayout) {
                    top_layout.setVisibility(View.GONE);
                    aa.setVisibility(View.GONE);
                    relativeLayout6.setVisibility(View.GONE);
                    aaa.setVisibility(View.GONE);
                }
                return;
            }
            this.isFullScreen = true;
            setSystemUIVisible(true);
            hideSmallScreen(false);
            //mVideoView.setDisplayOrientation(0);//支持播放画面以 0度，90度，180度，270度旋转
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏模式
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 3.5f);
            plview.setLayoutParams(params);
            fullScreenLayout.setVisibility(View.GONE);
    }

    /**
     * 屏幕切换时触发效果之显示/隐藏布局
     * @param isFullScreen true 全屏 反之
     */
    private void hideSmallScreen(boolean isFullScreen) {

        if (isFullScreen) {
            titleLayout.setVisibility(View.GONE);
            screenshots.setVisibility(View.GONE);
            audioButton.setVisibility(View.GONE);
            amplification.setVisibility(View.GONE);
            determine.setVisibility(View.GONE);
            return;
        }
        titleLayout.setVisibility(View.VISIBLE);
        screenshots.setVisibility(View.VISIBLE);
        audioButton.setVisibility(View.VISIBLE);
        amplification.setVisibility(View.VISIBLE);
        determine.setVisibility(View.VISIBLE);
        if (!isMyLandLayout) {
            screenshots.setVisibility(View.GONE);
            amplification.setVisibility(View.VISIBLE);
            determine.setVisibility(View.GONE);
        }
    }


    /**
     * 获取token是否失效
     */
    public void userInfoToken() {
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("success").equals(response.body().getFlag())) {
                    login.setVisibility(View.GONE);
                    video_text_view.setVisibility(View.GONE);
                    videoViewLayout.setVisibility(View.GONE);
                    mVideoView.setVisibility(View.VISIBLE);
//                    Log.i("info", "landNo.length() = "+landNo.length());
                    switch (landNo.length()){
                        case 1:
                            videoPath = VideoConfig.VideoUrl+"000"+landNo;
                            break;
                        case 2:
                            videoPath = VideoConfig.VideoUrl+"00"+landNo;
                            break;
                        case 3:
                            videoPath = VideoConfig.VideoUrl+"0"+landNo;
                            break;
                        case 4:
                            videoPath = VideoConfig.VideoUrl+landNo;
                            break;
                            default:
                                videoPath = "rtmp://live.hkstv.hk.lxdns.com/live/hks";//凤凰卫视
                    }
//                    Log.i("info", "landNo.videoPath = "+videoPath);
                    mVideoView.setVideoPath(videoPath);
                    mVideoView.setDisplayAspectRatio(PLVideoView.ASPECT_RATIO_PAVED_PARENT);

                    Queryfriends(userId);
                } else {
                    login.setVisibility(View.VISIBLE);
                    video_text_view.setVisibility(View.VISIBLE);
                    videoViewLayout.setVisibility(View.VISIBLE);
//                    videoBgViewLayout.setVisibility(View.VISIBLE);
                    login.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            videoFlag = true;
                            Intent intent = new Intent(LandVideoActivity.this, LoginActivity.class);
                            intent.putExtra("isLandVideo","isLandVideo");
                            startActivityForResult(intent,1009);
                        }
                    });
                    determine.setVisibility(View.GONE);
                    amplification.setVisibility(View.GONE);
                    screenshots.setVisibility(View.GONE);
                    audioButton.setVisibility(View.GONE);
                    handlerVideo.removeCallbacks(splashhandler);
                    mVideoView.setVisibility(View.GONE);
                    mVideoView.stopPlayback();//释放资源
                    mVideoView2.stopPlayback();
                    noLoginDialog();
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1:
                if (requestCode == 1009){
                    if(resultCode==RESULT_OK){

                    }
                }
        }
    }

    /**
     * 是否需要排队弹框
     * @param titleText
     * @param contentText
     */
    private void showUserInfoDialog(String titleText, String contentText) {
        final AlertUtilBest diyDialog =new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk(titleText)
                .setContent(contentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        init();
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    /**
     *查询该用户是否为地主好友
     */
    private void Queryfriends(String userId){
//        Log.e(TAG,userId);
        HttpHelper.initHttpHelper().seachFriend(Integer.valueOf(userId)).enqueue(new Callback<SeachFriend>() {
            @Override
            public void onResponse(Call<SeachFriend> call, Response<SeachFriend> response) {
//                Log.e(TAG,JSON.toJSONString(response.body()));
                if (response.body().getFlag().equals("success")) {
                    if (String.valueOf(response.body().getResult().getList()).equals("[]")) {
                        addBut.setVisibility(View.VISIBLE);
                        informationBut.setVisibility(View.GONE);
                    } else {
                        addBut.setVisibility(View.GONE);
                        informationBut.setVisibility(View.VISIBLE);
                        informationBut.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //TODO 判断当前是否有账号登录
                                if (!EMClient.getInstance().isLoggedInBefore() || "isOneLogin".equals(Config.isOneLogin)){
                                    Config.isOneLogin = "noOneLogin";
                                    loginEM(1);
                                }else {
                                    friendsDetails(Integer.parseInt(userId));
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<SeachFriend> call, Throwable t) {

            }
        });
    }
    //TODO 查询好友信息添加头像昵称
    public void friendsDetails(int userId) {
        HttpHelper.initHttpHelper().searchFriendDetail(userId).enqueue(new Callback<searchFriendDetail>() {
            @Override
            public void onResponse(Call<searchFriendDetail> call, Response<searchFriendDetail> response) {
                if (("success").equals(response.body().getFlag())) {
                    if (null != response.body().getResult().toString()) {
                        EaseUser u = new EaseUser(response.body().getResult().get(0).getUuid().toString());
                        u.setAvatar(response.body().getResult().get(0).getHeadImg());
                        u.setNickname(response.body().getResult().get(0).getNickname());
                        if (!Config.AllEMFriendInfo.containsKey("" + response.body().getResult().get(0).getId())) {
                            Config.AllEMFriendInfo.put("" + response.body().getResult().get(0).getId(), u);
                        }
                    }
                    //TODO 这里主要是根据用户uuid 存放自己的id 好填入自己的头像
                    EaseUser u = new EaseUser(getUuId());
                    if (!Config.AllEMFriendInfo.containsKey("" + getUserId())) {
                        Config.AllEMFriendInfo.put("" + getUserId(), u);
                    }
                    //TODO 进入聊天
                    startActivity(new Intent(LandVideoActivity.this, ChatActivity.class).putExtra(EaseConstant.EXTRA_USER_ID, String.valueOf(userId)));
                } else {
                    showToastShort(LandVideoActivity.this, response.body().getResult().toString());
                }
            }

            @Override
            public void onFailure(Call<searchFriendDetail> call, Throwable t) {
                showToastShort(LandVideoActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }
    /**
     * 获取该用户公开的成长相册
     */
    private void getAlbum(Boolean inversion,int pageNum){
        HttpHelper.initHttpHelper().searchPhotoAlbumPublic(Integer.valueOf(userId),pageNum,pageSize,"1").enqueue(new Callback<SearchPhotoAlbum>() {
            @Override
            public void onResponse(Call<SearchPhotoAlbum> call, Response<SearchPhotoAlbum> response) {
                Log.e("eeee1",JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())){
                    isLastPage = response.body().getResult().isIsLastPage();
                    if (response.body().getResult().getList().isEmpty()){
                        noPhotoGif.setVisibility(View.VISIBLE);
                        exhibition.setVisibility(View.GONE);
                    }else {
                        noPhotoGif.setVisibility(View.GONE);
                        exhibition.setVisibility(View.VISIBLE);
                        for (int i = 0;i < response.body().getResult().getList().size();i++){
                            MyAlbum myAlbum = new MyAlbum();
                            photoNum ++;
//                        if (String.valueOf(response.body().getResult().getList().get(i).getIsPublic()).equals("1")){
                            myAlbum.videoUrl = response.body().getResult().getList().get(i).getPhotoUrl();
                            String name =  response.body().getResult().getList().get(i).getAlbumName();
                            myAlbum.albumFirstPicture = response.body().getResult().getList().get(i).getAlbumFirstPicture();
                            if ("".equals(name)){
                                myAlbum.albumName = "无相册名";
                            }else {
                                myAlbum.albumName = name;
                            }
//                        myAlbum.num = String.valueOf(photoNum);
                            mdata.add(myAlbum);
//                        }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchPhotoAlbum> call, Throwable t) {
                noPhotoGif.setVisibility(View.VISIBLE);
                exhibition.setVisibility(View.GONE);
            }
        });
    }
    private void albumInit(){
        noPhotoGif = findViewById(R.id.no_photo_gif);
        exhibition = findViewById(R.id.exhibition);
        publicPhoto = findViewById(R.id.public_photo);
        adapter = new LandVideoExhibitionAdapter(LandVideoActivity.this,mdata);
        exhibition.setAdapter(adapter);
        gridLayoutManager = new GridLayoutManager(LandVideoActivity.this,1);
        gridLayoutManager.setReverseLayout(false);
//        int spanCount = 2;
//        int spacing = 50;
//        boolean includeEdge = true;
//        exhibition.addItemDecoration(new GridSpacingItemDecoration(spanCount,spacing,includeEdge));
        exhibition.setLayoutManager(gridLayoutManager);

        //给他人租地相册添加点击事件
        adapter.setItemClickListener(new CuringAdapter.MyItemClickListener(){
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(LandVideoActivity.this,OtherLandVideoPlayActivity.class);
                intent.putExtra("albumName",mdata.get(position).albumName);
                intent.putExtra("videoUrl",mdata.get(position).videoUrl);
                intent.putExtra("abstract",mdata.get(position).photoAbstract);
                intent.putExtra("albumFirstPicture",mdata.get(position).albumFirstPicture);
                startActivity(intent);
            }
        });
    }
    public void AutoLoading() {
        //上拉滑动自动请求数据
        exhibition.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                    if (lastVisiblePosition >= gridLayoutManager.getItemCount() - 1) {
                        if (isLastPage == true) {
//                            Toast.makeText(getApplication(), "没有更多数据", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            getAlbum(false,pageNum);
                        }
                    }
                }
            }
        });
    }

    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog =new AlertUtilBest(LandVideoActivity.this);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        videoFlag = true;
                        Intent intent = new Intent(LandVideoActivity.this, LoginActivity.class);
                        intent.putExtra("isLandVideo","isLandVideo");
                        startActivityForResult(intent,1009);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    public void loginEM(int num){
//        loginEM("15170029037","e10adc3949ba59abbe56e057f20f883e");
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (response.isSuccessful() && response.body().getFlag().equals("success")){
                    GetSelfInfo.ResultBean resultBean = response.body().getResult();
                    loginEM(String.valueOf(resultBean.getId()),resultBean.getPassword(),num);
                }
            }

            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }
    //login环信
    public void loginEM(String user, String pwd, int num){
        EMClient.getInstance().login(user,pwd,new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                Log.d("EMLogmain","登录环信成功");
                //TODO 以下两个方法是为了保证进入主页面后本地会话和群组都 load 完毕。
                EMClient.getInstance().chatManager().loadAllConversations();
                EMClient.getInstance().groupManager().loadAllGroups();
                // 获取华为 HMS 推送 token
//                HMSPushHelper.getInstance().connectHMS(LandVideoActivity.this);
//                HMSPushHelper.getInstance().getHMSPushToken();
                switch (num){
                    case 1:
                        //聊天
                        friendsDetails(Integer.parseInt(userId));
                        break;
                    case 2:
                        //客服
                        customerAllot();
                        break;
                }
            }

            @Override
            public void onProgress(int progress, String status) {}

            @Override
            public void onError(int code, String message) {
//                Log.d("EMLogmain", "登录聊天服务器失败！"+code+"信息："+message);
                Toast.makeText(LandVideoActivity.this,"登录聊天服务器失败！",Toast.LENGTH_SHORT).show();
            }
        });
    }

    //TODO 获得用户头像
    private String getHeadImg(){
        return getSharedPreferences("User_data", MODE_PRIVATE).getString("userImg", "");
    }

    //TODO 获得用户uuid
    private String getUuId() {
        return getSharedPreferences("User_data", MODE_PRIVATE).getString("uuid", "");
    }

    private String getUserToken(){
        return App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("token","");
    }

    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }
    //获取是否允许流量播放
    private String getTraffic(){
        return getSharedPreferences("data",MODE_PRIVATE).getString("traffic","");
    }

    /**
     * 复位摄像头朝向
     */
    private void resetButton(){
        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraController(10,0,-1);
                //seekBar.setProgress(0);
            }
        });
    }

    /**
     * 设置视角settingBtn
     */
    private void settingButton(){
        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraController(11,0,-1);
            }
        });
    }

    /**
     * 滑动条加
     */
    private void addSeekBar(){
        seekBarAdd.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        seekBarAdd.setImageResource(R.mipmap.add_selected);
                        cameraController(4,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        seekBarAdd.setImageResource(R.mipmap.icon_web_42);
                        cameraController(4,1,Config.previousCommand);
                        Config.previousCommand = 4;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }

    /**
     * 滑动条减
     */
    private void deleteSeekBar(){
        seekBarDelete.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        seekBarDelete.setImageResource(R.mipmap.delete_selected);
                        cameraController(5,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        seekBarDelete.setImageResource(R.mipmap.icon_web_43);
                        cameraController(5,1,Config.previousCommand);
                        Config.previousCommand = 5;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }

    /**
     * 操作摄像头
     * @param dwPTZCommand
     * @param dwStop
     * @param previousCommand
     */
    private void cameraController(int dwPTZCommand,int dwStop,int previousCommand){
        long clientTime = System.currentTimeMillis(); //客户端点击按钮时间
        int clientType = 2; //客户端类型(1 Web,2 Android,3 IOS)
        int networkType = 0; //网络类型(1 4G,2 wifi)
        if (NetWorkUtils.isWifiConnected(this)) {
            networkType = 2;
        }
        HttpHelper.initHttpHelper()
                .cameraController(Integer.parseInt(landNo),dwPTZCommand,dwStop,previousCommand,clientTime,clientType,networkType,Config.devNo)
                .enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                String rescult = "";
                try{
                    rescult = response.body().getFlag();
                }catch (Exception e){
                    showToastShort(LandVideoActivity.this, "请求错误");
                    return;
                }
                if ("success".equals(rescult)) {
//                    Log.i(TAG, "onResponse: "+"摄像头操作成功");
                } else {
//                    Log.e(TAG, "onResponse: "+"摄像头操作失败");
                    showToastShort(LandVideoActivity.this, response.body().getResult().toString());
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
//                Log.e(TAG, "onFailure: "+"摄像头操作异常"+t);
                showToastShort(LandVideoActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }

    /**
     * 直播声音开关
     */
    private void fullScreenAudioSwitch(){
        fullScreenAudioButton.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (Config.audioIsPlay) {
                    //播放
                    Config.audioIsPlay = false;
                    audioButton.setImageResource(R.drawable.audio_close);
                    fullScreenAudioButton.setImageResource(R.drawable.audio_close);
                    //mVideoView2.pause();
                    LandVideoActivity.mVideoView2.setVolume(0,0);
                    setAudioTipsLayout("声音已关闭", 5000);
                } else {
                    //未播放
                    Config.audioIsPlay = true;
                    audioButton.setImageResource(R.drawable.audio_open);
                    fullScreenAudioButton.setImageResource(R.drawable.audio_open);
                    //mVideoView2.start();
                    LandVideoActivity.mVideoView2.setVolume(10,10);
                    setAudioTipsLayout("声音已开启", 5000);
                }
            }
        });
    }

    /**
     * 大屏状态返回按钮点击事件
     */
    private void fullScreenBackClick(){
        fullScreenBack.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                videoViewFullScreen(false);
            }
        });
    }

    /**
     * 摄像头向上操作
     */
    private void upperButton(){
        upper.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        upper.setBackgroundResource(R.drawable.icon_upper_selected);
                        cameraController(0,0, Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        upper.setBackgroundResource(R.drawable.icon_upper);
                        cameraController(0,1,Config.previousCommand);
                        Config.previousCommand = 0;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }
    /**
     * 摄像头向下操作
     */
    private void lowerButton(){
        lower.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        lower.setBackgroundResource(R.drawable.icon_lower_selected);
                        cameraController(1,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        lower.setBackgroundResource(R.drawable.icon_lower);
                        cameraController(1,1,Config.previousCommand);
                        Config.previousCommand = 1;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }
    /**
     * 摄像头向左操作
     */
    private void leftButton(){
        left.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        left.setBackgroundResource(R.drawable.icon_left_selected);
                        cameraController(2,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        left.setBackgroundResource(R.drawable.icon_left);
                        cameraController(2,1,Config.previousCommand);
                        Config.previousCommand = 2;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }
    /**
     * 摄像头向右操作
     */
    private void rightButton(){
        right.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        System.out.println("++++++ACTION_DOWN");//点击
                        right.setBackgroundResource(R.drawable.icon_right_selected);
                        cameraController(3,0,Config.previousCommand);
                        break;
                    case MotionEvent.ACTION_UP:
                        System.out.println("++++++ACTION_UP");//抬起
                        right.setBackgroundResource(R.drawable.icon_right);
                        cameraController(3,1,Config.previousCommand);
                        Config.previousCommand = 3;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        System.out.println("++++++ACTION_CANCEL");
                        break;
                }
                return true;
            }
        });
    }
    /**
     * APP是否处于前台唤醒状态
     *
     * @return
     */
    public boolean isAppOnForeground() {
        ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = getApplicationContext().getPackageName();
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null)
            return false;

        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            // The name of the process that this object is associated with.
            if (appProcess.processName.equals(packageName)
                    && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }

        return false;
    }

    /**
     * 判断当前系统时间是否在特定时间的段内
     *
     * @param beginHour 开始的小时，例如5
     * @param beginMin 开始小时的分钟数，例如00
     * @param endHour 结束小时，例如 8
     * @param endMin 结束小时的分钟数，例如00
     * @return true表示在范围内，否则false
     */
    public static boolean isCurrentInTimeScope(int beginHour, int beginMin, int endHour, int endMin) {
        boolean result = false;// 结果
        final long aDayInMillis = 1000 * 60 * 60 * 24;// 一天的全部毫秒数
        final long currentTimeMillis = System.currentTimeMillis();// 当前时间

        Time now = new Time();// 注意这里导入的时候选择android.text.format.Time类,而不是java.sql.Time类
        now.set(currentTimeMillis);

        Time startTime = new Time();
        startTime.set(currentTimeMillis);
        startTime.hour = beginHour;
        startTime.minute = beginMin;

        Time endTime = new Time();
        endTime.set(currentTimeMillis);
        endTime.hour = endHour;
        endTime.minute = endMin;

        if (!startTime.before(endTime)) {
            // 跨天的特殊情况（比如22:00-8:00）
            startTime.set(startTime.toMillis(true) - aDayInMillis);
            result = !now.before(startTime) && !now.after(endTime); // startTime <= now <= endTime
            Time startTimeInThisDay = new Time();
            startTimeInThisDay.set(startTime.toMillis(true) + aDayInMillis);
            if (!now.before(startTimeInThisDay)) {
                result = true;
            }
        } else {
            // 普通情况(比如 8:00 - 14:00)
            result = !now.before(startTime) && !now.after(endTime); // startTime <= now <= endTime
        }
        return result;
    }
}