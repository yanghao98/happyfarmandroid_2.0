package zhixin.cn.com.happyfarm_user.Page;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.TutorialAdapter;
import zhixin.cn.com.happyfarm_user.BreedingTutorialPageActivity;
import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.TutorialDetailsActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.SearchTutorial;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.model.Tutorial;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static com.mob.tools.utils.DeviceHelper.getApplication;

/**
 * Created by Administrator on 2018/3/21.
 */

public class TutorialPage extends Fragment {
    private ListView listView;
    private RefreshLayout refreshLayout;
    private ArrayList<Tutorial> tutorials = new ArrayList<>();
    private TutorialAdapter adapter;

    private Boolean isLastPage = false;
    private ArrayList<String> tutorialName = new ArrayList<>();
    private ArrayList<Integer> tutorialId = new ArrayList<>();
    private Tutorial tutorial;
    private RelativeLayout tutorialListPrompt;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tutorial_layout, null,false);
        refreshLayout = (RefreshLayout) view.findViewById(R.id.refreshLayout);
        tutorialListPrompt = view.findViewById(R.id.tutorial_list_prompt);
        tutorialListPrompt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), BreedingTutorialPageActivity.class);
                startActivity(intent);
            }
        });
        refreshLayout.setEnableHeaderTranslationContent(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()).setShowBezierWave(false));
        // 设置 Footer 为 球脉冲
        refreshLayout.setRefreshFooter(new ClassicsFooter(getContext()).setSpinnerStyle(SpinnerStyle.Scale));
        if (!tutorials.isEmpty()){
            tutorials.clear();
        }
        StaggerLoadData();
        initRecyclerView(view);
        topRefreshLayout();
        bottomRefreshLayout();
        userInfoTokenTest();
        return view;

    }
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        landType = ((LandVideoActivity)activity).getLandType();
//    }
    private void initRecyclerView(View view) {
        listView = view.findViewById(R.id.tutorial_list);

        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new TutorialAdapter(getContext(), tutorials);
        //设置适配器
        listView.setAdapter(adapter);
        //ListView item的点击事件
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getContext(), TutorialDetailsActivity.class);
//                Log.e("tutorialId", response.body().getResult().getList().get(i).getId() + "");
//                        intent.putExtra("tutorialId",response.body().getResult().getList().get(i).getId());
                intent.putExtra("tutorialName", tutorialName.get(i) + "");
                intent.putExtra("tutorialId", tutorialId.get(i) + "");
                startActivity(intent);
            }
        });

    }

    private void StaggerLoadData() {

        Call<SearchTutorial> searchTutorial = HttpHelper.initHttpHelper().LandVideoSearchTutorial();
        searchTutorial.enqueue(new Callback<SearchTutorial>() {
            @Override
            public void onResponse(Call<SearchTutorial> call, Response<SearchTutorial> response) {
                if (("success").equals(response.body().getFlag())) {
                    if (response.body().getResult().getList().isEmpty()){
                        tutorialListPrompt.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        isLastPage = true;
                    }else {
                        tutorialListPrompt.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        isLastPage = response.body().getResult().isIsLastPage();
                        Log.e("data", JSON.toJSONString(response.body()));
                        for (int i = 0; i <= response.body().getResult().getList().size() - 1; i++) {
                            tutorial = new Tutorial(response.body().getResult().getList().get(i).getTutorialName(),response.body().getResult().getList().get(i).getCropImg());
                            tutorialId.add(response.body().getResult().getList().get(i).getId());
                            tutorialName.add(response.body().getResult().getList().get(i).getTutorialName());
                            tutorials.add(tutorial);
                        }
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    tutorialListPrompt.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchTutorial> call, Throwable t) {
                tutorialListPrompt.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!tutorials.isEmpty()){
                    tutorials.clear();
                }
                isLastPage = false;
                refreshlayout.setNoMoreData(false);//TODO 恢复没有更多数据的原始状态

                StaggerLoadData();
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }

    //上拉加载
    public void bottomRefreshLayout() {
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (isLastPage) {
                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
                        } else {
//                            Toast.makeText(getApplication(), "数据加载完毕", Toast.LENGTH_SHORT).show();
                            StaggerLoadData();
//                            adapter.loadMore(initData());
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }

    /**
     * 验证token是否有效
     */
    private   void userInfoTokenTest(){
         HttpHelper.initHttpHelper().test().enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                System.out.println("###"+ JSON.toJSONString(response.body()));
                if (("failed").equals(response.body().getFlag())) {
                    tutorialListPrompt.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                } else if (("success").equals(response.body().getFlag())){
                    tutorialListPrompt.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }

}
