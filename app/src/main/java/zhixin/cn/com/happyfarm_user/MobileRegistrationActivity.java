package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.ISRegister;
import zhixin.cn.com.happyfarm_user.other.TextChangedListener;
import zhixin.cn.com.happyfarm_user.other.Validator;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by DELL on 2018/3/5.
 */

public class MobileRegistrationActivity extends ABaseActivity {
    private Dialog mDialog;
    String phone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile_registration_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("手机注册");

        TextView textView = findViewById(R.id.mobile_registration_textView);

        textView .setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

        EditText editText = findViewById(R.id.mobil_registration_edit);

        findViewById(R.id.mobil_registration_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phone = editText.getText().toString();
                if (phone.equals("")){
                    Toast.makeText(MobileRegistrationActivity.this, "请输入手机号",Toast.LENGTH_SHORT).show();
                }else {
                    if (Validator.isChinaPhoneLegal(phone)){
                        mDialog = LoadingDialog.createLoadingDialog(MobileRegistrationActivity.this,null);
                        Call<ISRegister> isRegister = HttpHelper.initHttpHelper().isRegister(phone);
                        isRegister.enqueue(new Callback<ISRegister>() {
                            @Override
                            public void onResponse(Call<ISRegister> call, Response<ISRegister> response) {
//                                Log.e("12331312321", JSON.toJSONString(response.body()));
                                if (response.body().getFlag().equals("success")){
                                    LoadingDialog.closeDialog(mDialog);
                                    Intent intent = new Intent(MobileRegistrationActivity.this,RegisterActivity.class);
                                    intent.putExtra("tel",phone);
                                    startActivity(intent);
                                    finish();
                                }else {
                                    LoadingDialog.closeDialog(mDialog);
                                    Toast.makeText(MobileRegistrationActivity.this, response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<ISRegister> call, Throwable t) {
                                LoadingDialog.closeDialog(mDialog);
//                                Log.e("erro",call+","+t);
                            }
                        });
                    }else {
                        Toast.makeText(MobileRegistrationActivity.this,"请输入正确的手机号",Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        //点击空白处退出键盘
        findViewById(R.id.mobilRegBackView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
        //限制手机号长度
        TextChangedListener textChangedListener = new TextChangedListener(11,editText,MobileRegistrationActivity.this, TextString.PhoneLong);
        editText.addTextChangedListener(textChangedListener.getmTextWatcher());
    }

    public void back(View view){
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm =  (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }


}
