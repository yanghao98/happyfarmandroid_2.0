package zhixin.cn.com.happyfarm_user.Page;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hyphenate.chat.EMClient;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.ViewPagerAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.VipPageAdapter;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.PersonalMessageActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.ShoppingCartActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.ShoppingCart;
import zhixin.cn.com.happyfarm_user.model.ShoppingCartNum;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.model.VipSignIn;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.MODE_PRIVATE;


public class VipPage extends Fragment {
    private RelativeLayout titleBarLayout;
    private ViewPager viewPager;
    private MagicIndicator vipIndicator;
    private List<String> titles;
    private List<Fragment> frags;
    private Activity activity;
    private View myView;
    private Badge messageBadge;
    private int aaa = 1;
    private Badge messageBadgeShoppingCart;
    private int messageBadgeNum = 0;
    private ImageView shoppingCart;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        searchUserInfo();
        View view = inflater.inflate(R.layout.vip_page_activity,null,false);
        myView = view;
        TextView title = view.findViewById(R.id.main_page_title_name);
        title.setText("专享");
        aaa = 2;
        initData();
        initPage(view);
        setStatusBarHeight();
        initVipTab();
        searchShoppingCartNum();
        Log.i("打印消息数量3", "数量" + aaa);
        return view;
    }


    /**
     * 初始化页面布局
     * @param view
     */
    private void initPage(View view) {

        titleBarLayout = view.findViewById(R.id.title_bar_layout);
        viewPager = view.findViewById(R.id.vip_tabLayout_content);
        vipIndicator = view.findViewById(R.id.vip_tabLayout);
        VipPageAdapter adapter = new VipPageAdapter(getChildFragmentManager(), frags);
        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(adapter);
        ImageView imageView = view.findViewById(R.id.main_page_message_icon);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PersonalMessageActivity.class);
                startActivity(intent);

            }
        });
        shoppingCart = view.findViewById(R.id.shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
                    @Override
                    public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                        if ("success".equals(response.body().getFlag())) {
                            startActivity(new Intent(getContext(), ShoppingCartActivity.class));
                        } else {
                            noLoginDialog();
                        }
                    }
                    @Override
                    public void onFailure(Call<GetSelfInfo> call, Throwable t) {
                        noLoginDialog();
                    }
                });
            }
        });
        messageBadge = new QBadgeView(getContext())
                .bindTarget(myView.findViewById(R.id.main_page_icon_view_total))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
        setMsgNum(EMClient.getInstance().chatManager().getUnreadMessageCount());

        messageBadgeShoppingCart = new QBadgeView(getContext())
                .bindTarget(myView.findViewById(R.id.shopping_cart_icon))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
    }

    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(getActivity());
        //相对布局使用方法
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        titleBarLayout.setLayoutParams(params);
    }
    /**
     * 获取状态栏高度
     * @param context 上下文
     * @return int 状态栏高度值
     */
    protected static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    /**
     * 加载tabLayout的菜单名和对应的页面
     */
    private void initData(){
        frags = new ArrayList<>();
        frags.add(new ActivityPage());
        frags.add(new AllWelfarePage());
        frags.add(new AlbumsPage());
        frags.add(new CustomerServicePage());

        titles = new ArrayList<>();
        titles.add("活动");
        titles.add("福利");
        titles.add("相册");
        titles.add("客服");
    }

    /**
     * 加载tabLayout的方法
     */
    private void initVipTab() {
        CommonNavigator navigator = new CommonNavigator(getContext());
        navigator.setAdjustMode(true);//设置平分屏幕宽度
        navigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return frags.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context,final int i) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(titles.get(i));
                titleView.setTextSize(16);
                TextPaint textPaint = titleView.getPaint();
                textPaint.setFakeBoldText(true);
                titleView.setNormalColor(Color.parseColor("#9a9a9a"));//未选中颜色
                titleView.setSelectedColor(Color.parseColor("#1DAC6D"));//选中颜色
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewPager.setCurrentItem(i);
                    }
                });
                return titleView;
            }
            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);//设置下滑线  自适应文字长度MODE_WRAP_CONTENT MODE_MATCH_EDGE
                linePagerIndicator.setColors(Color.parseColor("#1DAC6D"));
                return linePagerIndicator;
            }
        });
        vipIndicator.setNavigator(navigator);
        /**设置中间竖线**/
        //绑定viewpager滚动事件
        ViewPagerHelper.bind(vipIndicator, viewPager);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    /**
     * 获取购物车数量
     */
    public void searchShoppingCartNum() {
        HttpHelper.initHttpHelper().searchShoppingCartNum().enqueue(new Callback<ShoppingCartNum>() {
            @Override
            public void onResponse(Call<ShoppingCartNum> call, Response<ShoppingCartNum> response) {
                System.out.println("购物车数量"+JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    messageBadgeShoppingCart.setBadgeNumber(response.body().getResult());
                }
            }
            @Override
            public void onFailure(Call<ShoppingCartNum> call, Throwable t) {

            }
        });
    }

    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(activity);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }

                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    /**
     * 验证token是否有效
     */
    public  void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                System.out.println("###"+ JSON.toJSONString(response.body()));
                if (("failed").equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(activity, "User_data", strArr);
                    if ("用户信息失效".equals(response.body().getResult())){
//                        myView.findViewById(R.id.vip_tabLayout_content).setVisibility(View.GONE);
                    }
                    myView.findViewById(R.id.main_page_message_icon).setVisibility(View.GONE);
                } else if (("success").equals(response.body().getFlag())){
//                        myView.findViewById(R.id.vip_tabLayout_content).setVisibility(View.VISIBLE);
                    myView.findViewById(R.id.main_page_message_icon).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void searchUserInfo() {
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if ("success".equals(response.body().getFlag())) {
                    Config.invitationCode = response.body().getResult().getInvitationCode();
                }
            }
            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }


    /**
     * 设置消息显示数量
     * @param num 消息数量
     * position 设置显示tablayout的item 暂时未时候，考虑后期可能所有的item都需要
     */
    private void setMsgNum( int num){
        if (messageBadge != null){
//            Log.i(TAG, "消息设置成功setMsgNum: "+num);
            messageBadge.setBadgeNumber(num);
        }else {
//            Log.i(TAG, "消息设置失败setMsgNum: "+num);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        userInfoTokenTest();
        int total = EMClient.getInstance().chatManager().getUnreadMessageCount();
        setMsgNum(total);
    }

    public void getEMNumber(int number){
        setMsgNum(number);
        System.out.println("我看看这个值是否会改变" + aaa);
    }
}
