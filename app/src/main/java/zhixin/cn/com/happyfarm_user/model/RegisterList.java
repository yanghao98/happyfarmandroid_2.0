package zhixin.cn.com.happyfarm_user.model;

import android.content.Intent;

/**
 * Created by DELL on 2018/2/7.
 */

public class RegisterList {


    /**
     * result : 注册成功
     * flag : success
     */

    private String result;
    private Integer errCode;
    private String flag;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }
}
