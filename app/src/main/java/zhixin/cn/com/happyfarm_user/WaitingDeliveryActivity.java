package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.CropAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.WaitingDeliveryAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.searchOrderlogList;

public class WaitingDeliveryActivity extends AppCompatActivity {

    private RefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private WaitingDeliveryAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private RelativeLayout orderBuyPrompt;
    private List<searchOrderlogList.ResultBean.ListBean> list = new ArrayList<>();
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waiting_delivery_activity);
        initView();
    }
    private void initView() {
        pageNum = 1;
        pageSize = 15;
        TextView title = findViewById(R.id.title_name);
        title.setText("待发货订单");
        recyclerView = findViewById(R.id.waiting_delivery_recycle);
        orderBuyPrompt = findViewById(R.id.waiting_delivery_order_buy_prompt);
        refreshLayout = (RefreshLayout) findViewById(R.id.waiting_delivery_refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(WaitingDeliveryActivity.this).setShowBezierWave(false));
        //设置 Footer 为 球脉冲 样式
        refreshLayout.setRefreshFooter(new ClassicsFooter(WaitingDeliveryActivity.this).setSpinnerStyle(SpinnerStyle.Scale));
    }
    /**
     * 初始化适配器
     */
    private void initRecyclerView() {
        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new WaitingDeliveryAdapter(WaitingDeliveryActivity.this, list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(WaitingDeliveryActivity.this, 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
//        adapter.setItemClickListener(new WaitingDeliveryAdapter.MyItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
////                Log.i("我选择了第几个", "onItemClick: " + JSON.toJSONString(list.get(position)));
//                Toast.makeText(WaitingDeliveryActivity.this,"提醒成功！",Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!list.isEmpty()) {
            list.clear();
        }
        searchOrderlogListByState(1);
    }

    private void searchOrderlogListByState(int pageNum) {
//        6ff4404d-9379-4da6-b483-8072cb38bbf7
        dialog = LoadingDialog.createLoadingDialog(WaitingDeliveryActivity.this, "正在加载数据");
        HttpHelper.initHttpHelper().searchOrderlogListByState(1,pageNum,pageSize).enqueue(new Callback<searchOrderlogList>() {
            @Override
            public void onResponse(Call<searchOrderlogList> call, Response<searchOrderlogList> response) {
                Log.i("SearchOrderlogList1", "onResponse: " + JSON.toJSONString(response.body().getResult().getList()));
                if (("success").equals(response.body().getFlag())) {
                    isLastPage = response.body().getResult().isIsLastPage();
//                    Log.i("成功了", "onResponse: " + isLastPage);
                    if (response.body().getResult().getList().isEmpty()){
                        orderBuyPrompt.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }else {
                        list = response.body().getResult().getList();
                        orderBuyPrompt.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    initRecyclerView();
                } else {
                    Log.i("失败了", "onResponse: ");
                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(Call<searchOrderlogList> call, Throwable t) {
                orderBuyPrompt.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                Log.i("异常", "onFailure: " + t);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }


    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!list.isEmpty()){
                    list.clear();
                }
                isLastPage = false;
                pageNum = 1;
                searchOrderlogListByState(1);
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }

    //上拉加载
    public void bottomRefreshLayout() {
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("info", "上拉刷新: "+isLastPage);
                        if (isLastPage) {
                            Toast.makeText(WaitingDeliveryActivity.this, "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
                        } else {
//                            Toast.makeText(getApplication(), "数据加载完毕", Toast.LENGTH_SHORT).show();
                            pageNum++;
                            searchOrderlogListByState(pageNum);
//                            adapter.loadMore(initData());
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }

    public void back(View view) {
        finish();
    }
}
