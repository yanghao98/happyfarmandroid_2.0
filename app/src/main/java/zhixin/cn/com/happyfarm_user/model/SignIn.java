package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * SignIn
 *
 * @author: Administrator.
 * @date: 2019/6/26
 */
public class SignIn {

    /**
     * result : 签到成功
     * flag : success
     * message : 0.5
     */

    private ResultBean result;
    private String flag;
    private int errCode;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? "" : flag;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }


    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public class ResultBean {
        private float eggs;
        private List<ListBean> giftList;
        private int signGift;//1：鸡窝搭建成功 2：小鸡孵化成功 3：领取签到饲料 4：领取成品鸡

        public float getEggs() {
            return eggs;
        }

        public void setEggs(float eggs) {
            this.eggs = eggs;
        }

        public List<ListBean> getGiftList() {
            return giftList;
        }

        public void setGiftList(List<ListBean> giftList) {
            this.giftList = giftList;
        }

        public int getSignGift() {
            return signGift;
        }

        public void setSignGift(int signGift) {
            this.signGift = signGift;
        }

        public class ListBean {
            private int id;
            private String gitfName;
            private int manageId;
            private int signingDay;
            private long updateTime;
            private long startTime;
            private boolean state;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getGitfName() {
                return gitfName;
            }

            public void setGitfName(String gitfName) {
                this.gitfName = gitfName;
            }

            public int getManageId() {
                return manageId;
            }

            public void setManageId(int manageId) {
                this.manageId = manageId;
            }

            public int getSigningDay() {
                return signingDay;
            }

            public void setSigningDay(int signingDay) {
                this.signingDay = signingDay;
            }

            public long getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(long updateTime) {
                this.updateTime = updateTime;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public boolean isState() {
                return state;
            }

            public void setState(boolean state) {
                this.state = state;
            }
        }
    }
}
