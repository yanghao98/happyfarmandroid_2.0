package zhixin.cn.com.happyfarm_user.db;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import zhixin.cn.com.happyfarm_user.other.FileByteUtil;

public class RetrofitUtil {

    public static MultipartBody filesToMultipartBody(List<File> files) {
        MultipartBody.Builder builder = new MultipartBody.Builder();

        for (File file : files) {
            String mimeType = FileByteUtil.getMimeType(file.getPath());
            RequestBody requestBody = RequestBody.create( MediaType.parse(mimeType), file);
            builder.addFormDataPart("file", file.getName(), requestBody);
        }
        builder.setType(MultipartBody.FORM);
        MultipartBody multipartBody = builder.build();
        return multipartBody;
    }


    public static List<MultipartBody.Part> filesToMultipartBodyParts(List<File> files) {
        List<MultipartBody.Part> parts = new ArrayList<>(files.size());
        for (File file : files) {
            String mimeType = FileByteUtil.getMimeType(file.getPath());
            RequestBody requestBody = RequestBody.create( MediaType.parse(mimeType), file);
            MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
            parts.add(part);
        }
        return parts;
    }

    public static  MultipartBody.Part  fileToMultipartBodyPart (File file) {
        //String type = FileUtil.fileType(file);
//             TODO: 16-4-2  这里为了简单起见，没有判断file的类型;
      RequestBody requestBody = RequestBody.create(MediaType.parse("text/x-markdown; charset=utf-8"), file);
//        String mimeType = MediaUtil.getMimeType(file.getPath());
//        RequestBody requestBody = RequestBody.create( MediaType.parse(mimeType), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        return part;
    }
}