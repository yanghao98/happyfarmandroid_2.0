package zhixin.cn.com.happyfarm_user.Page;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.ListDialogAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.EggManagementActivity;
import zhixin.cn.com.happyfarm_user.PaymentInformationActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.searchReceivingAddress;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.TextString;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * EggSendBackPage
 *
 * @author: Administrator.
 * @date: 2019/5/17
 */
public class EggSendBackPage extends Fragment {

    private ImageButton eggAddBtn;
    private ImageButton eggDelBtn;
    private TextView setNumber;
    private TextView username;
    private TextView tel;
    private Button eggAddressBt;
    private Button sendBackBt;
    private int number = 0;
    private int eggDiamond;
    private int plotId;
    private String plotName;
    private TextView confirmPlotName;
    private Dialog mDialog;
    private List<Integer> Idlist;
    private List<String> addressIdList;
    private List<Integer> plotIdList;
    private AlertDialog alertDialog;
    private Dialog dialog;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.egg_send_back_page_layout,null, false);
        initView(view);
        onClick();
        getSelfInfo();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        eggDiamond = ((EggManagementActivity) activity).getEggDiamond().intValue();
        this.activity = activity;
    }

    private void initView(View view) {
        eggAddBtn = view.findViewById(R.id.egg_confirm_harvest);
        eggDelBtn = view.findViewById(R.id.egg_harvest_reduce);
        setNumber = view.findViewById(R.id.egg_harvest_number);
        confirmPlotName = view.findViewById(R.id.egg_confirm_plotName);
        sendBackBt = view.findViewById(R.id.send_back_bt);
        username = view.findViewById(R.id.egg_username);
        tel = view.findViewById(R.id.egg_usertel);
        eggAddressBt = view.findViewById(R.id.egg_confirm_adress);
    }

    private void onClick() {
        eggAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (number > eggDiamond) {
                    Toast.makeText(getContext(), "余额不足", Toast.LENGTH_SHORT).show();
                    return;
                }
                number = number + 1;
                setNumber.setText(String.valueOf(number));
            }
        });

        eggDelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (number >= 1){
                    number = number - 1;
                    setNumber.setText(String.valueOf(number));
                }
            }
        });
        eggAddressBt.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                searchAddress();
            }
        });
        sendBackBt.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (number <= 0) {
                    Toast.makeText(getContext(), "未选择余额数量", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (plotId <= 0) {
                    Toast.makeText(getContext(), "未选择收货地址", Toast.LENGTH_SHORT).show();
                    return;
                }
                interceptDialog();
            }
        });
    }
    /**
     * 租地拦截弹框
     * 由于业务限制，需要租地时告诉用户目前只对南京开放
     */
    private void interceptDialog(){
        final AlertUtilBest diyDialog = new AlertUtilBest(activity);
        diyDialog.setCancel("取消")
                .setOk(getResources().getString(R.string.intercept_ok))
                .setContent(getResources().getString(R.string.intercept_egg_content))
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        addOrderLogEgg();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    private void addOrderLogEgg() {
        dialog = LoadingDialog.createLoadingDialog(getActivity(),"正在加载");
        Call<CheckSMS> deleteEgg = HttpHelper.initHttpHelper().addOrderLog(-1, (double)number, plotId);
        deleteEgg.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                LoadingDialog.closeDialog(dialog);
                if (null != response) {
                    if ("success".equals(response.body().getFlag())) {
                        eggDiamond -= number;
                        setNumber.setText(String.valueOf(0));
                        number = 0;
                        Toast.makeText(getContext(), response.body().getResult(), Toast.LENGTH_SHORT).show();
                        activity.finish();
                    } else {
                        Toast.makeText(getContext(), response.body().getResult(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                LoadingDialog.closeDialog(dialog);
                Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 获取用户信息
     */
    public void getSelfInfo(){
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (null != response.body()) {
                    if (("success").equals(response.body().getFlag())){
                        plotId = response.body().getResult().getPlotId();
                        username.setText(response.body().getResult().getNickname());
                        tel.setText(response.body().getResult().getTel());
                        plotName = response.body().getResult().getPlotName();
                        confirmPlotName.setText(plotName);
                    }
                } else {
                    Toast.makeText(getContext(), "系统错误", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }

            }

            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {
                Toast.makeText(getContext(), "系统错误", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        });
    }

    private void searchAddress() {
        mDialog = LoadingDialog.createLoadingDialog(getContext(),"正在加载地址...");
        HttpHelper.initHttpHelper().searchReceivingAddress().enqueue(new Callback<searchReceivingAddress>() {
            @Override
            public void onResponse(Call<searchReceivingAddress> call, Response<searchReceivingAddress> response) {
                if (("success").equals(response.body().getFlag())){
                    Idlist = new ArrayList<>();
                    addressIdList = new ArrayList<>();
                    plotIdList = new ArrayList<>();
                    for (int i = 0;i<response.body().getResult().size();i++){
                        Idlist.add(response.body().getResult().get(i).getId());
                        addressIdList.add(response.body().getResult().get(i).getAddress());
                        plotIdList.add(response.body().getResult().get(i).getPlotId());
                    }
                    LoadingDialog.closeDialog(mDialog);
                    ShowDialogs(addressIdList,Idlist,plotIdList,"地址");
                } else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(getContext(), getResources().getString(R.string.loading_failed),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<searchReceivingAddress> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * 地址弹框
     * @param list 地址名称数组
     * @param idlist 地址表id数组
     * @param plotIdList  收货id数组
     * @param titles 弹框标题
     */
    public void ShowDialogs(List<String> list,List<Integer> idlist,List<Integer> plotIdList,String titles){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_dialog, null);
        ListView myListView = layout.findViewById(R.id.formcustomspinner_list);
        ListDialogAdapter adapter = new ListDialogAdapter(getContext(), list);
        myListView.setAdapter(adapter);
        TextView title = layout.findViewById(R.id.label);
        title.setText(titles);

        layout.findViewById(R.id.list_dialog_bt).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mDialog = LoadingDialog.createLoadingDialog(getContext(),"稍后");
                HttpHelper.initHttpHelper().updateReceivingAddress(idlist.get(i),plotIdList.get(i),"1").enqueue(new Callback<CheckSMS>() {
                    @Override
                    public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                        Log.e("地址。。",JSON.toJSONString(response.body()));
                        if (("success").equals(response.body().getFlag())){
                            LoadingDialog.closeDialog(mDialog);
                            Toast.makeText(getContext(), getResources().getString(R.string.update_success),Toast.LENGTH_SHORT).show();
                            plotId = idlist.get(i);
                            confirmPlotName.setText(list.get(i));
                            alertDialog.cancel();
                        }else {
                            LoadingDialog.closeDialog(mDialog);
                            Toast.makeText(getContext(), getResources().getString(R.string.update_failed),Toast.LENGTH_SHORT).show();
                            alertDialog.cancel();
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckSMS> call, Throwable t) {
                        LoadingDialog.closeDialog(mDialog);
                    }
                });
            }
        });
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.show();
    }
}
