package zhixin.cn.com.happyfarm_user.Page;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.hyphenate.chat.EMClient;
import com.qiniu.android.common.AutoZone;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.InvitationCodeActivity;
import zhixin.cn.com.happyfarm_user.LuckDrawRecordActivity;
import zhixin.cn.com.happyfarm_user.MyDynamicActivity;
import zhixin.cn.com.happyfarm_user.MyRecipeActivity;
import zhixin.cn.com.happyfarm_user.MyVegetablesActivity;
import zhixin.cn.com.happyfarm_user.PersonalMessageActivity;
import zhixin.cn.com.happyfarm_user.ServiceAgreementActivity;
import zhixin.cn.com.happyfarm_user.SignInActivity;
import zhixin.cn.com.happyfarm_user.TransactionDetailsActivity;
import zhixin.cn.com.happyfarm_user.WaitingDeliveryActivity;
import zhixin.cn.com.happyfarm_user.WaitingForGoodsActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.ChangePassWordActivity;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.EggManagementActivity;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.ManageAddressActivity;
import zhixin.cn.com.happyfarm_user.MyAlbumActivity;
import zhixin.cn.com.happyfarm_user.MyCollectionActivity;
import zhixin.cn.com.happyfarm_user.OrderRecordActivity;
import zhixin.cn.com.happyfarm_user.PerfectInformationActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.RebindPhoneActivity;
import zhixin.cn.com.happyfarm_user.ReviewImageActivity;
import zhixin.cn.com.happyfarm_user.SettingActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.QiNiu;
import zhixin.cn.com.happyfarm_user.model.SignIn;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.AlertUtilOneButton;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.GuideView;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.SignInDialog;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by DELL on 2018/3/1.
 */

public class PersonalCenterPage extends Fragment{
    //退出时的时间
    private long exitTime;
    private Dialog mDialog;
    private TextView userDiamond;
//    private TextView userIntegral;
    private CircleImageView userImage;
    private TextView userNickname;
    private int Integral;
    private Float Diamond;
    private Double integralEgg;
//    private RelativeLayout userOrderRecord;//订单记录
    private RelativeLayout infoDI;
    private RelativeLayout userLogoImgLogin;
    private RelativeLayout userLogoImgOut;//用户未登录布局文件
    private RelativeLayout userLoginLayout;//用户登录布局文件
    private String UserNickName;
    private String UserImg;
    private String UserTel;
    private String UserCertificatesNo;
    private String UserRealname;
    private int UserPlotId;
    private String UserPlotName;
    private String UserEmail;
    private String InvitationCode;
    private int UserGender;
    private long UserBirthday;
//    private TextView infoPercentage;//完善进度
    private Button changeImg;
    private String tokenStr;
    private ImageView settingUp;
    private Button sign_in;
    private Button loginBtn;
    private Button addDiamondBtn;
    private UploadManager uploadManager;
    private String headImgName;
    private RelativeLayout user_info_view;
    private  View view;
    private int x1 = 0;

    public static final int TAKE_PHOTO = 1;//拍照
    public static final int CHOOSE_PHOTO = 2;//选择相册
    public static final int PICTURE_CUT = 3;//剪切图片
    private Uri imageUri;//相机拍照图片保存地址
    private Uri outputUri;//裁剪完照片保存地址
    private String imagePath;//打开相册选择照片的路径
    private boolean isClickCamera;//是否是拍照裁剪
    // 创建File对象，用于存储拍照后的图片
    private File outputImage;
    private Uri uri;
    private File dataFile;
    private final String TAG = "PersonalCenterPage->";
    private RelativeLayout personalCenterTitleBar;
    private Activity activity;

    private GuideView personalCenterPageGuideView;

    private Badge messageBadge;
    private Badge messageBadge1;

    //最新改动（改为按钮）
    private LinearLayout myAlbum;//成长相册
    private LinearLayout userCollection;//我的收藏
    private LinearLayout userInvitationCode;//邀请码
    private LinearLayout userInformation;//个人信息
    private LinearLayout userManageAddress;//收货地址
    private LinearLayout userRebindPhone;//重绑手机号
    private LinearLayout userChangePassword;//修改密码
    private LinearLayout userAgreement;//用户协议
    private LinearLayout myRecipe;//我的菜谱
    private LinearLayout luckDrawRecord;//抽奖记录
    private LinearLayout goMyDynamic;//我的动态
    private LinearLayout transactionDetails;//交易明细

    private TextView landTimeContent;//租地有效期
    private ImageView waitingDelivery;
    private ImageView waitingForGoods;
    private ImageView userOrderRecord;
    private ImageView userMyVegetables;
    private SignInDialog signInDialog;

    private TextView eggNumber;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.personal_center_layout,null,false);
        //自动识别上传区域
        Configuration config = new Configuration.Builder().zone(AutoZone.autoZone).build();
        uploadManager = new UploadManager(config);
        TextView title = view.findViewById(R.id.title_name);
        title.setText("个人中心");
        ImageView imageView = view.findViewById(R.id.personal_center_message_icon);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PersonalMessageActivity.class);
                startActivity(intent);

            }
        });
        initView();
        setStatusBarHeight();
        initTest();
        personalConterPageGuide();//引导蒙层
        userSetting();
        userLoginBtn();
        userAgreementClick();
        myRecipe();
        luckDrawRecord();
        return view;
    }

    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(activity);
        //相对布局使用方法
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        personalCenterTitleBar.setLayoutParams(params);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    /**
     * 获取状态栏高度
     * @param context 上下文
     * @return int 状态栏高度值
     */
    protected static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    private void initView() {
        personalCenterTitleBar = view.findViewById(R.id.personal_center_title_bar_layout);
        myAlbum = view.findViewById(R.id.user_growth_album);
        userCollection = view.findViewById(R.id.user_collection);
        userInvitationCode = view.findViewById(R.id.user_invitation_code);
        userInformation= view.findViewById(R.id.user_information);
        userRebindPhone= view.findViewById(R.id.user_rebinding_phone);
        userChangePassword= view.findViewById(R.id.user_change_password);
        userManageAddress= view.findViewById(R.id.user_manage_address);
//        infoPercentage = view.findViewById(R.id.info_percentage);//完善进度
        infoDI = view.findViewById(R.id.info_d_i);
        changeImg = view.findViewById(R.id.change_user_img);
        settingUp = view.findViewById(R.id.personal_toolbar_setting_img);
        addDiamondBtn = view.findViewById(R.id.add_diamond_btn);
//        userCollection(view.findViewById(R.id.user_collection));
        userDiamond = view.findViewById(R.id.user_diamond);
//        userIntegral = view.findViewById(R.id.user_integral);
        userNickname = view.findViewById(R.id.user_nickname_my);
        userImage = view.findViewById(R.id.user_image);
        sign_in = view.findViewById(R.id.sign_in);
        loginBtn = view.findViewById(R.id.login_in_btn);
        userLogoImgLogin = view.findViewById(R.id.user_logo_img);
        userLogoImgOut = view.findViewById(R.id.user_logo_out_img);
        userLoginLayout = view.findViewById(R.id.no_login_layout);
        user_info_view = view.findViewById(R.id.user_info_view);
        userAgreement = view.findViewById(R.id.user_agreement);
        userOrderRecord = view.findViewById(R.id.order_record);
        userMyVegetables = view.findViewById(R.id.my_vegetables);
        waitingDelivery = view.findViewById(R.id.waiting_delivery);
        waitingForGoods = view.findViewById(R.id.waiting_for_goods);
        myRecipe = view.findViewById(R.id.my_recipe);
        luckDrawRecord = view.findViewById(R.id.luck_draw_record);
        goMyDynamic = view.findViewById(R.id.go_my_dynamic);
        transactionDetails = view.findViewById(R.id.transaction_details);
        landTimeContent = view.findViewById(R.id.land_time_content);
        eggNumber = view.findViewById(R.id.egg_number);
        //设置图标角标
        messageBadge = new QBadgeView(getContext())
                .bindTarget(view.findViewById(R.id.personal_toolbar_setting_img_message_icon))//获取需要显示的位置
                .setBadgeTextSize(7, true);
        if (Config.isAbout) {
            setMsgNum();
        }
        //消息图标角标
        messageBadge1 = new QBadgeView(getContext())
                .bindTarget(view.findViewById(R.id.personal_center_message_icon_view_total))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
        setMsgNum1(EMClient.getInstance().chatManager().getUnreadMessageCount());
    }

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences pref = activity.getSharedPreferences("User_data",MODE_PRIVATE);
        tokenStr = pref.getString("token","");

        if ("".equals(tokenStr)){
            userLogoImgOut.setVisibility(View.VISIBLE);
            userLoginLayout.setVisibility(View.GONE);
        }else {
            initTest();
            qiniuToken();//七牛token
            //sign_in.setVisibility(View.VISIBLE);
//            getUserInfo();//获取用户信息
            signInClick();//签到点击
            userImgClick();//头像点击
        }
        userInfoTokenTest();
    }


    //TODO 头像点击事件
    private void userImgClick(){
        userImage.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    ViewCompat.setTransitionName(v, "imageView");
                    ActivityOptionsCompat option = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, v, "imageView");
                    Intent intent = new Intent(activity, ReviewImageActivity.class);
                    intent.putExtra("image_url", UserImg);
                    intent.putExtra("activity","Personal");
                    activity.startActivity(intent, option.toBundle());
                }

            }
        });
    }
    //TODO 获取七牛token
    private void qiniuToken(){
        if ("".equals(Config.qiuNiuToken)){
            HttpHelper.initHttpHelper().getQiniuToken().enqueue(new Callback<QiNiu>() {
                @Override
                public void onResponse(Call<QiNiu> call, Response<QiNiu> response) {
                    Log.e("data", JSON.toJSONString(response.body()));
                    if ("success".equals(response.body().getFlag())) {
                        LoadingDialog.closeDialog(mDialog);
                        Config.qiuNiuToken = response.body().getResult().getUploadToken();
                        Config.qiuNiuDomain = response.body().getResult().getDomain();
                        Config.qiuNiuImgUrl = response.body().getResult().getImgUrl();
                    }
                }
                @Override
                public void onFailure(Call<QiNiu> call, Throwable t) {

                }
            });
        }
    }
    /**
     * 签到点击事件
     */
    private void signInClick() {
        sign_in.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                sign_in.setEnabled(false);//TODO 禁用按钮点击
                HttpHelper.initHttpHelper().updateIsSign().enqueue(new Callback<SignIn>() {
                    @Override
                    public void onResponse(Call<SignIn> call, Response<SignIn> response) {
                        Log.e("sssssssss",JSON.toJSONString(response.body()));
                        if (null != response.body()){
                            if (("success").equals(response.body().getFlag())){
                                sign_in.setText("已签到");
//                                integralEgg = response.body().getResult().getEggs()+ integralEgg;
                                if (0 < response.body().getResult().getGiftList().size()) {
                                    Log.i("这里应该弹一个弹窗", "onResponse: ");
                                    //卡片弹出框设置
                                    signInDialog = new SignInDialog(getContext(),response.body().getResult().getSignGift());
                                    signInDialog.showDialog();
                                } else {
                                    Toast toast = Toast.makeText(getContext(), "签到成功！植信蛋+"+response.body().getResult().getEggs()+"个", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                }
                                getUserInfo();
                            }else {
                                int result = response.body().getErrCode();
                                if ( -103 == result) {
                                    sign_in.setText("已签到");
                                    Toast.makeText(getContext(), "今日已签到！",Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getContext(), "签到失败",Toast.LENGTH_SHORT).show();
                                    sign_in.setEnabled(true);//TODO 开启按钮点击
                                }
                            }
                        }else {
                            sign_in.setText("已签到");
                        }
                    }

                    @Override
                    public void onFailure(Call<SignIn> call, Throwable t) {
                        sign_in.setEnabled(true);//TODO 开启按钮点击
                        Toast.makeText(getContext(),TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
    //TODO 设置点击事件
    private void userSetting(){
        settingUp.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    Intent intent = new Intent(getContext(), SettingActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
    //TODO 登录注册点击事件
    private void userLoginBtn(){
        loginBtn.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {
                if (Utils.isFastClick()) {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
    //TODO 充值余额按钮点击事件
    private void userAddDiamond(){
        addDiamondBtn.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
//                    Intent intent = new Intent(getContext(), RechargeActivity.class);
                        Intent intent = new Intent(activity, EggManagementActivity.class);
                        intent.putExtra("Diamond", Diamond);
                        startActivity(intent);
                    }
                }
            }
        });
    }
    //TODO 修改头像点击事件
    public void userChangeImg(){
        changeImg.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)){
                        Toast.makeText(activity,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
                    }else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    }else {
                        ActionSheetDialog();
                    }
                }
            }
        });
    }

    //TODO 我的相册点击事件
    public void myAlbum(){
        myAlbum.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)){
                        Toast.makeText(activity,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
                    }else if ("".equals(tokenStr)){
                        noLoginDialog();
                    }else {
                        Intent intent = new Intent(getContext(), MyAlbumActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    //TODO 我的收藏点击事件
    public void userCollection(){
        userCollection.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)){
                        Toast.makeText(activity,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
                    }else if ("".equals(tokenStr)){
                        noLoginDialog();
                    }else {
                        Intent intent = new Intent(getContext(), MyCollectionActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }
    //TODO 我的邀请码点击事件
    public void userInvitationCode() {
        userInvitationCode.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = new Intent(getContext(), InvitationCodeActivity.class);
                        intent.putExtra("invitation_code_activity", InvitationCode);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    //TODO 获取用户信息
    public void userInformation(){
        userInformation.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = new Intent(getContext(), PerfectInformationActivity.class);
                        intent.putExtra("nickName", UserNickName);
                        intent.putExtra("userImg", UserImg);
                        intent.putExtra("UserRealname", UserRealname);
                        intent.putExtra("tel", UserTel);
                        intent.putExtra("UserPlotName", UserPlotName);
                        intent.putExtra("UserCertificatesNo", UserCertificatesNo);
                        intent.putExtra("UserPlotId", UserPlotId + "");
                        intent.putExtra("UserGender", UserGender + "");
                        intent.putExtra("UserBirthday", UserBirthday + "");
                        intent.putExtra("UserEmail", UserEmail + "");
                        intent.putExtra("landType", 0 + "");
                        intent.putExtra("infosuccessful", "infosuccessful");
                        startActivity(intent);
                    }
                }
            }
        });
    }
    //TODO 重绑手机号点击事件
    public void userRebindPhone(){
        userRebindPhone.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = (new Intent(getContext(), RebindPhoneActivity.class));
                        intent.putExtra("tel", UserTel);
                        startActivity(intent);
                    }
                }
            }
        });
    }
    //TODO 修改密码点击事件
    public void userChangePassword(){
        userChangePassword.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = (new Intent(getContext(), ChangePassWordActivity.class));
                        intent.putExtra("tel", UserTel);
                        startActivity(intent);
                    }
                }
            }
        });
    }
    //TODO 管理收获地址点击事件
    public void userManageAddress(){
        userManageAddress.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else if (NumUtil.checkNull(UserRealname)) {
                        showUserInfoDialog();
                    } else if (NumUtil.checkNull(UserCertificatesNo)) {
                        showUserInfoDialog();
                    } else {
                        Intent intent = (new Intent(getContext(), ManageAddressActivity.class));
                        intent.putExtra("UserRealname", UserRealname);
                        intent.putExtra("tel", UserTel);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    //TODO 待发货点击事件
    public void waitingDelivery() {
        waitingDelivery.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = new Intent(activity, WaitingDeliveryActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }
    //TODO 待收货点击事件
    public void waitingForGoods() {
        waitingForGoods.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = new Intent(activity, WaitingForGoodsActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }
    //TODO 订单记录点击事件
    public void userOrderRecord(){
        userOrderRecord.setOnClickListener(new OnMultiClickListener() {
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = new Intent(activity, OrderRecordActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    //TODO 已上架商品
    public void userMyVegetables() {
        userMyVegetables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = new Intent(activity, MyVegetablesActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    /**
     * 用户协议点击事件
     */
    private void userAgreementClick() {
        userAgreement.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = new Intent(activity, ServiceAgreementActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    /**
     * 用户菜谱点击事件
     */
    private void myRecipe() {
        myRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent = new Intent(activity, MyRecipeActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    /**
     * 用户中奖记录
     */
    private void luckDrawRecord() {
        luckDrawRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        startActivity(new Intent(activity, LuckDrawRecordActivity.class));
                    }
                }
            }
        });
        //我的动态
        goMyDynamic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        Intent intent3 = new Intent(activity, MyDynamicActivity.class);
                        intent3.putExtra("UserImg", UserImg);
                        intent3.putExtra("UserNickName", UserNickName);
                        LoadingDialog.closeDialog(mDialog);
                        startActivity(intent3);
                    }
                }
            }
        });
        //交易明细
        transactionDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    if (!NetWorkUtils.isNetworkAvalible(activity)) {
                        Toast.makeText(activity, "您的网络似乎已经断开了", Toast.LENGTH_SHORT).show();
                    } else if ("".equals(tokenStr)) {
                        noLoginDialog();
                    } else {
                        startActivity(new Intent(activity, TransactionDetailsActivity.class));
                    }
                }
            }
        });

    }

    public void getUserInfo(){
        //TODO 获取用户信息 方便传值
        retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (("success").equals(response.body().getFlag())){
                    Config.AUDIO_STATE = response.body().getResult().getIsVoice() == 1;
//                    Integral = response.body().getResult().getIntegral();
                    Diamond = Float.valueOf(response.body().getResult().getDiamond().toString());
                    integralEgg = response.body().getResult().getIntegralEgg();
                    UserNickName = response.body().getResult().getNickname();
                    UserImg = response.body().getResult().getHeadImg();
                    UserTel = response.body().getResult().getTel();
                    UserCertificatesNo = response.body().getResult().getCertificatesNo();
                    UserRealname = response.body().getResult().getRealname();
                    UserPlotId = response.body().getResult().getPlotId();
                    UserPlotName = response.body().getResult().getPlotName();
                    InvitationCode = response.body().getResult().getInvitationCode();
                    UserGender = response.body().getResult().getGender();
                    UserBirthday = response.body().getResult().getBirthday();
                    UserEmail = response.body().getResult().getEmail();
                    if (0 == response.body().getResult().getLeaseTime() || 0 ==  response.body().getResult().getEndTime()) {
                        landTimeContent .setText("暂未购买租地");
                    } else  {
                        landTimeContent .setText("租地有效期：" + Utils.getDateToStringYMDHMS(response.body().getResult().getLeaseTime())+"-"+Utils.getDateToStringYMDHMS(response.body().getResult().getEndTime())); ;
                    }
                    if (NumUtil.checkNull(UserImg)){
                        userImage.setImageResource(R.drawable.icon_user_img);
                    }else {
                        //Picasso使用了流式接口的调用方式
                        //Picasso类是核心实现类。
                        //实现图片加载功能至少需要三个参数：
                        Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                                .load(UserImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                                .placeholder(R.drawable.icon_user_img)
                                .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                                .into(userImage);//into(ImageView targetImageView)：图片最终要展示的地方。
                                x1 = 20;
                    }
                    if (!(UserGender == 0)){
                        x1 = x1+16;
                    }
                    if (!NumUtil.checkNull(UserRealname)){
                        x1 = x1+16;
                    }
                    if (!NumUtil.checkNull(UserEmail)){
                        x1 = x1+16;
                    }
                    if (!NumUtil.checkNull(UserCertificatesNo)){
                        x1 = x1+16;
                    }
                    if (!NumUtil.checkNull(UserBirthday)){
                        x1 = x1+16;
                    }
                    //完善个人信息的进度
//                    infoPercentage.setVisibility(View.VISIBLE);
//                    infoPercentage.setText(x1+"%");
                    userNickname.setText(UserNickName);
                    userDiamond.setText(Diamond.toString());
                    eggNumber.setText("植信蛋："+integralEgg.toString()+"个");
//                    userIntegral.setText(""+Integral);
                    if (1 == response.body().getResult().getIsSign()){
                        sign_in.setText("已签到");
                        sign_in.setEnabled(false);//TODO 禁用按钮点击
                    }else {
                        sign_in.setText("签到");
                        sign_in.setEnabled(true);//TODO 开启按钮点击
                    }
                }else {
//                    infoPercentage.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                //Toast.makeText(getContext(), "网络连接失败,请检查一下网络", Toast.LENGTH_SHORT).show();
                sign_in.setText("签到");
                sign_in.setEnabled(true);//TODO 开启按钮点击
            }
        });
    }
    public void initTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if (("success").equals(response.body().getFlag())){
                    saveState("1");
//                    userNickname.setText(""+UserNickName);
//                    infoPercentage.setVisibility(View.VISIBLE);
                    userLoginLayout.setVisibility(View.VISIBLE);
                    userLogoImgOut.setVisibility(View.GONE);
                }else {
                    SharedPreferences pref = activity.getSharedPreferences("User_data",MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.remove("token");
                    editor.remove("userId");
                    editor.apply();
                    saveState("0");
                    tokenStr = "";
                    userImage.setImageResource(R.drawable.icon_user_img);
                    userLogoImgOut.setVisibility(View.VISIBLE);
                    userLoginLayout.setVisibility(View.GONE);
//                    infoPercentage.setVisibility(View.GONE);
                }
                myAlbum();
                userCollection();
                userInvitationCode();
                userInformation();
                userRebindPhone();
                userChangePassword();
                userManageAddress();
                userOrderRecord();
                userMyVegetables();
                userChangeImg();
                userAddDiamond();
                waitingDelivery();
                waitingForGoods();
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                userLogoImgOut.setVisibility(View.VISIBLE);
                userLoginLayout.setVisibility(View.GONE);
//                infoPercentage.setVisibility(View.GONE);
                Log.e("onStart", "失败");
            }
        });

    }

    public void saveState(String tokenStr){
        //获取SharedPreferences对象
        SharedPreferences.Editor editor = activity.getSharedPreferences("data",MODE_PRIVATE).edit();
        //设置参数
        editor.putString("state",tokenStr);
        //提交
        editor.apply();
    }

    /**
     * TODO 完善用户信息layout弹窗
     */
    private void showUserInfoDialog(){
        final AlertUtilBest diyDialog = new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(TextString.InfoTitleText)
                .setContent(TextString.InfoContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(activity, PerfectInformationActivity.class);
                        intent.putExtra("nickName", UserNickName);
                        intent.putExtra("userImg", UserImg);
                        intent.putExtra("UserRealname", UserRealname);
                        intent.putExtra("tel", UserTel);
                        intent.putExtra("UserPlotName", UserPlotName);
                        intent.putExtra("UserCertificatesNo", UserCertificatesNo);
                        intent.putExtra("UserPlotId", UserPlotId + "");
                        intent.putExtra("UserGender", UserGender + "");
                        intent.putExtra("UserBirthday", UserBirthday + "");
                        intent.putExtra("UserEmail",UserEmail+"");
                        intent.putExtra("PersonalCenter","PersonalCenter");
                        intent.putExtra("landType",0+"");
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog(){
        final AlertUtilBest diyDialog = new AlertUtilBest(activity);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    //TODO 上传头像弹框
    private void ActionSheetDialog() {
        final String[] stringItems = {"拍照", "从相册中选择"};
        final ActionSheetDialog dialog = new ActionSheetDialog(activity, stringItems, null);
        dialog.title("上传头像")//标题
                .titleTextSize_SP(14.5f)//字体大小
                .show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        initializeCamera();
                        break;
                    case 1:
                        initializeAlbum();
                        break;
                        default:
                }
                dialog.dismiss();
            }
        });
    }

    private void initializeCamera() {
        //获取相机权限，如果不开启会报错  REQUEST_GET_PERMISSION与回调对应
        //在Fragment中申请权限，不要使用ActivityCompat.requestPermissions, 直接使用Fragment的requestPermissions方法，否则在Fragment中无法回掉onRequestPermissionsResult
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
        }else {
            openCamera();
        }
    }

    private void initializeAlbum() {
        //动态权限
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        } else {
            openAlbum();//打开相册
        }
    }
    //图片结果处理方法
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TAKE_PHOTO://拍照
                isClickCamera = true;
                if (resultCode == RESULT_OK) {
                    cropPhoto(imageUri);//裁剪图片
                }
                break;
            case CHOOSE_PHOTO://打开相册

                if (data == null) {//这里判断主要测试与小米手机，点击相册允许后，再点取消会崩溃
                    return;
                }// 判断手机系统版本号
                if (Build.VERSION.SDK_INT >= 19) {
                    // 4.4及以上系统使用这个方法处理图片
                    handleImageOnKitKat(data);
                } else {
                    // 4.4以下系统使用这个方法处理图片
                    handleImageBeforeKitKat(data);
                }
                break;
            case PICTURE_CUT://裁剪完成
                if (data != null) {
                    Bitmap bitmap;
                    try {
                        //此处判断似乎没有作用，已去除
//                        if (isClickCamera) {
//                            bitmap = BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(outputUri));
//                        } else {
//                            bitmap = BitmapFactory.decodeFile(imagePath);
//                        }
//                        //TODO 这里暂时有问题
//                        if (bitmap == null){
//                            //用于删除上面创建的拍照产生的图片
//                            outputImage.delete();
//                        }else {
//                            //设置剪切完的图片显示位置
//                            Log.e("uussee",String.valueOf(bitmap));
//                        }

                        //用于七牛 先拿到图片的位置
                        dataFile = new File(outputUri.getPath());
                        Log.d(TAG, "onActivityResult: 图片大小：" + dataFile.length()/1024);
                        try {
                            //将图片进行压缩
                            dataFile = new Compressor(activity).compressToFile(dataFile);
                            Log.d(TAG, "onActivityResult: 图片压缩后路径：" + dataFile);
                            Log.d(TAG, "onActivityResult: 图片大小：" + dataFile.length()/1024);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if ( dataFile != null){
                            long timeStampSec = System.currentTimeMillis()/1000;
                            String timestamp = String.format("%010d", timeStampSec);
                            headImgName = "headImg_" + getUserId() + "_" + timestamp;//<指定七牛服务上的文件名，或 null>;
                            uploadManager.put(dataFile, headImgName, Config.qiuNiuToken,
                                    new UpCompletionHandler() {
                                        @Override
                                        public void complete(String key, ResponseInfo info, org.json.JSONObject response) {
                                            //res包含hash、key等信息，具体字段取决于上传策略的设置
                                            if(info.isOK()) {
                                                String headImg = Config.qiuNiuImgUrl+headImgName;
                                                Picasso.get()
                                                        .load(headImg)
                                                        .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                                                        .into(userImage);

                                                UserImg = headImg;//TODO 防止跳完善个人信息页面头像不变动
                                                userHeadImage(headImg);
                                                if (dataFile.exists()) {
                                                    dataFile.delete();
                                                }
//                                                Log.i("qiniu", "Upload Success");
                                            } else {
                                                Config.qiuNiuToken = "";
                                                Config.qiuNiuDomain = "";
                                                Config.qiuNiuDomain = "";
//                                                Log.i("qiniu", "Upload Fail");
                                                //如果失败，这里可以把info信息上报自己的服务器，便于后面分析上传错误原因
                                            }
//                                            Log.i("qiniu", key + ",\r\n " + info + ",\r\n " + response);
                                        }
                                    },null);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }
    //Todo 上传头像接口
    public void userHeadImage(String headImage){
        Call<CheckSMS> updateUserInfo = HttpHelper.initHttpHelper().updateUserImg(headImage);
        updateUserInfo.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if (("success").equals(response.body().getFlag())) {
                    LoadingDialog.closeDialog(mDialog);
//                    view.postInvalidate();
                    new GameThread().run();
                    saveHeadImage(headImage);
                    Toast.makeText(getContext(), "头像上传成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openCamera() {
        //yyyy_MM_dd_HH_mm_ss 获取当前时间
        String filename = DateUtil.dateYMD_HMS();
        // 创建File对象，用于存储拍照后的图片
        outputImage = new File(Environment.getExternalStorageDirectory(), filename + ".png");
        try {
            if (outputImage.exists()) {
                outputImage.delete();
            }else {
                outputImage.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT < 24) {
            imageUri = Uri.fromFile(outputImage);
        } else {
            //Android 7.0系统开始 使用本地真实的Uri路径不安全,使用FileProvider封装共享Uri
            //参数二:fileprovider绝对路径 com.dyb.testcamerademo：项目包名
            imageUri = FileProvider.getUriForFile(activity, "zhixin.cn.com.happyfarm_user", outputImage);
        }
        // 启动相机程序
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, TAKE_PHOTO);

    }


    public void saveHeadImage(String ImageUrl){
        SharedPreferences.Editor editor = activity.getSharedPreferences("User_data",MODE_PRIVATE).edit();
        editor.putString("userImg",ImageUrl);
        editor.apply();
    }

    /**
     * 打开相册
     */
    private void openAlbum() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent, CHOOSE_PHOTO); // 打开相册
    }
    /**
     * 裁剪图片
     */
    private void cropPhoto(Uri uri) {
        String filename = DateUtil.dateYMD_HMS();
        // 创建File对象，用于存储裁剪后的图片，避免更改原图
        //TODO id 加 head
        File file = new File(Environment.getExternalStorageDirectory(), filename+"_head.png");
        try {
            if (file.exists()) {
                file.delete();
            }else {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        outputUri = Uri.fromFile(file);
        Intent intent = new Intent("com.android.camera.action.CROP");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.setDataAndType(uri, "image/*");
        //裁剪图片的宽高比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("crop", "true");//可裁剪
        // 裁剪后输出图片的尺寸大小
        //intent.putExtra("outputX", 400);
        //intent.putExtra("outputY", 200);
        intent.putExtra("scale", true);//支持缩放
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.toString());//输出图片格式
        intent.putExtra("noFaceDetection", true);//取消人脸识别
        startActivityForResult(intent, PICTURE_CUT);
    }

    // 4.4及以上系统使用这个方法处理图片 相册图片返回的不再是真实的Uri,而是分装过的Uri
    @TargetApi(19)
    private void handleImageOnKitKat(Intent data) {
//        imagePath = null;
        uri = data.getData();
//        Log.d("TAG", "handleImageOnKitKat: uri is " + uri);
//        if (DocumentsContract.isDocumentUri(activity, uri)) {
//            // 如果是document类型的Uri，则通过document id处理
//            String docId = DocumentsContract.getDocumentId(uri);
//            if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
//                String id = docId.split(":")[1]; // 解析出数字格式的id
//                String selection = MediaStore.Images.Media._ID + "=" + id;
//                imagePath = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection);
//            } else if ("com.android.providers.downloads.documents".equals(uri.getAuthority())) {
//                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
//                imagePath = getImagePath(contentUri, null);
//            }
//        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
//            // 如果是content类型的Uri，则使用普通方式处理
//            imagePath = getImagePath(uri, null);
//        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
//            // 如果是file类型的Uri，直接获取图片路径即可
//            imagePath = uri.getPath();
//        }
        cropPhoto(uri);
        user_info_view.postInvalidate();
    }

    private String getImagePath(Uri uri, String selection) {
        String path = null;
        // 通过Uri和selection来获取真实的图片路径
        Cursor cursor = activity.getContentResolver().query(uri, null, selection, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }

    private void handleImageBeforeKitKat(Intent data) {
        Uri uri = data.getData();
//        imagePath = getImagePath(uri, null);
        cropPhoto(uri);
    }

    //获取用户权限  打开相机
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            //此处1 ，2  与上方打开的1，2对应
            case 1:
                //判断是否有权限
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera();//打开相机
                } else {
                    Toast.makeText(getContext(), "你需要授权才可以使用哦", Toast.LENGTH_LONG).show();
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openAlbum();
                } else {
                    Toast.makeText(getContext(), "你需要授权才可以使用哦", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        x1 = 0;
        if (!Config.isLogin){
            //隐藏“完善个人信息”layout上的百分比
//            infoPercentage.setVisibility(View.GONE);
        }
        normalDialogOneBtn(Config.personalCenterDialogShow);
        //更新消息角标
        int total = EMClient.getInstance().chatManager().getUnreadMessageCount();
        setMsgNum1(total);
        getUserInfo();//获取用户信息
    }
    private void normalDialogOneBtn(boolean isShow) {
        if (isShow) {
            Config.personalCenterDialogShow = false;
            final AlertUtilOneButton diyDialog = new AlertUtilOneButton(activity);
            diyDialog.setOk("确定")
                    .setContent(activity.getResources().getString(R.string.pwd_update))
                    .setDialogClickListener(new AlertUtilOneButton.DialogClickListener() {
                        @Override
                        public void ok() {
                            diyDialog.cancle();
                            String[] strArr = {"token", "userId", "userImg", "uuid"};
                            SharedPreferencesUtils.cleanShared(activity, "User_data", strArr);
                            Intent intent = new Intent(activity, LoginActivity.class);
                            startActivity(intent);
                        }
                    });
            diyDialog.builder();
            diyDialog.show();
        }
    }
    public void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                if ("failed".equals(response.body().getFlag())) {
                    SharedPreferences pref = activity.getSharedPreferences("User_data",MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.remove("userId");
                    editor.remove("token");
                    editor.apply();
                    saveState("0");
                    userNickname.setText("请登录");
                    userImage.setImageResource(R.drawable.icon_user_img);
                    userLoginLayout.setVisibility(View.GONE);
                    userLogoImgOut.setVisibility(View.VISIBLE);
//                    infoPercentage.setVisibility(View.GONE);
                    tokenStr = "";
                    Config.isLogin = false;
                    view.findViewById(R.id.personal_center_message_icon).setVisibility(View.GONE);
                }else {
                    Config.isLogin = true;
                    view.findViewById(R.id.personal_center_message_icon).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
                Config.isLogin = false;
                saveState("0");
                userNickname.setText("请登录");
                userImage.setImageResource(R.drawable.icon_user_img);
                userLoginLayout.setVisibility(View.GONE);
                userLogoImgOut.setVisibility(View.VISIBLE);
//                infoPercentage.setVisibility(View.GONE);
                view.findViewById(R.id.personal_center_message_icon).setVisibility(View.GONE);

                tokenStr = "";
            }
        });
    }

    class GameThread implements Runnable {
        public void run() {
            view.postInvalidate();
//                user_info_view.invalidate();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        x1 = 0;
    }

    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }

    /**
     * 个人中心引导页面
     */
    public void personalConterPageGuide() {
        //判断是否第一次登陆
        if (("isFirst").equals(getGuide()) ) {//不是第一次加载初始页面
            System.out.println("不是第一次登陆");
        } else {
            TextView tv = new TextView(getContext());
            tv.setText(R.string.tv6);
            tv.setTextColor(getResources().getColor(R.color.white));
            tv.setTextSize(20);
            tv.setGravity(Gravity.CENTER);

            // 使用图片
            final ImageView iv = new ImageView(getContext());
            iv.setImageResource(R.mipmap.wozhidaole);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            iv.setLayoutParams(params);

            personalCenterPageGuideView = GuideView.Builder
                    .newInstance(getContext())
                    .setTargetView(view.findViewById(R.id.login_in_btn))
                    .setCustomGuideView(tv)
                    .setTextGuideView(iv)
                    .setDirction(GuideView.Direction.BOTTOM)
                    .setShape(GuideView.MyShape.ELLIPSE)   // 设置矩形显示区域，
                    .setRadius(10)          // 设置圆形或矩形透明区域半径，默认是targetView的显示矩形的半径，如果是矩形，这里是设置矩形圆角大小
                    .setBgColor(getResources().getColor(R.color.shadow))
                    .setOnclickListener(new GuideView.OnClickCallback() {
                        @Override
                        public void onClickedGuideView() {
                            personalCenterPageGuideView.hide();
                            Intent intent = new Intent(activity, LoginActivity.class);
                            startActivity(intent);
                            saveGuide("isFirst");
                        }
                    })
                    .build();
            personalCenterPageGuideView.show();
        }
    }

    /**
     * 使用SharedPreferences保存导航页第几次
     */
    public void saveGuide(String isFirst){
        //获取SharedPreferences对象
        SharedPreferences.Editor editor = getActivity().getSharedPreferences("PersonalConterPage",MODE_PRIVATE).edit();
        //设置参数
        editor.putString("isFirst", isFirst);
        //提交
        editor.apply();
    }
    /**
     * 是否是第一次安装
     * @return
     */
    private String getGuide(){
        return getActivity().getSharedPreferences("PersonalConterPage",MODE_PRIVATE).getString("isFirst","");
    }

    /**
     * 设置设置图标按钮
     * num 消息数量
     * position 设置显示tablayout的item 暂时未时候，考虑后期可能所有的item都需要
     */
    private void setMsgNum(){
        if (messageBadge != null){
//            Log.i(TAG, "消息设置成功setMsgNum: "+num);
            messageBadge.setBadgeText("!");

        }else {
//            Log.i(TAG, "消息设置失败setMsgNum: "+num);
        }
    }
    /**
     * 设置消息图标按钮
     * num 消息数量
     * position 设置显示tablayout的item 暂时未时候，考虑后期可能所有的item都需要
     */
    private void setMsgNum1(int num){
        if (messageBadge1 != null){
//            Log.i(TAG, "消息设置成功setMsgNum: "+num);
            messageBadge1.setBadgeNumber(num);

        }else {
//            Log.i(TAG, "消息设置失败setMsgNum: "+num);
        }
    }

    /**
     * 获取本地软件版本号名称
     */
    public static String getLocalVersionName(Context ctx) {
        String localVersion = "";
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }
    public void getEMNumber(int number){
        setMsgNum1(number);
    }
}
