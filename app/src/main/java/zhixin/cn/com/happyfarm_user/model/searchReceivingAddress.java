package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * Created by Administrator on 2018/5/9.
 */

public class searchReceivingAddress {


    /**
     * result : [{"id":60,"usersId":12,"plotId":4,"startTime":1525922681000,"updateTime":1526021879000,"isDefault":1,"state":0,"realname":"神奇人物","tel":"15170029037","address":"内蒙古自治区呼伦贝尔市鄂温克族自治旗伊敏苏木天鹅湖"},{"id":8,"usersId":12,"plotId":1,"startTime":1525836820000,"updateTime":1526021850000,"isDefault":0,"state":1,"realname":"神奇人物","tel":"15170029037","address":"江苏省无锡市滨湖区滨湖街道仙河苑"},{"id":61,"usersId":12,"plotId":11,"startTime":1525932599000,"updateTime":1526002034000,"isDefault":0,"state":0,"realname":"神奇人物","tel":"15170029037","address":"福建省福州市仓山区建新镇tade益达"},{"id":78,"usersId":12,"plotId":7,"startTime":1526022142000,"updateTime":1526022142000,"isDefault":0,"state":0,"realname":"神奇人物","tel":"15170029037","address":"云南省德宏傣族景颇族自治州瑞丽市勐卯镇天使的故乡"},{"id":80,"usersId":12,"plotId":5,"startTime":1526112582000,"updateTime":1526112582000,"isDefault":0,"state":0,"realname":"神奇人物","tel":"15170029037","address":"江苏省无锡市新吴区坊前镇瑞城花园"},{"id":81,"usersId":12,"plotId":6,"startTime":1526112593000,"updateTime":1526112593000,"isDefault":0,"state":0,"realname":"神奇人物","tel":"15170029037","address":"贵州省毕节市赫章县新发苗族彝族回族乡朗诗绿郡"}]
     * flag : success
     */

    private String flag;
    private List<ResultBean> result;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * id : 60
         * usersId : 12
         * plotId : 4
         * startTime : 1525922681000
         * updateTime : 1526021879000
         * isDefault : 1
         * state : 0
         * realname : 神奇人物
         * tel : 15170029037
         * address : 内蒙古自治区呼伦贝尔市鄂温克族自治旗伊敏苏木天鹅湖
         */

        private int id;
        private int usersId;
        private int plotId;
        private long startTime;
        private long updateTime;
        private int isDefault;
        private int state;
        private String realname;
        private String tel;
        private String address;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUsersId() {
            return usersId;
        }

        public void setUsersId(int usersId) {
            this.usersId = usersId;
        }

        public int getPlotId() {
            return plotId;
        }

        public void setPlotId(int plotId) {
            this.plotId = plotId;
        }

        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public int getIsDefault() {
            return isDefault;
        }

        public void setIsDefault(int isDefault) {
            this.isDefault = isDefault;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
