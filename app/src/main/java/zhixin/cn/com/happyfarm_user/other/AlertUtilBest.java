package zhixin.cn.com.happyfarm_user.other;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import zhixin.cn.com.happyfarm_user.R;

/**
 * Created by Administrator on 2019/1/16.
 */

public class AlertUtilBest {

    private Context context;
    private Dialog dialog;

    private String content;
    private String cancel;
    private String ok;

    private DialogClickListener dialogClickListener;

    public void setDialogClickListener(DialogClickListener dialogClickListener) {
        this.dialogClickListener = dialogClickListener;
    }

    public AlertUtilBest setContent(String content) {
        this.content = content;
        return this;
    }

    public AlertUtilBest setCancel(String cancel) {
        this.cancel = cancel;
        return this;
    }

    public AlertUtilBest setOk(String ok) {
        this.ok = ok;
        return this;
    }

    /**
     * dialog弹出后会点击屏幕，dialog不消失；点击物理返回键dialog消失
     * @param isCancel true消失
     * @return
     */
    public AlertUtilBest setCanceledOnTouchOutside(boolean isCancel) {
        if (dialog != null)
            this.dialog.setCanceledOnTouchOutside(isCancel);
        return this;
    }

    /**
     * dialog.setCancelable(false);
     * dialog弹出后会点击屏幕或物理返回键，dialog不消失
     * @param isCancel true消失
     * @return
     */
    public AlertUtilBest setCancelable(boolean isCancel){
        if (dialog != null)
            this.dialog.setCancelable(isCancel);
        return this;
    }

    public AlertUtilBest(Context context) {
        this.context = context;
    }

    public AlertUtilBest builder() {
        dialog = new Dialog(context, R.style.dialog);
        View view = LayoutInflater.from(context).inflate(
                R.layout.layout_perfect_information_dialog, null);
        dialog.setContentView(view);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        //设置左右间距
        params.width = context.getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(context, 100f);
        params.bottomMargin = DensityUtil.dp2px(context, 0f);

        TextView tv_dialog_content = (TextView) view.findViewById(R.id.user_info_content);
        TextView tv_cancel = (TextView) view.findViewById(R.id.lease_immediately_cancel_bt);
        TextView tv_ok = (TextView) view.findViewById(R.id.lease_immediately_bt);
        if (!TextUtils.isEmpty(content))
            tv_dialog_content.setText(content);
        if (!TextUtils.isEmpty(cancel))
            tv_cancel.setText(cancel);
        if (!TextUtils.isEmpty(ok))
            tv_ok.setText(ok);

//        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                Log.e("Dialog", "onCancel: "+"消失");
//            }
//        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogClickListener != null)
                    dialogClickListener.cancel();
            }
        });
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogClickListener != null)
                    dialogClickListener.ok();
            }
        });
        view.setLayoutParams(params);
        return this;
    }

    public void show() {
        if (dialog != null)
            dialog.show();
    }

    public void cancel() {
        if (dialog != null)
            dialog.cancel();
    }


    public interface DialogClickListener {
        void cancel();

        void ok();
    }
}
