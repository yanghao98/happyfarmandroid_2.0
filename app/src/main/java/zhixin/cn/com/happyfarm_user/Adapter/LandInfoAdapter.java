package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.MyImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.LandInfoCardItem;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.utils.Config;

public class LandInfoAdapter extends PagerAdapter implements CardAdapter {


    private List<CardView> mViews;
    private List<LandInfoCardItem.landInfoCardBean> mData;
    private float mBaseElevation;
    private int landId;//当前用户的租地编号
    public LandInfoAdapter() {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
    }

    public void addCardItem(List<LandInfoCardItem.landInfoCardBean> item,int landId) {
        mData = item;
        for ( int i = 0;i < mData.size();i++ ){
            mViews.add(null);
        }
        this.landId = landId;
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.land_info_adapter, container, false);
        container.addView(view);
        bind(mData.get(position), view,landId);
        CardView cardView = (CardView) view.findViewById(R.id.cardView);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }
        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
//        Log.e("position 个数：", "instantiateItem: "+mData.size());

        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(LandInfoCardItem.landInfoCardBean item, View view,int landId) {
        LinearLayout goOtherLand = view.findViewById(R.id.go_other_land);
        CircleImageView myImageView =  view.findViewById(R.id.user_icon_image);
        TextView titleTextView = (TextView) view.findViewById(R.id.titleTextView);
        TextView contentTextView = (TextView) view.findViewById(R.id.contentTextView);
        TextView corpNames = (TextView) view.findViewById(R.id.corp_name);
        if (NumUtil.checkNull(item.getHeadImg())){
            myImageView.setImageResource(R.drawable.icon_user_img);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(item.getHeadImg())//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .placeholder(R.drawable.icon_user_img)
                    .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                    .into(myImageView);//into(ImageView targetImageView)：图片最终要展示的地方。
        }
        titleTextView.setText(item.getUserName());
        contentTextView.setText(item.getAlias());
        if (null == item.getCropName()) {
            corpNames.setText("暂未种植");
        } else {
            corpNames.setText(item.getCropName());

        }
        goOtherLand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), LandVideoActivity.class);
//                Log.i("当前用户租地编号", "onClick: " + landId);
//                Log.i("点击收藏地块的编号", "onClick: " + item.getId());
                    if (item.getId() == landId) {
                        intent.putExtra("state","0");
                    } else {
                        intent.putExtra("state","2");
                    }
                intent.putExtra("landNo",item.getLandNo().toString());
                intent.putExtra("landId",item.getId().toString());
                intent.putExtra("UserName",item.getUserName());
                intent.putExtra("UserId",item.getUserId().toString());
                intent.putExtra("LeaseTime", DateUtil.getStrTime(String.valueOf(item.getLeaseTime())));
                intent.putExtra("ExpirationTime",DateUtil.getStrTime(String.valueOf(item.getExpirationTime())));
                intent.putExtra("OtherUserImg",item.getHeadImg());
//                intent.putExtra("isLandlord",);
//                intent.putExtra("superUser",);
                intent.putExtra("landType",item.getLandType().toString());
                view.getContext().startActivity(intent);
            }
        });
    }
}
