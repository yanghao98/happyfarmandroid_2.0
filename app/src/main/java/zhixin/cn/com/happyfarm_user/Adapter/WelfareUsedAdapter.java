package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.LuckDrawRecord;

import static zhixin.cn.com.happyfarm_user.other.DateUtil.getStrTimeYear;

public class WelfareUsedAdapter extends RecyclerView.Adapter<WelfareUsedAdapter.StaggerViewHolder> {

    private Context myContext;
    private List<LuckDrawRecord.LuckDrawBean.LuckDrawList> luckDrawLists;
    public WelfareUsedAdapter(Context context, List<LuckDrawRecord.LuckDrawBean.LuckDrawList>  listView) {
        this.myContext = context;
        this.luckDrawLists = listView;
    }

    @NonNull
    @Override
    public WelfareUsedAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(myContext, R.layout.welfare_used_item,null);
        WelfareUsedAdapter.StaggerViewHolder staggerViewHolder = new WelfareUsedAdapter.StaggerViewHolder(view);
        return staggerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WelfareUsedAdapter.StaggerViewHolder staggerViewHolder, int i) {
        LuckDrawRecord.LuckDrawBean.LuckDrawList dataBean = luckDrawLists.get(i);
        staggerViewHolder.setData(dataBean);
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if(luckDrawLists!=null&&luckDrawLists.size()>0){
            return luckDrawLists.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView1;
        private TextView textView2;

        public StaggerViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.WelfarePage_image);
            textView1 = itemView.findViewById(R.id.WelfarePage_luck_draw_content);
            textView2 = itemView.findViewById(R.id.WelfarePage_dut_time_content);
        }

        public void setData(LuckDrawRecord.LuckDrawBean.LuckDrawList  luckDrawList) {
            Glide.with(myContext)
                    .load(luckDrawList.getImageUrl())
                    .into(imageView);
            if (1 == luckDrawList.getLuckyDrawType()) {
                textView1.setText("租地享受" + luckDrawList.getLuckyDrawContent()+"优惠");
            } else {
                textView1.setText(luckDrawList.getLuckyDrawContent()+ "兑换卡一张");
            }
            textView2.setText("到期时间：" + getStrTimeYear(luckDrawList.getDueTime()));
        }
    }
}
