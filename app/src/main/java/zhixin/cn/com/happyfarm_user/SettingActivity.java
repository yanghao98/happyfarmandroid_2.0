package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.kyleduo.switchbutton.SwitchButton;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.CleanMessageUtil;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by Administrator on 2018/5/12.
 */

public class SettingActivity extends ABaseActivity {

    private Dialog mDialog;
    private RelativeLayout userAbout;
    private RelativeLayout userLoginOut;
    private RelativeLayout cacheLayout;
    private RelativeLayout feedbackLayout;
    private RelativeLayout privacyPolicyLayout;
    private TextView cacheView;
    private SwitchButton trafficSb;
//    private RelativeLayout detectionUpdateLayout;
//    private TextView detectionUpdateView;
    private SwitchButton audioSb;
    private RelativeLayout audioLayout;
    private RelativeLayout helpLayout;
    private ImageView aboutIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.setting_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("设置");

        initView();
        initTest();
        cacheOnclick();
        feedbackOnclick();
        privacyPolicyOnclick();
        helpLayout();
        userAboutBtn();
        userLoginOutBtn();
        trafficSwitch();
        audioSwitch();
//        updateOnclick();
    }
    private void initView() {
        userAbout = findViewById(R.id.about_us_layout);
        userLoginOut = findViewById(R.id.user_login_out);
        cacheLayout = findViewById(R.id.clear_cache_layout);
        privacyPolicyLayout = findViewById(R.id.privacy_policy_layout);
        aboutIcon = findViewById(R.id.about_icon1);
        aboutIcon.setVisibility(View.GONE);
        helpLayout = findViewById(R.id.help_layout);
        feedbackLayout = findViewById(R.id.feedback_layout);
        trafficSb = findViewById(R.id.sb_custom_flyme);
        cacheView = findViewById(R.id.cache_view);
//        detectionUpdateLayout = findViewById(R.id.detection_update_layout);
//        detectionUpdateView = findViewById(R.id.detection_update_view);
        //音频开关
        audioSb = findViewById(R.id.sb_audio_flyme);
        //音频开关布局
        audioLayout = findViewById(R.id.audio_layout);
        if (Config.isAbout) {
            aboutIcon.setVisibility(View.VISIBLE);
        }

    }
    //TODO 关于我们点击事件
    private void userAboutBtn(){
        userAbout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {
                Intent intent = new Intent(SettingActivity.this, AboutUsActivity.class);
                startActivity(intent);
            }
        });
    }
    //TODO 反馈点击事件
    private void feedbackOnclick(){
        feedbackLayout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {
                Intent intent = new Intent(SettingActivity.this, FeedBackActivity.class);
                intent.putExtra("page", "SettingActivity");
                startActivity(intent);
            }
        });
    }
    //TODO 流量播放点击事件
    private void trafficSwitch() {
        trafficSb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    saveTraffic("trafficYes");
                    showToastShort(SettingActivity.this, "开启移动数据观看");
                } else {
                    SharedPreferences pref = getSharedPreferences("data",MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.remove("traffic");
                    editor.apply();
                    showToastShort(SettingActivity.this, "关闭移动数据观看");
                }

            }
        });
    }

    /**
     * 音频直播开启设置
     */
    private void audioSwitch() {
        audioSb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    //开启事件
                    audioReq(1);
                } else {
                    //关闭事件
                    audioReq(0);
                }
            }
        });
    }

    /**
     * 开关直播声音接口
     */
    private void audioReq(Integer isVoice) {
        Call<CheckSMS> voice = HttpHelper.initHttpHelper().updateVoice(isVoice);
        voice.enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                Log.d("开关直播声音接口", "onResponse: " + response.body());
                if (null != response.body()) {
                    if ("success".equals(response.body().getFlag())) {
                        int voiceState = response.body().getMessage();
                        Config.AUDIO_STATE = voiceState == 1;
                        String str = voiceState == 1 ? "开启直播音频声音" : "关闭音频直播声音";
                        showToastShort(SettingActivity.this, str);
                    } else {
                        showToastShort(SettingActivity.this, response.body().getResult());
                        Boolean isCheck = isVoice != 1;
                        audioSb.setChecked(isCheck);
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                showToastShort(SettingActivity.this, TextString.NetworkRequestFailed);
                Boolean isCheck = isVoice != 1;
                audioSb.setChecked(isCheck);
            }
        });
    }

    //TODO 隐私点击事件
    private void privacyPolicyOnclick(){
        privacyPolicyLayout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {
                Intent intent = new Intent(SettingActivity.this,TermsActivity.class);
                intent.putExtra("termsName", "privacy");//隐私
                intent.putExtra("url","");//因为帮助里面的操作说明也用到了TermsActivity页面所以穿个空url进去进行区分
                startActivity(intent);
            }
        });
    }

//    //TODO 帮助点击事件
    private void helpLayout() {
        helpLayout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
//                Log.i("****", "onMultiClick: 点击了帮助按钮");
                Intent intent = new Intent(SettingActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });
    }


    //TODO 清楚缓存点击事件
    private void cacheOnclick(){
        cacheLayout.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {
                removeCache();
            }
        });
    }
    //TODO 退出登录点击事件
    private void userLoginOutBtn(){
        userLoginOut.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View view) {
                MaterialDialogDefault();
            }
        });
    }
    //TODO 清楚缓存弹框
    private void removeCache(){
        final AlertUtilBest diyDialog =new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk("确定")
                .setContent("您确定要清楚应用缓存吗？")
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        CleanMessageUtil.clearAllCache(SettingActivity.this);
                        try {
                            cacheView.setText(CleanMessageUtil.getTotalCacheSize(SettingActivity.this));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    //TODO 退出登录弹框
    private void MaterialDialogDefault() {
        final AlertUtilBest diyDialog =new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk("确定")
                .setContent("您确定要退出登录吗？")
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        mDialog = LoadingDialog.createLoadingDialog(SettingActivity.this,"退出中");
                        Call<CheckSMS> loginOut = HttpHelper.initHttpHelper().loginOut();
                        loginOut.enqueue(new Callback<CheckSMS>() {
                            @Override
                            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                                //去除if判断，无论是否退出成功或失败都清除原始保留数据
//                                if (("success").equals(response.body().getFlag())){
                                LoadingDialog.closeDialog(mDialog);
                                // CleanMessageUtil.clearAllCache(SettingActivity.this);//TODO 退出登录清除缓存
                                EMLogout();
                                Config.isOneLogin = "isOneLogin";
                                Config.isLogin = false;
                                // MiPushClient.pausePush(SettingActivity.this,null);//暂停小米推送
                                SharedPreferences pref = getSharedPreferences("User_data",MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.remove("token");
                                editor.remove("userId");
                                editor.remove("userImg");
                                editor.remove("uuid");
                                editor.apply();
                                if (Config.AllEMFriendInfo.size() != 0) {
                                    Config.AllEMFriendInfo.clear();//TODO 退出登录清空环信设置头像的数组内容，预防登录其他账号无法加载头像与昵称问题
//                                    Log.i("环信设置头像", "onResponse: "+Config.AllEMFriendInfo);
                                }
                                finish();
                                showToastShort(SettingActivity.this,"退出登录成功");
//                                }else {
//                                    LoadingDialog.closeDialog(mDialog);
//                                    Toast.makeText(SettingActivity.this,response.body().getFlag(),Toast.LENGTH_SHORT).show();
//                                }
                            }

                            @Override
                            public void onFailure(Call<CheckSMS> call, Throwable t) {
                                LoadingDialog.closeDialog(mDialog);
                                showToastShort(SettingActivity.this, TextString.NetworkRequestFailed);
                            }
                        });
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    /**
     * 检查更新
     */
//    private void updateOnclick() {
//        detectionUpdateLayout.setOnClickListener(new OnMultiClickListener() {
//            @Override
//            public void onMultiClick(View v) {
//
//            }
//        });
//    }
    // TODO 环信退出登录
    public void EMLogout(){
        // true 代表退出登录以后不接收离线消息
        EMClient.getInstance().logout(true, new EMCallBack() {

            @Override
            public void onSuccess() {
                // TODO Auto-generated method stub
//                Log.i("EMLogout", "info"+"退出成功");
            }

            @Override
            public void onProgress(int progress, String status) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onError(int code, String message) {
                // TODO Auto-generated method stub
//                Log.i("EMLogout", "info" + "退出失败" + code + message);
            }
        });
    }
    private void initTest(){
        if (Config.isLogin){
            userLoginOut.setVisibility(View.VISIBLE);
            //显示音频开关
            audioLayout.setVisibility(View.VISIBLE);
//            Log.i("设置页面退出登录按钮", "已登录"+Config.isLogin);
        } else {
            userLoginOut.setVisibility(View.GONE);
            //关闭音频开关
            audioLayout.setVisibility(View.GONE);
//            Log.i("设置页面退出登录按钮", "未登录"+Config.isLogin);
        }
        try {
            cacheView.setText(CleanMessageUtil.getTotalCacheSize(SettingActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //流量播放
        if ("trafficYes".equals(getTraffic())){
            trafficSb.setChecked(true);
        }else {
            trafficSb.setChecked(false);
        }
        //直播音频开启状态
        if (Config.AUDIO_STATE) {
            audioSb.setChecked(true);
        } else {
            audioSb.setChecked(false);
        }

    }

    public void back(View view) {
        finish();
    }
    /**
     * 重写返回键
     * 监听Back键按下事件
     * super.onBackPressed()会自动调用finish()方法,关闭当前Activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * 使用SharedPreferences保存流量播放
     * @param traffic
     */
    private void saveTraffic(String traffic){
        //获取SharedPreferences对象
        SharedPreferences.Editor editor = getSharedPreferences("data",MODE_PRIVATE).edit();
        //设置参数
        editor.putString("traffic",traffic);
        //提交
        editor.apply();
    }
    //获取是否允许流量播放
    private String getTraffic(){
        return getSharedPreferences("data",MODE_PRIVATE).getString("traffic","");
    }
}
