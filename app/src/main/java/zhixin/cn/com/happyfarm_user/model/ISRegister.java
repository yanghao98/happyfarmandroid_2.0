package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by DELL on 2018/3/27.
 */

public class ISRegister {


    /**
     * result : 用户已存在
     * flag : failed
     * errCode : -103
     */

    private String result;
    private String flag;
    private int errCode;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }
}
