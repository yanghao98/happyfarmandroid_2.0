package zhixin.cn.com.happyfarm_user.model;

public class ChickenCoopController {

    private ChickenCoopBean chickenCoopBean;
    private String flag;

    public ChickenCoopBean getChickenCoopBean() {
        return chickenCoopBean;
    }

    public void setChickenCoopBean(ChickenCoopBean chickenCoopBean) {
        this.chickenCoopBean = chickenCoopBean;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ChickenCoopBean{
        public Integer landNumber;
        public Integer type;
        public String ip;
        public Integer loopClose;

        public Integer getLandNumber() {
            return landNumber;
        }

        public void setLandNumber(Integer landNumber) {
            this.landNumber = landNumber;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public Integer getLoopClose() {
            return loopClose;
        }

        public void setLoopClose(Integer loopClose) {
            this.loopClose = loopClose;
        }
    }
}
