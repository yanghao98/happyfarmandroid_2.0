package zhixin.cn.com.happyfarm_user.model;

import com.google.gson.Gson;

public class Test {

    /**
     * result : 用户信息失效
     * flag : failed
     */

    private String result;
    private String flag;

    public static Test objectFromData(String str) {

        return new Gson().fromJson(str, Test.class);
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
