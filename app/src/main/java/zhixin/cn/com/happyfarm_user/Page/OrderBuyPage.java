package zhixin.cn.com.happyfarm_user.Page;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.OrderBuyPageAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.WaitingForGoodsActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.OrderBuy;
import zhixin.cn.com.happyfarm_user.model.searchOrderlogList;
import zhixin.cn.com.happyfarm_user.other.DateUtil;

/**
 * Created by Administrator on 2018/5/17.
 */

public class OrderBuyPage extends Fragment {
    private static final String TAG = "OrderBuyPage->";
    private GridLayoutManager gridLayoutManager;
    private RecyclerView recyclerView;
    private ArrayList<OrderBuy> list = new ArrayList<>();
    private OrderBuyPageAdapter adapter;
    private RelativeLayout orderBuyPrompt;
    private RefreshLayout refreshLayout;
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.order_buy_layout,null,false);
        pageNum = 1;
        pageSize = 15;
        orderBuyPrompt = view.findViewById(R.id.order_buy_prompt);
        refreshLayout = (RefreshLayout) view.findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
//        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()).setShowBezierWave(false));
        //设置 Footer 为 球脉冲 样式
        refreshLayout.setRefreshFooter(new ClassicsFooter(getContext()).setSpinnerStyle(SpinnerStyle.Scale));
        initRecyclerView(view);
//        StaggerLoadData(false);

        topRefreshLayout();
        bottomRefreshLayout();
        //自动加载
        recyclerViewButtom();
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        if (!list.isEmpty()){
            list.clear();
        }
        StaggerLoadData(false,1);
    }
    private void initRecyclerView(View v) {
        recyclerView = v.findViewById(R.id.order_buy_recycler);

        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new OrderBuyPageAdapter(getContext(), list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
    }

    private void StaggerLoadData(Boolean inversion, int pageNum) {
        dialog = LoadingDialog.createLoadingDialog(getContext(), "正在加载数据");
        HttpHelper.initHttpHelper().searchOrderlogListBuy(1,pageNum,pageSize,4).enqueue(new Callback<searchOrderlogList>() {
            @Override
            public void onResponse(Call<searchOrderlogList> call, Response<searchOrderlogList> response) {
                Log.i(TAG, "onResponse: "+ JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())){
                    isLastPage = response.body().getResult().isIsLastPage();
                    //if (String.valueOf(response.body().getResult().getList()).equals("[]"))
                    if (response.body().getResult().getList().isEmpty()){
                        orderBuyPrompt.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        orderBuyPrompt.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        for (int i = 0;i<response.body().getResult().getList().size();i++){
                            OrderBuy data = new OrderBuy();
                            data.buyAllTime = DateUtil.pointTime(String.valueOf(response.body().getResult().getList().get(i).getStartTime()));
                            data.buyAllImg = String.valueOf(response.body().getResult().getList().get(i).getOrderImg());
                            data.buyAllCropName = response.body().getResult().getList().get(i).getCropName();
//                            int num = response.body().getResult().getList().get(i).getNum();
                            data.buyAllNum = String.valueOf(response.body().getResult().getList().get(i).getClosing());
                            int fertilizer = response.body().getResult().getList().get(i).getIsOrganic();
                            if (fertilizer == 0){
                                data.buyAllFertilizer = "";
                            }else if (fertilizer == 1){
                                data.buyAllFertilizer = "(有机肥)";
                            }else {
                                data.buyAllFertilizer = "(无机肥)";
                            }
                            data.buyAllBuyName = response.body().getResult().getList().get(i).getSellerNickname();
                            data.buyAllOnePrice = String.valueOf(response.body().getResult().getList().get(i).getPrice());
                            data.buyConsuAmoutP = String.valueOf(response.body().getResult().getList().get(i).getFinalPrice());
                            int state = response.body().getResult().getList().get(i).getState();
                            if (0 == state){
                                data.buyViewDetail = "待发货";
                            }else if (1 == state){
                                data.buyViewDetail = "正在配送";
                            }else {
                                data.buyViewDetail = "查看提货码";
                            }
                            data.buyOrderNumber = response.body().getResult().getList().get(i).getOrderNum();
                            list.add(data);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }else {
                    Toast.makeText(getContext(),response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                    orderBuyPrompt.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(Call<searchOrderlogList> call, Throwable t) {
                orderBuyPrompt.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (!list.isEmpty())
                    list.clear();
                isLastPage = false;
                refreshlayout.setNoMoreData(false);//TODO 恢复没有更多数据的原始状态
                pageNum = 1;
                StaggerLoadData(false,1);
                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }
    public void recyclerViewButtom(){
        //上拉滑动自动请求数据
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                    if(lastVisiblePosition >= gridLayoutManager.getItemCount() - 1){
                        if (isLastPage == true) {
//                            Toast.makeText(getContext(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            StaggerLoadData(false, pageNum);

                        }
                    }
                }
            }
        });
    }
    //上拉加载
    public void bottomRefreshLayout() {
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("info", "上拉刷新: "+isLastPage);
                        if (isLastPage) {
                            Toast.makeText(getContext(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
                        } else {
//                            Toast.makeText(getApplication(), "数据加载完毕", Toast.LENGTH_SHORT).show();
                            pageNum++;
                            StaggerLoadData(false, pageNum);
//                            adapter.loadMore(initData());
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }
}
