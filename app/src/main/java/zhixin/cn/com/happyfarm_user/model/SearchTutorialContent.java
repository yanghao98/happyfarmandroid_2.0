package zhixin.cn.com.happyfarm_user.model;

public class SearchTutorialContent {


    /**
     * result : {"id":13,"tutorialName":"123","tutorialContent":"<p><em><strong>保持室温，湿度。<\/strong><\/em><\/p><p><em><strong>保持室温，湿度。<\/strong><\/em><\/p>","startTime":1.521785538E12,"keyword":"123","priority":1,"state":1,"updateTime":1.522290813E12,"managerId":1}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * id : 13
         * tutorialName : 123
         * tutorialContent : <p><em><strong>保持室温，湿度。</strong></em></p><p><em><strong>保持室温，湿度。</strong></em></p>
         * startTime : 1.521785538E12
         * keyword : 123
         * priority : 1
         * state : 1
         * updateTime : 1.522290813E12
         * managerId : 1
         */

        private int id;
        private String tutorialName;
        private String tutorialContent;
        private double startTime;
        private String keyword;
        private int priority;
        private int state;
        private double updateTime;
        private int managerId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTutorialName() {
            return tutorialName;
        }

        public void setTutorialName(String tutorialName) {
            this.tutorialName = tutorialName;
        }

        public String getTutorialContent() {
            return tutorialContent;
        }

        public void setTutorialContent(String tutorialContent) {
            this.tutorialContent = tutorialContent;
        }

        public double getStartTime() {
            return startTime;
        }

        public void setStartTime(double startTime) {
            this.startTime = startTime;
        }

        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public double getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(double updateTime) {
            this.updateTime = updateTime;
        }

        public int getManagerId() {
            return managerId;
        }

        public void setManagerId(int managerId) {
            this.managerId = managerId;
        }
    }
}
