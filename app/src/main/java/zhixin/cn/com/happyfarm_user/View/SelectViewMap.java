package zhixin.cn.com.happyfarm_user.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.model.NewPoint;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;

public class SelectViewMap extends View {

    private Paint myPaint;
    private Paint otherPaint;
    private Paint poultryPaint;
    private Paint availablePaint;
    private Paint unAvailablePaint;
    private Paint cursorPaint;

    private int pointW = (int) dp2Px(5);
    private int space = (int) dp2Px(1);
    private int cursorW = 0;
    private int cursorH = 0;
    private int cursorW_s = 0;
    private int cursorH_s = 0;
    private float scale = 0; //view 与map 的比例
    private int width = 0;
    private int height = 0;

    private Point cursorPoint = new Point(0, 0);

    private List<NewPoint> values = new ArrayList<>();

    public SelectViewMap(Context context) {
        super(context);
    }

    public SelectViewMap(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        //小地图我的租地
        myPaint = new Paint();
        myPaint.setColor(0xFFFDBF76);
        //小地图可选租地
        availablePaint = new Paint();
        availablePaint.setColor(0xFF1DAC6D);
        //小地图禁用租地
        unAvailablePaint = new Paint();
        unAvailablePaint.setColor(0xFFAAAAAA);
        //小地图他人租地
        otherPaint = new Paint();
        otherPaint.setColor(0xFFF39199);
        //小地图家禽租地
        poultryPaint = new Paint();
        poultryPaint.setColor(0xFFD282D2);//0xFFD282D2 Color.rgb(210, 130, 210) （透明）00 –> FF（不透明）
        cursorPaint = new Paint();
        cursorPaint.setColor(0xFFAAAAAA);
//        cursorPaint.setColor(Color.RED);
        //设置外边框的宽度
        cursorPaint.setStrokeWidth(DensityUtil.dp2px(getContext(), 1.3f));
        //空心
        cursorPaint.setStyle(Paint.Style.STROKE);

    }

    /**
     * 绘画左上角租地列表小地图
     * @param values
     * @param scrollViewW
     * @param scrollViewH
     * @param minX 最小x坐標
     * @param maxX 最大x坐標
     * @param minY 最小y坐標
     * @param maxY 最大y坐標
     */
    public void setValues(List<NewPoint> values,int scrollViewW,int scrollViewH,int minX, int maxX, int minY, int maxY) {
        this.values.clear();
        int xNum = maxX - minX + 1;
        int yNum = maxY - minY + 1;

        cursorW_s = (int) ((float)scrollViewW / (DensityUtil.dp2px(getContext(),45) * xNum) * DensityUtil.dp2px(getContext(),240));
        cursorH_s = (int) ((float)scrollViewH / (DensityUtil.dp2px(getContext(),45) * yNum) * DensityUtil.dp2px(getContext(),140));
        //cursorH_s = (int)(cursorW_s * (scrollViewH * 1f / scrollViewW));
//        pointW = w * 5 / 6;
//        space =  w / 6;
        width = xNum * (pointW + space) + space;
        height = yNum * (pointW + space) + space;
        scale = DensityUtil.dp2px(getContext(),45) / (width / xNum);
        for (NewPoint point : values) {
            NewPoint point1 = new NewPoint();
            int xc = point.getLand().getX() * (pointW + space) + space - minX * (pointW + space);
            int yc = point.getLand().getY() * (pointW + space) + space - minY * (pointW + space);
            point1.setType(point.getType());
            point1.setxC(xc);
            point1.setyC(yc);
            this.values.add(point1);
        }

        requestLayout();
    }


    /**
     * 在滑動的時候會調用
     * @param x
     * @param y
     * @param sc
     */
    public void setCursorPaint(int x, int y, float sc) {
        cursorPoint = new Point((int)(x/scale/sc),(int)(y/scale/sc));
        cursorW = (int) (cursorW_s / sc);
        cursorH = (int) (cursorH_s / sc);
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for (NewPoint p : this.values) {
            Paint paint = null;
            if (p.getType() == SelectView.Available) {
                paint = availablePaint;
            } else if (p.getType() == SelectView.UnAvailable) {
                paint = unAvailablePaint;
            } else if (p.getType() == SelectView.My) {
                paint = myPaint;
            } else if (p.getType() == SelectView.Other) {
                paint = otherPaint;
            } else if (p.getType() == SelectView.Poultry) {
                paint = poultryPaint;
            }
            if (paint != null) {
                canvas.drawRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, paint);
            }
        }
        canvas.drawRect(cursorPoint.x, cursorPoint.y, cursorPoint.x + cursorW, cursorPoint.y + cursorH, cursorPaint);
    }

    private float dp2Px(int dp) {
        float density = getContext().getResources().getDisplayMetrics().density;
        return (float) (dp * density + 0.5);
    }
}
