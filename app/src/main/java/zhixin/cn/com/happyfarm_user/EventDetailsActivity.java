package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.CreateActivity;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.MyWebChromeClient;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.RoundImageView;
import zhixin.cn.com.happyfarm_user.utils.Utils;
import zhixin.cn.com.happyfarm_user.zxing.activity.CaptureActivity;

import static zhixin.cn.com.happyfarm_user.utils.CreateQRCode.createQRCodeBitmap;

public class EventDetailsActivity extends AppCompatActivity {
    private Button perfectNext;
    private Button openQCCode;
    private ImageView ZXingIcon;
    private RelativeLayout ZXingLayout;
    private ImageView OnZXingLayout;
//    private Button open_camera;
    private String TAG = "EventDetailsActivity";
    private int width = 800;
    private int height = 800;
    private String character_set = "UTF-8";
    private String error_correction_level = "H";
    private String margin = "1";
    private float logoPercent = 0.2F;
    private WebView webView;
    private ProgressBar progressBar;
    private RoundImageView activityImage;
    private TextView activityName;
    private TextView activityTime;
    private TextView activityAddres;
    private Dialog dialog;
    private boolean TokenIsOK = false;
    private int activityId = 0;
    private Map<String,Object> order = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_details_activity);
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        userInfoTokenTest();
        initView();
        selectCreateActivity();
    }

    private void initView() {
//        Glide.with(EventDetailsActivity.this)
//                .load("")
//                .into(new SimpleTarget<Bitmap>() {
//                    @Override
//                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                        mBitmapCover = resource;
//                    }
//                })
        perfectNext = findViewById(R.id.perfect_next);
        openQCCode = findViewById(R.id.open_QC_code);
        ZXingIcon = findViewById(R.id.ZXing_icon);
        ZXingLayout = findViewById(R.id.ZXing_layout);
        OnZXingLayout = findViewById(R.id.on_ZXing_layout);
//        open_camera = findViewById(R.id.open_camera);
//        String obj = "<html><head><meta charset=\"utf-8\"></head><body><p>1.植信网会对中央门周边地区的用户派发“邀请券”。</p><p>2.每个用户凭“邀请券”可到新模范马路店换取一份免费礼品。</p><p><img style=\"display: inline-block;  max-width: 100%;height: 230px; \" src=\"http://img.trustwusee.com/storeAddress.png\" alt=\"\"/></p></body></html>";
        progressBar = findViewById(R.id.progressBar2);
        webView = findViewById(R.id.webView2);
        webView.getSettings().setJavaScriptEnabled(true);//启用js
        /**************************关于图片不能显示暂时加的内容***************************/
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // TODO Auto-generated method stub
                // handler.cancel();// Android默认的处理方式
                handler.proceed();// 接受所有网站的证书
                // handleMessage(Message msg);// 进行其他处理
            }
        });
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(
                    WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }
        webView.getSettings().setBlockNetworkImage(false);//解决图片不显示
        webView.clearCache(true); //加载的之前调用即可
        /**************************关于图片不能显示暂时加的内容***************************/

        activityImage = findViewById(R.id.activity_image);
        activityName = findViewById(R.id.activity_name);
        activityTime = findViewById(R.id.activity_time);
        activityAddres = findViewById(R.id.activity_addres);
        setOnClick();
    }

    private int getUserId(){
        int userId;
        if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }

    public void setOnClick() {
        openQCCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap qrCodeBitmap = createQRCodeBitmap(JSON.toJSONString(order), width, height, character_set, error_correction_level, margin, Color.BLACK, Color.WHITE, BitmapFactory.decodeResource(getResources(), R.mipmap.logo, null), logoPercent);
                ZXingIcon.setImageBitmap(qrCodeBitmap);
                ZXingLayout.setVisibility(View.VISIBLE);
            }
        });

        perfectNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TokenIsOK) {
                    addParticipationActivity();
                } else {
                    Toast.makeText(EventDetailsActivity.this,"身份信息失效，请重新登录",Toast.LENGTH_SHORT).show();
                }
            }
        });


        OnZXingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ZXingLayout.setVisibility(View.GONE);
            }
        });

        //开启扫码摄像头
//        open_camera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent openCameraIntent = new Intent(EventDetailsActivity.this, CaptureActivity.class);
//                startActivityForResult(openCameraIntent, 0);
//            }
//        });
    }

    /**
     * 查询活动内容
     */
    private void selectCreateActivity() {
        dialog = LoadingDialog.createLoadingDialog(EventDetailsActivity.this, "正在加载数据");
        HttpHelper.initHttpHelper().selectCreateActivity().enqueue(new Callback<CreateActivity>() {
            @Override
            public void onResponse(Call<CreateActivity> call, Response<CreateActivity> response) {
                Log.i(TAG, "onResponse: " + JSON.toJSONString(response.body().getResult()));
                if ("success".equals(response.body().getFlag())) {
                    activityId = response.body().getResult().getId();
                    order.put("userId", getUserId());
                    order.put("activityId",activityId);
                    activityImage.setImageURL(response.body().getResult().getActivityImage());
                    activityName.setText(response.body().getResult().getActivityName());
                    activityTime.setText("活动时间：" + response.body().getResult().getActivityTime());
                    activityAddres.setText("活动地点：" + response.body().getResult().getActivityAddres());
                    webView.loadDataWithBaseURL(null,response.body().getResult().getActivityContent(),"text/html","UTF-8",null);
//                webView.loadDataWithBaseURL(null,obj,"text/html","UTF-8",null);
                    //开启缓存
                    webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                    //判断页面加载过程
                    webView.setWebChromeClient(new MyWebChromeClient() {
                        @Override
                        public void onProgressChanged(WebView view, int newProgress) {
                            // TODO Auto-generated method stub
                            if (newProgress == 100) {
                                // 网页加载完成
                                progressBar.setVisibility(View.GONE);//加载完网页进度条消失
                            } else {
                                // 加载中
                                progressBar.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                                progressBar.setProgress(newProgress);//设置进度值
                            }

                        }

                    });
//                    perfectNext.setVisibility(View.GONE);
//                    openQCCode.setVisibility(View.VISIBLE);
                }
                if (-1 == response.body().getMessage()) {
                    perfectNext.setVisibility(View.VISIBLE);
                    openQCCode.setVisibility(View.GONE);
                } else if (0 == response.body().getMessage()){
                    perfectNext.setVisibility(View.GONE);
                    openQCCode.setVisibility(View.VISIBLE);
                } else if (1 == response.body().getMessage()) {
                    perfectNext.setVisibility(View.GONE);
                    openQCCode.setVisibility(View.GONE);
                    findViewById(R.id.ok_button).setVisibility(View.VISIBLE);
                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(Call<CreateActivity> call, Throwable t) {
                LoadingDialog.closeDialog(dialog);
            }
        });
    }

    /**
     * 参加活动
     */

    private void addParticipationActivity() {
        dialog = LoadingDialog.createLoadingDialog(EventDetailsActivity.this, "正在加载数据");
        if (0 != activityId) {
            HttpHelper.initHttpHelper().addParticipationActivity(activityId).enqueue(new Callback<CheckSMS>() {
                @Override
                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                    Log.i(TAG, "onResponse: " + JSON.toJSONString(response.body()));
                    if ("success".equals(response.body().getFlag())) {
                        Toast.makeText(EventDetailsActivity.this,response.body().getResult(),Toast.LENGTH_SHORT).show();
                        LoadingDialog.closeDialog(dialog);
                        perfectNext.setVisibility(View.GONE);
                        openQCCode.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<CheckSMS> call, Throwable t) {
                    LoadingDialog.closeDialog(dialog);
                }
            });
        }

    }

    /**
     * 验证token是否有效
     */
    public  void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
                System.out.println("###"+ JSON.toJSONString(response.body()));
                if (("failed").equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    if ("用户信息失效".equals(response.body().getResult())){
//                        myView.findViewById(R.id.vip_tabLayout_content).setVisibility(View.GONE);
                    }
                    TokenIsOK = false;
                } else if (("success").equals(response.body().getFlag())){
                    TokenIsOK = true;
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                TokenIsOK = false;
            }
        });
    }

    public void back(View view) {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //处理扫描结果（在界面上显示）
        if (resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            String scanResult = bundle.getString("result");
            Toast.makeText(this, "二维码内容："+scanResult, Toast.LENGTH_SHORT).show();
        }
    }
}
