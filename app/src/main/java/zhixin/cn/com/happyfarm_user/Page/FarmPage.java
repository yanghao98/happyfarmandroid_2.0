package zhixin.cn.com.happyfarm_user.Page;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.view.menu.MenuBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hyphenate.EMCallBack;
import com.hyphenate.EMClientListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.easeui.model.EaseAtMessageHelper;

import java.util.ArrayList;
import java.util.List;

import me.kareluo.ui.OptionMenu;
import me.kareluo.ui.OptionMenuView;
import me.kareluo.ui.PopupMenuView;
import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.ViewPagerAdapter;
//import zhixin.cn.com.happyfarm_user.HMSPushHelper;
import zhixin.cn.com.happyfarm_user.MainActivity;
import zhixin.cn.com.happyfarm_user.ManageAddress2Activity;
import zhixin.cn.com.happyfarm_user.PersonalMessageActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.LandVideoActivity;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.MyCollectionActivity;
import zhixin.cn.com.happyfarm_user.MyDynamicActivity;
import zhixin.cn.com.happyfarm_user.PerfectInformationActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.View.OmnidirectionalScrollView;
import zhixin.cn.com.happyfarm_user.View.SelectView;
import zhixin.cn.com.happyfarm_user.View.SelectViewMap;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.easyMob.ConverActivity;
import zhixin.cn.com.happyfarm_user.model.AllLand;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.LuckDraw;
import zhixin.cn.com.happyfarm_user.model.NewPoint;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.popupMenu.PopupMenu;
import zhixin.cn.com.happyfarm_user.popupMenu.adapter.PopupMenuAdapter;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.CustomToast;
import zhixin.cn.com.happyfarm_user.utils.GuideView;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

import static android.content.Context.MODE_PRIVATE;


public class FarmPage extends Fragment {

    private char Available = 'A';
    private char Other = 'O';
    private char My = 'M';
    private char Poultry = 'P';//家禽

    private String LandNos;
    private String LandNo;
    private String LandId;
    private String landId;
    private String isLandlord = "";
    private int superUser;
    private String UserName;
    private String UserId;
    private String LeaseTime;
    private int leaseTerm;
    private String ExpirationTime;
    private String UserNickName;
    private String UserImg;
    private String UserTel;
    private String UserCertificatesNo;
    private String UserRealname;
    private int UserPlotId;
    private String UserPlotName;
    private int UserGender;
    private long UserBirthday;
    private String OtherUserImg;
    private String OthersTel;
    private String UserEmail;
    private int landType;
    private static AllLand allLand;
    private String alias;

    private SelectView mSelectView;
//    private SelectViewMap mSelectViewMap;
    private OmnidirectionalScrollView omnidirectionalScrollView;
    private ImageView toolbar;
    private PopupMenuView mPopupMenuView;
    private Dialog mDialog;

    private long delayMillis = 100;
    private long lastScrollUpdate = -1;
    private Runnable scrollerTask;
    private ImageView homeImgRefresh;
    private ProgressBar progressBar;
//    private RelativeLayout titleBarLayout;
    private Activity activity;
//    private ImageView massageButton;
//    private Badge messageBadge;
    private View myView;
    //蒙层使用
    private GuideView guideView;

    private String TAG = "FarmPage";
    private ImageView backButton;

    private TextView landLocation;
    private TextView landTopNumber;
    private TextView landTopTypeText;
    private TextView landTopState;

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.farm_layout, null, false);
//        titleBarLayout = view.findViewById(R.id.farm_title_layout);
        myView = view;
        //返回按钮需要特殊对待（添加晚了点击返回按钮会奔溃）
        backButton = view.findViewById(R.id.back_button);
//        searchLuckDraw();
//        setStatusBarHeight();
        // 根据menu资源文件创建
        //mPopupMenuView = new PopupMenuView(activity R.menu.menu, new MenuBuilder(activity));
        mPopupMenuView = new PopupMenuView(activity, R.layout.layout_custom_menu);
        mPopupMenuView.inflate(R.menu.menu, new MenuBuilder(activity));
        //显示样式（纵向）
        mPopupMenuView.setOrientation(LinearLayout.VERTICAL);
        //显示样式（横向）mPopupMenuView.setOrientation(LinearLayout.HORIZONTAL);
        toolbar = view.findViewById(R.id.home_toolbar);
        homeImgRefresh = view.findViewById(R.id.home_img_refresh);

        setHasOptionsMenu(true);
        userInfoTokenTest();
//        setToolbar(toolbar);

        mSelectView = view.findViewById(R.id.selectView);
//        mSelectViewMap = view.findViewById(R.id.select_view_map);
        omnidirectionalScrollView = view.findViewById(R.id.select_scroll_view);
        progressBar = view.findViewById(R.id.farm_progress_bar);
//        massageButton = view.findViewById(R.id.message_icon);
        //菜单按钮
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopBottom();
            }
        });
        //刷新按钮
        homeImgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastClick()) {
                    // 进行点击事件后的逻辑操作
                    searchAllLand(1);
                }
            }
        });
        //返回主页面
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                System.out.println("返回主页面");
                Intent  intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
//        //跳转消息页面按钮
//        massageButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.i(TAG, "onClick: 加载消息页面");
//                Intent intent = new Intent(getActivity(), PersonalMessageActivity.class);
//                startActivity(intent);
//
//            }
//        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            omnidirectionalScrollView.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
//                Log.i("TAG", String.format("x:%d\ty:%d,l:%d\tt:%d", scrollX, scrollY, omnidirectionalScrollView.leftW, omnidirectionalScrollView.leftH));
//                mSelectViewMap.setCursorPaint(scrollX + omnidirectionalScrollView.leftW, scrollY + omnidirectionalScrollView.leftH, mSelectView.getScaleX());
                if (lastScrollUpdate == -1) {
//                    scrollerStart();
                }
                lastScrollUpdate = System.currentTimeMillis();
            });
        }

        scrollerTask = () -> {
            if (System.currentTimeMillis() - lastScrollUpdate > delayMillis) {
//                scrollerStop();
                lastScrollUpdate = -1;
            } else {
                postDelayed(scrollerTask, delayMillis);
            }
        };


        mSelectView.setSelectListener(point -> {

            Log.e("FarmPage ->", "landInFo:" + JSON.toJSONString(point.getLand()));
            LandNos = String.valueOf(point.getLand().getLandNo());
            UserName = point.getLand().getUserName();
            OthersTel = point.getLand().getTel();
            UserId = String.valueOf(point.getLand().getUserId());
            landId = String.valueOf(point.getLand().getId());
            alias = point.getLand().getAlias();
            //point.getLand().getIsHosted();
            LeaseTime = DateUtil.getStrTime(String.valueOf(point.getLand().getLeaseTime()));
            leaseTerm = point.getLand().getLeaseTerm();
            ExpirationTime = DateUtil.subMonth(LeaseTime, leaseTerm);
            OtherUserImg = point.getLand().getHeadImg();
            landType = point.getLand().getLandType();
//            ExpirationTime  = stampToDate(String.valueOf(point.getLand().getExpirationTime()));

            if (point.getType() == Available) {
                showDialog1(LandNos, landType,alias);
            } else if (point.getType() == Other) {
                UserName = point.getLand().getUserName();
                searchLandById(Integer.parseInt(landId), UserName, UserId,LandNos, LeaseTime, ExpirationTime, OtherUserImg, OthersTel, landType);
            } else if (point.getType() == My) {
                showDialog3(LandNos, landType,alias);
            } else if (point.getType() == Poultry) {
                showDialog1(LandNos, landType,alias);
            }
        });
//        messageBadge = new QBadgeView(getContext())
//                .bindTarget(myView.findViewById(R.id.message_icon_view_total))//获取需要显示的位置
//                .setBadgeNumber(0)
//                .setBadgeTextSize(7, true);
        //TODO 判断当前是否有账号登录
        if (!EMClient.getInstance().isLoggedInBefore() || "isOneLogin".equals(Config.isOneLogin)){
            Config.isOneLogin = "noOneLogin";
//                LoginActivity.loginEM();
//            System.out.println("首页页面登录环信");
            //登录环信
            loginEM();
        }

        landLocation = view.findViewById(R.id.land_location);
        landTopNumber = view.findViewById(R.id.land_top_number);
        landTopTypeText = view.findViewById(R.id.land_top_type_text);
        landTopState = view.findViewById(R.id.land_top_state);
        return view;
    }


//    /**
//     * 设置消息显示数量
//     * @param num 消息数量
//     * @param position 设置显示tablayout的item 暂时未时候，考虑后期可能所有的item都需要
//     */
//    private void setMsgNum(int position, int num){
//        if (messageBadge != null){
////            Log.i(TAG, "消息设置成功setMsgNum: "+num);
//            messageBadge.setBadgeNumber(num);
//        }else {
////            Log.i(TAG, "消息设置失败setMsgNum: "+num);
//        }
//    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

//    /**
//     * 设置导航栏距离顶部的距离
//     */
//    private void setStatusBarHeight() {
//        //设置按钮偏移位置(相对于状态栏的top)
//        int statusBarHeight = getStatusBarHeight(activity);
//        //相对布局使用方法
////        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
////        params.setMargins(0, statusBarHeight, 0, 0);
////        titleBarLayout.setLayoutParams(params);
//        //线性布局使用方法
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
//    }

    /**
     * 获取状态栏高度
     *
     * @param context 上下文
     * @return int 状态栏高度值
     */
    protected static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    private Handler handler = new Handler();

    private void postDelayed(Runnable scrollerTask, long delayMillis) {
        handler.postDelayed(scrollerTask, delayMillis);
    }

//    private void scrollerStart() {
//        postDelayed(scrollerTask, delayMillis);
//        ObjectAnimator animator = ObjectAnimator.ofFloat(mSelectViewMap, "alpha", 0f, 1f);
//        animator.setDuration(500);//时间1s
//        if (mSelectViewMap.getAlpha() == 0) {
//            animator.start();
//        }
//    }
//
//    private void scrollerStop() {
//        handler.postDelayed(() -> {
//            ObjectAnimator animator = ObjectAnimator.ofFloat(mSelectViewMap, "alpha", 1f, 0f);
//            animator.setDuration(500);
//            if (mSelectViewMap.getAlpha() == 1) {
//                animator.start();
//            }
//        }, 1000);
//    }

    @Override
    public void onStart() {
        super.onStart();
        //主要用于刚进来获取用户信息，好做传值
        getUserInfo();

    }

    @Override
    public void onResume() {
        super.onResume();
        searchAllLand(2);
        if ("".equals(getToken())){
//            setMsgNum(2,0);
        }else {
            int total = EMClient.getInstance().chatManager().getUnreadMessageCount();
//            setMsgNum(2,total);
        }
    }

    private String getToken(){
        return App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("token","");
    }


    private void setSelectViewValue() {
        List<NewPoint> points = new ArrayList<>();
        for (AllLand.ResultBean.ListBean point : allLand.getResult().getList()) {
            char type = 0;
            if (1 == superUser) {
                if (point.getState() == 0) {
                    //禁用租地
                    type = SelectView.UnAvailable;
                } else {
                    type = SelectView.My;
                }
            } else {
                Log.i(TAG, point.getUserId() + "setSelectViewValue: " + getUserId());
                if (point.getState() == 0) {
                    //禁用租地
                    type = SelectView.UnAvailable;
                } else if (point.getUserId() == 0) {
                    //未出租租地
                    if (1 == point.getLandType()) {
                        //蔬菜租地
                        type = SelectView.Available;
                    } else if (2 == point.getLandType()) {
                        //家禽租地
                        type = SelectView.Poultry;
                    }
                } else if (point.getUserId() == getUserId()) {
                    //我的租地
                    type = SelectView.My;
                } else {
                    //userId为其他时
                    //他人租地
                    type = SelectView.Other;
                }
            }

            points.add(new NewPoint(type, point));
        }
        //地块最小x坐标
        int minXInfo = allLand.getResult().getInfo().getMinX();
        int maxXInfo = allLand.getResult().getInfo().getMaxX();
        //地块最小y坐标
        int minYInfo = allLand.getResult().getInfo().getMinY();
        int maxYInfo = allLand.getResult().getInfo().getMaxY();
        //大地圖
        mSelectView.setValues(points, minXInfo, maxXInfo, minYInfo, maxYInfo);
        //小地圖
//        mSelectViewMap.setValues(points, omnidirectionalScrollView.getWidth(), omnidirectionalScrollView.getHeight(), minXInfo, maxXInfo, minYInfo, maxYInfo);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }

    /**
     * 空地弹框
     *
     * @param LandNo   地號
     * @param landType 租地類型
     */
    private void showDialog1(String LandNo, int landType, String alias) {
        //用于上面地块信息展示
        landTopNumber.setText(alias + "地块");
        landLocation.setText(alias);
        if (landType == 1) {
            landTopTypeText.setText("种类：蔬菜租地");
            landLocation.setBackgroundResource(R.drawable.land_list_label_available);
        } else {
            landTopTypeText.setText("种类：家禽租地");
            landLocation.setBackgroundResource(R.drawable.land_list_label_available2);
        }
        landTopState.setText("状态：未租");

        //弹框内容
        Dialog bottomDialog = new Dialog(activity, R.style.BottomDialog);
        bottomDialog.setOnCancelListener(dialog -> {
            this.mSelectView.unSelect();
        });
        View contentView = LayoutInflater.from(activity).inflate(R.layout.layout_seelease_dialog, null);
        TextView landNo = contentView.findViewById(R.id.landNo);
        landNo.setText(alias);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(activity, 0);
        params.bottomMargin = DensityUtil.dp2px(activity, 0);
        contentView.findViewById(R.id.camera_control_see).setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                Intent intent = new Intent(activity, LandVideoActivity.class);
                intent.putExtra("state", "1");
                intent.putExtra("landNo", LandNo);
                intent.putExtra("landId", landId);
                intent.putExtra("alias",alias);
                intent.putExtra("UserId", "0");
                intent.putExtra("landType", landType + "");
                startActivity(intent);
                bottomDialog.cancel();
            }
        });
        contentView.findViewById(R.id.camera_control_determine).setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                mDialog = LoadingDialog.createLoadingDialog(activity, "");
//                Toast.makeText(getContext(),"如需租地，请联系客服！",Toast.LENGTH_SHORT).show();
                getSelfInfo();
            }
        });
        contentView.findViewById(R.id.close_text).setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    /**
     * 他人租地 弹框
     *
     * @param LandNo         租地编号
     * @param UserName       用户名
     * @param UserId         用户id
     * @param landids        租地id
     * @param LeaseTime      租期开始时间
     * @param ExpirationTime 租期结束时间
     * @param OtherUserImg   头像
     * @param OthersTel      手机号
     * @param landType       租地类型
     */
    private void showDialog2(String LandNo, String UserName, String UserId, String landids,
                             String LeaseTime, String ExpirationTime, String OtherUserImg,
                             String OthersTel, int landType,String corpName,String alias) {

        //用于上面地块信息展示
        landTopNumber.setText(alias + "地块");
        landLocation.setText(alias);
        if (landType == 1) {
            landTopTypeText.setText("种类：蔬菜租地");
            landLocation.setBackgroundResource(R.drawable.land_list_label_available);
        } else {
            landTopTypeText.setText("种类：家禽租地");
            landLocation.setBackgroundResource(R.drawable.land_list_label_available2);
        }
        landTopState.setText("状态：已租");

        //弹框内容
        Dialog bottomDialog = new Dialog(activity, R.style.BottomDialog);
        bottomDialog.setOnCancelListener(dialog -> {
            this.mSelectView.unSelect();
        });

        View contentView = LayoutInflater.from(activity).inflate(R.layout.layout_otherslease_dialog, null);
        bottomDialog.setContentView(contentView);
        TextView landNo = contentView.findViewById(R.id.user_land);
        TextView UserNames = contentView.findViewById(R.id.userName);
        TextView leaseTime = contentView.findViewById(R.id.leaseTime);
        TextView expirationTime = contentView.findViewById(R.id.expirationTime);
        TextView productType = contentView.findViewById(R.id.product_type);
        productType.setText(corpName);
        landNo.setText(alias);
        UserNames.setText(UserName);
        leaseTime.setText(LeaseTime);
        expirationTime.setText(ExpirationTime);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(activity, 0);
        params.bottomMargin = DensityUtil.dp2px(activity, 0);
        contentView.findViewById(R.id.others_bt).setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                Intent intent = new Intent(activity, LandVideoActivity.class);
                intent.putExtra("UserName", UserName);
                intent.putExtra("UserId", UserId);
                intent.putExtra("landNo", LandNo);
                intent.putExtra("landId", landids);
                intent.putExtra("LeaseTime", LeaseTime);
                intent.putExtra("ExpirationTime", ExpirationTime);
                intent.putExtra("OtherUserImg", OtherUserImg);
                intent.putExtra("OthersTel", OthersTel);
                intent.putExtra("state", "2");
                intent.putExtra("landType", landType + "");
                startActivity(intent);
                bottomDialog.cancel();
            }
        });
        contentView.findViewById(R.id.otherslease_bt).setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();

    }

    /**
     * TODO 我的租地弹框
     *
     * @param landNo   地号
     * @param landType 租地类型
     */
    private void showDialog3(String landNo, int landType,String alias) {
        //用于上面地块信息展示
        landTopNumber.setText(alias + "地块");
        landLocation.setText(alias);
        if (landType == 1) {
            landTopTypeText.setText("种类：蔬菜租地");
            landLocation.setBackgroundResource(R.drawable.land_list_label_available);
        } else {
            landTopTypeText.setText("种类：家禽租地");
            landLocation.setBackgroundResource(R.drawable.land_list_label_available2);
        }
        landTopState.setText("状态：自己租地");

        //弹框内容
        Dialog bottomDialog = new Dialog(activity, R.style.BottomDialog);
        View contentView = LayoutInflater.from(activity).inflate(R.layout.layout_my_dialog, null);
        TextView LandNo = contentView.findViewById(R.id.landNo);
        bottomDialog.setOnCancelListener(dialog -> {
            this.mSelectView.unSelect();
        });
        LandNo.setText(alias);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(activity, 0);
        params.bottomMargin = DensityUtil.dp2px(activity, 0);
        contentView.findViewById(R.id.my_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("landId", landId);
                Intent intent = new Intent(activity, LandVideoActivity.class);
                intent.putExtra("state", "0");
                intent.putExtra("landNo", landNo);
                intent.putExtra("landId", landId);
                intent.putExtra("UserId", "0");
                intent.putExtra("isLandlord", isLandlord);
                intent.putExtra("superUser", superUser + "");
                intent.putExtra("landType", landType + "");
                startActivity(intent);
                bottomDialog.cancel();
            }
        });
        contentView.findViewById(R.id.my_imgbt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.cancel();
            }
        });
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();

    }

    //完善用户信息layout弹窗
    private void showUserInfoDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(activity);
        diyDialog.setCancel("取消")
                .setOk(TextString.InfoTitleText)
                .setContent(TextString.InfoContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        Intent intent = new Intent(activity, PerfectInformationActivity.class);
                        intent.putExtra("nickName", UserNickName);
                        intent.putExtra("userImg", UserImg);
                        intent.putExtra("UserRealname", UserRealname);
                        intent.putExtra("landNo", LandNos);
                        intent.putExtra("landId", landId);
                        intent.putExtra("tel", UserTel);
                        intent.putExtra("UserPlotName", UserPlotName);
                        intent.putExtra("UserCertificatesNo", UserCertificatesNo);
                        intent.putExtra("UserPlotId", UserPlotId + "");
                        intent.putExtra("UserGender", UserGender + "");
                        intent.putExtra("UserBirthday", UserBirthday + "");
                        intent.putExtra("UserEmail", UserEmail + "");
                        intent.putExtra("landType", landType + "");
//                        System.out.println("用户全部信息打印：" + UserCertificatesNo + UserNickName + UserImg + UserName + LandNos + UserTel + UserName + UserPlotId + UserPlotName + UserRealname);
                        startActivity(intent);
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    private String tel;
    private String realname;

    public void getSelfInfo() {
        //获取用户信息是否为地主
        retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
//                System.out.println(JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    LoadingDialog.closeDialog(mDialog);
                    UserNickName = response.body().getResult().getNickname();
                    UserImg = response.body().getResult().getHeadImg();
                    tel = response.body().getResult().getTel();
                    realname = response.body().getResult().getRealname();
                    //判断是否是地主。
                    if (1 == response.body().getResult().getIsLandlord()) {
                        Toast.makeText(activity, "您已是地主，您的地在等待您养护哦", Toast.LENGTH_SHORT).show();
                    } else {
                        if (NumUtil.checkNull(response.body().getResult().getRealname())) {
                            showUserInfoDialog();
                        } else if (NumUtil.checkNull(response.body().getResult().getCertificatesNo())) {
                            showUserInfoDialog();
                        } else if (NumUtil.checkNull(response.body().getResult().getPlotId())) {
                            showUserInfoDialog();
                        } else if (response.body().getResult().getGender() == 0) {
                            showUserInfoDialog();
                        } else if (NumUtil.checkNull(response.body().getResult().getBirthday())) {
                            showUserInfoDialog();
                        } else if (NumUtil.checkNull(response.body().getResult().getTel())) {
                            showUserInfoDialog();
                        } else if (NumUtil.checkNull(response.body().getResult().getEmail())) {
                            showUserInfoDialog();
                        } else {
                            interceptDialog();
                        }
                    }
                } else {
                    LoadingDialog.closeDialog(mDialog);
                    Toast.makeText(activity, response.body().getResult().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                LoadingDialog.closeDialog(mDialog);
                try {
                    if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                        noLoginDialog();
                    } else {
                        CustomToast.showToast(activity, TextString.NetworkRequestFailed);
                    }
                } catch (Exception e) {
                    Log.e("FarmPage->", "onFailure: " + e);
                }
            }
        });
    }

    /**
     * 租地拦截弹框
     * 由于业务限制，需要租地时告诉用户目前只对南京开放
     */
    private void interceptDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(activity);
        diyDialog.setCancel("取消")
                .setOk(getResources().getString(R.string.intercept_ok))
                .setContent(getResources().getString(R.string.intercept_content))
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }

                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        //跳转确认收货地址页面
                        Intent intent = new Intent(activity, ManageAddress2Activity.class);
                        intent.putExtra("tel", tel);
                        intent.putExtra("UserRealname", realname);
                        intent.putExtra("landNo", LandNos);
                        intent.putExtra("landId", landId);
                        intent.putExtra("landType", landType + "");
                        startActivity(intent);
//                        //跳转支付页面
//                        Intent intent = new Intent(activity, PaymentInformationActivity.class);
//                        intent.putExtra("landNo", LandNos);
//                        intent.putExtra("landId", landId);
//                        intent.putExtra("landType", landType + "");
//                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    /**
     * 获取用户信息
     */
    public void getUserInfo() {
        //获取用户信息是否为地主
        retrofit2.Call<GetSelfInfo> getSelfInfo = HttpHelper.initHttpHelper().getSelfInfo();
        getSelfInfo.enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(retrofit2.Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                Log.e("state", response.body().getFlag());
                if (("success").equals(response.body().getFlag())) {
                    UserNickName = response.body().getResult().getNickname();
                    isLandlord = String.valueOf(response.body().getResult().getIsLandlord());
                    superUser = response.body().getResult().getSuperUser();
                    Log.i("superUser", "onResponse: " + superUser);
                    UserImg = response.body().getResult().getHeadImg();
                    UserTel = response.body().getResult().getTel();
                    UserCertificatesNo = response.body().getResult().getCertificatesNo();
                    UserRealname = response.body().getResult().getRealname();
                    UserPlotId = response.body().getResult().getPlotId();
                    UserPlotName = response.body().getResult().getPlotName();
                    UserGender = response.body().getResult().getGender();
                    UserBirthday = response.body().getResult().getBirthday();
                    UserEmail = response.body().getResult().getEmail();
                } else {
                    superUser = 0;
                    isLandlord = "0";
//                    Toast.makeText(activity,"网络错误",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GetSelfInfo> call, Throwable t) {
                superUser = 0;
                isLandlord = "0";
//                Toast.makeText(activity, "网络连接失败,请检查一下网络哦~", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private PopupMenu popupMenu;

    //点击菜单图片显示
    private void showPopBottom() {
//        popupMenu1();
        popupMenu2();
    }

    private void popupMenu1() {
        zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu popupMenuModel = new zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu();
        popupMenuModel.setViewName("我 的 租 地");
        zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu popupMenuMode2 = new zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu();
        popupMenuMode2.setViewName("我 的 动 态");
        zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu popupMenuMode3 = new zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu();
        popupMenuMode3.setViewName("我 的 收 藏");
        List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> list = new ArrayList<>();
        list.add(popupMenuModel);
        list.add(popupMenuMode2);
        list.add(popupMenuMode3);
        popupMenu = new PopupMenu(activity, list);
        popupMenu.showLocation(R.id.home_toolbar);
        popupMenu.backgroundAlpha(activity, 0.7f);//0.0-1.0
        popupMenu.getAdapter().setItemClickListener(new PopupMenuAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(List<zhixin.cn.com.happyfarm_user.popupMenu.model.PopupMenu> mList, View view, int position) {
                String token = activity.getSharedPreferences("User_data", MODE_PRIVATE).getString("token", "");
                if ("".equals(token)) {
                    noLoginDialog();
                    return;
                }
                if (!NetWorkUtils.isNetworkAvalible(activity)) {
                    CustomToast.showToast(activity, "请检查网络，稍后再试");
                    return;
                }
                mDialog = LoadingDialog.createLoadingDialog(activity, "");
                Call<GetSelfInfo> getSelfInfoCall = HttpHelper.initHttpHelper().getSelfInfo();
                getSelfInfoCall.enqueue(new Callback<GetSelfInfo>() {
                    @Override
                    public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                        if ("success".equals(response.body().getFlag())) {
                            LandId = String.valueOf(response.body().getResult().getLandId());
                            LandNo = String.valueOf(response.body().getResult().getLandNo());
                            Log.e("LandId", LandId);
                            Log.e("LandNo", LandNo);
                            switch (position) {
                                case 0:
                                    if ("".equals(LandId) || "".equals(LandNo) || "0".equals(LandId) || "0".equals(LandNo)) {
                                        CustomToast.showToast(activity, "您尚未购买租地");
                                        //Toast.makeText(activity,"您尚未购买租地",Toast.LENGTH_SHORT).show();
                                    } else {
                                        //LandId = String.valueOf(Double.valueOf(LandId).intValue());//转换为Int类型
                                        //LandNo = String.valueOf(Double.valueOf(LandNo).intValue());//转换为Int类型
                                        Intent intent1 = new Intent(activity, LandVideoActivity.class);
                                        intent1.putExtra("state", "0");
                                        intent1.putExtra("landNo", LandNo);
                                        intent1.putExtra("landId", LandId);
                                        intent1.putExtra("UserId", "0");
                                        intent1.putExtra("isLandlord", isLandlord);
                                        intent1.putExtra("superUser", superUser + "");
                                        intent1.putExtra("landType", landType + "");
                                        LoadingDialog.closeDialog(mDialog);
                                        startActivity(intent1);
                                    }
                                    LoadingDialog.closeDialog(mDialog);
                                    break;
                                case 1:
                                    Intent intent3 = new Intent(activity, MyDynamicActivity.class);
                                    Log.i("info", "ssssss: " + UserNickName);
                                    intent3.putExtra("UserImg", UserImg);
                                    intent3.putExtra("UserNickName", UserNickName);
                                    intent3.putExtra("UserNickName", UserNickName);
                                    LoadingDialog.closeDialog(mDialog);
                                    startActivity(intent3);
                                    break;
                                case 2:
                                    Intent intent = new Intent(activity, MyCollectionActivity.class);
                                    LoadingDialog.closeDialog(mDialog);
                                    startActivity(intent);
                                    break;
                                default:
                                    LoadingDialog.closeDialog(mDialog);
                                    break;
                            }
                        } else {
                            LoadingDialog.closeDialog(mDialog);
                            noLoginDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetSelfInfo> call, Throwable t) {
                        LoadingDialog.closeDialog(mDialog);
                        try {
                            if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                                noLoginDialog();
                            } else {
                                CustomToast.showToast(activity, TextString.NetworkRequestFailed);
                            }
                        } catch (Exception e) {
                            Log.e("FarmPage->", "onFailure: " + e);
                        }

                    }
                });
                popupMenu.dismiss();
            }
        });
    }

    private void popupMenu2() {
        // 设置点击监听事件
        mPopupMenuView.setOnMenuClickListener(new OptionMenuView.OnOptionMenuClickListener() {
            @Override
            public boolean onOptionMenuClick(int position, OptionMenu menu) {
                String token = activity.getSharedPreferences("User_data", MODE_PRIVATE).getString("token", "");
                if ("".equals(token)) {
                    noLoginDialog();
                    return true;
                }
                if (!NetWorkUtils.isNetworkAvalible(activity)) {
                    CustomToast.showToast(activity, "请检查网络，稍后再试");
                    return true;
                }
                mDialog = LoadingDialog.createLoadingDialog(activity, "");
                Call<GetSelfInfo> getSelfInfoCall = HttpHelper.initHttpHelper().getSelfInfo();
                getSelfInfoCall.enqueue(new Callback<GetSelfInfo>() {
                    @Override
                    public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                        if ("success".equals(response.body().getFlag())) {
                            LandId = String.valueOf(response.body().getResult().getLandId());
                            LandNo = String.valueOf(response.body().getResult().getLandNo());
                            Log.e("LandId", LandId);
                            Log.e("LandNo", LandNo);
                            switch (position) {
                                case 0:
                                    if ("".equals(LandId) || "".equals(LandNo) || "0".equals(LandId) || "0".equals(LandNo)) {
                                        CustomToast.showToast(activity, "您尚未购买租地");
                                        //Toast.makeText(activity,"您尚未购买租地",Toast.LENGTH_SHORT).show();
                                    } else {
                                        //LandId = String.valueOf(Double.valueOf(LandId).intValue());//转换为Int类型
                                        //LandNo = String.valueOf(Double.valueOf(LandNo).intValue());//转换为Int类型
                                        Intent intent1 = new Intent(activity, LandVideoActivity.class);
                                        intent1.putExtra("state", "0");
                                        intent1.putExtra("landNo", LandNo);
                                        intent1.putExtra("landId", LandId);
                                        intent1.putExtra("UserId", "0");
                                        intent1.putExtra("isLandlord", isLandlord);
                                        intent1.putExtra("superUser", superUser + "");
                                        intent1.putExtra("landType", landType + "");
                                        LoadingDialog.closeDialog(mDialog);
                                        startActivity(intent1);
                                    }
                                    LoadingDialog.closeDialog(mDialog);
                                    break;
                                case 1:
                                    Intent intent3 = new Intent(activity, MyDynamicActivity.class);
                                    Log.i("info", "ssssss: " + UserNickName);
                                    intent3.putExtra("UserImg", UserImg);
                                    intent3.putExtra("UserNickName", UserNickName);
                                    intent3.putExtra("UserNickName", UserNickName);
                                    LoadingDialog.closeDialog(mDialog);
                                    startActivity(intent3);
                                    break;
                                case 2:
                                    Intent intent = new Intent(activity, MyCollectionActivity.class);
                                    LoadingDialog.closeDialog(mDialog);
                                    startActivity(intent);
                                    break;
                                default:
                                    LoadingDialog.closeDialog(mDialog);
                                    break;
                            }
                        } else {
                            LoadingDialog.closeDialog(mDialog);
                            noLoginDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetSelfInfo> call, Throwable t) {
                        LoadingDialog.closeDialog(mDialog);
                        try {
                            if (String.valueOf(t).contains("com.google.gson.JsonSyntaxException")) {
                                noLoginDialog();
                            } else {
                                CustomToast.showToast(activity, TextString.NetworkRequestFailed);
                            }
                        } catch (Exception e) {
                            Log.e("FarmPage->", "onFailure: " + e);
                        }
                    }
                });
                return true;
            }
        });
        // 显示在mButtom控件的周围
        mPopupMenuView.show(toolbar);
    }

    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(activity);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }

                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    public void userInfoTokenTest() {
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
//                Log.i(TAG, "onResponse: " +JSON.toJSONString(response.body().getFlag()));
                if ("failed".equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(activity, "User_data", strArr);
//                    allLand();
                    saveState("0");
//                    myView.findViewById(R.id.message_icon).setVisibility(View.GONE);
                } else {
//                    myView.findViewById(R.id.message_icon).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Log.i("请求失败打印：", "onFailure: " + t);
                Toast.makeText(activity, TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
//                myView.findViewById(R.id.message_icon).setVisibility(View.GONE);
            }
        });
    }

    /**
     * 在MainActivity内当点击菜园item时会触发查询所有租地接口
     * 但是在生命周期onStart内也加入了查询所有租地接口，主要是防止回到租地页面或者第一次来到页面时没有数据显示
     * 然后下面是记录方法是否在规定时间内触发了多次
     * 两次之间的间隔不能少于600毫秒
     */
    private static final int MIN_CLICK_DELAY_TIME = 600;
    private static long lastClickTime;

    //TODO 渲染所有地
    public void searchAllLand(int count) {
//        progressBar = myView.findViewById(R.id.farm_progress_bar);
        Log.i("全部租地列表", "进入的生命周期" + count);
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            Log.i("全部租地列表", "进入2");
            // 超过点击间隔后再将lastClickTime重置为当前点击时间
            lastClickTime = curClickTime;
            if (Config.bottomBarItem == 1)
                progressBar.setVisibility(View.VISIBLE);
            Call<AllLand> searchAllLandList = HttpHelper.initHttpHelper().landList();
            searchAllLandList.enqueue(new Callback<AllLand>() {
                @Override
                public void onResponse(Call<AllLand> call, Response<AllLand> response) {
//                    progressBar.setVisibility(View.GONE);
                    Log.i("全部租地列表", JSON.toJSONString(response.body()));
                    if (response.isSuccessful()) {
                        allLand = response.body();
                        setSelectViewValue();
                        for (AllLand.ResultBean.ListBean listBean : allLand.getResult().getList()) {
                            //判断用户身份是否失效
                            if (0 == getUserId()) {
                                return;
                            }
                            if (getUserId() == listBean.getUserId()) {
                                landType = listBean.getLandType();
                                return;
                            }
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }
                @Override
                public void onFailure(Call<AllLand> call, Throwable t) {
                    Log.i("全部租地列表", "进入3" + t.getMessage());
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(activity, TextString.LandRequestFailed, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void searchLandById(int landId, String UserName, String UserId, String landids,String LeaseTime, String ExpirationTime, String OtherUserImg,String OthersTel, int landType) {
            Call<AllLand> searchAllLandList = HttpHelper.initHttpHelper().landListByLandId(landId);
            searchAllLandList.enqueue(new Callback<AllLand>() {
                @Override
                public void onResponse(Call<AllLand> call, Response<AllLand> response) {
//                    Log.i("FarmPage", JSON.toJSONString(response));
//                    Log.i("FarmPage", JSON.toJSONString(response.body()));
                    if (response.isSuccessful()) {
                        String corp = response.body().getResult().getList().get(0).getCropName();
                        String corpName = "".equals(corp) ? "暂未种植" : corp;//产品类型
                        showDialog2(Integer.toString(landId),UserName,UserId,landids,LeaseTime,ExpirationTime,OtherUserImg,OthersTel,landType,corpName,alias);
                    }
                }

                @Override
                public void onFailure(Call<AllLand> call, Throwable t) {

                }
            });

    }

    public void saveState(String state) {
        //获取SharedPreferences对象
        SharedPreferences.Editor editor = activity.getSharedPreferences("data", MODE_PRIVATE).edit();
        //设置参数
        editor.putString("state", state);
        //提交
        editor.apply();
    }

    /**
     * 获取用户id
     *
     * @return 返回用户id int类型
     */
    private int getUserId() {
        int userId = 0;
        if (App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", "").equals("")) {
            return userId;
        } else {
            userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data", MODE_PRIVATE).getString("userId", ""));
        }
        return userId;
    }

    private void loginEM(){
//        loginEM("15170029037","e10adc3949ba59abbe56e057f20f883e");
        HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
            @Override
            public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                if (response.isSuccessful() && response.body().getFlag().equals("success")){
                    GetSelfInfo.ResultBean resultBean = response.body().getResult();
                    loginEM(String.valueOf(resultBean.getId()),resultBean.getPassword());
                }
            }

            @Override
            public void onFailure(Call<GetSelfInfo> call, Throwable t) {

            }
        });
    }
    //login环信
    private void loginEM(String user, String pwd){
        EMClient.getInstance().login(user,pwd,new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                Log.d("EMLogmain","登录环信成功");
                //TODO 以下两个方法是为了保证进入主页面后本地会话和群组都 load 完毕。
                EMClient.getInstance().chatManager().loadAllConversations();
                EMClient.getInstance().groupManager().loadAllGroups();
                // 获取华为 HMS 推送 token
//                HMSPushHelper.getInstance().connectHMS(getActivity());
//                HMSPushHelper.getInstance().getHMSPushToken();
//                setMsgNum(2,EMClient.getInstance().chatManager().getUnreadMessageCount());
                Log.i("打印消息数量1", "数量" + EMClient.getInstance().chatManager().getUnreadMessageCount());
            }

            @Override
            public void onProgress(int progress, String status) {}

            @Override
            public void onError(int code, String message) {
                Log.d("EMLogmain", "登录聊天服务器失败！"+code+"信息："+message);
                try {
                    Toast.makeText(getContext(),"登录聊天服务器失败！",Toast.LENGTH_SHORT).show();
                }catch (Exception e) {
                }
            }
        });
    }

//    public void getEMNumber(int number){
//        setMsgNum(2,number);
//    }
}


