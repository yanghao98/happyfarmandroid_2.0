package zhixin.cn.com.happyfarm_user.Page;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.baidu.platform.comapi.map.E;
import com.hyphenate.chat.EMClient;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.AllGoodAdapter;
import zhixin.cn.com.happyfarm_user.DetailsOfVegetablesActivity;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.LoginActivity;
import zhixin.cn.com.happyfarm_user.MyRecipeActivity;
import zhixin.cn.com.happyfarm_user.PersonalMessageActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.ShoppingCartActivity;
import zhixin.cn.com.happyfarm_user.TheMarketActivtiy;
import zhixin.cn.com.happyfarm_user.View.SmartScrollView;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AdSeat;
import zhixin.cn.com.happyfarm_user.model.AllGoods;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.DishCategory;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.SearchAllGoods;
import zhixin.cn.com.happyfarm_user.model.ShoppingCartNum;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.utils.Config;
import zhixin.cn.com.happyfarm_user.utils.GlideImageLoader;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

import static android.content.Context.MODE_PRIVATE;

public class NewMarketPage extends Fragment {

    private Activity activity;
    private View myView;
    protected ImageButton clearSearch;
    protected EditText query;
    private TextView titleName;
    private RelativeLayout titleBarLayout;
//    private TextView recommendedOne;
//    private TextView recommendedTwo;
    private ImageView shoppingCart;
    private RecyclerView mRecyclerView;
    private AllGoodAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private ArrayList<AllGoods> list = new ArrayList<>();

    //动画时间
    private int AnimationDuration = 1000;
    //正在执行的动画数量
    private int number = 0;
    //是否完成清理
    private boolean isClean = false;
    private FrameLayout animation_viewGroup;
    private Handler myHandler = new Handler(){
        public void handleMessage(Message msg){
            switch(msg.what){
                case 0:
                    //用来清除动画后留下的垃圾
                    try{
                        animation_viewGroup.removeAllViews();
                    }catch(Exception e){

                    }
                    isClean = false;
                    break;
                default:
                    break;
            }
        }
    };

    private Badge messageBadgeShoppingCart;
    private int messageBadgeNum = 0;
    public int shoppingCartNumber = 0;//加 入购物车数量
    public int selectedCropId = 0;
    private Dialog mDialog;
    private List<DishCategory.ResultBean.ListBean> dishCategoryList;
    private String TAG = "NewMarketPage";
    private Badge messageBadge;
    private RelativeLayout myGoodPrompt;
    private SmartScrollView smartScrollView;
    private int pageNum = 1;
    private boolean isLastPage = false;
    private Dialog dialog;
    private String tokenStr;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_market_page_layout, null, false);
        myView = view;
        initPage(view);
        searchShoppingCartNum();
//        CommodityInformation(3);
        searchAdSeat();
//        searchDishCategory();//查询今日特价和今日折扣
//        initRecyclerView(view);
        setStatusBarHeight();
//        AutoLoading();
        return view;
    }

    private void initPage(View view) {
        titleName = view.findViewById(R.id.main_page_title_name);
        titleName.setText("菜场");
        titleBarLayout = view.findViewById(R.id.title_bar_layout);
        query = view.findViewById(R.id.query);
        clearSearch = view.findViewById(R.id.search_clear);
        shoppingCart = view.findViewById(R.id.shopping_cart);
        mRecyclerView = view.findViewById(R.id.market_page_list);
        myGoodPrompt = view.findViewById(R.id.my_goods_prompt);
        smartScrollView = view.findViewById(R.id.smart_scrollView);
        smartScrollView.setScrollViewListener(new SmartScrollView.ScrollViewListener() {
            @Override
            public void onScrollChanged(SmartScrollView scrollView, int x, int y, int oldx, int oldy) {
                View childAt = scrollView.getChildAt(0);
                int childHeight = childAt.getHeight();//获取子控件高度
                int height = scrollView.getHeight();//获取Scrollview的控件高度
                if (y + height == childHeight) {//判断条件 当子控件高度=Scrollview的控件高度+x的时候控件到达底部
                    if (Utils.isFastClick()) {
                        if (isLastPage) {
                            Toast.makeText(getContext(), "没有更多数据", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            CommodityInformation(3, pageNum);
                        }
                    }
                }
            }
        });
        //今日推荐和今日折扣按钮
//        recommendedOne = view.findViewById(R.id.recommended_one);
//        recommendedTwo = view.findViewById(R.id.recommended_two);
//        recommendedOne.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                for (int i = 0;i < dishCategoryList.size();i++) {
//                    if (recommendedOne.getText().equals(dishCategoryList.get(i).getCategoriesName())) {
//                        Intent intent = new Intent(getContext(), TheMarketActivtiy.class);
//                        intent.putExtra("dishCategoryId",dishCategoryList.get(i).getId());
//                        Log.i(TAG, "onClick: "+JSON.toJSONString(dishCategoryList.get(i)));
//                        startActivity(intent);
//                    }
//                }
//
//            }
//        });
//        recommendedTwo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                for (int i = 0;i < dishCategoryList.size();i++) {
//                    if (recommendedTwo.getText().equals(dishCategoryList.get(i).getCategoriesName())) {
//                        Intent intent = new Intent(getContext(), TheMarketActivtiy.class);
//                        intent.putExtra("dishCategoryId", dishCategoryList.get(i).getId());
//                        Log.i(TAG, "onClick: "+JSON.toJSONString(dishCategoryList.get(i)));
//                        startActivity(intent);
//                    }
//                }
//            }
//        });
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = activity.getSharedPreferences("User_data",MODE_PRIVATE);
                tokenStr = pref.getString("token","");
                if ("".equals(tokenStr)) {
                    noLoginDialog();
                } else {
                    startActivity(new Intent(getContext(), ShoppingCartActivity.class));
                }
            }
        });
        ImageView imageView = view.findViewById(R.id.main_page_message_icon);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    Intent intent = new Intent(getActivity(), PersonalMessageActivity.class);
                    startActivity(intent);
                }

            }
        });

        messageBadgeShoppingCart = new QBadgeView(getContext())
                .bindTarget(myView.findViewById(R.id.shopping_cart_icon))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);

        messageBadge = new QBadgeView(getContext())
                .bindTarget(myView.findViewById(R.id.main_page_icon_view_total))//获取需要显示的位置
                .setBadgeNumber(0)
                .setBadgeTextSize(7, true);
//        setMsgNum(EMClient.getInstance().chatManager().getUnreadMessageCount());
    }

    private void initRecyclerView(View view,ArrayList<AllGoods> myList) {

        adapter = new AllGoodAdapter(getContext(),myList);
        //设置适配器
        //解决数据加载不完的问题
        mRecyclerView.setNestedScrollingEnabled(false);
        //当知道Adapter内Item的改变不会影响RecyclerView宽高的时候，可以设置为true让RecyclerView避免重新计算大小
        mRecyclerView.setHasFixedSize(true);
        //解决数据加载完成后, 没有停留在顶部的问题
        mRecyclerView.setFocusable(false);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        mLinearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);


//        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
//        gridLayoutManager = new GridLayoutManager(getContext(), 1);
//        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
//        gridLayoutManager.setReverseLayout(false);
//        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
//        mRecyclerView.setLayoutManager(gridLayoutManager);


        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });


        //TODO 设置一行一个 ，左右间距 50
//        collection_recycle.addItemDecoration(new GridSpacingItemDecoration(spanCount,spacing,includeEdge));
        //item点击事件
        adapter.setItemClickListener(new AllGoodAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int positions) {
//                Log.e(TAG, "点击的菜品: "+JSON.toJSONString(list.get(positions).cropId) );
                Intent intent = new Intent(getContext(), DetailsOfVegetablesActivity.class);
                intent.putExtra("corpId",list.get(positions).cropId);
                startActivity(intent);
            }
        });
        animation_viewGroup = createAnimLayout();
        adapter.SetOnSetHolderClickListener(new AllGoodAdapter.HolderClickListener() {
            @Override
            public void onHolderClick(Drawable drawable, int[] start_location,AllGoods goods) {
                if (Utils.isFastClick()) {
                    mDialog = LoadingDialog.createLoadingDialog(getContext(), "添加中...");
                    HttpHelper.initHttpHelper().getSelfInfo().enqueue(new Callback<GetSelfInfo>() {
                        @Override
                        public void onResponse(Call<GetSelfInfo> call, Response<GetSelfInfo> response) {
                            if ("success".equals(response.body().getFlag())) {
//                                Log.i("加入购物车菜品的详细信息", "onClick: " + JSON.toJSONString(goods));
                                addShoppingCart(drawable, start_location, goods.cropId);
                            } else {
                                noLoginDialog();
                                LoadingDialog.closeDialog(mDialog);
                            }
                        }

                        @Override
                        public void onFailure(Call<GetSelfInfo> call, Throwable t) {
                            noLoginDialog();
                            LoadingDialog.closeDialog(mDialog);
                        }
                    });
                }
            }
        });
    }

    /**
     * 获取购物车数量
     */
    public void searchShoppingCartNum() {
        HttpHelper.initHttpHelper().searchShoppingCartNum().enqueue(new Callback<ShoppingCartNum>() {
            @Override
            public void onResponse(Call<ShoppingCartNum> call, Response<ShoppingCartNum> response) {
                if ("success".equals(response.body().getFlag())) {
                    messageBadgeNum = response.body().getResult();
                    messageBadgeShoppingCart.setBadgeNumber(response.body().getResult());
                }
            }
            @Override
            public void onFailure(Call<ShoppingCartNum> call, Throwable t) {

            }
        });
    }

    /**
     * 获取广告栏图片
     */
    private void searchAdSeat() {
        HttpHelper.initHttpHelper().searchAdSeat(1).enqueue(new Callback<AdSeat>() {
            @Override
            public void onResponse(Call<AdSeat> call, Response<AdSeat> response) {
                try {
                    if ("success".equals(response.body().getFlag())) {
                        List images = new ArrayList();
                        List<AdSeat.ResultBean.ListBean> obj = response.body().getResult().getList();
//                    images.add("http://image14.m1905.cn/uploadfile/2018/0907/thumb_1_1380_460_20180907013518839623.jpg");
                        for (int i = 0;i < obj.size();i++) {
                            images.add(obj.get(i).getSeatImg());
                        }
                        Banner banner = (Banner) getActivity().findViewById(R.id.banner);
                        //设置图片加载器
                        banner.setImageLoader(new GlideImageLoader());
                        //设置图片集合
                        banner.setImages(images);
                        //banner设置方法全部调用完毕时最后调用
                        banner.start();
                        //增加点击事件
                        banner.setOnBannerListener(new OnBannerListener() {
                            @Override
                            public void OnBannerClick(int position) {
//                                Toast.makeText(getContext(), "position"+response.body().getResult().getList().get(position).getGoodsId(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getContext(), DetailsOfVegetablesActivity.class);
                                intent.putExtra("corpId",response.body().getResult().getList().get(position).getGoodsId());
                                startActivity(intent);
                            }
                        });
                    }
                } catch (Exception e) {
                    Log.e(TAG, "菜场banner没来的急加载 ", e);
                }
            }

            @Override
            public void onFailure(Call<AdSeat> call, Throwable t) {

            }
        });
    }

    //TODO 获取全部商品信息
    private void CommodityInformation(int priceState,int pageNumber) {
        Log.i(TAG, "CommodityInformation: " + pageNumber);
        dialog = LoadingDialog.createLoadingDialog(getContext(), "正在加载数据");
        HttpHelper.initHttpHelper().searchAllGoodsByPriceState(priceState,pageNumber,15).enqueue(new Callback<SearchAllGoods>() {
            @Override
            public void onResponse(Call<SearchAllGoods> call, Response<SearchAllGoods> response) {
//                Log.e("AllGoodPage-->", JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
//                    Log.e(TAG,JSON.toJSONString(response.body()));
//                    isLastPage = response.body().getResult().isIsLastPage();
                    if (response.body().getResult().getList().isEmpty()) {
                        myGoodPrompt.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    } else {
                        isLastPage = response.body().getResult().isIsLastPage();
                        for (int i = 0; i < response.body().getResult().getList().size(); i++) {
                            SearchAllGoods.ResultBean.ListBean listBean = response.body().getResult().getList().get(i);
                            AllGoods dataBean = new AllGoods();
                            dataBean.cropId = listBean.getId();
                            dataBean.sellerId = listBean.getSellerId();
                            dataBean.cropImg = listBean.getCropImg();
                            dataBean.cropNames = listBean.getCropName();
                            dataBean.shelfTime = DateUtil.getStrTime(String.valueOf(listBean.getStartTime()));
//                        dataBean.isOrganic = response.body().getResult().getList().get(i).getIsOrganic();
//                            if (listBean.getIsOrganic() == 1) {
//                                dataBean.isOrganic = "(有机肥)";
//                            } else if (listBean.getIsOrganic() == 2) {
//                                dataBean.isOrganic = "(无机肥)";
//                            } else {
//                                dataBean.isOrganic = "";
//                            }
                            dataBean.nums = String.valueOf(listBean.getNum());
                            dataBean.sellerNickname = listBean.getSellerNickname();
                            dataBean.prices = String.valueOf(listBean.getPrice());
                            list.add(dataBean);
                        }
                        myGoodPrompt.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
//                        adapter.notifyDataSetChanged();
                        initRecyclerView(myView,list);
                        Log.i(TAG, "onResponse: " + list.size());
                    }
                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(Call<SearchAllGoods> call, Throwable t) {
                myGoodPrompt.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }


    /**
     * 查询菜品类别
     */
//    private void searchDishCategory() {
//        HttpHelper.initHttpHelper().searchDishCategory().enqueue(new Callback<DishCategory>() {
//            @Override
//            public void onResponse(Call<DishCategory> call, Response<DishCategory> response) {
//                Log.i("NewMarketPage", "onResponse: "+JSON.toJSONString(response.body()));
//                if ("success".equals(response.body().getFlag())) {
//                    dishCategoryList = response.body().getResult().list;
//                    recommendedOne.setText(response.body().getResult().list.get(0).getCategoriesName());
//                    recommendedTwo.setText(response.body().getResult().list.get(1).getCategoriesName());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<DishCategory> call, Throwable t) {
//
//            }
//        });
//    }


    /**
     * 添加购物车
     */
    private void addShoppingCart(Drawable drawable,int[] start_location,Integer goodsId) {

        HttpHelper.initHttpHelper().addShoppingCart(goodsId).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if ("success".equals(response.body().getFlag())) {
                    doAnim(drawable,start_location);
                    messageBadgeNum++;
                    messageBadgeShoppingCart.setBadgeNumber(messageBadgeNum);
                    Toast.makeText(getContext(),"加入购物车成功",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(),response.body().getResult(),Toast.LENGTH_SHORT).show();
                }
                LoadingDialog.closeDialog(mDialog);
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(getContext(),"加入购物车失败",Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(mDialog);

            }
        });
    }

    public void getEMNumber(int number){
        setMsgNum(number);
    }
    /**
     * 设置消息显示数量
     * @param num 消息数量
     * position 设置显示tablayout的item 暂时未时候，考虑后期可能所有的item都需要
     */
    public void setMsgNum( int num){
        if (messageBadge != null){
//            Log.i(TAG, "消息设置成功setMsgNum: "+num);
            messageBadge.setBadgeNumber(num);
        }else {
            Log.i(TAG, "消息设置失败setMsgNum: "+num);
        }
    }
    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(getContext());
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }

                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    /**
     * 从此开始制作动画
     * @param drawable
     * @param start_location
     */
    private void doAnim(Drawable drawable,int[] start_location){
        if(!isClean){
            setAnim(drawable,start_location);
        }else{
            try{
                animation_viewGroup.removeAllViews();
                isClean = false;
                setAnim(drawable,start_location);
            }catch(Exception e){
                e.printStackTrace();
            }
            finally{
                isClean = true;
            }
        }
    }

    /**
     * @Description: 创建动画层
     * @param
     * @return void
     * @throws
     */
    private FrameLayout createAnimLayout(){
        ViewGroup rootView = (ViewGroup)getActivity().getWindow().getDecorView();
        FrameLayout animLayout = new FrameLayout(getContext());
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,FrameLayout.LayoutParams.MATCH_PARENT);
        animLayout.setLayoutParams(lp);
        animLayout.setBackgroundResource(android.R.color.transparent);
        rootView.addView(animLayout);
        return animLayout;

    }

    /**
     * @deprecated 将要执行动画的view 添加到动画层
     * @param vg
     *        动画运行的层 这里是frameLayout
     * @param view
     *        要运行动画的View
     * @param location
     *        动画的起始位置
     * @return
     */
    private View addViewToAnimLayout(ViewGroup vg,View view,int[] location){
        int x = location[0];
        int y = location[1];
        vg.addView(view);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                dip2px(getContext(),90),dip2px(getContext(),90));
        lp.leftMargin = x;
        lp.topMargin = y;
        view.setPadding(5, 5, 5, 5);
        view.setLayoutParams(lp);

        return view;
    }
    /**
     * dip，dp转化成px 用来处理不同分辨路的屏幕
     * @param context
     * @param dpValue
     * @return
     */
    private int dip2px(Context context, float dpValue){
        float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dpValue*scale +0.5f);
    }

    /**
     * 动画效果设置
     * @param drawable
     *       将要加入购物车的商品
     * @param start_location
     *        起始位置
     */
    private void setAnim(Drawable drawable,int[] start_location){


        Animation mScaleAnimation = new ScaleAnimation(1.5f,0.0f,1.5f,0.0f,Animation.RELATIVE_TO_SELF,0.1f,Animation.RELATIVE_TO_SELF,0.1f);
        mScaleAnimation.setDuration(AnimationDuration);
        mScaleAnimation.setFillAfter(true);


        final ImageView iview = new ImageView(getContext());
        iview.setImageDrawable(drawable);
        final View view = addViewToAnimLayout(animation_viewGroup,iview,start_location);
        view.setAlpha(0.6f);

        int[] end_location = new int[2];
        //结束点控件位置

        myView.findViewById(R.id.shopping_cart).getLocationInWindow(end_location);
        int endX = end_location[0];
        int endY = end_location[1]-start_location[1];

        Animation mTranslateAnimation = new TranslateAnimation(0,endX,0,endY);
        Animation mRotateAnimation = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mRotateAnimation.setDuration(AnimationDuration);
        mTranslateAnimation.setDuration(AnimationDuration);
        AnimationSet mAnimationSet = new AnimationSet(true);

        mAnimationSet.setFillAfter(true);
        mAnimationSet.addAnimation(mRotateAnimation);
        mAnimationSet.addAnimation(mScaleAnimation);
        mAnimationSet.addAnimation(mTranslateAnimation);

        mAnimationSet.setAnimationListener(new Animation.AnimationListener(){


            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub
                number++;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub

                number--;
                if(number==0){
                    isClean = true;
                    myHandler.sendEmptyMessage(0);
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

        });
        view.startAnimation(mAnimationSet);

    }


    /**
     * 内存过低时及时处理动画产生的未处理冗余
     */
    @Override
    public void onLowMemory() {
        // TODO Auto-generated method stub
        isClean = true;
        try{
            animation_viewGroup.removeAllViews();
        }catch(Exception e){
            e.printStackTrace();
        }
        isClean = false;
        super.onLowMemory();
    }
    //动画制作完成


    /**
     * 设置导航栏距离顶部的距离
     */
    private void setStatusBarHeight() {
        //设置按钮偏移位置(相对于状态栏的top)
        int statusBarHeight = getStatusBarHeight(getActivity());
        //相对布局使用方法
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
//        params.setMargins(0, statusBarHeight, 0, 0);
//        titleBarLayout.setLayoutParams(params);
        //线性布局使用方法
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 40 + statusBarHeight);
        params.setMargins(0, statusBarHeight, 0, 0);
        titleBarLayout.setLayoutParams(params);
    }
    /**
     * 获取状态栏高度
     * @param context 上下文
     * @return int 状态栏高度值
     */
    protected static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    /**
     * 验证token是否有效
     */
    public  void userInfoTokenTest(){
        Call<Test> test = HttpHelper.initHttpHelper().test();
        test.enqueue(new Callback<Test>() {
            @Override
            public void onResponse(Call<Test> call, Response<Test> response) {
//                System.out.println("###"+ JSON.toJSONString(response.body()));
                if (("failed").equals(response.body().getFlag())) {
                    String[] strArr = {"token", "userId", "userImg", "uuid"};
                    SharedPreferencesUtils.cleanShared(getContext(), "User_data", strArr);
                    if ("用户信息失效".equals(response.body().getResult())){
//                        myView.findViewById(R.id.vip_tabLayout_content).setVisibility(View.GONE);
                    }
                    myView.findViewById(R.id.main_page_message_icon).setVisibility(View.GONE);
                } else if (("success").equals(response.body().getFlag())){
//                        myView.findViewById(R.id.vip_tabLayout_content).setVisibility(View.VISIBLE);
                    myView.findViewById(R.id.main_page_message_icon).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Test> call, Throwable t) {
                Toast.makeText(getContext(), TextString.NetworkRequestFailed,Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onResume() {
        searchShoppingCartNum();
        userInfoTokenTest();
        if (!list.isEmpty()) {
            list.clear();
        }
        pageNum = 1;
        CommodityInformation(3,pageNum);
        setMsgNum(EMClient.getInstance().chatManager().getUnreadMessageCount());
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: " + "页面被销毁了");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onDestroy: " + "页面不可见");
        list.clear();
    }
}
