package zhixin.cn.com.happyfarm_user;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.githang.statusbar.StatusBarCompat;
import com.squareup.picasso.Picasso;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cn.sharesdk.onekeyshare.OnekeyShare;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.utils.Config;

public class InvitationCodeActivity extends AppCompatActivity {
    private String invitationCode;

    private TextView invitationCodeText;
    private Button button;
    private Button copyButton;
    private Button shareButton;
    private EditText editText;
    private ClipboardManager cm;
    private ClipData mClipData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invitation_code_activity);
        TextView title = findViewById(R.id.title_name);
        title.setText("邀请码");
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        Intent intent = getIntent();
        invitationCode = intent.getStringExtra("invitation_code_activity");
        Config.invitationCode = intent.getStringExtra("invitation_code_activity");
        initView();
    }

    private void initView() {
        invitationCodeText = findViewById(R.id.invitation_code);
        invitationCodeText.setText(Config.invitationCode);
        editText = findViewById(R.id.user_invitation_code);
        button = findViewById(R.id.invitation_code_button);
        copyButton = findViewById(R.id.copy_button);
        shareButton = findViewById(R.id.share_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                System.out.println(invitationCode);
//                System.out.println(editText.getText().toString());
                if ("".equals(editText.getText().toString())){
                    Toast.makeText(InvitationCodeActivity.this,"请填写邀请码",Toast.LENGTH_SHORT).show();
                } else {
                    if (invitationCode.equals(editText.getText().toString())) {
                        Toast.makeText(InvitationCodeActivity.this,"自己不能邀请自己哟！",Toast.LENGTH_SHORT).show();
                    } else {
                        addInvitation();
                    }
                }
            }
        });
        copyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //获取剪贴板管理器：
                cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                // 创建普通字符型ClipData
                mClipData = ClipData.newPlainText("Label", "您的好友"+App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("nickName","")+"现邀请您加入植信家园，邀请码为："+invitationCode+"。\n\n请点击链接获取植信APP：https://www.trustwusee.com/downloadApp");
                // 将ClipData内容放到系统剪贴板里。
                cm.setPrimaryClip(mClipData);
                Toast.makeText(InvitationCodeActivity.this,"已经复制到粘贴板",Toast.LENGTH_SHORT).show();
            }
        });
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InvitationCodeActivity.this,ShareActivity.class);
                intent.putExtra("invitationCode",invitationCode);
                startActivity(intent);
            }
        });
    }




    /**
     * 提交邀请码
     * @param
     */
    private void addInvitation(){
        HttpHelper.initHttpHelper().addInvitation(editText.getText().toString()).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                if ("success".equals(response.body().getFlag())) {
                    Toast.makeText(InvitationCodeActivity.this,"邀请成功",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(InvitationCodeActivity.this,response.body().getResult(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {

            }
        });
    }



    public void back(View view){
        finish();
    }

}
