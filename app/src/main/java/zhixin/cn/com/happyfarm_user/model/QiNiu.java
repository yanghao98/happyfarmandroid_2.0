package zhixin.cn.com.happyfarm_user.model;

import com.google.gson.Gson;

/**
 * Created by HuYueling on 2018/4/21.
 */

public class QiNiu {

    /**
     * result : {"imgUrl":"http://7xtaye.com2.z0.glb.clouddn.com/","uploadToken":"Ts9HwfqPf_91Bm_XmGHng-z2zKBvU9iItTF84Pqa:16CHFNaGW463L5zMiK_T2Q1kWIk=:eyJzY29wZSI6Inh5cHJvamVjdCIsImRlYWRsaW5lIjoxNTI1ODUwMDMxfQ==","domain":"xyproject"}
     * flag : success
     */

    private ResultBean result;
    private String flag;

    public static QiNiu objectFromData(String str) {

        return new Gson().fromJson(str, QiNiu.class);
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        /**
         * imgUrl : http://7xtaye.com2.z0.glb.clouddn.com/
         * uploadToken : Ts9HwfqPf_91Bm_XmGHng-z2zKBvU9iItTF84Pqa:16CHFNaGW463L5zMiK_T2Q1kWIk=:eyJzY29wZSI6Inh5cHJvamVjdCIsImRlYWRsaW5lIjoxNTI1ODUwMDMxfQ==
         * domain : xyproject
         */

        private String imgUrl;
        private String uploadToken;
        private String domain;

        public static ResultBean objectFromData(String str) {

            return new Gson().fromJson(str, ResultBean.class);
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getUploadToken() {
            return uploadToken;
        }

        public void setUploadToken(String uploadToken) {
            this.uploadToken = uploadToken;
        }

        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }
    }
}
