package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.githang.statusbar.StatusBarCompat;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.Cooking;
import zhixin.cn.com.happyfarm_user.model.CookingInfo;
import zhixin.cn.com.happyfarm_user.model.GetSelfInfo;
import zhixin.cn.com.happyfarm_user.model.Test;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.utils.SharedPreferencesUtils;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

public class CookingRecommendedActivity extends AppCompatActivity {

    private ListView imageList;
    private int cookingId;
    private TextView material;
    private TextView cookingName;
    private MyImageView myImageView;
    private TextView cookingUsername;
    private TextView praiseNumber;
    private ImageView giveLike;
    private LinearLayout cookingRecommendedList;
    private ImageView cookingTitleImage;
    private ImageView addCookingRecommended;
    private String TAG = "CookingRecommendedActivity";
    private RelativeLayout noCookingRecommended;
    private ScrollView  cookingRecommended;
    private Dialog dialog;
    private Dialog myDialog;
    private List<CookingInfo.ResultBean.CookingDetails> details = new ArrayList<>();
    private String tokenStr;
    private int praiseState;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cooking_recommended_activity);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        Intent intent = getIntent();
        cookingId = intent.getIntExtra("cookingId",0);
        material = findViewById(R.id.material);
        cookingName = findViewById(R.id.cooking_name);
        myImageView = findViewById(R.id.cooking_user_image);
        cookingUsername = findViewById(R.id.cooking_username);
        praiseNumber = findViewById(R.id.praise_number);
        cookingTitleImage = findViewById(R.id.cooking_title_image);
        addCookingRecommended = findViewById(R.id.add_cooking_recommended);
        addCookingRecommended.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getSharedPreferences("User_data",MODE_PRIVATE);
                tokenStr = pref.getString("token","");
                if ("".equals(tokenStr)) {
                    noLoginDialog();
                } else {
                    Intent intent = new Intent(CookingRecommendedActivity.this, AddMakeCookingActivity.class);
                    startActivity(intent);
                }
            }
        });
        giveLike = findViewById(R.id.give_like);
//        giveLike.setImageResource(R.mipmap.no_give_like);
        giveLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isFastClick()) {
                    giveLike();
                }
            }
        });

        cookingRecommendedList = findViewById(R.id.cooking_recommended_list);
        noCookingRecommended = findViewById(R.id.no_cooking_recommended);
        cookingRecommended = findViewById(R.id.cooking_recommended_layout);

    }

    public void back(View view) {
        finish();
    }

    private void giveLike() {
        SharedPreferences pref = getSharedPreferences("User_data",MODE_PRIVATE);
        tokenStr = pref.getString("token","");
        if ("".equals(tokenStr)) {
            noLoginDialog();
        } else {
            addCookingMethodsPraise();
        }
    }

    private void addItemView(CookingInfo.ResultBean.CookingDetails detail) {
        View hotelEvaluateView = View.inflate(this, R.layout.cooking_info_list_item, null);
        TextView textView = hotelEvaluateView.findViewById(R.id.cooking_text);
        ImageView imageView = hotelEvaluateView.findViewById(R.id.cooking_image);
        textView.setText("\t\t\t\t第"+detail.getStepNum()+"步"+detail.getCookingExplain());
        try {
            Glide.with(this)
                .load(detail.getCookingImg())
                .into(imageView);
        } catch (Exception e) {
            Log.e(TAG, "图片加载异常 ",e);
        }
        cookingRecommendedList.addView(hotelEvaluateView);
    }


    /**
     * 查询烹饪信息
     */
    private void searchCookingMethods() {
        dialog = LoadingDialog.createLoadingDialog(CookingRecommendedActivity.this, "正在加载数据");
        HttpHelper.initHttpHelper().searchCookingMethodsDetails(cookingId).enqueue(new Callback<CookingInfo>() {
            @Override
            public void onResponse(Call<CookingInfo> call, Response<CookingInfo> response) {
//                System.out.println("结果"+JSON.toJSONString(response.body().getResult()));
                if ("success".equals(response.body().getFlag())) {
                    if (!details.isEmpty()) {
                        details.clear();
                    }
                    details = response.body().getResult().getCookingDetails();
                    material.setText("\t\t\t\t" + response.body().getResult().getMaterial());
                    cookingName.setText(response.body().getResult().getCookingName());
                    myImageView.setImageURL(response.body().getResult().getHeadImg());
                    cookingUsername.setText(response.body().getResult().getNickname());
                    praiseNumber.setText(response.body().getResult().getPraise().toString());
                    try {
                        Glide.with(CookingRecommendedActivity.this)
                                .load(response.body().getResult().getCookingImg())
                                .into(cookingTitleImage);
                    } catch (Exception e) {
                        Log.e(TAG, "图片加载异常 ",e);
                    }
//                    Log.e("点赞条数：", "onResponse: "+response.body().getResult().getPraiseState() );
                    praiseState = response.body().getResult().getPraiseState();
                    if (0 == response.body().getResult().getPraiseState()) {
                        giveLike.setImageResource(R.mipmap.give_like);
                    } else {
                        giveLike.setImageResource(R.mipmap.no_give_like);
                    }
                    cookingRecommendedList.removeAllViews();
                    for (int i = 0; i < details.size(); i++) {
                        addItemView(details.get(i));
                    }
                } else {
                    noCookingRecommended.setVisibility(View.VISIBLE);
                    cookingRecommended.setVisibility(View.GONE);
                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(Call<CookingInfo> call, Throwable t) {
                noCookingRecommended.setVisibility(View.VISIBLE);
                cookingRecommended.setVisibility(View.GONE);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }
    /**
     * 点赞功能
     */
    private void addCookingMethodsPraise() {
        String text = "";
        if (0 == praiseState) {
            text = "点赞中";
        } else {
            text = "取消点赞";
        }
        myDialog = LoadingDialog.createLoadingDialog(CookingRecommendedActivity.this, text);
        HttpHelper.initHttpHelper().addCookingMethodsPraise(cookingId).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                LoadingDialog.closeDialog(myDialog);
                if ("success".equals(response.body().getFlag())) {
//                    Toast.makeText(CookingRecommendedActivity.this,"点赞成功",Toast.LENGTH_SHORT).show();
                    searchCookingMethods();
                } else {
                    Toast.makeText(CookingRecommendedActivity.this,"点赞失败",Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(CookingRecommendedActivity.this,"网络错误",Toast.LENGTH_SHORT).show();
                LoadingDialog.closeDialog(myDialog);
            }
        });
    }

    /**
     * 未登录弹框点击事件
     */
    private void noLoginDialog() {
        final AlertUtilBest diyDialog = new AlertUtilBest(CookingRecommendedActivity.this);
        diyDialog.setCancel("取消")
                .setOk(TextString.DialogTitleText)
                .setContent(TextString.DialogContentText)
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        diyDialog.cancel();
                        Intent intent = new Intent(CookingRecommendedActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        searchCookingMethods();
    }
}
