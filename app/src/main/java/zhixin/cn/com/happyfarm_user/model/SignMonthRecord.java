package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class SignMonthRecord {

    private ResultBean result;
    private String flag;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class ResultBean {
        private String reward;
        private int whatDay;//本周期签到天数
        private int signDay;//总签到天数
        private int monthlyReward;//是否有一只鸡（连签1个月）
        private int sevenReward;//是否有一个鸡窝（连签7天）
        private int multimonthlyReward;//饲料包个数 从第二个月开始 最多11个
        private int annualReward;//鸡是否有一只成品鸡了
        private int totalAttendance;//签到人数
        private String signInRules;
        private int cycle;

        private List<ResultBeanList> list;

        public String getReward() {
            return reward;
        }

        public void setReward(String reward) {
            this.reward = reward;
        }

        public int getWhatDay() {
            return whatDay;
        }

        public void setWhatDay(int whatDay) {
            this.whatDay = whatDay;
        }

        public int getSignDay() {
            return signDay;
        }

        public void setSignDay(int signDay) {
            this.signDay = signDay;
        }

        public String getSignInRules() {
            return signInRules;
        }

        public void setSignInRules(String signInRules) {
            this.signInRules = signInRules;
        }

        public List<ResultBeanList> getList() {
            return list;
        }

        public void setList(List<ResultBeanList> list) {
            this.list = list;
        }

        public int getMonthlyReward() {
            return monthlyReward;
        }

        public void setMonthlyReward(int monthlyReward) {
            this.monthlyReward = monthlyReward;
        }

        public int getSevenReward() {
            return sevenReward;
        }

        public void setSevenReward(int sevenReward) {
            this.sevenReward = sevenReward;
        }

        public int getMultimonthlyReward() {
            return multimonthlyReward;
        }

        public void setMultimonthlyReward(int multimonthlyReward) {
            this.multimonthlyReward = multimonthlyReward;
        }

        public int getAnnualReward() {
            return annualReward;
        }

        public void setAnnualReward(int annualReward) {
            this.annualReward = annualReward;
        }

        public int getTotalAttendance() {
            return totalAttendance;
        }

        public void setTotalAttendance(int totalAttendance) {
            this.totalAttendance = totalAttendance;
        }

        public int getCycle() {
            return cycle;
        }

        public void setCycle(int cycle) {
            this.cycle = cycle;
        }

        public static class ResultBeanList {
            private int id;
            private int userId;
            private long startTime;
            private String userName;
            private int fewDay;
            private int state;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public long getStartTime() {
                return startTime;
            }

            public void setStartTime(long startTime) {
                this.startTime = startTime;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public int getFewDay() {
                return fewDay;
            }

            public void setFewDay(int fewDay) {
                this.fewDay = fewDay;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }
        }
    }



}
