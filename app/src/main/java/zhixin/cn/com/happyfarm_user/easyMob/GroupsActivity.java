/**
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package zhixin.cn.com.happyfarm_user.easyMob;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMGroup;
import com.hyphenate.easeui.EaseUI;
import com.hyphenate.easeui.domain.EaseUser;
import com.hyphenate.easeui.utils.EaseCommonUtils;
import com.hyphenate.exceptions.HyphenateException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.other.DensityUtil;
import zhixin.cn.com.happyfarm_user.utils.Config;

public class GroupsActivity extends ABaseActivity {
	public static final String TAG = "GroupsActivity";
	private ListView groupListView;
	protected List<EMGroup> grouplist;
	private GroupAdapter groupAdapter;
	private InputMethodManager inputMethodManager;
	public static GroupsActivity instance;
	private View progressBar;
	private SwipeRefreshLayout swipeRefreshLayout;
	private ImageView createGroup;
	private Map<String,EaseUser> mEMFriendInfo = new HashMap<>();
	private Dialog mDialog;
	private int positions;
	
	//TODO　下面隐掉部分为原来部分
	Handler handler = new Handler(){
	    public void handleMessage(android.os.Message msg) {
	        swipeRefreshLayout.setRefreshing(false);
	        switch (msg.what) {
            case 0:
                refresh();
                break;
            case 1:
                Toast.makeText(GroupsActivity.this, R.string.Failed_to_get_group_chat_information, Toast.LENGTH_LONG).show();
                break;

            default:
                break;
            }
	    }
	};

		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//启动activity时不自动弹出软键盘
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.em_fragment_groups);
		//状态栏设置
		StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

		instance = this;
		inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		grouplist = EMClient.getInstance().groupManager().getAllGroups();
		groupListView = (ListView) findViewById(R.id.list);
		//创建群组
		createGroup = findViewById(R.id.create_Group_toolbar);
		//show group list
        groupAdapter = new GroupAdapter(this, 1, grouplist);
        groupListView.setAdapter(groupAdapter);
		
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
		swipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
		//pull down to refresh
		swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				new Thread(){
					@Override
					public void run(){
						try {
							EMClient.getInstance().groupManager().getJoinedGroupsFromServer();
							handler.sendEmptyMessage(0);
						} catch (HyphenateException e) {
							e.printStackTrace();
							handler.sendEmptyMessage(1);
						}
					}
				}.start();
			}
		});
		
		groupListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				if (position == 1) {
//					// create a new group
//					startActivityForResult(new Intent(GroupsActivity.this, NewGroupActivity.class), 0);
//				} else if (position == 2) {
//					// join a public group
//					startActivityForResult(new Intent(GroupsActivity.this, PublicGroupsActivity.class), 0);
//				} else {
					// enter group chat
//					Intent intent = new Intent(GroupsActivity.this, ChatActivity.class);
					// it is group chat
//					intent.putExtra("chatType", Constant.CHATTYPE_GROUP);
//					intent.putExtra("userId", groupAdapter.getItem(position - 1).getGroupId());
//					startActivityForResult(intent, 0);
				positions = position;
				Log.i(TAG, "onItemClick: "+groupAdapter.getItem(positions - 1).getGroupId());
				searchGroupMembers();
//				}
			}

		});
		//TODO 长按群组pop弹框功能
//		groupListView.setOnItemLongClickListener((parent, view, position, id) -> {
//			if (position > 0){
////				if (position > 2){
//				PopupMenu menu = new PopupMenu(GroupsActivity.this, view);
//				menu.getMenuInflater().inflate(R.menu.group_long_click_menu, menu.getMenu());
//				menu.setOnMenuItemClickListener(item -> {
//					if (item.getItemId() == R.id.delete) {
//						String groupId = groupAdapter.getItem(position - 1).getGroupId();
////						String groupId = groupAdapter.getItem(position - 3).getGroupId();
//						String owner = EMClient.getInstance().groupManager().getGroup(groupId).getOwner();
//						if (owner.equals(getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""))){
//							Toast.makeText(GroupsActivity.this,"群主不能退出",Toast.LENGTH_SHORT).show();
//						}else {
//							AsyncTask.execute(()->{
//								try {
//									EMClient.getInstance().groupManager().leaveGroup(groupId);
//									runOnUiThread(this::refresh);
//								} catch (HyphenateException e) {
//									e.printStackTrace();
//								}
//							});
//
//						}
//					}
//					return true;
//				});
//				menu.show();
//			}
//			return true;
//		});
		groupListView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
					if (getCurrentFocus() != null)
						inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
								InputMethodManager.HIDE_NOT_ALWAYS);
				}
				return false;
			}
		});
		EaseUI.getInstance().setUserProfileProvider(new EaseUI.EaseUserProfileProvider() {
			@Override
			public EaseUser getUser(String username) {

				Log.i("info", "用户信息: "+username);
				//TODO 这里用来填入用户的昵称和头像
				EaseUser user = Config.AllEMFriendInfo.get(username);
				//TODO 这里是设置自己的头像
				if (user != null && username.equals(EMClient.getInstance().getCurrentUser())){
					user.setAvatar(getHeadImg());
					user.setNickname(username);
				}
				return user;
			}
		});
		CreateGroup();
		registerGroupChangeReceiver();
	}
	//创建群组
	public void CreateGroup(){
		createGroup.setOnClickListener(view ->{
			CreateGroupDialog();
		});
	}
	void registerGroupChangeReceiver() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Constant.ACTION_GROUP_CHANAGED);
		BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if(action.equals(Constant.ACTION_GROUP_CHANAGED)){
					if (EaseCommonUtils.getTopActivity(GroupsActivity.this).equals(GroupsActivity.class.getName())) {
						refresh();
					}
				}
			}
		};
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
		broadcastManager.registerReceiver(broadcastReceiver, intentFilter);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onResume() {
		new Thread(){
			@Override
			public void run(){
				try {
					EMClient.getInstance().groupManager().getJoinedGroupsFromServer();
					handler.sendEmptyMessage(0);
				} catch (HyphenateException e) {
					e.printStackTrace();
					handler.sendEmptyMessage(1);
				}
			}
		}.start();
//        refresh();
		super.onResume();
	}
	
	private void refresh(){
		EMClient.getInstance().groupManager().loadAllGroups();
	    grouplist = EMClient.getInstance().groupManager().getAllGroups();
        groupAdapter = new GroupAdapter(this, 1, grouplist);
        groupListView.setAdapter(groupAdapter);
        groupAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		instance = null;
	}

	public void back(View view) {
		finish();
	}
	//TODO 查询群成员
	public void searchGroupMembers(){
		mDialog = LoadingDialog.createLoadingDialog(GroupsActivity.this, null);
		HttpHelper.initHttpHelper().searchGroupMembers(String.valueOf(groupAdapter.getItem(positions - 1).getGroupId())).enqueue(new Callback<searchGroup>() {
			@Override
			public void onResponse(Call<searchGroup> call, Response<searchGroup> response) {
				if (response.body().getFlag().equals("success")){
					if (!("[]").equals(response.body().getResult().getListOwners().size())) {
						for (int i = 0; i <= response.body().getResult().getListOwners().size() - 1; i++) {
							EaseUser u = new EaseUser(response.body().getResult().getListOwners().get(i).getUuid().toString());
							u.setAvatar(response.body().getResult().getListOwners().get(i).getHeadImg());
							u.setNickname(response.body().getResult().getListOwners().get(i).getNickname());
//							mEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListOwners().get(i).getUserId()), u);

							if (!Config.AllEMFriendInfo.containsKey(""+String.valueOf(response.body().getResult().getListOwners().get(i).getUserId()))){
								Config.AllEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListOwners().get(i).getUserId()), u);
							}

						}
					}
//					if (!("[]").equals(response.body().getResult().getListAdmins().size())) {
//						for (int i = 0; i <= response.body().getResult().getListAdmins().size() - 1; i++) {
//							EaseUser u = new EaseUser(response.body().getResult().getListAdmins().get(i).getUuid().toString());
//							u.setAvatar(response.body().getResult().getListAdmins().get(i).getHeadImg());
//							u.setNickname(response.body().getResult().getListAdmins().get(i).getNickname());
//							mEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListAdmins().get(i).getUserId()), u);
//
//							if (!Config.AllEMFriendInfo.containsKey(""+String.valueOf(response.body().getResult().getListAdmins().get(i).getUserId()))){
//								Config.AllEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListAdmins().get(i).getUserId()), u);
//							}
//						}
//					}
					if (!("[]").equals(response.body().getResult().getListMembers().size())) {
						for (int i = 0; i <= response.body().getResult().getListMembers().size() - 1; i++) {
							EaseUser u = new EaseUser(response.body().getResult().getListMembers().get(i).getUuid().toString());
							u.setAvatar(response.body().getResult().getListMembers().get(i).getHeadImg());
							u.setNickname(response.body().getResult().getListMembers().get(i).getNickname());
//							mEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListMembers().get(i).getUserId()), u);

							if (!Config.AllEMFriendInfo.containsKey(""+String.valueOf(response.body().getResult().getListMembers().get(i).getUserId()))){
								Config.AllEMFriendInfo.put(""+String.valueOf(response.body().getResult().getListMembers().get(i).getUserId()), u);
							}
						}
					}
					//TODO 这里主要是根据用户uuid 存放自己的id 好填入自己的头像
					EaseUser u = new EaseUser(getUuId());
//					mEMFriendInfo.put(""+getUserId(), u);

					if (!Config.AllEMFriendInfo.containsKey(""+getUserId())){
						Config.AllEMFriendInfo.put(""+getUserId(), u);
					}

					// enter group chat
					Intent intent = new Intent(GroupsActivity.this, ChatActivity.class);
					// it is group chat
					intent.putExtra("chatType", Constant.CHATTYPE_GROUP);
					intent.putExtra("userId", groupAdapter.getItem(positions - 1).getGroupId());
					startActivityForResult(intent, 0);
					LoadingDialog.closeDialog(mDialog);
				}else{
					LoadingDialog.closeDialog(mDialog);
				}
				Log.i("GroupInfoActivity->", JSON.toJSONString(response.body()));
			}

			@Override
			public void onFailure(Call<searchGroup> call, Throwable t) {
				LoadingDialog.closeDialog(mDialog);
				Toast.makeText(GroupsActivity.this,"连接超时",Toast.LENGTH_SHORT).show();
			}
		});
	}
	//TODO 创建群组弹框
	private void CreateGroupDialog() {
		AlertDialog.Builder setDeBugDialog = new AlertDialog.Builder(this);
		//获取界面
		View dialogView = LayoutInflater.from(this).inflate(R.layout.em_create_group_dialog, null);
		//获取文本框输入的值
		EditText text = dialogView.findViewById(R.id.group_name_editText);
		//将界面填充到AlertDiaLog容器
		setDeBugDialog.setView(dialogView);

		// 取消点击外部消失弹窗
		setDeBugDialog.setCancelable(true);
		//创建AlertDiaLog
		final AlertDialog customAlert = setDeBugDialog.show();

		customAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        customAlert.getWindow().setLayout(getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(GroupsActivity.this, 100f), 700);
		customAlert.getWindow().setGravity(Gravity.CENTER);
		//下一步
		dialogView.findViewById(R.id.group_next_btn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//判断是否为空
				if (TextUtils.isEmpty(text.getText())){
					Toast.makeText(GroupsActivity.this,"请输入群名称",Toast.LENGTH_SHORT).show();
				}else {
					Intent intent = new Intent(GroupsActivity.this,SelectContactsActivity.class);
					intent.putExtra("groupName", text.getText().toString());
					startActivity(intent);
					customAlert.dismiss();
				}
			}
		});
		//取消
		dialogView.findViewById(R.id.group_cancel_btn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				customAlert.dismiss();
			}
		});

	}
	//TODO 获得用户头像
	private String getHeadImg(){
		return getSharedPreferences("User_data",MODE_PRIVATE).getString("userImg","");
	}
	//TODO 获得用户uuid
	private String getUuId(){
		return getSharedPreferences("User_data",MODE_PRIVATE).getString("uuid","");
	}
	//TODO 获得用户id
	private int getUserId(){
		int userId;
		if (App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
			userId = 0;
			return userId;
		}else {
			userId = Integer.valueOf(App.CONTEXT.getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
		}
		return userId;
	}
}
