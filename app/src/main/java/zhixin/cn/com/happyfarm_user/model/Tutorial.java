package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by Administrator on 2018/3/21.
 */

public class Tutorial {
    private String name;
    private String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Tutorial(String name , String image) {
        this.name = name;
        this.image = image;
    }
}
