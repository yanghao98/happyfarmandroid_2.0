package zhixin.cn.com.happyfarm_user.easyMob;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.friendRecommendation;
import zhixin.cn.com.happyfarm_user.model.searchDetail;
import zhixin.cn.com.happyfarm_user.other.AlertUtilOneButton;
import zhixin.cn.com.happyfarm_user.utils.TextString;

/**
 * Created by Administrator on 2018/5/23.
 */

public class AddFriendsActivity extends ABaseActivity {
    private Dialog dialog;
    private SearchView mSearchView;
    private RelativeLayout addFriendsBackView;
    private RecyclerView recyclerView;
    private AddFriendsAdapter adapter;
    private ArrayList<AddFriends> list = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;
    private TextView friendText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //启动activity时不自动弹出软键盘 //TODO 添加朋友页面软键盘
        hideKeyboard();
        setContentView(R.layout.add_friends_layout);
        TextView title = findViewById(R.id.title_name);
        title.setText("添加朋友");

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        initView();
        SearchView();
        hideKeyboardView();
        initRecyclerView();
        friendRec();
    }

    private void initView() {
        addFriendsBackView = findViewById(R.id.add_friends_back_view);
        mSearchView = findViewById(R.id.searchView);
        friendText = findViewById(R.id.friendRecommendation);
        //mSearchView 你的searchview对象  TODO 更改搜索图标
        ImageView searchButton = (ImageView)mSearchView.findViewById(R.id.search_mag_icon);
        //需求 —— 修改SearchView左边图标
        //重新设置ImageView的宽高，使其为自适应图片宽高
        LinearLayout.LayoutParams lpimg = new LinearLayout.LayoutParams(50, 50);
        lpimg.gravity = Gravity.CENTER_VERTICAL;
        searchButton.setLayoutParams(lpimg);
        searchButton.setImageResource(R.drawable.icon_search);
        //根据id-search_src_text获取TextView
        SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) mSearchView.findViewById(R.id.search_src_text);
        //修改字体大小
        searchText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
//        searchButton.setPadding(1,1,1,1);
        searchButton.setLeft(1);
        searchButton.setMaxWidth(25);
        searchButton.setMaxHeight(25);
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.add_friends_RecyclerView);

//        recyclerView.setBackground(R.layout);
        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new AddFriendsAdapter(AddFriendsActivity.this, list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(AddFriendsActivity.this, 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
        //调用按钮返回事件回调的方法
        adapter.buttonSetOnclick(new AddFriendsAdapter.ButtonInterface() {
            @Override
            public void onclick(View view, int position, int addFriendsId) {
                sendFriendApply(addFriendsId);
            }
        });

    }
    public void SearchView(){
        // 设置搜索文本监听
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            // 当点击搜索按钮时触发该方法
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (list != null){
                    list.clear();
                }
                dialog = LoadingDialog.createLoadingDialog(AddFriendsActivity.this,"搜索中...");
                HttpHelper.initHttpHelper().searchDetail(query).enqueue(new Callback<searchDetail>() {
                    @Override
                    public void onResponse(Call<searchDetail> call, Response<searchDetail> response) {
                        if (response.isSuccessful() && "success".equals(response.body().getFlag())){
                            friendText.setText("好友搜索");
                            LoadingDialog.closeDialog(dialog);
                            if (response.body().getResult().getList().isEmpty()) {
                                Toast.makeText(AddFriendsActivity.this,"未找到到您输入的用户",Toast.LENGTH_SHORT).show();
                            }else {
                                for (int i = 0; i < response.body().getResult().getList().size(); i++) {
                                    searchDetail.ResultBean.ListBean listBean = response.body().getResult().getList().get(i);
                                    AddFriends dataBean = new AddFriends();
                                    dataBean.AddFriendsImage = listBean.getHeadImg();
                                    dataBean.AddFriendsId = String.valueOf(listBean.getId());
                                    dataBean.AddFriendsNickname = String.valueOf(listBean.getNickname());
                                    list.add(dataBean);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<searchDetail> call, Throwable t) {
                        LoadingDialog.closeDialog(dialog);
                    }
                });
                return false;
            }

            // 当搜索内容改变时触发该方法
            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });
    }
    //TODO 好友推荐
    private void friendRec(){
        //dialog = LoadingDialog.createLoadingDialog(AddFriendsActivity.this,"");
        HttpHelper.initHttpHelper().friendRecommendation().enqueue(new Callback<friendRecommendation>() {
            @Override
            public void onResponse(Call<friendRecommendation> call, Response<friendRecommendation> response) {
                if (response.isSuccessful() && response.body().getFlag().equals("success")){
                    LoadingDialog.closeDialog(dialog);
                    if (response.body().getResult().getList().isEmpty()) {
//                        Toast.makeText(AddFriendsActivity.this,"没有查到你输入的用户",Toast.LENGTH_SHORT).show();
                    }else {
                        for (int i = 0; i < response.body().getResult().getList().size(); i++) {
                            friendRecommendation.ResultBean.ListBean listBean = response.body().getResult().getList().get(i);
                            AddFriends dataBean = new AddFriends();
                            dataBean.AddFriendsImage = listBean.getHeadImg();
                            dataBean.AddFriendsId = String.valueOf(listBean.getId());
                            dataBean.AddFriendsNickname = String.valueOf(listBean.getNickname());
                            list.add(dataBean);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<friendRecommendation> call, Throwable t) {
                LoadingDialog.closeDialog(dialog);
            }
        });
    }
    //TODO 发送好友申请接口
    private void sendFriendApply(int addFriendsId){
        HttpHelper.initHttpHelper().addApply(addFriendsId).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                Log.e("好友：", JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())) {
                    //参数为要添加的好友的username和添加理由
                    try {
                        EMClient.getInstance().contactManager().addContact(String.valueOf(addFriendsId), "");
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                    }
                    NormalDialogOneBtn("好友申请已发送，等待对方确认");
                } else {
                    NormalDialogOneBtn(response.body().getResult());
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                Toast.makeText(AddFriendsActivity.this, TextString.NetworkRequestFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void NormalDialogOneBtn(String pickupCode) {
        final AlertUtilOneButton diyDialog = new AlertUtilOneButton(this);
        diyDialog.setOk("确定")
                .setContent(pickupCode)
                .setDialogClickListener(new AlertUtilOneButton.DialogClickListener() {
                    @Override
                    public void ok() {
                        diyDialog.cancle();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }
    public void back(View view) {
        finish();
    }
    //隐藏键盘
    public void hideKeyboardView(){
        addFriendsBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }

    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm =  (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}
