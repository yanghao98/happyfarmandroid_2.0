package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

/**
 * Created by DELL on 2018/2/5.
 */

public class LandList {


    /**
     * result : {"pageNum":1,"pageSize":10,"size":7,"orderBy":" land_no ASC","startRow":1,"endRow":7,"total":7,"pages":1,"list":[{"id":11,"x":24,"y":90,"rent":90,"state":1,"user_id":127},{"id":3,"x":34,"y":2,"rent":30,"state":1},{"id":15,"x":2,"y":3,"rent":40,"state":1},{"id":13,"x":3,"y":32,"rent":33,"state":1,"user_id":7},{"id":14,"x":2,"y":32,"rent":33,"state":1,"user_id":136},{"id":12,"x":22,"y":12,"rent":33,"state":1,"user_id":129},{"id":2,"x":53,"y":32,"rent":0,"state":1,"user_id":33}],"firstPage":1,"prePage":0,"nextPage":0,"lastPage":1,"isFirstPage":true,"isLastPage":true,"hasPreviousPage":false,"hasNextPage":false,"navigatePages":8,"navigatepageNums":[1]}
     * flag : success
     * message : {"maxNo":454,"maxX":53,"minX":2,"maxY":90,"minY":2}
     */

    private ResultBean result;
    private String flag;
    private MessageBean message;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public static class ResultBean {
        /**
         * pageNum : 1
         * pageSize : 10
         * size : 7
         * orderBy :  land_no ASC
         * startRow : 1
         * endRow : 7
         * total : 7
         * pages : 1
         * list : [{"id":11,"x":24,"y":90,"rent":90,"state":1,"user_id":127},{"id":3,"x":34,"y":2,"rent":30,"state":1},{"id":15,"x":2,"y":3,"rent":40,"state":1},{"id":13,"x":3,"y":32,"rent":33,"state":1,"user_id":7},{"id":14,"x":2,"y":32,"rent":33,"state":1,"user_id":136},{"id":12,"x":22,"y":12,"rent":33,"state":1,"user_id":129},{"id":2,"x":53,"y":32,"rent":0,"state":1,"user_id":33}]
         * firstPage : 1
         * prePage : 0
         * nextPage : 0
         * lastPage : 1
         * isFirstPage : true
         * isLastPage : true
         * hasPreviousPage : false
         * hasNextPage : false
         * navigatePages : 8
         * navigatepageNums : [1]
         */

        private int pageNum;
        private int pageSize;
        private int size;
        private String orderBy;
        private int startRow;
        private int endRow;
        private int total;
        private int pages;
        private int firstPage;
        private int prePage;
        private int nextPage;
        private int lastPage;
        private boolean isFirstPage;
        private boolean isLastPage;
        private boolean hasPreviousPage;
        private boolean hasNextPage;
        private int navigatePages;
        private List<ListBean> list;
        private List<Integer> navigatepageNums;

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public String getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(String orderBy) {
            this.orderBy = orderBy;
        }

        public int getStartRow() {
            return startRow;
        }

        public void setStartRow(int startRow) {
            this.startRow = startRow;
        }

        public int getEndRow() {
            return endRow;
        }

        public void setEndRow(int endRow) {
            this.endRow = endRow;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(int firstPage) {
            this.firstPage = firstPage;
        }

        public int getPrePage() {
            return prePage;
        }

        public void setPrePage(int prePage) {
            this.prePage = prePage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public boolean isIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public boolean isIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public boolean isHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public int getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(int navigatePages) {
            this.navigatePages = navigatePages;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<Integer> getNavigatepageNums() {
            return navigatepageNums;
        }

        public void setNavigatepageNums(List<Integer> navigatepageNums) {
            this.navigatepageNums = navigatepageNums;
        }

        public static class ListBean {
            /**
             * id : 11
             * x : 24
             * y : 90
             * rent : 90
             * state : 1
             * user_id : 127
             */

            private int id;
            private int x;
            private int y;
            private int rent;
            private int state;
            private int user_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getX() {
                return x;
            }

            public void setX(int x) {
                this.x = x;
            }

            public int getY() {
                return y;
            }

            public void setY(int y) {
                this.y = y;
            }

            public int getRent() {
                return rent;
            }

            public void setRent(int rent) {
                this.rent = rent;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }
        }
    }

    public static class MessageBean {
        /**
         * maxNo : 454
         * maxX : 53
         * minX : 2
         * maxY : 90
         * minY : 2
         */

        private int maxNo;
        private int maxX;
        private int minX;
        private int maxY;
        private int minY;

        public int getMaxNo() {
            return maxNo;
        }

        public void setMaxNo(int maxNo) {
            this.maxNo = maxNo;
        }

        public int getMaxX() {
            return maxX;
        }

        public void setMaxX(int maxX) {
            this.maxX = maxX;
        }

        public int getMinX() {
            return minX;
        }

        public void setMinX(int minX) {
            this.minX = minX;
        }

        public int getMaxY() {
            return maxY;
        }

        public void setMaxY(int maxY) {
            this.maxY = maxY;
        }

        public int getMinY() {
            return minY;
        }

        public void setMinY(int minY) {
            this.minY = minY;
        }
    }
}
