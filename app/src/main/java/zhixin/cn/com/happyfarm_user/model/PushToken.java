package zhixin.cn.com.happyfarm_user.model;

/**
 * PushToken
 *
 * @author: Administrator.
 * @date: 2019/2/20
 */

public class PushToken {


    /**
     * result : 身份信息失效
     * flag : failed
     * errCode : -102
     */

    private String result;
    private String flag;
    private int errCode;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }
}
