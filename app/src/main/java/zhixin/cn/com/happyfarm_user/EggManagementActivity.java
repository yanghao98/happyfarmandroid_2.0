package zhixin.cn.com.happyfarm_user;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextPaint;
import android.view.View;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.Adapter.ViewPagerAdapter;
import zhixin.cn.com.happyfarm_user.Page.EggRechargePage;
import zhixin.cn.com.happyfarm_user.Page.EggSendBackPage;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;

/**
 * EggManagementActivity
 *
 * @author: Administrator.
 * @date: 2019/5/17
 */
public class EggManagementActivity extends ABaseActivity {

    private List<Fragment> frags;
    private List<String> titles;
    private ViewPager pager;
    private Float eggDiamond;
    private TextView eggNumber;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.egg_management_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this,getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText(getResources().getString(R.string.egg_title));
        initData();
        initPager();
        initIndicator();
        eggDiamond = getIntent().getFloatExtra("Diamond", 0.0F);
        eggNumber = findViewById(R.id.egg_number);
        eggNumber.setText("余额：" + eggDiamond + "元");
    }

    private void initData() {
        frags = new ArrayList<>();
        frags.add(new EggRechargePage());
//        frags.add(new EggSendBackPage());
        titles=new ArrayList<>();
        titles.add("充值");
//        titles.add("提现");
    }

    private void initPager() {
        pager = findViewById(R.id.egg_manager_viewpager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), frags);
        pager.setAdapter(adapter);
    }

    private void initIndicator() {
        MagicIndicator indicator = findViewById(R.id.top_indicator);
        CommonNavigator navigator = new CommonNavigator(this);
        navigator.setAdjustMode(true);//设置平分屏幕宽度
        navigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return frags.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int i) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(titles.get(i));
                titleView.setTextSize(16);
                TextPaint tp = titleView.getPaint();
                tp.setFakeBoldText(true);
                titleView.setNormalColor(Color.parseColor("#9a9a9a"));//未选中颜色
                titleView.setSelectedColor(Color.parseColor("#1DAC6D"));//选中颜色
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pager.setCurrentItem(i);
                    }
                });
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);//设置下滑线  自适应文字长度MODE_WRAP_CONTENT MODE_MATCH_EDGE
                linePagerIndicator.setColors(Color.parseColor("#1DAC6D"));
                return linePagerIndicator;
            }
        });
        indicator.setNavigator(navigator);
        /**设置中间竖线**/
//        LinearLayout titleContainer = navigator.getTitleContainer(); // must after setNavigator
//        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
//        titleContainer.setDividerPadding(UIUtil.dip2px(this, 20));
//        titleContainer.setDividerDrawable(getResources().getDrawable(R.drawable.simple_splitter));
        /**设置中间竖线**/
        //绑定viewpager滚动事件
        ViewPagerHelper.bind(indicator, pager);
    }
    public Float getEggDiamond() {
        return eggDiamond;
    }
    @Override
    public void back(View view) {
        finish();
    }
}
