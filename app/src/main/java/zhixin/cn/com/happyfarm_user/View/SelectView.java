package zhixin.cn.com.happyfarm_user.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.animation.ScaleAnimation;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.model.NewPoint;

public class SelectView extends View {

    private Paint myPaint;
    private Paint otherPaint;
    private Paint selectPaint;
    private Paint availablePaint;
    private Paint poultryPaint;
    private Paint unAvailablePaint;
    private Paint textPaint;
    private List<NewPoint> values = new ArrayList<>();
    private int space = (int) dp2Px(10);//TODO 应该是设置地块间距
    private int pointW = (int) dp2Px(35);//TODO 设置地块大小
//    private int minPointW = (int) dp2Px(30);
    private int width = 0;
    private int height = 0;
    private SelectListener mListener;
    private ScaleGestureDetector mScaleGestureDetector;
    private GestureDetector mGestureDetector;

    private NewPoint selectPoint;

    public static char Available = 'A';//可用蔬菜
    public static char UnAvailable = 'U';//禁用
    public static char Other = 'O';//他人
    public static char My = 'M';//我的
    public static char Poultry = 'P';//家禽
    private static final float MIN_MOVE_DISTANCE = 15;//TODO 分钟移动距离
    private long mStartTime;
    private float mCurrentScale;
    private float mPreScale;
    private final String TAG = "SelectView->";
    public SelectView(Context context) {
        super(context);
    }

    public SelectView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        myPaint = new Paint();
        myPaint.setColor(0xFFFDBF76);//自己租地颜色
        availablePaint = new Paint();
        availablePaint.setColor(0xFF1DAC6D);//蔬菜租地颜色
        unAvailablePaint = new Paint();
        unAvailablePaint.setColor(0xFFAAAAAA);//禁用租地颜色
        otherPaint = new Paint();
        otherPaint.setColor(0xFFF39199);//他人租地颜色
        poultryPaint = new Paint();
        poultryPaint.setColor(0xFFD282D2);//家禽租地颜色（透明）00 –> FF（不透明）

        //文字绘制
        textPaint = new Paint();
        textPaint.setTextSize(dp2Px(8));//TODO 设置地块地号大小
        textPaint.setColor(Color.WHITE);
        textPaint.setTextAlign(Paint.Align.CENTER);

        //选中绘制
        selectPaint = new Paint();
        selectPaint.setColor(0xFFFFFF00);
        selectPaint.setStrokeWidth(dp2Px(3));
        selectPaint.setStyle(Paint.Style.STROKE);

        mGestureDetector = new GestureDetector(context, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                //Log.e(TAG, "onSingleTapUp: "+ e.getAction() );
                for (NewPoint point : SelectView.this.values) {
                        if (e.getX() > point.getxC() && e.getX() < (point.getxC() + pointW) && e.getY() > point.getyC() && (e.getY() < point.getyC() + pointW)) {
                            selectPoint = point;
                            SelectView.this.mListener.select(point);
                            invalidate();
                            break;
                        }
                }
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        mScaleGestureDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                //Log.i(TAG,"onScale"+detector.getScaleFactor());
                if (mPreScale == 0){
                    mPreScale = detector.getScaleFactor();
                }
                mCurrentScale = detector.getScaleFactor() * mPreScale;
                if(mCurrentScale < 0.5 || mCurrentScale > 2){
                    return true;
                }else {
                    setScaleX(mCurrentScale);
                    setScaleY(mCurrentScale);
                    mPreScale = mCurrentScale;
//                    SelectView.this.width = (int) (SelectView.this.width_s * mCurrentScale);
//                    SelectView.this.height = (int) (SelectView.this.height_s * mCurrentScale);
//                    requestLayout();
                    return false;
                }
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                //Log.i(TAG,"begin"+detector);
                setPivotX(detector.getFocusX());
                setPivotY(detector.getFocusY());
                return true;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
                //Log.i(TAG,"end"+detector);
            }
        });
    }

    public void unSelect(){
        selectPoint = null;
        invalidate();
    }

    /**
     * 绘画租地的位置与宽高
     * @param values
     * @param minX
     * @param maxX
     * @param minY
     * @param maxY
     */
    public void setValues(List<NewPoint> values,int minX, int maxX, int minY, int maxY) {
        this.width = (maxX - minX + 1) * (pointW + space) + space;
        this.height = (maxY - minY + 1) * (pointW + space) + space;
        this.values = values;
        for (NewPoint point : values) {
            int xc = point.getLand().getX() * (pointW + space) + space;
            int yc = point.getLand().getY() * (pointW + space) + space;
            point.setxC(xc - minX * (pointW + space));
            point.setyC(yc - minY * (pointW + space));
        }

        requestLayout();
//        sw = getResources().getDisplayMetrics().widthPixels / (float)this.width;
    }
    private float sw = 0;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for (NewPoint p : values) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (p.getType() == Available) {
                    canvas.drawRoundRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, 15, 15, availablePaint);
                } else if (p.getType() == UnAvailable) {
                    canvas.drawRoundRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, 15, 15, unAvailablePaint);
                } else if (p.getType() == My) {
                    canvas.drawRoundRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, 15, 15, myPaint);
                } else if (p.getType() == Other) {
                    canvas.drawRoundRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, 15, 15, otherPaint);
                } else if (p.getType() == Poultry) {
                    canvas.drawRoundRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, 15, 15, poultryPaint);
                }
            } else {
                if (p.getType() == Available) {
                    canvas.drawRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, availablePaint);
                } else if (p.getType() == UnAvailable) {
                    canvas.drawRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, unAvailablePaint);
                } else if (p.getType() == My) {
                    canvas.drawRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, myPaint);
                } else if (p.getType() == Other) {
                    canvas.drawRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, otherPaint);
                } else if (p.getType() == Poultry) {
                    canvas.drawRect(p.getxC(), p.getyC(), p.getxC() + pointW, p.getyC() + pointW, poultryPaint);
                }
            }
            //设置地号内容
            canvas.drawText(String.valueOf(p.getLand().getAlias()), p.getxC() + pointW / 2, p.getyC() + pointW / 2, textPaint);
        }
        if (selectPoint != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                canvas.drawRoundRect();
                canvas.drawRoundRect(selectPoint.getxC(), selectPoint.getyC(), selectPoint.getxC() + pointW, selectPoint.getyC() + pointW, 15, 15, selectPaint);
            } else {
                canvas.drawRect(selectPoint.getxC(), selectPoint.getyC(), selectPoint.getxC() + pointW, selectPoint.getyC() + pointW, selectPaint);
            }
        }

    }

    private boolean isScale;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        getParent().requestDisallowInterceptTouchEvent(true);
        mScaleGestureDetector.onTouchEvent(event);
        mGestureDetector.onTouchEvent(event);
        return true;
    }

    public interface SelectListener {
        void select(NewPoint point);
    }

    public void setSelectListener(SelectListener listener) {
        mListener = listener;
    }

    private float dp2Px(int dp) {
        float density = getContext().getResources().getDisplayMetrics().density;
        return (float) (dp * density + 0.5);
    }

    private float mFingerSpace;
    /**放大*/
    public final static int AMPLIFICATION = 7;
    /**缩小*/
    public final static int NARROW = 8;

    private int mTouchMode;

    private void getGestures(MotionEvent event) {

        if (event.getPointerCount() >= 2) {

            float centerX = (event.getX(0) + event.getX(1)) / 2;
            float centerY = (event.getY(0) + event.getY(1)) / 2;
            float x1 = event.getX(0) - event.getX(1);
            float y1 = event.getY(0) - event.getY(1);

            float value = (float) Math.sqrt(x1 * x1 + y1 * y1);
            if (mFingerSpace == 0) {
                mFingerSpace = value;
            } else {
                if (System.currentTimeMillis() - mStartTime > 50) {
                    float fingerDistanceChange = value - mFingerSpace;
                    if (Math.abs(fingerDistanceChange) > MIN_MOVE_DISTANCE) {
                        float scale = value / mFingerSpace;
                        if (scale > 1) {
                            mTouchMode = AMPLIFICATION;
                            ScaleAnimation scaleAnimation = new ScaleAnimation(1,2,1,2);
                            scaleAnimation.setDuration(500);
                            scaleAnimation.setFillAfter(true);
                            startAnimation(scaleAnimation);

                        } else {
                            mTouchMode = NARROW;
                            ScaleAnimation scaleAnimation = new ScaleAnimation(2,1,2,1);
                            scaleAnimation.setDuration(500);
                            scaleAnimation.setFillAfter(true);
                            startAnimation(scaleAnimation);

                        }

                        Log.i("DEBUG", "getGestures: " + mTouchMode);
                        mStartTime = System.currentTimeMillis();
                        mFingerSpace = value;
                    }
                }
            }
        }
    }
}
