package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.yanzhenjie.recyclerview.swipe.SwipeItemClickListener;
import com.yanzhenjie.recyclerview.swipe.SwipeMenu;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuBridge;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuCreator;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuItem;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuItemClickListener;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.CuringAdapter;
import zhixin.cn.com.happyfarm_user.Adapter.MyAlbumAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.MyAlbum;
import zhixin.cn.com.happyfarm_user.model.SearchPhotoAlbum;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;
import zhixin.cn.com.happyfarm_user.other.DateUtil;
import zhixin.cn.com.happyfarm_user.other.GridSpacingItemDecoration;
import zhixin.cn.com.happyfarm_user.utils.Config;

public class MyAlbumActivity extends ABaseActivity {

    private ArrayList<MyAlbum> mdata = new ArrayList<>();
    private SwipeMenuRecyclerView collection_recycle;
    private Dialog dialog;
    private RefreshLayout refreshLayout;
    private RelativeLayout myAlbumPrompt;
    private MyAlbumAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private int pageNum;
    private int pageSize;
    private Boolean isLastPage = false;
    private String TAG = "MyAlbumActivity";
    private Dialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_album_layout);
        //加载没有内容提示布局
        myAlbumPrompt = findViewById(R.id.my_album_prompt);
        //加载之前先让暂时还没有内容不提示
        myAlbumPrompt.setVisibility(View.GONE);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("成长相册");

        pageNum = 1;
        pageSize = 5;

        refreshLayout = (RefreshLayout) findViewById(R.id.refreshLayout);
        refreshLayout.setEnableHeaderTranslationContent(false);
        //取消上拉加载
        refreshLayout.setEnableLoadMore(false);
        //设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
        refreshLayout.setRefreshHeader(new MaterialHeader(MyAlbumActivity.this).setShowBezierWave(false));

        initRecyclerView();
//        StaggerLoadData(false,1);
        if (!mdata.isEmpty()) {
            mdata.clear();
        }
        StaggerLoadData(false, 1);
        topRefreshLayout();
        AutoLoading();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Config.myAlbumRefresh){
            Config.myAlbumRefresh = false;
            if (!mdata.isEmpty()) {
                mdata.clear();
            }
            StaggerLoadData(false, 1);
        }
    }

    private void StaggerLoadData(Boolean inversion, int pageNum) {
        dialog = LoadingDialog.createLoadingDialog(MyAlbumActivity.this, "数据正在加载");
//        Log.d(TAG, "StaggerLoadData: "+"数据正在加载");
        HttpHelper.initHttpHelper().searchPhotoAlbum(0, pageNum, 5).enqueue(new Callback<SearchPhotoAlbum>() {
            @Override
            public void onResponse(Call<SearchPhotoAlbum> call, Response<SearchPhotoAlbum> response) {
//                Log.e("MyAlbumActivity->", JSON.toJSONString(response.body().getResult().getList()));
                if (("success").equals(response.body().getFlag())) {
                    isLastPage = response.body().getResult().isIsLastPage();
                    if (String.valueOf(response.body().getResult().getList()).equals("[]")) {
                        myAlbumPrompt.setVisibility(View.VISIBLE);
                        collection_recycle.setVisibility(View.GONE);
                        LoadingDialog.closeDialog(dialog);
                    } else {

                        myAlbumPrompt.setVisibility(View.GONE);
                        collection_recycle.setVisibility(View.VISIBLE);
                        for (int i = 0; i < response.body().getResult().getList().size(); i++) {
//                            Log.d(TAG,"成长相册里的相册详情："+ JSON.toJSONString(response.body().getResult().getList().get(i).getAlbumFirstPicture()));
                            MyAlbum myAlbum = new MyAlbum();
                            myAlbum.albumName = response.body().getResult().getList().get(i).getAlbumName();
                            myAlbum.photoAbstract = String.valueOf(response.body().getResult().getList().get(i).getPhotoAbstract());
                            myAlbum.isPublic = String.valueOf(response.body().getResult().getList().get(i).getIsPublic());
                            myAlbum.videoUrl = response.body().getResult().getList().get(i).getPhotoUrl();
                            myAlbum.Times = DateUtil.getStrTime(String.valueOf(response.body().getResult().getList().get(i).getStartTime()));
                            myAlbum.albumFirstPicture = response.body().getResult().getList().get(i).getAlbumFirstPicture();
//                            Log.e("MyAlbumActivity->", DateUtil.getStrTime(String.valueOf(response.body().getResult().getList().get(i).getStartTime())));
                            myAlbum.id = String.valueOf(response.body().getResult().getList().get(i).getId());
                            mdata.add(myAlbum);
                        }
                        LoadingDialog.closeDialog(dialog);
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    myAlbumPrompt.setVisibility(View.VISIBLE);
                    collection_recycle.setVisibility(View.GONE);
                    LoadingDialog.closeDialog(dialog);
                }
            }

            @Override
            public void onFailure(Call<SearchPhotoAlbum> call, Throwable t) {
                myAlbumPrompt.setVisibility(View.VISIBLE);
                collection_recycle.setVisibility(View.GONE);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }

    private void initRecyclerView() {

        int spanCount = 1;
        int spacing = 50;
        boolean includeEdge = true;
        collection_recycle = findViewById(R.id.album_recycle);

        // 设置菜单创建器。
        collection_recycle.setSwipeMenuCreator(swipeMenuCreator);
        // 设置菜单Item点击监听。
        collection_recycle.setSwipeMenuItemClickListener(mMenuItemClickListener);

        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new MyAlbumAdapter(MyAlbumActivity.this, mdata);
        //设置适配器
        collection_recycle.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(MyAlbumActivity.this, 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);

        collection_recycle.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        collection_recycle.setLayoutManager(gridLayoutManager);
        //TODO 设置一行一个 ，左右间距 50
        collection_recycle.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        adapter.setItemClickListener(new CuringAdapter.MyItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(MyAlbumActivity.this, AlbumDetailsActivity.class);
                intent.putExtra("albumName", mdata.get(position).albumName);
                intent.putExtra("photoAbstract", mdata.get(position).photoAbstract);
                intent.putExtra("isPublic", mdata.get(position).isPublic);
                intent.putExtra("videoUrl", mdata.get(position).videoUrl);
                intent.putExtra("id", mdata.get(position).id);
                intent.putExtra("userGrowthAlbum",mdata.get(position).albumFirstPicture);
                startActivity(intent);
            }
        });
    }

    /**
     * Todo 菜单创建器。在Item要创建菜单的时候调用。
     */
    private SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
        @Override
        public void onCreateMenu(SwipeMenu swipeLeftMenu, SwipeMenu swipeRightMenu, int viewType) {
            int height = AbsListView.LayoutParams.MATCH_PARENT;//.setHeight(ViewGroup.LayoutParams.MATCH_PARENT)
//            Log.i("item高度打印数据：", String.valueOf(height)+String.valueOf(heightView));
            SwipeMenuItem deleteItem = new SwipeMenuItem(MyAlbumActivity.this)
                    .setText("删除") // 文字。
                    .setHeight(height)//设置高，这里使用match_parent，就是与item的高相同
                    .setWidth(300)//设置宽
                    .setBackground(new ColorDrawable(0xFFFF0000))
                    .setTextColor(Color.WHITE) // 文字颜色。
                    .setTextSize(16);// 文字大小。
            swipeRightMenu.addMenuItem(deleteItem);// 添加一个按钮到右侧侧菜单。
        }
    };
    /**
     *TODO 菜单点击监听。
     */
    private SwipeMenuItemClickListener mMenuItemClickListener = new SwipeMenuItemClickListener() {
        @Override
        public void onItemClick(SwipeMenuBridge menuBridge) {
            // 任何操作必须先关闭菜单，否则可能出现Item菜单打开状态错乱。
            menuBridge.closeMenu();
            int direction = menuBridge.getDirection(); // 左侧还是右侧菜单。
            int adapterPosition = menuBridge.getAdapterPosition(); // RecyclerView的Item的position。
            int menuPosition = menuBridge.getPosition(); // 菜单在RecyclerView的Item中的Position。
            switch (menuPosition){
                case 0 :
                    MaterialDialogDefault(menuPosition,adapterPosition);
                    break;
            }
        }
    };
    //TODO item删除弹框
    private void MaterialDialogDefault(int menuPosition, int adapterPosition) {
        final AlertUtilBest diyDialog =new AlertUtilBest(this);
        diyDialog.setCancel("取消")
                .setOk("确定")
                .setContent("您确定要删除该视频吗？")
                .setDialogClickListener(new AlertUtilBest.DialogClickListener() {
                    @Override
                    public void cancel() {
                        diyDialog.cancel();
                    }
                    @Override
                    public void ok() {
                        mDialog = LoadingDialog.createLoadingDialog(MyAlbumActivity.this,"");
                        HttpHelper.initHttpHelper().deletePhotoAlbum(Integer.valueOf(mdata.get(adapterPosition).id)).enqueue(new Callback<CheckSMS>() {
                            @Override
                            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                                LoadingDialog.closeDialog(mDialog);
                                if ("success".equals(response.body().getFlag())) {
                                    Toast.makeText(MyAlbumActivity.this,"删除成功" ,Toast.LENGTH_SHORT).show();
                                    if (!mdata.isEmpty())
                                        mdata.clear();
                                    StaggerLoadData(false, 1);
                                }else {
                                    Toast.makeText(MyAlbumActivity.this,"删除失败",Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<CheckSMS> call, Throwable t) {
                                Toast.makeText(MyAlbumActivity.this,"网络错误，稍后重试",Toast.LENGTH_SHORT).show();
                                LoadingDialog.closeDialog(mDialog);
                            }
                        });
                        diyDialog.cancel();
                    }
                });
        diyDialog.builder();
        diyDialog.show();
    }

    public void AutoLoading() {
        //上拉滑动自动请求数据
        collection_recycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                    if (lastVisiblePosition >= gridLayoutManager.getItemCount() - 1) {
                        if (isLastPage == true) {
//                            Toast.makeText(getApplication(), "没有更多数据", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            StaggerLoadData(false, pageNum);
                        }
                    }
                }
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (mdata != null)
                    mdata.clear();
                StaggerLoadData(false, 1);
                refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
            }
        });
    }

    public void back(View view) {
        finish();
    }
}
