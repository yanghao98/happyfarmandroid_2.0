package zhixin.cn.com.happyfarm_user;

/**
 * Created by HuYueling on 2018/3/19.
 */

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import java.util.ArrayList;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Adapter.MyDynamicAdapter;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.MyDynamic;
import zhixin.cn.com.happyfarm_user.model.SeachTrack;


public class MyDynamicActivity extends ABaseActivity {

    private RecyclerView recyclerView;
    private RefreshLayout mRefreshLayout;
    private RelativeLayout myDynamicPrompt;
    private String UserImg;
    private String UserNickName;
    private int pageNum;
    private int pageSize;
    private int total;
    private Dialog dialog;
    private Boolean isLastPage = false;
    private MyDynamicAdapter adapter;
    private ArrayList<MyDynamic> list = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //      setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);// 横屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏
        setContentView(R.layout.my_dynamic_layout);
        pageNum = 1;
        pageSize = 15;
        mRefreshLayout = (RefreshLayout) findViewById(R.id.refreshLayout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        Intent intent = getIntent();
        UserImg = intent.getStringExtra("UserImg");
        UserNickName = intent.getStringExtra("UserNickName");
//        Log.i("info", "UserNickName: " + UserNickName);
//        Log.i("info", "UserImg: " + UserImg);

        TextView title = findViewById(R.id.title_name);
        title.setText("我的动态");
        myDynamicPrompt = findViewById(R.id.my_dynamic_prompt);
        initRecyclerView();
        StaggerLoadData(false, UserImg, UserNickName, 1);
        topRefreshLayout();
        bottomRefreshLayout();

        //上拉滑动自动请求数据
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    int lastVisiblePosition = gridLayoutManager.findLastVisibleItemPosition();
                    if(lastVisiblePosition >= gridLayoutManager.getItemCount() - 1){
                        if (isLastPage == true) {
//                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                        } else {
                            pageNum++;
                            StaggerLoadData(false, UserImg, UserNickName, pageNum);

                        }
                    }
                }
            }
        });
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);

        //创建适配器Adapter对象  参数：1.上下文2.数据加载集合
        adapter = new MyDynamicAdapter(MyDynamicActivity.this, list);
        //设置适配器
        recyclerView.setAdapter(adapter);
        //GridLayoutManager布局管理器,布局管理器所需参数:1.上下文 2.item的列数
        gridLayoutManager = new GridLayoutManager(MyDynamicActivity.this, 1);
        //通过布局管理器控制条目排列的顺序  true:反向显示 false:正向显示
        gridLayoutManager.setReverseLayout(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //设置布局管理器，参数StaggeredGridLayoutManager，可以是RecyclerView实现和StaggeredGridView一样的效果
        recyclerView.setLayoutManager(gridLayoutManager);
    }

    private void StaggerLoadData(Boolean inversion, String UserImg, String UserName, int pageNum) {
//        Log.i("info", "StaggerLoadData: "+ pageNum + " size = "+pageSize);
        dialog = LoadingDialog.createLoadingDialog(MyDynamicActivity.this, "正在加载数据");
        retrofit2.Call<SeachTrack> searchIrrationInfo = HttpHelper.initHttpHelper().seachTrack(pageNum, pageSize);
        searchIrrationInfo.enqueue(new Callback<SeachTrack>() {
            @Override
            public void onResponse(retrofit2.Call<SeachTrack> call, Response<SeachTrack> response) {
//                Log.e("UserLogs", JSON.toJSONString(response.body()));
                //集合对象
                if ("success".equals(response.body().getFlag())) {
                    total = response.body().getResult().getTotal();
                    isLastPage = response.body().getResult().isIsLastPage();
                    if (response.body().getResult().getList().isEmpty()) {
                        myDynamicPrompt.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        myDynamicPrompt.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        //给DataBean类放数据，最后把装好数据的DataBean类放到集合里
                        for (int i = 0; i <= response.body().getResult().getList().size() - 1; i++) {
                            MyDynamic dataBean = new MyDynamic();
//                        dataBean.textTitle = String.valueOf(response.body().getResult().getList().get(i).getCommenterId());
                            dataBean.textContent = String.valueOf(response.body().getResult().getList().get(i).getContent());
                            if (response.body().getResult().getList().get(i).getCommenterType() == 1) {
                                dataBean.textTitle = UserName;
                                dataBean.userImg = UserImg;
                            } else if (response.body().getResult().getList().get(i).getCommenterType() == 2) {
                                dataBean.textTitle = "农民伯伯";
                            } else {
                                dataBean.textTitle = "系统消息";
                            }
                            if (response.body().getResult().getList().get(i).getIsIrrational() == -1) {
                                dataBean.isIrrational = "";
                            } else if (response.body().getResult().getList().get(i).getIsIrrational() == 0) {
                                //不合理
                                dataBean.isIrrational = "不合理";
                            } else {
                                //合理
                                dataBean.isIrrational = "合理";
                            }
                            dataBean.dynamicTime = response.body().getResult().getList().get(i).getStartTime();
                            list.add(dataBean);
                        }
                        adapter.notifyDataSetChanged();

                    }
                } else {
                    myDynamicPrompt.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    Toast.makeText(MyDynamicActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
                }
                LoadingDialog.closeDialog(dialog);
            }

            @Override
            public void onFailure(retrofit2.Call<SeachTrack> call, Throwable t) {
                myDynamicPrompt.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }

    //下拉刷新
    public void topRefreshLayout() {
        mRefreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
        //设置一个下拉监听器
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
                if (list != null)
                    list.clear();
                isLastPage = false;
                pageNum = 1;
                refreshlayout.setNoMoreData(false);//TODO 恢复没有更多数据的原始状态
                StaggerLoadData(false, UserImg, UserNickName, 1);
                refreshlayout.finishRefresh(0/*,false*/);//传入false表示刷新失败
            }
        });
    }

    //上拉加载
    public void bottomRefreshLayout() {
//        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore(RefreshLayout refreshlayout) {
//                if (isLastPage == true) {
//                    Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
////                    refreshlayout.finishLoadMore();
//                    refreshlayout.finishLoadMoreWithNoMoreData();//将不会再次触发加载更多事件
//                } else {
//                    Toast.makeText(getApplication(), "数据正在加载...", Toast.LENGTH_SHORT).show();
//                    pageNum++;
//                    StaggerLoadData(false, UserImg, UserNickName, pageNum);
////                            adapter.loadMore(initData());
//                    refreshlayout.finishLoadMore();
//                }
//                refreshlayout.finishLoadMore(2000/*,false*/);//传入false表示加载失败
//            }
//        });
        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                refreshLayout.getLayout().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        Log.i("info", "上拉刷新: "+isLastPage);

                        if (isLastPage == true) {
                            Toast.makeText(getApplication(), "数据全部加载完毕", Toast.LENGTH_SHORT).show();
                            refreshLayout.finishLoadMore();
                            refreshLayout.finishLoadMoreWithNoMoreData();//TODO 将不会再次触发加载更多事件
                        } else {
                            pageNum++;
                            StaggerLoadData(false, UserImg, UserNickName, pageNum);
                            refreshLayout.finishLoadMore();
                        }
                    }
                }, 0);
            }
        });
    }

    public void back(View view) {
        finish();
    }

    /**
     * 重写返回键
     * 监听Back键按下事件
     * super.onBackPressed()会自动调用finish()方法,关闭当前Activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
