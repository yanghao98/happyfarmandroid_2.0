package zhixin.cn.com.happyfarm_user.popupMenu.model;

/**
 * PopupMenu
 *
 * @author: Administrator.
 * @date: 2019/7/12
 */
public class PopupMenu {

    private Integer viewId;

    private String viewName;

    private String viewImage;

    public Integer getViewId() {
        return viewId;
    }

    public void setViewId(Integer viewId) {
        this.viewId = viewId;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public String getViewImage() {
        return viewImage;
    }

    public void setViewImage(String viewImage) {
        this.viewImage = viewImage;
    }

    @Override
    public String toString() {
        return "PopupMenu{" +
                "viewId=" + viewId +
                ", viewName='" + viewName + '\'' +
                ", viewImage='" + viewImage + '\'' +
                '}';
    }
}
