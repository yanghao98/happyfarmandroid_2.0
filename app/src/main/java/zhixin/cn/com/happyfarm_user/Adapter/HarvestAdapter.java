package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Harvest;

/**
 * Created by DELL on 2018/3/21.
 */

public class HarvestAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Harvest> mDatas;

    public HarvestAdapter(Context context,List<Harvest> datas){

        mInflater = LayoutInflater.from(context);
        this.mDatas = datas;

    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null){

            convertView = mInflater.inflate(R.layout.harvest_item,parent,false);
            holder = new ViewHolder();
            holder.vgetable_name = convertView.findViewById(R.id.harvest_vgetable_name);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        Harvest harvest = mDatas.get(position);
        holder.vgetable_name.setText(harvest.getCropName());

        return convertView;
    }

    public class ViewHolder{
        TextView vgetable_name;
    }

}
