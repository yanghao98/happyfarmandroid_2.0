/**
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package zhixin.cn.com.happyfarm_user.easyMob;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.githang.statusbar.StatusBarCompat;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.other.AlertUtilBest;

/**
 * Application and notification
 *
 */
public class NewFriendsMsgActivity extends ABaseActivity implements NewFriendsAdapter.InnerItemOnclickListener,
		AdapterView.OnItemClickListener {
	private final String TAG = "NewFriendsMsgActivity";
	private ImageView addFriend;
	private ListView listView;
	private NewFriendsAdapter newFriendsAdapter;
	private List<NewFriends> friendsList = new ArrayList<NewFriends>();
	private List<Integer> idList = new ArrayList<>();
	private InputMethodManager inputMethodManager;
	protected ImageButton clearSearch;
	protected EditText query;
	private RelativeLayout newFriendsPrompt;
	private RefreshLayout refreshLayout;
	private Dialog mDialog;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//启动activity时不自动弹出软键盘 //TODO 新的朋友页面软键盘
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.em_activity_new_friends_msg);
		TextView title = findViewById(R.id.title_name);
		title.setText("新的朋友");

		//状态栏设置
		StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
		inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		addFriend = findViewById(R.id.add_friends_toolbar);
		//search
		query = findViewById(com.hyphenate.easeui.R.id.query);
		clearSearch = findViewById(com.hyphenate.easeui.R.id.search_clear);
		listView = findViewById(R.id.list);
		newFriendsPrompt = findViewById(R.id.new_friends_prompt);
		refreshLayout = findViewById(R.id.refreshLayout);
		refreshLayout.setEnableHeaderTranslationContent(false);
		//取消上拉加载
        refreshLayout.setEnableLoadMore(false);
		//设置 Header 为 Material风格 //setShowBezierWave 是否开启背景色
		refreshLayout.setRefreshHeader(new MaterialHeader(this).setShowBezierWave(false));
		AddFriend();
		/***后期新增**/
//		SearchFriend();
		initData();
		topRefreshLayout();
		query.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				newFriendsAdapter.getFilter().filter(s);
				if (s.length() > 0) {
					clearSearch.setVisibility(View.VISIBLE);
				} else {
					clearSearch.setVisibility(View.INVISIBLE);

				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void afterTextChanged(Editable s) {
			}
		});
		clearSearch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				query.getText().clear();
				hideSoftKeyboard();
			}
		});
		listView.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				hideSoftKeyboard();
				return false;
			}
		});
		/***********/
		//TODO 这是原来添加部分  我隐掉了
//		ListView listView = (ListView) findViewById(R.id.list);
//		InviteMessgeDao dao = new InviteMessgeDao(this);
//		List<InviteMessage> msgs = dao.getMessagesList();
//		Collections.reverse(msgs);

//		NewFriendsMsgAdapter adapter = new NewFriendsMsgAdapter(this, 1, msgs);
//		listView.setAdapter(adapter);
//		dao.saveUnreadMessageCount(0);
		FriendRequestCallback();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
//		EMClient.getInstance().contactManager().removeContactListener(mContactListener);
	}

	//TODO 监听好友状态事件
	public void FriendRequestCallback(){

	}
	//TODO 添加朋友点击事件
	public void AddFriend(){
		addFriend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(NewFriendsMsgActivity.this, AddFriendsActivity.class);
				startActivity(intent);
			}
		});
	}
	/***后期新增**/
	//下拉刷新
	public void topRefreshLayout() {
		refreshLayout.setEnableAutoLoadMore(false);//开启自动加载功能（非必须）
		//设置一个下拉监听器
		refreshLayout.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh(RefreshLayout refreshlayout) {//网络上请求最新数据
				friendsList.clear();
				idList.clear();
				SearchFriend();
				refreshlayout.finishRefresh(/*,false*/);//传入false表示刷新失败
			}
		});
	}
	private void initData(){
		newFriendsAdapter = new NewFriendsAdapter(NewFriendsMsgActivity.this,friendsList);
		newFriendsAdapter.setOnInnerItemOnClickListener(this);
		listView.setOnItemClickListener(this);
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
										   int position, long id) {
				PopupMenu menu = new PopupMenu(NewFriendsMsgActivity.this, view);
				menu.getMenuInflater().inflate(R.menu.contact_long_click_menu, menu.getMenu());
				menu.setOnMenuItemClickListener(item -> {
					if (item.getItemId() == R.id.delete) {
						NormalDialogStyleTwo(position);
					}
					return true;
				});
				menu.show();
				return true;
			}
		});
		listView.setAdapter(newFriendsAdapter);
	}
	//TODO 查询数据库好友申请
	public void SearchFriend(){
		//mDialog = LoadingDialog.createLoadingDialog(NewFriendsMsgActivity.this, "");
		HttpHelper.initHttpHelper().searchMyApply().enqueue(new Callback<SearchMyApply>() {
			@Override
			public void onResponse(Call<SearchMyApply> call, Response<SearchMyApply> response) {
				if (response.isSuccessful() && response.body().getFlag().equals("success")){
					LoadingDialog.closeDialog(mDialog);
					if (String.valueOf(response.body().getResult().getList()).equals("[]")) {
						newFriendsPrompt.setVisibility(View.VISIBLE);
						listView.setVisibility(View.GONE);
//						Toast.makeText(NewFriendsMsgActivity.this,"没有查到你输入的用户",Toast.LENGTH_SHORT).show();
					}else {
						newFriendsPrompt.setVisibility(View.GONE);
						listView.setVisibility(View.VISIBLE);
						for (int i = 0; i <= response.body().getResult().getList().size() - 1; i++) {
							NewFriends dataBean = new NewFriends();
							//TODO 别人加我
							if (String.valueOf(response.body().getResult().getList().get(i).getFriendId()).equals(getUserId())){
								dataBean.friendsImg = response.body().getResult().getList().get(i).getUserImg();
								dataBean.friendsId = String.valueOf(response.body().getResult().getList().get(i).getUserId());
								dataBean.friendsName = String.valueOf(response.body().getResult().getList().get(i).getUserNickname());
								dataBean.friendsIsAgree = String.valueOf(response.body().getResult().getList().get(i).getIsAgree());
							}else {
								dataBean.friendsImg = response.body().getResult().getList().get(i).getFriendImg();
								dataBean.friendsId = String.valueOf(response.body().getResult().getList().get(i).getFriendId());
								dataBean.friendsName = String.valueOf(response.body().getResult().getList().get(i).getFriendNickname());
								//（-1拒绝 0未处理 1同意）
								if ("-1".equals(String.valueOf(response.body().getResult().getList().get(i).getIsAgree()))){
									dataBean.friendsIsAgree = "拒绝";
								}else if ("0".equals(String.valueOf(response.body().getResult().getList().get(i).getIsAgree()))){
									dataBean.friendsIsAgree = "未处理";
								}else {
									dataBean.friendsIsAgree = "同意";
								}

							}
							idList.add(response.body().getResult().getList().get(i).getId());
							friendsList.add(dataBean);
						}
						newFriendsAdapter.notifyDataSetChanged();
					}
				}
			}

			@Override
			public void onFailure(Call<SearchMyApply> call, Throwable t) {
				LoadingDialog.closeDialog(mDialog);
				newFriendsPrompt.setVisibility(View.VISIBLE);
				listView.setVisibility(View.GONE);
			}
		});
	}
	//TODO 修改数据库好友申请
	public void UpdateApplyResult(int id,String isAgree){
		mDialog = LoadingDialog.createLoadingDialog(NewFriendsMsgActivity.this, "");
		HttpHelper.initHttpHelper().updateApplyResult(id,isAgree).enqueue(new Callback<CheckSMS>() {
			@Override
			public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
				LoadingDialog.closeDialog(mDialog);
				if (response.isSuccessful() && response.body().getFlag().equals("success")){
					if ("1".equals(isAgree)){
						friendsList.clear();
						idList.clear();
						Toast.makeText(NewFriendsMsgActivity.this,"同意了好友申请",Toast.LENGTH_SHORT).show();
						SearchFriend();
					}else {
						friendsList.clear();
						idList.clear();
						SearchFriend();
						Toast.makeText(NewFriendsMsgActivity.this,"拒绝了好友申请",Toast.LENGTH_SHORT).show();
					}
				}else {
					Toast.makeText(NewFriendsMsgActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onFailure(Call<CheckSMS> call, Throwable t) {
				LoadingDialog.closeDialog(mDialog);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (friendsList != null)
			friendsList.clear();
		if (idList != null)
			idList.clear();
		SearchFriend();
	}

	//TODO 删除好友申请
	public void UpdateApplyState(int id){
		mDialog = LoadingDialog.createLoadingDialog(NewFriendsMsgActivity.this, "");
		HttpHelper.initHttpHelper().updateApplyState(id,"-1").enqueue(new Callback<CheckSMS>() {
			@Override
			public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
				LoadingDialog.closeDialog(mDialog);
				if (response.isSuccessful() && response.body().getFlag().equals("success")){
					friendsList.clear();
					idList.clear();
					Toast.makeText(NewFriendsMsgActivity.this,"删除成功",Toast.LENGTH_SHORT).show();
					SearchFriend();
				}else {
					Toast.makeText(NewFriendsMsgActivity.this,response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onFailure(Call<CheckSMS> call, Throwable t) {
				LoadingDialog.closeDialog(mDialog);
			}
		});
	}
	private void NormalDialogStyleTwo(int position){
		final AlertUtilBest diyDialog = new AlertUtilBest(this);
		diyDialog.setCancel("取消")
				.setOk("确定")
				.setContent("您确认删除这个好友申请记录吗？")
				.setDialogClickListener(new AlertUtilBest.DialogClickListener() {
					@Override
					public void cancel() {
						diyDialog.cancel();
					}
					@Override
					public void ok() {
						UpdateApplyState(idList.get(position));
						diyDialog.cancel();
					}
				});
		diyDialog.builder();
		diyDialog.show();
	}
	@Override
	public void itemClick(View v) {
		int position;
		position = (Integer) v.getTag();
		switch (v.getId()) {
			case R.id.new_friends_add:
				//同意
				UpdateApplyResult(idList.get(position),"1");
				Log.e("内部item--1-->", position + "");
				break;
			case R.id.new_friends_refuse:
				//拒绝
				UpdateApplyResult(idList.get(position),"-1");
				Log.e("内部item--2-->", position + "");
				break;
			default:
				break;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
							long id) {
		Log.e("整体item----->", position + "");
	}
	protected void hideSoftKeyboard() {
		if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
			if (getCurrentFocus() != null)
				inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}
	/***********/
	public void back(View view) {
		finish();
	}

	//TODO 获得用户id
	private String getUserId(){
		return getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","");
	}
}
