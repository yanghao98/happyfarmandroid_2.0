package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.util.List;

import zhixin.cn.com.happyfarm_user.MyImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.Cooking;
import zhixin.cn.com.happyfarm_user.utils.GlideRoundTransform;

public class CookingListAdapter extends BaseAdapter {

    private Context context;
    private List<Cooking.ResultBean.CookingList> list;
    private MyImageView cookingImage;
    public CookingListAdapter(Context _context, List<Cooking.ResultBean.CookingList> _list) {
        this.list = _list;
        this.context = _context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        convertView = layoutInflater.inflate(R.layout.cooking_list_item, null);
        TextView tvCity = (TextView) convertView.findViewById(R.id.cooking_name);
        Cooking.ResultBean.CookingList city = list.get(position);
        cookingImage = convertView.findViewById(R.id.cooking_image);
//        RequestOptions options = new RequestOptions().error(R.drawable.img_load_failure).bitmapTransform(new RoundedCorners(30));//图片圆角为30
        Glide.with(context)
                .load(city.getCookingImg())
                .transform(new CenterCrop(context),new GlideRoundTransform(context,5))
                .into(cookingImage);

        tvCity.setText(city.getCookingName());
        return convertView;
    }
}


