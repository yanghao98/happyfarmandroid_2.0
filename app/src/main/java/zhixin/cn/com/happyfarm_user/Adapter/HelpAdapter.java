package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.TermsActivity;
import zhixin.cn.com.happyfarm_user.model.Helps;


public class HelpAdapter extends ArrayAdapter {

    private String TAG = "HelpAdapter";
    private final int resoutceId;

    public HelpAdapter(@NonNull Context context,  int textViewResourceId, @NonNull List objects) {
        super(context, textViewResourceId, objects);
        resoutceId = textViewResourceId;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Helps helps = (Helps) getItem(position);
        View view = LayoutInflater.from(getContext()).inflate(resoutceId,null);
        TextView textView = view.findViewById(R.id.help_name);
        textView.setText(helps.getHelpName());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: 我点击了："+helps.getHelpName()+"Url:"+helps.getHelpUrl());
                Intent intent = new Intent(getContext(),TermsActivity.class);
                intent.putExtra("termsName",helps.getHelpName());
                intent.putExtra("url",helps.getHelpUrl());
                getContext().startActivity(intent);
            }
        });
        return view;
    }
}
