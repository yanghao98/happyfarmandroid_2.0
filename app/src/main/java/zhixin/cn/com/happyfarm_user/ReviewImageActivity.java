package zhixin.cn.com.happyfarm_user;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.githang.statusbar.StatusBarCompat;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;
import zhixin.cn.com.happyfarm_user.utils.Utils;

public class ReviewImageActivity extends Activity {

    private String imageUrl;
    private ImageView imageView;
    private final String TAG = "ReviewImageActivity->";
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        }
        setStatusBarTransparent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_image);
        context = this;
        Intent intent = getIntent();
        imageView = findViewById(R.id.imageView);
        String fromActivityName = intent.getStringExtra("activity");
        imageUrl = intent.getStringExtra("image_url");
        //判断来源页面 Personal个人中心 空的话来源历史相册页面
        if (TextUtils.isEmpty(fromActivityName)){
            Picasso.get().load(imageUrl).into(imageView);
        }else {
            if (TextUtils.isEmpty(imageUrl)){
                imageView.setImageResource(R.drawable.icon_user_img);
            }else {
                Picasso.get()
                        .load(imageUrl)
                        .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                        .into(imageView);
            }
        }
        imageViewClick();
    }

    /**
     * 图片点击事件
     */
    private void imageViewClick() {
        imageView.setOnClickListener(new OnMultiClickListener() {
            @Override
            public void onMultiClick(View v) {
                if (Build.VERSION.SDK_INT < 21) {
                    finish();
                } else {
                    finishAfterTransition();
                }
            }
        });
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ActionSheetDialog();
                return false;
            }
        });
    }

    /**
     * 图片保存
     */
    private void ActionSheetDialog() {
        final String[] stringItems = {"保存本地"};
        final ActionSheetDialog dialog = new ActionSheetDialog(this, stringItems, null);
        dialog.title("保存图片")//标题
                .titleTextSize_SP(14.5f)//字体大小
                .show();
        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        //空链接
                        if (TextUtils.isEmpty(imageUrl)){
                            saveDrawableImg();
                        }else {
                            new Thread(t).start();
                        }
                        break;
                    default:
                }
                dialog.dismiss();
            }
        });
    }

    /**
     * 保存软件自带图片至本地
     */
    private void saveDrawableImg() {
        Resources res = this.getResources();
        BitmapDrawable d = (BitmapDrawable) res.getDrawable(R.drawable.icon_user_img);
        Bitmap img = d.getBitmap();

        String fn = "user_image.png";
        String path = Environment.getExternalStorageDirectory().getPath() + "/HappyFarm/Image/" + fn;
        try{
            OutputStream os = new FileOutputStream(path);
            img.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.close();
        }catch(Exception e){
//            Log.e("TAG", "", e);
        }
    }

    //为了下载图片资源，开辟一个新的子线程
    Thread t = new Thread() {
        public void run() {
            //下载图片的路径
            String iPath = String.valueOf(imageUrl);
            Bitmap bitmap = Utils.downloadBitmap(iPath);
            boolean res = saveBitmap(bitmap, "zhiXin_image" + System.currentTimeMillis() + ".png");
            if (res) {
                handler.sendEmptyMessage(111);
            } else {
                handler.sendEmptyMessage(222);
            }
        }
    };

    /**
     * 保存图片bitmap
     * @param bitmap 图片
     * @param picName 图片名称
     * return boolean
     */
    public boolean saveBitmap(Bitmap bitmap, String picName) {
//        Log.e(TAG, "保存图片");
        initPath();
        try {
            String path = Environment.getExternalStorageDirectory().getPath() + "/HappyFarm/Image/" + picName;
            FileOutputStream fos = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.flush();
            fos.close();
//            Log.i(TAG, "已经保存");
            return true;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }
    /**
     * 初始化文件夹
     */
    private void initPath() {
        String path1 = Environment.getExternalStorageDirectory().getPath() + "/HappyFarm";
        File file = new File(path1);
        if (!file.exists()) file.mkdir();
        String path2 = Environment.getExternalStorageDirectory().getPath() + "/HappyFarm/Image";
        File file2 = new File(path2);
        if (!file2.exists()) file2.mkdir();
    }
    //手柄更新的作用
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 111:
                    Toast.makeText(context, "保存成功", Toast.LENGTH_SHORT).show();
                    break;
                case 222:
                    Toast.makeText(context, "保存失败", Toast.LENGTH_SHORT).show();
                    break;
                    default:
            }
        }
    };

    /**
     * 设置透明状态栏
     */
    protected void setStatusBarTransparent(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}
