package zhixin.cn.com.happyfarm_user.model;

/**
 * Created by DELL on 2018/3/15.
 */

public class Programme {

    String programme;

    String acreage;

    String term;

    String times;

    String money;

    String lease;

//    public Programme(){
//
//    }

    public Programme(String programme,String acreage,String term,String times,String money){

        this.programme = programme;
        this.acreage = acreage;
        this.term = term;
        this.times = times;
        this.money = money;

    }

    public String getProgramme() {
        return programme;
    }

    public void setProgramme(String programme) {
        this.programme = programme;
    }

    public String getAcreage() {
        return acreage;
    }

    public void setAcreage(String acreage) {
        this.acreage = acreage;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
