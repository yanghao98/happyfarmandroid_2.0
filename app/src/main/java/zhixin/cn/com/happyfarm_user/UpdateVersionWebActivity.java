package zhixin.cn.com.happyfarm_user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.githang.statusbar.StatusBarCompat;

import zhixin.cn.com.happyfarm_user.other.MyWebChromeClient;

public class UpdateVersionWebActivity extends AppCompatActivity {

    private WebView webView;
    private ProgressBar progressBar;
    private String TAG = "UpdateVersionWebActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_version_web_activity);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        TextView title = findViewById(R.id.title_name);
        initView();

        Intent intent = getIntent();
        String termsName = intent.getStringExtra("termsName");
        String url = intent.getStringExtra("url");
        title.setText(termsName);
        //WebView加载web资源
        webView.loadUrl(url);

    }
    private void initView () {
        webView = findViewById(R.id.update_version_web_terms_webView);
        progressBar = findViewById(R.id.update_version_web_terms_progressBar);
        //启用支持javascript
        WebSettings settings = webView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");//这句话去掉也没事。。只是设置了编码格式
        settings.setJavaScriptEnabled(true);  //这句话必须保留。。不解释
        settings.setDomStorageEnabled(true);//这句话必须保留。。否则无法播放优酷视频网页。。其他的可以
        //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("intent") || url.startsWith("youku")) {
                    return true;
                } else {
                    return super.shouldOverrideUrlLoading(view, url);
                }
            }
        });

        //开启缓存
//        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        // 设置出现缩放工具
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setTextZoom(70);
        // 设置可以访问文件
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        //判断页面加载过程
        webView.setWebChromeClient(new MyWebChromeClient());
        //TODO webView返回上一页
        webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
                        webView.goBack();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            // TODO Auto-generated method stub
            if (newProgress == 100) {
                // 网页加载完成
                progressBar.setVisibility(View.GONE);//加载完网页进度条消失
            } else {
                // 加载中
                progressBar.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                progressBar.setProgress(newProgress);//设置进度值
            }

        }


    }

    public void back(View view) {
        finish();
    }

}
