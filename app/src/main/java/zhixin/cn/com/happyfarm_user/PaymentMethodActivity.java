package zhixin.cn.com.happyfarm_user;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alipay.sdk.app.PayTask;
import com.githang.statusbar.StatusBarCompat;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.AliPay;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.WeChat;
import zhixin.cn.com.happyfarm_user.other.Constants;
import zhixin.cn.com.happyfarm_user.EventBus.MessageEvent;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.PayResult;
import zhixin.cn.com.happyfarm_user.utils.TextString;
import zhixin.cn.com.happyfarm_user.utils.Utils;

/**
 * Created by DELL on 2018/3/17.
 */

public class PaymentMethodActivity extends ABaseActivity {

    private Dialog mDialog;
    private String landNo;
    private String landId;
    private String lease;
    private Integer landType;
    String packageName;
    String money;
    String times;
    String payState;
    int luckDrawRecordId;
    private static final int SDK_PAY_FLAG = 1;
    private String orderInfo;
    private String loginIp;
    private int wxPayFlag = -1;
    private CardView aliPayCardView;
    private CardView weChatCardView;
    private CardView unionPayCardView;
    private RadioButton alipay;
    private RadioButton wechat;
    private RadioButton unionpay;
    private Button method_bt;
    private int deductionAmount;

    private int payStateType;
    private int addressId;
    private String orderIdList;
    private String sumList;
    private String goodsIdList;
    private String priceList;
    private Dialog dialog;


    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    @SuppressWarnings("unchecked")
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    /**
                     对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
//                    Log.e("resultStatus",resultStatus);
                    // 判断resultStatus 为9000则代表支付成功
                    if (TextUtils.equals(resultStatus, "9000")) {
                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
                        Toast.makeText(PaymentMethodActivity.this, "支付成功", Toast.LENGTH_SHORT).show();
                        if (msg.arg1 == 0){
//                            initBuyLand(Integer.valueOf(money),Integer.valueOf(lease),Integer.valueOf(landNo));
                            Intent intent = new Intent(PaymentMethodActivity.this,CompletionPaymentActivity.class);
                            intent.putExtra("landNo",String.valueOf(landNo));
                            intent.putExtra("landType",landType+"");
                            startActivity(intent);
                            finish();
                        }else if (msg.arg1 == 1){
                            finish();
                            Log.i("类型为1返回首页", "handleMessage: " + payStateType);
                            if (1 == payStateType) {
                                startActivity(new Intent(PaymentMethodActivity.this, MainActivity.class));
                            }
//                            addMoney(Integer.valueOf(money),Integer.valueOf(packageName),landNo);
                        }
                    } else {
                        Toast.makeText(PaymentMethodActivity.this, payResult.getMemo(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_method_layout);
        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));
        TextView title = findViewById(R.id.title_name);
        title.setText("支付方式");
        initView();
        //注册
        EventBus.getDefault().register(this);
        payClick();//支付方式点击事件
        confirmPay();
    }

    /**
     * 初始化数据
     */
    private void initView() {
        TextView method_packageName = findViewById(R.id.method_packageName);
        TextView method_money = findViewById(R.id.method_money);
        TextView method_times = findViewById(R.id.method_times);
        alipay = findViewById(R.id.alipay_radio);
        wechat = findViewById(R.id.wechat_radio);
        unionpay = findViewById(R.id.unionpay_radio);
        aliPayCardView = findViewById(R.id.method_alipay);
        weChatCardView = findViewById(R.id.method_weChat);
        unionPayCardView = findViewById(R.id.method_unionPay);

        method_bt = findViewById(R.id.method_bt);
        LinearLayout linearLayout = findViewById(R.id.method_linear);
        RelativeLayout number = findViewById(R.id.number);
        RelativeLayout information = findViewById(R.id.information);
        TextView landNos = findViewById(R.id.method_landNo);
        landNos.setText(landNo);
        Intent intent = getIntent();
        packageName = intent.getStringExtra("Name");
        deductionAmount = intent.getIntExtra("deductionAmount",0);
        money = intent.getStringExtra("Money");
//        money = "0.01";
        times = intent.getStringExtra("packageTimes");
        payState = intent.getStringExtra("state");
        landNo = intent.getStringExtra("landNo");
        lease = intent.getStringExtra("lease");
        landId = intent.getStringExtra("landId");

        payStateType = intent.getIntExtra("stateType",0);
        money = intent.getStringExtra("Money");
        orderIdList = intent.getStringExtra("orderId");
        addressId = intent.getIntExtra("plotId",0);
        priceList = intent.getStringExtra("price");
        sumList = intent.getStringExtra("sum");
        goodsIdList = intent.getStringExtra("goodsId");
        //判断是否购买蔬菜 如果是则显示 余额购买
        if (1 == payStateType) {
            unionPayCardView.setVisibility(View.VISIBLE);
        }
        try {
            landType = Integer.valueOf(intent.getStringExtra("landType"));
        } catch (Exception e){
            e.printStackTrace();
        }
        if (payState.equals("0")) {
            number.setVisibility(View.VISIBLE);
            information.setVisibility(View.GONE);
            method_packageName.setText(packageName);
            method_money.setText(money);
            method_times.setText(times);
        }else if (payState.equals("1")) {
            linearLayout.setVisibility(View.GONE);
            number.setVisibility(View.GONE);
            information.setVisibility(View.VISIBLE);
            method_packageName.setText(packageName+"元");
            method_money.setText(money);
            if (1 == payStateType) {
                method_packageName.setText("购买商品");
            }
        }
    }

    /**
     * 事件响应方法
     * 接收消息
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {

        String msg = event.getPassword();
        if ("WXPaySuccess".equals(msg) && wxPayFlag == 0){
            Intent intent = new Intent(PaymentMethodActivity.this,CompletionPaymentActivity.class);
            intent.putExtra("landNo",String.valueOf(landNo));
            intent.putExtra("landType",landType+"");
            startActivity(intent);
            finish();
//            initBuyLand(Integer.valueOf(money),Integer.valueOf(lease),Integer.valueOf(landNo));
        }
        if ("WXPaySuccess".equals(msg) && wxPayFlag == 1){
            finish();
            if (1 == payStateType) {
                startActivity(new Intent(PaymentMethodActivity.this, MainActivity.class));
            }
//            addMoney(Integer.valueOf(money),Integer.valueOf(packageName),landNo);
        }
    }

    public void back(View view){
        finish();
    }

    /**
     * 确认支付按钮点击事件
     */
    private void confirmPay() {
        method_bt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                loginIp = NetWorkUtils.getIPAddress(getApplicationContext());
                if (unionpay.isChecked()){
//                    Toast.makeText(PaymentMethodActivity.this,"功能暂未开放",Toast.LENGTH_SHORT).show();
                    addOrderLogGeneration();
                    return;
                } else if (loginIp.equals("-1")) {
                    Toast.makeText(PaymentMethodActivity.this, "请查看网络是否链接",Toast.LENGTH_SHORT).show();
                    return;
                }
                switch (payState) {
                    case "0"://租地状态
                        if (alipay.isChecked()){
//                        initBuyLand(Integer.valueOf(money),Integer.valueOf(lease),Integer.valueOf(landNo));
//                            Log.e("packageName",packageName);
//                            Log.e("money",money);
                            alipay(packageName,money,0,"购买租地");
                        } else if (wechat.isChecked()){
                            wxPayFlag = 0;
                            weChatPay(loginIp, Double.parseDouble(money), "购买租地");
                            loginIp = NetWorkUtils.getIPAddress(getApplicationContext());
                        } else {
                            Toast.makeText(PaymentMethodActivity.this,"请选择支付方式",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case "1"://充值余额
                        if (1 == payStateType) {
                            if (alipay.isChecked()){
                                aliPayVegetables();
                            } else if (wechat.isChecked()){
                                wxPayFlag = 1;
                                weChatPayVegetables();
                            } else {
                                Toast.makeText(PaymentMethodActivity.this,"请选择支付方式",Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (alipay.isChecked()){
                                alipay(packageName,money,1,"余额充值");
                            } else if (wechat.isChecked()){
                                wxPayFlag = 1;
                                weChatPayD(packageName,loginIp, Double.parseDouble(money), "余额充值");
                            } else {
                                Toast.makeText(PaymentMethodActivity.this,"请选择支付方式",Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;
                    default:
                }
            }
        });
    }

    //int userId, int landId, int getDiamond, int lease,String amount,String subject
    public void alipay(String Name,String moneys,int arg,String orderName){
        if (arg == 1){
            aliPayNetD(Name,moneys,arg,orderName);
        }else {
            aliPayNet(Name,moneys,arg,orderName);
        }
    }

    /**
     * 支付宝购买余额
     * @param Name
     * @param moneys
     * @param arg
     * @param orderName
     */
    private void aliPayNetD(String Name,String moneys,int arg,String orderName){
//        Log.i("aliPay", "aliPayNetD: "+getUserId()+"余额："+Integer.parseInt(Name)+"多少钱："+moneys+"支付名称："+orderName);
        dialog = LoadingDialog.createLoadingDialog(PaymentMethodActivity.this, "正在跳转支付");
        HttpHelper.initHttpHelper().generatePaymentOrderD(getUserId(),Integer.parseInt(Name),moneys,orderName).enqueue(new Callback<AliPay>() {
            @Override
            public void onResponse(Call<AliPay> call, Response<AliPay> response) {
                LoadingDialog.closeDialog(dialog);
//                Log.e("orderInfo", JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    orderInfo = response.body().getResult();
                    Runnable payRunnable = new Runnable() {
                        @Override
                        public void run() {
//                        EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);
                            PayTask alipay = new PayTask(PaymentMethodActivity.this);//调用支付接口
//                            Log.i("msp",orderInfo);
                            Map<String, String> result = alipay.payV2(orderInfo, true);//支付结果
//                            Log.i("msp", result.toString());
                            Message msg = new Message();
                            msg.what = SDK_PAY_FLAG;
                            msg.obj = result;
                            msg.arg1 = arg;
                            handler.sendMessage(msg);
                        }
                    };

                    Thread payThread = new Thread(payRunnable);
                    payThread.start();
                } else {
                    Toast.makeText(PaymentMethodActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AliPay> call, Throwable t) {
//                Log.e("orderInfo",orderInfo);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }

    /**
     * 支付宝支付 租地购买
     * @param Name
     * @param moneys
     * @param arg
     * @param orderName
     */
    private void aliPayNet(String Name,String moneys,int arg,String orderName){
        dialog = LoadingDialog.createLoadingDialog(PaymentMethodActivity.this, "正在跳转支付");
            HttpHelper.initHttpHelper().generatePaymentOrderNoluckDrawRecordId(getUserId(),Integer.parseInt(landId), Integer.parseInt(lease),moneys,orderName,deductionAmount).enqueue(new Callback<AliPay>() {
                @Override
                public void onResponse(Call<AliPay> call, Response<AliPay> response) {
//                Log.e("orderInfo", JSON.toJSONString(response.body()));
                    LoadingDialog.closeDialog(dialog);
                    if ("success".equals(response.body().getFlag())) {
                        orderInfo = response.body().getResult();
                        Runnable payRunnable = new Runnable() {
                            @Override
                            public void run() {
//                        EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);
                                PayTask alipay = new PayTask(PaymentMethodActivity.this);//调用支付接口
//                            Log.i("msp",orderInfo);
                                Map<String, String> result = alipay.payV2(orderInfo, true);//支付结果
//                            Log.i("msp", result.toString());
                                Message msg = new Message();
                                msg.what = SDK_PAY_FLAG;
                                msg.obj = result;
                                msg.arg1 = arg;
                                handler.sendMessage(msg);
                            }
                        };
                        Thread payThread = new Thread(payRunnable);
                        payThread.start();
                    } else {
                        Toast.makeText(PaymentMethodActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AliPay> call, Throwable t) {
//                Log.e("orderInfo",orderInfo);
                    LoadingDialog.closeDialog(dialog);
                }
            });

    }
    public void weChatPay(String ip,Double moneys,String name){
        dialog = LoadingDialog.createLoadingDialog(PaymentMethodActivity.this, "正在跳转支付");
        PayReq request = new PayReq();
        IWXAPI msgApi  = WXAPIFactory.createWXAPI(this, Constants.APP_ID ,false);
        // 将该app注册到微信
        msgApi.registerApp(Constants.APP_ID);
        //获取用户信息是否为地主
            HttpHelper.initHttpHelper().weChatPay(Integer.parseInt(landId), Integer.parseInt(lease),ip, moneys, name,deductionAmount).enqueue(new Callback<WeChat>() {
                @Override
                public void onResponse(Call<WeChat> call, Response<WeChat> response) {
//                Log.e("orderInfo", JSON.toJSONString(response.body()));
                    LoadingDialog.closeDialog(dialog);
                    if (("success").equals(response.body().getFlag())){
                        request.appId = response.body().getResult().getAppid();
                        request.partnerId = response.body().getResult().getPartnerid();
                        request.nonceStr = response.body().getResult().getNoncestr();
                        request.prepayId = response.body().getResult().getPrepayid();
                        request.sign = response.body().getResult().getSign();
                        request.packageValue = "Sign=WXPay";
                        request.timeStamp = String.valueOf(response.body().getResult().getTimestamp());
                        msgApi.sendReq(request);
                    } else {
                        Toast.makeText(PaymentMethodActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<WeChat> call, Throwable t) {
                    LoadingDialog.closeDialog(dialog);
                    showToastShort(PaymentMethodActivity.this, TextString.NetworkRequestFailed);
                }
            });
    }
    public void weChatPayD(String Name,String ip,Double moneys,String name){
        dialog = LoadingDialog.createLoadingDialog(PaymentMethodActivity.this, "正在跳转支付");
        PayReq request = new PayReq();
        IWXAPI msgApi  = WXAPIFactory.createWXAPI(this, Constants.APP_ID ,false);
        // 将该app注册到微信
        msgApi.registerApp(Constants.APP_ID);
        //获取用户信息是否为地主
        HttpHelper.initHttpHelper().weChatPayD(Integer.parseInt(Name),ip, moneys, name).enqueue(new Callback<WeChat>() {
            @Override
            public void onResponse(Call<WeChat> call, Response<WeChat> response) {
                LoadingDialog.closeDialog(dialog);
//                Log.e("orderInfo", JSON.toJSONString(response.body()));
                if (("success").equals(response.body().getFlag())) {
                    request.appId = response.body().getResult().getAppid();
                    request.partnerId = response.body().getResult().getPartnerid();
                    request.nonceStr = response.body().getResult().getNoncestr();
                    request.prepayId = response.body().getResult().getPrepayid();
                    request.sign = response.body().getResult().getSign();
                    request.packageValue = "Sign=WXPay";
                    request.timeStamp = String.valueOf(response.body().getResult().getTimestamp());
                    msgApi.sendReq(request);
                } else {
                    Toast.makeText(PaymentMethodActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<WeChat> call, Throwable t) {
                LoadingDialog.closeDialog(dialog);
                showToastShort(PaymentMethodActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }


    //支付宝购买蔬菜
    public void aliPayVegetables(){
        dialog = LoadingDialog.createLoadingDialog(PaymentMethodActivity.this, "正在跳转支付");
//        Log.i("支付宝支付购买参数", "aliPayVegetables: " +getUserId()+"@@"+money+"@@"+orderIdList+"@@"+addressId+"@@"+priceList+"@@"+sumList+"@@"+goodsIdList);
        HttpHelper.initHttpHelper().generatePaymentOrderVegetables(getUserId(),String.valueOf(money),"购买商品",orderIdList,addressId,priceList,sumList,goodsIdList).enqueue(new Callback<AliPay>() {
            @Override
            public void onResponse(Call<AliPay> call, Response<AliPay> response) {
                LoadingDialog.closeDialog(dialog);
//                Log.e("orderInfo", JSON.toJSONString(response.body()));
                if ("success".equals(response.body().getFlag())) {
                    orderInfo = response.body().getResult();
                    Runnable payRunnable = new Runnable() {
                        @Override
                        public void run() {
//                        EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);
                            PayTask alipay = new PayTask(PaymentMethodActivity.this);//调用支付接口
//                            Log.i("msp",orderInfo);
                            Map<String, String> result = alipay.payV2(orderInfo, true);//支付结果
//                            Log.i("msp", result.toString());
                            Message msg = new Message();
                            msg.what = SDK_PAY_FLAG;
                            msg.obj = result;
                            msg.arg1 = 1;
                            handler.sendMessage(msg);
                        }
                    };

                    Thread payThread = new Thread(payRunnable);
                    payThread.start();
                } else {
                    Toast.makeText(PaymentMethodActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AliPay> call, Throwable t) {
//                        Log.e("orderInfo",orderInfo);
                LoadingDialog.closeDialog(dialog);
            }
        });
    }


    //支微信购买蔬菜
    public void weChatPayVegetables() {
        dialog = LoadingDialog.createLoadingDialog(PaymentMethodActivity.this, "正在跳转支付");
//        Log.i("微信支付购买参数", "aliPayVegetables: " +getUserId()+"@@"+money+"@@"+orderIdList+"@@"+addressId+"@@"+priceList+"@@"+sumList+"@@"+goodsIdList);
        PayReq request = new PayReq();
        IWXAPI msgApi  = WXAPIFactory.createWXAPI(this, Constants.APP_ID ,false);
        // 将该app注册到微信
        msgApi.registerApp(Constants.APP_ID);
        HttpHelper.initHttpHelper().weChatPayVegetables(loginIp,getUserId(),Double.valueOf(money),"购买商品",orderIdList,addressId,priceList,sumList,goodsIdList).enqueue(new Callback<WeChat>() {
            @Override
            public void onResponse(Call<WeChat> call, Response<WeChat> response) {
//                Log.e("orderInfo", JSON.toJSONString(response.body()));
                LoadingDialog.closeDialog(dialog);
                if (("success").equals(response.body().getFlag())){
                    request.appId = response.body().getResult().getAppid();
                    request.partnerId = response.body().getResult().getPartnerid();
                    request.nonceStr = response.body().getResult().getNoncestr();
                    request.prepayId = response.body().getResult().getPrepayid();
                    request.sign = response.body().getResult().getSign();
                    request.packageValue = "Sign=WXPay";
                    request.timeStamp = String.valueOf(response.body().getResult().getTimestamp());
                    msgApi.sendReq(request);
                } else {
                    Toast.makeText(PaymentMethodActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<WeChat> call, Throwable t) {
                LoadingDialog.closeDialog(dialog);
                showToastShort(PaymentMethodActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }

    //余额支付买菜
    public void addOrderLogGeneration() {
        dialog = LoadingDialog.createLoadingDialog(PaymentMethodActivity.this,"正在支付");
        HttpHelper.initHttpHelper().addOrderLogGeneration(orderIdList,addressId,Double.valueOf(money),priceList,sumList,goodsIdList,getUserId()).enqueue(new Callback<CheckSMS>() {
            @Override
            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                LoadingDialog.closeDialog(dialog);
                if ("success".equals(response.body().getFlag())) {
                    showToastShort(PaymentMethodActivity.this, "支付成功");
                    startActivity(new Intent(PaymentMethodActivity.this, MainActivity.class));
                } else {
                    showToastShort(PaymentMethodActivity.this, "支付失败");
                }
            }

            @Override
            public void onFailure(Call<CheckSMS> call, Throwable t) {
                LoadingDialog.closeDialog(dialog);
                showToastShort(PaymentMethodActivity.this, TextString.NetworkRequestFailed);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //反注册
        EventBus.getDefault().unregister(this);
    }
    private int getUserId(){
        int userId;
        if (getSharedPreferences("User_data",MODE_PRIVATE).getString("userId","").equals("")){
            userId = 0;
            return userId;
        }else {
            userId = Integer.valueOf(getSharedPreferences("User_data",MODE_PRIVATE).getString("userId",""));
        }
        return userId;
    }

    //TODO 支付方式点击事件
    private void payClick(){
        alipay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wechat.setChecked(false);
                unionpay.setChecked(false);
            }
        });
        aliPayCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alipay.setChecked(true);
                wechat.setChecked(false);
                unionpay.setChecked(false);
            }
        });
        wechat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alipay.setChecked(false);
                unionpay.setChecked(false);
            }
        });
        weChatCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alipay.setChecked(false);
                wechat.setChecked(true);
                unionpay.setChecked(false);
            }
        });
        unionpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wechat.setChecked(false);
                alipay.setChecked(false);
            }
        });
        unionPayCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wechat.setChecked(false);
                alipay.setChecked(false);
                unionpay.setChecked(true);
            }
        });
    }
}
