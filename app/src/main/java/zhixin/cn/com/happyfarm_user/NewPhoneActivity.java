package zhixin.cn.com.happyfarm_user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import org.apaches.commons.codec.binary.Base64;

import java.security.PublicKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.ISRegister;
import zhixin.cn.com.happyfarm_user.model.PublicKeyList;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.RSAUtil;
import zhixin.cn.com.happyfarm_user.other.Validator;

/**
 * Created by Administrator on 2018/7/24.
 */

public class NewPhoneActivity extends ABaseActivity {

    private TextView originalPhone;
    private String userTel;
    private Button newNextBtn;
    private String state;
    private Button newCodeBtn;
    private EditText newCodeText;
    private EditText newPhone;
    private Dialog mDialog;
    private int count = 60;
    private int COUNT_TIME = 0;
    private byte[] tel2;
    private String publicKey1;
    private String tel_encryption;
    private String tel_unencrypted;
    private String captcha;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_phone_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("绑定新手机号");

        originalPhone = findViewById(R.id.new_phone_num);
        newNextBtn = findViewById(R.id.new_phone_next);
        newCodeBtn = findViewById(R.id.new_code_bt);
        newCodeText = findViewById(R.id.new_verification_code);
        newPhone = findViewById(R.id.new_phone_text);

        Intent intent = getIntent();
        userTel = intent.getStringExtra("tel");
        originalPhone.setText(userTel);
        forgotNextBtn ();
        rebindCodeBtn ();
    }

    //TODO 重置手机号按钮点击事件
    public void forgotNextBtn (){
        newNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tel_unencrypted = newPhone.getText().toString();
                captcha = newCodeText.getText().toString();
                if (!NetWorkUtils.isNetworkAvalible(NewPhoneActivity.this)){
                    Toast.makeText(NewPhoneActivity.this,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
                }else if (state.equals("")){
                    Toast.makeText(NewPhoneActivity.this,"您还未登录",Toast.LENGTH_SHORT).show();
                }else {
                    if (newPhone.getText().toString().equals(userTel)){
                        Toast.makeText(NewPhoneActivity.this,  "您输入的手机号与原号码相同", Toast.LENGTH_LONG).show();
                    }else if (newPhone.getText().toString().equals("")){
                        Toast.makeText(NewPhoneActivity.this,  "请输入您要更改的手机号", Toast.LENGTH_LONG).show();
                    }else if (newCodeText.getText().toString().equals("")){
                        Toast.makeText(NewPhoneActivity.this,  "请输入您的验证码", Toast.LENGTH_LONG).show();
                    }else if (!Validator.isChinaPhoneLegal(newPhone.getText().toString())){
                        Toast.makeText(NewPhoneActivity.this,  "请输入正确的手机号", Toast.LENGTH_LONG).show();
                    }else {
                        mDialog = LoadingDialog.createLoadingDialog(NewPhoneActivity.this,"");
                        Call<CheckSMS> checkSMS = HttpHelper.initHttpHelper().checkSMS(tel_unencrypted,captcha);
                        checkSMS.enqueue(new Callback<CheckSMS>() {
                            @Override
                            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                Log.e("state", JSON.toJSONString(response.body()));
                                if ("success".equals(response.body().getFlag())){
                                    Call<CheckSMS> updateUserPhone = HttpHelper.initHttpHelper().updateUserPhone(tel_unencrypted);
                                    updateUserPhone.enqueue(new Callback<CheckSMS>() {
                                        @Override
                                        public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                            Log.e("state",JSON.toJSONString(response.body()));
                                            if ("success".equals(response.body().getFlag())){
                                                LoadingDialog.closeDialog(mDialog);
                                                Toast.makeText(NewPhoneActivity.this, "更改成功,需要您重新登录哦",Toast.LENGTH_SHORT).show();
                                                //TODO 清除缓存 Token
                                                SharedPreferences pref = getSharedPreferences("User_data",MODE_PRIVATE);
                                                SharedPreferences.Editor editor = pref.edit();
                                                editor.remove("token");
                                                editor.remove("userId");
                                                editor.apply();
                                                finish();
                                            }else {
                                                LoadingDialog.closeDialog(mDialog);
                                                Toast.makeText(NewPhoneActivity.this, "更改失败",Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CheckSMS> call, Throwable t) {
                                            LoadingDialog.closeDialog(mDialog);
                                        }
                                    });
                                }else {
                                    LoadingDialog.closeDialog(mDialog);
                                    Toast.makeText(NewPhoneActivity.this, "验证码错误",Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckSMS> call, Throwable t) {
                                LoadingDialog.closeDialog(mDialog);
                            }
                        });
                    }

                }
            }
        });
        //点击空白处退出键盘
        findViewById(R.id.newPhoneBackView).setOnClickListener(v -> {
            hideKeyboard();
        });
    }
    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm =  (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }
    //TODO 验证码点击事件
    public void rebindCodeBtn (){
        newCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetWorkUtils.isNetworkAvalible(NewPhoneActivity.this)){
                    Toast.makeText(NewPhoneActivity.this,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
                }else if (state.equals("")){
                    Toast.makeText(NewPhoneActivity.this,"您还未登录",Toast.LENGTH_SHORT).show();
                }else if (newPhone.getText().toString().equals(userTel)){
                    Toast.makeText(NewPhoneActivity.this,  "您输入的手机号与原号码相同", Toast.LENGTH_LONG).show();
                }else if (!Validator.isChinaPhoneLegal(newPhone.getText().toString())){
                    Toast.makeText(NewPhoneActivity.this,  "请输入正确的手机号", Toast.LENGTH_LONG).show();
                }else {
                    tel_unencrypted = newPhone.getText().toString();
//                tel = tel_unencrypted;
                    tel2 = tel_unencrypted.getBytes();
                    mDialog = LoadingDialog.createLoadingDialog(NewPhoneActivity.this,null);
                    //验证该手机号是否注册
                    Call<ISRegister> isRegister = HttpHelper.initHttpHelper().isRegister(tel_unencrypted);
                    isRegister.enqueue(new Callback<ISRegister>() {
                        @Override
                        public void onResponse(Call<ISRegister> call, Response<ISRegister> response) {
//                            Log.e("boole", JSON.toJSONString(response.body()));
                            if ("failed".equals(response.body().getFlag())){
                                LoadingDialog.closeDialog(mDialog);
                                Toast.makeText(NewPhoneActivity.this, response.body().getResult().toString(),Toast.LENGTH_SHORT).show();
                            }else {
                                //手机号加密
                                Call<PublicKeyList> publicKey = HttpHelper.initHttpHelper().publicKey(tel_unencrypted);
                                publicKey.enqueue(new Callback<PublicKeyList>() {
                                    @Override
                                    public void onResponse(Call<PublicKeyList> call, Response<PublicKeyList> response) {
                                        publicKey1 = response.body().getResult();
                                        try {
                                            PublicKey publickey = RSAUtil.getPublicKey(publicKey1);
                                            tel2 = RSAUtil.encrypt(tel2,publickey);
                                            tel_encryption = Base64.encodeBase64String(tel2);
//                                        Log.e("tel",tel_encryption+"\n"+tel_unencrypted);
                                            //发送验证码
                                            Call<CheckSMS> getSMS = HttpHelper.initHttpHelper().getSMS(tel_encryption,tel_unencrypted);
                                            getSMS.enqueue(new Callback<CheckSMS>() {
                                                @Override
                                                public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
                                                    Log.e("state",JSON.toJSONString(response.body()));
                                                    if ("success".equals(response.body().getFlag())){
                                                        LoadingDialog.closeDialog(mDialog);
                                                        clickButton(view);
                                                        Toast.makeText(NewPhoneActivity.this, "发送成功",Toast.LENGTH_SHORT).show();
                                                    }else {
                                                        LoadingDialog.closeDialog(mDialog);
                                                        Toast.makeText(NewPhoneActivity.this, "发送失败",Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<CheckSMS> call, Throwable t) {
                                                    LoadingDialog.closeDialog(mDialog);
                                                    Toast.makeText(NewPhoneActivity.this, "发送失败",Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<PublicKeyList> call, Throwable t) {
                                        LoadingDialog.closeDialog(mDialog);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<ISRegister> call, Throwable t) {
                            LoadingDialog.closeDialog(mDialog);
                        }
                    });
                }
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences pref = NewPhoneActivity.this.getSharedPreferences("User_data",MODE_PRIVATE);
        state = pref.getString("token","");
    }
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if(count == 0){
                count = 60;
                newCodeBtn.setText("重新发送");
                newCodeBtn.setClickable(true);
                return;
            }
            count--;
            newCodeBtn.setText(count+"s后重新发送");
            sendEmptyMessageDelayed(COUNT_TIME,1000);
        };
    };
    public void clickButton(View view){
        handler.sendEmptyMessage(COUNT_TIME);
        newCodeBtn.setClickable(false);
    }
    public void back(View view) {
        finish();
    }
    /**
     * 重写返回键
     * 监听Back键按下事件
     * super.onBackPressed()会自动调用finish()方法,关闭当前Activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
