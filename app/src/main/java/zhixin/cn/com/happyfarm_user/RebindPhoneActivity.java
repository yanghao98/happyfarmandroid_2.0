package zhixin.cn.com.happyfarm_user;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.githang.statusbar.StatusBarCompat;

import org.apaches.commons.codec.binary.Base64;

import java.security.PublicKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import zhixin.cn.com.happyfarm_user.Dialog.LoadingDialog;
import zhixin.cn.com.happyfarm_user.base.ABaseActivity;
import zhixin.cn.com.happyfarm_user.db.HttpHelper;
import zhixin.cn.com.happyfarm_user.model.CheckSMS;
import zhixin.cn.com.happyfarm_user.model.PublicKeyList;
import zhixin.cn.com.happyfarm_user.other.NetWorkUtils;
import zhixin.cn.com.happyfarm_user.other.RSAUtil;
import zhixin.cn.com.happyfarm_user.other.TextChangedListener;
import zhixin.cn.com.happyfarm_user.other.Validator;

/**
 * Created by HuYueling on 2018/5/7.
 */

public class RebindPhoneActivity extends ABaseActivity {

    private TextView originalPhone;
    private String userTel;
    private Button rebindNextBtn;
    private String state;
    private Button rebindCodeBtn;
    private EditText rebindCode;
    private EditText rebindPhone;
    private Dialog mDialog;
    private int count = 60;
    private int COUNT_TIME = 0;
    private byte[] tel2;
    private String publicKey1;
    private String tel_encryption;
    private String tel_unencrypted;
    private String captcha;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rebind_phone_layout);

        //状态栏设置
        StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.theme_color));

        TextView title = findViewById(R.id.title_name);
        title.setText("重新绑定手机号");
        originalPhone = findViewById(R.id.original_phone_num);
        rebindNextBtn = findViewById(R.id.rebind_next);
        rebindCodeBtn = findViewById(R.id.rebind_code_bt);
        rebindCode = findViewById(R.id.rebind_verification_code);
        rebindPhone = findViewById(R.id.rebind_phone);

        Intent intent = getIntent();
        userTel = intent.getStringExtra("tel");
        originalPhone.setText(userTel);
        forgotNextBtn ();
        rebindCodeBtn ();

        //限制手机号长度
        TextChangedListener textChangedListener = new TextChangedListener(11,rebindPhone,RebindPhoneActivity.this,"手机号最大长度为11位");
        rebindPhone.addTextChangedListener(textChangedListener.getmTextWatcher());

    }
    //TODO 重置手机号按钮点击事件
    public void forgotNextBtn (){
        rebindNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tel_unencrypted = rebindPhone.getText().toString();
                captcha = rebindCode.getText().toString();
                if (!NetWorkUtils.isNetworkAvalible(RebindPhoneActivity.this)){
                    Toast.makeText(RebindPhoneActivity.this,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
                }else if (state.equals("")){
                    Toast.makeText(RebindPhoneActivity.this,"您还未登录",Toast.LENGTH_SHORT).show();
                }else {
                    if (rebindPhone.getText().toString().equals("")){
                        Toast.makeText(RebindPhoneActivity.this,  "请输入您要更改的手机号", Toast.LENGTH_LONG).show();
                    }else if (!Validator.isChinaPhoneLegal(rebindPhone.getText().toString())){
                        Toast.makeText(RebindPhoneActivity.this,  "请输入正确的手机号", Toast.LENGTH_LONG).show();
                    } else if (rebindCode.getText().toString().equals("")){
                        Toast.makeText(RebindPhoneActivity.this,  "请输入您的验证码", Toast.LENGTH_LONG).show();
                    }else if (rebindPhone.getText().toString().equals(userTel)){
                        Toast.makeText(RebindPhoneActivity.this,  "您输入的手机号与原号码相同", Toast.LENGTH_LONG).show();
                    } else {
                        mDialog = LoadingDialog.createLoadingDialog(RebindPhoneActivity.this,null);
                        Call<CheckSMS> checkSMS = HttpHelper.initHttpHelper().checkSMS(tel_unencrypted,captcha);
                        checkSMS.enqueue(new Callback<CheckSMS>() {
                            @Override
                            public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                Log.e("state",JSON.toJSONString(response.body()));
                                if ("success".equals(response.body().getFlag())){
                                    Call<CheckSMS> updateUserPhone = HttpHelper.initHttpHelper().updateUserPhone(tel_unencrypted);
                                    updateUserPhone.enqueue(new Callback<CheckSMS>() {
                                        @Override
                                        public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                            Log.e("state",JSON.toJSONString(response.body()));
                                            if ("success".equals(response.body().getFlag())){
                                                LoadingDialog.closeDialog(mDialog);
//                                                Toast.makeText(RebindPhoneActivity.this, "更改成功,需要您重新登录哦",Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(RebindPhoneActivity.this,NewPhoneActivity.class);
                                                intent.putExtra("tel",userTel);
                                                startActivity(intent);
                                                finish();
                                            }else {
                                                LoadingDialog.closeDialog(mDialog);
                                                Toast.makeText(RebindPhoneActivity.this, "更改失败",Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CheckSMS> call, Throwable t) {
                                            LoadingDialog.closeDialog(mDialog);
                                        }
                                    });
                                }else {
                                    LoadingDialog.closeDialog(mDialog);
                                    Toast.makeText(RebindPhoneActivity.this, "验证码错误",Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckSMS> call, Throwable t) {
                                LoadingDialog.closeDialog(mDialog);
                            }
                        });
                    }

                }
            }
        });
        //点击空白处退出键盘
        findViewById(R.id.rebindPhoneBackView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }
    /**
     * TODO hide 隐藏键盘方法
     */
    public void hideKeyboard() {
        InputMethodManager imm =  (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }
    //TODO 验证码点击事件
    public void rebindCodeBtn (){
        rebindCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetWorkUtils.isNetworkAvalible(RebindPhoneActivity.this)){
                    Toast.makeText(RebindPhoneActivity.this,"您的网络似乎已经断开了",Toast.LENGTH_SHORT).show();
                }else if (state.equals("")){
                    Toast.makeText(RebindPhoneActivity.this,"您还未登录",Toast.LENGTH_SHORT).show();
                }else if (!Validator.isChinaPhoneLegal(rebindPhone.getText().toString())){
                    Toast.makeText(RebindPhoneActivity.this,  "请输入正确的手机号", Toast.LENGTH_LONG).show();
                } else if (rebindPhone.getText().toString().equals(userTel)){
                    Toast.makeText(RebindPhoneActivity.this,  "您输入的手机号与原号码相同", Toast.LENGTH_LONG).show();
                }else {
                    tel_unencrypted = rebindPhone.getText().toString();
//                tel = tel_unencrypted;
                    tel2 = tel_unencrypted.getBytes();
                    mDialog = LoadingDialog.createLoadingDialog(RebindPhoneActivity.this,null);
                    //手机号加密
                    Call<PublicKeyList> publicKey = HttpHelper.initHttpHelper().publicKey(tel_unencrypted);
                    publicKey.enqueue(new Callback<PublicKeyList>() {
                        @Override
                        public void onResponse(Call<PublicKeyList> call, Response<PublicKeyList> response) {
                            publicKey1 = response.body().getResult();
                            try {
                                PublicKey publickey = RSAUtil.getPublicKey(publicKey1);
                                tel2 = RSAUtil.encrypt(tel2,publickey);
                                tel_encryption = Base64.encodeBase64String(tel2);
//                                        Log.e("tel",tel_encryption+"\n"+tel_unencrypted);
                                //发送验证码
                                Call<CheckSMS> getSMS = HttpHelper.initHttpHelper().getSMS(tel_encryption,tel_unencrypted);
                                getSMS.enqueue(new Callback<CheckSMS>() {
                                    @Override
                                    public void onResponse(Call<CheckSMS> call, Response<CheckSMS> response) {
//                                        Log.e("state",JSON.toJSONString(response.body()));
                                        if ("success".equals(response.body().getFlag())){
                                            LoadingDialog.closeDialog(mDialog);
                                            clickButton(view);
                                            Toast.makeText(RebindPhoneActivity.this, "发送成功",Toast.LENGTH_SHORT).show();
                                        }else {
                                            LoadingDialog.closeDialog(mDialog);
                                            Toast.makeText(RebindPhoneActivity.this, "发送失败",Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CheckSMS> call, Throwable t) {
                                        LoadingDialog.closeDialog(mDialog);
                                        Toast.makeText(RebindPhoneActivity.this, "发送失败",Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<PublicKeyList> call, Throwable t) {
                            LoadingDialog.closeDialog(mDialog);
                        }
                    });
                }
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences pref = RebindPhoneActivity.this.getSharedPreferences("User_data",MODE_PRIVATE);
        state = pref.getString("token","");
    }
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if(count == 0){
                count = 60;
                rebindCodeBtn.setText("重新发送");
                rebindCodeBtn.setClickable(true);
                return;
            }
            count--;
            rebindCodeBtn.setText(count+"s后重新发送");
            sendEmptyMessageDelayed(COUNT_TIME,1000);
        };
    };
    public void clickButton(View view){
        handler.sendEmptyMessage(COUNT_TIME);
        rebindCodeBtn.setClickable(false);
    }
    public void back(View view) {
        finish();
    }
    /**
     * 重写返回键
     * 监听Back键按下事件
     * super.onBackPressed()会自动调用finish()方法,关闭当前Activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
