package zhixin.cn.com.happyfarm_user.easyMob;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import zhixin.cn.com.happyfarm_user.Adapter.ManageAddressAdapter;
import zhixin.cn.com.happyfarm_user.ModifyAddressActivity;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.ManageAddress;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

/**
 * Created by Administrator on 2018/6/22.
 */

public class GroupInfoAdapter extends RecyclerView.Adapter<GroupInfoAdapter.StaggerViewHolder>{

    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<GroupInfo> mList;
    private AdapterView.OnItemClickListener itemClickListener;
    private int position;

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public GroupInfoAdapter(Context context, List<GroupInfo> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public GroupInfoAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.em_group_info_items, null);
        //创建一个staggerViewHolder对象
        GroupInfoAdapter.StaggerViewHolder staggerViewHolder = new GroupInfoAdapter.StaggerViewHolder(view);
        //立即下架点击事件
//        staggerViewHolder.manageAddressBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                position = staggerViewHolder.getAdapterPosition();
//                GroupInfo groupInfo = mList.get(position);
//                Intent intent = new Intent(mContext, ModifyAddressActivity.class);
//                intent.putExtra("newAddress", "修改收货地址");
//                intent.putExtra("UserRealname",manageAddress.manageAddressName);
//                intent.putExtra("address", manageAddress.manageAddressDetail);
//                intent.putExtra("plotId",  manageAddress.plotId + "");
//                intent.putExtra("addressId", manageAddress.addressId + "");
//                intent.putExtra("isDefault", manageAddress.isDefault+"");
//                intent.putExtra("tel", manageAddress.manageAddressPhone);
//                mContext.startActivity(intent);
//            }
//        });
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(GroupInfoAdapter.StaggerViewHolder holder, int position) {
        //从集合里拿对应item数据对象
        GroupInfo dataBean = mList.get(position);
        //给Holder里面的控件对象设置数据
        setData(holder,dataBean);
    }

    public void setData(GroupInfoAdapter.StaggerViewHolder holder,GroupInfo data) {
        holder.groupUserName.setText(data.groupName);
        if (NumUtil.checkNull(data.groupImg)){
            holder.groupUserImg.setImageResource(R.drawable.icon_user_img);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                    .load(data.groupImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                    .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                    .into(holder.groupUserImg);//into(ImageView targetImageView)：图片最终要展示的地方。
        }
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private final TextView groupUserName;
        private final CircleImageView groupUserImg;

        public StaggerViewHolder(View itemView) {
            super(itemView);
            groupUserName = itemView.findViewById(R.id.group_user_name);
            groupUserName.setSelected(true);//添加跑马灯效果
            groupUserImg = itemView.findViewById(R.id.group_user_image);
        }

    }
    /**
     * 列表点击事件
     *
     * @param itemClickListener
     */
    public void setOnItemClickListener(AdapterView.OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
