package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hyphenate.util.DensityUtil;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.PersonalMessage;
import zhixin.cn.com.happyfarm_user.other.NumUtil;
import zhixin.cn.com.happyfarm_user.utils.OnMultiClickListener;

/**
 * Created by Administrator on 2018/6/13.
 */

public class PersonalMessageAdapter extends RecyclerView.Adapter<PersonalMessageAdapter.StaggerViewHolder>{

    private Context mContext;
    //泛型是RecycleView所需的bean类
    private List<PersonalMessage> mList;
    private MyItemClickListener mItemClickListener;
    private NewFriendClickListener newFriendClickListener;
    private GroupClickListener groupClickListener;
    private CustomerClickListener customerClickListener;
    private int position;
    private Set<String> listStr = new HashSet<>();

    //创建构造方法；一个需要接受两个参数，上下文，集合对象（包含了我们所需要的数据）
    public PersonalMessageAdapter(Context context, List<PersonalMessage> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public PersonalMessageAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.personal_message_items, null);
        //创建一个staggerViewHolder对象
        PersonalMessageAdapter.StaggerViewHolder staggerViewHolder = new PersonalMessageAdapter.StaggerViewHolder(view);
//        多选框点击事件
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    //当VIewHolder和数据绑定时回调
    public void onBindViewHolder(@NonNull PersonalMessageAdapter.StaggerViewHolder holder, int position) {
//        if (holder.getAdapterPosition() == 0) {
//            holder.headerLayout.setVisibility(View.VISIBLE);
//        } else {
//            holder.headerLayout.setVisibility(View.GONE);
//        }
        //从集合里拿对应item数据对象
        PersonalMessage dataBean = mList.get(position);
        int section = getSectionForPosition(position);
        //如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
        if (position == getPositionForSection(section)) {
            holder.header.setVisibility(View.VISIBLE);
            holder.messageTopView.setVisibility(View.VISIBLE);//显示上方横线
            holder.header.setText(mList.get(position).getLetters());
        } else {
            holder.header.setVisibility(View.GONE);
            holder.messageTopView.setVisibility(View.GONE);//隐藏上方横线
        }
        //添加最后一项距离底部高度 start
        if (getItemCount() > 3 && position == getItemCount() - 1) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.messageItem.getLayoutParams();
            layoutParams.setMargins(0, 0, 0, DensityUtil.dip2px(mContext, 30));//4个参数按顺序分别是左上右下
            holder.messageItem.setLayoutParams(layoutParams);
        }else{
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.messageItem.getLayoutParams();
            layoutParams.setMargins(0, 0, 0, 0);//4个参数按顺序分别是左上右下
            holder.messageItem.setLayoutParams(layoutParams);
        }
        //添加最后一项距离底部高度 end
        if (mItemClickListener != null){
            holder.itemView.setOnClickListener(new OnMultiClickListener() {
                @Override
                public void onMultiClick(View v) {
                    //TODO 这里不传int pos = holder.getLayoutPosition();  主要是搜索出来的点击会去选中item的位置
                    int pos = Integer.parseInt(mList.get(position).getPersonalId());
                    mItemClickListener.onItemClick(v,pos);
                }
            });
            holder.messageNewFriend.setOnClickListener(new OnMultiClickListener() {
                @Override
                public void onMultiClick(View view) {
                    newFriendClickListener.onNewFriendClick(view);
                }
            });
            holder.messageGroup.setOnClickListener(new OnMultiClickListener() {
                @Override
                public void onMultiClick(View view) {
                    groupClickListener.onGroupClick(view);
                }
            });
            holder.messageCustomer.setOnClickListener(new OnMultiClickListener() {
                @Override
                public void onMultiClick(View view) {
                    customerClickListener.onCustomerClick(view);
                }
            });
        }
        //给Holder里面的控件对象设置数据
        setData(holder,dataBean);
    }

    public void setData(PersonalMessageAdapter.StaggerViewHolder holder,PersonalMessage data) {
        holder.contactName.setText(data.getPersonalName());
        /*  TODO 搜索框搜索选中状态消失 加上一下判断  */
//        if (listStr.contains(data.contactId)){
//            holder.checkBox.setChecked(true);
//        }else {
//            holder.checkBox.setChecked(false);
//        }
        /*  TODO *******************************  */
        if (NumUtil.checkNull(data.getPersonalImg())){
            holder.contactImg.setImageResource(R.drawable.icon_user_img);
        }else {
            //Picasso使用了流式接口的调用方式
            //Picasso类是核心实现类。
            //实现图片加载功能至少需要三个参数：
//            Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
//                    .load(data.getPersonalImg())//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
//                    .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
//                    .resize(35, 35)
//                    .centerCrop()
//                    .into(holder.contactImg);//into(ImageView targetImageView)：图片最终要展示的地方。
            Glide.with(mContext)
                    .load(data.getPersonalImg())
                    .into(holder.contactImg);
        }
    }

    @Override
    public int getItemCount() {
        //决定RecycleView有多少条item
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {

        private final CircleImageView contactImg;
        private final TextView contactName;
        private final TextView header;
        private final RelativeLayout messageItem;//item布局
        private final View messageTopView;//item布局上方横线
        private final RelativeLayout headerLayout;
        private final RelativeLayout messageNewFriend;//新的朋友
        private final RelativeLayout messageGroup;//群组
        private final RelativeLayout messageCustomer;//客服
        private final ImageView newFriendIcon;
        private final ImageView groupIcon;
        private final ImageView customerIcon;

        public StaggerViewHolder(View itemView) {
            super(itemView);
            contactImg = itemView.findViewById(R.id.personal_avatar);
            contactName = itemView.findViewById(R.id.personal_name);
            header = itemView.findViewById(R.id.personal_header);
            messageItem = itemView.findViewById(R.id.message_item);
            messageTopView = itemView.findViewById(R.id.message_top_view);
            headerLayout = itemView.findViewById(R.id.head_layout);
            messageNewFriend = itemView.findViewById(R.id.message_new_friend2);
            messageGroup = itemView.findViewById(R.id.message_group2);
            messageCustomer = itemView.findViewById(R.id.message_customer2);
            newFriendIcon = itemView.findViewById(R.id.new_friend_icon);
            groupIcon = itemView.findViewById(R.id.group_icon);
            customerIcon = itemView.findViewById(R.id.customer_icon);
        }

    }
    public Set<String> contactIdList() {
        return listStr;
    }
    /**
     * 创建一个回调接口
     */
    public interface MyItemClickListener {
        void onItemClick(View view, int position);

//        void onNewFriendClick(View view);
//        void onGroupClick(View view);
//        void onCustomerClick(View view);
    }

    public interface NewFriendClickListener {
        void onNewFriendClick(View view);
    }

    public interface GroupClickListener {
        void onGroupClick(View view);
    }
    public interface CustomerClickListener {
        void onCustomerClick(View view);
    }

    /**
     * 在activity里面adapter就是调用的这个方法,将点击事件监听传递过来,并赋值给全局的监听
     *
     * @param myItemClickListener
     */
    public void setItemClickListener(PersonalMessageAdapter.MyItemClickListener myItemClickListener) {
        this.mItemClickListener = myItemClickListener;
    }

    public void setNewFriendClickListener(PersonalMessageAdapter.NewFriendClickListener newFriendClickListener) {
        this.newFriendClickListener = newFriendClickListener;
    }

    public void setGroupClickListener(PersonalMessageAdapter.GroupClickListener groupClickListener) {
        this.groupClickListener = groupClickListener;
    }

    public void setCustomerClickListener(PersonalMessageAdapter.CustomerClickListener customerClickListener) {
        this.customerClickListener = customerClickListener;
    }
    /**
     * 提供给Activity刷新数据
     * @param list
     */
    public void updateList(List<PersonalMessage> list){
        this.mList = list;

        notifyDataSetChanged();
    }
    /**
     * 根据ListView的当前位置获取分类的首字母的char ascii值
     */
    public int getSectionForPosition(int position) {
        return mList.get(position).getLetters().charAt(0);
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mList.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }
}
