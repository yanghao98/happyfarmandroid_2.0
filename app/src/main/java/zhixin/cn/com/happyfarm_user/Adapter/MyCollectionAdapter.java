package zhixin.cn.com.happyfarm_user.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import zhixin.cn.com.happyfarm_user.R;
import zhixin.cn.com.happyfarm_user.model.MyCollection;
import zhixin.cn.com.happyfarm_user.other.NumUtil;

public class MyCollectionAdapter extends RecyclerView.Adapter<MyCollectionAdapter.StaggerViewHolder>{

    private CuringAdapter.MyItemClickListener mItemClickListener;
    private Context mContext;
    private List<MyCollection> mList;

    public MyCollectionAdapter(Context context,List<MyCollection> list){
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public MyCollectionAdapter.StaggerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //转换一个View布局，决定了item的样子， 参数:1.上下文 2.Xml布局资源 3.为null
        View view = View.inflate(mContext, R.layout.my_collection_item, null);
        //创建一个staggerViewHolder对象
        StaggerViewHolder staggerViewHolder = new StaggerViewHolder(view);
        //把staggerViewHolder对象传出去
        return staggerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyCollectionAdapter.StaggerViewHolder holder, int position) {
        MyCollection dataBean = mList.get(position);
        holder.setData(dataBean);
        if (mItemClickListener != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = holder.getLayoutPosition();
                    mItemClickListener.onItemClick(view,pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(mList!=null&&mList.size()>0){
            return mList.size();
        }
        return 0;
    }

    public class StaggerViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView mIcon;
        private TextView LandNos;
        private TextView UserNames;

        public StaggerViewHolder(View itemView) {
            super(itemView);
            mIcon = itemView.findViewById(R.id.userImage);
            LandNos = itemView.findViewById(R.id.landNos);
            UserNames = itemView.findViewById(R.id.userName);
        }

        public void setData(MyCollection data) {
//            Log.e("MyCollectionAdapter->", "setData: "+data.UserImg);
            if(NumUtil.checkNull(data.UserImg)){
                mIcon.setImageResource(R.drawable.icon_user_img);
            }else {
                //给ImageView设置图片数据
                //Picasso使用了流式接口的调用方式
                //Picasso类是核心实现类。
                //实现图片加载功能至少需要三个参数：
                Picasso.get()//with(Context context)Context对于很多Android API的调用都是必须的，这里就不多说了
                        .load(data.UserImg)//load(String imageUrl)：被加载图像的Url地址。大多情况下，一个字符串代表一个网络图片的URL。
                        .placeholder(R.drawable.icon_user_img)
                        .error(R.drawable.icon_user_img)//如果请求图像无法加载，则可以使用一个错误绘制。
                        .into(mIcon);//into(ImageView targetImageView)：图片最终要展示的地方。
            }
            LandNos.setText(data.LandNo);
            UserNames.setText(data.UserName);

        }

    }

    /**
     * 创建一个回调接口
     */
    public interface MyItemClickListener {
        void onItemClick(View view, int position);
    }

    /**
     * 在activity里面adapter就是调用的这个方法,将点击事件监听传递过来,并赋值给全局的监听
     *
     * @param myItemClickListener
     */
    public void setItemClickListener(CuringAdapter.MyItemClickListener myItemClickListener) {
        this.mItemClickListener = myItemClickListener;
    }

}
