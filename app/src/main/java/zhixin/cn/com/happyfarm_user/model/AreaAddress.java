package zhixin.cn.com.happyfarm_user.model;

import java.util.List;

public class AreaAddress {
    private List<AreaAddressBean> result;
    private String flag;

    public List<AreaAddressBean> getResult() {
        return result;
    }

    public void setResult(List<AreaAddressBean> result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static class AreaAddressBean {
        private int id;
        private String awardRegion;
        private int state;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAwardRegion() {
            return awardRegion;
        }

        public void setAwardRegion(String awardRegion) {
            this.awardRegion = awardRegion;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }
}
