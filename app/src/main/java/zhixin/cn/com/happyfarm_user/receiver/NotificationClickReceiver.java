package zhixin.cn.com.happyfarm_user.receiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import zhixin.cn.com.happyfarm_user.base.App;
import zhixin.cn.com.happyfarm_user.utils.SystemHelper;

/**
 * NotificationClickReceiver
 *
 * @author: Administrator.
 * @date: 2019/8/6
 */
public class NotificationClickReceiver extends BroadcastReceiver {

    @SuppressLint("LongLogTag")
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("NotificationClickReceiver->", "onReceive: ");
        if (SystemHelper.isRun(App.CONTEXT)){
            Log.i("NotificationClickReceiver->", "onReceive: " + "app在运行");
            SystemHelper.setTopApp(App.getInstance());
        }
    }
}